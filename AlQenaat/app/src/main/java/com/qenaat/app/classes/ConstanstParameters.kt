package com.qenaat.app.classes

/**
 * Created by shahbazshaikh on 29/03/16.
 */
object ConstanstParameters {
    var key_YouTube: String? = "YouTube"
    var key_Instagram: String? = "Instagram"
    var key_FaceBook: String? = "FaceBook"
    var key_Twitter: String? = "Twitter"
    var key_SnapChat: String? = "SnapChat"
    var KUWAIT_CODE = "965"
    var COUNTRY_CODE = "code"
    var COUNTRY_NAME = "name"
    var COUNTRY_FLAG = "flag"
    var API_SUCCESS_CODE = 200
    var AUTH_TEXT = "Bearer"
}