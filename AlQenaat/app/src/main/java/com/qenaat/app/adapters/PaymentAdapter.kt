package com.qenaat.app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetPaymentLists


class PaymentAdapter(a: FragmentActivity, itemsData: ArrayList<GetPaymentLists.ListItem>) :
    RecyclerView.Adapter<PaymentAdapter.ViewHolder?>() {

    private val itemsData: ArrayList<GetPaymentLists.ListItem>
    var act: FragmentActivity
    var languageSeassionManager: LanguageSessionManager
    var type: String = ""

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.payment_list, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
//            itemsData.get(position).list?.forEach {
//                if (it != null) {
//
//                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
//
//
//                        viewHolder.tv_category.text =
//                            act.getString(R.string.category) + "" + it.CatName
//
//                        Log.e("categoryyyyyyyyy", "" + viewHolder.tv_category.text)
////                        viewHolder.tv_category.text = act.getString(R.string.category)+""+ itemsData.get(position)!!.CatName
//                        viewHolder.tv_donationID.text =
//                            act.getString(R.string.DonationID) + "" + it.DonationNumber
//                        viewHolder.tv_amount.text =
//                            act.getString(R.string.amount) + "" + it.DonnationValue
//                        if (it.SubCatName == null) {
//                            viewHolder.tv_subcategory.text =
//                                act.getString(R.string.subcategory) + "" + "N/A"
//
//                        } else {
//                            viewHolder.tv_subcategory.text =
//                                act.getString(R.string.subcategory) + "" + it.SubCatName
//
//                        }
//
//                    } else {
//                        viewHolder.tv_category.text =
//                            act.getString(R.string.category) + "" + it.CatNameAR
//                        viewHolder.tv_donationID.text =
//                            act.getString(R.string.DonationID) + "" + it.DonationNumber
//                        viewHolder.tv_amount.text =
//                            act.getString(R.string.amount) + "" + it.DonnationValue
////                viewHolder.tv_subcategory.text = act.getString(R.string.subcategory)+""+itemsData.get(position).SubCatNameAR
//
//                        if (it.SubCatName == null) {
//                            viewHolder.tv_subcategory.text =
//                                act.getString(R.string.subcategory) + "" + "N/A"
//
//                        } else {
//                            viewHolder.tv_subcategory.text =
//                                act.getString(R.string.subcategory) + "" + it.SubCatNameAR
//
//                        }
//
//                    }
////
//                    if (it.IsPaid == "0") {
//                        viewHolder.tv_notpaid.setVisibility(View.VISIBLE)
//                        viewHolder.tv_paid.setVisibility(View.GONE)
//                    } else {
//                        viewHolder.tv_paid.setVisibility(View.VISIBLE)
//                        viewHolder.tv_notpaid.setVisibility(View.GONE)
//                    }
//                }
//
//            }

        viewHolder.relative_content.visibility = View.VISIBLE

            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {

                viewHolder.tv_category.text =
                    act.getString(R.string.category) + " " + itemsData.get(position)!!.CatName
                viewHolder.tv_donationID.text =
                    act.getString(R.string.DonationID) + " " + itemsData.get(position)!!.DonationNumber
                viewHolder.tv_amount.text =
                    act.getString(R.string.amount) + " " + itemsData.get(position)!!.DonnationValue

                if (itemsData.get(position)!!.SubCatName == null) {

                    viewHolder.tv_subcategory.text =
                        act.getString(R.string.subcategory) + " " + "N/A"

                } else {
                    viewHolder.tv_subcategory.text =
                        act.getString(R.string.subcategory) + " " + itemsData.get(position)!!.SubCatName

                }

            } else {
                viewHolder.tv_category.text =
                    act.getString(R.string.category) + " " + itemsData.get(position)!!.CatNameAR
                viewHolder.tv_donationID.text =
                    act.getString(R.string.DonationID) + " " + itemsData.get(position)!!.DonationNumber
                viewHolder.tv_amount.text =
                    act.getString(R.string.amount) + " " + itemsData.get(position)!!.DonnationValue
//                viewHolder.tv_subcategory.text = act.getString(R.string.subcategory)+""+itemsData.get(position).SubCatNameAR

                if (itemsData.get(position)!!.SubCatName == null) {
                    viewHolder.tv_subcategory.text =
                        act.getString(R.string.subcategory) + " " + "N/A"

                } else {
                    viewHolder.tv_subcategory.text =
                        act.getString(R.string.subcategory) + " " + itemsData.get(position)!!.SubCatNameAR

                }

            }

            if (itemsData.get(position)!!.IsPaid == "0") {
                viewHolder.tv_notpaid.setVisibility(View.VISIBLE)
                viewHolder.tv_paid.setVisibility(View.GONE)
            } else {
                viewHolder.tv_paid.setVisibility(View.VISIBLE)
                viewHolder.tv_notpaid.setVisibility(View.GONE)
            }


    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {

        var relative_parent: RelativeLayout
        var tv_donationID: TextView
        var tv_category: TextView
        var tv_subcategory: TextView
        var tv_amount: TextView
        var tv_paid: TextView
        var tv_notpaid: TextView
        var relative_content: RelativeLayout

        init {
            tv_donationID = itemLayoutView.findViewById<View?>(R.id.tv_donationID) as TextView
            relative_content = itemLayoutView.findViewById<View?>(R.id.relative_content) as RelativeLayout
            tv_category = itemLayoutView.findViewById<View?>(R.id.tv_category) as TextView
            tv_subcategory = itemLayoutView.findViewById<View?>(R.id.tv_subcategory) as TextView
            tv_amount = itemLayoutView.findViewById<View?>(R.id.tv_amount) as TextView
            tv_paid = itemLayoutView.findViewById<View?>(R.id.tv_paid) as TextView
            tv_notpaid = itemLayoutView.findViewById<View?>(R.id.tv_notpaid) as TextView
            relative_parent =
                itemLayoutView.findViewById<View?>(R.id.relative_parent) as RelativeLayout
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        this.type = type
        act = a
        this.itemsData = itemsData
        languageSeassionManager = LanguageSessionManager(act)
    }

}


