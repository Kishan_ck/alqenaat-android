package com.qenaat.app.Utils

import android.content.Context
import com.jdev.countryutil.Country
import com.qenaat.app.classes.ConstanstParameters.COUNTRY_CODE
import com.qenaat.app.classes.ConstanstParameters.COUNTRY_FLAG
import com.qenaat.app.classes.ConstanstParameters.COUNTRY_NAME

class Util {

    companion object {

        //const val KEY_PAYMENT = "sk_test_L9ejIsig5Fq1vBUw6mCX2HMb"
        //const val KEY_PAYMENT = "sk_test_l06oHhuZ2vXIfLxGz3bPJjCM"
        //const val KEY_PAYMENT = "sk_test_rcqXAdjRlsZyIiDa7hN5E8V1"
        const val KEY_PAYMENT = "sk_test_rcqXAdjRlsZyIiDa7hN5E8V1"

        fun getUserCountryInfo(context: Context, action: String): String {
            var text = ""
            val country = Country.getCountryFromSIM(context)
            if (country != null) {
                when (action) {
                    COUNTRY_CODE -> text = country.dialCode
                    COUNTRY_NAME -> text = country.code
                    COUNTRY_FLAG -> text = country.flag.toString()
                }
            }
            return text
        }

        fun getCountryByCode(context: Context, code: String): Country {
            return Country.getCountryByDialCode(code)
        }
    }
}