package com.qenaat.app.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GPSTracker
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager

/**
 * Created by DELL on 11/10/2016.
 */
class CurrentLocationFragment : Fragment(), View.OnClickListener {
    lateinit var act: FragmentActivity
    lateinit var mLangSessionManager: LanguageSessionManager
    lateinit var mainLayout: RelativeLayout
    private lateinit var map: GoogleMap

    // GPSTracker class
    lateinit var gps: GPSTracker
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    lateinit var tv_done: TextView
    lateinit var tv_hybrid_map: TextView
    lateinit var tv_normal_map: TextView
    lateinit var relative_map: RelativeLayout
    lateinit var relative_normal_map: RelativeLayout
    lateinit var relative_hybrid_map: RelativeLayout
    var isNormalMap = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            mLangSessionManager = LanguageSessionManager(act)
            mSessionManager = SessionManager(act)
            if (arguments != null) {
                //urlForMap = getArguments().getString("urlForMap");
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return try {
            mainLayout = inflater.inflate(R.layout.current_location, null) as RelativeLayout
            initViews(mainLayout)
            mainLayout
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun initViews(mainLayout: RelativeLayout) {
        Log.e("initViews", "initViews")
        try {
            tv_done = mainLayout.findViewById(R.id.tv_done) as TextView
            tv_hybrid_map = mainLayout.findViewById(R.id.tv_hybrid_map) as TextView
            tv_normal_map = mainLayout.findViewById(R.id.tv_normal_map) as TextView
            relative_normal_map = mainLayout.findViewById(R.id.relative_normal_map) as RelativeLayout
            relative_hybrid_map = mainLayout.findViewById(R.id.relative_hybrid_map) as RelativeLayout
            relative_map = mainLayout.findViewById(R.id.relative_map) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_map)
            relative_hybrid_map.setOnClickListener(this)
            relative_normal_map.setOnClickListener(this)
            tv_done.setOnClickListener(this)
            tv_done.setTypeface(ContentActivity.Companion.tf)
        } catch (e: Exception) {
            Log.e(TAG + " " + " initViews: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onStart() {
        super.onStart()
     //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(mLangSessionManager)
        gps = GPSTracker(act)
        // check if GPS enabled
        if (gps.canGetLocation()) {
            if (ContentActivity.Companion.latitude.length > 0
                    && ContentActivity.Companion.longitude.length > 0) {
                Log.d("latitude", "there")
                currentLatitude = ContentActivity.Companion.latitude.toDouble()
                currentLongitude = ContentActivity.Companion.longitude.toDouble()
            } else {
                Log.d("latitude", "not there")
                currentLatitude = gps.getLatitude()
                currentLongitude = gps.getLongitude()
            }
            ContentActivity.Companion.longitude = currentLongitude.toString() + ""
            ContentActivity.Companion.latitude = currentLatitude.toString() + ""
            ContentActivity.Companion.location = "$currentLatitude, $currentLongitude"
            val supportMapFragment: SupportMapFragment? = fragment.getChildFragmentManager().findFragmentById(R.id.map) as SupportMapFragment?
            supportMapFragment?.getMapAsync(object : OnMapReadyCallback {
                override fun onMapReady(googleMap: GoogleMap?) {
                    map = googleMap!!
                    setupMap()
                }
            })
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert()
        }
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.relative_normal_map -> {
                relative_hybrid_map.setBackgroundColor(Color.parseColor("#f8e8c7"))
                tv_hybrid_map.setTextColor(Color.LTGRAY)
                relative_normal_map.setBackgroundColor(Color.parseColor("#263eaa"))
                tv_normal_map.setTextColor(Color.parseColor("#f8e8c7"))
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL)
            }
            R.id.relative_hybrid_map -> {
                relative_normal_map.setBackgroundColor(Color.parseColor("#f8e8c7"))
                tv_normal_map.setTextColor(Color.LTGRAY)
                relative_hybrid_map.setBackgroundColor(Color.parseColor("#263eaa"))
                tv_hybrid_map.setTextColor(Color.parseColor("#f8e8c7"))
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID)
            }
            R.id.tv_done -> fragmentManager?.popBackStack()
        }
    }

    private fun moveMap() {
        val cameraPosition: CameraPosition = CameraPosition.Builder().target(LatLng(currentLatitude,
                currentLongitude)).zoom(15f).build()
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun setupMap() {

        // Creating a marker
        val markerOptions = MarkerOptions()

        // Setting the position for the marker
        markerOptions.position(LatLng(currentLatitude, currentLongitude))

        // Clears the previously touched position
        map.clear()

        // Animating to the touched position
        map.animateCamera(CameraUpdateFactory.newLatLng(LatLng(currentLatitude,
                currentLongitude)))

        // Placing a marker on the touched position
        map.addMarker(markerOptions)

        // \n is for new line
        //Toast.makeText(act, "Your Location is - \nLat: " + currentLatitude + "\nLong: " +
        //currentLongitude, Toast.LENGTH_LONG).show();
        moveMap()
        isNormalMap = false
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID)
        map.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
            override fun onMapClick(latLng: LatLng?) {

                // Creating a marker
                val markerOptions = MarkerOptions()

                // Setting the position for the marker
                markerOptions.position(latLng!!)

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude.toString() + " : " + latLng.longitude)
                currentLatitude = latLng.latitude
                currentLongitude = latLng.longitude
                ContentActivity.Companion.longitude = currentLongitude.toString() + ""
                ContentActivity.Companion.latitude = currentLatitude.toString() + ""
                ContentActivity.Companion.location = "$currentLatitude, $currentLongitude"

                // Clears the previously touched position
                map.clear()

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng))

                // Placing a marker on the touched position
                map.addMarker(markerOptions)
            }
        })
    }

    companion object {
        protected val TAG = CurrentLocationFragment::class.java.simpleName
        lateinit var fragment: CurrentLocationFragment
        lateinit var mSessionManager: SessionManager
        fun newInstance(act: FragmentActivity): CurrentLocationFragment? {
            fragment = CurrentLocationFragment()
            fragment.act = act
            return fragment
        }
    }
}