package com.qenaat.app.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.content.ClipboardManager
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.text.*
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util.Companion.KEY_PAYMENT
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetProjectDetail
import com.qenaat.app.model.GetProjects
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import company.tap.gosellapi.GoSellSDK
import company.tap.gosellapi.SettingsManager
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.PhoneNumber
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.internal.interfaces.PaymentInterface
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.AppearanceMode
import company.tap.gosellapi.open.enums.CardType
import company.tap.gosellapi.open.enums.TransactionMode
import company.tap.gosellapi.open.models.*
import kotlinx.android.synthetic.main.project_donation_details.*
import kotlinx.android.synthetic.main.top_bar.view.*
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.math.BigDecimal
import java.util.*

/**
 * Created by DELL on 14-Nov-17.
 */
class ProjectDonationDetailsFragment : Fragment(), View.OnClickListener, SessionDelegate, PaymentInterface {
    private var Charge_id = ""
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var relative_top: RelativeLayout
    lateinit var img_event: ImageView
    lateinit var img_reminder: ImageView
    lateinit var img_fav: ImageView
    lateinit var img_share: ImageView
    lateinit var img_share1: ImageView
    lateinit var img_report: ImageView
    lateinit var img_facebook: ImageView
    lateinit var img_instagram: ImageView
    lateinit var img_twitter: ImageView
    lateinit var img_youtube: ImageView
    lateinit var img_location: ImageView
    lateinit var img_call: ImageView
    lateinit var img_calender: ImageView
    lateinit var linear_date: LinearLayout
    lateinit var linear_bottom: LinearLayout
    lateinit var linear_social: LinearLayout
    lateinit var tv_days: TextView
    lateinit var tv_views: TextView
    lateinit var tv_occasion_details: TextView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var getProjectsForDonation: GetProjectDetail
    lateinit var projectId: String
    lateinit var productImages: Array<String?>
    private val CALL_PHONE_PERMISSION_CODE = 23
    lateinit var pShareUri: Uri
    lateinit var file: File
    lateinit var relative_desc: RelativeLayout
    lateinit var tv_tree: TextView
    private var loadingFinished = true
    private var redirect = false
    lateinit var webView: WebView
    lateinit var img_count: TextView
    lateinit var img_expire: ImageView
    lateinit var relative_gallery: RelativeLayout
    lateinit var relative_bottom: RelativeLayout
    lateinit var tv_pay: TextView
    lateinit var tv_due_amount: TextView
    lateinit var tv_kd_label: TextView
    lateinit var et_due_amount: EditText
    private lateinit var sdkSession: SDKSession
    private var settingsManager: SettingsManager? = null
    private val SDK_REQUEST_CODE = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments?.containsKey("GetProjects")!!) {
                    val gson = Gson()
                    projectId = gson.fromJson(arguments?.getString("GetProjects"),
                            GetProjects::class.java).Id!!
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.project_donation_details, null) as RelativeLayout
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            relative_bottom = mainLayout.findViewById(R.id.relative_bottom) as RelativeLayout
            relative_gallery = mainLayout.findViewById(R.id.relative_gallery) as RelativeLayout
            img_expire = mainLayout.findViewById(R.id.img_expire) as ImageView
            img_count = mainLayout.findViewById(R.id.img_count) as TextView
            webView = mainLayout.findViewById(R.id.webView) as WebView
            tv_kd_label = mainLayout.findViewById(R.id.tv_kd_label) as TextView
            tv_due_amount = mainLayout.findViewById(R.id.tv_due_amount) as TextView
            tv_pay = mainLayout.findViewById(R.id.tv_pay) as TextView
            tv_tree = mainLayout.findViewById(R.id.tv_tree) as TextView
            et_due_amount = mainLayout.findViewById(R.id.et_due_amount) as EditText
            relative_desc = mainLayout.findViewById(R.id.relative_desc) as RelativeLayout
            img_reminder = mainLayout.findViewById(R.id.img_reminder) as ImageView
            img_fav = mainLayout.findViewById(R.id.img_fav) as ImageView
            img_share = mainLayout.findViewById(R.id.img_share) as ImageView
            img_share1 = mainLayout.findViewById(R.id.img_share1) as ImageView
            img_report = mainLayout.findViewById(R.id.img_report) as ImageView
            img_facebook = mainLayout.findViewById(R.id.img_facebook) as ImageView
            img_instagram = mainLayout.findViewById(R.id.img_instagram) as ImageView
            img_twitter = mainLayout.findViewById(R.id.img_twitter) as ImageView
            img_youtube = mainLayout.findViewById(R.id.img_youtube) as ImageView
            img_location = mainLayout.findViewById(R.id.img_location) as ImageView
            img_call = mainLayout.findViewById(R.id.img_call) as ImageView
            img_calender = mainLayout.findViewById(R.id.img_calender) as ImageView
            img_event = mainLayout.findViewById(R.id.img_event) as ImageView
            tv_views = mainLayout.findViewById(R.id.tv_views) as TextView
            tv_occasion_details = mainLayout.findViewById(R.id.tv_occasion_details) as TextView
            tv_days = mainLayout.findViewById(R.id.tv_days) as TextView
            linear_date = mainLayout.findViewById(R.id.linear_date) as LinearLayout
            linear_bottom = mainLayout.findViewById(R.id.linear_bottom) as LinearLayout
            linear_social = mainLayout.findViewById(R.id.linear_social) as LinearLayout
            relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            img_calender.setOnClickListener(this)
            img_call.setOnClickListener(this)
            img_location.setOnClickListener(this)
            img_youtube.setOnClickListener(this)
            img_twitter.setOnClickListener(this)
            img_instagram.setOnClickListener(this)
            img_facebook.setOnClickListener(this)
            img_report.setOnClickListener(this)
            img_share.setOnClickListener(this)
            img_share1.setOnClickListener(this)
            tv_pay.setOnClickListener(this)
            img_fav.setOnClickListener(this)
            img_reminder.setOnClickListener(this)
            img_event.setOnClickListener(this)
            tv_views.setTypeface(ContentActivity.Companion.tf)
            tv_tree.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_occasion_details.setTypeface(ContentActivity.Companion.tf)
            tv_days.setTypeface(ContentActivity.Companion.tf)
            img_reminder.setVisibility(View.GONE)
            img_calender.setVisibility(View.GONE)
            img_call.setImageResource(R.drawable.call_big)
            img_location.setImageResource(R.drawable.location_big)
            tv_days.setVisibility(View.VISIBLE)
        }
        img_location.setVisibility(View.VISIBLE)
        img_call.setImageResource(R.drawable.call_big)
        img_location.setImageResource(R.drawable.location_big)
        img_call.setVisibility(View.VISIBLE)
        linear_bottom.setVisibility(View.GONE)
        relative_desc.setBackgroundResource(R.drawable.details_bg_big)
        img_share1.setVisibility(View.VISIBLE)
        linear_date.setVisibility(View.VISIBLE)
        ContentActivity.Companion.setTextFonts(mainLayout)

        settingsManager = SettingsManager.getInstance()
        settingsManager?.setPref(act)

        et_due_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (!et_due_amount.checkEmpty())
                    sdkSession.setAmount(BigDecimal(et_due_amount.getTextValue()))
                else
                    sdkSession.setAmount(BigDecimal("0"))
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

        startSDK()
    }

    override fun onStart() {
        super.onStart()
        //  ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.ProjectDetails).replace("*", ""))
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ProjectsForDonationById()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.img_share -> shareContent()
            R.id.tv_pay ->
                if (getProjectsForDonation.HaveOpenBudget.equals("false", ignoreCase = true)) {
                    if (et_due_amount.getText().toString().length > 0) {
                        if (et_due_amount.getText().toString().toDouble() > 0) {
                            var isOpenDonation = false
                            if (getProjectsForDonation.HaveOpenBudget.equals("true", ignoreCase = true)) {
                                isOpenDonation = true
                            }
                            if (isOpenDonation) {
                                AddDonationForProject()
                            } else {
                                if (getProjectsForDonation.Amount!!.toDouble() >= et_due_amount.getText().toString().toInt()) {
                                    AddDonationForProject()
                                } else {
                                    Snackbar.make(mainLayout, act.getString(R.string.RemainingAmountError), Snackbar.LENGTH_LONG).show()
                                }
                            }
                        } else {
                            Snackbar.make(mainLayout, act.getString(R.string.AmountInvalid), Snackbar.LENGTH_LONG).show()
                        }
                    } else {
                        Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                    }
                } else {
                    Snackbar.make(mainLayout, act.getString(R.string.ProjectClosed), Snackbar.LENGTH_LONG).show()
                }
            R.id.img_event -> {

//                if(getProjectsForDonation.getCompanyPhotos().size()>0){
//
//                    productImages = new String[getProjectsForDonation.getCompanyPhotos().size()];
//
//                    for(int i=0; i<getProjectsForDonation.getCompanyPhotos().size(); i++){
//
//                        GetProjects.CompanyPhotos photo = getProjectsForDonation.getCompanyPhotos().get(i);
//
//                        productImages[i] = photo.getPhotos();
//
//                    }
//
//
//                } else{
                if (getProjectsForDonation.Photo != null && getProjectsForDonation.Photo != "") {
                    productImages = arrayOfNulls<String?>(1)
                    productImages[0] = getProjectsForDonation.Photo
                }

//                }
                val b = Bundle()
                b.putInt("position", 0)
                b.putStringArray("productImages", productImages)
                ContentActivity.Companion.openGalleryFragmentFadeIn(b)
            }
        }
    }

    fun ProjectsForDonationById() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetProjectDetail(projectId)?.enqueue(
                object : Callback<GetProjectDetail?> {
                    override fun onFailure(call: Call<GetProjectDetail?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<GetProjectDetail?>,
                                            response: Response<GetProjectDetail?>) {
                        if (response.body() != null) {
                            val getContentses = response.body()
                            if (mloading != null) {
                                if (getContentses != null) {
                                    getProjectsForDonation = getContentses
                                    relative_bottom.setVisibility(View.GONE)
                                    LL_payButton.setVisibility(View.GONE)

                                    if (getProjectsForDonation.Amount!!.length > 0) {
                                        if (getProjectsForDonation.Amount!!.toDouble() > 0) {
                                            relative_bottom.setVisibility(View.VISIBLE)
                                            LL_payButton.setVisibility(View.VISIBLE)
                                        }
                                    }
                                    img_expire.setVisibility(View.GONE)

//                            img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                                    R.drawable.no_img_details)).getBitmap().getWidth();
                                    img_event.getLayoutParams().height = (act.getResources().getDrawable(
                                            R.drawable.no_img_details) as BitmapDrawable).getBitmap().getHeight()
                                    if (getProjectsForDonation.Photo!!.length > 0) {
                                        Picasso.with(act).load(getProjectsForDonation.Photo).placeholder(R.drawable.no_img_details)
                                                .error(R.drawable.no_img_details).into(img_event)
                                    }
                                    relative_gallery.setVisibility(View.INVISIBLE)
                                    //                            if(getProjectsForDonation.getCompanyPhotos().size()>=2){
//                                relative_gallery.setVisibility(View.VISIBLE);
//                                img_count.setText(getProjectsForDonation.getCompanyPhotos().size()+"");
//                            }

                                    //tv_days.setText(getProjectsForDonation.getFullName());
                                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                                        setContent("", getProjectsForDonation.ProjectNameEN + "\n\n" + getProjectsForDonation.DetailsEN)
                                    } else {
                                        setContent("", getProjectsForDonation.ProjectNameAR + "\n\n" + getProjectsForDonation.DetailsAr)
                                    }
                                    if (getProjectsForDonation.HaveOpenBudget.equals("false", ignoreCase = true)) {
                                        tv_days.setText(act.getString(R.string.TotalCost) + " " + getProjectsForDonation.Amount + " " + act.getString(R.string.KD))
                                        tv_due_amount.setText(getProjectsForDonation.Amount + " " + act.getString(R.string.KD))
                                    } else {
                                        tv_days.setText(act.getString(R.string.OpenDonation))
                                        tv_due_amount.setText("")
                                        relative_bottom.setVisibility(View.VISIBLE)
                                        LL_payButton.setVisibility(View.VISIBLE)
                                    }
                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {

        //Checking the request code of our request
        if (requestCode == CALL_PHONE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
            } else {
                //Displaying another toast if permission is not granted
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    fun setContent(title: String?, content: String?) {
        val sp: Spannable = SpannableString(title)
        Linkify.addLinks(sp, Linkify.ALL)
        val sp1: Spannable = SpannableString(content)
        Linkify.addLinks(sp1, Linkify.ALL)
        var finalHTML = ""
        finalHTML = if (title?.length!! > 0) {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center><center>" + Html.toHtml(sp)
                    + "</center></b></span style='color: #4c4c4c;'><br /><br /><center>" + Html.toHtml(sp1) + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        } else {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center>" + Html.toHtml(sp)
                    + "</center></span style='color: #4c4c4c;'><center>" + Html.toHtml(sp1) + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        }
        webView.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
        webView.setBackgroundColor(0)
        webView.setWebViewClient(WebViewClient())
        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true)
        webView.getSettings().setPluginState(WebSettings.PluginState.ON)
        webView.setWebChromeClient(WebChromeClient())
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, urlNewString: String?): Boolean {
                if (!loadingFinished) {
                    redirect = true
                }
                loadingFinished = false
                //webView.loadUrl(urlNewString);
                // Here the String url hold 'Clicked URL'
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlNewString)));
                if (urlNewString?.startsWith("tel:")!!) {
                    val intent = Intent(Intent.ACTION_DIAL,
                            Uri.parse(urlNewString))
                    startActivity(intent)
                } else if (urlNewString.startsWith("http:") || urlNewString.startsWith("https:")) {
                    view?.loadUrl(urlNewString)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, facIcon: Bitmap?) {
                loadingFinished = false
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                mloading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (!redirect) {
                    loadingFinished = true
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
                if (loadingFinished && !redirect) {
                    //HIDE LOADING IT HAS FINISHED
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                } else {
                    redirect = false
                }
            }
        })
    }

    private fun AddDonationForProject() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.AddDonationForProject(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                getProjectsForDonation.Id, et_due_amount.getText().toString(),
                "4", Charge_id)?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (outResponse.contains("https") || outResponse.contains("http")) {
                                val b = Bundle()
                                b.putString("id", outResponse)
                                val frag: Fragment = PayPaymentFragment.Companion.newInstance(act)!!
                                frag.arguments = b
                                act.getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                        .addToBackStack(frag.javaClass.name)
                                        .replace(R.id.content_frame, frag).commit()
                            } else {
                                if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                })
    }

    // shareType : 1-fb , 5-twitter , 6-Instgram  , 2-wtsapp  , 3-gmail  , 4sms
    @SuppressLint("DefaultLocale")
    private fun Share(shareType: Int, appName: String?, content: String?) {
        val targetedShareIntents: MutableList<Intent?> = ArrayList()
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        val resInfo: MutableList<ResolveInfo?> = act.getPackageManager().queryIntentActivities(share, 0)
        if (!resInfo.isEmpty()) {
            when (shareType) {
                1 -> {
                    Log.d("inside fbshare", "test0001")
                    // facebook_icon
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info?.activityInfo?.packageName?.toLowerCase()?.contains(appName!!)!! || info.activityInfo.name.toLowerCase().contains(appName!!)) {
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        act.startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Facebook is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                5 -> {
                    // twitter
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info?.activityInfo?.packageName?.toLowerCase()?.contains(appName!!)!! || info.activityInfo.name.toLowerCase().contains(appName!!)) {
                            Log.e("Twitter", "Twitter-Data: $content")
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)

//                            Uri uri = Uri.parse(products.getPhotos());
//                            targetedShare.putExtra(Intent.EXTRA_STREAM, String.valueOf(uri));
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Twitter is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                6 ->                     //Instagram
                    if (1 == 1) {
                        try {
                            Log.d("product_1_image", getProjectsForDonation.Photo)
                            val shareIntent = Intent(Intent.ACTION_SEND)
                            shareIntent.type = "image/*"
                            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            shareIntent.putExtra(Intent.EXTRA_STREAM, pShareUri)
                            shareIntent.putExtra(Intent.EXTRA_TEXT, content)
                            shareIntent.setPackage("com.instagram.android")
                            targetedShareIntents.add(shareIntent)
                            val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                            startActivity(chooserIntent)
                        } catch (e: Exception) {
                            Toast.makeText(act, "Instagram is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                        }
                    }
                2 -> {
                    // whatsapp
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        // put here your mime type
                        if (info!!.activityInfo.packageName.toLowerCase().contains(
                                        appName!!)
                                || info.activityInfo.name.toLowerCase().contains(
                                        appName)) {
                            Log.d("whatsapp_share", content)
                            targetedShare.putExtra(Intent.EXTRA_TEXT, """
     $content

     ${getProjectsForDonation.Photo}
     """.trimIndent())
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "WhatsApp is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                3 -> {
                    // Gmail
                    val txt = "<img src=\"" + getProjectsForDonation.Photo + "\" width=\"600px\"  height=\"600px\"/>"
                    Log.e("Email", "Email txt = $txt")
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/html"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, act.getString(R.string.app_name))
                    shareIntent.putExtra(Intent.EXTRA_TEXT, """$content
 ${Html.fromHtml(txt)}""")
                    for (app in resInfo) {
                        if (app!!.activityInfo.name.toLowerCase().contains(appName!!)) {
                            val activity: ActivityInfo = app.activityInfo
                            val name = ComponentName(activity.applicationInfo.packageName, activity.name)
                            shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                            shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                            shareIntent.component = name
                            act.startActivity(shareIntent)
                            break
                        }
                    }
                }
                4 -> {
                    //SMS
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("smsto:")
                    smsIntent.type = "vnd.android-dir/mms-sms"
                    smsIntent.putExtra(Intent.EXTRA_SUBJECT, act.getString(R.string.app_name) + "\n" + content)
                    smsIntent.putExtra("sms_body", content)
                    try {
                        startActivity(smsIntent)
                        Log.i("Finished sending SMS...", "")
                    } catch (ex: ActivityNotFoundException) {
                    }
                }
                else -> {
                }
            }
        }
    }

    internal inner class GetAndSaveBitmapForArticle : AsyncTask<String?, Void?, Uri?>() {
        protected override fun doInBackground(vararg params: String?): Uri? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            file = File(dir, UUID.randomUUID().toString() + ".png")
            val fOut: FileOutputStream
            try {
                val bm: Bitmap = Picasso.with(act).load(params[0]).get()
                fOut = FileOutputStream(file)
                bm.compress(Bitmap.CompressFormat.PNG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            pShareUri = Uri.fromFile(file)
            return pShareUri
        }
    }

    private fun shareContent() {

//        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
//               getProjectsForDonation.getCompanyCategoryNameEn()+  " @ AlQenaat " + " \n " + getProjectsForDonation.getDetailsEN() + "\n\n" + getProjectsForDonation.getPhotos() + "\n\n" + act.getString(R.string.UrlLink)
//                : getProjectsForDonation.getCompanyCategoryNameAr() +  " @ AlQenaat" + " \n " + getProjectsForDonation.getDetailsAR() + "\n\n" + getProjectsForDonation.getPhotos() + "\n\n" + act.getString(R.string.UrlLink);
        val shareProductInfo: String = if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) getProjectsForDonation.ProjectNameEN + "" + "\n\n" + getProjectsForDonation.DetailsEN + "" + "\n\n" + act.getString(R.string.UrlLink1) else getProjectsForDonation.ProjectNameAR + "" + "\n\n" + getProjectsForDonation.DetailsAr + "" + "\n\n" + act.getString(R.string.UrlLink1)


/*        String contentType="";
        if(getProjectsForDonation.getContentType().equalsIgnoreCase("1")){
            contentType = act.getString(R.string.NewsLabel);
        }
        else if(getProjectsForDonation.getContentType().equalsIgnoreCase("2")){
            if(getProjectsForDonation.getIsActivity().equalsIgnoreCase("1")){
                contentType = act.getString(R.string.SpecialOccasionLabel);
            }
            else{
                contentType = act.getString(R.string.OccasionLabel);
            }

        }
        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
               act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getProjectsForDonation.getContentLink() + "\n\n" + act.getString(R.string.UrlLink)
                : act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getProjectsForDonation.getContentLink() + "\n\n" + act.getString(R.string.UrlLink);
**/
        val ob: GetAndSaveBitmapForArticle = GetAndSaveBitmapForArticle()
        val clipboard = act.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("AlQenaat", shareProductInfo)
        clipboard.setPrimaryClip(clip)
        ob.execute(getProjectsForDonation.Photo)
        val dialog = Dialog(act)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.share_dialog)
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        val mimg_close: ImageView?
        val mimg_facebook: ImageView?
        val mimg_twitter: ImageView?
        val mimg_instagram: ImageView?
        val mimg_email: ImageView?
        val mimg_sms: ImageView?
        val mimg_whats: ImageView?
        mimg_facebook = dialog.findViewById<View?>(R.id.img_facebook) as ImageView
        val title: TextView = dialog.findViewById<View?>(R.id.shartext) as TextView
        title.setTypeface(ContentActivity.Companion.tf)
        mimg_twitter = dialog.findViewById<View?>(R.id.img_twitter) as ImageView
        mimg_instagram = dialog.findViewById<View?>(R.id.img_instagram) as ImageView
        mimg_email = dialog.findViewById<View?>(R.id.img_email) as ImageView
        mimg_sms = dialog.findViewById<View?>(R.id.img_sms) as ImageView
        mimg_whats = dialog.findViewById<View?>(R.id.img_whats) as ImageView
        mimg_close = dialog.findViewById<View?>(R.id.img_searchClose) as ImageView
        mimg_close.setOnClickListener(View.OnClickListener {
            file.delete()
            dialog.dismiss()
        })
        mimg_facebook.setOnClickListener(View.OnClickListener { /*Share(1,
                                "com.facebook_icon.katana",
                                products.getDecription() + "\n" +
                                        products.getPhotos());*/
            dialog.dismiss()
            if (mSessionManager.isFacebookOpenBefore()) {
                Share(1,
                        "com.facebook_icon.katana",
                        shareProductInfo)
            } else {
                AlertDialog.Builder(act)
                        .setTitle(act.getString(R.string.AttentionLabel))
                        .setMessage(act.getString(R.string.AttentionText))
                        .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                Share(1,
                                        "com.facebook_icon.katana",
                                        shareProductInfo)
                                mSessionManager.FacebookOpened()
                            }
                        })
                        .setIcon(R.drawable.icon_512)
                        .show()
            }
        })
        mimg_twitter.setOnClickListener(View.OnClickListener {
            file.delete()
            dialog.dismiss()
            Share(5,
                    "twitter",
                    shareProductInfo)
        })
        mimg_instagram.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            AlertDialog.Builder(act)
                    .setIcon(R.drawable.icon_512)
                    .setTitle(act.getString(R.string.AttentionLabel))
                    .setMessage(act.getString(R.string.AttentionText))
                    .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            Share(6,
                                    "instagram",
                                    shareProductInfo)
                        }
                    })
                    .setIcon(R.drawable.icon_512)
                    .show()
        })
        mimg_email.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            file.delete()
            Share(3,
                    "gmail",
                    shareProductInfo)
        })
        mimg_sms.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            file.delete()
            Share(4,
                    "sms",
                    shareProductInfo)
        })
        mimg_whats.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            file.delete()
            Share(2,
                    "whatsapp",
                    shareProductInfo)
        })
        dialog.show()
    }

    companion object {
        protected val TAG = ProjectDonationDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: ProjectDonationDetailsFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): ProjectDonationDetailsFragment {
            fragment = ProjectDonationDetailsFragment()
            Companion.act = act
            return fragment
        }
    }

    private fun startSDK() {
        /**
         * Required step.
         * Configure SDK with your Secret API key and App Bundle name registered with tap company.
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            configureApp()
        }
        /**
         * Optional step
         * Here you can configure your app theme (Look and Feel).
         */
        configureSDKThemeObject()
        /**
         * Required step.
         * Configure SDK Session with all required data.
         */
        configureSDKSession()
        /**
         * Required step.
         * Choose between different SDK modes
         */
        configureSDKMode()
        /**
         * If you included Tap Pay Button then configure it first, if not then ignore this step.
         */
        initPayButton()
    }

    /**
     * Required step.
     * Configure SDK with your Secret API key and App Bundle name registered with tap company.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun configureApp() {
        //GoSellSDK.init(act, "sk_test_kovrMB0mupFJXfNZWx6Etg5y",
        //GoSellSDK.init(act, "sk_test_DMUdB4qz1XeoKfZn2SYwsP95",
        GoSellSDK.init(act, KEY_PAYMENT,
                "com.qenaat.app") // to be replaced by merchant
        GoSellSDK.setLocale(languageSeassionManager.getLang()) //  language to be set by merchant
    }

    /**
     * Configure SDK Theme
     */
    private fun configureSDKThemeObject() {
        ThemeObject.getInstance()
                .setAppearanceMode(AppearanceMode.WINDOWED_MODE)
                .setSdkLanguage(languageSeassionManager.getLang())
                .setHeaderTextColor(resources.getColor(R.color.black1))
                .setHeaderTextSize(17)
                .setHeaderBackgroundColor(resources.getColor(R.color.french_gray_new))
                .setCardInputTextColor(resources.getColor(R.color.black))
                .setCardInputInvalidTextColor(resources.getColor(R.color.red))
                .setCardInputPlaceholderTextColor(resources.getColor(R.color.gray))
                .setSaveCardSwitchOffThumbTint(resources.getColor(R.color.french_gray_new))
                .setSaveCardSwitchOnThumbTint(resources.getColor(R.color.color_button))
                .setSaveCardSwitchOffTrackTint(resources.getColor(R.color.french_gray))
                .setSaveCardSwitchOnTrackTint(resources.getColor(R.color.color_button))
                .setScanIconDrawable(resources.getDrawable(R.drawable.btn_card_scanner_normal))
                .setPayButtonResourceId(R.drawable.btn_pay_selector) //btn_pay_merchant_selector
                .setPayButtonDisabledTitleColor(resources.getColor(R.color.white))
                .setPayButtonEnabledTitleColor(resources.getColor(R.color.color_text_light))
                .setPayButtonTextSize(16)
                .setPayButtonLoaderVisible(true)
                .setPayButtonSecurityIconVisible(true) //.setPayButtonText("PAY BTN CAN BE VERY VERY VERY  LONGGGG LONGGGGG") // **Optional**
                // setup dialog textcolor and textsize
                .setDialogTextColor(resources.getColor(R.color.black1)).dialogTextSize = 17 // **Optional**
    }


    /**
     * Configure SDK Session
     */
    private fun configureSDKSession() {

        // Instantiate SDK Session
        sdkSession = SDKSession(this) //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession.setTransactionCurrency(TapCurrency("KWD")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession.setAmount(BigDecimal(0)) //** Required **

        // Set Payment Items array list
        sdkSession.setPaymentItems(ArrayList<PaymentItem>()) // ** Optional ** you can pass empty array list


//       sdkSession.setPaymentType("CARD");   //** Merchant can pass paymentType

        // Set Taxes array list
        sdkSession.setTaxes(ArrayList<Tax>()) // ** Optional ** you can pass empty array list

        // Set Shipping array list
        sdkSession.setShipping(ArrayList<Shipping>()) // ** Optional ** you can pass empty array list

        // Post URL
        sdkSession.setPostURL("") // ** Optional **

        // Payment Description
        sdkSession.setPaymentDescription("") //** Optional **

        // Payment Extra Info
        sdkSession.setPaymentMetadata(HashMap<String, String>()) // ** Optional ** you can pass empty array hash map

        // Payment Reference
        sdkSession.setPaymentReference(null) // ** Optional ** you can pass null

        // Payment Statement Descriptor
        sdkSession.setPaymentStatementDescriptor("") // ** Optional **

        // Enable or Disable Saving Card
        sdkSession.isUserAllowedToSaveCard(true) //  ** Required ** you can pass boolean

        // Enable or Disable 3DSecure
        sdkSession.isRequires3DSecure(true)

        //Set Receipt Settings [SMS - Email ]
        sdkSession.setReceiptSettings(Receipt(false, false)) // ** Optional ** you can pass Receipt object or null

        // Set Authorize Action
        sdkSession.setAuthorizeAction(null) // ** Optional ** you can pass AuthorizeAction object or null
        sdkSession.setDestination(null) // ** Optional ** you can pass Destinations object or null
        sdkSession.setMerchantID(null) // ** Optional ** you can pass merchant id or null
        sdkSession.setCardType(CardType.CREDIT) // ** Optional ** you can pass which cardType[CREDIT/DEBIT] you want.By default it loads all available cards for Merchant.

        // sdkSession.setDefaultCardHolderName("TEST TAP"); // ** Optional ** you can pass default CardHolderName of the user .So you don't need to type it.
        // sdkSession.isUserAllowedToEnableCardHolderName(false); // ** Optional ** you can enable/ disable  default CardHolderName .
    }


    /**
     * Configure SDK Theme
     */
    private fun configureSDKMode() {
        /**
         * You have to choose only one Mode of the following modes:
         * Note:-
         * - In case of using PayButton, then don't call sdkSession.start(this); because the SDK will start when user clicks the tap pay button.
         */
        //////////////////////////////////////////////////////    SDK with UI //////////////////////
        /**
         * 1- Start using  SDK features through SDK main activity (With Tap CARD FORM)
         */
        startSDKWithUI()
    }


    /**
     * Start using  SDK features through SDK main activity
     */
    private fun startSDKWithUI() {
        if (sdkSession != null) {
            val trx_mode = if (settingsManager != null) settingsManager?.getTransactionsMode("key_sdk_transaction_mode") else TransactionMode.PURCHASE
            // set transaction mode [TransactionMode.PURCHASE - TransactionMode.AUTHORIZE_CAPTURE - TransactionMode.SAVE_CARD - TransactionMode.TOKENIZE_CARD ]
            sdkSession.setTransactionMode(trx_mode) //** Required **
            // if you are not using tap button then start SDK using the following call
            //sdkSession.start(this);
        }
    }

    /**
     * Include pay button in merchant page
     */
    private fun initPayButton() {
        if (ThemeObject.getInstance().payButtonFont != null) payButtonView.setupFontTypeFace(ThemeObject.getInstance().payButtonFont)
        if (ThemeObject.getInstance().payButtonDisabledTitleColor != 0 && ThemeObject.getInstance().payButtonEnabledTitleColor != 0) payButtonView.setupTextColor(ThemeObject.getInstance().payButtonEnabledTitleColor,
                ThemeObject.getInstance().payButtonDisabledTitleColor)
        if (ThemeObject.getInstance().payButtonTextSize != 0) payButtonView.getPayButton().setTextSize(ThemeObject.getInstance().payButtonTextSize.toFloat())
        //
        if (ThemeObject.getInstance().isPayButtSecurityIconVisible) payButtonView.getSecurityIconView().setVisibility(if (ThemeObject.getInstance().isPayButtSecurityIconVisible) View.VISIBLE else View.INVISIBLE)
        if (ThemeObject.getInstance().payButtonResourceId != 0) payButtonView.setBackgroundSelector(ThemeObject.getInstance().payButtonResourceId)
        if (sdkSession != null) {
            val trx_mode: TransactionMode = sdkSession.getTransactionMode()
            if (trx_mode != null) {
                if (TransactionMode.SAVE_CARD == trx_mode || TransactionMode.SAVE_CARD_NO_UI == trx_mode) {
                    payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.save_card))
                } else if (TransactionMode.TOKENIZE_CARD == trx_mode || TransactionMode.TOKENIZE_CARD_NO_UI == trx_mode) {
                    payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.tokenize))
                } else {
                    payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.pay))
                }
            } else {
                configureSDKMode()
            }
            sdkSession.setButtonView(payButtonView, act, SDK_REQUEST_CODE)
        }
    }

    private fun getCustomer(): Customer { // test customer id cus_Kh1b4220191939i1KP2506448
        val customer = if ((settingsManager != null)) settingsManager?.getCustomer() else null
        val phoneNumber = if (customer != null) customer.getPhone() else PhoneNumber("965", "69045932")
        return Customer.CustomerBuilder(null).email("abc@abc.com").firstName("firstname")
                .lastName("lastname").metadata("").phone(PhoneNumber(phoneNumber?.getCountryCode(), phoneNumber?.getNumber()))
                .middleName("middlename").build()
    }

    override fun sessionCancelled() {
        Log.e("Tap_Payment", " Session Cancelled.......")
    }

    override fun savedCardsList(cardsList: CardsList) {

    }

    override fun sessionIsStarting() {
        Log.e("Tap_Payment", " Session Is Starting.....")
    }

    override fun invalidCardDetails() {
        Log.e("Tap_Payment", " Card details are invalid....")
    }

    override fun cardSavingFailed(charge: Charge) {

    }

    override fun backendUnknownError(message: String?) {
        Log.e("Tap_Payment", "Backend Un-Known error.... : $message")
    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {

    }

    override fun cardSaved(charge: Charge) {

    }

    override fun paymentSucceed(charge: Charge) {
        mainLayout.showSnakeBar(getString(R.string.payment_succeeded))
        Charge_id = charge.id

        AddDonationForProject()

        Log.e("Tap_Payment", "Payment Succeeded : charge id : " + charge.id)
        Log.e("Tap_Payment", "Payment Succeeded : charge status : " + charge.status)
        Log.e("Tap_Payment", "Payment Succeeded : description : " + charge.description)
        Log.e("Tap_Payment", "Payment Succeeded : message : " + charge.response.message)
        Log.e("Tap_Payment", "##############################################################################")
        if (charge.card != null) {
            Log.e("Tap_Payment", "Payment Succeeded : first six : " + charge.card!!.firstSix)
            Log.e("Tap_Payment", "Payment Succeeded : last four: " + charge.card!!.last4)
            Log.e("Tap_Payment", "Payment Succeeded : card object : " + charge.card!!.getObject())
            Log.e("Tap_Payment", "Payment Succeeded : brand : " + charge.card!!.brand)
            Log.e("Tap_Payment", """
    Payment Succeeded : expiry : ${charge.card!!.expiry!!.month}
    ${charge.card!!.expiry!!.year}
    """.trimIndent())
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (charge.acquirer != null) {
            Log.e("Tap_Payment", "Payment Succeeded : acquirer id : " + charge.acquirer!!.id)
            Log.e("Tap_Payment", "Payment Succeeded : acquirer response code : " + charge.acquirer!!.response.code)
            Log.e("Tap_Payment", "Payment Succeeded : acquirer response message: " + charge.acquirer!!.response.message)
        }
        Log.e("Tap_Payment", "##############################################################################")
        if (charge.source != null) {
            Log.e("Tap_Payment", "Payment Succeeded : source id: " + charge.source.id)
            Log.e("Tap_Payment", "Payment Succeeded : source channel: " + charge.source.channel)
            Log.e("Tap_Payment", "Payment Succeeded : source object: " + charge.source.getObject())
            Log.e("Tap_Payment", "Payment Succeeded : source payment method: " + charge.source.paymentMethodStringValue)
            Log.e("Tap_Payment", "Payment Succeeded : source payment type: " + charge.source.paymentType)
            Log.e("Tap_Payment", "Payment Succeeded : source type: " + charge.source.type)
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (charge.expiry != null) {
            Log.e("Tap_Payment", "Payment Succeeded : expiry type :" + charge.expiry!!.type)
            Log.e("Tap_Payment", "Payment Succeeded : expiry period :" + charge.expiry!!.period)
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (charge.transaction != null) {
            Log.e("Tap_Payment", "Payment Succeeded : authorizationID :" + charge.transaction.authorizationID)
            Log.e("Tap_Payment", "Payment Succeeded : transactionUrl :" + charge.transaction.url)
        }

        //et_amount.setText("")
        configureSDKSession()
    }

    override fun authorizationFailed(authorize: Authorize?) {
        Log.e("Tap_Payment", "Authorize Failed : " + authorize!!.status)
        Log.e("Tap_Payment", "Authorize Failed : " + authorize.description)
        Log.e("Tap_Payment", "Authorize Failed : " + authorize.response.message)
    }

    override fun cardTokenizedSuccessfully(token: Token) {
        Log.e("Tap_Payment", "Card Tokenized Succeeded : ")
        Log.e("Tap_Payment", "Token card : " + token.card.firstSix + " **** " + token.card.lastFour)
        Log.e("Tap_Payment", "Token card : " + token.card.fingerprint + " **** " + token.card.funding)
        Log.e("Tap_Payment", "Token card : " + token.card.id + " ****** " + token.card.name)
        Log.e("Tap_Payment", "Token card : " + token.card.address + " ****** " + token.card.getObject())
        Log.e("Tap_Payment", "Token card : " + token.card.expirationMonth + " ****** " + token.card.expirationYear)
    }

    override fun authorizationSucceed(authorize: Authorize) {
        Log.e("Tap_Payment", "Authorize Succeeded : $authorize")
        Log.e("Tap_Payment", "Authorize Succeeded : " + authorize.status)
        Log.e("Tap_Payment", "Authorize Succeeded : " + authorize.response.message)

        if (authorize.card != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : first six : " + authorize.card!!.firstSix)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : last four: " + authorize.card!!.last4)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : card object : " + authorize.card!!.getObject())
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (authorize.acquirer != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : acquirer id : " + authorize.acquirer!!.id)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : acquirer response code : " + authorize.acquirer!!.response.code)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : acquirer response message: " + authorize.acquirer!!.response.message)
        }
        Log.e("Tap_Payment", "##############################################################################")
        if (authorize.source != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source id: " + authorize.source.id)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source channel: " + authorize.source.channel)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source object: " + authorize.source.getObject())
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source payment method: " + authorize.source.paymentMethodStringValue)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source payment type: " + authorize.source.paymentType)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source type: " + authorize.source.type)
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (authorize.expiry != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : expiry type :" + authorize.expiry!!.type)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : expiry period :" + authorize.expiry!!.period)
        }

        //et_amount.setText("")
        configureSDKSession()
    }

    override fun invalidTransactionMode() {
        Log.e("Tap_Payment", " invalidTransactionMode  ......")
    }

    override fun sdkError(goSellError: GoSellError?) {
        if (goSellError != null) {
            Log.e("Tap_Payment", "SDK Process Error : " + goSellError.errorBody)
            Log.e("Tap_Payment", "SDK Process Error : " + goSellError.errorMessage)
            Log.e("Tap_Payment", "SDK Process Error : " + goSellError.errorCode)
        }
    }

    override fun sessionFailedToStart() {
        Log.e("Tap_Payment", "Session Failed to start.........")
    }

    override fun paymentFailed(charge: Charge?) {
        mainLayout.showSnakeBar(getString(R.string.payment_failed))
        Log.e("Tap_Payment", "Payment Failed : " + charge!!.status)
        Log.e("Tap_Payment", "Payment Failed : " + charge.description)
        Log.e("Tap_Payment", "Payment Failed : " + charge.response.code)
        Log.e("Tap_Payment", "Payment Failed : " + charge.response.message)
    }

    override fun sessionHasStarted() {
        Log.e("Tap_Payment", " Session Has Started .......")
    }

    override fun invalidCustomerID() {
        Log.e("Tap_Payment", "Invalid Customer ID .......")
    }

    override fun onPayment() {
        isValid()
    }

    private fun isValid(): Boolean {
        when {
            et_due_amount.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_amount))
                sdkSession.isValid = false
                return false
            }
            et_due_amount.getTextValue().trim().toInt() <= 0  ->{
                mainLayout.showSnakeBar(getString(R.string.amount_grather_then_0))
                sdkSession.isValid = false
                et_due_amount.requestFocus()
                return false
            }

        }
        sdkSession.isValid = true
        return true
    }
}