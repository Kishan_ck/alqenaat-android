package com.qenaat.app.adapters

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener
import com.nostra13.universalimageloader.core.assist.ImageScaleType
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer
import com.polites.android.GestureImageView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R

/**
 * Created by DELL on 11/08/2016.
 */
class ItemGalleryPagerAdapter(a: Activity, d: Array<String>) : PagerAdapter() {
    private val activity: Activity
    private val mGetProductGalleryClass: Array<String>
    var options: DisplayImageOptions

    override fun getCount(): Int {
        return mGetProductGalleryClass.size
    }

    override fun destroyItem(container: View, position: Int, `object`: Any) {
        (container as ViewPager?)?.removeView(`object` as View?)
    }

    override fun finishUpdate(container: View) {}

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        var imageLayout: View? = null
        if (imageLayout == null) {
            imageLayout = inflater?.inflate(R.layout.gallery_item, view, false)
        }
        val imageView: GestureImageView? = imageLayout
                ?.findViewById<View?>(R.id.image) as GestureImageView?
        val spinner: ProgressBar? = imageLayout
                ?.findViewById<View?>(R.id.loading) as ProgressBar?
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        ContentActivity.Companion.mImageLoader?.displayImage(mGetProductGalleryClass.get(position)
                , imageView, options, object : ImageLoadingListener {
            override fun onLoadingStarted(imageUri: String?, view: View?) {
                spinner?.setVisibility(View.VISIBLE)
            }

            override fun onLoadingFailed(imageUri: String?, view: View?,
                                         failReason: FailReason?) {
                spinner?.setVisibility(View.GONE)
            }

            override fun onLoadingComplete(arg0: String?, view: View?, arg2: Bitmap?) {
                spinner?.setVisibility(View.GONE)
            }

            override fun onLoadingCancelled(arg0: String?, arg1: View?) {
                spinner?.setVisibility(View.GONE)
            }
        })
        (view as ViewPager?)?.addView(imageLayout, 0)
        return imageLayout!!
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}
    override fun saveState(): Parcelable? {
        return null
    }

    override fun startUpdate(container: View) {}

    companion object {
        private var inflater: LayoutInflater? = null
    }

    init {
        activity = a
        mGetProductGalleryClass = d
        inflater = activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        options = DisplayImageOptions.Builder().cacheOnDisc(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565).cacheInMemory(true)
                .cacheOnDisc(true).displayer(FadeInBitmapDisplayer(300))
                .build()
    }
}