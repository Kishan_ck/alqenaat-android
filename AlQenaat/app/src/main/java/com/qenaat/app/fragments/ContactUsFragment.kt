package com.qenaat.app.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl
import com.qenaat.app.classes.FixControl.checkEmailPattern
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetAppSettings
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader

/**
 * Created by DELL on 29-Oct-17.
 */
class ContactUsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: LinearLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSessionManager: LanguageSessionManager
    lateinit var mimg_twitter: ImageView
    lateinit var mimg_instagram: ImageView
    lateinit var mimg_facebook: ImageView
    lateinit var img_youtube: ImageView
    lateinit var img_snapchat: ImageView
    lateinit var tv_send: TextView
    lateinit var tv_call: TextView
    private lateinit var mloading: ProgressBar
    lateinit var ed_name: EditText
    lateinit var ed_email: EditText
    lateinit var ed_message: EditText
    lateinit var linear_call: LinearLayout
    lateinit var scroll_view: ScrollView
    var permissionStr = ""
    private var getAppSettings: GetAppSettings? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSessionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.contact_us, null) as LinearLayout
            act.softWindow()
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: LinearLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            linear_call = mainLayout.findViewById<View?>(R.id.linear_call) as LinearLayout
            tv_call = mainLayout.findViewById<View?>(R.id.tv_call) as TextView
            tv_send = mainLayout.findViewById<View?>(R.id.tv_send) as TextView
            tv_send.setOnClickListener(this)
            scroll_view = mainLayout.findViewById<View?>(R.id.scroll_view) as ScrollView
            mimg_twitter = mainLayout.findViewById<View?>(R.id.img_twitter) as ImageView
            mimg_twitter.setOnClickListener(this)
            mimg_instagram = mainLayout.findViewById<View?>(R.id.img_instagram) as ImageView
            mimg_instagram.setOnClickListener(this)
            mimg_facebook = mainLayout.findViewById<View?>(R.id.img_facebook) as ImageView
            mimg_facebook.setOnClickListener(this)
            img_youtube = mainLayout.findViewById<View?>(R.id.img_youtube) as ImageView
            img_youtube.setOnClickListener(this)
            img_snapchat = mainLayout.findViewById<View?>(R.id.img_snapchat) as ImageView
            img_snapchat.setOnClickListener(this)
            ed_name = mainLayout.findViewById<View?>(R.id.ed_name) as EditText
            ed_email = mainLayout.findViewById<View?>(R.id.ed_email) as EditText
            ed_message = mainLayout.findViewById<View?>(R.id.ed_message) as EditText
            ed_name.setTypeface(ContentActivity.Companion.tf)
            ed_email.setTypeface(ContentActivity.Companion.tf)
            ed_message.setTypeface(ContentActivity.Companion.tf)
            tv_send.setTypeface(ContentActivity.Companion.tf)
            ed_message.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.text_area) as BitmapDrawable).bitmap.width
            ed_message.getLayoutParams().height = (act.getResources().getDrawable(
                    R.drawable.text_area) as BitmapDrawable).bitmap.height
            //ContentActivity.setTextFonts(mainLayout);
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.ContactUsLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSessionManager)
        ed_message.setMovementMethod(ScrollingMovementMethod())
        scroll_view.setOnTouchListener(OnTouchListener { v, event ->
            ed_message.getParent().requestDisallowInterceptTouchEvent(false)
            false
        })
        ed_message.setOnTouchListener(OnTouchListener { v, event ->
            ed_message.getParent().requestDisallowInterceptTouchEvent(true)
            false
        })
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.settings()?.enqueue(object : Callback<GetAppSettings?> {
            override fun onFailure(call: Call<GetAppSettings?>, t: Throwable) {
                t.printStackTrace()
                mloading.setVisibility(View.INVISIBLE)
                GlobalFunctions.EnableLayout(mainLayout)
            }

            override fun onResponse(call: Call<GetAppSettings?>, response: Response<GetAppSettings?>) {
                if (response.body() != null) {
                    val settings = response.body()
                    if (settings != null) {
                        if (settings != null) {
                            getAppSettings = settings
                            languageSessionManager.SetFaceBook(settings.Facebook)
                            languageSessionManager.SetTwitter(settings.Twitter)
                            languageSessionManager.SetYoutube(settings.Youtube)
                            languageSessionManager.setInstagram(settings.Instagram)
                            languageSessionManager.Setkey_SnapChat(settings.SnapChat)
                            tv_call.setText("+965 " + settings.Phone)
                            linear_call.setOnClickListener(View.OnClickListener {
                                /* if (GlobalFunctions.isReadCallAllowed(act)) {
                                 val intent = Intent(Intent.ACTION_CALL)
                                 intent.data = Uri.parse("tel:" + settings.Phone)
                                 startActivity(intent)
                                 return@OnClickListener
                             }*/
                                if (settings.Phone!!.isNotEmpty()) {
                                    permissionStr = Manifest.permission.CALL_PHONE
                                    GlobalFunctions.requestCallPermission(act)
                                } else
                                    FixControl.showToast(act, getString(R.string.no_number_found))
                            })
                        }
                    }
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.img_snapchat -> if (languageSessionManager.Getkey_SnapChat()?.length!! > 0) {
                if (languageSessionManager.Getkey_SnapChat()!!.contains("http") || languageSessionManager.Getkey_SnapChat()!!.contains("https"))
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(languageSessionManager.Getkey_SnapChat())))
            } else {
                Toast.makeText(act, getString(R.string.OperationFailed), Toast.LENGTH_SHORT).show()
            }
            R.id.img_youtube -> if (languageSessionManager.GetYoutube()!!.length > 0) {
                if (languageSessionManager.GetYoutube()!!.contains("http") || languageSessionManager.GetYoutube()!!.contains("https")) startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(languageSessionManager.GetYoutube())))
            } else {
                Toast.makeText(act, getString(R.string.OperationFailed), Toast.LENGTH_SHORT).show()
            }
            R.id.img_twitter -> if (languageSessionManager.GetTwitter()!!.length > 0) {
                if (languageSessionManager.GetTwitter()!!.contains("http") || languageSessionManager.GetTwitter()!!.contains("https")) startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(languageSessionManager.GetTwitter())))
            } else {
                Toast.makeText(act, getString(R.string.OperationFailed), Toast.LENGTH_SHORT).show()
            }
            R.id.img_instagram -> if (languageSessionManager.getInstagram()!!.length > 0) {
                if (languageSessionManager.getInstagram()!!.contains("http") || languageSessionManager.getInstagram()!!.contains("https")) startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(languageSessionManager.getInstagram())))
            } else {
                Toast.makeText(act, getString(R.string.OperationFailed), Toast.LENGTH_SHORT).show()
            }
            R.id.img_facebook -> if (languageSessionManager.GetFaceBook()!!.length > 0) {
                if (languageSessionManager.GetFaceBook()!!.contains("http") || languageSessionManager.GetFaceBook()!!.contains("https")) startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(languageSessionManager.GetFaceBook())))
            } else {
                Toast.makeText(act, getString(R.string.OperationFailed), Toast.LENGTH_SHORT).show()
            }
            R.id.tv_send -> if (isValid()) {
                //if (FixControl.isValidEmaillId(ed_email.getText().toString())) {
                mloading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
                QenaatAPICall.getCallingAPIInterface()?.contactUs(
                        ed_email.getText().toString(),
                        ed_message.getText().toString())?.enqueue(
                        object : Callback<ResponseBody?> {
                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                Log.e("ContactUs", t.message)
                                t.printStackTrace()
                                mloading.setVisibility(View.INVISIBLE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ResponseBody?>,
                                                    response: Response<ResponseBody?>) {
                                val body = response.body()
                                Log.e("ContactUs", body.toString())
                                var outResponse = ""
                                try {
                                    val reader = BufferedReader(InputStreamReader
                                    (ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                if (outResponse != null) {
                                    outResponse = outResponse.replace("\"", "")
                                    outResponse = outResponse.replace("\n", "")
                                    Log.e("outResponse not null ", outResponse)
                                    if (outResponse.toInt() > 0) {
                                        Snackbar.make(mainLayout, NewsFragment.act?.getString(R.string.MessageSentLabel)!!, Snackbar.LENGTH_LONG).show()
                                        ed_email.setText("")
                                        ed_message.setText("")
                                    } else if (outResponse == "-1") {
                                        Snackbar.make(mainLayout, NewsFragment.act?.getString(R.string.OperationFailed)!!, Snackbar.LENGTH_LONG).show()
                                    }
                                }
                                mloading.setVisibility(View.INVISIBLE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }
                        })
            }
            /*else {
                Snackbar.make(mainLayout, act.getString(R.string.EmailIsWrongLabel), Snackbar.LENGTH_LONG).show()
            }*/
            /*} else {
                Snackbar.make(mainLayout, act.getString(R.string.FillAllFields), Snackbar.LENGTH_LONG).show()
            }*/
        }
    }

    private fun isValid(): Boolean {
        when {
            ed_email.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_email))
                ed_email.requestFocus()
                return false
            }
            !ed_email.checkEmailPattern() -> {
                mainLayout.showSnakeBar(getString(R.string.EmailIsWrongLabel))
                ed_email.requestFocus()
                return false
            }
            ed_message.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_message))
                ed_message.requestFocus()
                return false
            }
        }
        return true
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.CALL_PERMISSION_CODE -> {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:" + getAppSettings?.Phone)
                    startActivity(intent)
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act)
        }
    }

    companion object {
        protected val TAG = ContactUsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: ContactUsFragment
        private const val PERMISSION_CODE = 23
        fun newInstance(act: FragmentActivity): ContactUsFragment? {
            fragment = ContactUsFragment()
            Companion.act = act
            return fragment
        }
    }
}