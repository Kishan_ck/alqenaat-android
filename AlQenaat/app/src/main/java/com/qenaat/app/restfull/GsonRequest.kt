//package com.qenaat.app.restfull
//
//import android.text.TextUtils
//import com.android.volley.*
//import com.android.volley.toolbox.HttpHeaderParser
//import com.google.gson.Gson
//import com.google.gson.JsonSyntaxException
//import java.io.UnsupportedEncodingException
//import java.nio.charset.Charset
//import kotlin.jvm.Throws
//
//class GsonRequest<T>(url: String?, private val clazz: Class<T?>?,
//                     private val headers: MutableMap<String?, String?>?,
//                     private val params_: MutableMap<String?, String?>?,
//                     private val listener: Response.Listener<T?>?,
//                     errorListener: Response.ErrorListener?) : Request<T?>(Method.GET, url, errorListener) {
//
//    private val gson: Gson? = Gson()
//    private var mPriority: Priority? = null
//    private var jsonBody: String? = null
//    override fun getPriority(): Priority? {
//        return mPriority
//    }
//
//    @Throws(AuthFailureError::class)
//    override fun getBody(): ByteArray? {
//        return if (TextUtils.isEmpty(jsonBody)) " ".toByteArray() else jsonBody?.toByteArray()
//    }
//
//    fun setPriority(mPriority: Priority?) {
//        this.mPriority = mPriority
//    }
//
//    @Throws(AuthFailureError::class)
//    override fun getHeaders(): MutableMap<String?, String?>? {
//        return headers ?: super.getHeaders()
//    }
//
//    override fun deliverResponse(response: T?) {
//        listener?.onResponse(response)
//    }
//
//    fun setJsonBody(jsonBody: String?) {
//        this.jsonBody = jsonBody
//    }
//
//    override fun parseNetworkResponse(response: NetworkResponse?): Response<T?>? {
//        return try {
//            val json = String(response?.data!!,
//                    Charset.forName(HttpHeaderParser.parseCharset(response.headers)))
//            setJsonBody(json)
//            Response.success(gson?.fromJson(json, clazz),
//                    HttpHeaderParser.parseCacheHeaders(response))
//        } catch (e: UnsupportedEncodingException) {
//            Response.error(ParseError(e))
//        } catch (e: JsonSyntaxException) {
//            Response.error(ParseError(e))
//        }
//    }
//
//}