package com.qenaat.app.networking

import com.google.gson.JsonObject
import com.qenaat.app.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.http.*
import java.util.*

/**
 * Created by shahbazshaikh on 14/07/16.
 */
interface QenaatAPIInterface {

    @GET("PageContent/GetDonnationText")
    fun GetDonnationText(): Call<GetAboutUs?>?

    @GET("AppSetting/GetAppSettings")
    fun settings(): Call<GetAppSettings?>?

    @FormUrlEncoded
    @POST("ContactUs/SendEmail")
    fun contactUs(@Field("Email") Email: String?,
                  @Field("Message") Message: String?): Call<ResponseBody?>?

    @POST("ReSendSMS")
    fun resendSMS(@Query("UserId") userId: String?): Call<ResponseBody?>?

    @POST("Users/Activate")
    fun activate(@Query("UserName") UserName: String?,
                 @Query("ActiveCode") code: String?): Call<ResponseBody?>?

    @GET("PageContent/GetAboutUsContents")
    fun GetAboutUs(): Call<GetAboutUs?>?

    @GET("News/GetNews")
    fun GetNews(@Query("newsId") newsId: String?,
                @Query("ForHomeSlider") ForHomeSlider: String?,
                @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetNews>?>?

    @GET("Donation/GetProjectList")
    fun GetProjectsForDonation(): Call<ArrayList<GetProjects>?>?

    @GET("Donation/GetProject")
    fun GetProjectDetail(@Query("projectId") projectId: String?): Call<GetProjectDetail?>?

    @GET("Donation/GetRequests")
    fun GetMyRequestedHelps(@Query("UserId") UserId: String?): Call<ArrayList<GetRequests?>?>?

    @GET("Donation/GetRequestHelpForm")
    fun GetRequestHelpForm(@Query("UserId") UserId: String?): Call<ArrayList<RequestForHelp?>?>?

    @GET("RequestForHelp/GetRequests")
    fun GetRequests(@Header("Authorization") auth: String
            /* @Query("UserId") UserId: String?,*/): Call<ArrayList<RequestForHelp>?>?

    @GET("Event/GetEvent")
    fun GetEvent(@Query("eventId") eventId: String?,
                 @Query("UserId") UserId: String?): Call<GetEvents?>?

    @GET("Donation/GetRequestHelpMessages")
    fun GetRequestHelpMessages(@Query("UserId") UserId: String?,
                               @Query("RequestHelpId") RequestHelpId: String?): Call<ArrayList<GetRequestHelpMessages?>?>?

    @GET("Donation/ProjectsForDonationById")
    fun ProjectsForDonationById(@Query("ProjectDonationId") ProjectDonationId: String): Call<ArrayList<GetProjects?>?>?

    @GET("Hall/GetHall")
    fun GetHallById(@Query("hallId") hallId: String): Call<GetHalls?>?

    @GET("Dewania/GetDewania")
    fun GetDewaniaById(@Query("dewaniaId") dewaniaId: String): Call<GetDewanias?>?

    @GET("Event/GetEvents")
    fun GetEvents(@Query("eventId") eventId: String?,
                  @Query("fordead") fordead: String?,
                  @Query("ForHomeSlider") ForHomeSlider: String?,
                  @Query("pageIndex") pageIndex: String?,
                  @Query("UserId") UserId: String?,
                  @Query("loginUserId") loginUserId: String?,
                  @Query("categoryId") categoryId: String): Call<ArrayList<GetEvents>?>?

    @GET("Dewania/GetDewanias")
    fun GetDewanias(@Query("dewaniaId") dewaniaId: String?,
                    @Query("areaId") areaId: String?,
                    @Query("weekDays") weekDays: String?,
                    @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetDewanias?>?>?

    @GET("Dewania/GetAreas")
    fun GetAreas(@Query("cityId") cityId: String?): Call<ArrayList<GetAreas>?>?

    @GET("FamilyTree/GetBranches")
    fun GetBranches(): Call<ArrayList<GetBranches>?>?

    @GET("Service/GetServices")
    fun GetServices(@Header("Authorization") auth: String,
                    @Query("userId") userId: String?,
                    @Query("ServiceId") ServiceId: String?,
                    @Query("ServiceTypeId") ServiceTypeId: String?,
                    @Query("pageIndex") pageIndex: String?,
                    @Query("type") Type: String?): Call<ArrayList<GetServices>?>?

    @GET("Service/GetServices")
    fun GetServicesAuth(@Header("Authorization") auth: String,
                        @Query("userId") userId: String?,
                        @Query("ServiceId") ServiceId: String?,
                        @Query("ServiceTypeId") ServiceTypeId: String?,
                        @Query("pageIndex") pageIndex: String?,
                        @Query("type") Type: String?): Call<ArrayList<GetServices>?>?

    @POST("InvitationList/SearchForPersonToAddToList")
    fun SearchForPersonToAddToList(@Header("Content-Type") content_type: String?,
                                   @Body SearchForPersonToAddToList: SearchForPersonToAddToList?,
                                   @Query("pageIndex") pageIndex: String?):
            Call<ArrayList<GetFamilyTree?>?>?

    @GET("InvitationList/GetInvitationListPersons")
    fun GetInvitationListPersons(@Query("invitationListId") invitationListId: String?,
                                 @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetFamilyTree?>?>?

    @POST("InvitationList/AppendPersonsToInvitationList")
    fun AppendPersonsToInvitationList(@Header("Content-Type") content_type: String?,
                                      @Body AppendPersonsToInvitationList: AppendPersonsToInvitationList?)
            : Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("InvitationList/DeletePersonsToInvitationList")
    fun RemovePersonsToInvitationList(@Field("InvitationListId") invitationListId: String?,
                                      @Field("PersonId") PersonId: String?): Call<ResponseBody?>?

    @GET("City/GetCities")
    fun GetCity(): Call<ArrayList<GetCities>?>?

    @GET("FamilyTree/GetFamilyNames")
    fun GetFamilyNames(): Call<ArrayList<GetFamilyNames>?>?

    @GET("Service/GetArea")
    fun GetArea(@Query("cityId") cityId: String?): Call<ArrayList<Area?>?>?

    @GET("Service/GetServicesRequestORecived")
    fun GetServicesRequestORecived(@Query("userId") userId: String?,
                                   @Query("type") type: String?,
                                   @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetServices>?>?

    @GET("Hall/GetHalls")
    fun GetHalls(@Query("hallId") hallId: String?,
                 @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetHalls>>?

    @GET("Hall/GetBookedHalls")
    fun GetBookedHalls(@Header("Authorization") auth: String,
                       @Query("hallId") hallId: String?,
                       @Query("userId") userId: String?,
                       @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetHalls>>

    @GET("Hall/GetHallCalender")
    fun GetHallCalender(@Header("Authorization") auth: String,
                        @Query("hallId") hallId: String?,
                        @Query("toDayDate") toDayDate: String?): Call<ArrayList<GetHallCalender?>?>?

    @GET("Donation/GetMonthlyDonationSubscription")
    fun GetMonthlyDonations(@Header("Authorization") auth: String,
                            @Query("Id") Id: String?,
                            @Query("UserId") UserId: String?,
                            @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetMonthlyDonations>?>?

    @GET("Company/GetCompanies")
    fun GetCompanies(@Header("Authorization") auth: String,
                     @Query("CompanyId") CompanyId: String?,
                     @Query("UserId") UserId: String?,
                     @Query("CompanyCategoryId") CompanyCategoryId: String?,
                     @Query("pageIndex") pageIndex: String?,
                     @Query("ForFamily") ForFamily: String?): Call<ArrayList<GetCompanies?>?>?

    @GET("Donation/GetMonthlyDonationSubscriptionInvoices")
    fun GetMonthlyDonationsInvoices(@Query("pageIndex") pageIndex: String?,
                                    @Query("MonthlyDonationSubscriptionId") MonthlyDonationSubscriptionId: String?)
            : Call<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>?

    @GET("Company/GetCompanyCategories")
    fun GetCompanyCategories(): Call<ArrayList<GetCompanyCategories?>?>?

    @POST("Service/RequestService")
    fun RequestService(@Query("ServiceId") ServiceId: String?,
                       @Query("UserId") UserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Service/SendService")
    fun SendService(@Header("Authorization") auth: String,
                    @Field("ServiceTypeId") ServiceTypeId: String?,
                    @Field("NameAr") NameAr: String?,
                    @Field("NameEn") NameEn: String?,
                    @Field("DescribtionAr") DescribtionAr: String?,
                    @Field("DescribtionEn") DescribtionEn: String?,
                    @Field("UserId") UserId: String?,
                    @Field("ServicePrice") ServicePrice: String?,
                    @Field("FromTime") FromTime: String?,
                    @Field("ToTime") ToTime: String?): Call<ResponseBody?>?

    @POST("Donation/SendRequestHelpMessages")
    fun SendRequestHelpMessages(@Query("RequestHelpId") RequestHelpId: String?,
                                @Query("Message") Message: String?,
                                @Query("Photos") Photo: String?,
                                @Query("UserId") UserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Donation/AddQuickDonation")
    fun AddQuickDonation(@Header("Authorization") auth: String,
                         @Field("amount") DonnationValue: String?,
            //   @Query("Title") Title: String?,
                         @Field("donationType") donationType: String?,
                         @Field("userId") UserId: String?,
                         @Field("charge_id") charge_id: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Donation/AddDonationForProject")
    fun AddDonationForProject(@Header("Authorization") auth: String,
                              @Field("projectId") projectId: String?,
                              @Field("amount") amount: String?,
                              @Field("donationType") donationType: String?,
                              @Field("charge_id") charge_id: String?): Call<ResponseBody?>?

    @POST("Donation/RequestHelp")
    fun RequestHelp(@Query("DonnationValue") DonnationValue: String?,
                    @Query("Title") Title: String?,
                    @Query("Description") Description: String?,
                    @Query("Attachments") Attachments: String?,
                    @Query("UserId") UserId: String?): Call<ResponseBody?>?

    @POST("RequestForHelp/AddRequestForHelp")
    fun RequestHelpForm(@Header("Authorization") auth: String,
                        @Header("Content-Type") content_type: String?,
                        @Body AddRequestForHelp: JsonObject): Call<ResponseBody?>?

    @POST("RequestForHelp/EditRequestForHelp")
    fun EditRequestForHelp(@Header("Authorization") auth: String,
                           @Header("Content-Type") content_type: String?,
                           @Body AddRequestForHelp: JsonObject?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("FamilyTree/IdentifyUser")
    fun IdentifyUser(@Header("Authorization") auth: String,
                     @Field("FamilyTreeId") FamilyTreeId: String?,
                     @Field("Attachments") Attachments: String?,
                     @Field("UserId") UserId: String?): Call<ResponseBody?>?

    @POST("RequestForHelp/AddRequestForHelp")
    fun AddRequestForHelp(@Query("RequestName") RequestName: String?,
                          @Query("RequestDate") RequestDate: String?,
                          @Query("MobileNumber") MobileNumber: String?,
                          @Query("HomeType") HomeType: String?,
                          @Query("RentAmount") RentAmount: String?,
                          @Query("MaritalStatus") MaritalStatus: String?,
                          @Query("MyIncomeType") MyIncomeType: String?,
                          @Query("MyIncomeAmount") MyIncomeAmount: String?,
                          @Query("HusbandIncomeType") HusbandIncomeType: String?,
                          @Query("HusbandIncomeAmount") HusbandIncomeAmount: String?,
                          @Query("WifeIncomeType") WifeIncomeType: String?,
                          @Query("WifeIncomeAmount") WifeIncomeAmount: String?,
                          @Query("Employer") Employer: String?,
                          @Query("WorkType") WorkType: String?,
                          @Query("Position") Position: String?,
                          @Query("IncomeSourceIds") IncomeSourceIds: String?,
                          @Query("RequestReasons") RequestReasons: String?,
                          @Header("Content-Type") content_type: String?,
                          @Body RequestForHelpPersons: MutableList<RequestForHelpPersons?>?,
                          @Query("UserId") UserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("RequestForHelp/AddRequestForHelpDocument")
    fun UploadRequestHelpDocument(@Header("Authorization") auth: String,
                                  @Field("requestForHelpId") requestForHelpId: String?,
                                  @Field("attachmentTypeId") attachmentTypeId: String?,
                                  @Field("DocumentName") DocumentName: String?,
                                  @Field("UserId") UserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("FamilyTree/AddToFamilyTree")
    fun AddToFamilyTree(@Header("Authorization") auth: String,
                        @Field("ParentId") ParentId: String?,
                        @Field("AreaId") AreaId: String?,
                        @Field("BlockNumb") BlockNumb: String?,
                        @Field("DetailsAR") DetailsAR: String?,
                        @Field("DetailsEN") DetailsEN: String?,
                        @Field("NameEN") NameEN: String?,
                        @Field("NameAR") NameAR: String?,
                        @Field("SecondName") SecondName: String?,
                        @Field("ThirdName") ThirdName: String?,
                        @Field("FourthName") FourthName: String?,
                        @Field("FifthName") FifthName: String?,
                        @Field("PositionNameAR") PositionNameAR: String?,
                        @Field("PositionNameEN") PositionNameEN: String?,
                        @Field("DateOfBirth") DateOfBirth: String?,
                        @Field("DateOfDeath") DateOfDeath: String?,
                        @Field("Gender") Gender: String?,
                        @Field("AddressEN") AddressEN: String?,
                        @Field("AddressAR") AddressAR: String?,
                        @Field("IsLive") IsLive: String?,
                        @Field("Phone") Phone: String?,
                        @Field("CountryCode") CountryCode: String?,
                        @Field("Email") Email: String?,
                        @Field("FaceBook") FaceBook: String?,
                        @Field("Instgram") Instgram: String?,
                        @Field("Twitter") Twitter: String?,
                        @Field("Photo") Photo: String?,
                        @Field("UserId") UserId: String?,
                        @Field("CivilIdNo") CivilIdNo: String?,
                        @Field("FamilyNameId") FamilyNameId: String?): Call<ResponseBody?>?

    @POST("Donation/CancelMonthlyDonnationSubscription")
    fun CancelMonthlyDonation(@Query("MonthlyDonationSubscriptionId") MonthlyDonationId: String?,
                              @Query("UserId") UserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Donation/CancelMonthlyDonnationSubscription")
    fun CancelMonthlyDonationInvoice(@Field("subscriptionId") MonthlyDonationId: String?,
                                     @Field("invoiceId") invoiceid: String?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Donation/SubscripeToMonthlyDonation")
    fun AddMonthlyDonation(@Header("Authorization") auth: String,
                           @Field("amount") DonnationValue: String?,
            // @Query("Title") Title: String?,
            // @Query("Description") Description: String?,
                           @Field("userId") UserId: String?,
                           @Field("numberOfMonths") NoOfMonth: String?,
                           @Field("startDate") StartDate: String?): Call<ResponseBody?>?

    @GET("FamilyTree/GetFamilyTree")
    fun GetFamilyTree(@Query("parentId") parentId: String?): Call<ArrayList<GetFamilyTree?>?>?

    @POST("Donation/AddMonthlyDonation")
    fun AddMonthlyDonationInvoice(@Query("invoiceId") InvoiceId: String?,
                                  @Query("Title") Title: String?,
                                  @Query("Description") Description: String?,
                                  @Query("userId") UserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Donation/MonthlySubscriptionInvoiceDonate")
    fun monthlySubscriptionInvoiceDonate(@Header("Authorization") auth: String,
                                         @Field("invoiceId") InvoiceId: String?,
                                         @Field("donationType") donationType: String?,
                                         @Field("subscriptionId") subscriptionId: String?,
                                         @Field("amount") amount: String?,
                                         @Field("charge_id") charge_id: String?,
                                         @Field("userId") UserId: String?
    ): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Donation/AddZakatDonation")
    fun ZakaatDonation(@Header("Authorization") auth: String,
                       @Field("amount") amount: String?,
                       @Field("donationType") donationType: String?,
                       @Field("zakatType") zakatType: String?,
                       @Field("charge_id") charge_id: String?,
                       @Field("zakatNotes") zakatNotes: String?): Call<ResponseBody?>?

@FormUrlEncoded
    @POST("Donation/addDonationForCause")
    fun CauseDonation(@Header("Authorization") auth: String,
                       @Field("amount") amount: String?,
                       @Field("category_id") donationType: String?,
                       @Field("cause") zakatType: String?,
                       @Field("charge_id") charge_id: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("InvitationList/DeleteInvitationList")
    fun DeleteInvitationList(@Header("Authorization") auth: String,
                             @Field("InvitationListId") InvitationListId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    //@POST("InvitationList/ResendInvitationList")
    @POST("Event/resendEvent")
    fun ResendInvitationList(@Header("Authorization") auth: String,
                             @Field("EventId") EventId: String?,
                             @Field("InvitationListId") InvitationListId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    //@POST("InvitationList/RemindInvitationList")
    @POST("Event/remindEvent")
    fun RemindInvitationList(@Header("Authorization") auth: String,
                             @Field("EventId") EventId: String?,
                             @Field("InvitationListId") InvitationListId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("News/SendNews")
    fun SendNews(@Header("Authorization") auth: String,
                 @Field("NewsTypeId") NewsTypeId: String?,
                 @Field("NewsTitleAr") NewsTitleAr: String?,
                 @Field("NewsTitleEn") NewsTitleEn: String?,
                 @Field("HeadLineEn") HeadLineEn: String?,
                 @Field("HeadLineAr") HeadLineAr: String?,
                 @Field("DetailsEn") DetailsEn: String?,
                 @Field("DetailsAr") DetailsAr: String?,
                 @Field("Photo") Photo: String?,
                 @Field("Facebook") FaceBook: String?,
                 @Field("Instagram") Instagram: String?,
                 @Field("Twitter") Twitter: String?,
                 @Field("YouTube") YouTube: String?,
                 @Field("Latitude") Latitude: String?,
                 @Field("Longitude") Longitude: String?,
                 @Field("Phone") Mobile: String?,
                 @Field("CountryCode") CountryCode: String?,
                 @Field("InsertUserId") InsertUserId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Company/AddCompany")
    fun SendCompanies(@Header("Authorization") auth: String,
                      @Field("CompanyCategoryId") CompanyCategoryId: String?,
                      @Field("TitleAR") TitleAR: String?,
                      @Field("TitleEN") TitleEN: String?,
                      @Field("DetailsEN") DetailsEN: String?,
                      @Field("DetailsAR") DetailsAR: String?,
                      @Field("CompanyLogo") CompanyLogo: String?,
                      @Field("Facebook") FaceBook: String?,
                      @Field("Instagram") Instagram: String?,
                      @Field("Twitter") Twitter: String?,
                      @Field("YouTube") YouTube: String?,
                      @Field("Latitude") Latitude: String?,
                      @Field("Longitude") Longitude: String?,
                      @Field("Telephone") Telephone: String?,
                      @Field("CountryCode") CountryCode: String?,
                      @Field("UserId") UserId: String?,
                      @Field("ForFamily") ForFamily: String?,
                      @Field("Email") Email: String?,
                      @Field("Fax") Fax: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Event/SendEvent")
    fun SendEvent(@Header("Authorization") auth: String,
                  @Field("EventTypeId") EventTypeId: String?,
                  @Field("EventDate") EventDate: String?,
                  @Field("EventNameAr") EventNameAr: String?,
                  @Field("EventNameEn") EventNameEn: String?,
                  @Field("RequestFullName") FullName: String?,
                  @Field("RequestPhone") RequestMobile: String?,
                  @Field("RequestCountryCode") RequestCountryCode: String?,
                  @Field("Photo") Photo: String?,
                  @Field("Facebook") Facebook: String?,
                  @Field("Instagram") Instagram: String?,
                  @Field("Twitter") Twitter: String?,
                  @Field("YouTube") YouTube: String?,
                  @Field("Latitude") Latitude: String?,
                  @Field("Longitude") Longitude: String?,
                  @Field("Phone") Mobile: String?,
                  @Field("CountryCode") CountryCode: String?,
                  @Field("DetailsEn") DetailsEn: String?,
                  @Field("DetailsAr") DetailsAr: String?,
                  @Field("UserId") UserId: String?,
                  @Field("InvitationListId") InvitationListId: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Dewania/SendDewania")
    fun SendDewania(@Field("TitleEn") TitleEn: String?,
                    @Field("TitleAr") TitleAr: String?,
                    @Field("DetailsEn") DetailsEn: String?,
                    @Field("DetailsAr") DetailsAr: String?,
                    @Field("WeekDays") WeekDays: String?,
                    @Field("AreaId") AreaId: String?,
                    @Field("PersonId") PersonId: String?,
                    @Field("RequestFullName") RequestFullName: String?,
                    @Field("RequestPhone") RequestPhone: String?,
                    @Field("RequestCountryCode") RequestCountryCode: String?,
                    @Field("Longitude") Longitude: String?,
                    @Field("Latitude") Latitude: String?,
                    @Field("Phone") Phone: String?,
                    @Field("CountryCode") CountryCode: String?,
                    @Field("YouTube") YouTube: String?,
                    @Field("Facebook") FaceBook: String?,
                    @Field("Twitter") Twitter: String?,
                    @Field("Instagram") Instagram: String?,
                    @Field("PhotosList") PhotosList: String?,
                    @Field("IsHouse") IsHouse: String?): Call<ResponseBody?>?

    @GET("Event/GetEventTypes")
    fun GetEventTypes(): Call<ArrayList<GetEventTypes?>?>?

    @GET("Donation/GetZakatTypes")
    fun ZakaatType(): Call<ArrayList<ZakaatType?>?>?

    @GET("Donation/donationForCause")
    fun CauseType(@Header("Authorization") auth: String,
                    @Header("Accept") accept: String)
    : Call<CauseType?>?

    @GET("Service/GetServiceTypes")
    fun GetServiceTypes(): Call<ArrayList<GetServiceTypes>?>?

    @GET("RequestForHelp/GetRequestForHelpDocuments")
    fun GetRequestForHelpDocuments(@Header("Authorization") auth: String,
                                   @Query("RequestForHelpId") RequestForHelpId: String?,
                                   @Query("AttachmentTypeId") AttachmentTypeId: String?,
                                   @Query("userId") userId: String?)
            : Call<ArrayList<GetRequestForHelpDocuments>?>?

    @GET("RequestForHelp/GetAttachmentTypes")
    fun GetRequestFromDocuments(@Header("Authorization") auth: String,
                                @Query("requestForHelpId") requestForHelpId: String?,
                                @Query("requestDocumentId") requestDocumentId: String?,
                                @Query("userId") userId: String?): Call<ArrayList<GetAttachmentTypes>?>?

    @GET("Dewania/GetWeekDays")
    fun GetWeekDays(): Call<ArrayList<GetWeekDays?>?>?

    @GET("Users/GetUser")
    fun GetUser(@Header("Authorization") auth: String,
                @Query("userId") userId: String?): Call<ArrayList<GetUser?>?>?

    @GET("News/GetNewsTypes")
    fun GetNewsTypes(): Call<ArrayList<GetNewsTypes?>?>?

    @GET("PageContent/GetTermsandconditions")
    fun GetTermsAndConditions(): Call<GetTermsandconditions?>?

    @POST("FileService/Upload")
    fun uploadFile(@Query("FileName") FileName: String?,
                   @Query("FileObjectType") FileObjectType: String?,
                   cb: Callback<String?>?)

    /* @POST("FileService/Upload")
     fun uploadImage(*//*@Body File: MultipartTypedOutput?,*//*
            *//*@Part("FileName") FileName: String?,*//*
            *//*@Part("FileObjectType") FileObjectType: String?,*//*
            cb: Callback<ArrayList<UploadFile?>?>)*/

    @Multipart
    @POST("FileService/Upload")
    fun uploadImage(@Part("FileObjectType") FileObjectType: RequestBody?,
                    @Part File: MutableList<MultipartBody.Part>?): Call<ArrayList<UploadFile?>?>

    @FormUrlEncoded
    @POST("Hall/BookHall")
    fun MakeBooking(@Header("Authorization") auth: String,
                    @Field("HallId") HallId: String?,
                    @Field("UserId") UserId: String?,
                    @Field("ReqName") RequestName: String?,
                    @Field("ReqPhone") RequestPhone: String?,
                    @Field("CountryCode") CountryCode: String?,
                    @Field("BookingDate") BookingDate: String?,
                    @Field("Details") Details: String): Call<ResponseBody?>?

    @GET("GetTopBookingUsers")
    fun GetTopBookingUser(@Query("HallId") hallId: String?,
                          @Query("BookingDate") bookingDate: String?): Call<MutableList<GetTopBookingUser?>?>?

    @GET("InvitationList/GetInvitationLists")
    fun GetInvitationLists(@Header("Authorization") auth: String,
                           @Query("userId") userId: String?,
                           @Query("IsMyList") IsMyList: String?,
                           @Query("pageIndex") pageIndex: String?): Call<ArrayList<GetInvitationLists>?>?

    @GET("Payment/PaymentList")
    fun GetPaymentLists(@Header("Authorization") auth: String,
                        @Header("Accept") accept: String,
                           @Query("UserId") userId: String?,
                           @Query("pageIndex") pageIndex: String?): Call<GetPaymentLists>


    @FormUrlEncoded
    @POST("Users/Login")
    fun login(@Field("userName") userName: String?,
              @Field("password") password: String?,
              @Field("deviceToken") deviceToken: String?): Call<RegisterModel>

    @FormUrlEncoded
    @POST("InvitationList/AddInvitationList")
    fun AddInvitationList(@Header("Authorization") auth: String,
                          @Field("Name") Name: String?,
                          @Field("UserId") UserId: String?,
                          @Field("IsPublic") IsPublic: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("InvitationList/EditInvitationList")
    fun EditInvitationList(@Header("Authorization") auth: String,
                           @Field("InvitationListId") InvitationListId: String?,
                           @Field("Name") Name: String?,
                           @Field("UserId") UserId: String?,
                           @Field("IsPublic") IsPublic: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Users/ForgetPassword")
    fun forgetPassword(@Field("UserName") userName: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Users/Register")
    fun register(@Field("Name") Name: String?,
                 @Field("Email") Email: String?,
                 @Field("Phone") Phone: String?,
                 @Field("IsFamily") IsFamily: String?,
                 @Field("AlternateNo") AlternateNo: String?,
                 @Field("UserName") UserName: String?,
                 @Field("Password") Password: String?,
                 @Field("CivilId") CivilId: String?,
                 @Field("Gender") Gender: String?,
                 @Field("CountryCode") CountryCode: String?,
                 @Field("deviceToken") deviceToken: String?): Call<RegisterModel>

    @FormUrlEncoded
    @POST("Users/EditUser")
    fun EditUser(@Header("Authorization") auth: String,
                 @Field("userId") userId: String?,
                 @Field("Email") email: String?,
                 @Field("UserName") userName: String?,
            /* @Field("Password") password: String?,*/
                 @Field("deviceToken") deviceToken: String?,
                 @Field("Name") Name: String?,
                 @Field("Phone") Phone: String?,
                 @Field("CountryCode") CountryCode: String?,
                 @Field("CivilId") CivilId: String?,
                 @Field("Gender") Gender: String?,
                 @Field("IsFamily") IsFamily: String?,
                 @Field("AlternateNo") AlternateNo: String?): Call<ResponseBody?>?

    @FormUrlEncoded
    @POST("Users/ChangePassword")
    fun changePassword(@Header("Authorization") auth: String,
                       @Field("userId") userId: String?,
                       @Field("oldPassword") oldPassword: String?,
                       @Field("password") password: String?): Call<ResponseBody?>?

    @Headers("Content-Type: application/json;charset=UTF-8")
    @FormUrlEncoded
    @POST("Users/Logout")
    fun logout(/*@Field("userId") userId: String?,*/
            @Header("Authorization") auth: String,
            @Field("deviceToken") deviceToken: String?): Call<RegisterModel>

    @FormUrlEncoded
    @POST("Users/AddTokenForAndroid")
    fun insertToken(@Header("Authorization") auth: String,
                    @Field("token") token: String?,
                    @Field("deviceType") deviceType: String?,
                    @Field("deviceId") deviceId: String?,
                    @Field("userId") userId: String?): Call<ResponseBody?>?

    @GET("CommercialAds/GetCommercialAds")
    fun GetBottomRandomAd(@Query("CommAddPlacesId") CommAddPlacesId: String?)
            : Call<ArrayList<GetCommercialAds?>?>?

    @GET("AppSetting/CheckTestingVersion")
    fun CheckTestingVersion(): Call<ArrayList<CheckTestingVersion?>?>?


}