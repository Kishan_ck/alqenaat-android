package com.qenaat.app.adapters

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.fragments.QuickDonateFragment
import com.qenaat.app.model.GetMonthlyDonations
import java.util.*

/**
 * Created by DELL on 14-Nov-17.
 */
class MonthlyDonationAdapter(var act: FragmentActivity,
                             private val itemsData: ArrayList<GetMonthlyDonations>) : RecyclerView.Adapter<MonthlyDonationAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.monthly_donation_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.tv_title.setVisibility(View.GONE)
            viewHolder.tv_title.setText(itemsData.get(position).Title)
            viewHolder.tv_area.setText("""${act.getString(R.string.NoOfMonthsLabel)} ${itemsData.get(position).NumberOfMonths}
${itemsData.get(position).Amount} ${act.getString(R.string.KD)} ${act.getString(R.string.PerMonth)}""")
            viewHolder.tv_day.setOnClickListener(View.OnClickListener { QuickDonateFragment.Companion.CancelMonthlyDonation(position) })
            viewHolder.tv_details.setOnClickListener(View.OnClickListener {

                val gson = Gson()
                val b = Bundle()

                b.putString("GetMonthlyDonations", gson.toJson(itemsData.get(position)))
                ContentActivity.Companion.openSubscriptionInvoiceFragment(b)
            })
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetMonthlyDonations", gson.toJson(itemsData.get(position)))
                Log.e("rohann", "rohannn11111" + b)
                ContentActivity.Companion.openSubscriptionInvoiceFragment(b)
            })

            /*viewHolder.tv_day.setVisibility(View.VISIBLE);

            if(itemsData.get(position).getFinished().equalsIgnoreCase("true") ||
                    itemsData.get(position).getCanceled().equalsIgnoreCase("true")){

                viewHolder.tv_day.setVisibility(View.GONE);

            }*/viewHolder.tv_day.setVisibility(View.GONE)
            viewHolder.tv_details.setVisibility(View.GONE)
            viewHolder.img_arrow.setVisibility(View.VISIBLE)
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var img_arrow: ImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var tv_area: TextView
        var tv_day: TextView
        var tv_title: TextView
        var tv_details: TextView

        init {
            img_arrow = itemLayoutView.findViewById<View?>(R.id.img_arrow) as ImageView
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            tv_day = itemLayoutView.findViewById<View?>(R.id.tv_day) as TextView
            tv_area = itemLayoutView.findViewById<View?>(R.id.tv_area) as TextView
            tv_details = itemLayoutView.findViewById<View?>(R.id.tv_details) as TextView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_content)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}