package com.qenaat.app.fragments


import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jdev.countryutil.Constants
import com.jdev.countryutil.CountryUtil
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util.Companion.getUserCountryInfo
import com.qenaat.app.adapters.AlertAdapterFA
import com.qenaat.app.classes.*
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkEmptyString
import com.qenaat.app.classes.FixControl.checkFixLength
import com.qenaat.app.classes.FixControl.createPartFromFile
import com.qenaat.app.classes.FixControl.createPartFromString
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.model.*
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.add_news.*
import me.drakeet.materialdialog.MaterialDialog
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by DELL on 12-Nov-17.
 */
class AddNewsFragment : Fragment(), View.OnClickListener {
    lateinit var act: FragmentActivity
    lateinit var mLangSessionManager: LanguageSessionManager
    lateinit var mainLayout: RelativeLayout
    lateinit var tv_tree_label: TextView
    lateinit var tv_category_label: TextView
    lateinit var tv_my_invitation_list_label: TextView
    lateinit var tv_others_list_label: TextView
    lateinit var tv_location_label: TextView
    lateinit var tv_date: TextView
    lateinit var et_full_name: EditText
    lateinit var et_title: EditText
    lateinit var et_mobile: EditText
    lateinit var et_request_mobile: EditText
    lateinit var et_details_label: EditText
    lateinit var et_youTube: EditText
    lateinit var et_facebook: EditText
    lateinit var et_instagram: EditText
    lateinit var et_twitter: EditText
    lateinit var mloading: ProgressBar
    lateinit var relative_image1: RelativeLayout
    lateinit var relative_image2: RelativeLayout
    lateinit var relative_image3: RelativeLayout
    lateinit var relative_image4: RelativeLayout
    lateinit var relative_image5: RelativeLayout
    lateinit var img_select1: ImageView
    lateinit var img_select2: ImageView
    lateinit var img_select3: ImageView
    lateinit var img_select4: ImageView
    lateinit var img_select5: ImageView
    lateinit var img_delete1: ImageView
    lateinit var img_delete2: ImageView
    lateinit var img_delete3: ImageView
    lateinit var img_delete4: ImageView
    lateinit var img_delete5: ImageView
    lateinit var tv_add_images: TextView
    private var arrayListPhoto: ArrayList<String?> = ArrayList()
    var imageCount = 0
    lateinit var tv_add_ad: TextView
    private var index = 0
    private var current_path: String = ""
    private var images: StringBuilder = StringBuilder()
    var isEdit = false

    //GetProducts items;
    private var arrayListPhotoEdit: ArrayList<String?> = ArrayList()
    lateinit var horizontalScrollView: HorizontalScrollView
    var countryId: String = ""
    var cityId: String = ""
    var treeId: String = ""
    var catId: String = ""
    var myInvitationId: String = ""
    var otherInvitationId: String = ""
    var isAgree = false
    private val fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private val mFormatter: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.ENGLISH)
    var getNewsTypesArrayList: ArrayList<GetNewsTypes?> = ArrayList()
    var getCompanyCategoriesArrayList: ArrayList<GetCompanyCategories?> = ArrayList()
    var myInvitationListsArrayList: ArrayList<GetInvitationLists?> = ArrayList()
    var otherInvitationListsArrayList: ArrayList<GetInvitationLists?> = ArrayList()
    var getEventTypesArrayList: ArrayList<GetEventTypes?> = ArrayList()
    var contentType: String = "1"
    lateinit var scroll_view: ScrollView
    lateinit var relative_family_input: RelativeLayout
    lateinit var img_check_family_input: ImageView
    lateinit var tv_family_input: TextView
    var isQenaatFamily = false
    var config: Config = Config()
    private var imagesCameraGallery: ArrayList<Image?> = ArrayList()
    private var countryCode: String = ""
    private var countryFlag: Int = 0
    private var countryName: String = ""
    private var countryCodeRequest: String = ""
    private var countryFlagRequest: Int = 0
    private var countryNameRequest: String = ""
    private var isRequestPhone = false

    //val multipartTypedOutput = MultipartTypedOutput()
    var permissionStr = ""
    private var imagePartList: MutableList<MultipartBody.Part>? = mutableListOf()
    private var textPart: RequestBody? = null

    var comingFrom: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        countryCode = getUserCountryInfo(act, ConstanstParameters.COUNTRY_CODE)
        countryName = getUserCountryInfo(act, ConstanstParameters.COUNTRY_NAME)
        countryFlag = getUserCountryInfo(act, ConstanstParameters.COUNTRY_FLAG).toInt()

        countryCodeRequest = getUserCountryInfo(act, ConstanstParameters.COUNTRY_CODE)
        countryNameRequest = getUserCountryInfo(act, ConstanstParameters.COUNTRY_NAME)
        countryFlagRequest = getUserCountryInfo(act, ConstanstParameters.COUNTRY_FLAG).toInt()


        try {
            Log.d("onCreate", "Bundle")
            mLangSessionManager = LanguageSessionManager(act)
            mSessionManager = SessionManager(act)
            if (arguments != null) {
                if (arguments?.getString("isEdit").equals("0", ignoreCase = true)) {
                    isEdit = false
                } else if (arguments?.getString("isEdit").equals("1", ignoreCase = true)) {
                    isEdit = true
                    //                    Gson gson = new Gson();
//                    items = gson.fromJson(getArguments().getString("GetProducts"),
//                            GetProducts.class);
                }
                contentType = arguments?.getString("contentType")!!
                if (arguments!!.containsKey("comingFrom")) {
                    comingFrom = arguments!!.getString("comingFrom")!!
                }
            }

        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return try {
            mainLayout = inflater.inflate(R.layout.add_news, null) as RelativeLayout
            act.softWindow()
            mainLayout
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        try {
            img_check_family_input = mainLayout.findViewById<View?>(R.id.img_check_family_input) as ImageView
            relative_family_input = mainLayout.findViewById<View?>(R.id.relative_family_input) as RelativeLayout
            tv_family_input = mainLayout.findViewById<View?>(R.id.tv_family_input) as TextView
            relative_family_input.setOnClickListener(this)
            tv_add_ad = mainLayout.findViewById<View?>(R.id.tv_add_ad) as TextView
            tv_add_images = mainLayout.findViewById<View?>(R.id.tv_add_images) as TextView
            horizontalScrollView = mainLayout.findViewById<View?>(R.id.horizontalScrollView) as HorizontalScrollView
            scroll_view = mainLayout.findViewById<View?>(R.id.scroll_view) as ScrollView
            img_delete1 = mainLayout.findViewById<View?>(R.id.img_delete1) as ImageView
            img_delete2 = mainLayout.findViewById<View?>(R.id.img_delete2) as ImageView
            img_delete3 = mainLayout.findViewById<View?>(R.id.img_delete3) as ImageView
            img_delete4 = mainLayout.findViewById<View?>(R.id.img_delete4) as ImageView
            img_delete5 = mainLayout.findViewById<View?>(R.id.img_delete5) as ImageView
            img_select1 = mainLayout.findViewById<View?>(R.id.img_select1) as ImageView
            img_select2 = mainLayout.findViewById<View?>(R.id.img_select2) as ImageView
            img_select3 = mainLayout.findViewById<View?>(R.id.img_select3) as ImageView
            img_select4 = mainLayout.findViewById<View?>(R.id.img_select4) as ImageView
            img_select5 = mainLayout.findViewById<View?>(R.id.img_select5) as ImageView
            relative_image1 = mainLayout.findViewById<View?>(R.id.relative_image1) as RelativeLayout
            relative_image2 = mainLayout.findViewById<View?>(R.id.relative_image2) as RelativeLayout
            relative_image3 = mainLayout.findViewById<View?>(R.id.relative_image3) as RelativeLayout
            relative_image4 = mainLayout.findViewById<View?>(R.id.relative_image4) as RelativeLayout
            relative_image5 = mainLayout.findViewById<View?>(R.id.relative_image5) as RelativeLayout
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            tv_tree_label = mainLayout.findViewById<View?>(R.id.tv_tree_label) as TextView
            tv_category_label = mainLayout.findViewById<View?>(R.id.tv_category_label) as TextView
            tv_my_invitation_list_label = mainLayout.findViewById<View?>(R.id.tv_my_invitation_list_label) as TextView
            tv_others_list_label = mainLayout.findViewById<View?>(R.id.tv_others_list_label) as TextView
            et_mobile = mainLayout.findViewById<View?>(R.id.et_mobile) as EditText
            et_request_mobile = mainLayout.findViewById<View?>(R.id.et_request_mobile) as EditText
            et_details_label = mainLayout.findViewById<View?>(R.id.et_details_label) as EditText
            et_youTube = mainLayout.findViewById<View?>(R.id.et_youTube) as EditText
            et_facebook = mainLayout.findViewById<View?>(R.id.et_facebook) as EditText
            et_instagram = mainLayout.findViewById<View?>(R.id.et_instagram) as EditText
            et_twitter = mainLayout.findViewById<View?>(R.id.et_twitter) as EditText
            et_full_name = mainLayout.findViewById<View?>(R.id.et_full_name) as EditText
            et_title = mainLayout.findViewById<View?>(R.id.et_title) as EditText
            tv_location_label = mainLayout.findViewById<View?>(R.id.tv_location_label) as TextView
            tv_date = mainLayout.findViewById<View?>(R.id.tv_date) as TextView
            tv_location_label.setOnClickListener(this)
            tv_date.setOnClickListener(this)
            tv_add_ad.setOnClickListener(this)
            tv_add_images.setOnClickListener(this)
            tv_category_label.setOnClickListener(this)
            tv_my_invitation_list_label.setOnClickListener(this)
            tv_others_list_label.setOnClickListener(this)
            img_delete1.setOnClickListener(this)
            img_select1.setOnClickListener(this)
            //            relative_image1.setOnClickListener(this);
            img_delete2.setOnClickListener(this)
            img_delete3.setOnClickListener(this)
            img_delete4.setOnClickListener(this)
            img_delete5.setOnClickListener(this)
            LL_country_code.setOnClickListener(this)
            LL_country_code_request.setOnClickListener(this)
            et_mobile.setTypeface(ContentActivity.Companion.tf)
            et_request_mobile.setTypeface(ContentActivity.Companion.tf)
            et_details_label.setTypeface(ContentActivity.Companion.tf)
            et_youTube.setTypeface(ContentActivity.Companion.tf)
            et_facebook.setTypeface(ContentActivity.Companion.tf)
            et_instagram.setTypeface(ContentActivity.Companion.tf)
            et_twitter.setTypeface(ContentActivity.Companion.tf)
            et_full_name.setTypeface(ContentActivity.Companion.tf)
            et_title.setTypeface(ContentActivity.Companion.tf)
            tv_location_label.setTypeface(ContentActivity.Companion.tf)
            tv_date.setTypeface(ContentActivity.Companion.tf)
            tv_tree_label.setTypeface(ContentActivity.Companion.tf)
            tv_category_label.setTypeface(ContentActivity.Companion.tf)
            tv_others_list_label.setTypeface(ContentActivity.Companion.tf)
            tv_my_invitation_list_label.setTypeface(ContentActivity.Companion.tf)
            var imgW = (act.getResources().getDrawable(
                    R.drawable.add_photos) as BitmapDrawable).bitmap.width
            val imgH = (act.getResources().getDrawable(
                    R.drawable.add_photos) as BitmapDrawable).bitmap.height
            img_select1.getLayoutParams().height = imgH
            img_select1.getLayoutParams().width = imgW
            img_select2.getLayoutParams().height = imgH
            img_select2.getLayoutParams().width = imgW
            img_select3.getLayoutParams().height = imgH
            img_select3.getLayoutParams().width = imgW
            img_select4.getLayoutParams().height = imgH
            img_select4.getLayoutParams().width = imgW
            img_select5.getLayoutParams().height = imgH
            img_select5.getLayoutParams().width = imgW
            imgW = (act.getResources().getDrawable(
                    R.drawable.desc) as BitmapDrawable).bitmap.width
            //et_details_label.getLayoutParams().width = imgW
            dateFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            relative_family_input.setVisibility(View.GONE)

            tv_country_code.isSelected = true
            tv_country_code_request.isSelected = true
            tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
            iv_country_flag.setImageResource(countryFlag)
            tv_country_code_request.text = String.format("(%s) %s", countryNameRequest, countryCodeRequest)
            iv_country_flag_request.setImageResource(countryFlagRequest)

            //News
            if (contentType.equals("1", ignoreCase = true)) {
                tv_location_label.setVisibility(View.VISIBLE)
                et_title.setHint(act.getString(R.string.TitleLabel))
                tv_add_ad.setText(act.getString(R.string.AddNews))
                tv_category_label.setHint(act.getString(R.string.SelectNews1))
                et_details_label.setHint(act.getString(R.string.NewsDetails))
                til_full_name.setVisibility(View.GONE)
                et_mobile.setVisibility(View.VISIBLE)
                tv_date.setVisibility(View.GONE)
                RL_mobile_request_main.setVisibility(View.GONE)
            }
            //Occasion
            else if (contentType.equals("2", ignoreCase = true)) {
                tv_my_invitation_list_label.setVisibility(View.VISIBLE)
                tv_others_list_label.setVisibility(View.VISIBLE)
                et_mobile.setVisibility(View.VISIBLE)
                tv_location_label.setVisibility(View.VISIBLE)
                tv_date.setVisibility(View.VISIBLE)
                til_full_name.setVisibility(View.VISIBLE)
                et_title.setHint(act.getString(R.string.TitleLabel))
                tv_add_ad.setText(act.getString(R.string.AddOccasion))
//                tv_add_ad.setText(act.getString(R.string.AddEvent))
                tv_category_label.setHint(act.getString(R.string.SelectOccasion1))
                et_details_label.setHint(act.getString(R.string.OccasionDetails))
                Log.d("HOUR_OF_DAY1", "" + Calendar.getInstance()[Calendar.HOUR_OF_DAY])
                Log.d("HOUR_OF_DAY2", "" + Calendar.getInstance()[Calendar.MINUTE])
                tv_date.setText(mFormatter.format(Date()).toString())


                val ok = Calendar.getInstance()[Calendar.HOUR_OF_DAY] <= 16;
                val ok1 = Calendar.getInstance()[Calendar.HOUR_OF_DAY] >= 17
                val ok2 = Calendar.getInstance()[Calendar.HOUR_OF_DAY] <= 17
                val ok3 = Calendar.getInstance()[Calendar.MINUTE] <= 30
                Log.e("111111", "" + ok);
                Log.e("111111", "" + ok1);
                Log.e("111111", "" + ok2);
                Log.e("111111", "" + ok3);

                if (Calendar.getInstance()[Calendar.HOUR_OF_DAY] <= 16
                        || Calendar.getInstance()[Calendar.HOUR_OF_DAY] >= 17
                        && Calendar.getInstance()[Calendar.HOUR_OF_DAY] <= 17
                        && Calendar.getInstance()[Calendar.MINUTE] <= 30
                ) {

                    Log.e("111111", "INNNNNNNNNNNNNNNN");
                    val date = Date() // your date
                    val cal = Calendar.getInstance()
                    cal.time = date
                    val year = cal[Calendar.YEAR]
                    val month = cal[Calendar.MONTH]
                    val day = cal[Calendar.DAY_OF_MONTH]

                    // tv_from_time.setText(new Date().getDay() + "/" + new Date().getMonth() + "/" + new Date().getYear() + " 05:30 PM");
                    /*tv_date.setText(cal[Calendar.DAY_OF_MONTH].toString() + "/"
                            + cal[Calendar.MONTH]
                            + "/" + cal[Calendar.YEAR] + " 05:30 PM")*/
//                    tv_date.setText(cal[Calendar.DAY_OF_MONTH].toString() + "/" + (
//                            if (cal[Calendar.MONTH].toString().length in 1..1) "0" + cal[Calendar.MONTH]
//                            else cal[Calendar.MONTH].toString())
//                            + "/" + cal[Calendar.YEAR] + " 05:30 PM")
                }
            }
            //Company
            else if (contentType.equals("3", ignoreCase = true)) {
                tv_location_label.setVisibility(View.VISIBLE)
                relative_family_input.setVisibility(View.VISIBLE)
                et_title.setHint(act.getString(R.string.TitleLabel))
                tv_add_ad.setText(act.getString(R.string.AddCompany))
                tv_category_label.setHint(act.getString(R.string.SelectCategory))
                et_details_label.setHint(act.getString(R.string.CompanyDetails))
                til_full_name.setVisibility(View.GONE)
                et_mobile.setVisibility(View.VISIBLE)
                tv_date.setVisibility(View.GONE)
                RL_mobile_request_main.setVisibility(View.GONE)

            }
            ContentActivity.Companion.setTextFonts(mainLayout)
            //setImagePickerConfig()
        } catch (e: Exception) {
            Log.e(TAG + " " + " initViews: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("onStart", "onStart")
        //ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle.putVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAddAd.putVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(mLangSessionManager)
        /* et_details_label.setMovementMethod(ScrollingMovementMethod())
         scroll_view.setOnTouchListener(OnTouchListener { v, event ->
             et_details_label.getParent().requestDisallowInterceptTouchEvent(false)
             false
         })
         et_details_label.setOnTouchListener(OnTouchListener { v, event ->
             et_details_label.getParent().requestDisallowInterceptTouchEvent(true)
             false
         })*/
        if (contentType.equals("1", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.AddNews))
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else if (contentType.equals("2", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.AddOccasion))
//            ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.AddEvent))
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else if (contentType.equals("3", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.AddCompany))
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        }
        if (isEdit) {
        } else {
            if (ContentActivity.Companion.catId.length > 0) {
                tv_category_label.setText(ContentActivity.Companion.catName)
                catId = ContentActivity.Companion.catId
            } else {
                getCategories()

            }
            if (ContentActivity.Companion.myInvitationId.length > 0) {
                tv_my_invitation_list_label.setText(ContentActivity.Companion.myInvitationName)
                myInvitationId = ContentActivity.Companion.myInvitationId
            } else {
                GetInvitationLists("true")

            }
            if (ContentActivity.Companion.otherInvitationId.length > 0) {
                tv_others_list_label.setText(ContentActivity.Companion.otherInvitationName)
                otherInvitationId = ContentActivity.Companion.otherInvitationId
            } else {
                GetInvitationLists("" +
                        "false")
            }
            if (ContentActivity.Companion.treeId.length > 0) {
                tv_tree_label.setText(ContentActivity.Companion.treeName)
                treeId = ContentActivity.Companion.treeId
            }
            //            else{
//
//                GetTrees();
//
//            }
            if (ContentActivity.Companion.fullName.length > 0) {
                et_full_name.setText(ContentActivity.Companion.fullName)
            }
            if (ContentActivity.Companion.title.length > 0) {
                et_title.setText(ContentActivity.Companion.title)
            }
            if (ContentActivity.Companion.mobile.length > 0) {
                et_mobile.setText(ContentActivity.Companion.mobile)
            }
            if (ContentActivity.Companion.request_mobile.length > 0) {
                et_request_mobile.setText(ContentActivity.Companion.request_mobile)
            }
            if (ContentActivity.Companion.location.length > 0) {
                tv_location_label.setText(ContentActivity.Companion.location)
            }
            if (ContentActivity.Companion.details.length > 0) {
                et_details_label.setText(ContentActivity.Companion.details)
            }
            if (ContentActivity.Companion.date.length > 0) {
                tv_date.setText(ContentActivity.Companion.date)
            }
            if (ContentActivity.Companion.youTube.length > 0) {
                et_youTube.setText(ContentActivity.Companion.youTube)
            }
            if (ContentActivity.Companion.facebook.length > 0) {
                et_facebook.setText(ContentActivity.Companion.facebook)
            }
            if (ContentActivity.Companion.instagram.length > 0) {
                et_instagram.setText(ContentActivity.Companion.instagram)
            }
            if (ContentActivity.Companion.twitter.length > 0) {
                et_twitter.setText(ContentActivity.Companion.twitter)
            }
            val gson: Gson
            if (mSessionManager?.getImagePath() !== "" && mSessionManager?.getImagePath() != null) {
                gson = Gson()
                val listType = object : TypeToken<ArrayList<String?>?>() {}.type
                arrayListPhoto = gson.fromJson(mSessionManager?.getImagePath(), listType)
                setImages(arrayListPhoto.size)
            }
        }
    }

    override fun onClick(view: View?) {
        val b = Bundle()
        Log.d("onClick", "onClick==" + view?.getId())
        val gson = Gson()
        when (view?.getId()) {
            R.id.LL_country_code_request -> {
                isRequestPhone = true
                CountryUtil(act).setTitle("").build()
            }
            R.id.LL_country_code -> {
                isRequestPhone = false
                CountryUtil(act).setTitle("").build()
            }
            R.id.relative_family_input -> {
                isQenaatFamily = !isQenaatFamily
                img_check_family_input.setImageResource(R.drawable.check)
                if (isQenaatFamily) {
                    img_check_family_input.setImageResource(R.drawable.checked)
                }
            }
            /*R.id.tv_add_ad -> {
                act.hideKeyboard()
                //if(arrayListPhoto.size()>0 || arrayListPhotoEdit.size()>0){
                var isError = false
                var isPhoneError = false
                if (catId.length > 0 && et_title.getText().toString().length > 0
                        && et_request_mobile.getText().toString().length > 0
                        && et_details_label.getText().toString().length > 0) {
                    //  isError = false;
                } else {
                    isError = true
                }
                if (contentType.equals("2", ignoreCase = true)) {

                    //disable here et_full_name
                    if (tv_date.getText().toString().length > 0) {

                        //  isError = false;
                    } else {
                        isError = true
                    }
                } else {
                    treeId = "0"
                    et_full_name.setText("aaa")
                    //tv_date.setText(mFormatter.format(Date()).toString())
                }
                if (et_mobile.getText().toString().length > 0) {
                    if (FixControl.CheckMobileNumberIs_8(et_mobile)!!) {
                    } else {
                        isPhoneError = true
                    }
                }
                if (isError) {
                    Toast.makeText(act, act.getString(R.string.FillAllFields), Toast.LENGTH_LONG).show()
                } else {
                    if (isPhoneError) {
                        Toast.makeText(act, act.getString(R.string.MobileError), Toast.LENGTH_LONG).show()
                    } else {
                        index = 0
                        mloading.setVisibility(View.VISIBLE)
                        GlobalFunctions.DisableLayout(mainLayout)
                        if (arrayListPhoto.size > 0) {
                            current_path = if (isEdit) {
                                arrayListPhotoEdit.get(index)!!
                            } else {
                                arrayListPhoto.get(index)!!
                            }
                            val uploadImagesAysn = UploadImagesAysn()
                            uploadImagesAysn.execute()
                        } else SendNews()
                    }
                }
            }*/
            R.id.tv_add_ad -> {
                act.hideKeyboard()
                if (isValid(contentType)) {
                    index = 0
                    mloading.visibility = View.VISIBLE
                    GlobalFunctions.DisableLayout(mainLayout)
                    if (arrayListPhoto.size > 0) {
                        current_path = if (isEdit) {
                            arrayListPhotoEdit.get(index)!!
                        } else {
                            arrayListPhoto.get(index)!!
                        }
                        uploadPhotos()
                        /*val uploadImagesAysn = UploadImagesAysn()
                        uploadImagesAysn.execute()*/
                    } else SendNews()
                }
            }
            R.id.img_select1 -> Log.d("test", "clicked img_select1")
            R.id.img_delete1 -> {
                Log.d("test", "clicked img_delete1")
                if (isEdit) {
                    arrayListPhotoEdit.remove(img_select1.getTag().toString())
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                    Log.e("arrayListPhoto after", "" + arrayListPhotoEdit.size)
                    setEditImages(arrayListPhotoEdit.size)
                } else {
                    arrayListPhoto.remove(img_select1.getTag().toString())
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                    Log.e("arrayListPhoto after", "" + arrayListPhoto.size)
                    setImages(arrayListPhoto.size)
                }
            }
            R.id.img_delete2 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select2.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select2.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.img_delete3 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select3.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select3.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.img_delete4 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select4.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select4.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.img_delete5 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select5.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select5.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.tv_add_images -> {
                permissionStr = Manifest.permission.CAMERA
                GlobalFunctions.requestMultiplePermission(act,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE))

                /*var imageCount = 1
                imageCount = if (contentType.equals("1", ignoreCase = true)) {
                    1
                } else {
                    1
                }

                //First checking if the app is already having the permission
                if (GlobalFunctions.isReadStorageAllowed(act)) {
                    if (GlobalFunctions.isCameraAllowed(act)) {
                        saveData()
                        if (mLangSessionManager.getLang() == "ar") {
                            Toast.makeText(act, act.getString(R.string.SlideLeft), Toast.LENGTH_LONG).show()
                        }
                        if (isEdit) {
                            if (arrayListPhotoEdit.size < imageCount) {
                                ImagePicker.with(this)
                                        .setFolderMode(false)
                                        .setCameraOnly(false)
                                        .setFolderTitle(act.getString(R.string.GalleryLabel))
                                        .setMultipleMode(true)
                                        //.setSelectedImages(config.getSelectedImages())
                                        .setMaxSize(imageCount - arrayListPhotoEdit.size)
                                        .start()
                            }
                        } else {
                            if (arrayListPhoto.size < imageCount) {
                                ImagePicker.with(this)
                                        .setFolderMode(false)
                                        .setCameraOnly(false)
                                        .setFolderTitle(act.getString(R.string.GalleryLabel))
                                        .setMultipleMode(true)
                                        //.setSelectedImages(config.getSelectedImages())
                                        .setMaxSize(imageCount - arrayListPhoto.size)
                                        .start()
                            }
                        }
                        return
                    }
                }

                //If the app has not the permission then asking for the permission
                if (GlobalFunctions.isCameraAllowed(act)) {
                    GlobalFunctions.requestStoragePermission(act)
                } else {
                    GlobalFunctions.requestCameraePermission(act)
                }*/
            }
            R.id.tv_location_label -> {
                saveData()
                /*if (GlobalFunctions.isGPSAllowed(act)) {
                    ContentActivity.Companion.openCurrentLocationFragmentFadeIn()
                    return
                }*/
                permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                GlobalFunctions.requestGPSPermission(act)
            }
            R.id.tv_date -> SlideDateTimePicker.Builder(act.getSupportFragmentManager())
                    .setListener(listener)
                    .setInitialDate(Date())
                    .setMinDate(Date())
                    .build()
                    .show()
            R.id.tv_my_invitation_list_label ->
                if (contentType.equals("2", ignoreCase = true)) {
                    if (myInvitationListsArrayList.size > 0) {
                        val inflater1 = act
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val v1 = inflater1.inflate(R.layout.country_dialog, null)
                        val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                        val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                        tv_title1.setText(act.getString(R.string.SelectLabel))
                        tv_title1.setTypeface(ContentActivity.Companion.tf)
                        val arrayList1 = ArrayList<String?>()
                        for (newsTypes in myInvitationListsArrayList) {
                            if (mLangSessionManager.getLang() == "en") {
                                arrayList1.add(newsTypes?.Name)
                            } else {
                                arrayList1.add(newsTypes?.Name)
                            }
                        }
                        arrayList1.add(act.getString(R.string.CancelLabel))
                        val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                        listView1.setAdapter(alertAdapter1)
                        FixControl.setListViewHeightBasedOnChildren(listView1)
                        val alert1 = MaterialDialog(act).setContentView(v1)
                        alert1.show()
                        alert1.setCanceledOnTouchOutside(true)
                        listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                            alert1.dismiss()
                            val item = arrayList1[i]
                            if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                            } else {
                                myInvitationId = myInvitationListsArrayList.get(i)?.Id!!
                                ContentActivity.Companion.myInvitationId = myInvitationId
                                tv_my_invitation_list_label.setText(item)
                                ContentActivity.Companion.myInvitationName = item!!

                                //clear other invitation
                                otherInvitationId = ""
                                ContentActivity.Companion.otherInvitationId = otherInvitationId
                                tv_others_list_label.setText("")
                                ContentActivity.Companion.otherInvitationName = ""
                            }
                        })
                    }
                }
            R.id.tv_others_list_label -> if (contentType.equals("2", ignoreCase = true)) {
                if (otherInvitationListsArrayList.size > 0) {
                    val inflater1 = act
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val v1 = inflater1.inflate(R.layout.country_dialog, null)
                    val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                    val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                    tv_title1.setText(act.getString(R.string.SelectLabel))
                    tv_title1.setTypeface(ContentActivity.Companion.tf)
                    val arrayList1 = ArrayList<String?>()
                    for (newsTypes in otherInvitationListsArrayList) {
                        if (mLangSessionManager.getLang() == "en") {
                            arrayList1.add(newsTypes?.Name)
                        } else {
                            arrayList1.add(newsTypes?.Name)
                        }
                    }
                    arrayList1.add(act.getString(R.string.CancelLabel))
                    val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                    listView1.setAdapter(alertAdapter1)
                    FixControl.setListViewHeightBasedOnChildren(listView1)
                    val alert1 = MaterialDialog(act).setContentView(v1)
                    alert1.show()
                    alert1.setCanceledOnTouchOutside(true)
                    listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                        alert1.dismiss()
                        val item = arrayList1[i]
                        if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                        } else {
                            otherInvitationId = otherInvitationListsArrayList.get(i)?.Id!!
                            ContentActivity.Companion.otherInvitationId = otherInvitationId
                            tv_others_list_label.setText(item)
                            ContentActivity.Companion.otherInvitationName = item!!

                            //clear my invitation
                            myInvitationId = ""
                            ContentActivity.Companion.myInvitationId = myInvitationId
                            tv_my_invitation_list_label.setText("")
                            ContentActivity.Companion.myInvitationName = ""
                        }
                    })
                }
            }
            R.id.tv_category_label -> if (contentType.equals("1", ignoreCase = true)) {
                if (getNewsTypesArrayList.size > 0) {
                    val inflater1 = act
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val v1 = inflater1.inflate(R.layout.country_dialog, null)
                    val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                    val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                    if (contentType.equals("1", ignoreCase = true)) {
                        tv_title1.setText(act.getString(R.string.SelectNews1))
                    } else {
                        tv_title1.setText(act.getString(R.string.SelectOccasion1))
                    }
                    tv_title1.setTypeface(ContentActivity.Companion.tf)
                    val arrayList1 = ArrayList<String?>()
                    for (newsTypes in getNewsTypesArrayList) {
                        if (mLangSessionManager.getLang() == "en") {
                            arrayList1.add(newsTypes?.NameEn)
                        } else {
                            arrayList1.add(newsTypes?.NameAr)
                        }
                    }
                    arrayList1.add(act.getString(R.string.CancelLabel))
                    val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                    listView1.setAdapter(alertAdapter1)
                    FixControl.setListViewHeightBasedOnChildren(listView1)
                    val alert1 = MaterialDialog(act).setContentView(v1)
                    alert1.show()
                    alert1.setCanceledOnTouchOutside(true)
                    listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                        alert1.dismiss()
                        val item = arrayList1[i]
                        if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                        } else {
                            catId = getNewsTypesArrayList.get(i)?.Id!!
                            ContentActivity.Companion.catId = catId
                            tv_category_label.setText(item)
                            ContentActivity.Companion.catName = item!!
                        }
                    })
                }
            } else if (contentType.equals("2", ignoreCase = true)) {
                if (getEventTypesArrayList.size > 0) {
                    val inflater1 = act
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val v1 = inflater1.inflate(R.layout.country_dialog, null)
                    val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                    val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                    if (contentType.equals("1", ignoreCase = true)) {
                        tv_title1.setText(act.getString(R.string.SelectNews1))
                    } else {
                        tv_title1.setText(act.getString(R.string.SelectOccasion1))
                    }
                    tv_title1.setTypeface(ContentActivity.Companion.tf)
                    val arrayList1 = ArrayList<String?>()
                    for (eventTypes in getEventTypesArrayList) {
                        if (mLangSessionManager.getLang() == "en") {
                            arrayList1.add(eventTypes?.EventTypeEn)
                        } else {
                            arrayList1.add(eventTypes?.EventTypeAr)
                        }
                    }
                    arrayList1.add(act.getString(R.string.CancelLabel))
                    val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                    listView1.setAdapter(alertAdapter1)
                    FixControl.setListViewHeightBasedOnChildren(listView1)
                    val alert1 = MaterialDialog(act).setContentView(v1)
                    alert1.show()
                    alert1.setCanceledOnTouchOutside(true)
                    listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                        alert1.dismiss()
                        val item = arrayList1[i]
                        if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                        } else {
                            catId = getEventTypesArrayList.get(i)?.Id!!
                            ContentActivity.Companion.catId = catId
                            tv_category_label.setText(item)
                            ContentActivity.Companion.catName = item!!
                        }
                    })
                }
            } else if (contentType.equals("3", ignoreCase = true)) {
                if (getCompanyCategoriesArrayList.size > 0) {
                    val inflater1 = act
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val v1 = inflater1.inflate(R.layout.country_dialog, null)
                    val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                    val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                    if (contentType.equals("3", ignoreCase = true)) {
                        tv_title1.setText(act.getString(R.string.SelectCategory))
                    } else {
                        tv_title1.setText(act.getString(R.string.SelectOccasion1))
                    }
                    tv_title1.setTypeface(ContentActivity.Companion.tf)
                    val arrayList1 = ArrayList<String?>()
                    for (companyCategories in getCompanyCategoriesArrayList) {
                        if (mLangSessionManager.getLang() == "en") {
                            arrayList1.add(companyCategories?.CompanyCategoryNameEn)
                        } else {
                            arrayList1.add(companyCategories?.CompanyCategoryNameAr)
                        }
                    }
                    arrayList1.add(act.getString(R.string.CancelLabel))
                    val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                    listView1.setAdapter(alertAdapter1)
                    FixControl.setListViewHeightBasedOnChildren(listView1)
                    val alert1 = MaterialDialog(act).setContentView(v1)
                    alert1.show()
                    alert1.setCanceledOnTouchOutside(true)
                    listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                        alert1.dismiss()
                        val item = arrayList1[i]
                        if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                        } else {
                            catId = getCompanyCategoriesArrayList.get(i)?.Id!!
                            ContentActivity.Companion.catId = catId
                            tv_category_label.setText(item)
                            ContentActivity.Companion.catName = item!!
                        }
                    })
                }
            }
        }
    }

    private fun isValid(contentType: String): Boolean {
        when {
            catId.checkEmptyString() -> {
                when (contentType) {
                    //News
                    "1" -> mainLayout.showSnakeBar(getString(R.string.SelectNews1))
                    //Occasion
                    "2" -> mainLayout.showSnakeBar(getString(R.string.select_occasion))
                    //Company
                    "3" -> mainLayout.showSnakeBar(getString(R.string.select_company_category))
                }
                return false
            }
            et_title.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_title))
                et_title.requestFocus()
                return false
            }
            et_details_label.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_details))
                et_details_label.requestFocus()
                return false
            }
            else -> {
                when (contentType) {
                    //News, Company
                    "1", "3" -> {
                        when {
                            et_mobile.checkEmpty() -> {
                                mainLayout.showSnakeBar(getString(R.string.enter_mobile_number))
                                et_mobile.requestFocus()
                                return false
                            }
                            !et_mobile.checkFixLength(8) -> {
                                mainLayout.showSnakeBar(getString(R.string.mobile_character))
                                et_mobile.requestFocus()
                                return false
                            }
                        }
                    }
                    //Occasion
                    "2" -> {
                        when {
                            tv_date.checkEmpty() -> {
                                mainLayout.showSnakeBar(getString(R.string.enter_date))
                                tv_date.requestFocus()
                                return false
                            }
                            !et_mobile.checkEmpty() -> {
                                when {
                                    !et_mobile.checkFixLength(8) -> {
                                        mainLayout.showSnakeBar(getString(R.string.mobile_character))
                                        et_mobile.requestFocus()
                                        return false
                                    }
                                }
                            }
                            et_request_mobile.checkEmpty() -> {
                                mainLayout.showSnakeBar(getString(R.string.enter_request_mobile))
                                et_request_mobile.requestFocus()
                                return false
                            }
                            !et_request_mobile.checkFixLength(8) -> {
                                mainLayout.showSnakeBar(getString(R.string.mobile_character))
                                et_request_mobile.requestFocus()
                                return false
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ContentActivity.Companion.FRAGMENT_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
            }
        } else if (requestCode == Config.RC_PICK_IMAGES && resultCode == Activity.RESULT_OK && data != null) {
            imagesCameraGallery = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES)
            if (imagesCameraGallery != null) {
                for (uri in imagesCameraGallery) {

                    //mMedia.add(uri);
                    if (isEdit) {
                        arrayListPhotoEdit.add(uri?.path)
                    } else {
                        arrayListPhoto.add(uri?.path)
                    }
                }
                imageCount = if (isEdit) {
                    val gson = Gson()
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                    setEditImages(arrayListPhotoEdit.size)
                    arrayListPhotoEdit.size
                } else {
                    val gson = Gson()
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                    setImages(arrayListPhoto.size)
                    arrayListPhoto.size
                }
            }

            //do something
        }
        /*else if (requestCode == 3 && data!=null) {

            Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

            if (parcelableUris == null) {
                return;
            }

            // Java doesn't allow array casting, this is a little hack
            Uri[] uris = new Uri[parcelableUris.length];
            Log.e("uris size ",""+uris.length);
            //mimg_deletePhoto1.setText(""+uris.length);
            System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

            if (uris != null) {
                for (Uri uri : uris) {
                    Log.e(TAG, " uri: " + uri.toString());
                    //mMedia.add(uri);
                    Log.d("uri", ""+uri.getPath());
                    if(isEdit){
                        arrayListPhotoEdit.add(uri.getPath());
                    }
                    else{
                        arrayListPhoto.add(uri.getPath());
                    }

                }
                if(isEdit){
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit));
                    setEditImages(arrayListPhotoEdit.size());
                    imageCount = arrayListPhotoEdit.size();
                }
                else{
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto));
                    setImages(arrayListPhoto.size());
                    imageCount = arrayListPhoto.size();
                }

            }
        }*/
        else if (requestCode == Constants.KEY_RESULT_CODE) {
            try {
                if (isRequestPhone) {
                    countryNameRequest = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                    countryCodeRequest = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                    tv_country_code_request.text = String.format("(%s) %s", countryNameRequest, countryCodeRequest)
                    countryFlagRequest = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                    iv_country_flag_request.setImageResource(countryFlagRequest)
                } else {
                    countryName = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                    countryCode = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                    tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
                    countryFlag = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                    iv_country_flag.setImageResource(countryFlag)
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
                relative_image1.setVisibility(View.GONE)
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            1 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            2 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            3 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhoto.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select3)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            4 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhoto.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select3)
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhoto.get(3))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(3))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select4)
                relative_image5.setVisibility(View.GONE)
            }
            5 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhoto.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select3)
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhoto.get(3))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(3))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select4)
                relative_image5.setVisibility(View.VISIBLE)
                img_select5.setTag(arrayListPhoto.get(4))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(4))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select5)
            }
        }
    }

    private fun uploadPhotos() {
        /*multipartTypedOutput.addPart("FileObjectType",
                if (contentType.equals("1", ignoreCase = true)) TypedString("news")
                else if (contentType.equals("2", ignoreCase = true)) TypedString("events") else TypedString("company"))*/
        textPart = when (contentType) {
            "1" -> "news".createPartFromString()
            "2" -> "events".createPartFromString()
            "3" -> "company".createPartFromString()
            else -> "".createPartFromString()
        }

        SaveAndGetImageTask().execute(BitmapFactory.decodeFile(current_path))
    }

    private fun uploadPhotosAPI() {
        QenaatAPICall.getCallingAPIInterface()?.uploadImage(textPart, imagePartList)?.enqueue(
                object : Callback<java.util.ArrayList<UploadFile?>?> {

                    override fun onFailure(call: Call<ArrayList<UploadFile?>?>, t: Throwable) {
                        t.printStackTrace()
                        GlobalFunctions.EnableLayout(mainLayout)
                        mloading?.setVisibility(View.GONE)
                        mainLayout?.showSnakeBar(t?.message!!)
                    }

                    override fun onResponse(call: Call<ArrayList<UploadFile?>?>, response: Response<ArrayList<UploadFile?>?>) {
                        GlobalFunctions.EnableLayout(mainLayout)
                        if (response.body() != null) {
                            val uploadFileList = response.body()

                            var isSuccess = false
                            for (i in uploadFileList?.indices!!) {
                                if (uploadFileList[i]?.Type == 1) {
                                    isSuccess = true
                                    Log.e("imageUrl", uploadFileList[i]?.FileUrl)
                                } else {
                                    isSuccess = false
                                    Log.e("imageUrlError", uploadFileList[i]?.error)
                                    mainLayout?.showSnakeBar(uploadFileList[i]?.error!!)
                                }
                            }
                            Log.e("imageUrl", images.toString())
                            Log.e("photo upload", isSuccess.toString())
                            if (isSuccess) SendNews()
                            mloading?.setVisibility(View.GONE)
                        }
                    }
                })
    }

    //Save image in hidden folder and get it...
    internal inner class SaveAndGetImageTask() : AsyncTask<Bitmap?, Void?, File?>() {
        protected override fun doInBackground(vararg params: Bitmap?): File? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/.Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            val currentTime = "img_" + Calendar.getInstance().timeInMillis + ".png"
            images?.append(currentTime)
            /*  if (images?.isNotEmpty()!!)
                  images!!.append(",")*/
            val file = File(dir, currentTime)
            val fOut: FileOutputStream
            fOut = FileOutputStream(file)
            params[0]?.compress(Bitmap.CompressFormat.PNG, 60, fOut)
            fOut.flush()
            fOut.close()
            return file
        }

        override fun onPostExecute(file: File?) {
            super.onPostExecute(file)

            //multipartTypedOutput.addPart("File", TypedFile("image/*", file))
            imagePartList?.add(createPartFromFile("File", file!!)!!)
            uploadPhotosAPI()
        }
    }

//    private fun makeFile(): TypedFile? {
//        Log.e("currentPath", current_path)
//        // this will make file which is required by Retrofit.
//
//        val pos: Int = current_path.lastIndexOf("/")
//        val pathWithoutName: String = current_path.substring(0, pos)
//
//        val filename = current_path.substring(current_path.lastIndexOf('/') + 1)
//
//        val currentTime = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
//
//        //Replace file name
//        val fromFile = File(pathWithoutName, filename)
//        val toFile = File(pathWithoutName, currentTime)
//        fromFile.renameTo(toFile)
//
//        images.append(currentTime)
//
//        return TypedFile("image/*", toFile)
//    }

    internal inner class UploadImagesAysn : AsyncTask<String?, String?, String?>() {
        override fun onPostExecute(paramString: String?) {
            Log.d("image updates", "" + index + " image uploaded -> " + index + 1 + " image started")
            index = index + 1
            if (isEdit) {
                if (index < arrayListPhotoEdit.size) {
                    current_path = arrayListPhotoEdit.get(index)!!
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    SendNews()
                }
            } else {
                if (index < arrayListPhoto.size) {
                    current_path = arrayListPhoto.get(index)!!
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    SendNews()
                }
            }
        }

        override fun doInBackground(vararg params: String?): String? {

            /*String PhotoName = GlobalFunctions.SendMultipartFile(
                    current_path, getString(R.string.UploadContentPhoto), ".jpg");*/
            if (!current_path.contains("http://otabi.")) {
                var file: File? = null
                val myBitmap: Bitmap? = null
                var filename = ""
                var angle = 0
                val ei: ExifInterface
                try {
                    ei = ExifInterface(current_path)
                    val orientation = ei.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, 1)
                    when (orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> angle = 90
                        ExifInterface.ORIENTATION_ROTATE_180 -> angle = 180
                        ExifInterface.ORIENTATION_ROTATE_270 -> angle = 270
                    }
                } catch (e1: IOException) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace()
                }
                if (isEdit) {
//                    filename = getNameOfImage(current_path
//                    );
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    Log.d("str1", "0-> " + images.toString())
                    val str1 = images.toString().replace(getNameOfImage(current_path)!!, filename)
                    Log.d("str1", "1-> " + getNameOfImage(current_path))
                    Log.d("str1", "2-> $filename")
                    images = StringBuilder()
                    images.append(str1)
                    Log.d("str1", "3-> " + images.toString())
                } else {
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    images.append(filename)
                    if (index != arrayListPhoto.size - 1) {
                        images.append(",")
                    }
                }
                file = File(current_path)

                /*myBitmap = decodeSampledBitmapFromPath(file.getAbsolutePath(),
                        1000, 1000);

                Matrix matrix = new Matrix();
                matrix.postRotate(angle);
                myBitmap = Bitmap
                        .createBitmap(myBitmap, 0, 0, myBitmap.getWidth(),
                                myBitmap.getHeight(), matrix, true);

                OutputStream os;
                try {
                    os = new FileOutputStream(current_path);
                    myBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
                var conn: HttpURLConnection? = null
                var dos: DataOutputStream? = null
                var inStream: DataInputStream? = null
                val lineEnd = "\r\n"
                val twoHyphens = "--"
                val boundary = "*****"
                var bytesRead: Int
                var bytesAvailable: Int
                var bufferSize: Int
                val buffer: ByteArray
                val maxBufferSize = 1 * 1024 * 1024
                try {
                    val fileInputStream = FileInputStream(Compressor(act).compressToFile(file))
                    //once check url correct or not
                    val urlStr = QenaatConstant.Photo_URL +
                            if (contentType.equals("1", ignoreCase = true)) "news" else if (contentType.equals("2", ignoreCase = true)) "events" else "company"
                    val url = URL(urlStr)
                    Log.e("urlStr", urlStr)
                    conn = url.openConnection() as HttpURLConnection
                    conn.doInput = true
                    conn.doOutput = true
                    conn.requestMethod = "POST"
                    conn.useCaches = false
                    conn.setRequestProperty("Connection", "Keep-Alive")
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=$boundary")
                    dos = DataOutputStream(conn.outputStream)
                    dos.writeBytes(twoHyphens + boundary + lineEnd)
                    dos.writeBytes("Content-Disposition: form-data; name=\"pic\";"
                            + " filename=\"" + filename + "\"" + lineEnd)
                    dos.writeBytes(lineEnd)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    buffer = ByteArray(bufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize)
                        bytesAvailable = fileInputStream.available()
                        bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    }
                    dos.writeBytes(lineEnd)
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)
                    fileInputStream.close()
                    dos.flush()
                    dos.close()
                } catch (ex: MalformedURLException) {
                    println("Error:$ex")
                } catch (ioe: IOException) {
                    println("Error:$ioe")
                }
                try {
                    inStream = DataInputStream(conn?.getInputStream())
                    var str: String?
                    while (inStream.readLine().also { str = it } != null) {
                        println(str)
                        Log.e("image response", str)
                    }
                    inStream.close()
                } catch (ioex: IOException) {
                    println("Error: $ioex")
                }
            }
            return "1"
        }
    }

    private fun getNamesOfImages() {
        images = StringBuilder()
        if (isEdit) {
            for (i in arrayListPhotoEdit.indices) {
                val file = File(arrayListPhotoEdit.get(i))
                //Log.d("file name", ""+FilenameUtils.getBaseName(file.getName()));
                images.append(getNameOfImage(arrayListPhotoEdit.get(i)))
                //images.append(FilenameUtils.getBaseName(file.getName()));
                if (i != arrayListPhotoEdit.size - 1) {
                    images.append(",")
                }
                Log.e("images", images.toString())
            }
        } else {
            /*for (int i = 0; i < arrayListPhoto.size(); i++) {

                File file = new File(arrayListPhoto.get(i));
                //Log.d("file name", ""+FilenameUtils.getBaseName(file.getName()));

                images.append(getNameOfImage(arrayListPhoto.get(i)));
                //images.append(FilenameUtils.getBaseName(file.getName()));

                if (!(i == arrayListPhoto.size() - 1)) {
                    images.append(",");
                }

                Log.e("images", images.toString());
            }*/
        }
    }

    // method to get the name of the image from the path
    fun getNameOfImage(path: String?): String? {
        val index = path?.lastIndexOf('/')
        return path?.substring(index!! + 1)
    }

    fun decodeSampledBitmapFromPath(path: String?, reqWidth: Int,
                                    reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(path, options)
    }

    fun calculateInSampleSize(options: BitmapFactory.Options,
                              reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            inSampleSize = if (width > height) {
                Math.round(height as Float / reqHeight as Float)
            } else {
                Math.round(width as Float / reqWidth as Float)
            }
        }
        return inSampleSize
    }


    private fun SendNews() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        when (contentType) {
            //News
            "1" -> {
                QenaatAPICall.getCallingAPIInterface()?.SendNews(
                        "$AUTH_TEXT ${mSessionManager?.getAuthToken()}",
                        catId,
                        et_title.getText().toString(),
                        et_title.getText().toString(),
                        et_title.getText().toString(),
                        et_title.getText().toString(),
                        et_details_label.getText().toString(),
                        et_details_label.getText().toString(),
                        images.toString(),
                        et_facebook.getText().toString(),
                        et_instagram.getText().toString(),
                        et_twitter.getText().toString(),
                        et_youTube.getText().toString(),
                        ContentActivity.Companion.latitude,
                        ContentActivity.Companion.longitude,
                        et_mobile.getText().toString(),
                        if (et_mobile.checkEmpty()) null else countryCode,
                        mSessionManager?.getUserCode())?.enqueue(
                        object : Callback<ResponseBody?> {

                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                t.printStackTrace()
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                mloading.visibility = View.GONE
                                GlobalFunctions.EnableLayout(mainLayout)
                                val body = response?.body()
                                var outResponse = ""
                                try {
                                    val reader = BufferedReader(InputStreamReader(
                                            ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                if (outResponse != null) {
                                    outResponse = outResponse.replace("\"", "")
                                    outResponse = outResponse.replace("\n", "")
                                    Log.e("outResponse not null ", outResponse)
                                    when {
                                        outResponse.toInt() > 0 -> {
                                            mainLayout.showSnakeBar(getString(R.string.ContentAddedLabel))
                                            fragmentManager?.popBackStack()
                                        }
                                        outResponse == "-1" -> {
                                            Toast.makeText(act, act.getString(R.string.OperationFailed), Toast.LENGTH_LONG).show()
                                        }
                                        outResponse == "-2" -> {
                                            Toast.makeText(act, act.getString(R.string.DataMissing), Toast.LENGTH_LONG).show()
                                        }
                                    }
                                }
                            }
                        })
            }
            //Occasion
            "2" -> {
                QenaatAPICall.getCallingAPIInterface()?.SendEvent(
                        "$AUTH_TEXT ${mSessionManager?.getAuthToken()}",
                        GlobalFunctions.EncodeParameter(catId),
                        GlobalFunctions.EncodeParameter(tv_date.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_title.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_title.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_full_name.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_request_mobile.getText().toString()),
                        countryCodeRequest,
                        GlobalFunctions.EncodeParameter(images.toString()),
                        GlobalFunctions.EncodeParameter(et_facebook.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_instagram.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_twitter.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_youTube.getText().toString()),
                        GlobalFunctions.EncodeParameter(ContentActivity.Companion.latitude),
                        GlobalFunctions.EncodeParameter(ContentActivity.Companion.longitude),
                        GlobalFunctions.EncodeParameter(et_mobile.getText().toString()),
                        if (et_mobile.checkEmpty()) null else countryCode,
                        GlobalFunctions.EncodeParameter(et_details_label.getText().toString()),
                        GlobalFunctions.EncodeParameter(et_details_label.getText().toString()),
                        mSessionManager?.getUserCode(),
                        if (myInvitationId.isNotEmpty())
                            GlobalFunctions.EncodeParameter(myInvitationId)
                        else GlobalFunctions.EncodeParameter(otherInvitationId))?.enqueue(
                        object : Callback<ResponseBody?> {

                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                t.printStackTrace()
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                val body = response?.body()
                                var outResponse = ""
                                try {
                                    val reader = BufferedReader(InputStreamReader(
                                            ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                if (outResponse != null) {
                                    outResponse = outResponse.replace("\"", "")
                                    outResponse = outResponse.replace("\n", "")
                                    Log.e("outResponse not null ", outResponse)
                                    when {
                                        outResponse.toInt() > 0 -> {
                                            mainLayout.showSnakeBar(getString(R.string.ContentAddedLabel))
                                            fragmentManager?.popBackStack()
                                        }
                                        outResponse == "-1" -> {
                                            mainLayout.showSnakeBar(getString(R.string.OperationFailed))
                                        }
                                        outResponse == "-2" -> {
                                            mainLayout.showSnakeBar(getString(R.string.DataMissing))
                                        }
                                    }
                                }
                            }
                        })
            }
            //Company
            "3" -> {
                QenaatAPICall.getCallingAPIInterface()?.SendCompanies(
                        "$AUTH_TEXT ${mSessionManager?.getAuthToken()}",
                        catId,
                        et_title.getText().toString(),
                        et_title.getText().toString(),
                        et_details_label.getText().toString(),
                        et_details_label.getText().toString(),
                        images.toString(),
                        et_facebook.getText().toString(),
                        et_twitter.getText().toString(),
                        et_instagram.getText().toString(),
                        et_youTube.getText().toString(),
                        ContentActivity.Companion.latitude,
                        ContentActivity.Companion.longitude,
                        et_mobile.getText().toString(),
                        countryCode,
                        mSessionManager?.getUserCode(),
                        if (isQenaatFamily) "true" else "false",
                        "a@a.com",
                            "123456")?.enqueue(
                        object : Callback<ResponseBody?> {

                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                t.printStackTrace()
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                val body = response?.body()
                                var outResponse = ""
                                try {
                                    val reader = BufferedReader(InputStreamReader(
                                            ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                if (outResponse != null) {
                                    outResponse = outResponse.replace("\"", "")
                                    outResponse = outResponse.replace("\n", "")
                                    Log.e("outResponse not null ", outResponse)
                                    if (Integer.valueOf(outResponse) > 0) {
                                        mainLayout.showSnakeBar(getString(R.string.ContentAddedLabel))
                                        fragmentManager?.popBackStack()
                                    } else if (outResponse == "-1") {
                                        Toast.makeText(act, act.getString(R.string.OperationFailed), Toast.LENGTH_LONG).show()
                                    } else if (outResponse == "-2") {
                                        Toast.makeText(act, act.getString(R.string.DataMissing), Toast.LENGTH_LONG).show()
                                    }
                                }
                            }
                        })
            }
        }
    }

    private fun setEditImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
                relative_image1.setVisibility(View.GONE)
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            1 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            2 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            3 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhotoEdit.get(2))
                if (arrayListPhotoEdit.get(2)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                }
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            4 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhotoEdit.get(2))
                if (arrayListPhotoEdit.get(2)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                }
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhotoEdit.get(3))
                if (arrayListPhotoEdit.get(3)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                }
                relative_image5.setVisibility(View.GONE)
            }
            5 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhotoEdit.get(2))
                if (arrayListPhotoEdit.get(2)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                }
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhotoEdit.get(3))
                if (arrayListPhotoEdit.get(3)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                }
                relative_image5.setVisibility(View.VISIBLE)
                img_select5.setTag(arrayListPhotoEdit.get(4))
                if (arrayListPhotoEdit.get(4)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(4))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select5)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(4))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select5)
                }
            }
        }
    }

    private fun GetTrees() {
        mloading.setVisibility(View.VISIBLE)

        /*OtabiAPICall.getCallingAPIInterface().GetTrees("0", mSessionManager.getUserCode(), new Callback<List<GetTrees>>() {
            @Override
            public void success(final List<GetTrees> trees, retrofit.client.Response response) {

                if(trees != null){

                    getTreesArrayList.clear();

                    getTreesArrayList.addAll(trees);

                }

                showTreeDialog();

                mloading.setVisibility(View.INVISIBLE);

            }

            @Override
            public void failure(RetrofitError error) {

                mloading.setVisibility(View.INVISIBLE);

            }

        });*/
    }

    private fun saveData() {
        if (tv_category_label.getText().length > 0) {
            ContentActivity.Companion.catName = tv_category_label.getText().toString()
            ContentActivity.Companion.catId = catId
        }
        if (tv_my_invitation_list_label.getText().length > 0) {
            ContentActivity.Companion.myInvitationName = tv_my_invitation_list_label.getText().toString()
            ContentActivity.Companion.myInvitationId = myInvitationId
        }
        if (tv_others_list_label.getText().length > 0) {
            ContentActivity.Companion.otherInvitationName = tv_others_list_label.getText().toString()
            ContentActivity.Companion.otherInvitationId = otherInvitationId
        }
        if (tv_tree_label.getText().length > 0) {
            ContentActivity.Companion.treeName = tv_tree_label.getText().toString()
            ContentActivity.Companion.treeId = treeId
        }
        if (et_title.getText().length > 0) {
            ContentActivity.Companion.title = et_title.getText().toString()
        }
        if (et_full_name.getText().length > 0) {
            ContentActivity.Companion.fullName = et_full_name.getText().toString()
        }
        if (et_mobile.getText().length > 0) {
            ContentActivity.Companion.mobile = et_mobile.getText().toString()
        }
        if (et_request_mobile.getText().length > 0) {
            ContentActivity.Companion.request_mobile = et_request_mobile.getText().toString()
        }
        if (tv_location_label.getText().length > 0) {
            ContentActivity.Companion.location = tv_location_label.getText().toString()
        }
        if (et_details_label.getText().length > 0) {
            ContentActivity.Companion.details = et_details_label.getText().toString()
        }
        if (tv_date.getText().length > 0) {
            ContentActivity.Companion.date = tv_date.getText().toString()
        }
        if (et_youTube.getText().length > 0) {
            ContentActivity.Companion.youTube = et_youTube.getText().toString()
        }
        if (et_facebook.getText().length > 0) {
            ContentActivity.Companion.facebook = et_facebook.getText().toString()
        }
        if (et_instagram.getText().length > 0) {
            ContentActivity.Companion.instagram = et_instagram.getText().toString()
        }
        if (et_twitter.getText().length > 0) {
            ContentActivity.Companion.twitter = et_twitter.getText().toString()
        }
    }

    private val listener: SlideDateTimeListener? = object : SlideDateTimeListener() {
        override fun onDateTimeSet(date: Date?) {
            tv_date.setText(mFormatter.format(date).toString())
            ContentActivity.Companion.date = mFormatter.format(date).toString()
        }

        // Optional cancel listener
        override fun onDateTimeCancel() {}
    }

    private fun getCategories() {
        mloading.setVisibility(View.VISIBLE)
        if (contentType.equals("1", ignoreCase = true)) {
            QenaatAPICall.getCallingAPIInterface()?.GetNewsTypes()?.enqueue(
                    object : Callback<ArrayList<GetNewsTypes?>?> {

                        override fun onFailure(call: Call<ArrayList<GetNewsTypes?>?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.INVISIBLE)
                        }

                        override fun onResponse(call: Call<ArrayList<GetNewsTypes?>?>, response: Response<ArrayList<GetNewsTypes?>?>) {
                            if (response.body() != null) {
                                val getCategories = response.body()
                                if (getCategories != null) {
                                    getNewsTypesArrayList.clear()
                                    getNewsTypesArrayList.addAll(getCategories)
                                }
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    })
        } else if (contentType.equals("2", ignoreCase = true)) {
            QenaatAPICall.getCallingAPIInterface()?.GetEventTypes()?.enqueue(
                    object : Callback<ArrayList<GetEventTypes?>?> {

                        override fun onFailure(call: Call<ArrayList<GetEventTypes?>?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.INVISIBLE)
                        }

                        override fun onResponse(call: Call<ArrayList<GetEventTypes?>?>, response: Response<ArrayList<GetEventTypes?>?>) {
                            if (response.body() != null) {
                                val getCategories = response.body()
                                if (getCategories != null) {
                                    getEventTypesArrayList.clear()
                                    getEventTypesArrayList.addAll(getCategories)
                                }
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    })
        } else if (contentType.equals("3", ignoreCase = true)) {
            QenaatAPICall.getCallingAPIInterface()?.GetCompanyCategories()?.enqueue(
                    object : Callback<ArrayList<GetCompanyCategories?>?> {

                        override fun onFailure(call: Call<ArrayList<GetCompanyCategories?>?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.INVISIBLE)
                        }

                        override fun onResponse(call: Call<ArrayList<GetCompanyCategories?>?>, response: Response<ArrayList<GetCompanyCategories?>?>) {
                            if (response.body() != null) {

                                val getCategories = response.body()
                                if (getCategories != null) {
                                    getCompanyCategoriesArrayList.clear()
                                    getCompanyCategoriesArrayList.addAll(getCategories)
                                }
                                mloading.setVisibility(View.INVISIBLE)
                            }
                        }
                    })
        }
    }

    private fun GetInvitationLists(isMyList: String?) {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetInvitationLists(
                "$AUTH_TEXT ${mSessionManager?.getAuthToken()}",
                mSessionManager?.getUserCode(),
                isMyList,
                "-1")?.enqueue(
                object : Callback<ArrayList<GetInvitationLists>?> {

                    override fun onFailure(call: Call<ArrayList<GetInvitationLists>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetInvitationLists>?>, response: Response<ArrayList<GetInvitationLists>?>) {
                        if (response.body() != null) {
                            val getCategories = response.body()
                            if (getCategories != null) {
                                if (isMyList.equals("true", ignoreCase = true)) {
                                    myInvitationListsArrayList.clear()
                                    myInvitationListsArrayList.addAll(getCategories)
                                } else {
                                    otherInvitationListsArrayList.clear()
                                    otherInvitationListsArrayList.addAll(getCategories)
                                }
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                    }
                })
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.MULTIPLE_PERMISSION_CODE -> {
                    var imageCount = 1
                    imageCount = if (contentType.equals("1", ignoreCase = true)) {
                        1
                    } else {
                        1
                    }
                    saveData()
                    if (mLangSessionManager.getLang() == "ar") {
                        Toast.makeText(act, act.getString(R.string.SlideLeft), Toast.LENGTH_LONG).show()
                    }
                    if (isEdit) {
                        if (arrayListPhotoEdit.size < imageCount) {
                            ImagePicker.with(this)
                                    .setFolderMode(false)
                                    .setCameraOnly(false)
                                    .setFolderTitle(act.getString(R.string.GalleryLabel))
                                    .setMultipleMode(true)
                                    //.setSelectedImages(config.getSelectedImages())
                                    .setMaxSize(imageCount - arrayListPhotoEdit.size)
                                    .start()
                        }
                    } else {
                        if (arrayListPhoto.size < imageCount) {
                            ImagePicker.with(this)
                                    .setFolderMode(false)
                                    .setCameraOnly(false)
                                    .setFolderTitle(act.getString(R.string.GalleryLabel))
                                    .setMultipleMode(true)
                                    //.setSelectedImages(config.getSelectedImages())
                                    .setMaxSize(imageCount - arrayListPhoto.size)
                                    .start()
                        }
                    }
                }
                GlobalFunctions.LOCATION_PERMISSION_CODE -> {
                    ContentActivity.openCurrentLocationFragmentFadeIn()
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act!!, permissionStr))
                mainLayout?.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act!!)
        }
    }

    private fun showTreeDialog() {

        /*if(getTreesArrayList.size()>0){
            LayoutInflater inflater1 = (LayoutInflater) act
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v1 = inflater1.inflate(R.layout.country_dialog,null);

            ListView listView1 = (ListView)v1.findViewById(R.id.lv);
            TextView tv_title1 = (TextView)v1.findViewById(R.id.tv_title);
            tv_title1.setText(act.getString(R.string.SelectFamily));
            tv_title1.setTypeface(ContentActivity.tf);

            final ArrayList<String> arrayList1 = new ArrayList<>();
            for (GetTrees trees : getTreesArrayList){
                if(mLangSessionManager.getLang().equals("en")){
                    arrayList1.add(trees.getCompanyCategoryNameEn());
                }
                else{
                    arrayList1.add(trees.getCompanyCategoryNameAr());
                }
            }
            arrayList1.add(act.getString(R.string.CancelLabel));
            AlertAdapter alertAdapter1 = new AlertAdapter(act,arrayList1);
            listView1.setAdapter(alertAdapter1);
            FixControl.setListViewHeightBasedOnChildren(listView1);

            final MaterialDialog alert1 = new MaterialDialog(act).setContentView(v1);
            alert1.show();
            alert1.setCanceledOnTouchOutside(true);

            listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    alert1.dismiss();
                    String item = arrayList1.get(i);

                    if(item.equalsIgnoreCase(act.getString(R.string.CancelLabel))){

                    }
                    else {
                        treeId = getTreesArrayList.get(i).getRequestForHelpId();
                        ContentActivity.treeId = treeId;
                        tv_tree_label.setText(item);
                        ContentActivity.treeName = item;
                    }
                }
            });

        }*/
    }

//    private fun setImagePickerConfig() {
//        config = Config()
//        config.setCameraOnly(false)
//        config.setMultipleMode(true)
//        config.setFolderMode(false)
//        config.setShowCamera(true)
//        config.setMaxSize(Config.MAX_SIZE)
//        config.setDoneTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_action_done))
//        config.setFolderTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_title_folder))
//        config.setImageTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_title_image))
//        config.setSavePath(SavePath.DEFAULT)
//        config.setSelectedImages(ArrayList())
//    }

    companion object {
        protected val TAG = AddNewsFragment::class.java.simpleName
        var fragment: AddNewsFragment? = null
        var mSessionManager: SessionManager? = null
        var bitmaps: ArrayList<Bitmap?>? = ArrayList()
        private const val PERMISSION_CODE = 23
        private const val INTENT_REQUEST_GET_IMAGES = 13
        fun newInstance(act: FragmentActivity?): AddNewsFragment {
            fragment = AddNewsFragment()
            fragment!!.act = act!!
            return fragment!!
        }
    }
}