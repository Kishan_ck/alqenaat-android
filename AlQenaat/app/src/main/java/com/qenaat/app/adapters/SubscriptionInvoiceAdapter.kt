package com.qenaat.app.adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.demo.alertDialog
import com.qenaat.app.fragments.SubscriptionInvoiceFragment
import com.qenaat.app.model.GetMonthlyDonationSubscriptionInvoices
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 05-Dec-17.
 */
class SubscriptionInvoiceAdapter(var act: FragmentActivity,
                                 private val itemsData: ArrayList<GetMonthlyDonationSubscriptionInvoices> , val connenct : Connenct) : RecyclerView.Adapter<SubscriptionInvoiceAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager

    interface Connenct{
        fun pay(position: Int);
        fun cancel(position: Int);
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.monthly_donation_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.tv_details.setText(act.getString(R.string.PayLabel))
            viewHolder.tv_details.setVisibility(View.VISIBLE)
            viewHolder.tv_day.setVisibility(View.VISIBLE)
//            viewHolder.tv_area.setText(itemsData.get(position).Amount + " " + act.getString(R.string.KD))
            viewHolder.tv_area.text = itemsData.get(position).Amount + " " + act.getString(R.string.KD)
            if (itemsData.get(position).IsPaid.equals("true", ignoreCase = true) ||
                    itemsData.get(position).Canceled.equals("true", ignoreCase = true)) {
                viewHolder.tv_details.setVisibility(View.GONE)
                viewHolder.tv_day.setVisibility(View.GONE)
            }
            if (itemsData.get(position).IsPaid.equals("true", ignoreCase = true)) {
                viewHolder.tv_area.setText("""${act.getString(R.string.Paid)} ${itemsData.get(position).Amount} ${act.getString(R.string.KD)}""")
            }
            val originalFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
            val targetFormat: DateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.ENGLISH)
            var date: Date? = null
            try {
                date = originalFormat.parse(itemsData.get(position).InvoceDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val formattedDate = targetFormat.format(date) // 20120821
            viewHolder.tv_title.setText(formattedDate)
            viewHolder.tv_day.setOnClickListener(View.OnClickListener {
                AlertDialog.Builder(act)
                        .setTitle(act.getString(R.string.Confirm))
                        .setMessage(act.getString(R.string.CancelSubscriptionLabel))
                        .setPositiveButton(android.R.string.yes) { dialog, which -> connenct.cancel(position)
                         }
                        .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
                        .setIcon(R.drawable.icon_512)
                        .show()
            })

//            var HI_REQUEST_CODE_DAY=9011

            viewHolder.tv_details.setOnClickListener(View.OnClickListener {
           connenct.pay(position);

//           act.startActivityForResult(Intent(act, alertDialog::class.java), HI_REQUEST_CODE_DAY)

//                var intent = Intent(act, alertDialog::class.java)
//                intent.putExtra("amount",viewHolder.tv_area.text)
//                intent.putExtra("paymentID",itemsData.get(position).Id)
//                act.startActivity(intent)

                Log.e("paymentamounttttt",""+viewHolder.tv_area.text )
                Log.e("paymentamounttttt",""+itemsData.get(position).Id )
                Log.e("paymentclickkk","1>>>>>")
//                SubscriptionInvoiceFragment.Companion.AddMonthlyDonationInvoice(itemsData.get(position))

            })

            if (itemsData.get(position).Canceled.equals("true", ignoreCase = true)) {
                viewHolder.tv_area.setText("""${act.getString(R.string.CancelledLabel)} ${itemsData.get(position).Amount} ${act.getString(R.string.KD)}""".trimMargin())
            }
        }
    }


    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var tv_area: TextView
        var tv_day: TextView
        var tv_title: TextView
        var tv_details: TextView
        var tv_status: TextView

        init {
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            tv_status = itemLayoutView.findViewById<View?>(R.id.tv_status) as TextView
            tv_day = itemLayoutView.findViewById<View?>(R.id.tv_day) as TextView
            tv_area = itemLayoutView.findViewById<View?>(R.id.tv_area) as TextView
            tv_details = itemLayoutView.findViewById<View?>(R.id.tv_details) as TextView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_content)

        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}