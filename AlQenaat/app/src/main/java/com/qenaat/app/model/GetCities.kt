package com.qenaat.app.model

/**
 * Created by shahbazshaikh on 02/08/16.
 */
class GetCities {
    var Id: String? = null
    var NameEN: String? = null
    var NameAR: String? = null
    var CountryId: String? = null
    var CountryAR: String? = null
    var CountryEN: String? = null
    var IsSelected: String? = null
}