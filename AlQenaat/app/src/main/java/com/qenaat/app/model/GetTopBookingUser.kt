package com.qenaat.app.model

/**
 * Created by DELL on 28-Dec-16.
 */
class GetTopBookingUser {
    var Id: String? = null
    var BookingNo: String? = null
    var HallId: String? = null
    var HallEn: String? = null
    var HallAr: String? = null
    var RequestFullName: String? = null
    var RequestPhone: String? = null
    var EventDate: String? = null
    var EventDateEn: String? = null
    var EventDateAr: String? = null
    var EventDetails: String? = null
    var BookingStatus: String? = null
}