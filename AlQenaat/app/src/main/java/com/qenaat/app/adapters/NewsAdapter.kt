package com.qenaat.app.adapters

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetEvents
import com.qenaat.app.model.GetNews
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by DELL on 31-Oct-17.
 */
class NewsAdapter(var act: FragmentActivity, private val itemsData: ArrayList<GetNews>,
                  getEventsArrayList: ArrayList<GetEvents>,
                  type: String) : RecyclerView.Adapter<RecyclerView.ViewHolder?>() {

    var languageSeassionManager: LanguageSessionManager
    var isHeaderLoaded = false
    var getEventsArrayList: ArrayList<GetEvents>
    var type: String = ""

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_HEADER) {
            val v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_header, parent, false)
            return HeaderViewHolder(v)
        } else if (viewType == TYPE_ITEM) {
            val v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_list_row, parent, false)
            return ViewHolder(v)
        } else {
            val v = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_header, parent, false)
            return HeaderViewHolder(v)
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        // - get data from your itemsData at this position

        // - replace the contents of the view with that itemsData
        if (viewHolder is HeaderViewHolder && !isHeaderLoaded) {
            Log.e("gggg!!!!!!!!!!!", "" + position);
            isHeaderLoaded = true
            val headerHolder = viewHolder as HeaderViewHolder
            headerHolder.relative_slider.setVisibility(View.VISIBLE)
            if (type.equals("news", ignoreCase = true)) {
                headerHolder.relative_slider.setVisibility(View.GONE)
            }
            val slidingImageAdapter = SlidingImageAdapter(act, getEventsArrayList)
            slidingImageAdapter.notifyDataSetChanged()
            headerHolder.view_pager.setAdapter(slidingImageAdapter)
            headerHolder.tabLayout.setupWithViewPager(headerHolder.view_pager, true)
            headerHolder.NUM_PAGES = getEventsArrayList.size
            //             Auto start of viewpager
            val handler = Handler()
            val Update = Runnable {
                if (headerHolder.currentPage == headerHolder.NUM_PAGES) {
                    headerHolder.currentPage = 0
                }
                headerHolder.view_pager.setCurrentItem(headerHolder.currentPage++, true)
            }
            headerHolder.swipeTimer = Timer()
            headerHolder.swipeTimer?.schedule(object : TimerTask() {
                override fun run() {
                    handler.post(Update)
                }
            }, 3000, 3000)
        } else if (viewHolder is ViewHolder) {
            Log.e("gggg@@@@@@@@@", "" + position);
            val genericViewHolder = viewHolder as ViewHolder
            if (itemsData.get(position - 1) != null) {
                genericViewHolder.relative_content.setVisibility(View.GONE)
                genericViewHolder.img_ad.setVisibility(View.GONE)
                if (itemsData.get(position - 1).Id.equals("-1", ignoreCase = true)) {
                    genericViewHolder.img_ad.setVisibility(View.VISIBLE)
                    genericViewHolder.img_ad.setImageResource(R.drawable.no_ing_list)
                    genericViewHolder.img_ad.getLayoutParams().width = (act.getResources().getDrawable(
                            R.drawable.ad) as BitmapDrawable).bitmap.width
                    genericViewHolder.img_ad.getLayoutParams().height = (act.getResources().getDrawable(
                            R.drawable.ad) as BitmapDrawable).bitmap.height
                    if (itemsData.get(position - 1).Photo?.length!! > 0) Picasso.with(act)
                            .load(itemsData.get(position - 1).Photo)
                            .error(R.drawable.ad)
                            .placeholder(R.drawable.ad)
                            .config(Bitmap.Config.RGB_565).fit()
                            .into(genericViewHolder.img_ad)
//                    genericViewHolder.img_ad.setOnClickListener(View.OnClickListener {
//                        if (itemsData.get(position - 1).DetailsEN?.length!! > 0 &&
//                                itemsData.get(position - 1).DetailsEN?.contains("http")!!) {
//                            act.startActivity(Intent(Intent.ACTION_VIEW,
//                                    Uri.parse(itemsData.get(position - 1).DetailsEN)))
//                        }
//                    })
                } else {
                    Log.e("gggg########", "" + position);
                    genericViewHolder.relative_content.setVisibility(View.VISIBLE)
                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                        genericViewHolder.tv_title.setText(itemsData.get(position - 1).HeadLineEN)
                        genericViewHolder.tv_category_name.setText(itemsData.get(position - 1).NewsCategoryEn)
                    } else {
                        genericViewHolder.tv_title.setText(itemsData.get(position - 1).HeadLineAR)
                        genericViewHolder.tv_category_name.setText(itemsData.get(position - 1).NewsCategoryAr)
                    }
                    genericViewHolder.tv_date.setText(itemsData.get(position - 1).AddedOn)
                    genericViewHolder.img_news.setImageResource(R.drawable.no_ing_list)
                    genericViewHolder.img_news.getLayoutParams().width = (act.getResources().getDrawable(
                            R.drawable.no_ing_list) as BitmapDrawable).bitmap.width
                    genericViewHolder.img_news.getLayoutParams().height = (act.getResources().getDrawable(
                            R.drawable.no_ing_list) as BitmapDrawable).bitmap.height
                    if (itemsData.get(position - 1).Photo?.length!! > 0) Picasso.with(act)
                            .load(itemsData.get(position - 1).Photo)
                            .error(R.drawable.no_ing_list)
                            .placeholder(R.drawable.no_ing_list)
                            .config(Bitmap.Config.RGB_565).fit()
                            .into(genericViewHolder.img_news)
                    if (itemsData.get(position - 1).Color?.length!! > 6) {
                        genericViewHolder.img_category_color.circleBackgroundColor = Color.parseColor(itemsData.get(position - 1).Color)
                        genericViewHolder.tv_category_name.setTextColor(Color.parseColor(itemsData.get(position - 1).Color))
                    }
                    genericViewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                        val gson = Gson()
                        val b = Bundle()
                        b.putString("GetNews", gson.toJson(itemsData.get(position - 1)))
                        ContentActivity.Companion.openNewsDetailsFragment(b)
                    })
                }
            }
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_news: ImageView
        var img_bg: ImageView
        var img_ad: ImageView
        var img_category_color: CircleImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var relative_category: RelativeLayout
        var tv_category_name: TextView
        var tv_title: TextView
        var tv_date: TextView

        init {
            img_ad = itemLayoutView.findViewById<View?>(R.id.img_ad) as ImageView
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            tv_category_name = itemLayoutView.findViewById<View?>(R.id.tv_category_name) as TextView
            tv_date = itemLayoutView.findViewById<View?>(R.id.tv_date) as TextView
            img_news = itemLayoutView.findViewById<View?>(R.id.img_news) as ImageView
            img_category_color = itemLayoutView.findViewById<View?>(R.id.img_category_color)
                    as CircleImageView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_category = itemLayoutView
                    .findViewById<View?>(R.id.relative_category) as RelativeLayout
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_content)
        }
    }

    internal inner class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var currentPage = 0
        var NUM_PAGES = 0
        var swipeTimer: Timer? = null
        var sliderPosition = 0
        var view_pager: ViewPager
        var tabLayout: TabLayout
        var relative_slider: RelativeLayout

        init {
            relative_slider = itemView.findViewById<View?>(R.id.relative_slider) as RelativeLayout
            tabLayout = itemView.findViewById<View?>(R.id.page_indicator) as TabLayout
            view_pager = itemView.findViewById<View?>(R.id.view_pager) as ViewPager
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size + 1
    }

    //    need to override this method
    override fun getItemViewType(position: Int): Int {
        return if (isPositionHeader(position)) {
            TYPE_HEADER
        } else TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    init {
        this.type = type
        this.getEventsArrayList = getEventsArrayList
        languageSeassionManager = LanguageSessionManager(act)
    }
}