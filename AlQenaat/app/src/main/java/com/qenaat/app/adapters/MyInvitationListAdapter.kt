package com.qenaat.app.adapters

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.fragments.MyInvitationListFragment
import com.qenaat.app.model.GetInvitationLists
import java.util.*

/**
 * Created by DELL on 11-Feb-18.
 */
class MyInvitationListAdapter(var act: FragmentActivity,
                              private val itemsData: ArrayList<GetInvitationLists>, var tabNumber: Int) : RecyclerView.Adapter<MyInvitationListAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager
    var imgH: Int = 0
    var imgW: Int = 0

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.tv_type.setVisibility(View.VISIBLE)
            viewHolder.img_delete.setVisibility(View.GONE)
            viewHolder.img_edit.setVisibility(View.GONE)
            if (tabNumber == 1) {
                viewHolder.tv_type.setVisibility(View.VISIBLE)
                viewHolder.img_delete.setVisibility(View.VISIBLE)
                viewHolder.img_edit.setVisibility(View.VISIBLE)
            }
            viewHolder.tv_type.setText(itemsData.get(position).Name)
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                //viewHolder.tv_name.setText(itemsData.get(position).getName() + " (" + itemsData.get(position).getPersonCount() + ")");
            } else {
                //viewHolder.tv_name.setText(itemsData.get(position).getName() + " (" + itemsData.get(position).getPersonCount() + ")");
            }
            if (itemsData.get(position).IsPublic.equals("true", ignoreCase = true)) {
                viewHolder.tv_name.setText(act.getString(R.string.Public) + " " + itemsData.get(position).PersonCount + " " +
                        act.getString(R.string.MembersLabel))
            } else {
                viewHolder.tv_name.setText(act.getString(R.string.Private) + " " + itemsData.get(position).PersonCount + " " +
                        act.getString(R.string.MembersLabel))
            }
            viewHolder.img_delete.setOnClickListener(View.OnClickListener {
                AlertDialog.Builder(act)
                        .setTitle(act.getString(R.string.Confirm))
                        .setMessage(act.getString(R.string.AreYouSureYouWantToDeleteInvitationLabel))
                        .setPositiveButton(android.R.string.yes) { dialog, which ->
                            Log.e("afterTextChanged", "update")
                            dialog.dismiss()
                            MyInvitationListFragment.Companion.deleteInvitation(itemsData.get(position).Id)
                        }
                        .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
                        .setIcon(R.drawable.icon_512)
                        .show()
            })
            viewHolder.img_edit.setOnClickListener(View.OnClickListener {
                if (tabNumber == 1) {
                    val gson = Gson()
                    ContentActivity.Companion.clearVariables()
                    val bundle = Bundle()
                    bundle.putString("isEdit", "true")
                    bundle.putString("GetInvitationLists", gson.toJson(itemsData.get(position)))
                    ContentActivity.Companion.openCreateMyListFragment(bundle)
                }
            })
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                //if(tabNumber==1){
                ContentActivity.Companion.clearVariables()
                var cityIdArray: Array<String?>? = null
                var areaIdArray: Array<String?>? = null
                var branchIdArray: Array<String?>? = null
                cityIdArray = arrayOfNulls<String?>(0)
                areaIdArray = arrayOfNulls<String?>(0)
                branchIdArray = arrayOfNulls<String?>(0)
                val bundle = Bundle()
                bundle.putStringArray("cityIdArray", cityIdArray)
                bundle.putStringArray("areaIdArray", areaIdArray)
                bundle.putStringArray("branchIdArray", branchIdArray)
                bundle.putString("gender", "0")
                bundle.putString("personType", "0")
                bundle.putString("ageFrom", "0")
                bundle.putString("ageTo", "0")
                bundle.putString("keyword", "")
                bundle.putString("comingFrom", "home")
                bundle.putInt("tabNumber", tabNumber)
                bundle.putString("invitationId", itemsData.get(position).Id)
                ContentActivity.Companion.openAddPersonToInvitationListFragment(bundle)
                // }
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var img_delete: ImageView
        var img_edit: ImageView
        var relative_parent: RelativeLayout
        var tv_name: TextView
        var tv_type: TextView

        init {
            tv_name = itemLayoutView.findViewById<View?>(R.id.tv_name) as TextView
            tv_type = itemLayoutView.findViewById<View?>(R.id.tv_type) as TextView
            img_edit = itemLayoutView.findViewById<View?>(R.id.img_edit) as ImageView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            img_delete = itemLayoutView.findViewById<View?>(R.id.img_delete) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_parent)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
        imgH = imgH
        imgW = imgW
    }
}