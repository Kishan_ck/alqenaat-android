package com.qenaat.app.classes

import android.graphics.*
import com.squareup.picasso.Transformation

/**
 * Created by DELL on 14-May-17.
 */
class RoundedCornersTransform // radius is corner radii in dp
// margin is the board in dp
(private val radius: Int, // dp
 private val margin: Int) : Transformation {

    override fun transform(source: Bitmap): Bitmap? {
        val paint = Paint()
        paint.isAntiAlias = true
        paint.shader = BitmapShader(source, Shader.TileMode.CLAMP,
                Shader.TileMode.CLAMP)
        val output = Bitmap.createBitmap(source.getWidth(),
                source.getHeight(), Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)
        canvas.drawRoundRect(RectF(margin.toFloat(), margin.toFloat(), (source.getWidth()
                - margin).toFloat(), (source.getHeight() - margin).toFloat()), radius.toFloat(), radius.toFloat(), paint)
        if (source != output) {
            source.recycle()
        }
        return output
    }

    override fun key(): String? {
        return "rounded"
    }

}