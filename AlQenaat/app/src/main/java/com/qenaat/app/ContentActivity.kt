package com.qenaat.app

import android.animation.ObjectAnimator
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.jdev.countryutil.Constants
import com.nostra13.universalimageloader.core.ImageLoader
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.LocaleHelper
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.fragments.*
import com.qenaat.app.interfaces.Interfaces
import com.qenaat.app.model.*
import com.qenaat.app.networking.QenaatAPICall
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.top_bar.*
import me.leolin.shortcutbadger.ShortcutBadgeException
import me.leolin.shortcutbadger.ShortcutBadger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContentActivity : FragmentActivity(), View.OnClickListener,
    DrawerFragment.FragmentDrawerListener {
    private var drawerFragment: DrawerFragment? = null
    var mcontent_frame: FrameLayout? = null
    var mfrm_mainFrame: FrameLayout? = null
    var mcontent_frameAbove: FrameLayout? = null
    var mMenuView: View? = null
    private var menuWidth = 0
    var isMenuOpen = false
    var fragType = 0
    private var mContent: Fragment? = null
    private val itemId: String? = null
    private val itemType: String? = null

    protected override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }


    protected override fun onCreate(savedInstanceState: Bundle?) {
        act = this@ContentActivity
        Log.d("CA-onCreate", "onCreate of ContentActivity")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)

        mLangSessionManager = LanguageSessionManager(this)
        mSessionManager = SessionManager(this)
        mImageLoader = ImageLoader.getInstance()
        menuWidth = getMenuWidth()
        initViews()
        if (getIntent().getExtras() != null) {
            fragType = getIntent().getIntExtra("com.ais.fragType", 1)
        }
        if (mLangSessionManager?.getLang() != null && mLangSessionManager?.getLang() != "") {
            if (mLangSessionManager?.getLang() == "en") {
                updateViews("en")
                typeFace = "fonts/verdana.ttf"
                tf = Typeface.createFromAsset(act?.getAssets(), typeFace)
                isEnglish = true
            } else if (mLangSessionManager?.getLang() == "ar") {
                updateViews("ar")
                typeFace = "fonts/GEDinarOne-Medium3.ttf"
                tf = Typeface.createFromAsset(act?.getAssets(), typeFace)
                isEnglish = false
            }
        } else {
            updateViews("ar")
            typeFace = "fonts/GEDinarOne-Medium3.ttf"
            tf = Typeface.createFromAsset(act?.getAssets(), typeFace)
            isEnglish = false
        }
        mtv_topTitle?.setTypeface(tf)
        /*tv_home?.setTypeface(tf)
        tv_news?.setTypeface(tf)
        tv_halls?.setTypeface(tf)
        tv_services?.setTypeface(tf)
        tv_deewaniya?.setTypeface(tf)
        tv_company?.setTypeface(tf)
        tv_about_us?.setTypeface(tf)
        tv_contact_us?.setTypeface(tf)
        tv_trees?.setTypeface(tf)
        tv_donation?.setTypeface(tf)
        tv_request_help?.setTypeface(tf)
        tv_occasion?.setTypeface(tf)
        tv_occasion_dead?.setTypeface(tf)*/
        mtv_cartCounter?.setTypeface(tf)
        if (mLangSessionManager?.getLang() == "en") {
            val tt: Typeface =
                Typeface.createFromAsset(act?.getAssets(), "fonts/GEDinarOne-Medium3.ttf")
            //mtv_lang.setTypeface(tt);
        } else {
            val tt: Typeface = Typeface.createFromAsset(act?.getAssets(), "fonts/verdana.ttf")
            //mtv_lang.setTypeface(tt);
        }

        if (savedInstanceState != null) {
            mContent = getSupportFragmentManager().getFragment(
                savedInstanceState, "mContent"
            )
        }
        CheckTestingVersion()

    }

    private fun setupDrawer(boolean: Boolean) {
        drawerFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_navigation_drawer) as DrawerFragment?
        if (drawerFragment != null) {
            drawerFragment!!.setUp(
                R.id.fragment_navigation_drawer,
                findViewById(R.id.drawer_layout),
                img_topMenu,
                boolean
            )
            drawerFragment!!.setDrawerListener(this)
        }
    }

    protected override fun onStart() {
        super.onStart()
        clearBadgeCount()

//        mSessionManager.LoginSeassion();
//
//        mSessionManager.setUserCode("18");
        Log.d("CA-onStart", "onStart of ContentActivity")
        Log.d("check_push", ">>>" + getIntent().getStringExtra("type") + "<<<")
        if (getIntent().hasExtra("type")) {
            val type: String = getIntent().getStringExtra("type")!!
            Log.d("type", "1>>>$type")
            //Projects
            if (type.equals("1", ignoreCase = true)) {
                Log.d("type", "2>>>$type")
                val getProjects = GetProjects()
                getProjects.Id = getIntent().getStringExtra("id")
                val gson = Gson()
                val b = Bundle()
                b.putString("GetProjects", gson.toJson(getProjects))
                openProjectDonationDetailsFragment(b)
            }
            //Monthly Donation
            if (type.equals("2", ignoreCase = true)) {
                Log.d("type", "3>>>$type")
                val getMonthlyDonations = GetMonthlyDonations()
                getMonthlyDonations.Id = getIntent().getStringExtra("id")
                val gson = Gson()
                val b = Bundle()
                b.putString("GetMonthlyDonations", gson.toJson(getMonthlyDonations))
                openSubscriptionInvoiceFragment(b)
            }
            //Occasions
            if (type.equals("4", ignoreCase = true)) {
                Log.d("type", "4>>>$type")
                val getEvents = GetEvents()
                getEvents.Id = getIntent().getStringExtra("id")
                val gson = Gson()
                val b = Bundle()
                b.putString("GetEvents.Events", gson.toJson(getEvents))
                openMonasbatDetailsFragment(b)
            }
            //Request Help
            if (type.equals("6", ignoreCase = true)) {
                Log.d("type", "4>>>$type")
                val getEvents = GetEvents()
                getEvents.Id = getIntent().getStringExtra("id")
                openMyRequestHelpFragment()
            }


            /*if (type.equalsIgnoreCase("1")) {

                GetNews getNews = new GetNews();

                getNews.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetNews", gson.toJson(getNews));
                ContentActivity.openNewsDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("2")) {

                GetEvents getEvents = new GetEvents();

                getEvents.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetEvents.Events", gson.toJson(getEvents));
                ContentActivity.openMonasbatDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("3")) {

                GetDewanias getDewanias = new GetDewanias();

                getDewanias.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetDewanias", gson.toJson(getDewanias));
                ContentActivity.openDeewaniyaDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("tree")) {

            }

            if (type.equalsIgnoreCase("4")) {

                GetHalls getHalls = new GetHalls();

                getHalls.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetHalls", gson.toJson(getHalls));
                b.putString("type", "halls");
                ContentActivity.openHallDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("5")) {

                GetCompanies companies = new GetCompanies();

                companies.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetCompanies", gson.toJson(companies));
                ContentActivity.openCompanyDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("6")) {

                GetMonthlyDonationSubscriptionInvoices subscriptionInvoices = new GetMonthlyDonationSubscriptionInvoices();

                subscriptionInvoices.setId(getIntent().getStringExtra("id"));

                SubscriptionInvoiceFragment.AddMonthlyDonationInvoice(subscriptionInvoices);

            }

            if (type.equalsIgnoreCase("7")) {

                GetServices services = new GetServices();

                services.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putInt("tabNumber", 0);
                b.putString("GetServices", gson.toJson(services));
                ContentActivity.openServiceDetailsFragment(b);

            }
            if (type.equalsIgnoreCase("8")) {

                GetProjects getProjects = new GetProjects();
                getProjects.setId(getIntent().getStringExtra("id"));
                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetProjects", gson.toJson(getProjects));
                ContentActivity.openProjectDonationDetailsFragment(b);

            }*/
        }
    }

    fun loadView() {
        if (mContent == null) {
            when (fragType) {
                1 -> {
                    val bundle = Bundle()
                    bundle.putString("comingFrom", "home")
                    mContent = NewsFragment.Companion.newInstance(act)
                    HomeFragment.Companion.getCategoriesArrayList?.clear()
                    mContent?.setArguments(bundle)
                    act?.getSupportFragmentManager()
                        ?.beginTransaction()
                        ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        ?.replace(
                            R.id.content_frame, mContent!!,
                            "HomeFragment"
                        )?.commit()
                }
            }
        }
    }

    fun initViews() {
        //try {
        img_close_dialog = findViewById(R.id.img_close_dialog) as ImageView
        webView = findViewById(R.id.webView) as WebView
        relative_dialog = findViewById(R.id.relative_dialog) as RelativeLayout
        tv_bg = findViewById(R.id.tv_bg) as TextView
        img_calendar_bg = findViewById(R.id.img_calendar_bg) as ImageView
        img_calendar_bg?.setOnClickListener(View.OnClickListener { })
        tv_ok = findViewById(R.id.tv_ok) as TextView
        relative_message = findViewById(R.id.relative_message) as RelativeLayout
        bottomBarView = findViewById(R.id.rel_bottomBar)!!
        //  topBarView = findViewById(R.id.rel_topBar)!!
        mrel_cart = findViewById(R.id.rel_cart) as RelativeLayout
        mrel_cart?.setOnClickListener(this)
        mtv_cartCounter = mrel_cart?.findViewById(R.id.tv_cartCounter) as TextView
        mimg_home = bottomBarView?.findViewById(R.id.img_home) as ImageView
        mimg_home?.setOnClickListener(this)
        mimg_products = bottomBarView?.findViewById(R.id.img_products) as ImageView
        mimg_products?.setOnClickListener(this)
        mimg_account = bottomBarView?.findViewById(R.id.img_account) as ImageView
        mimg_account?.setOnClickListener(this)
        mimg_search = bottomBarView?.findViewById(R.id.img_search) as ImageView
        mimg_search?.setOnClickListener(this)
        mimg_contact = bottomBarView?.findViewById(R.id.img_contact) as ImageView
        mimg_contact?.setOnClickListener(this)
        img_topmenu_ = findViewById(R.id.img_topMenu) as ImageView
//           mimg_topMenuNav?.setOnClickListener(this)
        img_topHall = findViewById(R.id.img_topHalls) as ImageView
        img_topHall?.setOnClickListener(this)
        img_topAddAd = findViewById(R.id.img_topAddAds) as ImageView
        img_topAddAd?.setOnClickListener(this)
        img_topAccount = findViewById(R.id.img_topAccounts) as ImageView
        img_topAccount?.setOnClickListener(this)
        img_topback_ = findViewById(R.id.img_topback) as ImageView
        img_topback_?.setOnClickListener(this)
        img_topProfile = findViewById(R.id.img_topProfiles) as CircleImageView
        img_topProfile?.setOnClickListener(this)
        img_topFilter = findViewById(R.id.img_topFilters) as ImageView
        img_topFilter?.setOnClickListener(this)
        mtv_topTitle = findViewById(R.id.tv_topTitle) as TextView
        mcontent_frame = findViewById(R.id.content_frame) as FrameLayout?
        mfrm_mainFrame = findViewById(R.id.frm_mainFrame) as FrameLayout?
        mcontent_frameAbove = findViewById(R.id.content_frameAbove) as FrameLayout?
        mcontent_frameAbove?.setOnClickListener(this)
        /* mMenuView = findViewById(R.id.view_menu)
         tv_home = mMenuView?.findViewById(R.id.tv_home) as TextView
         tv_home?.setOnClickListener(this)
         tv_news = mMenuView?.findViewById(R.id.tv_news) as TextView
         tv_news?.setOnClickListener(this)
         tv_halls = mMenuView?.findViewById(R.id.tv_halls) as TextView
         tv_halls?.setOnClickListener(this)
         tv_services = mMenuView?.findViewById(R.id.tv_services) as TextView
         tv_services?.setOnClickListener(this)
         tv_deewaniya = mMenuView?.findViewById(R.id.tv_deewaniya) as TextView
         tv_deewaniya?.setOnClickListener(this)
         tv_company = mMenuView?.findViewById(R.id.tv_company) as TextView
         tv_company?.setOnClickListener(this)
         tv_about_us = mMenuView?.findViewById(R.id.tv_about_us) as TextView
         tv_about_us?.setOnClickListener(this)
         tv_contact_us = mMenuView?.findViewById(R.id.tv_contact_us) as TextView
         tv_contact_us?.setOnClickListener(this)
         tv_trees = mMenuView?.findViewById(R.id.tv_trees) as TextView
         tv_trees?.setOnClickListener(this)
         tv_trees?.setVisibility(View.GONE)
         tv_donation = mMenuView?.findViewById(R.id.tv_donation) as TextView
         tv_donation?.setOnClickListener(this)
         tv_request_help = mMenuView?.findViewById(R.id.tv_request_help) as TextView
         tv_request_help?.setOnClickListener(this)
         tv_occasion = mMenuView?.findViewById(R.id.tv_occasion) as TextView
         tv_occasion?.setOnClickListener(this)
         tv_occasion_dead = mMenuView?.findViewById(R.id.tv_occasion_dead) as TextView
         tv_occasion_dead?.setOnClickListener(this)
         tv_lang = mMenuView?.findViewById(R.id.tv_lang) as TextView
         tv_lang?.setOnClickListener(this)*/

        //mtv_lang = (TextView) mMenuView.findViewById(R.id.tv_lang);
        //mtv_lang.setOnClickListener(this);
        /*} catch (e: Exception) {
            Log.e(TAG + " " + " initViews >> "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }*/


        var okok = mSessionManager.isLoggedin()
        Log.e("----->>>.", "" + okok);
        setupDrawer(okok)

    }

    private fun getMenuWidth(): Int {
        try {
            return (act?.getResources()?.getDrawable(
                R.drawable.side_menu_bg
            ) as BitmapDrawable).getBitmap().getWidth()
        } catch (e: Exception) {
            Log.e(
                "TAG" + " " + " getMenuWidth >> "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
        return 0
    }

    private fun setMenuInitialPosition() {
        /*   mMenuView?.setVisibility(View.VISIBLE)
           mMenuView?.setX(topBarView?.getWidth()?.toFloat()!!)*/
    }

    fun onMenuOpened() {
        Log.d(TAG, "onMenuOpened")
        GlobalFunctions.DisableLayout(mcontent_frame)
        mcontent_frameAbove?.setVisibility(View.VISIBLE)
        mcontent_frameAbove?.setEnabled(true)
    }

    fun onMenuClosed() {
        Log.d(TAG, "onMenuClosed")
        GlobalFunctions.EnableLayout(mcontent_frame)
        mcontent_frameAbove?.setVisibility(View.GONE)
    }

    private fun ToggleMainFrame(frm: FrameLayout?) {
        try {
            var translateAnimator: ObjectAnimator? = null
            translateAnimator = if (!isMenuOpen) {
                ObjectAnimator.ofFloat(
                    mfrm_mainFrame,
                    "translationX", 0.0f, menuWidth.toFloat()
                )
            } else {
                ObjectAnimator.ofFloat(
                    mfrm_mainFrame,
                    "translationX", menuWidth.toFloat(), 0.0f
                )
            }
            translateAnimator.interpolator = AccelerateDecelerateInterpolator()
            translateAnimator.duration = 200
            translateAnimator.start()
            isMenuOpen = !isMenuOpen
        } catch (e: Exception) {
            Log.e(
                "TAG" + " " + " ToogleMainFrame>> "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
    }

    private fun ToggleMenu(mToggleMenuType: Int, frm: FrameLayout?) {
        when (mToggleMenuType) {
            1 -> ToggleMainFrame(frm)
            2 -> ToggleBothMenuAndFrame(frm)
            else -> {
            }
        }
    }

    private fun ToggleBothMenuAndFrame(rel: FrameLayout?) {
        /*   try {
               setMenuInitialPosition()
               val translateAnimator1: ObjectAnimator?
               val translateAnimator2: ObjectAnimator?
               val translateAnimator3: ObjectAnimator?
               val translateAnimator4: ObjectAnimator?
               if (!isMenuOpen) {
                   translateAnimator1 = ObjectAnimator.ofFloat(mfrm_mainFrame,
                           "translationX", 0.0f, -menuWidth.toFloat())
                   translateAnimator2 = ObjectAnimator.ofFloat(topBarView,
                           "translationX", 0f, -menuWidth.toFloat())
                   translateAnimator4 = ObjectAnimator.ofFloat(bottomBarView,
                           "translationX", 0f, -menuWidth.toFloat())
                   translateAnimator3 = ObjectAnimator.ofFloat(mMenuView,
                           "translationX", topBarView?.getWidth()?.toFloat()!!,
                           topBarView?.getWidth()?.toFloat()!! - menuWidth.toFloat())
               } else {
                   translateAnimator1 = ObjectAnimator.ofFloat(mfrm_mainFrame,
                           "translationX", -menuWidth.toFloat(), 0.0f)
                   translateAnimator2 = ObjectAnimator.ofFloat(topBarView,
                           "translationX", -menuWidth.toFloat(), 0f)
                   translateAnimator4 = ObjectAnimator.ofFloat(bottomBarView,
                           "translationX", -menuWidth.toFloat(), 0f)
                   translateAnimator3 = ObjectAnimator.ofFloat(mMenuView,
                           "translationX", topBarView?.getWidth()?.toFloat()!! - menuWidth.toFloat(),
                           topBarView?.getWidth()?.toFloat()!!)
               }
               translateAnimator1.interpolator = AccelerateDecelerateInterpolator()
               translateAnimator2.interpolator = AccelerateDecelerateInterpolator()
               translateAnimator3.interpolator = AccelerateDecelerateInterpolator()
               translateAnimator4.interpolator = AccelerateDecelerateInterpolator()
               translateAnimator1.duration = 200
               translateAnimator2.duration = 200
               translateAnimator3.duration = 200
               translateAnimator4.duration = 200
               translateAnimator1.addListener(object : Animator.AnimatorListener {
                   override fun onAnimationStart(animation: Animator?) {
                       translateAnimator2.start()
                       translateAnimator3.start()
                       translateAnimator4.start()
                   }

                   override fun onAnimationRepeat(animation: Animator?) {
                   }

                   override fun onAnimationEnd(animation: Animator?) {
                       if (isMenuOpen) {
                           onMenuOpened()
                       } else {
                           onMenuClosed()
                       }
                   }

                   override fun onAnimationCancel(animation: Animator?) {
                   }
               })
               translateAnimator1.start()
               isMenuOpen = !isMenuOpen
           } catch (e: Exception) {
               Log.e(" ToggleBoth>> "
                       + Thread.currentThread().stackTrace[2].lineNumber,
                       e.message)
           }*/
    }

    override fun onClick(v: View) {

        when (v.getId()) {
            /*   R.id.img_topMenu -> {
                   tv_home?.setText(R.string.HomeLabel)
                   tv_news?.setText(R.string.NewsLabel)
                   tv_halls?.setText(R.string.HallsLabel)
                   tv_services?.setText(R.string.ServicesLabel)
                   tv_deewaniya?.setText(R.string.DeewaniyaLabel)
                   tv_company?.setText(R.string.CompaniesLabel)
                   tv_about_us?.setText(R.string.AboutUSLabel)
                   tv_contact_us?.setText(R.string.ContactUsLabel)
                   tv_trees?.setText(R.string.TreeLabel)
                   tv_donation?.setText(R.string.DonateLabel)
                   tv_request_help?.setText(R.string.RequestHelpLabel)
                   tv_occasion?.setText(R.string.OccasionLabel)
                   tv_occasion_dead?.setText(R.string.OccasionDeadLabel)
                   if (mLangSessionManager?.getLang().equals("en", ignoreCase = true)) {
                       tv_lang?.setText(act?.getString(R.string.ArabicLabel))
                   } else {
                       tv_lang?.setText(act?.getString(R.string.EnglishLabel))
                   }
                   mtv_topTitle?.setTypeface(tf)
                   tv_home?.setTypeface(tf)
                   tv_news?.setTypeface(tf)
                   tv_halls?.setTypeface(tf)
                   tv_services?.setTypeface(tf)
                   tv_deewaniya?.setTypeface(tf)
                   tv_company?.setTypeface(tf)
                   tv_about_us?.setTypeface(tf)
                   tv_contact_us?.setTypeface(tf)
                   tv_trees?.setTypeface(tf)
                   tv_donation?.setTypeface(tf)
                   tv_request_help?.setTypeface(tf)
                   tv_occasion?.setTypeface(tf)
                   tv_occasion_dead?.setTypeface(tf)
                   mtv_cartCounter?.setTypeface(tf)
                   tv_home?.setBackgroundResource(R.drawable.link_bg)
                   tv_news?.setBackgroundResource(R.drawable.link_bg)
                   tv_halls?.setBackgroundResource(R.drawable.link_bg)
                   tv_services?.setBackgroundResource(R.drawable.link_bg)
                   tv_deewaniya?.setBackgroundResource(R.drawable.link_bg)
                   tv_company?.setBackgroundResource(R.drawable.link_bg)
                   tv_about_us?.setBackgroundResource(R.drawable.link_bg)
                   tv_contact_us?.setBackgroundResource(R.drawable.link_bg)
                   tv_trees?.setBackgroundResource(R.drawable.link_bg)
                   tv_donation?.setBackgroundResource(R.drawable.link_bg)
                   tv_request_help?.setBackgroundResource(R.drawable.link_bg)
                   tv_occasion?.setBackgroundResource(R.drawable.link_bg)
                   tv_occasion_dead?.setBackgroundResource(R.drawable.link_bg)
                   ToggleMenu(2, mfrm_mainFrame)
               }*/
            R.id.img_topAccounts -> {

                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    openMyAccountFragment();
                } else {
                    openLoginFragment();
                }
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
            }
            R.id.img_topProfiles -> {
            }
            R.id.img_topback -> {

//                finish()
                onBackPressed()
            }

            R.id.content_frameAbove -> if (isMenuOpen) {
                ToggleMenu(2, mfrm_mainFrame)
            }
            R.id.tv_news -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
                val bundle = Bundle()
                bundle.putString("comingFrom", "news")
                openNewsFragment(bundle)
            }
            R.id.tv_halls -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                 val bundle = Bundle()
//                 bundle.putString("type", "halls")
//                 openHallFragment(bundle)
            }
            R.id.tv_services -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                 openServiceCategoryFragment()
            }
            R.id.tv_deewaniya -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                 openDeewaniyaFragment()
            }
            R.id.tv_company -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                 openCompanyCategoryFragment()
            }
            R.id.tv_about_us -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                 val b = Bundle()
//                 b.putString("type", "about_us")
//                 openAboutUsFragment(b)
            }
            R.id.tv_contact_us -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                 openContactUsFragment()
            }
            R.id.tv_home -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
                val bundle = Bundle()
                bundle.putString("comingFrom", "home")
                HomeFragment.getCategoriesArrayList.clear()
                openNewsFragment(bundle)

                /*val bundle = Bundle()
                bundle.putString("comingFrom", "home")
                mContent = NewsFragment.newInstance(act)
                HomeFragment.getCategoriesArrayList.clear()
                mContent?.setArguments(bundle)
                act?.getSupportFragmentManager()
                        ?.beginTransaction()
                        ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                        ?.replace(R.id.content_frame, mContent!!,
                                "HomeFragment")?.commit()*/
            }
            R.id.tv_trees -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                if (mSessionManager?.isLoggedin()!!
//                        && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null) {
//                    val b = Bundle()
//                    b.putString("comingFrom", "home")
//                    openHomeFragment(b)
//                } else {
//                    Snackbar.make(tv_trees!!, act?.getString(R.string.LoggedIn).toString(), Snackbar.LENGTH_LONG).show()
//                    openLoginFragment()
//                }
            }
            R.id.tv_donation -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                openDonateFragment()
            }
            R.id.tv_request_help -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
//                if (mSessionManager?.isLoggedin()!!
//                        && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null) {
//                    clearVariables()
//                    val bundle = Bundle()
//                    openMyRequestHelpFragment()
//                } else {
//                    Snackbar.make(tv_request_help!!, act?.getString(R.string.LoggedIn).toString(), Snackbar.LENGTH_LONG).show()
//                    openLoginFragment()
//                }
            }
            R.id.tv_occasion_dead -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
                val bundle = Bundle()
                bundle.putString("catId", "7")
                openMonasbatFragment(bundle)
            }
            R.id.tv_occasion -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
                val bundle = Bundle()
                bundle.putString("catId", "-1")
                openMonasbatFragment(bundle)
            }
            R.id.img_home -> tabType = 1
            R.id.img_products -> tabType = 2
            R.id.img_account -> tabType = 3
            R.id.img_search -> {
                Log.d("click_content", "clicked search")
                tabType = 4
            }
            R.id.img_contact -> tabType = 5
            R.id.rel_cart -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
            }
            R.id.tv_lang -> {
                if (mLangSessionManager?.getLang().equals("en", ignoreCase = true)) {
                    mLangSessionManager?.setLang("ar")
                    startActivity(
                        Intent(this@ContentActivity, SplashActivity::class.java)
                            .putExtra("com.qenaat.fragType", 1)
                    )
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                } else {
                    mLangSessionManager?.setLang("en")
                    startActivity(
                        Intent(this@ContentActivity, SplashActivity::class.java)
                            .putExtra("com.qenaat.fragType", 1)
                    )
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                }

            }
            //                if (isMenuOpen) {
//                    ToggleMenu(2, mfrm_mainFrame);
//                }

        }


    }


    override fun onBackPressed() {
        if (isMenuOpen) {
            ToggleMenu(2, mfrm_mainFrame)
        } else {
//            if(getSupportFragmentManager().getBackStackEntryCount()==1){
//

//            getActivity().onBackPressed()

//                finish();
//
//            }
//            else{
            super.onBackPressed()

//            }
        }
    }

    protected override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            Log.d("check_push", ">>>" + intent.getStringExtra("type") + "<<<")
            if (intent.hasExtra("type")) {
                val type: String = intent.getStringExtra("type")
                Log.d("type", "1>>>$type")
                if (type.equals("1", ignoreCase = true)) {
                    Log.d("type", "2>>>$type")
                    val getProjects = GetProjects()
                    getProjects.Id = intent.getStringExtra("id")
                    val gson = Gson()
                    val b = Bundle()
                    b.putString("GetProjects", gson.toJson(getProjects))
                    openProjectDonationDetailsFragment(b)
                }
                if (type.equals("2", ignoreCase = true)) {
                    Log.d("type", "3>>>$type")
                    val getMonthlyDonations = GetMonthlyDonations()
                    getMonthlyDonations.Id = intent.getStringExtra("id")
                    val gson = Gson()
                    val b = Bundle()
                    b.putString("GetMonthlyDonations", gson.toJson(getMonthlyDonations))
                    openSubscriptionInvoiceFragment(b)
                }
                if (type.equals("4", ignoreCase = true)) {
                    Log.d("type", "4>>>$type")
                    val getEvents = GetEvents()
                    getEvents.Id = intent.getStringExtra("id")
                    val gson = Gson()
                    val b = Bundle()
                    b.putString("GetEvents.Events", gson.toJson(getEvents))
                    openMonasbatDetailsFragment(b)
                }
                if (type.equals("6", ignoreCase = true)) {
                    Log.d("type", "4>>>$type")
                    val getEvents = GetEvents()
                    getEvents.Id = intent.getStringExtra("id")
                    openMyRequestHelpFragment()
                }


                /*if (type.equalsIgnoreCase("1")) {

                GetNews getNews = new GetNews();

                getNews.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetNews", gson.toJson(getNews));
                ContentActivity.openNewsDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("2")) {

                GetEvents getEvents = new GetEvents();

                getEvents.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetEvents.Events", gson.toJson(getEvents));
                ContentActivity.openMonasbatDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("3")) {

                GetDewanias getDewanias = new GetDewanias();

                getDewanias.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetDewanias", gson.toJson(getDewanias));
                ContentActivity.openDeewaniyaDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("tree")) {

            }

            if (type.equalsIgnoreCase("4")) {

                GetHalls getHalls = new GetHalls();

                getHalls.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetHalls", gson.toJson(getHalls));
                b.putString("type", "halls");
                ContentActivity.openHallDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("5")) {

                GetCompanies companies = new GetCompanies();

                companies.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetCompanies", gson.toJson(companies));
                ContentActivity.openCompanyDetailsFragment(b);

            }

            if (type.equalsIgnoreCase("6")) {

                GetMonthlyDonationSubscriptionInvoices subscriptionInvoices = new GetMonthlyDonationSubscriptionInvoices();

                subscriptionInvoices.setId(getIntent().getStringExtra("id"));

                SubscriptionInvoiceFragment.AddMonthlyDonationInvoice(subscriptionInvoices);

            }

            if (type.equalsIgnoreCase("7")) {

                GetServices services = new GetServices();

                services.setId(getIntent().getStringExtra("id"));

                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putInt("tabNumber", 0);
                b.putString("GetServices", gson.toJson(services));
                ContentActivity.openServiceDetailsFragment(b);

            }
            if (type.equalsIgnoreCase("8")) {

                GetProjects getProjects = new GetProjects();
                getProjects.setId(getIntent().getStringExtra("id"));
                Gson gson = new Gson();
                Bundle b = new Bundle();
                b.putString("GetProjects", gson.toJson(getProjects));
                ContentActivity.openProjectDonationDetailsFragment(b);

            }*/
            }
        }
    }

    private fun clearBadgeCount() {
        try {
            val shared: SharedPreferences = getSharedPreferences("counter", 0)
            val edit: SharedPreferences.Editor = shared.edit()
            edit.putString("cc", "0")
            edit.putInt("noti_counter", 0)
            edit.commit()
            ShortcutBadger.setBadge(getApplicationContext(), 0)
        } catch (e: ShortcutBadgeException) {
        }
    }

    fun CheckTestingVersion() {
        QenaatAPICall.getCallingAPIInterface()?.settings()
            ?.enqueue(object : Callback<GetAppSettings?> {
                override fun onFailure(call: Call<GetAppSettings?>, t: Throwable) {
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<GetAppSettings?>,
                    response: Response<GetAppSettings?>
                ) {
                    if (response.body() != null) {
                        val bottomRandomAds = response.body()
                        if (bottomRandomAds != null) {

                            try {
                                val pInfo: PackageInfo = act.packageManager
                                    .getPackageInfo(act.packageName, 0)
                                val versionCode: String = pInfo.versionCode.toString()
                                if (versionCode == bottomRandomAds.android_version) {
                                    loadView()
                                } else {
                                    if (bottomRandomAds.android_is_force_update == "0")
                                        showAlertUpdate(bottomRandomAds.android_message!!)
                                    else if (bottomRandomAds.android_is_force_update == "1")
                                        showAlertForceUpdate(bottomRandomAds.android_message!!)
                                }
                            } catch (e: PackageManager.NameNotFoundException) {
                                e.printStackTrace()
                            }

                            if (bottomRandomAds.ForPopTest.equals("true", ignoreCase = true)) {
                                setupMoreIfoDialog1("", bottomRandomAds.StartPopText)
                            }
                        }
                    }
                }
            })
    }

    //Forcefully update
    private fun showAlertForceUpdate(message: String) {
        AlertDialog.Builder(act)
            .setTitle(act.getString(R.string.app_name))
            .setMessage(message)
            .setPositiveButton(getString(R.string.playstore)) { _, which -> openPlayStore() }
            .setIcon(R.drawable.icon_512)
            .setCancelable(false)
            .show()
    }

    //No forcefully update
    private fun showAlertUpdate(message: String) {
        AlertDialog.Builder(act)
            .setTitle(act.getString(R.string.app_name))
            .setMessage(message)
            .setNegativeButton(getString(R.string.playstore)) { dialog, which ->
                dialog.dismiss()
                openPlayStore()
                loadView()
            }
            .setPositiveButton(getString(R.string.close)) { dialog, which ->
                dialog.dismiss()
                loadView()
            }
            .setIcon(R.drawable.icon_512)
            .setCancelable(false)
            .show()
    }

    private fun openPlayStore() {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }

    companion object : Interfaces.RequestHelpListener {
        protected val TAG = ContentActivity::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var bottomBarView: View

        lateinit var img_topHall: ImageView
        lateinit var img_topAddAd: ImageView
        lateinit var mimg_home: ImageView
        lateinit var mimg_search: ImageView
        lateinit var mimg_account: ImageView
        lateinit var mimg_products: ImageView
        lateinit var mimg_contact: ImageView
        lateinit var mtv_cartCounter: TextView
        lateinit var mtv_topTitle: TextView

        /* lateinit var tv_home: TextView
         lateinit var tv_news: TextView
         lateinit var tv_halls: TextView
         lateinit var tv_services: TextView
         lateinit var tv_deewaniya: TextView
         lateinit var tv_company: TextView
         lateinit var tv_about_us: TextView
         lateinit var tv_contact_us: TextView
         lateinit var tv_trees: TextView
         lateinit var tv_occasion: TextView
         lateinit var tv_occasion_dead: TextView
         lateinit var tv_donation: TextView
         lateinit var tv_request_help: TextView
         lateinit var tv_lang: TextView*/
        lateinit var mrel_cart: RelativeLayout
        var tabType = 1

        // lateinit var topBarView: View
        lateinit var typeFace: String
        lateinit var tf: Typeface
        lateinit var mLangSessionManager: LanguageSessionManager
        lateinit var mSessionManager: SessionManager
        var isEnglish = false
        lateinit var relative_message: RelativeLayout
        lateinit var tv_ok: TextView
        lateinit var img_calendar_bg: ImageView
        lateinit var mImageLoader: ImageLoader
        lateinit var webView: WebView
        lateinit var relative_dialog: RelativeLayout
        lateinit var tv_bg: TextView
        lateinit var img_close_dialog: ImageView
        var cityId: String = ""
        var familyNameId: String = ""
        var familyName: String = ""
        var countryId: String = ""
        var countryName: String = ""
        var cityName: String = ""
        var treeId: String = ""
        var treeName: String = ""
        var catId: String = ""
        var myInvitationId: String = ""
        var otherInvitationId: String = ""
        var typeId: String = ""
        var areaId: String = ""
        var myInvitationName: String = ""
        var otherInvitationName: String = ""
        var catName: String = ""
        var typeName: String = ""
        var areaName: String = ""
        var fullName: String = ""
        var mobile: String = ""
        var request_mobile: String = ""
        var location: String = ""
        var latitude: String = ""
        var longitude: String = ""
        var details: String = ""
        var to_time: String = ""
        var from_time: String = ""
        var date: String = ""
        var youTube: String = ""
        var facebook: String = ""
        var instagram: String = ""
        var twitter: String = ""
        var dayId: String = ""
        var title: String = ""
        var dayName: String = ""
        var price: String = ""
        var FRAGMENT_CODE = 111
        lateinit var img_topAccount: ImageView
        lateinit var img_topback_: ImageView
        lateinit var img_topmenu_: ImageView
        lateinit var img_topFilter: ImageView
        lateinit var img_topProfile: CircleImageView
        var parentId: String = ""
        var parentNameEn: String = ""
        var parentNameAr: String = ""
        lateinit var myRequestHelpFragment: Fragment

        fun setTextFonts(root: ViewGroup) {
            for (i in 0 until root.getChildCount()) {
                val v: View = root.getChildAt(i)
                if (v is TextView) {
                    (v as TextView).typeface = tf
                } else if (v is Button) {
                    (v as Button).typeface = tf
                } else if (v is EditText) {
                    (v as EditText).typeface = tf
                } else if (v is ViewGroup) {
                    setTextFonts(v as ViewGroup)
                }
            }
        }

        fun openContactUsFragment() {
            val frag: Fragment = ContactUsFragment.newInstance(act)!!
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openAddFamilyTreeFragment() {
            clearVariables()
            val frag: Fragment = AddFamilyTreeFragment.newInstance(act)!!
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openAboutUsFragment(b: Bundle?) {
            val frag: Fragment = AboutUsFragment.newInstance(act)!!
            frag.arguments = b
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openDeewaniyaDetailsFragment(b: Bundle?) {
            val frag: Fragment = DeewaniyaDetailsFragment.newInstance(act)!!
            frag.arguments = b
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openShowHallMapFragment(b: Bundle?) {
            val frag: Fragment = ShowHallMapFragment.newInstance(act)!!
            frag.arguments = b
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openHallDetailsFragment(b: Bundle?) {
            val frag: Fragment = HallDetailsFragment.newInstance(act)!!
            frag.arguments = b
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openShowDeewaniyaMapFragment(b: Bundle?) {
            val frag: Fragment = ShowDeewaniyaMapFragment.newInstance(act)!!
            frag.arguments = b
            act?.getSupportFragmentManager()
                ?.beginTransaction()
                ?.setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                ?.addToBackStack(frag.javaClass.name)
                ?.replace(R.id.content_frame, frag)?.commit()
        }

        fun openDeewaniyaFragment() {
            val frag: Fragment = DeewaniyaFragment.newInstance(act)!!
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openHallFragment(bundle: Bundle?) {
            val frag: Fragment = HallFragment.newInstance(act)!!
            frag.arguments = bundle
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openPaymentlistFragment() {
            val frag: Fragment = PaymentListFragment.newInstance(act)!!
//            frag.arguments = bundle
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openCurrentLocationFragmentFadeIn() {
            val frag: Fragment = CurrentLocationFragment.newInstance(act)!!
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openHomeFragment(bundle: Bundle?) {
            HomeFragment.Companion.getCategoriesArrayList?.clear()
            val frag: Fragment = HomeFragment.newInstance(act)!!
            frag.arguments = bundle
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openVIPPersonDetailsFragment(bundle: Bundle?) {
            val frag: Fragment = VIPPersonDetailsFragment.newInstance(act)!!
            frag.arguments = bundle
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openNewsFragment(bundle: Bundle?) {
            val frag: Fragment = NewsFragment.newInstance(act)!!
            frag.arguments = bundle
            act!!.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openServiceCategoryFragment() {
            val frag: Fragment = ServiceCategoryFragment.Companion.newInstance(act)!!
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openMonasbatFragment(bundle: Bundle?) {
            val frag: Fragment = MonasbatFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openServiceListFragment(bundle: Bundle?) {
            val frag: Fragment = ServiceListFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openQuickDonateFragment(bundle: Bundle?) {
            val frag: Fragment = QuickDonateFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openProjectDonationDetailsFragment(bundle: Bundle?) {
            val frag: Fragment = ProjectDonationDetailsFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openProjectForDonationFragment() {
            val frag: Fragment = ProjectForDonationFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openChangePasswordFragment() {
            val frag: Fragment = ChangePasswordFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openNewsDetailsFragment(bundle: Bundle?) {
            val frag: Fragment = NewsDetailsFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openSubscriptionInvoiceFragment(bundle: Bundle?) {
            val frag: Fragment = SubscriptionInvoiceFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }
        fun openShowMapFragment(bundle: Bundle?) {
            val frag: Fragment = ShowMapFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openDonateFragment() {
            val frag: Fragment = DonateFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openCompanyCategoryFragment() {
            val frag: Fragment = CompanyCategoryFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openMyAccountFragment() {
            val frag: Fragment = MyAccountFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openMonasbatDetailsFragment(bundle: Bundle?) {
            val frag: Fragment = MonasbatDetailsFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openAddServiceFragment(bundle: Bundle?) {
            val frag: Fragment = AddServiceFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openCalendarHallFragment(bundle: Bundle?) {
            val frag: Fragment = CalendarHallFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openAddNewsFragment(bundle: Bundle?) {
            val frag: Fragment = AddNewsFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openAddDeewaniyaFragment(bundle: Bundle?) {
            val frag: Fragment = AddDeewaniyaFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openAddRequestHelpFragment(bundle: Bundle?) {
            val frag: Fragment = AddRequestHelpFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openChatListFragment(bundle: Bundle?) {
            val frag: Fragment = ChatListFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openCompanyFragment(bundle: Bundle?) {
            val frag: Fragment = CompanyFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openCompanyDetailsFragment(bundle: Bundle?) {
            val frag: Fragment = CompanyDetailsFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openBookHallFragment(bundle: Bundle?) {
            val frag: Fragment = BookHallFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openServiceDetailsFragment(bundle: Bundle?) {
            val frag: Fragment = ServiceDetailsFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openRequestFormDocumentListFragment(bundle: Bundle?) {
            val frag: Fragment = RequestFormDocumentListFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openUploadRequestFormDocumentFragment(bundle: Bundle?) {
            val frag: Fragment = UploadRequestFormDocumentFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openAddPersonToInvitationListFragment(bundle: Bundle?) {
            // AddPersonToInvitationListFragment.getFamilyTreeArrayList.clear();
            //AddPersonToInvitationListFragment.page_index=0;
            val frag: Fragment = AddPersonToInvitationListFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openForgotPasswordFragment() {
            val frag: Fragment = ForgotPasswordFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openActivateAccountFragment() {
            val frag: Fragment = ActivateAccountFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openMyInvitationListFragment() {
            MyInvitationListFragment.Companion.tabNumber = 1
            MyInvitationListFragment.Companion.invitationListsArrayList1?.clear()
            MyInvitationListFragment.Companion.invitationListsArrayList2?.clear()
            MyInvitationListFragment.Companion.page_index = 0
            val frag: Fragment = MyInvitationListFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openCreateMyListFragment(bundle: Bundle?) {
            val frag: Fragment = CreateMyListFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openServicesMyAccountFragment() {
            val frag: Fragment = ServicesMyAccountFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openLoginFragment() {
            val frag: Fragment = LoginFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openRegisterFragment(bundle: Bundle?) {
            val frag: Fragment = RegisterFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openPersonFilterFragment(bundle: Bundle?) {
            val frag: Fragment = PersonFilterFragment.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openMyRequestHelpFragment() {
            //val frag: Fragment = MyRequestHelpFragment.newInstance(act)
            myRequestHelpFragment = MyRequestHelpFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(myRequestHelpFragment.javaClass.name)
                .replace(R.id.content_frame, myRequestHelpFragment).commit()
        }

        fun openMyOccasionFragment() {
            MyOccasionFragment.Companion.getEventsArrayList?.clear()
            MyOccasionFragment.Companion.page_index = 0
            val frag: Fragment = MyOccasionFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openGalleryFragmentFadeIn(bundle: Bundle?) {
            val frag: Fragment = GalleryFragment.newInstance(act)
            frag.arguments = bundle

            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openRequestHelpFormFragment(bundle: Bundle?, isRefresh: Boolean) {
            RequestHelpFormFragment.Companion.familyMembersArrayList?.clear()
            val frag: Fragment = if (isRefresh)
                RequestHelpFormFragment.Companion.newInstance(act, this)
            else
                RequestHelpFormFragment.Companion.newInstance(act)
            frag.arguments = bundle
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }

        fun openIdentifyUserFragment() {
            clearVariables()
            val frag: Fragment = IdentifyUserFragment.newInstance(act)
            act.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .addToBackStack(frag.javaClass.name)
                .replace(R.id.content_frame, frag).commit()
        }


        fun clearVariables() {
            title = ""
            cityId = ""
            familyName = ""
            familyNameId = ""
            countryId = ""
            price = ""
            countryName = ""
            cityName = ""
            treeId = ""
            treeName = ""
            fullName = ""
            mobile = ""
            location = ""
            latitude = ""
            longitude = ""
            details = ""
            date = ""
            youTube = ""
            facebook = ""
            instagram = ""
            twitter = ""
            catId = ""
            myInvitationId = ""
            otherInvitationId = ""
            myInvitationName = ""
            otherInvitationName = ""
            catName = ""
            areaId = ""
            areaName = ""
            dayName = ""
            dayId = ""
            typeId = ""
            typeName = ""
            mSessionManager?.setImagePath("")
        }

        fun enableLogin(languageSeassionManager: LanguageSessionManager?) {
            if (languageSeassionManager?.getLang() == "en") {
                updateViews("en")
                val typeFace = "fonts/verdana.ttf"
                tf = Typeface.createFromAsset(act.getAssets(), typeFace)
            } else if (languageSeassionManager?.getLang() == "ar") {
                updateViews("ar")
                val typeFace = "fonts/GEDinarOne-Medium3.ttf"
                tf = Typeface.createFromAsset(act.getAssets(), typeFace)
            }
            img_topAccount?.setVisibility(View.VISIBLE)
            img_topFilter?.setVisibility(View.GONE)
            mtv_topTitle?.setOnClickListener(null)
            if (mSessionManager?.isLoggedin()!!
                && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
            ) {
                img_topAccount?.setVisibility(View.VISIBLE)
            }
            //img_topProfile?.setVisibility(View.VISIBLE)
        }

        fun setupMoreIfoDialog(title: String?, mainHTMLText: String?) {
            var mainHTMLText = mainHTMLText
            Log.d("NotificationHistory", "3")
            tv_bg?.setOnClickListener(View.OnClickListener { })
            relative_dialog?.setVisibility(View.VISIBLE)
            img_close_dialog?.setOnClickListener(View.OnClickListener {
                relative_dialog?.setVisibility(
                    View.GONE
                )
            })
            relative_dialog?.setOnClickListener(View.OnClickListener { })
            var finalHTML = ""
            if (mLangSessionManager?.getLang().equals("en", ignoreCase = true)) {
                mainHTMLText = mainHTMLText?.replace("font-".toRegex(), "f_nt")
//                finalHTML = "<html><head><style type='text/css'>@font-face {font-family: 'Montserrat-Regular';src: url('fonts/verdana.ttf');}" +
//                        " body {background-color: " +
//                        "transparent;border: 0px;margin: 10px;padding: 0px;font-family: 'Montserrat-Regular'; font-size: 15px;width: 100%;" +
//                        "line-height: 150%;}</style></head><body dir='LTR'><div style='color: #8d6f4a;font-size:18px;text-align: center;font-weight: bold;'>" + title + "" + "</br>" +
//                        mainHTMLText + "<br /><br /><style type='text/css'> body {width:95%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html></div>"


                finalHTML =
                    "<html><head><style type='text/css'>@font-face {font-family: 'Montserrat-Regular';src: url('fonts/verdana.ttf');}" +
                            " body {background-color: " +
                            "transparent;border: 0px;margin: 10px;padding: 0px;font-family: 'Montserrat-Regular'; font-size: 15px;width: 100%;" +
                            "line-height: 150%;}</style></head><body ><div style='color: #8d6f4a;font-size:18px;text-align: center;font-weight: bold;'>" + title + "" +
                            mainHTMLText + "<style type='text/css'> body {width:95%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html></div>"

            } else {
                finalHTML =
                    "<html><head><style type='text/css'>@font-face {font-family: 'DroidSansArabic';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                            " body {background-color: " +
                            "transparent;border: 0px;margin: 10px;padding: 0px;font-family: 'DroidSansArabic'; font-size: 15px;width: 100%;" +
                            "line-height: 150%;}</style></head><body dir='RTL'><div style='color: #8d6f4a;font-size:18px;text-align: center;'>" + title + "" + "<br /><br/></div>" +
                            mainHTMLText + "<br /><br /><style type='text/css'> body {width:95%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>"
            }
            Log.d("finalHTML", "" + finalHTML)
            webView?.setBackgroundColor(Color.TRANSPARENT)
            webView?.loadDataWithBaseURL(
                "file:///android_asset/",
                finalHTML,
                "text/html",
                "UTF-8",
                null
            )
            webView?.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    var url = url
                    if (url?.startsWith("tel:")!!) {
                        val intent = Intent(Intent.ACTION_DIAL, Uri.parse(url))
                        act.startActivity(intent)
                        return true
                    } else if (url?.startsWith("mailto:")) {
                        url = url.substring(7)
                        val body = "Body of message."
                        val mail = Intent(Intent.ACTION_SEND)
                        mail.setType("application/octet-stream")
                        mail.putExtra(Intent.EXTRA_EMAIL, arrayOf(url))
                        mail.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                        mail.putExtra(Intent.EXTRA_TEXT, body)
                        act.startActivity(mail)
                        return true
                    } else if (url.startsWith("http:") || url.startsWith("https:")) {
                        view?.loadUrl(url)
                    } else if (url.startsWith("map:")) {
                        url = url.substring(4)
                        val map = "http://maps.google.com/maps?q=$url"
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(map))
                        act.startActivity(intent)
                        return true
                    }
                    return false
                }
            })
        }

        fun setupMoreIfoDialog1(title: String?, mainHTMLText: String?) {
            var mainHTMLText = mainHTMLText
            Log.d("NotificationHistory", "3")
            tv_bg?.setOnClickListener(View.OnClickListener { })
            relative_dialog?.setVisibility(View.VISIBLE)
            img_close_dialog?.setOnClickListener(View.OnClickListener {
                relative_dialog?.setVisibility(
                    View.GONE
                )
            })
            relative_dialog?.setOnClickListener(View.OnClickListener { })
            var finalHTML = ""
            if (mLangSessionManager?.getLang().equals("en", ignoreCase = true)) {
                mainHTMLText = mainHTMLText?.replace("font-".toRegex(), "f_nt")
                finalHTML =
                    "<html><head><style type='text/css'>@font-face {font-family: 'Montserrat-Regular';src: url('fonts/verdana.ttf');}" +
                            " body {background-color: " +
                            "transparent;border: 0px;margin: 10px;padding: 0px;font-family: 'Montserrat-Regular'; font-size: 15px;width: 100%;" +
                            "line-height: 150%;}</style></head><body dir='LTR'><div style='color: #8d6f4a;font-size:18px;text-align: center;font-weight: bold;'>" + title + "" + "</br>" +
                            mainHTMLText + "</div><br /><br /><style type='text/css'> body {width:95%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>"
            } else {
                finalHTML =
                    "<html><head><style type='text/css'>@font-face {font-family: 'DroidSansArabic';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                            " body {background-color: " +
                            "transparent;border: 0px;margin: 10px;padding: 0px;font-family: 'DroidSansArabic'; font-size: 15px;width: 100%;" +
                            "line-height: 150%;}</style></head><body dir='RTL'><div style='color: #8d6f4a;font-size:18px;text-align: center;'>" + title + "" + "<br /><br/>" +
                            mainHTMLText + "</div><br /><br /><style type='text/css'> body {width:95%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>"
            }
            Log.d("finalHTML", "" + finalHTML)
            webView?.setBackgroundColor(Color.TRANSPARENT)
            webView?.loadDataWithBaseURL(
                "file:///android_asset/",
                finalHTML,
                "text/html",
                "UTF-8",
                null
            )
            webView?.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    var url = url
                    if (url?.startsWith("tel:")!!) {
                        val intent = Intent(Intent.ACTION_DIAL, Uri.parse(url))
                        act.startActivity(intent)
                        return true
                    } else if (url?.startsWith("mailto:")) {
                        url = url?.substring(7)
                        val body = "Body of message."
                        val mail = Intent(Intent.ACTION_SEND)
                        mail.setType("application/octet-stream")
                        mail.putExtra(Intent.EXTRA_EMAIL, arrayOf(url))
                        mail.putExtra(Intent.EXTRA_SUBJECT, "Subject")
                        mail.putExtra(Intent.EXTRA_TEXT, body)
                        act.startActivity(mail)
                        return true
                    } else if (url.startsWith("http:") || url.startsWith("https:")) {
                        view?.loadUrl(url)
                    } else if (url.startsWith("map:")) {
                        url = url.substring(4)
                        val map = "http://maps.google.com/maps?q=$url"
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(map))
                        act.startActivity(intent)
                        return true
                    }
                    return false
                }
            })
        }

        private fun updateViews(languageCode: String?) {
            LocaleHelper.setLocale(act, languageCode)
        }

        override fun refreshRequestHelp() {
            //val frag: Fragment = MyRequestHelpFragment.newInstance(act)
            (myRequestHelpFragment as MyRequestHelpFragment).refreshList()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.KEY_RESULT_CODE) {
            val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
            fragment?.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDrawerItemSelected(position: Int, drawerItems: DrawerItems?) {
        when (drawerItems?.drawerItemType) {

            DrawerItems.DrawerItemType.Home -> {
                val bundle = Bundle()
                bundle.putString("comingFrom", "home")
                HomeFragment.getCategoriesArrayList.clear()
                openNewsFragment(bundle)

            }
            DrawerItems.DrawerItemType.Companies -> {

                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    openCompanyCategoryFragment()
                } else {
                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
                    openLoginFragment()
                }
            }
            DrawerItems.DrawerItemType.Deewaniya -> {
                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    openDeewaniyaFragment()
                } else {
                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
                    openLoginFragment()
                }
            }
            DrawerItems.DrawerItemType.ContactUs -> {
                openContactUsFragment()
            }
            DrawerItems.DrawerItemType.Donate -> {
                openDonateFragment()
            }
            DrawerItems.DrawerItemType.FamilyHistory -> {
                val b = Bundle()
                b.putString("type", "about_us")
                openAboutUsFragment(b)
            }
            DrawerItems.DrawerItemType.Familytree -> {
//                if (mSessionManager?.isLoggedin()!!
//                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
//                ) {
                val b = Bundle()
                b.putString("comingFrom", "home")
                openHomeFragment(b)
//                } else {
//                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
//                    openLoginFragment()
//                }
            }
            DrawerItems.DrawerItemType.Halls -> {
                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    val bundle = Bundle()
                    bundle.putString("type", "halls")
                    openHallFragment(bundle)
                } else {
                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
                    openLoginFragment()
                }
            }
            DrawerItems.DrawerItemType.Language -> {
                if (mLangSessionManager?.getLang().equals("en", ignoreCase = true)) {
                    mLangSessionManager?.setLang("ar")
                    startActivity(
                        Intent(this@ContentActivity, SplashActivity::class.java)
                            .putExtra("com.qenaat.fragType", 1)
                    )
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                } else {
                    mLangSessionManager?.setLang("en")
                    startActivity(
                        Intent(this@ContentActivity, SplashActivity::class.java)
                            .putExtra("com.qenaat.fragType", 1)
                    )
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                }
            }
            DrawerItems.DrawerItemType.News -> {

                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    val bundle = Bundle()
                    bundle.putString("comingFrom", "news")
                    openNewsFragment(bundle)
                } else {
                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
                    openLoginFragment()
                }
            }
            DrawerItems.DrawerItemType.Occasions -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
                val bundle = Bundle()
                bundle.putString("catId", "-1")
                openMonasbatFragment(bundle)
            }
//            DrawerItems.DrawerItemType.OccasionsDead -> {
//                if (isMenuOpen) {
//                    ToggleMenu(2, mfrm_mainFrame)
//                }
//                val bundle = Bundle()
//                bundle.putString("catId", "7")
//                openMonasbatFragment(bundle)
//            }
//            DrawerItems.DrawerItemType.Events -> {
//                if (isMenuOpen) {
//                    ToggleMenu(2, mfrm_mainFrame)
//                }
//                val bundle = Bundle()
//                bundle.putString("catId", "-1")
//                openMonasbatFragment(bundle)
//            }
            DrawerItems.DrawerItemType.Condolences -> {
                if (isMenuOpen) {
                    ToggleMenu(2, mfrm_mainFrame)
                }
                val bundle = Bundle()
                bundle.putString("catId", "7")
                openMonasbatFragment(bundle)
            }
            DrawerItems.DrawerItemType.RequestHelp -> {
                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    clearVariables()
                    val bundle = Bundle()
                    openMyRequestHelpFragment()
                } else {
                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
                    openLoginFragment()
                }
            }
            DrawerItems.DrawerItemType.Services -> {
                if (mSessionManager?.isLoggedin()!!
                    && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
                ) {
                    openServiceCategoryFragment()
                } else {
                    mcontent_frame?.showSnakeBar(getString(R.string.LoggedIn))
                    openLoginFragment()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        val fragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        fragment?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onRestart() {
        super.onRestart()

        CheckTestingVersion()
    }

}