package com.qenaat.app.adapters

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetHalls
import com.squareup.picasso.Picasso
import java.util.*

/**
 * Created by DELL on 09-Nov-17.
 */
class HallAdapter(a: FragmentActivity, itemsData: ArrayList<GetHalls>, type: String) : RecyclerView.Adapter<HallAdapter.ViewHolder?>() {
    private val itemsData: ArrayList<GetHalls>
    var act: FragmentActivity
    var languageSeassionManager: LanguageSessionManager
    var type: String = ""

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hall_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.tv_date.setVisibility(View.INVISIBLE)
            if (itemsData.get(position).BookingDate?.length!! > 0) {
                viewHolder.tv_date.setVisibility(View.VISIBLE)
                viewHolder.tv_date.setText(
                        if (itemsData.get(position).BookingDate?.split(" ".toRegex())?.toTypedArray()?.isNotEmpty()!!)
                            itemsData.get(position).BookingDate!!.split(" ".toRegex()).toTypedArray()[0]
                        else "")
            }
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                viewHolder.tv_title.setText(itemsData.get(position).HallNameEN)
                viewHolder.tv_address.setText(itemsData.get(position).HallAddressEN)
            } else {
                viewHolder.tv_title.setText(itemsData.get(position).HallNameAR)
                viewHolder.tv_address.setText(itemsData.get(position).HallAddressAR)
            }
            viewHolder.img_hall.setImageResource(R.drawable.box_img_no_img)

//            viewHolder.img_hall.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                    R.drawable.box_img_no_img)).getBitmap().getWidth();
            viewHolder.img_hall.getLayoutParams().height = (act.getResources().getDrawable(
                    R.drawable.box_img_no_img) as BitmapDrawable).bitmap.height
            if (itemsData.get(position).Photo?.length!! > 0) Picasso.with(act)
                    .load(itemsData.get(position).Photo)
                    .error(R.drawable.box_img_no_img)
                    .placeholder(R.drawable.box_img_no_img)
                    .config(Bitmap.Config.RGB_565).fit()
                    .into(viewHolder.img_hall)
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetHalls", gson.toJson(itemsData.get(position)))
                b.putString("type", "" + type)
                ContentActivity.Companion.openHallDetailsFragment(b)
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_hall: ImageView
        var img_bg: ImageView
        var img_location: ImageView
        var img_indicator: ImageView
        var relative_parent: RelativeLayout
        var tv_title: TextView
        var tv_address: TextView
        var tv_date: TextView

        init {
            tv_date = itemLayoutView.findViewById<View?>(R.id.tv_date) as TextView
            tv_address = itemLayoutView.findViewById<View?>(R.id.tv_address) as TextView
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            img_hall = itemLayoutView.findViewById<View?>(R.id.img_hall) as ImageView
            img_location = itemLayoutView.findViewById<View?>(R.id.img_location) as ImageView
            img_indicator = itemLayoutView.findViewById<View?>(R.id.img_indicator) as ImageView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        this.type = type
        act = a
        this.itemsData = itemsData
        languageSeassionManager = LanguageSessionManager(act)
    }
}