package com.qenaat.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetHalls
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by DELL on 11-Nov-17.
 */
class HallDetailsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var relative_top: RelativeLayout
    lateinit var relative_bottom: RelativeLayout
    lateinit var linear_date: LinearLayout
    lateinit var ll_location: LinearLayout
    lateinit var ll_phone: LinearLayout
    lateinit var img_call: ImageView
    lateinit var img_share: ImageView
    lateinit var img_book_hall: ImageView
    lateinit var img_event: ImageView
    lateinit var img_location: ImageView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var tv_occasion_details: TextView
    lateinit var tv_occasion_title: TextView
    lateinit var tv_occasion_price: TextView
    lateinit var tv_book_hall: TextView
    lateinit var getHalls: GetHalls
    lateinit var productImages: Array<String>
    private val CALL_PHONE_PERMISSION_CODE = 23
    lateinit var pShareUri: Uri
    lateinit var file: File
    lateinit var img_count: TextView
    lateinit var relative_gallery: RelativeLayout
    lateinit var tv_occasion_date: TextView
    var type: String = ""
    var permissionStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments?.containsKey("GetHalls")!!) {
                val gson = Gson()
                getHalls = gson.fromJson(arguments?.getString("GetHalls"),
                        GetHalls::class.java)
                type = arguments?.getString("type")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.hall_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        relative_gallery = mainLayout.findViewById(R.id.relative_gallery) as RelativeLayout
        img_count = mainLayout.findViewById(R.id.img_count) as TextView
        img_location = mainLayout.findViewById(R.id.img_location) as ImageView
        img_event = mainLayout.findViewById(R.id.img_event) as ImageView
        img_share = mainLayout.findViewById(R.id.img_share) as ImageView
        img_book_hall = mainLayout.findViewById(R.id.img_book_hall) as ImageView
        img_call = mainLayout.findViewById(R.id.img_call) as ImageView
        mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
        linear_date = mainLayout.findViewById(R.id.linear_date) as LinearLayout
        ll_phone = mainLayout.findViewById(R.id.ll_phone) as LinearLayout
        ll_location = mainLayout.findViewById(R.id.ll_location) as LinearLayout
        relative_bottom = mainLayout.findViewById(R.id.relative_bottom) as RelativeLayout
        relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
        tv_book_hall = mainLayout.findViewById(R.id.tv_book_hall) as TextView
        tv_occasion_date = mainLayout.findViewById(R.id.tv_occasion_date) as TextView
        tv_book_hall.setTypeface(ContentActivity.Companion.tf)
        tv_occasion_title = mainLayout.findViewById(R.id.tv_occasion_title) as TextView
        tv_occasion_title.setTypeface(ContentActivity.Companion.tf)
        tv_occasion_details = mainLayout.findViewById(R.id.tv_occasion_details) as TextView
        tv_occasion_details.setTypeface(ContentActivity.Companion.tf)
        tv_occasion_price = mainLayout.findViewById(R.id.tv_occasion_price) as TextView
        tv_occasion_price.setTypeface(ContentActivity.Companion.tf)
        tv_occasion_price.setVisibility(View.GONE)
        ll_location.setOnClickListener(this)
        relative_bottom.setOnClickListener(this)
        img_share.setOnClickListener(this)
        img_share.setVisibility(View.GONE)
        ll_phone.setOnClickListener(this)
        img_event.setOnClickListener(this)
        ContentActivity.Companion.setTextFonts(mainLayout)
    }

    override fun onStart() {
        super.onStart()
        //    ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)

        ContentActivity.Companion.bottomBarView.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        if (type.equals("my account", ignoreCase = true)) {
            relative_bottom.setVisibility(View.GONE)
            GetBookedHalls()
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else {
            GetHalls()
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.relative_bottom -> {
                val bundle = Bundle()
                val gson = Gson()
                bundle.putString("GetHalls", gson.toJson(getHalls))
                ContentActivity.Companion.openCalendarHallFragment(bundle)
            }
            R.id.img_share -> {

//                final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
//                        "AlQenaat @ " + getHalls.getCompanyCategoryNameEn() + " - " + getHalls.getDescriptionEn() + "\n\n" + getHalls.getPhotos() + "\n\n" + act.getString(R.string.UrlLink)
//                        : "AlQenaat @ " + getHalls.getCompanyCategoryNameEn() + " - " + getHalls.getDescriptionAr() + "\n\n" + getHalls.getPhotos() + "\n\n" + act.getString(R.string.UrlLink);

/*                final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
                    act.getString(R.string.CheckLabel)+  " " + act.getString(R.string.HallLabel) + " at LobQ8 app \n\n " + getHalls.getContentLink() + "\n\n" + act.getString(R.string.UrlLink)
                    : act.getString(R.string.CheckLabel)+  " " + act.getString(R.string.HallLabel) + " at LobQ8 app \n\n " + getHalls.getContentLink() + "\n\n" + act.getString(R.string.UrlLink);
*/
                val shareProductInfo = if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) """
     ${getHalls.HallNameEN}

     ${getHalls.HallDescriptionEN}

     ${act.getString(R.string.UrlLink1)}
     """.trimIndent() else """
     ${getHalls.HallNameAR}

     ${getHalls.HallDescriptionAR}

     ${act.getString(R.string.UrlLink1)}
     """.trimIndent()
                val ob = GetAndSaveBitmapForArticle()
                val clipboard = act.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clip = ClipData.newPlainText("AlQenaat", shareProductInfo)
                clipboard.setPrimaryClip(clip)
                ob.execute(getHalls.Photo)
                val dialog = Dialog(act)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.setContentView(R.layout.share_dialog)
                dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
                val mimg_close: ImageView
                val mimg_facebook: ImageView
                val mimg_twitter: ImageView
                val mimg_instagram: ImageView
                val mimg_email: ImageView
                val mimg_sms: ImageView
                val mimg_whats: ImageView
                mimg_facebook = dialog.findViewById(R.id.img_facebook) as ImageView
                val title = dialog.findViewById(R.id.shartext) as TextView
                title.setTypeface(ContentActivity.Companion.tf)
                mimg_twitter = dialog.findViewById(R.id.img_twitter) as ImageView
                mimg_instagram = dialog.findViewById(R.id.img_instagram) as ImageView
                mimg_email = dialog.findViewById(R.id.img_email) as ImageView
                mimg_sms = dialog.findViewById(R.id.img_sms) as ImageView
                mimg_whats = dialog.findViewById(R.id.img_whats) as ImageView
                mimg_close = dialog.findViewById(R.id.img_searchClose) as ImageView
                mimg_close.setOnClickListener(View.OnClickListener {
                    file.delete()
                    dialog.dismiss()
                })
                mimg_facebook.setOnClickListener(View.OnClickListener { /*Share(1,
                                "com.facebook.katana",
                                products.getDecription() + "\n" +
                                        products.getPhotos());*/
                    dialog.dismiss()
                    if (mSessionManager.isFacebookOpenBefore()) {
                        Share(1,
                                "com.facebook.katana",
                                shareProductInfo)
                    } else {
                        AlertDialog.Builder(act)
                                .setTitle(act.getString(R.string.AttentionLabel))
                                .setMessage(act.getString(R.string.AttentionText))
                                .setPositiveButton(android.R.string.yes) { dialog, which ->
                                    Share(1,
                                            "com.facebook.katana",
                                            shareProductInfo)
                                    mSessionManager.FacebookOpened()
                                }
                                .setIcon(R.drawable.icon_512)
                                .show()
                    }
                })
                mimg_twitter.setOnClickListener(View.OnClickListener {
                    file.delete()
                    dialog.dismiss()
                    Share(5,
                            "twitter",
                            shareProductInfo)
                })
                mimg_instagram.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()
                    AlertDialog.Builder(act)
                            .setIcon(R.drawable.icon_512)
                            .setTitle(act.getString(R.string.AttentionLabel))
                            .setMessage(act.getString(R.string.AttentionText))
                            .setPositiveButton(android.R.string.yes) { dialog, which ->
                                Share(6,
                                        "instagram",
                                        shareProductInfo)
                            }
                            .setIcon(R.drawable.icon_512)
                            .show()
                })
                mimg_email.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()
                    file.delete()
                    Share(3,
                            "gmail",
                            shareProductInfo)
                })
                mimg_sms.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()
                    file.delete()
                    Share(4,
                            "sms",
                            shareProductInfo)
                })
                mimg_whats.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()
                    file.delete()
                    Share(2,
                            "whatsapp",
                            shareProductInfo)
                })
                dialog.show()
            }
            R.id.ll_phone -> {
                /*if (GlobalFunctions.isReadCallAllowed(act)) {
                    if (getHalls.Phone!!.isNotEmpty()) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.data = Uri.parse("tel:" + getHalls.Phone)
                        act.startActivity(intent)
                    }
                    return
                }*/
                if (getHalls.Phone!!.isNotEmpty()) {
                    permissionStr = Manifest.permission.CALL_PHONE
                    GlobalFunctions.requestCallPermission(act)
                } else
                    FixControl.showToast(act, getString(R.string.no_number_found))
            }
            R.id.img_event -> if (getHalls.Photo!!.length > 0) {

                /*productImages = new String[getHalls.getHallPhotos().size()];

                    for(int i=0; i<getHalls.getHallPhotos().size(); i++){

                        GetHalls.HallPhotos photo = getHalls.getHallPhotos().get(i);

                        productImages[i] = photo.getPhotos();

                    }*/
                productImages = arrayOf<String>(1.toString())
                productImages[0] = getHalls.Photo!!
                val b = Bundle()
                b.putInt("position", 0)
                b.putStringArray("productImages", productImages)
                ContentActivity.Companion.openGalleryFragmentFadeIn(b)
            }
            R.id.ll_location -> {
                /* if (GlobalFunctions.isGPSAllowed(act)) {
                     if (getHalls.Latitude!!.length > 0 && getHalls.Longitude!!.length > 0) {
                         val gson = Gson()
                         val bundle = Bundle()
                         val hallsArrayList = ArrayList<GetHalls?>()
                         hallsArrayList.add(getHalls)
                         bundle.putString("GetHalls", gson.toJson(hallsArrayList))
                         ContentActivity.Companion.openShowHallMapFragment(bundle)
                     }
                     return
                 }*/
                if (getHalls.Latitude!!.isNotEmpty() && getHalls.Longitude!!.isNotEmpty()) {
                    permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                    GlobalFunctions.requestGPSPermission(act)
                } else
                    FixControl.showToast(act, getString(R.string.no_location_found))
            }
        }
    }

    fun GetHalls() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetHalls(getHalls.Id, "0")?.enqueue(
                object : Callback<ArrayList<GetHalls>> {

                    override fun onFailure(call: Call<ArrayList<GetHalls>>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetHalls>>, response: Response<ArrayList<GetHalls>>) {
                        if (response.body() != null) {
                            val getHallses = response.body()
                            if (mloading != null) {
                                if (getHallses != null) {
                                    if (getHallses.size > 0) {
                                        getHalls = getHallses[0]!!

//                            tv_occasion_date.setVisibility(View.GONE);
//
//                            if(getHalls.getBookingDate().length()>0){
//
//                                tv_occasion_date.setVisibility(View.VISIBLE);
//
//                                tv_occasion_date.setText(act.getString(R.string.BookingDateLabel) + " " + (getHalls.getBookingDate().split(" ").length> 0 ? getHalls.getBookingDate().split(" ")[0] : ""));
//
//                            }

//                            img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                                    R.drawable.no_img_details)).getBitmap().getWidth();
                                        img_event.getLayoutParams().height = (act.getResources().getDrawable(
                                                R.drawable.no_img_details) as BitmapDrawable).bitmap.height
                                        if (getHalls.Photo!!.length > 0) {
                                            Picasso.with(act).load(getHalls.Photo).placeholder(R.drawable.no_img_details)
                                                    .error(R.drawable.no_img_details).into(img_event)
                                        }
                                        relative_gallery.setVisibility(View.INVISIBLE)
                                        //                        if(getHalls.getHallPhotos().size()>=2){
//                            relative_gallery.setVisibility(View.VISIBLE);
//                            img_count.setText(getHalls.getHallPhotos().size()+"");
//                        }

                                        // tv_occasion_price.setText(getHalls.getBookValue() + act.getString(R.string.KD));
                                        if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                                            tv_occasion_title.setText(getHalls.HallNameEN)
                                            tv_occasion_details.setText(getHalls.HallDescriptionEN)
                                        } else {
                                            tv_occasion_title.setText(getHalls.HallNameAR)
                                            tv_occasion_details.setText(getHalls.HallDescriptionAR)
                                        }
                                    }
                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    fun GetBookedHalls() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetBookedHalls(
                "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                getHalls.Id,
                "0",
                "0")?.enqueue(
                object : Callback<ArrayList<GetHalls>> {

                    override fun onFailure(call: Call<ArrayList<GetHalls>>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetHalls>>, response: Response<ArrayList<GetHalls>>) {
                        if (response.body() != null) {
                            val getHallses = response.body()
                            if (mloading != null) {
                                if (getHallses != null) {
                                    if (getHallses.size > 0) {
                                        // getHalls = getHallses[0]!!
                                        tv_occasion_date.setVisibility(View.GONE)
                                        if (getHalls.BookingDate!!.length > 0) {
                                            tv_occasion_date.setVisibility(View.VISIBLE)
                                            tv_occasion_date.setText(act.getString(R.string.BookingDateLabel) + " " + if (getHalls.BookingDate!!.split(" ".toRegex()).toTypedArray().size > 0) getHalls.BookingDate!!.split(" ".toRegex()).toTypedArray()[0] else "")
                                        }

//                            img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                                    R.drawable.no_img_details)).getBitmap().getWidth();
                                        img_event.getLayoutParams().height = (act.getResources().getDrawable(
                                                R.drawable.no_img_details) as BitmapDrawable).bitmap.height
                                        if (getHalls.Photo!!.length > 0) {
                                            Picasso.with(act).load(getHalls.Photo).placeholder(R.drawable.no_img_details)
                                                    .error(R.drawable.no_img_details).into(img_event)
                                        }
                                        relative_gallery.setVisibility(View.INVISIBLE)
                                        //                        if(getHalls.getHallPhotos().size()>=2){
//                            relative_gallery.setVisibility(View.VISIBLE);
//                            img_count.setText(getHalls.getHallPhotos().size()+"");
//                        }

                                        // tv_occasion_price.setText(getHalls.getBookValue() + act.getString(R.string.KD));
                                        if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                                            tv_occasion_title.setText(getHalls.HallNameEN)
                                            tv_occasion_details.setText(getHalls.HallDescriptionEN)
                                        } else {
                                            tv_occasion_title.setText(getHalls.HallNameAR)
                                            tv_occasion_details.setText(getHalls.HallDescriptionAR)
                                        }
                                    }
                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    // shareType : 1-fb , 5-twitter , 6-Instgram  , 2-wtsapp  , 3-gmail  , 4sms
    @SuppressLint("DefaultLocale")
    private fun Share(shareType: Int, appName: String?, content: String?) {
        val targetedShareIntents: MutableList<Intent?> = ArrayList()
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        val resInfo = act.getPackageManager().queryIntentActivities(share, 0)
        if (!resInfo.isEmpty()) {
            when (shareType) {
                1 -> {
                    Log.d("inside fbshare", "test0001")
                    // facebook
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info.activityInfo.packageName.toLowerCase().contains(appName!!) || info.activityInfo.name.toLowerCase().contains(appName)) {
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        act.startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Facebook is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                5 -> {
                    // twitter
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info.activityInfo.packageName.toLowerCase().contains(appName!!) || info.activityInfo.name.toLowerCase().contains(appName)) {
                            Log.e("Twitter", "Twitter-Data: $content")
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)

//                            Uri uri = Uri.parse(products.getPhotos());
//                            targetedShare.putExtra(Intent.EXTRA_STREAM, String.valueOf(uri));
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Twitter is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                6 ->                     //Instagram
                    if (1 == 1) {
                        try {
                            Log.d("product_1_image", getHalls.Photo)
                            val shareIntent = Intent(Intent.ACTION_SEND)
                            shareIntent.type = "image/*"
                            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            shareIntent.putExtra(Intent.EXTRA_STREAM, pShareUri)
                            shareIntent.putExtra(Intent.EXTRA_TEXT, content)
                            shareIntent.setPackage("com.instagram.android")
                            targetedShareIntents.add(shareIntent)
                            val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                            startActivity(chooserIntent)
                        } catch (e: Exception) {
                            Toast.makeText(act, "Instagram is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                        }
                    }
                2 -> {
                    // whatsapp
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        // put here your mime type
                        if (info.activityInfo.packageName.toLowerCase().contains(
                                        appName!!)
                                || info.activityInfo.name.toLowerCase().contains(
                                        appName)) {
                            Log.d("whatsapp_share", content)
                            targetedShare.putExtra(Intent.EXTRA_TEXT, """
     $content

     ${getHalls.Photo}
     """.trimIndent())
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "WhatsApp is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                3 -> {
                    // Gmail
                    val txt = "<img src=\"" + getHalls.Photo + "\" width=\"600px\"  height=\"600px\"/>"
                    Log.e("Email", "Email txt = $txt")
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/html"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, act.getString(R.string.app_name))
                    shareIntent.putExtra(Intent.EXTRA_TEXT, """$content
 ${Html.fromHtml(txt)}""")
                    for (app in resInfo) {
                        if (app.activityInfo.name.toLowerCase().contains(appName!!)) {
                            val activity = app.activityInfo
                            val name = ComponentName(activity.applicationInfo.packageName, activity.name)
                            shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                            shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                            shareIntent.component = name
                            act.startActivity(shareIntent)
                            break
                        }
                    }
                }
                4 -> {
                    //SMS
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("smsto:")
                    smsIntent.type = "vnd.android-dir/mms-sms"
                    smsIntent.putExtra(Intent.EXTRA_SUBJECT, """
     ${act.getString(R.string.app_name)}
     $content
     """.trimIndent())
                    smsIntent.putExtra("sms_body", content)
                    try {
                        startActivity(smsIntent)
                        Log.i("Finished sending SMS...", "")
                    } catch (ex: ActivityNotFoundException) {
                    }
                }
                else -> {
                }
            }
        }
    }

    internal inner class GetAndSaveBitmapForArticle : AsyncTask<String?, Void?, Uri?>() {
        override fun doInBackground(vararg params: String?): Uri? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/AlQenaat"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            file = File(dir, UUID.randomUUID().toString() + ".png")
            val fOut: FileOutputStream
            try {
                val bm = Picasso.with(act).load(params[0]).get()
                fOut = FileOutputStream(file)
                bm.compress(Bitmap.CompressFormat.PNG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            pShareUri = FileProvider.getUriForFile(act!!,
                    act?.applicationContext?.packageName + ".fileprovider", file!!)
            return pShareUri
        }
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.CALL_PERMISSION_CODE -> {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:" + getHalls.Phone)
                    act.startActivity(intent!!)
                }
                GlobalFunctions.LOCATION_PERMISSION_CODE -> {
                    val gson = Gson()
                    val bundle = Bundle()
                    val hallsArrayList = ArrayList<GetHalls?>()
                    hallsArrayList.add(getHalls)
                    bundle.putString("GetHalls", gson.toJson(hallsArrayList))
                    ContentActivity.Companion.openShowHallMapFragment(bundle)
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        protected val TAG = HallDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: HallDetailsFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): HallDetailsFragment? {
            fragment = HallDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}