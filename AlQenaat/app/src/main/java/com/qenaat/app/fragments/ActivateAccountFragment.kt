package com.qenaat.app.fragments

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader

/**
 * Created by DELL on 25-May-17.
 */
class ActivateAccountFragment : Fragment(), View.OnClickListener {
    var XY: IntArray? = null
    var mainLayout: RelativeLayout? = null
    var mSessionManager: SessionManager? = null
    var languageSeassionManager: LanguageSessionManager? = null
    var mloading: ProgressBar? = null
    var et_mobile: EditText? = null
    var et_code: EditText? = null
    var et_username: EditText? = null
    var tv_btnactivate: TextView? = null
    var tv_country: TextView? = null
    var tv_activate_label: TextView? = null
    var tv_activate: TextView? = null
    var tv_btnResendCode: TextView? = null
    var tv_btnChangeMobile: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.activate_account, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " onCreate>>LineNumber: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout?) {
        if (mainLayout != null) {
            tv_btnResendCode = mainLayout.findViewById(R.id.tv_btnResendCode) as TextView
            tv_btnChangeMobile = mainLayout.findViewById(R.id.tv_btnChangeMobile) as TextView
            tv_activate_label = mainLayout.findViewById(R.id.tv_activate_label) as TextView
            tv_activate = mainLayout.findViewById(R.id.tv_activate) as TextView
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            et_mobile = mainLayout.findViewById(R.id.et_mobile) as EditText
            et_code = mainLayout.findViewById(R.id.et_code) as EditText
            et_username = mainLayout.findViewById(R.id.et_username) as EditText
            tv_btnactivate = mainLayout.findViewById(R.id.tv_btnactivate) as TextView
            tv_btnactivate?.setOnClickListener(this)
            tv_country = mainLayout.findViewById(R.id.tv_country) as TextView
            tv_country?.setOnClickListener(this)
            tv_btnChangeMobile?.setOnClickListener(this)
            tv_btnResendCode?.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_btnResendCode?.setTypeface(ContentActivity.Companion.tf)
            tv_btnChangeMobile?.setTypeface(ContentActivity.Companion.tf)
            et_mobile?.setTypeface(ContentActivity.Companion.tf)
            et_username?.setTypeface(ContentActivity.Companion.tf)
            et_code?.setTypeface(ContentActivity.Companion.tf)
            tv_country?.setTypeface(ContentActivity.Companion.tf)
            tv_activate?.setTypeface(ContentActivity.Companion.tf)
            tv_activate_label?.setTypeface(ContentActivity.Companion.tf)
        }
    }

    override fun onStart() {
        super.onStart()
        //ContentActivity.Companion.topBarView?.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAddAd?.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.mtv_topTitle?.setText(R.string.ActivateAccountLabel)
        if (mSessionManager?.isLoggedin()!!
                && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null) {
            fragmentManager?.popBackStack()
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.tv_btnChangeMobile -> {

                //ContentActivity.openChangeMobileFragment();
                val bundle = Bundle()
                bundle.putString("type", "register")
            }
            R.id.tv_btnResendCode -> {
                mloading?.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
                QenaatAPICall.getCallingAPIInterface()?.resendSMS(mSessionManager?.getUserCode())?.enqueue(
                        object : Callback<ResponseBody?> {
                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                t.printStackTrace()
                                mloading?.setVisibility(View.INVISIBLE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                val body = response?.body()
                                var outResponse = ""
                                try {
                                    val reader = BufferedReader(InputStreamReader(
                                            ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                if (outResponse != null) {
                                    outResponse = outResponse.replace("\"", "")
                                    outResponse = outResponse.replace("\n", "")
                                    Log.e("outResponse not null ", outResponse)
                                    if (outResponse.toInt() > 0) {
                                        //mSessionManager.LoginSeassion();
                                        mSessionManager?.setUserCode(outResponse)
                                        AlertDialog.Builder(act!!)
                                                .setMessage(act?.getString(R.string.ResendActivationCode))
                                                .setPositiveButton(android.R.string.ok, object : DialogInterface.OnClickListener {
                                                    override fun onClick(dialog: DialogInterface?, which: Int) {
                                                        dialog?.dismiss()
                                                    }
                                                })
                                                .setIcon(R.drawable.icon_512)
                                                .show()
                                    } else if (outResponse == "-1") {
                                        Snackbar.make(mainLayout!!, act?.getString(R.string.OperationFailed).toString(), Snackbar.LENGTH_LONG).show()
                                    } else if (outResponse == "-2") {
                                        Snackbar.make(mainLayout!!, act?.getString(R.string.DataMissing).toString(), Snackbar.LENGTH_LONG).show()
                                    } else if (outResponse == "-3") {
                                        Snackbar.make(mainLayout!!, act?.getString(R.string.UserNotExistsLabel).toString(), Snackbar.LENGTH_LONG).show()
                                    } else if (outResponse == "-4") {
                                        Snackbar.make(mainLayout!!, act?.getString(R.string.AlreadyActivated).toString(), Snackbar.LENGTH_LONG).show()
                                    }
                                }
                                mloading?.setVisibility(View.INVISIBLE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }
                        })
            }
            R.id.tv_btnactivate ->                 //Snackbar.make(mainLayout, "Login please wait", Snackbar.LENGTH_LONG).show();
                if (et_code?.getText().toString().length > 0
                        && et_username?.getText().toString().trim({ it <= ' ' }).length > 0) {
                    mloading?.setVisibility(View.VISIBLE)
                    GlobalFunctions.DisableLayout(mainLayout)
                    QenaatAPICall.getCallingAPIInterface()?.activate(
                            et_username?.getText().toString().trim({ it <= ' ' }),
                            et_code?.getText().toString())?.enqueue(object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            mloading?.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            val body = response?.body()
                            var outResponse = ""
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                outResponse = out.toString()
                                Log.d("outResponse", "" + outResponse)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (outResponse.toInt() > 0) {
                                    //mSessionManager.LoginSeassion();
                                    //mSessionManager.setUserCode(outResponse);
                                    Snackbar.make(mainLayout!!, act!!.getString(R.string.ActivateMessage), Snackbar.LENGTH_LONG).show()
                                    act?.supportFragmentManager?.popBackStack()
                                } else if (outResponse == "-1") {
                                    Snackbar.make(mainLayout!!, act!!.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout!!, act!!.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-3") {
                                    Snackbar.make(mainLayout!!, act!!.getString(R.string.UserNotExistsLabel), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-4") {
                                    Snackbar.make(mainLayout!!, act!!.getString(R.string.CodeIncorrectLabel), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-5") {
                                    Snackbar.make(mainLayout!!, act!!.getString(R.string.AlreadyActivated), Snackbar.LENGTH_LONG).show()
                                }
                            }
                            mloading?.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
                } else {
                    Snackbar.make(mainLayout!!, act!!.getString(R.string.FillAllFields), Snackbar.LENGTH_LONG).show()
                }
            R.id.tv_country -> {
            }
        }
    }

    companion object {
        protected val TAG = ActivateAccountFragment::class.java.simpleName
        var act: FragmentActivity? = null
        var fragment: ActivateAccountFragment? = null

        //    ArrayList<GetCountries> getCountriesArrayList = new ArrayList<>();
        fun newInstance(act: FragmentActivity?): ActivateAccountFragment {
            fragment = ActivateAccountFragment()
            Companion.act = act
            return fragment!!
        }
    }
}