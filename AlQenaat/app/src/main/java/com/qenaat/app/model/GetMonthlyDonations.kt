package com.qenaat.app.model

import kotlin.jvm.Throws

/**
 * Created by DELL on 14-Nov-17.
 */
class GetMonthlyDonations : Cloneable {
    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Any {
        return super.clone()
    }

    var Id: String? = null
    var FromDate: String? = null
    var Finished: String? = null
    var Canceled: String? = null
    var Decription: String? = null
    var UserName: String? = null
    var UserId: String? = null
    var Amount: String? = null
    var Title: String? = null
    var NumberOfMonths: String? = null
}