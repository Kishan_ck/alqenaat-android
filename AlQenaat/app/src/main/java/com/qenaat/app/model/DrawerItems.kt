package com.qenaat.app.model

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.SessionManager
import java.io.Serializable
import java.util.*

class DrawerItems private constructor(var drawerItemType: DrawerItemType, var displayText: String) :
    Serializable {
    var img_icon = 0
    var fragment: Fragment? = null

    enum class DrawerItemType {
        Home, Familytree, Occasions,/*Events,*/ Condolences, /*Occasions, OccasionsDead,*/ News, Donate, RequestHelp, Halls, Deewaniya, Services, Companies, FamilyHistory, ContactUs, Language
    }


    companion object {
        fun initSellerDrawerItems(context: Context, boolean: Boolean): ArrayList<DrawerItems> {
            val drawerItemsArrayList = ArrayList<DrawerItems>()

            if (boolean) {

                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Home,
                        context.getString(R.string.HomeLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Familytree,
                        context.getString(R.string.TreeLabel)
                    )
                )
            drawerItemsArrayList.add(DrawerItems(DrawerItemType.Occasions, context.getString(R.string.OccasionLabel)))
//                drawerItemsArrayList.add(
//                    DrawerItems(
//                        DrawerItemType.Events,
//                        context.getString(R.string.EventsLabel)
//                    )
//                )
//            drawerItemsArrayList.add(DrawerItems(DrawerItemType.OccasionsDead, context.getString(R.string.OccasionDeadLabel)))
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Condolences,
                        context.getString(R.string.CondolencesLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.News,
                        context.getString(R.string.NewsLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Donate,
                        context.getString(R.string.DonationeLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.RequestHelp,
                        context.getString(R.string.RequestHelpLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Halls,
                        context.getString(R.string.HallsLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Deewaniya,
                        context.getString(R.string.DeewaniyaLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Services,
                        context.getString(R.string.ServicesLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Companies,
                        context.getString(R.string.CompaniesLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.FamilyHistory,
                        context.getString(R.string.AboutFamilyLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.ContactUs,
                        context.getString(R.string.ContactUsLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Language,
                        context.getString(R.string.LangLabel)
                    )
                )

            } else {

                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Home,
                        context.getString(R.string.HomeLabel)
                    )
                )

                drawerItemsArrayList.add(DrawerItems(DrawerItemType.Occasions, context.getString(R.string.OccasionLabel)))
//                drawerItemsArrayList.add(
//                    DrawerItems(
//                        DrawerItemType.Events,
//                        context.getString(R.string.EventsLabel)
//                    )
//                )

                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Condolences,
                        context.getString(R.string.CondolencesLabel)
                    )
                )

                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.ContactUs,
                        context.getString(R.string.ContactUsLabel)
                    )
                )
                drawerItemsArrayList.add(
                    DrawerItems(
                        DrawerItemType.Language,
                        context.getString(R.string.LangLabel)
                    )
                )
            }
            return drawerItemsArrayList

        }
    }

}



