package com.qenaat.app.model

/**
 * Created by DELL on 29-Oct-17.
 */
class GetAppSettings {
    var Fax: String? = null
    var Id: String? = null
    var Title: String? = null
    var Details: String? = null
    var Website: String? = null
    var AboutUsEn: String? = null
    var AboutUsAr: String? = null
    var TermsEn: String? = null
    var TermsAr: String? = null
    var AdsPrices: String? = null
    var Whatsapp: String? = null
    var Address: String? = null
    var Email: String? = null
    var Phone: String? = null
    var Facebook: String? = null
    var Instagram: String? = null
    var ForPopTest: String? = null
    var StartPopText: String? = null
    var ShowActivePage = false
    var SnapChat: String? = null
    var Twitter: String? = null
    var Youtube: String? = null
    var OneOccColor: String? = null
    var TwoOccsColor: String? = null
    var MoreOccsColor: String? = null
    var HallGenColor: String? = null
    var PendingDays: String? = null
    var ShowOccsAsNews: String? = null
    var NewsSlider: String? = null
    var OccasionsSlider: String? = null
    var NAdsSlider: String? = null
    var OAdsSlider: String? = null
    var NewsMarquee: String? = null
    var OccasionsMarquee: String? = null
    var NBtwAds: String? = null
    var OBtwAd: String? = null
    var DBtwAdss: String? = null
    var OBtwAds: String? = null
    var DBtwAds: String? = null
    var PageSize: String? = null
    var AppStore: String? = null
    var GooglePlay: String? = null
    var EventText: String? = null
    var EventPhone: String? = null
    var android_version: String? = null
    var android_is_force_update: String? = null
    var android_message: String? = null
}