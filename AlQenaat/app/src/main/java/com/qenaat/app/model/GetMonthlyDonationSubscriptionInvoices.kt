package com.qenaat.app.model

import kotlin.jvm.Throws

/**
 * Created by DELL on 05-Dec-17.
 */
class GetMonthlyDonationSubscriptionInvoices : Cloneable {
    @Throws(CloneNotSupportedException::class)
    public override fun clone(): Any {
        return super.clone()
    }

    var Id: String? = null
    var MonthlyDonationSubscriptionId: String? = null
    var NotificationSend: String? = null
    var Canceled: String? = null
    var PaymentDate: String? = null
    var InvoceDate: String? = null
    var IsPaid: String? = null
    var Amount: String? = null
}