package com.qenaat.app.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetCompanyCategories
import java.util.*

/**
 * Created by DELL on 16-Nov-17.
 */
class CompanyCategoryAdapter(var act: FragmentActivity,
                             private val itemsData: ArrayList<GetCompanyCategories?>, var tabNumber: Int) : RecyclerView.Adapter<CompanyCategoryAdapter.ViewHolder?>() {

    var languageSeassionManager: LanguageSessionManager

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.tv_price.setVisibility(View.INVISIBLE)
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                viewHolder.tv_title.setText(itemsData.get(position)?.CompanyCategoryNameEn)
            } else {
                viewHolder.tv_title.setText(itemsData.get(position)?.CompanyCategoryNameAr)
            }
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetCompanyCategories", gson.toJson(itemsData.get(position)))
                b.putString("comingFrom", "category")
                b.putInt("tabNumber", tabNumber)
                ContentActivity.Companion.openCompanyFragment(b)
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var tv_title: TextView
        var tv_price: TextView

        init {
            tv_price = itemLayoutView.findViewById<View?>(R.id.tv_price) as TextView
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_parent)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}