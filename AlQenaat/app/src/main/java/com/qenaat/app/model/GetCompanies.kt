package com.qenaat.app.model

import java.util.*

/**
 * Created by DELL on 16-Nov-17.
 */
class GetCompanies {
    var IsActive: String? = null
    var ForFamily: String? = null
    var AdminUserId: String? = null
    var Id: String? = null
    var TitleEN: String? = null
    var TitleAR: String? = null
    var DetailsEN: String? = null
    var DetailsAR: String? = null
    var CompanyCategoryId: String? = null
    var CompanyCategoryNameEn: String? = null
    var CompanyCategoryNameAr: String? = null
    var CompanyLogo: String? = null
    var Email: String? = null
    var Telephone: String? = null
    var Fax: String? = null
    var YouTube: String? = null
    var Facebook: String? = null
    var Twitter: String? = null
    var Instagram: String? = null
    var Latitude: String? = null
    var Longitude: String? = null
    var companyPhotos: ArrayList<CompanyPhotos?>? = null

    inner class CompanyPhotos {
        var Id: String? = null
        var Photo: String? = null
    }
}