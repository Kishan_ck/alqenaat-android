package com.qenaat.app.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.SubscriptionInvoiceAdapter
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetMonthlyDonationSubscriptionInvoices
import com.qenaat.app.model.GetMonthlyDonations
import com.qenaat.app.networking.QenaatAPICall
import company.tap.gosellapi.SettingsManager
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.PhoneNumber
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.internal.interfaces.PaymentInterface
import company.tap.gosellapi.open.buttons.PayButtonView
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.CardType
import company.tap.gosellapi.open.enums.TransactionMode
import company.tap.gosellapi.open.models.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.math.BigDecimal
import java.util.*

/**
 * Created by DELL on 05-Dec-17.
 */
class SubscriptionInvoiceFragment : Fragment(), SubscriptionInvoiceAdapter.Connenct,
    SessionDelegate, PaymentInterface {
    lateinit var XY: IntArray
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mLayoutManager: LinearLayoutManager
    var page_index = 0
    var endOfREsults = false
    private val loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    private lateinit var sdkSession: SDKSession
    var idCurrent: String = "";
    private var settingsManager: SettingsManager? = null
    private val SDK_REQUEST_CODE = 1001
    private var HI_REQUEST_CODE_DAY=9011



    private fun configureSDKSession() {

        // Instantiate SDK Session
        sdkSession = SDKSession(this) //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession.setTransactionCurrency(TapCurrency("KWD")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession.setAmount(BigDecimal(0)) //** Required **

        // Set Payment Items array list
        sdkSession.setPaymentItems(ArrayList<PaymentItem>()) // ** Optional ** you can pass empty array list


//       sdkSession.setPaymentType("CARD");   //** Merchant can pass paymentType

        // Set Taxes array list
        // Set Taxes array list
        sdkSession.setTaxes(ArrayList<Tax>()) // ** Optional ** you can pass empty array list

        // Set Shipping array list
        sdkSession.setShipping(ArrayList<Shipping>()) // ** Optional ** you can pass empty array list

        // Post URL
        sdkSession.setPostURL("") // ** Optional **

        // Payment Description
        sdkSession.setPaymentDescription("") //** Optional **

        // Payment Extra Info
        sdkSession.setPaymentMetadata(HashMap<String, String>()) // ** Optional ** you can pass empty array hash map

        // Payment Reference
        sdkSession.setPaymentReference(null) // ** Optional ** you can pass null

        // Payment Statement Descriptor
        sdkSession.setPaymentStatementDescriptor("") // ** Optional **

        // Enable or Disable Saving Card
        sdkSession.isUserAllowedToSaveCard(true) //  ** Required ** you can pass boolean

        // Enable or Disable 3DSecure
        sdkSession.isRequires3DSecure(true)

        //Set Receipt Settings [SMS - Email ]
        sdkSession.setReceiptSettings(
            Receipt(
                false,
                false
            )
        ) // ** Optional ** you can pass Receipt object or null

        // Set Authorize Action
        sdkSession.setAuthorizeAction(null) // ** Optional ** you can pass AuthorizeAction object or null
        sdkSession.setDestination(null) // ** Optional ** you can pass Destinations object or null
        sdkSession.setMerchantID(null) // ** Optional ** you can pass merchant id or null
        sdkSession.setCardType(CardType.CREDIT) // ** Optional ** you can pass which cardType[CREDIT/DEBIT] you want.By default it loads all available cards for Merchant.

        // sdkSession.setDefaultCardHolderName("TEST TAP"); // ** Optional ** you can pass default CardHolderName of the user .So you don't need to type it.
        // sdkSession.isUserAllowedToEnableCardHolderName(false); // ** Optional ** you can enable/ disable  default CardHolderName .
    }

    private fun getCustomer(): Customer { // test customer id cus_Kh1b4220191939i1KP2506448
        val customer = if ((settingsManager != null)) settingsManager?.getCustomer() else null
        val phoneNumber =
            if (customer != null) customer.getPhone() else PhoneNumber("965", "69045932")
        return Customer.CustomerBuilder(null).email("abc@abc.com").firstName("firstname")
            .lastName("lastname").metadata("")
            .phone(PhoneNumber(phoneNumber?.getCountryCode(), phoneNumber?.getNumber()))
            .middleName("middlename").build()
    }

    private fun startSDKWithUI() {
        if (sdkSession != null) {
            val trx_mode =
                if (settingsManager != null) settingsManager?.getTransactionsMode("key_sdk_transaction_mode") else TransactionMode.PURCHASE
            // set transaction mode [TransactionMode.PURCHASE - TransactionMode.AUTHORIZE_CAPTURE - TransactionMode.SAVE_CARD - TransactionMode.TOKENIZE_CARD ]
            sdkSession.setTransactionMode(trx_mode) //** Required **
            // if you are not using tap button then start SDK using the following call
            //sdkSession.start(this);
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetMonthlyDonations")) {
                    val gson = Gson()
                    getMonthlyDonations = gson.fromJson(
                        arguments!!.getString("GetMonthlyDonations"),
                        GetMonthlyDonations::class.java
                    )
                }
            }
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
    }


    private fun initPayButton(payButtonView: PayButtonView) {
        Log.e("paymentclickkk", "4>>>>>")

        if (ThemeObject.getInstance().payButtonFont != null) payButtonView.setupFontTypeFace(
            ThemeObject.getInstance().payButtonFont
        )
        if (ThemeObject.getInstance().payButtonDisabledTitleColor != 0 && ThemeObject.getInstance().payButtonEnabledTitleColor != 0) payButtonView.setupTextColor(
            ThemeObject.getInstance().payButtonEnabledTitleColor,
            ThemeObject.getInstance().payButtonDisabledTitleColor
        )
        if (ThemeObject.getInstance().payButtonTextSize != 0) payButtonView.getPayButton()
            .setTextSize(ThemeObject.getInstance().payButtonTextSize.toFloat())
        //
        if (ThemeObject.getInstance().isPayButtSecurityIconVisible) payButtonView.getSecurityIconView()
            .setVisibility(if (ThemeObject.getInstance().isPayButtSecurityIconVisible) View.VISIBLE else View.INVISIBLE)
        if (ThemeObject.getInstance().payButtonResourceId != 0) payButtonView.setBackgroundSelector(
            ThemeObject.getInstance().payButtonResourceId
        )
        if (sdkSession != null) {
            val trx_mode: TransactionMode = sdkSession.getTransactionMode()
            if (trx_mode != null) {
                payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.pay))
            } else {
                startSDKWithUI()
            }
            sdkSession.setButtonView(payButtonView, act, SDK_REQUEST_CODE)
        }
    }


    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        initViews(mainLayout)
        configureSDKSession()
        startSDKWithUI()

        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())



    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.MonthlyDonateLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)


        ContentActivity.Companion.enableLogin(languageSeassionManager)

//        ContentActivity.img_topAddAd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                ContentActivity.clearVariables();
//
//                Bundle bundle = new Bundle();
//
//                bundle.putString("isEdit", "0");
//
//                bundle.putString("contentType", "1");
//
//                ContentActivity.openAddNewsFragment(bundle);
//
//            }
//        });

//        if(subscriptionInvoiceArrayList.size >0){
//            mAdapter = SubscriptionInvoiceAdapter(act, subscriptionInvoiceArrayList);
//
//            my_recycler_view.setAdapter(mAdapter);
//        }
//        else{
        GetMonthlyDonationsInvoices()
//        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    protected val TAG = SubscriptionInvoiceFragment::class.java.simpleName
    lateinit var my_recycler_view: RecyclerView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit private var mAdapter: RecyclerView.Adapter<*>
    lateinit var mainLayout: RelativeLayout
    var subscriptionInvoiceArrayList: ArrayList<GetMonthlyDonationSubscriptionInvoices> =
        ArrayList()
    lateinit var getMonthlyDonations: GetMonthlyDonations

    companion object {
        lateinit var fragment: SubscriptionInvoiceFragment
        lateinit var act: FragmentActivity

        fun newInstance(_act: FragmentActivity): SubscriptionInvoiceFragment {
            fragment = SubscriptionInvoiceFragment()
            act = _act
            return fragment
        }
    }


    fun filllist(subscriptionInvoices: ArrayList<GetMonthlyDonationSubscriptionInvoices>?) {
        if (mloading != null && subscriptionInvoices != null) {
            Log.d("getContentses size", "" + subscriptionInvoices.size)
            subscriptionInvoiceArrayList.clear()
            subscriptionInvoiceArrayList.addAll(subscriptionInvoices)
            mAdapter = SubscriptionInvoiceAdapter(act, subscriptionInvoiceArrayList, this)
            my_recycler_view.setAdapter(mAdapter)
        }
        mloading.setVisibility(View.GONE)
        GlobalFunctions.EnableLayout(mainLayout)
    }

    fun GetMonthlyDonationsInvoices() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)

        Log.e("ohkohk", "" + getMonthlyDonations.Id);

        QenaatAPICall.getCallingAPIInterface()?.GetMonthlyDonationsInvoices(
            "-1",
            getMonthlyDonations.Id
        )?.enqueue(
            object : Callback<ArrayList<GetMonthlyDonationSubscriptionInvoices>?> {

                override fun onFailure(
                    call: Call<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>,
                    t: Throwable
                ) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>,
                    response: Response<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>
                ) {
                    Log.e("ohkohk", "" + call.request().url)
                    if (response.body() != null) {
                        val subscriptionInvoices = response.body()
                        filllist(subscriptionInvoices)
                    }
                }
            })
    }

    fun CancelMonthlyDonation(position: Int) {
        val subscriptionInvoice: GetMonthlyDonationSubscriptionInvoices? =
            subscriptionInvoiceArrayList.get(position)
        idCurrent = subscriptionInvoiceArrayList.get(position).Id!!
        Log.e("pikapika11", "" + idCurrent)
        Log.e("pikapika22", "" + subscriptionInvoice?.Id)
        QenaatAPICall.getCallingAPIInterface()?.CancelMonthlyDonationInvoice(
            idCurrent,
            subscriptionInvoice?.Id
        )?.enqueue(
            object : Callback<ResponseBody?> {
                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    Log.e("Fkfkfkfkfk", "" + t.message)
                }

                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: Response<ResponseBody?>
                ) {
                    Log.e("Fkfkfkfkfk1", "" + response.code())
                    Log.e("Fkfkfkfkfk2", "" + response.body())
                    Log.e("Fkfkfkfkfk4", "" + response.errorBody()?.string())
                    Log.e("Fkfkfkfkfk3", "" + call.request().url)
                    val body = response?.body()?.string()
                    Log.e("Fkfkfkfkfk5", "" + body)
                    mloading.setVisibility(View.GONE)
                    if (body == "\"1\"") {
                        GetMonthlyDonationsInvoices()
                    } else {
                        GetMonthlyDonationsInvoices()
                    }

//                        try {
//                            val reader = BufferedReader(InputStreamReader(
//                                    ByteArrayInputStream(body?.bytes())))
//                            val out = StringBuilder()
//                            val newLine = System.getProperty("line.separator")
//                            var line: String?
//                            while (reader.readLine().also { line = it } != null) {
//                                out.append(line)
//                                out.append(newLine)
//                            }
//                            val outResponse = out.toString().trim { it <= ' ' }.replace("\"", "")
//                            val responseInteger = outResponse.toInt()
//                            if (responseInteger > 0) {
//                                Snackbar.make(mainLayout, act.getString(R.string.DonationCancelled), Snackbar.LENGTH_LONG).show()
//                            } else {
//                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
//                            }
//
//                        } catch (ex: Exception) {
//                            ex.printStackTrace()
//                            mloading.setVisibility(View.GONE)
//                        }
                }
            })
    }

    fun AddMonthlyDonationInvoice(subscriptionInvoice: GetMonthlyDonationSubscriptionInvoices?) {

        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.AddMonthlyDonationInvoice(
            subscriptionInvoice?.Id, "test", "test",
            mSessionManager.getUserCode()
        )?.enqueue(
            object : Callback<ResponseBody?> {

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ResponseBody?>,
                    response: Response<ResponseBody?>
                ) {
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    val body = response?.body()
                    var outResponse = ""
                    try {
                        val reader = BufferedReader(
                            InputStreamReader(
                                ByteArrayInputStream(body?.bytes())
                            )
                        )
                        val out = StringBuilder()
                        val newLine = System.getProperty("line.separator")
                        var line: String?
                        while (reader.readLine().also { line = it } != null) {
                            out.append(line)
                            out.append(newLine)
                        }
                        outResponse = out.toString()
                        Log.d("outResponse", "" + outResponse)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                    if (outResponse != null) {
                        outResponse = outResponse.replace("\"", "")
                        outResponse = outResponse.replace("\n", "")
                        Log.e("outResponse not null ", outResponse)
                        if (outResponse.contains("https") || outResponse.contains("http")) {
                            val b = Bundle()
                            b.putString("id", outResponse)
                            val frag: Fragment = PayPaymentFragment.Companion.newInstance(act)!!
                            frag.arguments = b
                            act.getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                .addToBackStack(frag.javaClass.name)
                                .replace(R.id.content_frame, frag).commit()
                        } else {
                            if (outResponse == "-1") {
                                Snackbar.make(
                                    mainLayout,
                                    act.getString(R.string.OperationFailed),
                                    Snackbar.LENGTH_LONG
                                ).show()
                            } else if (outResponse == "-2") {
                                Snackbar.make(
                                    mainLayout,
                                    act.getString(R.string.DataMissing),
                                    Snackbar.LENGTH_LONG
                                ).show()
                            }
                        }
                    }
                }
            })
    }


    lateinit var dialog: AlertDialog

    fun openAlertDialog(item: GetMonthlyDonationSubscriptionInvoices) {
//        Toast.makeText(activity as Context, "Clickkkeddd", Toast.LENGTH_SHORT).show()

//        AlertDialog.Builder(act)
//            .setTitle(act.getString(R.string.Confirm))
//            .setMessage(act.getString(R.string.CancelSubscriptionLabel))
//            .setPositiveButton(android.R.string.yes) { dialog, which -> /*connenct.cancel(position)*/
//            }
//            .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
//            .setIcon(R.drawable.icon_512)
//            .show()

        sdkSession.setAmount(BigDecimal(item.Amount))
        idCurrent = item.Id!!;
        val builder = AlertDialog.Builder(activity as Context)
        val customLayout: View = getLayoutInflater().inflate(R.layout.pay_dialog,null)
        builder.setView(customLayout)


        var message = customLayout.findViewById<TextView>(R.id.text_message)
        var payButtonView = customLayout.findViewById<PayButtonView>(R.id.pay_now_dialog)
        message.setText(resources.getString(R.string.pay_message) + " ${item.Amount}" + resources.getString(R.string.KD))
        initPayButton(payButtonView);

        dialog = builder.create()
        dialog.show()


        Log.e("paymentclickkk", "3>>>>>")
    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (requestCode == HI_REQUEST_CODE_DAY && resultCode == Activity.RESULT_OK) {
//            if (data != null) {
//                if(data.hasExtra("ispayment")){
//
//                    Log.e("ispayment","ispaymentttttt")
//
//                    initPayButton(pay_now_dialog)
//                }
//            }
//        }
//    }


    override fun pay(position: Int) {
        openAlertDialog(subscriptionInvoiceArrayList.get(position))

            Log.e("positionnnnn",""+position)
//        val intent = Intent(activity, alertDialog::class.java)
//        startActivityForResult(intent, HI_REQUEST_CODE_DAY)
        Log.e("paymentclickkk", "2>>>>>")
    }

    override fun cancel(position: Int) {
        CancelMonthlyDonation(position)
    }

    override fun sessionCancelled() {
        Log.e("OKOKOK", "sessionCancelled");
    }

    override fun savedCardsList(cardsList: CardsList) {
        Log.e("OKOKOK", "savedCardsList");

    }

    override fun sessionIsStarting() {
        dialog.cancel()
    }

    override fun invalidCardDetails() {
        Snackbar.make(mainLayout, act.getString(R.string.carddetail_invalid), Snackbar.LENGTH_LONG)
            .show()
    }

    override fun cardSavingFailed(charge: Charge) {
        Log.e("OKOKOK", "cardSavingFailed");
    }

    override fun backendUnknownError(message: String?) {
        Log.e("OKOKOK", "backendUnknownError  " + message);
    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {
        Log.e("OKOKOK", "userEnabledSaveCardOption  ");

    }

    override fun cardSaved(charge: Charge) {
        Log.e("OKOKOK", "cardSaved");

    }

    override fun paymentSucceed(charge: Charge) {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.monthlySubscriptionInvoiceDonate(
            "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
            idCurrent, "3", getMonthlyDonations.Id, charge.amount.toString(),
            charge.id.toString(), mSessionManager.getUserCode()
        )?.enqueue(object : Callback<ResponseBody?> {
            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                mloading.setVisibility(View.GONE);
                Log.e("OKOKOK", "" + t.message)
                Snackbar.make(mainLayout, act.getString(R.string.someErr), Snackbar.LENGTH_LONG)
                    .show()
            }

            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                mloading.setVisibility(View.GONE)
                var ok = response.body()?.string();
                if (response.code() == 200) {
                    if (ok.equals("\"1\"")) {
                        Snackbar.make(
                            mainLayout,
                            act.getString(R.string.payment_succeeded),
                            Snackbar.LENGTH_LONG
                        ).show()
                        GetMonthlyDonationsInvoices()
                    } else {
                        Snackbar.make(
                            mainLayout,
                            act.getString(R.string.money_deduct_someErr),
                            Snackbar.LENGTH_LONG
                        ).show()
                        GetMonthlyDonationsInvoices()
                    }

                } else {
                    Snackbar.make(
                        mainLayout,
                        act.getString(R.string.money_deduct_someErr),
                        Snackbar.LENGTH_LONG
                    ).show()
                }

            }

        })


    }

    override fun paymentFailed(charge: Charge?) {
        Snackbar.make(mainLayout, act.getString(R.string.someErr), Snackbar.LENGTH_LONG).show()

    }

    override fun authorizationFailed(authorize: Authorize?) {
        Log.e("OKOKOK", "authorizationFailed");
    }

    override fun cardTokenizedSuccessfully(token: Token) {
        Log.e("OKOKOK", "cardTokenizedSuccessfully");
    }

    override fun authorizationSucceed(authorize: Authorize) {
        Log.e("OKOKOK", "authorizationSucceed");
    }

    override fun invalidTransactionMode() {
        Log.e("OKOKOK", "invalidTransactionMode");
    }

    override fun sdkError(goSellError: GoSellError?) {

        Log.e("OKOKOK", "sdkError ::" + goSellError?.errorMessage);
    }

    override fun sessionFailedToStart() {
        Log.e("OKOKOK", "sdkError");
    }

    override fun sessionHasStarted() {
        Log.e("OKOKOK", "sessionHasStarted");

    }

    override fun invalidCustomerID() {
        Log.e("OKOKOK", "invalidCustomerID");
    }

    override fun onPayment() {
        Log.e("OKOKOK", "onPayment");
        sdkSession.isValid = true

    }
}