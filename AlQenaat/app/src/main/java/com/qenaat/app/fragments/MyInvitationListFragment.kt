package com.qenaat.app.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.MyInvitationListAdapter
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetInvitationLists
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by DELL on 11-Feb-18.
 */
class MyInvitationListFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    var invitationListsArrayList: ArrayList<GetInvitationLists> = ArrayList<GetInvitationLists>()
    lateinit var linear_my_invitation_list: LinearLayout
    lateinit var linear_others: LinearLayout
    lateinit var tv_my_invitation_list: TextView
    lateinit var tv_others: TextView
    lateinit var img_bg: ImageView
    lateinit var img_line1: ImageView
    lateinit var img_line2: ImageView
    lateinit var relative_top: RelativeLayout
    lateinit var relative_my_invitation_list: RelativeLayout
    lateinit var relative_others: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.my_invitation_list, null) as RelativeLayout
        initViews(mainLayout)
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        relative_my_invitation_list = mainLayout.findViewById(R.id.relative_my_invitation_list) as RelativeLayout
        relative_others = mainLayout.findViewById(R.id.relative_others) as RelativeLayout
        relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
        linear_others = mainLayout.findViewById(R.id.linear_others) as LinearLayout
        linear_my_invitation_list = mainLayout.findViewById(R.id.linear_my_invitation_list) as LinearLayout
        tv_others = mainLayout.findViewById(R.id.tv_others) as TextView
        tv_my_invitation_list = mainLayout.findViewById(R.id.tv_my_invitation_list) as TextView
        img_line1 = mainLayout.findViewById(R.id.img_line1) as ImageView
        img_line2 = mainLayout.findViewById(R.id.img_line2) as ImageView
        img_bg = mainLayout.findViewById(R.id.img_bg) as ImageView
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        tv_noDataFound = mainLayout.findViewById<View?>(R.id.tv_noDataFound) as TextView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
        relative_others.setOnClickListener(this)
        relative_my_invitation_list.setOnClickListener(this)
        ContentActivity.Companion.setTextFonts(mainLayout)
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.MyListLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                bundle.putString("isEdit", "false")
                ContentActivity.Companion.openCreateMyListFragment(bundle)
            } else {
                mainLayout.showSnakeBar(MonasbatFragment.act.getString(R.string.LoggedIn))
                ContentActivity.Companion.openLoginFragment()
            }
        })
        if (invitationListsArrayList.size > 0) {
            mAdapter = MyInvitationListAdapter(act, invitationListsArrayList, tabNumber)
            my_recycler_view.setAdapter(mAdapter)

        } else {
            GetInvitationLists()
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
        my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                //if (!endOfREsults) {
                if (!endOfREsults) {
                    visibleItemCount = mLayoutManager.getChildCount()
                    totalItemCount = mLayoutManager.getItemCount()
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                    if (loading_flag) {
                        if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                            if (tabNumber == 1) {
                                if (invitationListsArrayList1.size != 0) {
                                    progress_loading_more.setVisibility(View.VISIBLE)
                                    //GlobalFunctions.DisableLayout(mainLayout);
                                }
                            }
                            if (tabNumber == 2) {
                                if (invitationListsArrayList2.size != 0) {
                                    progress_loading_more.setVisibility(View.VISIBLE)
                                    //GlobalFunctions.DisableLayout(mainLayout);
                                }
                            }
                            loading_flag = false
                            page_index = page_index + 1
                            GetMoreInvitationLists()
                        }
                    }
                }
            }
        })
        setTabs(tabNumber)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.relative_others -> setTabs(2)
            R.id.relative_my_invitation_list -> setTabs(1)
        }
    }

    private fun setTabs(tab: Int) {
        when (tab) {
            1 -> {
                tabNumber = 1
                if (invitationListsArrayList1?.size!! > 0) {
                    tv_noDataFound.visibility = View.GONE
                    mAdapter = MyInvitationListAdapter(act, invitationListsArrayList1, tabNumber)
                    my_recycler_view.setAdapter(mAdapter)
                } else {
                    GetInvitationLists()
                }
                my_recycler_view.setVisibility(View.VISIBLE)
                relative_my_invitation_list.setBackgroundColor(Color.parseColor("#87776f"))
                tv_my_invitation_list.setTextColor(Color.parseColor("#fff4e4"))
                relative_others.setBackgroundColor(Color.TRANSPARENT)
                tv_others.setTextColor(Color.parseColor("#87776f"))
            }
            2 -> {
                tabNumber = 2
                if (invitationListsArrayList2.size > 0) {
                    tv_noDataFound.visibility = View.GONE
                    mAdapter = MyInvitationListAdapter(act, invitationListsArrayList2, tabNumber)
                    my_recycler_view.setAdapter(mAdapter)
                } else {
                    GetInvitationLists()
                }
                my_recycler_view.setVisibility(View.VISIBLE)
                relative_my_invitation_list.setBackgroundColor(Color.TRANSPARENT)
                tv_my_invitation_list.setTextColor(Color.parseColor("#87776f"))
                relative_others.setBackgroundColor(Color.parseColor("#87776f"))
                tv_others.setTextColor(Color.parseColor("#fff4e4"))
            }
        }
    }

    companion object {
        protected val TAG = MyInvitationListFragment::class.java.simpleName
        lateinit var my_recycler_view: RecyclerView
        lateinit var tv_noDataFound: TextView
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var mloading: ProgressBar
        lateinit var progress_loading_more: ProgressBar
        private lateinit var mAdapter: RecyclerView.Adapter<*>
        private lateinit var mLayoutManager: LinearLayoutManager
        lateinit var mainLayout: RelativeLayout
        lateinit var act: FragmentActivity
        lateinit var fragment: MyInvitationListFragment
        var page_index = 0
        var endOfREsults = false
        private var loading_flag = true
        var pastVisiblesItems = 0
        var visibleItemCount = 0
        var totalItemCount = 0
        var invitationListsArrayList1: ArrayList<GetInvitationLists> = ArrayList<GetInvitationLists>()
        var invitationListsArrayList2: ArrayList<GetInvitationLists> = ArrayList<GetInvitationLists>()
        var tabNumber = 1
        fun newInstance(act: FragmentActivity): MyInvitationListFragment {
            fragment = MyInvitationListFragment()
            Companion.act = act
            return fragment
        }

        fun GetInvitationLists() {
            page_index = 0
            endOfREsults = false
            loading_flag = true
            pastVisiblesItems = 0
            visibleItemCount = 0
            totalItemCount = 0

            mloading.setVisibility(View.VISIBLE)
            GlobalFunctions.DisableLayout(mainLayout)
            QenaatAPICall.getCallingAPIInterface()?.GetInvitationLists(
                    "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                    mSessionManager.getUserCode(),
                    if (tabNumber == 1) "true" else "false", "" + page_index)?.enqueue(
                    object : Callback<ArrayList<GetInvitationLists>?> {

                        override fun onFailure(call: Call<ArrayList<GetInvitationLists>?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ArrayList<GetInvitationLists>?>, response: Response<ArrayList<GetInvitationLists>?>) {
                            if (response.body() != null) {
                                val getServices = response.body()
                                if (mloading != null && getServices != null) {
                                    Log.d("getContentses size", "" + getServices.size)
                                    if (getServices?.size == 0)
                                        tv_noDataFound?.visibility = View.VISIBLE
                                    else
                                        tv_noDataFound?.visibility = View.GONE
                                    if (tabNumber == 1) {
                                        invitationListsArrayList1.clear()
                                        invitationListsArrayList1.addAll(getServices)
                                        Log.d("getServices size", "" + invitationListsArrayList1.size)
                                        mloading.setVisibility(View.GONE)
                                        GlobalFunctions.EnableLayout(mainLayout)

                                        // setupCategoryTab();
                                        mAdapter = MyInvitationListAdapter(act, invitationListsArrayList1, tabNumber)
                                    }
                                    if (tabNumber == 2) {
                                        invitationListsArrayList2.clear()
                                        invitationListsArrayList2.addAll(getServices)
                                        Log.d("getServices size", "" + invitationListsArrayList2.size)

                                        // setupCategoryTab();
                                        mAdapter = MyInvitationListAdapter(act, invitationListsArrayList2, tabNumber)
                                    }
                                    my_recycler_view.setAdapter(mAdapter)
                                    /*my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            //if (!endOfREsults) {
                                            if (!endOfREsults) {
                                                visibleItemCount = mLayoutManager.getChildCount()
                                                totalItemCount = mLayoutManager.getItemCount()
                                                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                                                if (loading_flag) {
                                                    if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                        if (tabNumber == 1) {
                                                            if (invitationListsArrayList1.size != 0) {
                                                                progress_loading_more.setVisibility(View.VISIBLE)
                                                                //GlobalFunctions.DisableLayout(mainLayout);
                                                            }
                                                        }
                                                        if (tabNumber == 2) {
                                                            if (invitationListsArrayList2.size != 0) {
                                                                progress_loading_more.setVisibility(View.VISIBLE)
                                                                //GlobalFunctions.DisableLayout(mainLayout);
                                                            }
                                                        }
                                                        loading_flag = false
                                                        page_index = page_index + 1
                                                        GetMoreInvitationLists()
                                                    }
                                                }
                                            }
                                        }
                                    })*/
                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        }

        private fun GetMoreInvitationLists() {
            QenaatAPICall.getCallingAPIInterface()?.GetInvitationLists(
                    "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                    mSessionManager.getUserCode(),
                    if (tabNumber == 1) "true" else "false", "" + page_index)?.enqueue(
                    object : Callback<ArrayList<GetInvitationLists>?> {

                        override fun onFailure(call: Call<ArrayList<GetInvitationLists>?>, t: Throwable) {
                            t.printStackTrace()
                            progress_loading_more.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ArrayList<GetInvitationLists>?>, response: Response<ArrayList<GetInvitationLists>?>) {
                            if (response.body() != null) {
                                val getServices = response.body()
                                if (progress_loading_more != null) {
                                    progress_loading_more.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                    if (getServices != null) {
                                        if (getServices.isEmpty()) {
                                            endOfREsults = true
                                            page_index = page_index - 1
                                        }
                                        loading_flag = true
                                        if (tabNumber == 1) {
                                            invitationListsArrayList1.addAll(getServices)
                                        }
                                        if (tabNumber == 1) {
                                            invitationListsArrayList2.addAll(getServices)
                                        }
                                        mAdapter.notifyDataSetChanged()
                                    }
                                }
                            }
                        }
                    })
        }

        fun deleteInvitation(id: String?) {
            mloading.setVisibility(View.VISIBLE)
            GlobalFunctions.DisableLayout(mainLayout)
            QenaatAPICall.getCallingAPIInterface()?.DeleteInvitationList(
                    "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                    id)?.enqueue(
                    object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            val body = response?.body()
                            var outResponse = ""
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                outResponse = out.toString()
                                Log.d("outResponse", "" + outResponse)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (outResponse.toInt() > 0) {
                                    GetInvitationLists()
                                } else if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-3") {
                                    Snackbar.make(mainLayout, act.getString(R.string.can_not_delete_list), Snackbar.LENGTH_LONG).show()
                                }
                            }
                            mloading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        }
    }
}