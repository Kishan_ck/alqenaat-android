package com.qenaat.app.fragments

import android.graphics.Bitmap
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetFamilyTree
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by DELL on 07-Dec-17.
 */
class VIPPersonDetailsFragment : Fragment() {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var relative_top: RelativeLayout
    lateinit var img_person: CircleImageView
    lateinit var tv_name: TextView
    lateinit var tv_position: TextView
    lateinit var tv_details: TextView
    lateinit var familyTree: GetFamilyTree
    lateinit var webView: WebView
    var loadingFinished = true
    var redirect = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetFamilyTree")) {
                    val gson = Gson()
                    familyTree = gson.fromJson(arguments!!.getString("GetFamilyTree"),
                            GetFamilyTree::class.java)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.vip_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            webView = mainLayout.findViewById(R.id.webView) as WebView
            tv_position = mainLayout.findViewById(R.id.tv_position) as TextView
            tv_details = mainLayout.findViewById(R.id.tv_details) as TextView
            tv_name = mainLayout.findViewById(R.id.tv_name) as TextView
            img_person = mainLayout.findViewById(R.id.img_person) as CircleImageView
            relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_name.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
    }

    override fun onStart() {
        super.onStart()
        if (familyTree != null) {
            img_person.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.add_photo) as BitmapDrawable).getBitmap().getWidth()
            img_person.getLayoutParams().height = (act.getResources().getDrawable(
                    R.drawable.add_photo) as BitmapDrawable).getBitmap().getHeight()
            if (familyTree.Photo!!.length > 0) Picasso.with(act)
                    .load(familyTree.Photo)
                    .error(R.drawable.add_photo)
                    .placeholder(R.drawable.add_photo)
                    .config(Bitmap.Config.RGB_565).fit()
                    .into(img_person)
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                tv_name.setText(familyTree.NameEn)
                tv_position.setText(familyTree.PositionNameEN)
            } else {
                tv_name.setText(familyTree.NameAr)
                tv_position.setText(familyTree.PositionNameAR)
            }
            tv_details.setText(familyTree.NameAr)
            Linkify.addLinks(tv_position, Linkify.PHONE_NUMBERS)
            webView.setWebViewClient(WebViewClient())
            webView.getSettings().setJavaScriptEnabled(true)
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true)
            webView.getSettings().setPluginState(WebSettings.PluginState.ON)
            webView.setWebChromeClient(WebChromeClient())
            var mainHTMLText = ""
            mainHTMLText = if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.DetailsLabel))
                ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
                familyTree.VIPDetailsEN!!
            } else {
                ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.DetailsLabel))
                ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
                familyTree.VIPDetailsAR!!
            }
            mainHTMLText = mainHTMLText.replace("kashida".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("font-size:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("text-justify:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("font-size:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("line-height:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("font-family".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("f ont-family:".toRegex(), "font-family:")
            mainHTMLText = mainHTMLText.replace("text-:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("150%".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("color".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("<strong>".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("padding".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("src=\\\"/uploads".toRegex(), "src=\\\"http://www.souq.com/uploads")
            mainHTMLText = mainHTMLText.replace("app.souq.com".toRegex(), "www.souq.com")
            mainHTMLText = mainHTMLText.replace("http://www.souq.comhttp://www.souq.com/".toRegex(), "http://www.souq.com/")
            mainHTMLText = mainHTMLText.replace("<font".toRegex(), "<f ff")
            mainHTMLText = mainHTMLText.replace("display:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("position:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("width:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("max-width:".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("<h1".toRegex(), "<h 1")
            mainHTMLText = mainHTMLText.replace("<h2".toRegex(), "<h 2")
            mainHTMLText = mainHTMLText.replace("<h3".toRegex(), "<h 3")
            mainHTMLText = mainHTMLText.replace("<h4".toRegex(), "<h 4")
            mainHTMLText = mainHTMLText.replace("<h5".toRegex(), "<h 5")
            mainHTMLText = mainHTMLText.replace("<h6".toRegex(), "<h 6")
            mainHTMLText = mainHTMLText.replace("id=\"".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("pastingspan1".toRegex(), "")
            mainHTMLText = mainHTMLText.replace("c olor".toRegex(), "color")
            mainHTMLText = mainHTMLText.replace("l ine-h eight:".toRegex(), "line-height:")
            mainHTMLText = mainHTMLText.replace("ff ont-size".toRegex(), "font-size")
            mainHTMLText = mainHTMLText.replace("bold".toRegex(), "")
            var finalHTML = ""
            finalHTML = if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/verdana.ttf');}" +
                        " body {background-color: " +
                        "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                        "text-align: justify;line-height: 150%;}</style></head><body dir='LTR'><br />"
                        + "</b></span style='color: #414142;'>" + mainHTMLText + "<br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
            } else {
                ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                        " body {background-color: " +
                        "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                        "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br />"
                        + "</b></span style='color: #414142;'>" + mainHTMLText + "<br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
            }
            Log.e("finalHTML", "" + finalHTML)
            webView.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
            webView.setBackgroundColor(0)
        }
    }

    companion object {
        protected val TAG = VIPPersonDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: VIPPersonDetailsFragment
        fun newInstance(act: FragmentActivity): VIPPersonDetailsFragment? {
            fragment = VIPPersonDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}