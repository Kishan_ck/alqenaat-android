package com.qenaat.app.model

/**
 * Created by DELL on 07-Feb-18.
 */
class GetRequestForHelpDocuments {
    var Id: String? = null
    var RequestForHelpId: String? = null
    var AttachmentTypeId: String? = null
    var DocumentName: String? = null
    var AttachmentTypeAR: String? = null
    var AttachmentTypeEN: String? = null
}