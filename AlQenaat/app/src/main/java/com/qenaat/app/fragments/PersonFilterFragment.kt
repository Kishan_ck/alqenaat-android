package com.qenaat.app.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.*
import com.qenaat.app.classes.FixControl
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.*
import com.qenaat.app.networking.QenaatAPICall
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 12-Feb-18.
 */
class PersonFilterFragment : Fragment(), View.OnClickListener {
    lateinit var act: FragmentActivity
    lateinit var mloading: ProgressBar
    lateinit var XY: IntArray
    lateinit var mSessionManager: SessionManager
    lateinit var languageSessionManager: LanguageSessionManager
    lateinit var mainLayout: RelativeLayout
    lateinit var tv_governorate: TextView
    lateinit var tv_area: TextView
    lateinit var tv_branch: TextView
    lateinit var tv_gender: TextView
    lateinit var tv_save: TextView
    lateinit var et_age_to: EditText
    lateinit var et_age_from: EditText
    lateinit var et_keyword: EditText
    lateinit var filters: Filters
    var cityAreaArrayList: ArrayList<GetCities> = ArrayList()
    var areaArrayList: ArrayList<GetAreas> = ArrayList()
    var getBranchesArrayList: ArrayList<GetBranches> = ArrayList()
    var getFamilyNamesArrayList: ArrayList<GetFamilyNames> = ArrayList()
    var getGenderArrayList: ArrayList<GetGender> = ArrayList()
    var getPersonTypesArrayList: ArrayList<GetPersonTypes> = ArrayList()
    var cityId: String = ""
    var areaId: String = ""
    var familyNameId: String = ""
    lateinit var relative_option_dialog: RelativeLayout
    lateinit var listView1: ListView
    lateinit var tv_title1: TextView
    lateinit var tv_msg: TextView
    lateinit var tv_done: TextView
    lateinit var tv_cancel: TextView
    lateinit var tv_person_type: TextView
    lateinit var linear_bottom: LinearLayout
    var gender: String = "0"
    var personType: String = "0"
    var ageFrom: String = "0"
    var ageTo: String = "0"
    var keyword: String = ""
    var invitationId: String = "0"
    var comingFrom: String = ""
    lateinit var tv_family_name: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSessionManager = LanguageSessionManager(act)
            if (arguments != null) {
                invitationId = arguments?.getString("invitationId")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return try {
            mainLayout = inflater.inflate(R.layout.add_filter, null) as RelativeLayout
            initViews(mainLayout)
            mainLayout
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun initViews(mainLayout: RelativeLayout?) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            linear_bottom = mainLayout.findViewById(R.id.linear_bottom) as LinearLayout
            tv_title1 = mainLayout.findViewById(R.id.tv_title) as TextView
            tv_msg = mainLayout.findViewById(R.id.tv_msg) as TextView
            tv_done = mainLayout.findViewById(R.id.tv_done) as TextView
            tv_done.setOnClickListener(this)
            tv_cancel = mainLayout.findViewById(R.id.tv_cancel) as TextView
            tv_cancel.setOnClickListener(this)
            listView1 = mainLayout.findViewById(R.id.lv) as ListView
            relative_option_dialog = mainLayout.findViewById(R.id.relative_option_dialog) as RelativeLayout
            tv_save = mainLayout.findViewById(R.id.tv_save) as TextView
            tv_save.setOnClickListener(this)
            tv_person_type = mainLayout.findViewById(R.id.tv_person_type) as TextView
            tv_person_type.setOnClickListener(this)
            tv_area = mainLayout.findViewById(R.id.tv_area) as TextView
            tv_area.setOnClickListener(this)
            tv_branch = mainLayout.findViewById(R.id.tv_branch) as TextView
            tv_branch.setOnClickListener(this)
            tv_family_name = mainLayout.findViewById(R.id.tv_family_name) as TextView
            tv_family_name.setOnClickListener(this)
            tv_gender = mainLayout.findViewById(R.id.tv_gender) as TextView
            tv_gender.setOnClickListener(this)
            tv_governorate = mainLayout.findViewById(R.id.tv_governorate) as TextView
            tv_governorate.setOnClickListener(this)
            et_age_from = mainLayout.findViewById(R.id.et_age_from) as EditText
            et_age_to = mainLayout.findViewById(R.id.et_age_to) as EditText
            et_keyword = mainLayout.findViewById(R.id.et_keyword) as EditText
            /* et_keyword.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
             tv_gender.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
             tv_branch.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
             tv_family_name.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
             tv_area.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
             tv_person_type.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
             tv_governorate.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.input) as BitmapDrawable).getBitmap().getWidth()*/
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_cancel.setText(act.getString(R.string.DoneLabel))
            relative_option_dialog.setOnClickListener(View.OnClickListener { })
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.enableLogin(languageSessionManager)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        if (areaArrayList.size > 0) {
        } else {
            if (cityId.length > 0) {
                GetAreas()
            }
        }
        if (cityAreaArrayList.size > 0) {
        } else {
            GetCity()
        }
        if (getBranchesArrayList.size > 0) {
        } else {
            GetBranches()
        }
        if (getFamilyNamesArrayList.size > 0) {
        } else {
            GetFamilyNames()
        }
        if (getGenderArrayList.size > 0) {
        } else {
            getGender()
        }
        if (getPersonTypesArrayList.size > 0) {
        } else {
            getPersonTypes()
        }
        setData()
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.tv_save -> gotonext()
            R.id.tv_area -> showAreaDialogue()
            R.id.tv_gender -> showGenderDialogue()
            R.id.tv_governorate -> showCityDialogue()
            R.id.tv_branch -> showBranchesDialogue()
            R.id.tv_family_name -> showFamilyNamesDialogue()
            R.id.tv_person_type -> showPersonTypesDialogue()
        }
    }

    private fun GetCity() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetCity()?.enqueue(object : Callback<ArrayList<GetCities>?> {

            override fun onFailure(call: Call<ArrayList<GetCities>?>, t: Throwable) {
                t.printStackTrace()
                mloading.setVisibility(View.INVISIBLE)
            }

            override fun onResponse(call: Call<ArrayList<GetCities>?>, response: Response<ArrayList<GetCities>?>) {
                if (response.body() != null) {
                    val cityAreas = response.body()
                    if (cityAreas != null) {
                        cityAreaArrayList.clear()
                        cityAreaArrayList.addAll(cityAreas)
                        clearCitySelection()
                    }
                    mloading.setVisibility(View.INVISIBLE)
                }
            }
        })
    }

    private fun showCityDialogue() {
        if (cityAreaArrayList.size > 0) {
            relative_option_dialog.setVisibility(View.VISIBLE)
            tv_title1.setText(getString(R.string.SelectGovernorate))
            tv_cancel.setTypeface(ContentActivity.Companion.tf)
            tv_title1.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            val alertAdapter1 = CityAdapter(act, cityAreaArrayList)
            listView1.setAdapter(alertAdapter1)
            alertAdapter1.notifyDataSetChanged()
            listView1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE)
            FixControl.setListViewHeightBasedOnChildren(listView1)
            listView1.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    val item: GetCities? = cityAreaArrayList.get(i)
                    if (item!!.NameEN.equals(getString(R.string.DoneLabel), ignoreCase = true) ||
                            item.NameAR.equals(getString(R.string.DoneLabel), ignoreCase = true)) {
                        relative_option_dialog.setVisibility(View.GONE)
                    } else {
                        if (item.IsSelected.equals("0", ignoreCase = true)) {
                            item.IsSelected = "1"
                        } else {
                            item.IsSelected = "0"
                        }
                        cityAreaArrayList.set(i, item)
                        alertAdapter1.notifyDataSetChanged()
                        makeCityIds()
                        makeCityNames()
                    }
                }
            })
            tv_cancel.setOnClickListener(View.OnClickListener {
                GetAreas()
                relative_option_dialog.setVisibility(View.GONE)
            })
        }
    }

    private fun clearCitySelection() {
        for (i in cityAreaArrayList.indices) {
            val cities: GetCities? = cityAreaArrayList.get(i)
            cities?.IsSelected = "0"
            cityAreaArrayList.set(i, cities!!)
        }
    }

    private fun makeCityIds() {
        cityId = ""
        for (cities in cityAreaArrayList) {
            if (cities.IsSelected.equals("1", ignoreCase = true)) {
                cityId = if (cityId.length > 0) {
                    cityId + "," + cities.Id
                } else {
                    cities.Id!!
                }
            }
        }
    }

    private fun GetAreas() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetAreas(cityId)?.enqueue(
                object : Callback<ArrayList<GetAreas>?> {
                    override fun onFailure(call: Call<ArrayList<GetAreas>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetAreas>?>, response: Response<ArrayList<GetAreas>?>) {
                        if (response.body() != null) {
                            val cityAreas = response.body()
                            if (cityAreas != null) {
                                areaArrayList.clear()
                                areaArrayList.addAll(cityAreas)
                                clearAreaSelection()
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun showAreaDialogue() {
        if (areaArrayList.size > 0) {
            relative_option_dialog.setVisibility(View.VISIBLE)
            tv_title1.setText(getString(R.string.SelectArea))
            tv_cancel.setTypeface(ContentActivity.Companion.tf)
            tv_title1.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            val alertAdapter1 = AreaAdapter(act, areaArrayList)
            listView1.setAdapter(alertAdapter1)
            alertAdapter1.notifyDataSetChanged()
            listView1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE)
            FixControl.setListViewHeightBasedOnChildren(listView1)
            listView1.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    val item: GetAreas? = areaArrayList.get(i)
                    if (item!!.TitleEn.equals(getString(R.string.DoneLabel), ignoreCase = true) ||
                            item.TitleAr.equals(getString(R.string.DoneLabel), ignoreCase = true)) {
                        relative_option_dialog.setVisibility(View.GONE)
                    } else {
                        if (item.IsSelected.equals("0", ignoreCase = true)) {
                            item.IsSelected = "1"
                        } else {
                            item.IsSelected = "0"
                        }
                        areaArrayList.set(i, item)
                        alertAdapter1.notifyDataSetChanged()
                        makeAreaNames()
                    }
                }
            })
            tv_cancel.setOnClickListener(View.OnClickListener { relative_option_dialog.setVisibility(View.GONE) })
        }
    }

    private fun clearAreaSelection() {
        for (i in areaArrayList.indices) {
            val areas: GetAreas? = areaArrayList.get(i)
            areas?.IsSelected = "0"
            areaArrayList.set(i, areas!!)
        }
    }

    private fun GetBranches() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetBranches()?.enqueue(
                object : Callback<ArrayList<GetBranches>?> {

                    override fun onFailure(call: Call<ArrayList<GetBranches>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetBranches>?>, response: Response<ArrayList<GetBranches>?>) {
                        if (response.body() != null) {
                            val getBranches = response.body()
                            if (getBranches != null) {
                                getBranchesArrayList.clear()
                                getBranchesArrayList.addAll(getBranches)
                                clearBranchesSelection()
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun showBranchesDialogue() {
        if (getBranchesArrayList.size > 0) {
            relative_option_dialog.setVisibility(View.VISIBLE)
            tv_title1.setText(getString(R.string.SelectLabel))
            tv_cancel.setTypeface(ContentActivity.Companion.tf)
            tv_title1.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            val alertAdapter1 = BranchAdapter(act, getBranchesArrayList)
            listView1.setAdapter(alertAdapter1)
            alertAdapter1.notifyDataSetChanged()
            listView1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE)
            FixControl.setListViewHeightBasedOnChildren(listView1)
            listView1.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    val item: GetBranches? = getBranchesArrayList.get(i)
                    if (item!!.NameEn.equals(getString(R.string.DoneLabel), ignoreCase = true) ||
                            item.NameAr.equals(getString(R.string.DoneLabel), ignoreCase = true)) {
                        relative_option_dialog.setVisibility(View.GONE)
                    } else {
                        if (item.IsSelected.equals("0", ignoreCase = true)) {
                            item.IsSelected = "1"
                        } else {
                            item.IsSelected = "0"
                        }
                        getBranchesArrayList.set(i, item)
                        alertAdapter1.notifyDataSetChanged()
                        makeBranchNames()
                    }
                }
            })
            tv_cancel.setOnClickListener(View.OnClickListener { relative_option_dialog.setVisibility(View.GONE) })
        }
    }

    private fun clearBranchesSelection() {
        for (i in getBranchesArrayList.indices) {
            val branches: GetBranches? = getBranchesArrayList.get(i)
            branches?.IsSelected = "0"
            getBranchesArrayList.set(i, branches!!)
        }
    }

    private fun GetFamilyNames() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetFamilyNames()?.enqueue(
                object : Callback<ArrayList<GetFamilyNames>?> {

                    override fun onFailure(call: Call<ArrayList<GetFamilyNames>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetFamilyNames>?>, response: Response<ArrayList<GetFamilyNames>?>) {
                        if (response.body() != null) {
                            val getFamilyNames = response.body()
                            if (getFamilyNames != null) {
                                getFamilyNamesArrayList.clear()
                                getFamilyNamesArrayList.addAll(getFamilyNames)
                                clearFamilyNamesSelection()
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun showFamilyNamesDialogue() {
        if (getFamilyNamesArrayList.size > 0) {
            relative_option_dialog.setVisibility(View.VISIBLE)
            tv_title1.setText(getString(R.string.SelectLabel))
            tv_cancel.setTypeface(ContentActivity.Companion.tf)
            tv_title1.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            val alertAdapter1 = FamilyNamesAdapter(act, getFamilyNamesArrayList)
            listView1.setAdapter(alertAdapter1)
            alertAdapter1.notifyDataSetChanged()
            listView1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE)
            FixControl.setListViewHeightBasedOnChildren(listView1)
            listView1.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    val item: GetFamilyNames? = getFamilyNamesArrayList.get(i)
                    if (item!!.NameEN.equals(getString(R.string.DoneLabel), ignoreCase = true) ||
                            item.NameAR.equals(getString(R.string.DoneLabel), ignoreCase = true)) {
                        relative_option_dialog.setVisibility(View.GONE)
                    } else {
                        if (item.IsSelected.equals("0", ignoreCase = true)) {
                            item.IsSelected = "1"
                        } else {
                            item.IsSelected = "0"
                        }
                        getFamilyNamesArrayList.set(i, item)
                        alertAdapter1.notifyDataSetChanged()
                        makeFamilyNames()
                    }
                }
            })
            tv_cancel.setOnClickListener(View.OnClickListener { relative_option_dialog.setVisibility(View.GONE) })
        }
    }

    private fun clearFamilyNamesSelection() {
        for (i in getFamilyNamesArrayList.indices) {
            val familyNames: GetFamilyNames? = getFamilyNamesArrayList.get(i)
            familyNames?.IsSelected = "0"
            getFamilyNamesArrayList.set(i, familyNames!!)
        }
    }

    private fun getGender() {
        var getGender = GetGender()
        getGender.Id = "1"
        getGender.TitleAr = act.getString(R.string.MaleLabel)
        getGender.TitleEn = act.getString(R.string.MaleLabel)
        getGenderArrayList.add(getGender)
        getGender = GetGender()
        getGender.Id = "2"
        getGender.TitleAr = act.getString(R.string.FemaleLabel)
        getGender.TitleEn = act.getString(R.string.FemaleLabel)
        getGenderArrayList.add(getGender)
        clearGenderSelection()
    }

    private fun showGenderDialogue() {
        if (getGenderArrayList.size > 0) {
            relative_option_dialog.setVisibility(View.VISIBLE)
            tv_title1.setText(getString(R.string.SelectLabel))
            tv_cancel.setTypeface(ContentActivity.Companion.tf)
            tv_title1.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            val alertAdapter1 = GenderAdapter(act, getGenderArrayList)
            listView1.setAdapter(alertAdapter1)
            alertAdapter1.notifyDataSetChanged()

            //listView1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
            FixControl.setListViewHeightBasedOnChildren(listView1)
            listView1.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    val item: GetGender? = getGenderArrayList.get(i)
                    if (item!!.TitleEn.equals(getString(R.string.DoneLabel), ignoreCase = true) ||
                            item.TitleAr.equals(getString(R.string.DoneLabel), ignoreCase = true)) {
                        relative_option_dialog.setVisibility(View.GONE)
                    } else {
                        if (item.IsSelected.equals("0", ignoreCase = true)) {
                            item.IsSelected = "1"
                        } else {
                            item.IsSelected = "0"
                        }
                        getGenderArrayList.set(i, item)
                        alertAdapter1.notifyDataSetChanged()
                        makeGenderNames()
                    }
                }
            })
            tv_cancel.setOnClickListener(View.OnClickListener { relative_option_dialog.setVisibility(View.GONE) })
        }
    }

    private fun clearGenderSelection() {
        for (i in getGenderArrayList.indices) {
            val getGender: GetGender? = getGenderArrayList.get(i)
            getGender?.IsSelected = "0"
            getGenderArrayList.set(i, getGender!!)
        }
    }

    private fun getPersonTypes() {
        var personTypes = GetPersonTypes()
        personTypes.Id = "1"
        personTypes.TitleAr = act.getString(R.string.VIPLabel)
        personTypes.TitleEn = act.getString(R.string.VIPLabel)
        getPersonTypesArrayList.add(personTypes)
        personTypes = GetPersonTypes()
        personTypes.Id = "2"
        personTypes.TitleAr = act.getString(R.string.NonVIPLabel)
        personTypes.TitleEn = act.getString(R.string.NonVIPLabel)
        getPersonTypesArrayList.add(personTypes)
        clearPersonTypesSelection()
    }

    private fun showPersonTypesDialogue() {
        if (getPersonTypesArrayList.size > 0) {
            relative_option_dialog.setVisibility(View.VISIBLE)
            tv_title1.setText(getString(R.string.SelectLabel))
            tv_cancel.setTypeface(ContentActivity.Companion.tf)
            tv_title1.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            val alertAdapter1 = PersonTypesAdapter(act, getPersonTypesArrayList)
            listView1.setAdapter(alertAdapter1)
            alertAdapter1.notifyDataSetChanged()
            listView1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE)
            FixControl.setListViewHeightBasedOnChildren(listView1)
            listView1.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                    val item: GetPersonTypes? = getPersonTypesArrayList.get(i)
                    if (item!!.TitleEn.equals(getString(R.string.DoneLabel), ignoreCase = true) ||
                            item.TitleAr.equals(getString(R.string.DoneLabel), ignoreCase = true)) {
                        relative_option_dialog.setVisibility(View.GONE)
                    } else {
                        if (item.IsSelected.equals("0", ignoreCase = true)) {
                            item.IsSelected = "1"
                        } else {
                            item.IsSelected = "0"
                        }
                        getPersonTypesArrayList.set(i, item)
                        alertAdapter1.notifyDataSetChanged()
                        makePersonTypeNames()
                    }
                }
            })
            tv_cancel.setOnClickListener(View.OnClickListener { relative_option_dialog.setVisibility(View.GONE) })
        }
    }

    private fun clearPersonTypesSelection() {
        for (i in getPersonTypesArrayList.indices) {
            val personTypes: GetPersonTypes? = getPersonTypesArrayList.get(i)
            personTypes?.IsSelected = "0"
            getPersonTypesArrayList.set(i, personTypes!!)
        }
    }

    private fun gotonext() {

        //getFragmentManager().popBackStack();
        var cityIdArray: Array<String?>
        var areaIdArray: Array<String?>
        var branchIdArray: Array<String?>
        var familyNameIdArray: Array<String?>
        cityIdArray = arrayOfNulls<String?>(0)
        areaIdArray = arrayOfNulls<String?>(0)
        branchIdArray = arrayOfNulls<String?>(0)
        familyNameIdArray = arrayOfNulls<String?>(0)


        //add city id
        var size = 0
        for (cities in cityAreaArrayList) {
            if (cities.IsSelected.equals("1", ignoreCase = true)) {
                size++
            }
        }
        cityIdArray = arrayOfNulls<String?>(size)
        Log.d("size", "cityIdArray -> $size")
        size = 0
        for (i in cityAreaArrayList.indices) {
            val cities: GetCities? = cityAreaArrayList.get(i)
            if (cities!!.IsSelected.equals("1", ignoreCase = true)) {
                cityIdArray[size] = cities.Id
                size++
            }
        }

        //add area id
        size = 0
        for (areas in areaArrayList) {
            if (areas.IsSelected.equals("1", ignoreCase = true)) {
                size++
            }
        }
        areaIdArray = arrayOfNulls<String?>(size)
        Log.d("size", "areaIdArray -> $size")
        size = 0
        for (i in areaArrayList.indices) {
            val areas: GetAreas? = areaArrayList.get(i)
            if (areas!!.IsSelected.equals("1", ignoreCase = true)) {
                areaIdArray[size] = areas.Id
                size++
            }
        }

        //add branch id
        size = 0
        for (branches in getBranchesArrayList) {
            if (branches.IsSelected.equals("1", ignoreCase = true)) {
                size++
            }
        }
        branchIdArray = arrayOfNulls<String?>(size)
        Log.d("size", "branchIdArray -> $size")
        size = 0
        for (i in getBranchesArrayList.indices) {
            val branches: GetBranches? = getBranchesArrayList.get(i)
            if (branches!!.IsSelected.equals("1", ignoreCase = true)) {
                branchIdArray[size] = branches.Id
                size++
            }
        }

        //add family names id
        size = 0
        for (familyNames in getFamilyNamesArrayList) {
            if (familyNames.IsSelected.equals("1", ignoreCase = true)) {
                size++
            }
        }
        familyNameIdArray = arrayOfNulls<String?>(size)
        Log.d("size", "familyNameIdArray -> $size")
        size = 0
        for (i in getFamilyNamesArrayList.indices) {
            val familyNames: GetFamilyNames? = getFamilyNamesArrayList.get(i)
            if (familyNames!!.IsSelected.equals("1", ignoreCase = true)) {
                familyNameIdArray[size] = familyNames.Id
                size++
            }
        }

        //add gender
        size = 0
        for (getGender in getGenderArrayList) {
            if (getGender.IsSelected.equals("1", ignoreCase = true)) {
                size++
            }
        }
        if (size == 1) {
            for (i in getGenderArrayList.indices) {
                val getGender: GetGender? = getGenderArrayList.get(i)
                if (getGender!!.IsSelected.equals("1", ignoreCase = true)) {
                    gender = getGender.Id!!
                }
            }
        } else {
            gender = "0"
        }

        //add person type
        size = 0
        for (personTypes in getPersonTypesArrayList) {
            if (personTypes.IsSelected.equals("1", ignoreCase = true)) {
                size++
            }
        }
        if (size == 1) {
            for (i in getPersonTypesArrayList.indices) {
                val personTypes: GetPersonTypes? = getPersonTypesArrayList.get(i)
                if (personTypes!!.IsSelected.equals("1", ignoreCase = true)) {
                    personType = personTypes.Id!!
                }
            }
        } else {
            personType = "0"
        }
        val bundle = Bundle()
        bundle.putStringArray("cityIdArray", cityIdArray)
        bundle.putStringArray("areaIdArray", areaIdArray)
        bundle.putStringArray("branchIdArray", branchIdArray)
        bundle.putStringArray("familyNameIdArray", familyNameIdArray)
        bundle.putString("gender", gender)
        bundle.putString("personType", personType)
        bundle.putString("ageFrom", et_age_from.getText().toString())
        ageFrom = et_age_from.getText().toString()
        bundle.putString("ageTo", et_age_to.getText().toString())
        ageTo = et_age_to.getText().toString()
        bundle.putString("keyword", et_keyword.getText().toString())
        keyword = et_keyword.getText().toString()
        bundle.putString("invitationId", invitationId)
        bundle.putString("comingFrom", "add")
        bundle.putInt("tabNumber", 1)
        ContentActivity.Companion.openAddPersonToInvitationListFragment(bundle)

    }

    private fun makeCityNames() {
        var name = ""
        for (cities in cityAreaArrayList) {
            if (cities.IsSelected.equals("1", ignoreCase = true)) {
                name = if (name.length > 0) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        name + "," + cities.NameEN
                    } else {
                        name + "," + cities.NameAR
                    }
                } else {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        cities.NameEN!!
                    } else {
                        cities.NameAR!!
                    }
                }
            }
        }
        tv_governorate.setText(name)
    }

    private fun makeAreaNames() {
        var name = ""
        for (areas in areaArrayList) {
            if (areas.IsSelected.equals("1", ignoreCase = true)) {
                name = if (name.length > 0) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        name + "," + areas.TitleEn
                    } else {
                        name + "," + areas.TitleAr
                    }
                } else {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        areas.TitleEn!!
                    } else {
                        areas.TitleAr!!
                    }
                }
            }
        }
        tv_area.setText(name)
    }

    private fun makeBranchNames() {
        var name = ""
        for (branches in getBranchesArrayList) {
            if (branches.IsSelected.equals("1", ignoreCase = true)) {
                name = if (name.length > 0) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        name + "," + branches.NameEn
                    } else {
                        name + "," + branches.NameAr
                    }
                } else {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        branches.NameEn!!
                    } else {
                        branches.NameAr!!
                    }
                }
            }
        }
        tv_branch.setText(name)
    }

    private fun makeFamilyNames() {
        var name = ""
        for (familyNames in getFamilyNamesArrayList) {
            if (familyNames.IsSelected.equals("1", ignoreCase = true)) {
                name = if (name.length > 0) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        name + "," + familyNames.NameEN
                    } else {
                        name + "," + familyNames.NameAR
                    }
                } else {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        familyNames.NameEN!!
                    } else {
                        familyNames.NameAR!!
                    }
                }
            }
        }
        tv_family_name.setText(name)
    }

    private fun makeGenderNames() {
        var name = ""
        for (getGender in getGenderArrayList) {
            if (getGender.IsSelected.equals("1", ignoreCase = true)) {
                name = if (name.length > 0) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        name + "," + getGender.TitleEn
                    } else {
                        name + "," + getGender.TitleAr
                    }
                } else {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        getGender.TitleEn!!
                    } else {
                        getGender.TitleAr!!
                    }
                }
            }
        }
        tv_gender.setText(name)
    }

    private fun makePersonTypeNames() {
        var name = ""
        for (personTypes in getPersonTypesArrayList) {
            if (personTypes.IsSelected.equals("1", ignoreCase = true)) {
                name = if (name.length > 0) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        name + "," + personTypes.TitleEn
                    } else {
                        name + "," + personTypes.TitleAr
                    }
                } else {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        personTypes.TitleEn!!
                    } else {
                        personTypes.TitleAr!!
                    }
                }
            }
        }
        tv_person_type.setText(name)
    }

    private fun setData() {
        if (!ageFrom.equals("0", ignoreCase = true)) {
            et_age_from.setText(ageFrom)
        }
        if (!ageTo.equals("0", ignoreCase = true)) {
            et_age_to.setText(ageTo)
        }
        if (!keyword.equals("0", ignoreCase = true)) {
            et_keyword.setText(keyword)
        }
        for (i in getGenderArrayList.indices) {
            val getGender: GetGender? = getGenderArrayList.get(i)
            if (gender.equals(getGender!!.Id, ignoreCase = true)) {
                getGender.IsSelected = "1"
                getGenderArrayList.set(i, getGender)
            }
        }
        for (i in getPersonTypesArrayList.indices) {
            val personTypes: GetPersonTypes? = getPersonTypesArrayList.get(i)
            if (personType.equals(personTypes?.Id, ignoreCase = true)) {
                personTypes?.IsSelected = "1"
                getPersonTypesArrayList.set(i, personTypes!!)
            }
        }
        makeCityNames()
        makeAreaNames()
        makeBranchNames()
        makeGenderNames()
        makePersonTypeNames()
    }

    companion object {
        protected val TAG = PersonFilterFragment::class.java.simpleName
        lateinit var fragment: PersonFilterFragment
        fun newInstance(act: FragmentActivity): PersonFilterFragment {
            fragment = PersonFilterFragment()
            fragment.act = act
            return fragment
        }
    }
}