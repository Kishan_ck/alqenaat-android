package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager

/**
 * Created by DELL on 14-Nov-17.
 */
class PayPaymentFragment : Fragment() {
    lateinit var XY: IntArray
    lateinit var mainLayout: LinearLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var Id: String
    lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                val gson = Gson()
                Id = arguments?.getString("id")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message + "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.pay_payment, null) as LinearLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message + "")
        }
        return mainLayout
    }

    fun initViews(mainLayout: LinearLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            webView = mainLayout.findViewById<View?>(R.id.webView) as WebView
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("URL_Load", Id)
        ContentActivity.Companion.enableLogin(languageSeassionManager)

        // GetTermsAndConditions();
        webView.setWebViewClient(WebViewClient())
        webView.getSettings().javaScriptEnabled = true
        webView.getSettings().javaScriptCanOpenWindowsAutomatically = true
        webView.getSettings().pluginState = WebSettings.PluginState.ON
        webView.setWebChromeClient(WebChromeClient())
        webView.loadUrl(Id)
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                Log.d("url_resp", url?.toLowerCase())
                if (url?.toLowerCase()?.contains(act.getString(R.string.SuccessPage))!!) {
                    Log.d("url_resp", "contains thanks")
                    Snackbar.make(mainLayout, act.getString(R.string.ThankYoForDonating), Snackbar.LENGTH_LONG).show()
                    act.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE) //
                    val b = Bundle()

                    //HomeFragment.getProductsArrayList.clear();
                    val bundle = Bundle()
                    bundle.putString("comingFrom", "home")
                    ContentActivity.Companion.openNewsFragment(bundle)
                    return false
                }
                if (url.toLowerCase().contains(act.getString(R.string.ErrorPage))) {
                    Log.d("url_resp", "contains error")
                    Snackbar.make(mainLayout, act.getString(R.string.someErr), Snackbar.LENGTH_LONG).show()
                    fragmentManager?.popBackStackImmediate()
                    return false
                }
                Log.d("url_resp", "contains nothing")
                webView.loadUrl(url)
                return true
            }
        })
    }

    companion object {
        protected val TAG = PayPaymentFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: PayPaymentFragment
        fun newInstance(act: FragmentActivity): PayPaymentFragment? {
            fragment = PayPaymentFragment()
            Companion.act = act
            return fragment
        }
    }
}