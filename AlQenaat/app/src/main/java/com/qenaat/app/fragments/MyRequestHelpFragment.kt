package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.MyRequestedHelpsAdapter
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.RequestForHelp
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 15-Nov-17.
 */
class MyRequestHelpFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var page_index = 0
    var endOfREsults = false
    private val loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var isComeBefore = false
    var getMyRequestedHelpsArrayList: ArrayList<RequestForHelp> = ArrayList<RequestForHelp>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.RequestHelpLabel)
        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            isComeBefore = true
            ContentActivity.Companion.clearVariables()
            val bundle = Bundle()
            bundle.putString("isEdit", "0")
            bundle.putString("contentType", "1")
            ContentActivity.Companion.openRequestHelpFormFragment(bundle, false)
        })
        if (isComeBefore) {
            fragmentManager?.popBackStack()
        } else {
            if (getMyRequestedHelpsArrayList.size > 0) {
                mAdapter = MyRequestedHelpsAdapter(act, getMyRequestedHelpsArrayList)
                my_recycler_view.setAdapter(mAdapter)
            } else {
                GetMyRequestedHelps()
            }
        }
    }

    fun GetMyRequestedHelps() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetRequests(
                "$AUTH_TEXT ${mSessionManager.getAuthToken()}"
                /*mSessionManager.getUserCode(),*/)?.enqueue(
                object : Callback<ArrayList<RequestForHelp>?> {
                    override fun onFailure(call: Call<ArrayList<RequestForHelp>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<RequestForHelp>?>, response: Response<ArrayList<RequestForHelp>?>) {
                        if (response.body() != null) {
                            val getMyRequestedHelps = response.body()
                            if (mloading != null && getMyRequestedHelps != null) {
                                Log.d("getContentses size", "" + getMyRequestedHelps.size)
                                getMyRequestedHelpsArrayList.clear()
                                getMyRequestedHelpsArrayList.addAll(getMyRequestedHelps)
                                if (getMyRequestedHelps.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = MyRequestedHelpsAdapter(act, getMyRequestedHelpsArrayList)
                                my_recycler_view.setAdapter(mAdapter)
//                                if (getMyRequestedHelpsArrayList.size == 0) {
//                                    isComeBefore = true
//                                    ContentActivity.Companion.clearVariables()
//                                    val bundle = Bundle()
//                                    bundle.putString("isEdit", "0")
//                                    bundle.putString("contentType", "1")
//                                    ContentActivity.Companion.openRequestHelpFormFragment(bundle, false)
//                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    fun refreshList(){
        GetMyRequestedHelps()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        protected val TAG = MyRequestHelpFragment::class.java.simpleName
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var act: FragmentActivity
        lateinit var fragment: MyRequestHelpFragment

        fun newInstance(act: FragmentActivity): MyRequestHelpFragment {
            fragment = MyRequestHelpFragment()
            Companion.act = act
            return fragment
        }
    }
}