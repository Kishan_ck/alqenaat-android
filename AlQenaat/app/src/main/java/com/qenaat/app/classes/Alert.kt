package com.qenaat.app.classes

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.os.Build
import android.view.ContextThemeWrapper

object Alert {
    private fun createAlert(context: Activity?,
                            title: String?, message: String): AlertDialog.Builder {
        val dialog: AlertDialog.Builder = when {
            Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP -> AlertDialog.Builder(ContextThemeWrapper(context,
                    android.R.style.Theme_Material_Light_Dialog_Alert))
            else -> AlertDialog.Builder(context)
        }

        //  dialog.setIcon(R.mipmap.ic_launcher);
        if (title != null) dialog.setTitle(title) else dialog.setTitle("Information")
        dialog.setMessage(message)
        dialog.setCancelable(false)
        return dialog
    }

    private fun createCustomAlert(context: Activity,
                                  title: String?, message: String, layout: Int): AlertDialog.Builder {
        val dialog: AlertDialog.Builder = when {
            Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP -> AlertDialog.Builder(ContextThemeWrapper(context,
                    android.R.style.Theme_Material_Light_Dialog_Alert))
            else -> AlertDialog.Builder(context)
        }
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(layout, null)
        dialog.setView(dialogView)
        if (title != null) dialog.setTitle(title) else dialog.setTitle("Information")
        dialog.setMessage(message)
        dialog.setCancelable(false)
        return dialog
    }

    fun alert(context: Context?, title: String?, message: String,
              negativeButton: String?, positiveButton: String?,
              negativeRunnable: Runnable?, positiveRunnable: Runnable?) {
        val dialog = createAlert(context as Activity?, title, message)
        if (negativeButton != null) {
            dialog.setNegativeButton(negativeButton
            ) { dialog, which ->
                dialog.cancel()
                negativeRunnable?.run()
            }
        }
        if (positiveButton != null) {
            dialog.setPositiveButton(positiveButton
            ) { dialog, which ->
                dialog.dismiss()
                positiveRunnable?.run()
            }
        }
        dialog.show()
    }

    fun alertCustomView(context: Activity, title: String?, message: String,
                        negativeButton: String?, positiveButton: String?,
                        negativeRunnable: Runnable?, positiveRunnable: Runnable?, layout: Int) {
        val dialog = createCustomAlert(context, title, message, layout)
        if (negativeButton != null) {
            dialog.setNegativeButton(negativeButton
            ) { dialog, which ->
                dialog.cancel()
                negativeRunnable?.run()
            }
        }
        if (positiveButton != null) {
            dialog.setPositiveButton(positiveButton
            ) { dialog, which ->
                dialog.dismiss()
                positiveRunnable?.run()
            }
        }
        dialog.show()
    }
}