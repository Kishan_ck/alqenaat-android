package com.qenaat.app.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.domain.Event
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetHallCalender
import com.qenaat.app.model.GetHalls
import com.qenaat.app.networking.QenaatAPICall
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by DELL on 09-Nov-17.
 */
class CalendarHallFragment : Fragment() {
    lateinit var XY: IntArray
    lateinit var mFCalendarView: CompactCalendarView
    lateinit var loading: ProgressBar
    var df: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    lateinit var date_show: TextView
    lateinit var relative_parent: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var img_arrow_left: ImageView
    lateinit var img_arrow_right: ImageView
    lateinit var img_close: ImageView
    lateinit var getHalls: GetHalls
    private val dateFormatForMonth: SimpleDateFormat = SimpleDateFormat("MMMM - yyyy", Locale.ENGLISH)
    var formattedDate: String = ""
    var swipeDate: String = ""
    lateinit var linear1: LinearLayout
    lateinit var linear2: LinearLayout
    lateinit var linear3: LinearLayout
    lateinit var linear4: LinearLayout
    lateinit var linear5: LinearLayout
    lateinit var relative_content: RelativeLayout
    lateinit var linear_bottom: RelativeLayout
    private var bookingses: ArrayList<GetHallCalender?>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments?.containsKey("GetHalls")!!) {
                    val gson = Gson()
                    getHalls = gson.fromJson(arguments?.getString("GetHalls"),
                            GetHalls::class.java)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.calender_layout, container, false)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        relative_content = view.findViewById<View?>(R.id.relative_content) as RelativeLayout
        linear_bottom = view.findViewById<View?>(R.id.linear_bottom) as RelativeLayout
        linear5 = view.findViewById<View?>(R.id.linear5) as LinearLayout
        linear4 = view.findViewById<View?>(R.id.linear4) as LinearLayout
        linear3 = view.findViewById<View?>(R.id.linear3) as LinearLayout
        linear2 = view.findViewById<View?>(R.id.linear2) as LinearLayout
        linear1 = view.findViewById<View?>(R.id.linear1) as LinearLayout
        linear1.setVisibility(View.VISIBLE)
        linear2.setVisibility(View.VISIBLE)
        linear3.setVisibility(View.VISIBLE)
        linear4.setVisibility(View.VISIBLE)
        linear5.setVisibility(View.GONE)
        img_arrow_left = view.findViewById<View?>(R.id.img_arrow_left) as ImageView
        img_arrow_right = view.findViewById<View?>(R.id.img_arrow_right) as ImageView
        img_close = view.findViewById<View?>(R.id.img_close) as ImageView
        relative_parent = view.findViewById<View?>(R.id.relative_parent) as RelativeLayout
        mFCalendarView = view.findViewById<View?>(R.id.compactcalendar_view) as CompactCalendarView
        val c = Calendar.getInstance()
        val dayColumnNames = arrayOf<String?>("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
        mFCalendarView.setDayColumnNames(dayColumnNames)
        loading = view.findViewById<View?>(R.id.loading) as ProgressBar
        date_show = view.findViewById<View?>(R.id.date_show) as TextView

        //mFCalendarView.setShouldShowMondayAsFirstDay(false);
        if (!mSessionManager.getIsCalendarPopupShownBefore()) {
            ContentActivity.Companion.relative_message.setVisibility(View.VISIBLE)
        }
        ContentActivity.Companion.tv_ok.setTypeface(ContentActivity.Companion.tf)
        ContentActivity.Companion.tv_ok.setOnClickListener(View.OnClickListener {
            ContentActivity.Companion.relative_message.setVisibility(View.GONE)
            mSessionManager.setIsCalendarPopupShownBefore(true)
        })
        mFCalendarView.setListener(object : CompactCalendarView.CompactCalendarViewListener {
            override fun onDayClick(dateClicked: Date?) {

                //mFCalendarView.shouldSelectFirstDayOfMonthOnScroll(false);
                if (mSessionManager.isLoggedin()
                        && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                    val eventList: MutableList<Event?> = mFCalendarView.getEvents(dateClicked)
                    val fromDate = Calendar.getInstance().time
                    Log.d("date--", df.format(fromDate))
                    //val date = df.format(fromDate)
                    val date = df.format(dateClicked)
                    if (fromDate.compareTo(dateClicked) < 0 || df.format(fromDate) == df.format(dateClicked)) {
                        Log.e("date", df.format(dateClicked))
                        //codes
                        if (eventList != null) {
                            if (eventList.size > 0) {
                                val status = eventList[0]?.getData() as String?
                                Log.d("status", status)
                                if (status.equals("free", ignoreCase = true)) {
                                    mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(255, 241, 241, 241))
                                } else if (status.equals("pending", ignoreCase = true)) {
                                    for (i in bookingses?.indices!!) {
                                        val item = bookingses!![i]
                                        if (date == item?.BookingDate) {
                                            if (item?.BookCode == 1)
                                                mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 219, 166, 42))
                                            else
                                                mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 150, 166, 42))
                                            break
                                        }
                                    }
                                    //mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 219, 166, 42))
                                } else if (status.equals("block", ignoreCase = true)) {
                                    mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 130, 130, 130))
                                } else {
                                    Log.d("confirmed", "confirmed")
                                    mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 214, 35, 35))

                                    // mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 161, 206, 82));
                                }
                                //if (status.equals("free", ignoreCase = true) || status.equals("pending", ignoreCase = true)) {
                                if (status.equals("free", ignoreCase = true)) {
                                    val bundle = Bundle()
                                    val gson = Gson()
                                    bundle.putString("GetHalls", gson.toJson(getHalls))
                                    bundle.putString("date", df.format(dateClicked!!))
                                    ContentActivity.Companion.openBookHallFragment(bundle)
                                    mFCalendarView.hideCalendar()
                                } else if (status.equals("block", ignoreCase = true)) {

                                    //Snackbar.make(date_show, df.format(dateClicked) + " " + getString(R.string.Blocked), Snackbar.LENGTH_LONG).show();
                                } else if (status.equals("pending", ignoreCase = true)) {
                                    for (i in bookingses?.indices!!) {
                                        val item = bookingses!![i]
                                        if (date == item?.BookingDate) {
                                            if (item?.BookCode == 1)
                                                date_show.showSnakeBar(getString(R.string.already_booked))
                                            else {
                                                val bundle = Bundle()
                                                val gson = Gson()
                                                bundle.putString("GetHalls", gson.toJson(getHalls))
                                                bundle.putString("date", df.format(dateClicked!!))
                                                ContentActivity.Companion.openBookHallFragment(bundle)
                                                mFCalendarView.hideCalendar()
                                            }
                                            break
                                        }
                                    }
                                } else {

                                    /* Bundle  bundle = new Bundle();

                                    Gson gson = new Gson();

                                    bundle.putString("GetHalls", gson.toJson(getHalls));

                                    bundle.putString("date", df.format(dateClicked));

                                    ContentActivity.openBookHallFragment(bundle);

                                    mFCalendarView.hideCalendar();*/
                                    Snackbar.make(date_show, getString(R.string.AlreadyBooked), Snackbar.LENGTH_LONG).show()
                                }
                            }
                        }
                    } else {
                        if (eventList != null) {
                            if (eventList.size > 0) {
                                val status = eventList[0]?.getData() as String?
                                if (status.equals("free", ignoreCase = true)) {
                                    mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(255, 241, 241, 241))
                                } else if (status.equals("pending", ignoreCase = true)) {
                                    /* eventDays.add(Event(Color.argb(150, 150, 166, 42),
                                             timeInMilliseconds, "pending"))*/
                                    for (i in bookingses?.indices!!) {
                                        val item = bookingses!![i]
                                        if (date == item?.BookingDate) {
                                            if (item?.BookCode == 1)
                                                mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 219, 166, 42))
                                            else
                                                mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 150, 166, 42))
                                        }
                                    }
                                    //mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 219, 166, 42))
                                } else if (status.equals("block", ignoreCase = true)) {
                                    mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 130, 130, 130))
                                } else {
                                    Log.d("confirmed", "confirmed")
                                    mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 214, 35, 35))

                                    // mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 161, 206, 82));
                                }
                            }
                        }
                        Log.d("date", "greater than")
                        Snackbar.make(date_show, act.getString(R.string.FromDateLessThenToDate), Snackbar.LENGTH_LONG).show()
                    }
                } else {
                    Snackbar.make(view, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                    ContentActivity.Companion.openLoginFragment()
                }
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date?) {

                //toolbar.setTitle(df.format(firstDayOfNewMonth));
                mFCalendarView.shouldSelectFirstDayOfMonthOnScroll(false)
                loading.setVisibility(View.VISIBLE)
                val selectedDate = df.format(firstDayOfNewMonth)
                Log.e("selected month", selectedDate)
                GetHallBookings(selectedDate)
                date_show.setText(dateFormatForMonth.format(firstDayOfNewMonth))
            }
        })
        println("Current time => " + c.time)
        formattedDate = df.format(c.time)
        Log.e("today_date", formattedDate)
        date_show.setText(formattedDate)
        GetHallBookings(formattedDate)
        img_arrow_left.setOnClickListener(View.OnClickListener { /*if(swipeDate.length()==0){
                    swipeDate = formattedDate;
                }

                Calendar cal = Calendar.getInstance();

                try {
                    cal.setTime(df.parse(swipeDate));
                    cal.add(Calendar.MONTH, -1);

                    swipeDate = df.format(cal.getTime());

                    Log.d("swipeDate", ""+swipeDate);

                    loading.setVisibility(View.VISIBLE);

                    GetHallBookings(swipeDate);

                    date_show.setText(dateFormatForMonth.format(df.parse(swipeDate)));

                    try {
                        mFCalendarView.setCurrentDate(df.parse(swipeDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }*/
            mFCalendarView.showPreviousMonth()
        })
        img_arrow_right.setOnClickListener(View.OnClickListener { /*if(swipeDate.length()==0){
                    swipeDate = formattedDate;
                }

                Calendar cal = Calendar.getInstance();

                try {
                    cal.setTime(df.parse(swipeDate));
                    cal.add(Calendar.MONTH, 1);

                    swipeDate = df.format(cal.getTime());

                    Log.d("swipeDate", ""+swipeDate);

                    loading.setVisibility(View.VISIBLE);

                    GetHallBookings(swipeDate);

                    date_show.setText(dateFormatForMonth.format(df.parse(swipeDate)));

                    try {
                        mFCalendarView.setCurrentDate(df.parse(swipeDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }*/
            mFCalendarView.showNextMonth()
        })
        img_close.setOnClickListener(View.OnClickListener { fragmentManager?.popBackStack() })
        ContentActivity.Companion.setTextFonts(linear_bottom)
        return view
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
    }

    fun GetHallBookings(date: String?) {
        loading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(relative_parent)
        QenaatAPICall.getCallingAPIInterface()?.GetHallCalender(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                getHalls.Id,
                date)?.enqueue(
                object : Callback<ArrayList<GetHallCalender?>?> {

                    override fun onFailure(call: Call<ArrayList<GetHallCalender?>?>, t: Throwable) {
                        t.printStackTrace()
                        loading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(relative_parent)
                    }

                    override fun onResponse(call: Call<ArrayList<GetHallCalender?>?>, response: Response<ArrayList<GetHallCalender?>?>) {
                        if (response.body() != null) {
                            //val bookingses = response.body()
                            bookingses = response.body()
                            if (loading != null) {
                                if (bookingses != null) {
                                    Log.d("bookingses size", "" + bookingses?.size)
                                    if (bookingses?.size!! > 0) {
                                        mFCalendarView.removeAllEvents()
                                        val eventDays: MutableList<Event?> = ArrayList()
                                        val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)

                                        //now set the selected date to the view
                                        for (i in bookingses?.indices!!) {
                                            val date: String = bookingses!![i]?.BookingDate!!
                                            val dateParts: Array<String?> = date.split("/".toRegex()).toTypedArray()
                                            Log.e("date added ", dateParts[2].toString() + "-" + dateParts[1] + "-" + dateParts[0])

                                            //eventDays.add(dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0]);
                                            try {
                                                val mDate = sdf.parse(bookingses!![i]?.BookingDate)
                                                val timeInMilliseconds = mDate.time
                                                println("Date in milli :: $timeInMilliseconds")
                                                if (bookingses!![i]?.BookingStatus.equals("2", ignoreCase = true)) {
                                                    if (date.equals(formattedDate, ignoreCase = true)) {
                                                        mFCalendarView.setCurrentDayBackgroundColor(Color.argb(219, 214, 166, 42))
                                                        mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(219, 214, 166, 42))
                                                    }
                                                    //Own
                                                    if (bookingses!![i]?.BookCode == 1) {
                                                        eventDays.add(Event(Color.argb(150, 219, 166, 42),
                                                                timeInMilliseconds, "pending"))
                                                    }
                                                    //Other User
                                                    else {
                                                        eventDays.add(Event(Color.argb(150, 150, 166, 42),
                                                                timeInMilliseconds, "pending"))
                                                    }
                                                    /*eventDays.add(Event(Color.argb(150, 219, 166, 42),
                                                            timeInMilliseconds, "pending"))*/
                                                } else if (bookingses!![i]?.BookingStatus.equals("3", ignoreCase = true)) {
                                                    if (date.equals(formattedDate, ignoreCase = true)) {
                                                        mFCalendarView.setCurrentDayBackgroundColor(Color.argb(150, 214, 35, 35))
                                                        mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 214, 35, 35))
                                                    }
                                                    //                                        else{
//
//                                            mFCalendarView.setCurrentDayBackgroundColor(Color.argb(150, 241, 241, 241));
//
//                                        }
                                                    eventDays.add(Event(Color.argb(150, 214, 35, 35),
                                                            timeInMilliseconds, "confirmed"))
                                                } else if (bookingses!![i]?.BookingStatus.equals("4", ignoreCase = true)) {
                                                    if (date.equals(formattedDate, ignoreCase = true)) {
                                                        mFCalendarView.setCurrentDayBackgroundColor(Color.argb(150, 130, 130, 130))
                                                        mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 130, 130, 130))
                                                    }
                                                    eventDays.add(Event(Color.argb(150, 130, 130, 130),
                                                            timeInMilliseconds, "block"))
                                                } else {
                                                    eventDays.add(Event(Color.argb(255, 241, 241, 241),
                                                            timeInMilliseconds, "free"))
                                                    Log.d("eventDays 1-- > ", "" + date)
                                                    Log.d("eventDays 2-- > ", "" + formattedDate)
                                                    if (date.equals(formattedDate, ignoreCase = true)) {
                                                        Log.d("eventDays 3-- > ", "" + date)
                                                        Log.d("eventDays 4-- > ", "" + formattedDate)
                                                        mFCalendarView.setCurrentDayBackgroundColor(Color.argb(150, 170, 206, 204))
                                                        mFCalendarView.setCurrentSelectedDayBackgroundColor(Color.argb(150, 170, 206, 204))
                                                        eventDays.add(Event(Color.argb(150, 170, 206, 204),
                                                                timeInMilliseconds, "free"))
                                                    }
                                                }
                                            } catch (e: ParseException) {
                                                e.printStackTrace()
                                            }

                                            //eventDays.add(Util.getCurrentDate());
                                        }
                                        mFCalendarView.addEvents(eventDays)
                                        date_show.setText(dateFormatForMonth.format(mFCalendarView.getFirstDayOfCurrentMonth()))
                                    }
                                }
                            }
                            loading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(relative_parent)
                        }
                    }
                })
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        protected val TAG = CalendarHallFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: CalendarHallFragment
        fun newInstance(act: FragmentActivity): CalendarHallFragment {
            fragment = CalendarHallFragment()
            Companion.act = act
            return fragment
        }
    }
}