package com.qenaat.app.pushnotification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import me.leolin.shortcutbadger.ShortcutBadgeException
import me.leolin.shortcutbadger.ShortcutBadger

/**
 * Created by DELL on 17-Nov-16.
 */
class MyFcmListenerService : FirebaseMessagingService() {
    private var mNotificationManager: NotificationManager? = null
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.e("onMessageReceived", "Notification Body: " + remoteMessage.data)
        Log.e("onMessageReceived", "Notification Body Values: " + remoteMessage.data.values)
        Log.e("onMessageReceived", "From: " + remoteMessage.from)
        Log.e("onMessageReceived", "id: " + remoteMessage.data.values.toTypedArray()[0])
        Log.e("onMessageReceived", "type: " + remoteMessage.data.values.toTypedArray()[1])
        Log.e("onMessageReceived", "message: " + remoteMessage.data.values.toTypedArray()[3])
        Id = remoteMessage.data.values.toTypedArray()[0].toString()
        type = remoteMessage.data.values.toTypedArray()[2].toString()

        //sendNotification(remoteMessage.getData().values.toTypedArray().get(3).toString())
        sendNotification(remoteMessage.data)
    }

    // [END receive_message]
    var Id: String? = ""
    var type: String? = ""

    private fun sendNotification(map: Map<String, String>) {
        mNotificationManager = this
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        val shared: SharedPreferences = getSharedPreferences("counter", 0)
        notificationID = shared.getString("cc", "0")?.toInt()!!
        val contentIntent: PendingIntent
        contentIntent = PendingIntent.getActivity(this, notificationID,
                Intent(this,
                        ContentActivity::class.java)
                        .putExtra("id", Id)
                        .putExtra("type", type),
                PendingIntent.FLAG_UPDATE_CURRENT)
        val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH)

            notificationChannel.enableLights(true)
            notificationChannel.setLightColor(Color.RED)
            notificationChannel.setVibrationPattern(longArrayOf(0, 1000, 500, 1000))
            notificationChannel.enableVibration(true)
            mNotificationManager?.createNotificationChannel(notificationChannel)
        }
        val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.icon_512)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentTitle(map["title"])
                .setContentText(map["body"])

        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
        mBuilder.setContentIntent(contentIntent)
        mNotificationManager?.notify(notificationID, mBuilder.build())
        val badgeCount: Int = shared.getInt("noti_counter", 0) + 1
        try {
            ShortcutBadger.setBadge(getApplicationContext(), badgeCount)
        } catch (e: ShortcutBadgeException) {
        }
        val edit: SharedPreferences.Editor = shared.edit()
        edit.putString("cc", (notificationID + 1).toString() + "")
        edit.putInt("noti_counter", shared.getInt("noti_counter", 0) + 1)
        edit.commit()
    }

    private fun sendNotification1(msg: String?) {
        mNotificationManager = this
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        val shared: SharedPreferences = getSharedPreferences("counter", 0)
        notificationID = shared.getString("cc", "0")?.toInt()!!
        val contentIntent: PendingIntent
        contentIntent = PendingIntent.getActivity(this, notificationID,
                Intent(this,
                        ContentActivity::class.java)
                        .putExtra("id", Id)
                        .putExtra("type", type),
                PendingIntent.FLAG_UPDATE_CURRENT)
        val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH)

            // Configure the notification channel.
            notificationChannel.setDescription(msg)
            notificationChannel.enableLights(true)
            notificationChannel.setLightColor(Color.RED)
            notificationChannel.setVibrationPattern(longArrayOf(0, 1000, 500, 1000))
            notificationChannel.enableVibration(true)
            mNotificationManager?.createNotificationChannel(notificationChannel)
        }
        val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.icon_512)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setStyle(NotificationCompat.BigTextStyle().bigText(msg)).setTicker(msg).setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentText(msg)

        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
        mBuilder.setContentIntent(contentIntent)
        mNotificationManager?.notify(notificationID, mBuilder.build())
        val badgeCount: Int = shared.getInt("noti_counter", 0) + 1
        try {
            ShortcutBadger.setBadge(getApplicationContext(), badgeCount)
        } catch (e: ShortcutBadgeException) {
        }
        val edit: SharedPreferences.Editor = shared.edit()
        edit.putString("cc", (notificationID + 1).toString() + "")
        edit.putInt("noti_counter", shared.getInt("noti_counter", 0) + 1)
        edit.commit()
    }

    companion object {
        var notificationID = 0
    }
}