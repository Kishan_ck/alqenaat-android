package com.qenaat.app.model

class CheckTestingVersion {
    private var ShowPopup: String? = null
    private var PopupText: String? = null

    fun getShowPopup(): String? {
        return ShowPopup
    }

    fun setShowPopup(showPopup: String?) {
        ShowPopup = showPopup
    }

    fun getPopupText(): String? {
        return PopupText
    }

    fun setPopupText(popupText: String?) {
        PopupText = popupText
    }
}