package com.qenaat.app.classes

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Point
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore.MediaColumns
import android.provider.Settings
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.qenaat.app.SplashActivity
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.nio.ByteBuffer
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.jvm.Throws

//import org.apache.http.entity.ContentType;
object GlobalFunctions {
    var startIndex: ArrayList<Int?>? = null
    var endIndex: ArrayList<Int?>? = null
    const val MULTIPLE_PERMISSION_CODE = 304
    const val CAMERA_PERMISSION_CODE = 300
    const val STORAGE_PERMISSION_CODE = 301
    const val LOCATION_PERMISSION_CODE = 302
    const val CALL_PERMISSION_CODE = 303

    @Throws(Exception::class)
    fun createTemporaryFile(part: String?, ext: String?): File? {
        var tempDir = Environment.getExternalStorageDirectory()
        tempDir = File(tempDir.absolutePath + "/.temp/")
        if (!tempDir.exists()) {
            tempDir.mkdir()
        }
        return File.createTempFile(part, ext, tempDir)
    }

    fun fixOrientationFromCamIntent(path: String?, mBitmap: Bitmap?): Bitmap? {
        return try {
            val f = File(path)
            if (mBitmap?.getWidth()!! > mBitmap?.getHeight()) {
                val mat = Matrix()
                mat.postRotate(90f)
                val options = BitmapFactory.Options()
                options.inSampleSize = 2
                val bmp = BitmapFactory.decodeStream(FileInputStream(f),
                        null, options)
                // ByteArrayOutputStream outstudentstreamOutputStream = new
                // ByteArrayOutputStream();
                // bitmap.compress(Bitmap.CompressFormat.PNG, 100,
                // outstudentstreamOutputStream);
                Bitmap.createBitmap(bmp!!, 0, 0, bmp.width,
                        bmp.height, mat, true)
            } else {
                mBitmap
            }
        } catch (e: IOException) {
            Log.w("TAG", "-- Error IOException")
            null
        } catch (oom: OutOfMemoryError) {
            Log.w("TAG", "-- Error Out Of Memory")
            null
        }
    }

    fun FixBitmapRotation(myBitmap: Bitmap?, src: String?): Bitmap? {
        var myBitmap = myBitmap
        return try {
            val exif = ExifInterface(src)
            val orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 1)
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90f)
            } else if (orientation == 3) {
                matrix.postRotate(180f)
            } else if (orientation == 8) {
                matrix.postRotate(270f)
            }
            myBitmap = Bitmap.createBitmap(myBitmap!!, 0, 0, myBitmap?.getWidth()!!,
                    myBitmap?.getHeight(), matrix, true) // rotating bitmap
            myBitmap
        } catch (e: Exception) {
            null
        }
    }

    fun FixOrientationFromExif(myBitmap: Bitmap?,
                               imagePath: String?): Bitmap? {
        var myBitmap = myBitmap
        var orientation = -1
        return try {
            val exif = ExifInterface(imagePath)
            val exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL)
            val matrix = Matrix()
            when (exifOrientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> {
                    orientation = 270
                    Log.d("EXIF", "Exif: $orientation")
                    matrix.postRotate(270f)
                }
                ExifInterface.ORIENTATION_ROTATE_180 -> {
                    orientation = 180
                    Log.d("EXIF", "Exif: $orientation")
                    matrix.postRotate(180f)
                }
                ExifInterface.ORIENTATION_ROTATE_90 -> {
                    orientation = 90
                    Log.d("EXIF", "Exif: $orientation")
                    matrix.postRotate(90f)
                }
                ExifInterface.ORIENTATION_NORMAL -> {
                    orientation = 0
                    Log.d("EXIF", "Exif: $orientation")
                    matrix.postRotate(0f)
                }
                else -> {
                }
            }
            myBitmap = Bitmap.createBitmap(myBitmap!!, 0, 0, myBitmap?.getWidth()!!,
                    myBitmap.getHeight(), matrix, true) // rotating bitmap
            myBitmap
        } catch (e: IOException) {
            Log.e("EXIF ORIENTATIONPROBLEM",
                    "Unable to get image exif orientation", e)
            null
        }
    }

    fun isFilePortrait(imagePath: String?): Boolean {
        return try {
            val exif = ExifInterface(imagePath)
            val exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL)
            if (exifOrientation == ExifInterface.ORIENTATION_NORMAL) {
                true
            } else false
        } catch (e: IOException) {
            Log.e("EXIF ORIENTATIONPROBLEM",
                    "Unable to get image exif orientation", e)
            false
        }
    }

    fun DisableLayout(layout: ViewGroup?) {
        layout?.setEnabled(false)
        for (i in 0 until layout?.getChildCount()!!) {
            val child = layout.getChildAt(i)
            if (child is ViewGroup) {
                DisableLayout(child as ViewGroup)
            } else {
                child.isEnabled = false
            }
        }
    }

    fun EnableLayout(layout: ViewGroup?) {
        layout?.setEnabled(true)
        for (i in 0 until layout?.getChildCount()!!) {
            val child = layout.getChildAt(i)
            if (child is ViewGroup) {
                EnableLayout(child as ViewGroup)
            } else {
                child.isEnabled = true
            }
        }
    }

    fun VisibleLayout(layout: ViewGroup?) {
        layout?.setEnabled(true)
        for (i in 0 until layout?.getChildCount()!!) {
            val child = layout.getChildAt(i)
            if (child is ViewGroup) {
                VisibleLayout(child as ViewGroup)
            } else {
                child.visibility = View.VISIBLE
            }
        }
    }

    fun HideLayout(layout: ViewGroup?) {
        layout?.setEnabled(true)
        for (i in 0 until layout?.getChildCount()!!) {
            val child = layout.getChildAt(i)
            if (child is ViewGroup) {
                HideLayout(child as ViewGroup)
            } else {
                child.visibility = View.GONE
            }
        }
    }

    @SuppressLint("NewApi")
    fun getScreenWidthAndHeight(context: Context?): IntArray? {
        val columnWidth: Int
        val columnHeigh: Int
        val wm = context
                ?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = wm.defaultDisplay
        val point = Point()
        val currentapiVersion = Build.VERSION.SDK_INT
        try {
            if (currentapiVersion >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                display.getSize(point)
            } else {
                point.x = display.width
                point.y = display.height
            }
        } catch (ignore: NoSuchMethodError) { // Older device
            point.x = display.width
            point.y = display.height
        }
        columnWidth = point.x
        columnHeigh = point.y
        return intArrayOf(columnWidth, columnHeigh)
    }

    fun EncodeParameter(s: String?): String? {
//        if (s.length() > 0) {
//            try {
//                return (URLEncoder.encode(s, "UTF-8"));
//            } catch (UnsupportedEncodingException e) {
//                e.printStackTrace();
//            }
//        } else
        return s
        //return "";
    }

    fun EncodeParameters(s: String?): String? {
        if (s?.length!! > 0) {
            try {
                return URLEncoder.encode(s, "UTF-8")
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
        } else return s
        return ""
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches()
        }
    }

    fun isNumeric(s: String?): Boolean {
        val pattern = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+$")
        return if (pattern.matcher(s).matches()) {
            true
        } else false
    }

    // @SuppressLint("NewApi")
    // public static String encodeToBase64(String string) {
    // String encodedString = "";
    // try {
    // byte[] byteData = null;
    // if (Build.VERSION.SDK_INT >= 8) // Build.VERSION_CODES.FROYO --> 8
    // {
    // byteData = android.util.Base64.encode(string.getBytes(),
    // android.util.Base64.DEFAULT);
    // } else {
    // byteData = Base64Utility.encode(string.getBytes(),
    // Base64Utility.DEFAULT);
    // }
    // encodedString = new String(byteData);
    // } catch (Exception e) {
    // }
    // return encodedString;
    // }
    fun GetMapUri(lat: String?, lng: String?, label: String?,
                  zoomValue: String?): Uri? {
        val uriBegin = "geo:$lat,$lng"
        val query = "$lat,$lng($label)"
        val encodedQuery = Uri.encode(query)
        val uriString = "$uriBegin?q=$encodedQuery&z=$zoomValue"
        return Uri.parse(uriString)
    }

    fun saveBitmap(bitmap: Bitmap?, ext: String?): File? {
        val extStorageDirectory = Environment.getExternalStorageDirectory()
                .toString() + "/"
        val fileName = UUID.randomUUID().toString() + ext
        var file: File? = null
        file = File(extStorageDirectory, fileName)
        Log.e("file exist", "" + file.exists().toString())
        try {
            val bos = ByteArrayOutputStream()
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            val bitmapdata = bos.toByteArray()

            // write the bytes in file
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        Log.e("file", "" + file)
        return file
    }

    fun CutTextAfterSpecificChar(text: String?, number: Int): String? {
        return if (text?.length!! >= 120) {
            text.substring(0, number)
        } else {
            text
        }
    }

    fun ConvertBitMapToByteArray(b: Bitmap?): ByteArray? {
        val bytes = getSizeInBytes(b)
        val buffer = ByteBuffer.allocate(bytes as Int)
        b?.copyPixelsToBuffer(buffer)
        return buffer.array()
    }

    fun getSizeInBytes(bitmap: Bitmap?): Long {
        return bitmap?.getRowBytes()!! * bitmap.getHeight().toLong()
    }

    fun returnToFirstActivity(con: Context?) {
        val intent = Intent(con, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("EXIT", true)
        con?.startActivity(intent)
    }

    fun CutTextAfterSpecificNumberSpace(text: String?, spaceNumber: Int): String? {
        var text = text
        return try {
            val intArr: Array<Int?>?
            if (text != null && text.length > 0) {
                intArr = chekHowManyContainsSpace(text.toCharArray(),
                        spaceNumber)
                if (intArr?.get(0)!! > spaceNumber) {
                    text = text.substring(0, intArr.get(1)!!) + " ..."
                    text
                } else {
                    text = "$text ."
                    text
                }
            } else text
        } catch (e: Exception) {
            e.printStackTrace()
            text
        }
    }

    fun getYoutubeVideoId(youtubeUrl: String?): String? {
        var video_id: String? = ""
        if (youtubeUrl != null && youtubeUrl.trim { it <= ' ' }.length > 0 && youtubeUrl.startsWith("http")) {
            val expression = ("^.*((youtu.be"
                    + "\\/)"
                    + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*")
            val input: CharSequence? = youtubeUrl
            val pattern = Pattern.compile(expression,
                    Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(input)
            if (matcher.matches()) {
                val groupIndex1 = matcher.group(7)
                if (groupIndex1 != null && groupIndex1.length == 11) video_id = groupIndex1
            }
        }
        return video_id
    }

    fun GetYoutubeURLFromIFrame(text: String?): String? {
        var firstText: String? = null
        var secondText: String? = null
        var thirdText: String? = null
        try {
            var firstIndex = 0
            if (text != null && text.length > 0) {
                return if (text.contains("src")) {
                    firstIndex = text.indexOf('"', text.indexOf("src"))
                    firstText = text.substring(0, firstIndex + 1)
                    secondText = text.substring(firstIndex + 1, text.length)
                    thirdText = secondText
                            .substring(0, secondText.indexOf('"'))
                    thirdText
                } else {
                    text
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return text
        }
        return thirdText
    }

    fun chekHowManyContainsSpace(charArr: CharArray?, spaceNumber: Int): Array<Int?>? {
        var _countSpace = 0
        var indexOfSpecificSpace = 0
        var _counter = 0
        try {
            for (i in charArr?.indices!!) {
                if (charArr.get(i) == ' ') _countSpace++
            }
            for (j in charArr.indices) {
                if (charArr.get(j) == ' ') {
                    if (charArr.get(j + 1) != ' ') {
                        _counter++
                    }
                    if (_counter == spaceNumber) {
                        indexOfSpecificSpace = j
                        break
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return arrayOf(_countSpace, indexOfSpecificSpace)
    }

    fun getPath(uri: Uri?, act: Activity?): String? {
        val projection = arrayOf<String?>(MediaColumns.DATA)
        val cursor = act?.getContentResolver()?.query(uri!!, projection, null,
                null, null)
        val column_index = cursor?.getColumnIndexOrThrow(MediaColumns.DATA)
        cursor?.moveToFirst()
        return cursor?.getString(column_index!!)
    }

    /**
     * Upload Image To Specific Folder On Server
     *
     * @param fileName            image to upload
     * @param serverUri           web address Server Uri
     * @param serverFolderUploads specify folder on server to store image, you can pass it as
     * empty string
     * @return Saved path to our image file
     * @author A.Hegazy
     */
    fun uploadImage(fileName: String?, serverUri: String?,
                    serverFolderUploads: String?): String? {
        var connection: HttpURLConnection? = null
        var outputStream: DataOutputStream? = null
        var pathToOurFile = fileName
        val lineEnd = "\r\n"
        val twoHyphens = "--"
        val boundary = "*****"
        val charset = "UTF-8"
        var bytesRead: Int
        var bytesAvailable: Int
        var bufferSize: Int
        val buffer: ByteArray
        val maxBufferSize = 1 * 1024 * 1024
        return try {
            val fileInputStream = FileInputStream(File(
                    pathToOurFile))
            val url = URL(serverUri)
            connection = url.openConnection() as HttpURLConnection

            // Allow Inputs & Outputs
            connection.doInput = true
            connection.doOutput = true
            connection.useCaches = false
            pathToOurFile = UUID.randomUUID().toString() + ".jpg"

            // Enable POST method
            connection.requestMethod = "POST"
            connection.setRequestProperty("Connection", "Keep-Alive")
            connection.setRequestProperty("Accept-Charset", charset)
            connection.setRequestProperty("ENCTYPE", "multipart/form-data")
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=$boundary")
            outputStream = DataOutputStream(connection.outputStream)
            outputStream.writeBytes(twoHyphens + boundary + lineEnd)
            outputStream.writeBytes("Content-Disposition: form-data; name=\""
                    + serverFolderUploads + "\"" + ";  filename=\""
                    + pathToOurFile + "\"" + lineEnd)
            outputStream.writeBytes(lineEnd)
            bytesAvailable = fileInputStream.available()
            bufferSize = Math.min(bytesAvailable, maxBufferSize)
            buffer = ByteArray(bufferSize)

            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize)
            try {
                while (bytesRead > 0) {
                    outputStream.write(buffer, 0, bytesRead)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd)

            // Responses from the server (code and message)
            val serverResponseCode = connection.responseCode
            val serverResponseMessage = connection.responseMessage
            fileInputStream.close()
            outputStream.flush()
            outputStream.close()
            if (serverResponseCode == 200 && serverResponseMessage == "OK") pathToOurFile else ""
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    /**
     * Upload Image To Specific Folder On Server
     *
     * @param bitmap              image to upload
     * @param serverUri           web address Server Uri
     * @param serverFolderUploads specify folder on server to store image, you can pass it as
     * empty string
     * @return Saved path to our image file
     * @author A.Hegazy
     */
    fun uploadImage(bitmap: Bitmap?, serverUri: String?,
                    serverFolderUploads: String?): String? {
        var connection: HttpURLConnection? = null
        var outputStream: DataOutputStream? = null
        val lineEnd = "\r\n"
        val twoHyphens = "--"
        val boundary = "*****"
        val charset = "UTF-8"
        val videoName: String
        val buffer: ByteArray?
        return try {
            val url = URL(serverUri)
            connection = url.openConnection() as HttpURLConnection

            // Allow Inputs & Outputs
            connection.doInput = true
            connection.doOutput = true
            connection.useCaches = false
            videoName = UUID.randomUUID().toString() + ".mp4"

            // Enable POST method
            connection.requestMethod = "POST"
            connection.setRequestProperty("Connection", "Keep-Alive")
            connection.setRequestProperty("Accept-Charset", charset)
            connection.setRequestProperty("ENCTYPE", "multipart/form-data")
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=$boundary")
            outputStream = DataOutputStream(connection.outputStream)
            outputStream.writeBytes(twoHyphens + boundary + lineEnd)
            outputStream.writeBytes("Content-Disposition: form-data; name=\""
                    + serverFolderUploads + "\"" + ";  filename=\"" + videoName
                    + "\"" + lineEnd)
            outputStream.writeBytes(lineEnd)
            buffer = ConvertBitMapToByteArray(bitmap)
            try {
                outputStream.write(buffer, 0, getSizeInBytes(bitmap) as Int)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd)

            // Responses from the server (code and message)
            val serverResponseCode = connection.responseCode
            val serverResponseMessage = connection.responseMessage
            outputStream.flush()
            outputStream.close()
            if (serverResponseCode == 200 && serverResponseMessage == "OK") videoName else ""
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    /**
     * Upload video To Specific Folder On Server
     *
     * @param fileName            image to upload
     * @param serverUri           web address Server Uri
     * @param serverFolderUploads specify folder on server to store video, you can pass it as
     * empty string
     * @return Saved path to our video file
     * @author A.Hegazy
     */
    fun uploadVideo(fileName: String?, serverUri: String?,
                    serverFolderUploads: String?): String? {
        var connection: HttpURLConnection? = null
        var outputStream: DataOutputStream? = null
        var pathToOurFile = fileName
        val lineEnd = "\r\n"
        val twoHyphens = "--"
        val boundary = "*****"
        val charset = "UTF-8"
        var bytesRead: Int
        var bytesAvailable: Int
        var bufferSize: Int
        val buffer: ByteArray?
        val maxBufferSize = 1 * 1024 * 1024
        return try {
            val f = File(pathToOurFile)
            val fileInputStream = FileInputStream(f)
            val url = URL(serverUri)
            connection = url.openConnection() as HttpURLConnection

            // Allow Inputs & Outputs
            connection.doInput = true
            connection.doOutput = true
            connection.useCaches = false
            pathToOurFile = UUID.randomUUID().toString() + ".mp4"

            // Enable POST method
            connection.requestMethod = "POST"
            connection.setRequestProperty("Connection", "Keep-Alive")
            connection.setRequestProperty("Accept-Charset", charset)
            connection.setRequestProperty("ENCTYPE", "multipart/form-data")
            connection.setRequestProperty("Content-Type",
                    "multipart/form-data;boundary=$boundary")
            outputStream = DataOutputStream(connection.outputStream)
            outputStream.writeBytes(twoHyphens + boundary + lineEnd)
            outputStream.writeBytes("Content-Disposition: form-data; name=\""
                    + serverFolderUploads + "\"" + ";  filename=\""
                    + pathToOurFile + "\"" + lineEnd)
            outputStream.writeBytes(lineEnd)
            bytesAvailable = fileInputStream.available()
            bufferSize = Math.min(bytesAvailable, maxBufferSize)
            // buffer = new byte[bufferSize];
            buffer = null //FileUtils.readFileToByteArray(f); @#$^@%^@$%^@$%&@$%&@$&&&&&&^%%%%%%%@##############@@@@@@@@@@@@@@
            // ******************************************************************************************************
            /*
			 * byte byt[]=new byte[bufferSize]; fileInputStream.read(byt);
			 *
			 * bytesRead = fileInputStream.read(buffer, 0, bufferSize);
			 *
			 * outputStream.write(buffer, 0, bufferSize);
			 */
            // Read file
            bytesRead = fileInputStream.read(buffer, 0, bufferSize)
            try {
                while (bytesRead > 0) {
                    outputStream.write(buffer, 0, bytesRead)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                }
            } catch (e: Exception) {
                Log.e("Uploading Video:::", e.message)
                e.printStackTrace()
            }
            outputStream.writeBytes(lineEnd)
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens
                    + lineEnd)

            // Responses from the server (code and message)
            val serverResponseCode = connection.responseCode
            Log.e("serverResponseCode:::", serverResponseCode.toString())
            val serverResponseMessage = connection.responseMessage
            Log.e("serverResponseMessage:", serverResponseMessage)
            fileInputStream.close()
            outputStream.flush()
            outputStream.close()
            if (serverResponseCode == 200 && serverResponseMessage == "OK") pathToOurFile else ""
        } catch (e: Exception) {
            Log.e("Uploading Video:::", e.message)
            e.printStackTrace()
            ""
        }
    }

    fun ResizeBitmap(bm: Bitmap?, newHeight: Int, newWidth: Int): Bitmap? {
        val width = bm?.getWidth()
        val height = bm?.getHeight()
        val scaleWidth = newWidth as Float / width!!
        val scaleHeight = newHeight as Float / height!!
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)

        // "RECREATE" THE NEW BITMAP
        return Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false)
    }

    fun sendEmail(act: Activity?, emailAddresses: Array<String?>?,
                  carbonCopies: Array<String?>?, subject: String?, message: String?) {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.data = Uri.parse("mailto:")
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emailAddresses)
        emailIntent.putExtra(Intent.EXTRA_CC, carbonCopies)
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        emailIntent.putExtra(Intent.EXTRA_TEXT, message)
        emailIntent.type = "message/rfc822"
        act?.startActivity(Intent.createChooser(emailIntent, "Email"))
    }

    fun GetWordStartWithHashTags(text: String?) {
        if (text?.length!! > 0) {
            val charLst = text.toCharArray()
            for (i in charLst.indices) {
                if (charLst[i] == '#' || charLst[i] == '@') {
                    startIndex?.add(i)
                }
            }
        }
    }

    fun GetWordEndWithHashTags(text: String?,
                               arrLst: ArrayList<Int?>?) {
        if (text?.length!! > 0 && arrLst?.size!! > 0) {
            val charLst = text.toCharArray()
            for (j in arrLst.indices) {
                for (i in arrLst.get(j)!! until charLst.size) {
                    if (charLst[i + 1] < charLst.size.toChar()) {
                        if (charLst[i + 1] == '#' || charLst[i + 1] == '@' || charLst[i + 1] == ' ') {
                            endIndex?.add(i + 1)
                            break
                        }
                    }
                }
            }
        }
    }

    //We are calling this method to check the permission status
    fun isReadCallAllowed(act: FragmentActivity?): Boolean {
        //Getting the permission status
        val result = ContextCompat.checkSelfPermission(act!!, Manifest.permission.CALL_PHONE)

        //If permission is granted returning true
        return if (result == PackageManager.PERMISSION_GRANTED) true else false

        //If permission is not granted returning false
    }

    //Requesting permission
    fun requestCallPermission(act: FragmentActivity?) {
        ActivityCompat.requestPermissions(act!!, arrayOf<String?>(Manifest.permission.CALL_PHONE),
                CALL_PERMISSION_CODE)
    }

    //We are calling this method to check the permission status
    fun isReadStorageAllowed(act: FragmentActivity?): Boolean {
        //Getting the permission status
        val result = ContextCompat.checkSelfPermission(act!!, Manifest.permission.READ_EXTERNAL_STORAGE)
        val result1 = ContextCompat.checkSelfPermission(act!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)

        //If permission is granted returning true
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED

        //If permission is not granted returning false
    }

    //Requesting permission
    fun requestStoragePermission(act: FragmentActivity?) {
        ActivityCompat.requestPermissions(act!!, arrayOf<String?>(Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
    }

    //We are calling this method to check the permission status
    fun isGPSAllowed(act: FragmentActivity?): Boolean {
        //Getting the permission status
        val result = ContextCompat.checkSelfPermission(act!!, Manifest.permission.ACCESS_FINE_LOCATION)

        //If permission is granted returning true
        return result == PackageManager.PERMISSION_GRANTED

        //If permission is not granted returning false
    }

    //Requesting permission
    fun requestGPSPermission(act: FragmentActivity?) {
        ActivityCompat.requestPermissions(act!!, arrayOf<String?>(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_CODE)
    }

    //We are calling this method to check the permission status
    fun isCameraAllowed(act: FragmentActivity?): Boolean {
        //Getting the permission status
        val result = ContextCompat.checkSelfPermission(act!!, Manifest.permission.CAMERA)

        //If permission is granted returning true
        return if (result == PackageManager.PERMISSION_GRANTED) true else false

        //If permission is not granted returning false
    }

    //Requesting permission
    fun requestCameraePermission(act: FragmentActivity?) {
        ActivityCompat.requestPermissions(act!!, arrayOf<String?>(Manifest.permission.CAMERA),
                CAMERA_PERMISSION_CODE)
    }

    fun requestMultiplePermission(act: FragmentActivity?, arrayPermission: Array<String>) {
        ActivityCompat.requestPermissions(act!!, arrayPermission, MULTIPLE_PERMISSION_CODE)
    }

    fun redirectAppPermission(activity: FragmentActivity) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", activity.packageName, null)
        intent.data = uri
        activity.startActivity(intent)
    }

    fun isUserLoggedIn(context: Context): Boolean {
        val sessionManager = SessionManager(context)
        return if (sessionManager.isLoggedin()
                && sessionManager.getUserCode() !== "" && sessionManager.getUserCode() != null) {
            true
        } else {
            false
        }
    }

    fun setMyDateTimeFormat(myDate: String?): String? {
        println("from Current Date Time : $myDate")
        val originalFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        val targetFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.ENGLISH)
        var date: Date? = null
        try {
            date = originalFormat.parse(myDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val formattedDate = targetFormat.format(date) // 20120821
        println("to Current Date Time : $formattedDate")
        return formattedDate
    }

    fun takeScreenshot(fragmentActivity: FragmentActivity?) {
        val now = Date()
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now)
        try {
            // image naming and path  to include sd card  appending name you choose for file
            val mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg"

            // create bitmap screen capture
            val v1 = fragmentActivity?.getWindow()?.decorView?.rootView
            v1?.isDrawingCacheEnabled = true
            val bitmap = Bitmap.createBitmap(v1?.drawingCache!!)
            v1?.isDrawingCacheEnabled = false
            val imageFile = File(mPath)
            val outputStream = FileOutputStream(imageFile)
            val quality = 100
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream)
            outputStream.flush()
            outputStream.close()
            openScreenshot(imageFile, fragmentActivity)
        } catch (e: Throwable) {
            // Several error may come out with file handling or DOM
            e.printStackTrace()
        }
    }

    fun openScreenshot(imageFile: File?, fragmentActivity: FragmentActivity?) {
        val intent = Intent()
        intent.action = Intent.ACTION_VIEW
        val uri = Uri.fromFile(imageFile)
        intent.setDataAndType(uri, "image/*")
        fragmentActivity?.startActivity(intent)
    }

    fun validateCivilID(civilID: String): Boolean {
        var valid = false
        val test = 11 - ((civilID.substring(0, 1).toInt() * 2 +
                civilID.substring(1, 2).toInt() * 1 +
                civilID.substring(2, 3).toInt() * 6 +
                civilID.substring(3, 4).toInt() * 3 +
                civilID.substring(4, 5).toInt() * 7 +
                civilID.substring(5, 6).toInt() * 9 +
                civilID.substring(6, 7).toInt() * 10 +
                civilID.substring(7, 8).toInt() * 5 +
                civilID.substring(8, 9).toInt() * 8 +
                civilID.substring(9, 10).toInt() * 4 +
                civilID.substring(10, 11).toInt() * 2) % 11).toDouble()
        val checksumValue = civilID.substring(11).toDouble()
        if (test == checksumValue) {
            valid = true
        }
        return valid
    }
}