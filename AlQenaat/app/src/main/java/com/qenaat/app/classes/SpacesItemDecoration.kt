package com.qenaat.app.classesimport

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by shahbazshaikh on 23/07/16.
 */
class SpacesItemDecoration(space: Int) : RecyclerView.ItemDecoration() {
    private val halfSpace: Int

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.getPaddingLeft() != halfSpace) {
            parent.setPadding(halfSpace, halfSpace, halfSpace, halfSpace)
            parent.setClipToPadding(false)
        }
        outRect.top = halfSpace
        outRect.bottom = halfSpace
        outRect.left = halfSpace
        outRect.right = halfSpace
    }

    init {
        halfSpace = space / 2
    }
}