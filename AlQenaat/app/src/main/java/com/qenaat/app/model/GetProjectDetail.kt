package com.qenaat.app.model

/**
 * Created by DELL on 14-Nov-17.
 */
class GetProjectDetail {
    var Id: String? = null
    var ProjectNameEN: String? = null
    var ProjectNameAR: String? = null
    var Amount: String? = null
    var TotalCost: String? = null
    var DetailsEN: String? = null
    var DetailsAr: String? = null
    var Photo: String? = null
    var HaveOpenBudget: String? = null
    var IsClosed: String? = null
    var Type: String? = null
    /*var photos: ArrayList<Photos?>? = null

    inner class Photos {
        var Id: String? = null
        var Photo: String? = null
    }*/
}