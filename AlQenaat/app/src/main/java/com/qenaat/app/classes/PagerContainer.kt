package com.qenaat.app.classes

import android.content.Context
import android.graphics.Point
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import androidx.viewpager.widget.ViewPager

/**
 * PagerContainer: A layout that displays a ViewPager with its children that are outside
 * the typical pager bounds.
 */
class PagerContainer : FrameLayout, ViewPager.OnPageChangeListener {
    private var mPager: ViewPager? = null
    var mNeedsRedraw = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        init()
    }

    private fun init() {
        //Disable clipping of children so non-selected pages are visible
        setClipChildren(false)

        //Child clipping doesn't work with hardware acceleration in Android 3.x/4.x
        //You need to set this value here if using hardware acceleration in an
        // application targeted at these releases.
        setLayerType(View.LAYER_TYPE_SOFTWARE, null)
    }

    protected override fun onFinishInflate() {
        super.onFinishInflate()
        try {
            mPager = getChildAt(0) as ViewPager?
            mPager?.setOnPageChangeListener(this)
        } catch (e: Exception) {
            throw IllegalStateException("The root child of PagerContainer must be a ViewPager")
        }
    }

    fun getViewPager(): ViewPager? {
        return mPager
    }

    private val mCenter: Point = Point()
    private val mInitialTouch: Point = Point()
    protected override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        mCenter?.x = w / 2
        mCenter?.y = h / 2
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        //We capture any touches not already handled by the ViewPager
        // to implement scrolling from a touch outside the pager bounds.
        when (ev.getAction()) {
            MotionEvent.ACTION_DOWN -> {
                mInitialTouch.x = ev.getX() as Int
                mInitialTouch.y = ev.getY() as Int
                ev.offsetLocation(mCenter.x - mInitialTouch.x.toFloat(), mCenter.y - mInitialTouch.y.toFloat())
            }
            else -> ev.offsetLocation(mCenter.x - mInitialTouch.x.toFloat(), mCenter.y - mInitialTouch.y.toFloat())
        }
        return mPager?.dispatchTouchEvent(ev)!!
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        //Force the container to redraw on scrolling.
        //Without this the outer pages render initially and then stay static
        if (mNeedsRedraw) invalidate()
    }

    override fun onPageSelected(position: Int) {}
    override fun onPageScrollStateChanged(state: Int) {
        mNeedsRedraw = state != ViewPager.SCROLL_STATE_IDLE
    }
}