package com.qenaat.app.model

/**
 * Created by DELL on 10/17/2016.
 */
class GetCommercialAds {

    var Id: String? = null
    var Name: String? = null
    var Photo: String? = null
    var Link: String? = null
    var FromDate: String? = null
    var ToDate: String? = null
    var IsActive: String? = null
    var CommAddPlacesId: String? = null
}