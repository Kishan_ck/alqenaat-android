package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.ServiceAdapter
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetServiceTypes
import com.qenaat.app.model.GetServices
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 29-Oct-17.
 */
class ServiceListFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    lateinit private var mAdapter: RecyclerView.Adapter<*>
    lateinit private var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    lateinit var getServiceTypes: GetServiceTypes
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var getServicesArrayList: ArrayList<GetServices> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetServiceTypes")) {
                    val gson = Gson()
                    getServiceTypes = gson.fromJson(arguments!!.getString("GetServiceTypes"),
                            GetServiceTypes::class.java)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById<View?>(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.ServicesLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                bundle.putString("isEdit", "0")
                bundle.putString("GetServiceTypes", Gson().toJson(getServiceTypes))
                ContentActivity.Companion.openAddServiceFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
        })
        if (getServicesArrayList.size > 0) {
            mAdapter = ServiceAdapter(act, getServicesArrayList, 0)
            my_recycler_view.setAdapter(mAdapter)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else {
            GetServices()
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
    }

    fun GetServices() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetServices("", "0", "0",
                getServiceTypes.Id, "" + page_index, "0")?.enqueue(
                object : Callback<ArrayList<GetServices>?> {
                    override fun onFailure(call: Call<ArrayList<GetServices>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetServices>?>, response: Response<ArrayList<GetServices>?>) {
                        if (response.body() != null) {
                            val getServices = response.body()
                            if (mloading != null && getServices != null) {
                                Log.d("getContentses size", "" + getServices.size)
                                getServicesArrayList.clear()
                                getServicesArrayList.addAll(getServices)
                                if (getServices.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = ServiceAdapter(act, getServicesArrayList, 0)
                                my_recycler_view.setAdapter(mAdapter)
                                my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        if (!endOfREsults) {
                                            visibleItemCount = mLayoutManager.getChildCount()
                                            totalItemCount = mLayoutManager.getItemCount()
                                            pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                                            if (loading_flag) {
                                                if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                    if (getServicesArrayList.size != 0) {
                                                        progress_loading_more.setVisibility(View.VISIBLE)
                                                        //GlobalFunctions.DisableLayout(mainLayout);
                                                    }
                                                    loading_flag = false
                                                    page_index = page_index + 1
                                                    GetMoreServices()
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    fun GetMoreServices() {
        QenaatAPICall.getCallingAPIInterface()?.GetServices("", "0", "0",
                getServiceTypes.Id, "" + page_index, "0")?.enqueue(
                object : Callback<ArrayList<GetServices>?> {

                    override fun onFailure(call: Call<ArrayList<GetServices>?>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetServices>?>, response: Response<ArrayList<GetServices>?>) {
                        if (response.body() != null) {
                            val getServices = response.body()
                            if (progress_loading_more != null) {
                                progress_loading_more.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                if (getServices != null) {
                                    if (getServices.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    getServicesArrayList.addAll(getServices)
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                })
    }

    companion object {
        protected val TAG = ServiceListFragment::class.java.simpleName
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var act: FragmentActivity
        lateinit var fragment: ServiceListFragment
        fun newInstance(act: FragmentActivity): ServiceListFragment {
            fragment = ServiceListFragment()
            Companion.act = act
            return fragment
        }
    }
}