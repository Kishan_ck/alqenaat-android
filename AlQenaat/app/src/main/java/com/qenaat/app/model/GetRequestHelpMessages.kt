package com.qenaat.app.model

/**
 * Created by DELL on 15-Nov-17.
 */
class GetRequestHelpMessages {
    var Id: String? = null
    var MessageContent: String? = null
    var Photo: String? = null
    var UserId: String? = null
    var TimeAgoEn: String? = null
    var TimeAgoAr: String? = null
}