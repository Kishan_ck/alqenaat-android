package com.qenaat.app.model

/**
 * Created by DELL on 12-Feb-18.
 */
class Filters {
    var CityIds: String? = null
    var CityNames: String? = null
    var AreaIds: String? = null
    var AreaNames: String? = null
    var Gender: String? = null
    var AgeFrom: String? = null
    var AgeTo: String? = null
    var PersonType: String? = null
    var Keyword: String? = null
}