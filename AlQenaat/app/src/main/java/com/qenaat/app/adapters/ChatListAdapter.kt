package com.qenaat.app.adapters

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.RoundedCornersTransform
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetRequestHelpMessages
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by DELL on 13-May-17.
 */
class ChatListAdapter(var act: FragmentActivity?, private val itemsData: ArrayList<GetRequestHelpMessages?>?) : RecyclerView.Adapter<ChatListAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager?
    var sessionManager: SessionManager?
    var type: String? = null

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData?.get(position) != null) {
            if (itemsData.get(position)?.UserId.equals(sessionManager?.getUserCode(), ignoreCase = true)) {
                viewHolder.linear_admin?.setVisibility(View.GONE)
                viewHolder.linear_user?.setVisibility(View.VISIBLE)
                viewHolder.relative_user_text?.setVisibility(View.GONE)
                viewHolder.relative_user_image?.setVisibility(View.GONE)
                if (itemsData.get(position)?.Photo?.length!! > 0) {
                    viewHolder.relative_user_image?.setVisibility(View.VISIBLE)
                    viewHolder.img_chat_user?.getLayoutParams()?.width = (act?.getResources()?.getDrawable(
                            R.drawable.cat_no_img) as BitmapDrawable).bitmap.width +
                            (act?.getResources()?.getDrawable(
                                    R.drawable.cat_no_img) as BitmapDrawable).bitmap.width / 2
                    viewHolder.img_chat_user?.getLayoutParams()?.height = (act?.getResources()?.getDrawable(
                            R.drawable.cat_no_img) as BitmapDrawable).bitmap.height
                    Picasso.with(act).load(itemsData.get(position)?.Photo).placeholder(R.drawable.cat_no_img)
                            .transform(RoundedCornersTransform(40, 0))
                            .error(R.drawable.cat_no_img).fit()
                            .centerCrop().into(viewHolder.img_chat_user)
                    viewHolder.img_chat_user?.setOnClickListener(View.OnClickListener {
                        val productImages: Array<String?>
                        productImages = arrayOfNulls<String?>(1)
                        productImages[0] = itemsData.get(position)?.Photo
                        val b = Bundle()
                        b.putInt("position", 0)
                        b.putStringArray("productImages", productImages)
                        ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                    })
                }
                if (itemsData.get(position)?.MessageContent?.length!! > 0) {
                    viewHolder.relative_user_text?.setVisibility(View.VISIBLE)
                    viewHolder.tv_message_user?.setText(itemsData.get(position)?.MessageContent)
                    Linkify.addLinks(viewHolder.tv_message_user!!, Linkify.ALL)
                }
                if (position > 0) {
                    if (itemsData.get(position - 1) != null) {
                        if (itemsData.get(position - 1)?.UserId != null &&
                                itemsData.get(position)?.UserId != null) {
                            if (itemsData.get(position - 1)?.UserId.equals(itemsData.get(position)?.UserId, ignoreCase = true)) {
                                viewHolder.tv_message_user?.setText(itemsData.get(position)?.MessageContent)
                                Linkify.addLinks(viewHolder.tv_message_user!!, Linkify.ALL)
                            }
                        }
                    }
                }
            } else {
                viewHolder.linear_admin?.setVisibility(View.VISIBLE)
                viewHolder.linear_user?.setVisibility(View.GONE)
                viewHolder.relative_admin_text?.setVisibility(View.GONE)
                viewHolder.relative_admin_image?.setVisibility(View.GONE)
                if (itemsData.get(position)?.Photo?.length!! > 0) {
                    viewHolder.relative_admin_image?.setVisibility(View.VISIBLE)
                    viewHolder.img_chat_admin?.getLayoutParams()?.width = (act?.getResources()?.getDrawable(
                            R.drawable.cat_no_img) as BitmapDrawable).bitmap.width + (act?.getResources()?.getDrawable(
                            R.drawable.cat_no_img) as BitmapDrawable).bitmap.width / 2
                    viewHolder.img_chat_admin?.getLayoutParams()?.height = (act?.getResources()?.getDrawable(
                            R.drawable.cat_no_img) as BitmapDrawable).bitmap.height
                    Picasso.with(act).load(itemsData.get(position)?.Photo).placeholder(R.drawable.cat_no_img)
                            .transform(RoundedCornersTransform(40, 0))
                            .error(R.drawable.cat_no_img).fit()
                            .centerCrop().into(viewHolder.img_chat_admin)
                    viewHolder.img_chat_admin?.setOnClickListener(View.OnClickListener {
                        val productImages: Array<String?>
                        productImages = arrayOfNulls<String?>(1)
                        productImages[0] = itemsData.get(position)?.Photo
                        val b = Bundle()
                        b.putInt("position", 0)
                        b.putStringArray("productImages", productImages)
                        ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                    })
                }
                if (itemsData.get(position)?.MessageContent?.length!! > 0) {
                    viewHolder.relative_admin_text?.setVisibility(View.VISIBLE)
                    viewHolder.tv_message_admin?.setText(itemsData.get(position)?.MessageContent)
                    Linkify.addLinks(viewHolder.tv_message_admin!!, Linkify.ALL)
                }
                if (position > 0) {
                    if (itemsData.get(position - 1) != null) {
                        if (itemsData.get(position - 1)?.UserId != null &&
                                itemsData.get(position)?.UserId != null) {
                            if (itemsData.get(position - 1)?.UserId.equals(itemsData.get(position)?.UserId, ignoreCase = true)) {
                                viewHolder.tv_message_admin?.setText(itemsData.get(position)?.MessageContent)
                                Linkify.addLinks(viewHolder.tv_message_admin!!, Linkify.ALL)
                            }
                        }
                    }
                }
            }
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var relative_parent: RelativeLayout?
        var relative_admin_image: RelativeLayout?
        var relative_admin_text: RelativeLayout?
        var relative_user_image: RelativeLayout?
        var relative_user_text: RelativeLayout?
        var tv_message_admin: TextView?
        var tv_message_user: TextView?
        var linear_admin: LinearLayout?
        var linear_user: LinearLayout?
        var img_profile: CircleImageView?
        var img_profile1: CircleImageView?
        var img_user_profile: CircleImageView?
        var img_user_profile1: CircleImageView?
        var img_chat_admin: ImageView?
        var img_chat_user: ImageView?

        init {
            relative_user_image = itemLayoutView.findViewById<View?>(R.id.relative_user_image) as RelativeLayout?
            relative_user_text = itemLayoutView.findViewById<View?>(R.id.relative_user_text) as RelativeLayout?
            tv_message_user = itemLayoutView.findViewById<View?>(R.id.tv_message_user) as TextView?
            linear_user = itemLayoutView.findViewById<View?>(R.id.linear_user) as LinearLayout?
            img_chat_user = itemLayoutView.findViewById<View?>(R.id.img_chat_user) as ImageView?
            img_user_profile = itemLayoutView.findViewById<View?>(R.id.img_user_profile) as CircleImageView?
            img_user_profile1 = itemLayoutView.findViewById<View?>(R.id.img_user_profile1) as CircleImageView?
            relative_admin_image = itemLayoutView.findViewById<View?>(R.id.relative_admin_image) as RelativeLayout?
            relative_admin_text = itemLayoutView.findViewById<View?>(R.id.relative_admin_text) as RelativeLayout?
            tv_message_admin = itemLayoutView.findViewById<View?>(R.id.tv_message_admin) as TextView?
            linear_admin = itemLayoutView.findViewById<View?>(R.id.linear_admin) as LinearLayout?
            img_chat_admin = itemLayoutView.findViewById<View?>(R.id.img_chat_admin) as ImageView?
            img_profile = itemLayoutView.findViewById<View?>(R.id.img_profile) as CircleImageView?
            img_profile1 = itemLayoutView.findViewById<View?>(R.id.img_profile1) as CircleImageView?
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout?
            ContentActivity.Companion.setTextFonts(relative_parent!!)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData?.size!!
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
        sessionManager = SessionManager(act!!)
    }
}