package com.qenaat.app.model

/**
 * Created by DELL on 15-Nov-17.
 */
class GetRequests {
    var Id: String? = null
    var Title: String? = null
    var Decription: String? = null
    var Amount: String? = null
    var Status: String? = null
    var UserId: String? = null
    var UserName: String? = null
    var photos: ArrayList<Photos?>? = null

    inner class Photos {
        var Id: String? = null
        var attachment: String? = null
    }
}