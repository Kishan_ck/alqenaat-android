package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.CompanyAdapter
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetCompanies
import com.qenaat.app.model.GetCompanyCategories
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 16-Nov-17.
 */
class CompanyFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var tabNumber = 0
    var getCompaniesArrayList: ArrayList<GetCompanies?> = ArrayList()
    lateinit var getCompanyCategories: GetCompanyCategories
    var comingFrom: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments?.containsKey("GetCompanyCategories")!!) {
                    val gson = Gson()
                    getCompanyCategories = gson.fromJson(arguments!!.getString("GetCompanyCategories"),
                            GetCompanyCategories::class.java)
                }
                if (arguments!!.containsKey("comingFrom")) {
                    comingFrom = arguments!!.getString("comingFrom")!!
                }
                if (arguments!!.containsKey("tabNumber")) {
                    tabNumber = arguments!!.getInt("tabNumber", 0)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById<View?>(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())

    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.CompaniesLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            if (mSessionManager!!.isLoggedin()
                    && mSessionManager!!.getUserCode() !== "" && mSessionManager!!.getUserCode() != null) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                bundle.putString("isEdit", "0")
                bundle.putString("contentType", "3")
                ContentActivity.Companion.openAddNewsFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act?.getString(R.string.LoggedIn)!!, Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
        })
        if (getCompaniesArrayList.size > 0) {
            mAdapter = CompanyAdapter(act, getCompaniesArrayList, comingFrom)
            my_recycler_view.setAdapter(mAdapter)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)


        } else {
            GetCompanies()
            if (comingFrom.equals("my account", ignoreCase = true)){
                ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            }
            else{
                ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            }

        }
    }

    fun GetCompanies() {

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetCompanies(
                if (comingFrom.equals("my account", ignoreCase = true)) "$AUTH_TEXT ${mSessionManager?.getAuthToken()}" else "",
                "0",
                if (comingFrom.equals("my account", ignoreCase = true)) mSessionManager!!.getUserCode() else "0",
                getCompanyCategories.Id, "" + page_index,
                if (tabNumber == 1) "true" else "false")?.enqueue(
                object : Callback<ArrayList<GetCompanies?>?> {

                    override fun onFailure(call: Call<ArrayList<GetCompanies?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetCompanies?>?>, response: Response<ArrayList<GetCompanies?>?>) {
                        if (response.body() != null) {
                            val getCompanies = response.body()
                            if (mloading != null && getCompanies != null) {
                                Log.d("getContentses size", "" + getCompanies.size)
                                getCompaniesArrayList.clear()
                                getCompaniesArrayList.addAll(getCompanies)
                                if (getCompanies.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = CompanyAdapter(act, getCompaniesArrayList, comingFrom)
                                my_recycler_view.setAdapter(mAdapter)
                                my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        if (!endOfREsults) {
                                            visibleItemCount = mLayoutManager.getChildCount()
                                            totalItemCount = mLayoutManager.getItemCount()
                                            pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                                            if (loading_flag) {
                                                if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                    if (getCompaniesArrayList.size != 0) {
                                                        progress_loading_more.setVisibility(View.VISIBLE)
                                                        //GlobalFunctions.DisableLayout(mainLayout);
                                                    }
                                                    loading_flag = false
                                                    page_index = page_index + 1
                                                    GetMoreCompanies()
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    fun GetMoreCompanies() {
        QenaatAPICall.getCallingAPIInterface()?.GetCompanies(
                if (comingFrom.equals("my account", ignoreCase = true)) "$AUTH_TEXT ${mSessionManager?.getAuthToken()}" else "",
                "0",
                if (comingFrom.equals("my account", ignoreCase = true)) mSessionManager?.getUserCode() else "0",
                getCompanyCategories.Id, "" + page_index,
                if (tabNumber == 1) "true" else "false")?.enqueue(
                object : Callback<ArrayList<GetCompanies?>?> {

                    override fun onFailure(call: Call<ArrayList<GetCompanies?>?>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetCompanies?>?>, response: Response<ArrayList<GetCompanies?>?>) {
                        if (response.body() != null) {
                            val getCompanies = response.body()
                            if (progress_loading_more != null) {
                                progress_loading_more.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                if (getCompanies != null) {
                                    if (getCompanies.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    getCompaniesArrayList.addAll(getCompanies)
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                })
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        protected val TAG = CompanyFragment::class.java.simpleName
        var mSessionManager: SessionManager? = null
        var languageSeassionManager: LanguageSessionManager? = null
        var act: FragmentActivity? = null
        lateinit var fragment: CompanyFragment
        fun newInstance(act: FragmentActivity?): CompanyFragment {
            fragment = CompanyFragment()
            Companion.act = act
            return fragment
        }
    }
}