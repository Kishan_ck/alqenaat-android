package com.qenaat.app.classes

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by webuser1 on 6/14/2015.
 */
class LanguageSessionManager @SuppressLint("CommitPrefEdits") constructor(var _context: Context?) {
    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    var PRIVATE_MODE = 0
    fun getLang(): String? {
        return pref.getString(KEY_Lang, "ar")
    }

    fun setLang(lang: String?) {
        editor.putString(KEY_Lang, lang)
        editor.commit()
    }

    fun getRegId(): String? {
        return pref.getString(KEY_RegID, "")
    }

    fun setRegId(id: String?) {
        editor.putString(KEY_RegID, id)
        editor.commit()
    }

    fun isRegIdToken(): Boolean {
        return pref.getBoolean(IS_Registered, false)
    }

    fun setNotificationStatus(status: Boolean) {
        // Storing notification value as TRUE
        editor.putBoolean(KEY_NotificationStatus, status)
        editor.commit()
    }

    fun getNotificationStatus(): Boolean {
        // Get Notification Status
        return pref.getBoolean(KEY_NotificationStatus, false)
    }

    fun getInstagram(): String? {
        return pref.getString(ConstanstParameters.key_Instagram, null)
    }

    fun setInstagram(instagram: String?) {
        editor.putString(ConstanstParameters.key_Instagram, instagram)
        editor.commit()
    }

    fun SetFaceBook(facebook: String?) {
        editor.putString(ConstanstParameters.key_FaceBook, facebook)
        editor.commit()
    }

    fun GetFaceBook(): String? {
        return pref.getString(ConstanstParameters.key_FaceBook, null)
    }

    fun SetTwitter(twitter: String?) {
        editor.putString(ConstanstParameters.key_Twitter, twitter)
        editor.commit()
    }

    fun GetTwitter(): String? {
        return pref.getString(ConstanstParameters.key_Twitter, null)
    }

    fun SetYoutube(youtube: String?) {
        editor.putString(ConstanstParameters.key_YouTube, youtube)
        editor.commit()
    }

    fun GetYoutube(): String? {
        return pref.getString(ConstanstParameters.key_YouTube, null)
    }

    fun Setkey_SnapChat(snapChat: String?) {
        editor.putString(ConstanstParameters.key_SnapChat, snapChat)
        editor.commit()
    }

    fun Getkey_SnapChat(): String? {
        return pref.getString(ConstanstParameters.key_SnapChat, null)
    }

    companion object {
        private val PREF_NAME: String? = "com.ais.pref.lang"
        val KEY_Lang: String? = "KEY_Lang"
        val KEY_RegID: String? = "regId"
        val KEY_NotificationStatus: String? = "NotificationStatus"
        private val IS_Registered: String? = "IsRegistered"
    }

    init {
        pref = _context?.getSharedPreferences(PREF_NAME, PRIVATE_MODE)!!
        editor = pref.edit()
    }
}