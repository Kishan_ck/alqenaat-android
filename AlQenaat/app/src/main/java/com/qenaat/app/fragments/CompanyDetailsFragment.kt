package com.qenaat.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.showToast
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.GlobalFunctions.CALL_PERMISSION_CODE
import com.qenaat.app.classes.GlobalFunctions.LOCATION_PERMISSION_CODE
import com.qenaat.app.classes.GlobalFunctions.redirectAppPermission
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetCompanies
import com.qenaat.app.model.GetNews
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by DELL on 16-Nov-17.
 */
class CompanyDetailsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var relative_top: RelativeLayout
    lateinit var img_event: ImageView
    lateinit var img_reminder: ImageView
    lateinit var img_fav: ImageView
    lateinit var img_share: ImageView
    lateinit var img_share1: ImageView
    lateinit var img_report: ImageView
    lateinit var img_facebook: ImageView
    lateinit var img_instagram: ImageView
    lateinit var img_twitter: ImageView
    lateinit var img_youtube: ImageView
    lateinit var img_location: ImageView
    lateinit var img_call: ImageView
    lateinit var img_calender: ImageView
    lateinit var linear_date: LinearLayout
    lateinit var linear_bottom: LinearLayout
    lateinit var linear_social: ConstraintLayout
    lateinit var tv_days: TextView
    lateinit var tv_views: TextView
    lateinit var tv_occasion_details: TextView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var getCompanies: GetCompanies
    lateinit var productImages: Array<String?>
    lateinit var pShareUri: Uri
    lateinit var file: File
    lateinit var relative_desc: RelativeLayout
    lateinit var tv_tree: TextView
    var loadingFinished = true
    var redirect = false
    lateinit var webView: WebView
    lateinit var img_count: TextView
    lateinit var img_expire: ImageView
    lateinit var relative_gallery: RelativeLayout
    lateinit var ll_location1: LinearLayout
    lateinit var ll_phone1: LinearLayout
    var permissionStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments?.containsKey("GetCompanies")!!) {
                    val gson = Gson()
                    getCompanies = gson.fromJson(arguments?.getString("GetCompanies"),
                            GetCompanies::class.java)
                }
            }
        } catch (e: Exception) {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.event_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            relative_gallery = mainLayout.findViewById(R.id.relative_gallery) as RelativeLayout
            img_expire = mainLayout.findViewById(R.id.img_expire) as ImageView
            img_count = mainLayout.findViewById(R.id.img_count) as TextView
            webView = mainLayout.findViewById(R.id.webView) as WebView
            tv_tree = mainLayout.findViewById(R.id.tv_tree) as TextView
            relative_desc = mainLayout.findViewById(R.id.relative_desc) as RelativeLayout
            img_reminder = mainLayout.findViewById(R.id.img_reminder) as ImageView
            img_fav = mainLayout.findViewById(R.id.img_fav) as ImageView
            img_share = mainLayout.findViewById(R.id.img_share) as ImageView
            img_share1 = mainLayout.findViewById(R.id.img_share1) as ImageView
            img_report = mainLayout.findViewById(R.id.img_report) as ImageView
            img_facebook = mainLayout.findViewById(R.id.img_facebook) as ImageView
            img_instagram = mainLayout.findViewById(R.id.img_instagram) as ImageView
            img_twitter = mainLayout.findViewById(R.id.img_twitter) as ImageView
            img_youtube = mainLayout.findViewById(R.id.img_youtube) as ImageView
            img_location = mainLayout.findViewById(R.id.img_location) as ImageView
            img_call = mainLayout.findViewById(R.id.img_call) as ImageView
            img_calender = mainLayout.findViewById(R.id.img_calender) as ImageView
            img_event = mainLayout.findViewById(R.id.img_event) as ImageView
            tv_views = mainLayout.findViewById(R.id.tv_views) as TextView
            tv_occasion_details = mainLayout.findViewById(R.id.tv_occasion_details) as TextView
            tv_days = mainLayout.findViewById(R.id.tv_days) as TextView
            linear_date = mainLayout.findViewById(R.id.linear_date) as LinearLayout
            linear_bottom = mainLayout.findViewById(R.id.linear_bottom) as LinearLayout
            linear_social = mainLayout.findViewById(R.id.linear_social) as ConstraintLayout
            relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading1) as ProgressBar
            ll_location1 = mainLayout.findViewById(R.id.ll_location1) as LinearLayout
            ll_phone1 = mainLayout.findViewById(R.id.ll_phone1) as LinearLayout
            img_calender.setOnClickListener(this)
            img_call.setOnClickListener(this)
            img_location.setOnClickListener(this)
            img_youtube.setOnClickListener(this)
            img_twitter.setOnClickListener(this)
            img_instagram.setOnClickListener(this)
            img_facebook.setOnClickListener(this)
            img_report.setOnClickListener(this)
            img_share.setOnClickListener(this)
            img_share1.setOnClickListener(this)
            img_fav.setOnClickListener(this)
            img_reminder.setOnClickListener(this)
            img_event.setOnClickListener(this)
            ll_location1.setOnClickListener(this)
            ll_phone1.setOnClickListener(this)
            tv_views.setTypeface(ContentActivity.Companion.tf)
            tv_tree.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_occasion_details.setTypeface(ContentActivity.Companion.tf)
            tv_days.setTypeface(ContentActivity.Companion.tf)
            img_reminder.setVisibility(View.VISIBLE)
            img_calender.setVisibility(View.GONE)
            img_call.setImageResource(R.drawable.call_big)
            img_location.setImageResource(R.drawable.location_big)
            tv_days.setVisibility(View.VISIBLE)
            linear_bottom.setVisibility(View.VISIBLE)
            img_reminder.setVisibility(View.INVISIBLE)
            tv_views.setVisibility(View.INVISIBLE)
            tv_tree.setVisibility(View.INVISIBLE)
            tv_days.setVisibility(View.INVISIBLE)
        }
    }

    override fun onStart() {
        super.onStart()
        //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.DetailsLabel).replace("*", ""))
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        GetContents()
        //setData()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.img_reminder -> {
            }
            R.id.img_event -> {
                /*if (getCompanies.companyPhotos?.size!! > 0) {
                    productImages = arrayOfNulls<String?>(getCompanies.companyPhotos?.size!!)
                    var i = 0
                    while (i < getCompanies.companyPhotos?.size!!) {
                        val photo: GetCompanies.CompanyPhotos = getCompanies.companyPhotos?.get(i)!!
                        productImages[i] = photo.Photo
                        i++
                    }
                } else {*/
                if (getCompanies.CompanyLogo != null && getCompanies.CompanyLogo != "") {
                    productImages = arrayOfNulls<String?>(1)
                    productImages[0] = getCompanies.CompanyLogo
                    //}
                    val b = Bundle()
                    b.putInt("position", 0)
                    b.putStringArray("productImages", productImages)
                    ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                }
            }
            R.id.img_fav -> {
            }
            R.id.img_share -> shareContent()
            R.id.img_share1 -> shareContent()
            R.id.img_report -> {
            }
            R.id.img_facebook -> if (getCompanies.Facebook?.isNotEmpty()!!) {
                if (getCompanies.Facebook!!.contains("http") || getCompanies.Facebook!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getCompanies.Facebook)))
                else
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getCompanies.Facebook}")))
            }
            R.id.img_instagram -> if (getCompanies.Instagram!!.isNotEmpty()) {
                if (getCompanies.Instagram!!.contains("http") || getCompanies.Instagram!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getCompanies.Instagram)))
                else
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getCompanies.Instagram}")))
            }
            R.id.img_twitter -> if (getCompanies.Twitter?.isNotEmpty()!!) {
                if (getCompanies.Twitter!!.contains("http") || getCompanies.Twitter!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getCompanies.Twitter)))
                else
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getCompanies.Twitter}")))
            }
            R.id.img_youtube -> if (getCompanies.YouTube?.isNotEmpty()!!) {
                if (getCompanies.YouTube!!.contains("http") || getCompanies.YouTube!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getCompanies.YouTube)))
                else
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getCompanies.YouTube}")))
            }
            R.id.ll_location1 -> {
                /* if (GlobalFunctions.isGPSAllowed(act)) {
                     if (getCompanies.Latitude!!.isNotEmpty() && getCompanies.Longitude!!.length > 0) {
                         val news = GetNews()
                         news.Latitude = getCompanies.Latitude
                         news.Longitude = getCompanies.Longitude
                         val gson = Gson()
                         val bundle = Bundle()
                         val contentsArrayList: ArrayList<GetNews?> = ArrayList<GetNews?>()
                         contentsArrayList.add(news)
                         bundle.putString("GetNews", gson.toJson(contentsArrayList))
                         ContentActivity.Companion.openShowMapFragment(bundle)
                     }
                     return
                 }*/
                if (getCompanies.Latitude!!.isNotEmpty() && getCompanies.Longitude!!.isNotEmpty()) {
                    permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                    GlobalFunctions.requestGPSPermission(act)
                } else
                    showToast(act, getString(R.string.no_location_found))
            }
            R.id.ll_phone1 -> {
                /* if (GlobalFunctions.isReadCallAllowed(act)) {
                     if (getCompanies.Telephone!!.length > 0) {
                         val intent = Intent(Intent.ACTION_CALL)
                         intent.data = Uri.parse("tel:" + getCompanies.Telephone)
                         act.startActivity(intent)
                     }
                     return
                 }*/
                if (getCompanies.Telephone!!.isNotEmpty()) {
                    permissionStr = Manifest.permission.CALL_PHONE
                    GlobalFunctions.requestCallPermission(act)
                } else
                    showToast(act, getString(R.string.no_number_found))
            }
            R.id.img_calender -> {
            }
        }
    }

    private fun setData() {
        img_expire.setVisibility(View.GONE)

//        img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                R.drawable.no_img_details)).getBitmap().getWidth();
        img_event.getLayoutParams().height = (act.getResources().getDrawable(
                R.drawable.no_img_details) as BitmapDrawable).getBitmap().getHeight()
        /* if (getCompanies.companyPhotos!!.size > 0) {
             Picasso.with(act).load(getCompanies.companyPhotos!!.get(0)!!.Photo).placeholder(R.drawable.no_img_details)
                     .error(R.drawable.no_img_details).into(img_event)
         }*/
        if (getCompanies.CompanyLogo != null && getCompanies.CompanyLogo != "") {
            Picasso.with(act).load(getCompanies.CompanyLogo).placeholder(R.drawable.no_img_details)
                    .error(R.drawable.no_img_details).into(img_event)
        }
        relative_gallery.setVisibility(View.INVISIBLE)
        //                        if(getCompanies.getContentPhotos().size()>=2){
//                            relative_gallery.setVisibility(View.VISIBLE);
//                            img_count.setText(getCompanies.getContentPhotos().size()+"");
//                        }

        //tv_days.setText(getCompanies.getFullName());
        tv_views.setVisibility(View.INVISIBLE)
        if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
            setContent("", getCompanies.TitleEN + "\n\n" + getCompanies.DetailsEN)
            tv_tree.setText(getCompanies.CompanyCategoryNameEn)
            tv_tree.setVisibility(View.VISIBLE)
            tv_days.setVisibility(View.INVISIBLE)
        } else {
            setContent("", getCompanies.TitleAR + "\n\n" + getCompanies.DetailsAR)
            tv_tree.setText(getCompanies.CompanyCategoryNameAr)
            tv_tree.setVisibility(View.VISIBLE)
            tv_days.setVisibility(View.INVISIBLE)
        }
        if (getCompanies.Telephone != null) if (getCompanies.Telephone!!.length > 0) {
        } else {
            img_call.setAlpha(50)
        }
        if (getCompanies.Latitude != null && getCompanies.Longitude != null) if (getCompanies.Latitude!!.length > 0 && getCompanies.Longitude!!.length > 0) {
        } else {
            img_location.setAlpha(50)
        }
        if (getCompanies.Facebook != null) if (getCompanies.Facebook!!.length > 0) {
        } else {
            img_facebook.setAlpha(50)
        }
        if (getCompanies.Twitter != null) if (getCompanies.Twitter!!.length > 0) {
        } else {
            img_twitter.setAlpha(50)
        }
        if (getCompanies.Instagram != null) if (getCompanies.Instagram!!.length > 0) {
        } else {
            img_instagram.setAlpha(50)
        }
        if (getCompanies.YouTube != null) if (getCompanies.YouTube!!.length > 0) {
        } else {
            img_youtube.setAlpha(50)
        }
    }

    fun GetContents() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetCompanies(
                "", getCompanies.Id, "0",
                "0", "0", getCompanies.ForFamily)?.enqueue(
                object : Callback<ArrayList<GetCompanies?>?> {

                    override fun onFailure(call: Call<ArrayList<GetCompanies?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetCompanies?>?>, response: Response<ArrayList<GetCompanies?>?>) {
                        if (response.body() != null) {
                            val getEvents = response.body()
                            if (mloading != null) {
                                if (getEvents != null) {
                                    if (getEvents.size > 0) {
                                        getCompanies = getEvents[0]!!
                                        setData()
                                    }
                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    // shareType : 1-fb , 5-twitter , 6-Instgram  , 2-wtsapp  , 3-gmail  , 4sms
    @SuppressLint("DefaultLocale")
    private fun Share(shareType: Int, appName: String?, content: String?) {
        val targetedShareIntents: MutableList<Intent?> = ArrayList()
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        val resInfo: MutableList<ResolveInfo?> = act.getPackageManager().queryIntentActivities(share, 0)
        if (!resInfo.isEmpty()) {
            when (shareType) {
                1 -> {
                    Log.d("inside fbshare", "test0001")
                    // facebook
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info!!.activityInfo.packageName.toLowerCase().contains(appName.toString()) || info.activityInfo.name.toLowerCase().contains(appName.toString())) {
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                                //targetedShareIntents.toArray<Parcelable?>(arrayOf<Parcelable?>()))
                                targetedShareIntents.toTypedArray())
                        act.startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Facebook is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                5 -> {
                    // twitter
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info?.activityInfo?.packageName?.toLowerCase()?.contains(appName.toString())!! || info.activityInfo.name.toLowerCase().contains(appName.toString())) {
                            Log.e("Twitter", "Twitter-Data: $content")
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)

//                            Uri uri = Uri.parse(products.getPhotos());
//                            targetedShare.putExtra(Intent.EXTRA_STREAM, String.valueOf(uri));
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Twitter is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                6 ->                     //Instagram
                    if (1 == 1) {
                        try {
                            Log.d("product_1_image", getCompanies.CompanyLogo)
                            val shareIntent = Intent(Intent.ACTION_SEND)
                            shareIntent.type = "image/*"
                            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            shareIntent.putExtra(Intent.EXTRA_STREAM, pShareUri)
                            shareIntent.putExtra(Intent.EXTRA_TEXT, content)
                            shareIntent.setPackage("com.instagram.android")
                            targetedShareIntents.add(shareIntent)
                            val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                            startActivity(chooserIntent)
                        } catch (e: Exception) {
                            Toast.makeText(act, "Instagram is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                        }
                    }
                2 -> {
                    // whatsapp
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        // put here your mime type
                        if (info!!.activityInfo.packageName.toLowerCase().contains(
                                        appName.toString())
                                || info.activityInfo.name.toLowerCase().contains(
                                        appName.toString())) {
                            Log.d("whatsapp_share", content)
                            targetedShare.putExtra(Intent.EXTRA_TEXT, """
     $content

     ${getCompanies.CompanyLogo}
     """.trimIndent())
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "WhatsApp is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                3 -> {
                    // Gmail
                    val txt = "<img src=\"" + getCompanies.CompanyLogo + "\" width=\"600px\"  height=\"600px\"/>"
                    Log.e("Email", "Email txt = $txt")
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/html"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, act.getString(R.string.app_name))
                    shareIntent.putExtra(Intent.EXTRA_TEXT, """$content
 ${Html.fromHtml(txt)}""")
                    for (app in resInfo) {
                        if (app!!.activityInfo.name.toLowerCase().contains(appName.toString())) {
                            val activity: ActivityInfo = app.activityInfo
                            val name = ComponentName(activity.applicationInfo.packageName, activity.name)
                            shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                            shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                            shareIntent.component = name
                            act.startActivity(shareIntent)
                            break
                        }
                    }
                }
                4 -> {
                    //SMS
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("smsto:")
                    smsIntent.type = "vnd.android-dir/mms-sms"
                    smsIntent.putExtra(Intent.EXTRA_SUBJECT, act.getString(R.string.app_name) + "\n" + content)
                    smsIntent.putExtra("sms_body", content)
                    try {
                        startActivity(smsIntent)
                        Log.i("Finished sending SMS...", "")
                    } catch (ex: ActivityNotFoundException) {
                    }
                }
                else -> {
                }
            }
        }
    }

    internal inner class GetAndSaveBitmapForArticle : AsyncTask<String?, Void?, Uri?>() {
        protected override fun doInBackground(vararg params: String?): Uri? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            file = File(dir, UUID.randomUUID().toString() + ".png")
            val fOut: FileOutputStream
            try {
                val bm: Bitmap = Picasso.with(act).load(params[0]).get()
                fOut = FileOutputStream(file)
                bm.compress(Bitmap.CompressFormat.PNG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            pShareUri = FileProvider.getUriForFile(act,
                    act.applicationContext?.packageName + ".fileprovider", file)
            return pShareUri
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                CALL_PERMISSION_CODE -> {
                    if (getCompanies.Telephone!!.isNotEmpty()) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.data = Uri.parse("tel:" + getCompanies.Telephone)
                        act.startActivity(intent!!)
                    }
                }
                LOCATION_PERMISSION_CODE -> {
                    if (getCompanies.Latitude!!.isNotEmpty() && getCompanies.Longitude!!.isNotEmpty()) {
                        val news = GetNews()
                        news.Latitude = getCompanies.Latitude
                        news.Longitude = getCompanies.Longitude
                        val gson = Gson()
                        val bundle = Bundle()
                        val contentsArrayList: ArrayList<GetNews?> = ArrayList<GetNews?>()
                        contentsArrayList.add(news)
                        bundle.putString("GetNews", gson.toJson(contentsArrayList))
                        ContentActivity.Companion.openShowMapFragment(bundle)
                    }
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                redirectAppPermission(act)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun shareContent() {

//        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
//               getCompanies.getCompanyCategoryNameEn()+  " @ AlQenaat " + " \n " + getCompanies.getDetailsEN() + "\n\n" + getCompanies.getPhotos() + "\n\n" + act.getString(R.string.UrlLink)
//                : getCompanies.getCompanyCategoryNameAr() +  " @ AlQenaat" + " \n " + getCompanies.getDetailsAR() + "\n\n" + getCompanies.getPhotos() + "\n\n" + act.getString(R.string.UrlLink);
        val shareProductInfo: String = if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) getCompanies.TitleEN + "" + "\n\n" + getCompanies.DetailsEN + "" + "\n\n" + act.getString(R.string.UrlLink1) else getCompanies.TitleAR + "" + "\n\n" + getCompanies.DetailsAR + "" + "\n\n" + act.getString(R.string.UrlLink1)


/*        String contentType="";
        if(getCompanies.getContentType().equalsIgnoreCase("1")){
            contentType = act.getString(R.string.NewsLabel);
        }
        else if(getCompanies.getContentType().equalsIgnoreCase("2")){
            if(getCompanies.getIsActivity().equalsIgnoreCase("1")){
                contentType = act.getString(R.string.SpecialOccasionLabel);
            }
            else{
                contentType = act.getString(R.string.OccasionLabel);
            }

        }
        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
               act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getCompanies.getContentLink() + "\n\n" + act.getString(R.string.UrlLink)
                : act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getCompanies.getContentLink() + "\n\n" + act.getString(R.string.UrlLink);
**/
        val ob: GetAndSaveBitmapForArticle = GetAndSaveBitmapForArticle()
        val clipboard = act.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("AlQenaat", shareProductInfo)
        clipboard.setPrimaryClip(clip)
        ob.execute(getCompanies.CompanyLogo)
        val dialog = Dialog(act)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.share_dialog)
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        val mimg_close: ImageView
        val mimg_facebook: ImageView
        val mimg_twitter: ImageView
        val mimg_instagram: ImageView
        val mimg_email: ImageView
        val mimg_sms: ImageView
        val mimg_whats: ImageView
        mimg_facebook = dialog.findViewById<View?>(R.id.img_facebook) as ImageView
        val title: TextView = dialog.findViewById<View?>(R.id.shartext) as TextView
        title.setTypeface(ContentActivity.Companion.tf)
        mimg_twitter = dialog.findViewById<View?>(R.id.img_twitter) as ImageView
        mimg_instagram = dialog.findViewById<View?>(R.id.img_instagram) as ImageView
        mimg_email = dialog.findViewById<View?>(R.id.img_email) as ImageView
        mimg_sms = dialog.findViewById<View?>(R.id.img_sms) as ImageView
        mimg_whats = dialog.findViewById<View?>(R.id.img_whats) as ImageView
        mimg_close = dialog.findViewById<View?>(R.id.img_searchClose) as ImageView
        mimg_close.setOnClickListener(View.OnClickListener {
            file.delete()
            dialog.dismiss()
        })
        mimg_facebook.setOnClickListener(View.OnClickListener { /*Share(1,
                                "com.facebook.katana",
                                products.getDecription() + "\n" +
                                        products.getPhotos());*/
            dialog.dismiss()
            if (mSessionManager.isFacebookOpenBefore()) {
                Share(1,
                        "com.facebook.katana",
                        shareProductInfo)
            } else {
                AlertDialog.Builder(act)
                        .setTitle(act.getString(R.string.AttentionLabel))
                        .setMessage(act.getString(R.string.AttentionText))
                        .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {
                                Share(1,
                                        "com.facebook.katana",
                                        shareProductInfo)
                                mSessionManager.FacebookOpened()
                            }
                        })
                        .setIcon(R.drawable.icon_512)
                        .show()
            }
        })
        mimg_twitter.setOnClickListener(View.OnClickListener {
            file.delete()
            dialog.dismiss()
            Share(5,
                    "twitter",
                    shareProductInfo)
        })
        mimg_instagram.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            AlertDialog.Builder(act)
                    .setIcon(R.drawable.icon_512)
                    .setTitle(act.getString(R.string.AttentionLabel))
                    .setMessage(act.getString(R.string.AttentionText))
                    .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            Share(6,
                                    "instagram",
                                    shareProductInfo)
                        }
                    })
                    .setIcon(R.drawable.icon_512)
                    .show()
        })
        mimg_email.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            file.delete()
            Share(3,
                    "gmail",
                    shareProductInfo)
        })
        mimg_sms.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            file.delete()
            Share(4,
                    "sms",
                    shareProductInfo)
        })
        mimg_whats.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            file.delete()
            Share(2,
                    "whatsapp",
                    shareProductInfo)
        })
        dialog.show()
    }

    fun setContent(title: String?, content: String?) {
        val sp: Spannable = SpannableString(title)
        Linkify.addLinks(sp, Linkify.ALL)
        val sp1: Spannable = SpannableString(content)
        Linkify.addLinks(sp1, Linkify.ALL)
        var finalHTML = ""
        finalHTML = if (title!!.length > 0) {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center><center>" + Html.toHtml(sp)
                    + "</center></b></span style='color: #4c4c4c;'><br /><br /><center>" + Html.toHtml(sp1) + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        } else {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center>" + Html.toHtml(sp)
                    + "</center></span style='color: #4c4c4c;'><center>" + Html.toHtml(sp1) + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        }
        webView.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
        webView.setBackgroundColor(0)
        webView.setWebViewClient(WebViewClient())
        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true)
        webView.getSettings().setPluginState(WebSettings.PluginState.ON)
        webView.setWebChromeClient(WebChromeClient())
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, urlNewString: String?): Boolean {
                if (!loadingFinished) {
                    redirect = true
                }
                loadingFinished = false
                //webView.loadUrl(urlNewString);
                // Here the String url hold 'Clicked URL'
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlNewString)));
                if (urlNewString!!.startsWith("tel:")) {
                    val intent = Intent(Intent.ACTION_DIAL,
                            Uri.parse(urlNewString))
                    startActivity(intent)
                } else if (urlNewString.startsWith("http:") || urlNewString.startsWith("https:")) {
                    view!!.loadUrl(urlNewString)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, facIcon: Bitmap?) {
                loadingFinished = false
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                mloading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (!redirect) {
                    loadingFinished = true
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
                if (loadingFinished && !redirect) {
                    //HIDE LOADING IT HAS FINISHED
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                } else {
                    redirect = false
                }
            }
        })
    }

    companion object {
        protected val TAG = CompanyDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: CompanyDetailsFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): CompanyDetailsFragment {
            fragment = CompanyDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}