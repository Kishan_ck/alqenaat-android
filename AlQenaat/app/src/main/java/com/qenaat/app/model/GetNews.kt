package com.qenaat.app.model

/**
 * Created by DELL on 31-Oct-17.
 */
class GetNews {
    var Id: String? = null
    var NewsCategoryEn: String? = null
    var NewsCategoryAr: String? = null
    var TitleEN: String? = null
    var TitleAR: String? = null
    var HeadLineEN: String? = null
    var HeadLineAR: String? = null
    var DetailsEN: String? = null
    var DetailsAR: String? = null
    var Photo: String? = null
    var Color: String? = null
    var Facebook: String? = null
    var Instagram: String? = null
    var Twitter: String? = null
    var YouTube: String? = null
    var Latitude: String? = null
    var Longitude: String? = null
    var Phone: String? = null
    var ViewersCount: String? = null
    var AddedOn: String? = null
}