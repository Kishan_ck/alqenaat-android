package com.qenaat.app.fragments

import android.Manifest
import android.app.AlarmManager
import android.app.Dialog
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.*
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.SocialMediaShare.imgUri
import com.qenaat.app.classes.SocialMediaShare.shareContext
import com.qenaat.app.classes.SocialMediaShare.shareData
import com.qenaat.app.fragments.DeewaniyaDetailsFragment
import com.qenaat.app.model.GetAppSettings
import com.qenaat.app.model.GetEvents
import com.qenaat.app.model.GetNews
import com.qenaat.app.networking.QenaatAPICall
import com.qenaat.app.service.NotifyUserService
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 09-Nov-17.
 */
class MonasbatDetailsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var relative_top: RelativeLayout
    lateinit var img_event: ImageView
    lateinit var img_reminder: ImageView
    lateinit var img_fav: ImageView
    lateinit var img_share: ImageView
    lateinit var img_share1: ImageView
    lateinit var img_report: ImageView
    lateinit var img_facebook: ImageView
    lateinit var img_instagram: ImageView
    lateinit var img_twitter: ImageView
    lateinit var img_youtube: ImageView
    lateinit var img_location: ImageView
    lateinit var img_call: ImageView
    lateinit var img_calender: ImageView
    lateinit var linear_date: LinearLayout
    lateinit var linear_bottom: LinearLayout
    lateinit var linear_social: ConstraintLayout
    lateinit var tv_days: TextView
    lateinit var tv_views: TextView
    lateinit var tv_occasion_details: TextView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var events: GetEvents
    lateinit var productImages: Array<String?>
    private val CALL_PHONE_PERMISSION_CODE = 23
    lateinit var pShareUri: Uri
    var file: File? = null
    lateinit var relative_desc: RelativeLayout
    lateinit var tv_tree: TextView
    var loadingFinished = true
    var redirect = false
    lateinit var webView: WebView
    lateinit var img_count: TextView
    lateinit var img_expire: ImageView
    lateinit var relative_gallery: RelativeLayout
    lateinit var appSettings: GetAppSettings
    private var catId = "0"
    var permissionStr = ""
    lateinit var ll_location1: LinearLayout
    lateinit var ll_phone1: LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetEvents.Events")) {
                    val gson = Gson()
                    events = gson.fromJson(
                        arguments!!.getString("GetEvents.Events"),
                        GetEvents::class.java
                    )
                    catId = arguments!!.getString("catId")!!
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            mainLayout = inflater.inflate(R.layout.event_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            relative_gallery = mainLayout.findViewById(R.id.relative_gallery) as RelativeLayout
            img_expire = mainLayout.findViewById(R.id.img_expire) as ImageView
            img_count = mainLayout.findViewById(R.id.img_count) as TextView
            webView = mainLayout.findViewById(R.id.webView) as WebView
            tv_tree = mainLayout.findViewById(R.id.tv_tree) as TextView
            relative_desc = mainLayout.findViewById(R.id.relative_desc) as RelativeLayout
            img_reminder = mainLayout.findViewById(R.id.img_reminder) as ImageView
            img_fav = mainLayout.findViewById(R.id.img_fav) as ImageView
            img_share = mainLayout.findViewById(R.id.img_share) as ImageView
            img_share1 = mainLayout.findViewById(R.id.img_share1) as ImageView
            img_report = mainLayout.findViewById(R.id.img_report) as ImageView
            img_facebook = mainLayout.findViewById(R.id.img_facebook) as ImageView
            img_instagram = mainLayout.findViewById(R.id.img_instagram) as ImageView
            img_twitter = mainLayout.findViewById(R.id.img_twitter) as ImageView
            img_youtube = mainLayout.findViewById(R.id.img_youtube) as ImageView
            img_location = mainLayout.findViewById(R.id.img_location) as ImageView
            img_call = mainLayout.findViewById(R.id.img_call) as ImageView
            img_calender = mainLayout.findViewById(R.id.img_calender) as ImageView
            img_event = mainLayout.findViewById(R.id.img_event) as ImageView
            tv_views = mainLayout.findViewById(R.id.tv_views) as TextView
            tv_occasion_details = mainLayout.findViewById(R.id.tv_occasion_details) as TextView
            tv_days = mainLayout.findViewById(R.id.tv_days) as TextView
            linear_date = mainLayout.findViewById(R.id.linear_date) as LinearLayout
            linear_bottom = mainLayout.findViewById(R.id.linear_bottom) as LinearLayout
            linear_social = mainLayout.findViewById(R.id.linear_social) as ConstraintLayout
            relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading1) as ProgressBar
            ll_location1 = mainLayout.findViewById(R.id.ll_location1) as LinearLayout
            ll_phone1 = mainLayout.findViewById(R.id.ll_phone1) as LinearLayout



            img_calender.setOnClickListener(this)
            img_call.setOnClickListener(this)
            img_location.setOnClickListener(this)
            img_youtube.setOnClickListener(this)
            img_twitter.setOnClickListener(this)
            img_instagram.setOnClickListener(this)
            img_facebook.setOnClickListener(this)
            img_report.setOnClickListener(this)
            img_share.setOnClickListener(this)
            img_share1.setOnClickListener(this)
            img_fav.setOnClickListener(this)
            img_reminder.setOnClickListener(this)
            img_event.setOnClickListener(this)
            ll_location1.setOnClickListener(this)
            ll_phone1.setOnClickListener(this)
            tv_views.setTypeface(ContentActivity.Companion.tf)
            tv_tree.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_occasion_details.setTypeface(ContentActivity.Companion.tf)
            tv_days.setTypeface(ContentActivity.Companion.tf)
            img_reminder.setVisibility(View.INVISIBLE)
            img_calender.setVisibility(View.GONE)
            img_call.setImageResource(R.drawable.call_big)
            img_location.setImageResource(R.drawable.location_big)
            tv_days.setVisibility(View.VISIBLE)
            linear_bottom.setVisibility(View.VISIBLE)
        }
    }

    override fun onStart() {
        super.onStart()
        //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle.setText(
            act.getString(R.string.OccasionDetails).replace("*", "")
        )
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        SocialMediaShare.shareContext = act
        GetContents()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.img_reminder -> {
                val dialog1 = Dialog(activity!!)
                dialog1.setContentView(R.layout.custom_layout)
                dialog1.setTitle(getString(R.string.reminder_select_time))

                //dialog1.getWindow().getAttributes().windowAnimations = R.style.Animation;
                dialog1.show()
                val btn_done = dialog1.findViewById<View?>(R.id.btn_done_date) as Button
                val dp: DatePicker? = dialog1.findViewById<View?>(R.id.datePicker1) as DatePicker
                dp?.setMinDate(System.currentTimeMillis() - 1000)

                //get the data
                val parts: Array<String?> =
                    (events.EventDate + " 12:00").substring(0, 10).split("/".toRegex())
                        .toTypedArray()
                dp?.updateDate(parts[2]?.toInt()!!, parts[1]!!.toInt() - 1, parts[0]!!.toInt())
                val tp: TimePicker = dialog1.findViewById<View?>(R.id.timePicker1) as TimePicker
                val parts1: Array<String?> = (events.EventDate + " 12:00").substring(
                    11,
                    (events.EventDate + " 12:00").length
                ).split(":".toRegex()).toTypedArray()
                Log.d("parts1", "0 -- " + parts1[0])
                Log.d("parts1", "1 -- " + parts1[1])
                tp.setIs24HourView(true)
                tp.setCurrentHour(parts1[0]!!.toInt())
                tp.setCurrentMinute(parts1[1]!!.toInt())
                tp.setIs24HourView(false)
                val btn_next = dialog1.findViewById<View?>(R.id.btn_next) as Button
                val viewSwitcher: ViewSwitcher? =
                    dialog1.findViewById<View?>(R.id.viewswitcher) as ViewSwitcher?
                btn_next.setOnClickListener(View.OnClickListener { viewSwitcher!!.showNext() })
                btn_done.setOnClickListener(View.OnClickListener {
                    var month: String = (dp!!.getMonth() + 1).toString() + ""
                    var day: String = dp.getDayOfMonth().toString() + ""
                    var hours =
                        if (tp.getCurrentHour() > 12) "0" + (tp.getCurrentHour() - 12) else "0" + tp.getCurrentHour()
                    if (hours.length == 3) {
                        hours = hours.substring(1, hours.length)
                    }
                    val min =
                        if (tp.getCurrentMinute() < 10) "0" + tp.getCurrentMinute() else tp.getCurrentMinute()
                            .toString() + ""
                    month = if (month.length == 1) "0$month" else month
                    day = if (day.length == 1) "0$day" else day
                    val date_selected =
                        day + "/" + month + "/" + dp.getYear() + " " + hours + ":" + min + if (tp.getCurrentHour() > 12) " PM" else " AM"
                    try {
                        val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH)
                        val date = sdf.parse(date_selected)
                        val calendar = Calendar.getInstance()
                        calendar.time = date
                        val mgr: AlarmManager = activity!!.getApplicationContext()
                            .getSystemService(Context.ALARM_SERVICE) as AlarmManager
                        val ii = Intent(activity, NotifyUserService::class.java)
                        ii.putExtra(
                            "message", """
     ${getString(R.string.remember_title)}
     ${events.EventDate} 
      (${
                                if (languageSeassionManager.getLang()
                                        .equals("en", ignoreCase = true)
                                ) events.EventNameEn else events.EventNameAr
                            })
     """.trimIndent()
                        )
                        val pi: PendingIntent = PendingIntent.getService(activity, 0, ii, 0)
                        mgr.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pi)
                        Snackbar.make(
                            btn_done,
                            getString(R.string.reminder_set_title),
                            Snackbar.LENGTH_LONG
                        ).show()
                        dialog1.dismiss()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                })
            }
            R.id.img_event -> {

                /*if(getCompanies.getContentPhotos().size()>0){

                    productImages = new String[getCompanies.getContentPhotos().size()];

                    for(int i = 0; i< getCompanies.getContentPhotos().size(); i++){

                        GetContents.ContentPhotos photo = getCompanies.getContentPhotos().get(i);

                        productImages[i] = photo.getPhotos();

                    }

                }
                else{*/
                if (events.Photo != null && events.Photo != "") {
                    productImages = arrayOfNulls<String?>(1)
                    productImages[0] = events.Photo

                    //}
                    val b = Bundle()
                    b.putInt("position", 0)
                    b.putStringArray("productImages", productImages)
                    ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                }
            }
            R.id.img_fav -> {
            }
            R.id.img_share -> {
                permissionStr = Manifest.permission.READ_EXTERNAL_STORAGE
                GlobalFunctions.requestStoragePermission(act)
            }
            R.id.img_share1 -> {
                permissionStr = Manifest.permission.READ_EXTERNAL_STORAGE
                GlobalFunctions.requestStoragePermission(act)
            }
            R.id.img_report -> {
            }
            R.id.img_facebook -> if (events.Facebook!!.length > 0) {
                if (events.Facebook!!.contains("http") || events.Facebook!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(events.Facebook)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${events.Facebook}")
                        )
                    )
            }
            R.id.img_instagram -> if (events.Instagram!!.length > 0) {
                if (events.Instagram!!.contains("http") || events.Instagram!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(events.Instagram)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${events.Instagram}")
                        )
                    )
            }
            R.id.img_twitter -> if (events.Twitter!!.length > 0) {
                if (events.Twitter!!.contains("http") || events.Twitter!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(events.Twitter)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${events.Twitter}")
                        )
                    )
            }
            R.id.img_youtube -> if (events.YouTube!!.length > 0) {
                if (events.YouTube!!.contains("http") || events.YouTube!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(events.YouTube)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${events.YouTube}")
                        )
                    )
            }
            R.id.ll_location1 -> {
                if (events.Latitude!!.isNotEmpty() && events.Longitude!!.isNotEmpty()) {
                    /* if (GlobalFunctions.isGPSAllowed(act)) {
                         val news = GetNews()
                         news.Latitude = events.Latitude
                         news.Longitude = events.Longitude
                         val gson = Gson()
                         val bundle = Bundle()
                         val contentsArrayList: ArrayList<GetNews?> = ArrayList<GetNews?>()
                         contentsArrayList.add(news)
                         bundle.putString("GetNews", gson.toJson(contentsArrayList))
                         ContentActivity.Companion.openShowMapFragment(bundle)
                         return
                     }*/
                    permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                    GlobalFunctions.requestGPSPermission(act)
                } else
                    FixControl.showToast(act, getString(R.string.no_location_found))
            }
            R.id.ll_phone1 -> {
                if (events.Phone!!.isNotEmpty()) {
                    /*if (GlobalFunctions.isReadCallAllowed(act)) {
                        //if (events.Phone!!.length > 0) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.data = Uri.parse("tel:" + events.Phone)
                        act.startActivity(intent)
                        //}
                        return
                    }*/
                    permissionStr = Manifest.permission.CALL_PHONE
                    GlobalFunctions.requestCallPermission(act)
                } else
                    FixControl.showToast(act, getString(R.string.no_number_found))
            }
            R.id.img_calender -> {
            }
        }
    }

    fun GetContents() {
        Log.d("GetEvents", "GetEvents-MonasbatDetails")
        mloading.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetEvents(
            events.Id, "false", "false", "0",
            "0", mSessionManager.getUserCode(), catId
        )?.enqueue(
            object : Callback<ArrayList<GetEvents>?> {

                override fun onFailure(call: Call<ArrayList<GetEvents>?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetEvents>?>,
                    response: Response<ArrayList<GetEvents>?>
                ) {
                    if (response.body() != null) {
                        val getEvents = response.body()
                        if (mloading != null) {
                            if (getEvents != null) {
                                if (getEvents.size > 0) {
                                    events = getEvents[0]!!
                                    img_expire.setVisibility(View.GONE)

//                            img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                                    R.drawable.no_img_details)).getBitmap().getWidth();
                                    img_event.getLayoutParams().height =
                                        (act.getResources().getDrawable(
                                            R.drawable.no_img_details
                                        ) as BitmapDrawable).getBitmap().getHeight()
                                    if (events.Photo != null) if (events.Photo!!.length > 0) {
                                        Picasso.with(act).load(events.Photo)
                                            .placeholder(R.drawable.no_img_details)
                                            .error(R.drawable.no_img_details).into(img_event)
                                        val ob = GetAndSaveBitmapForArticle()
                                        ob.execute(events.Photo)
                                    }
                                    relative_gallery.setVisibility(View.INVISIBLE)
                                    //                        if(getCompanies.getContentPhotos().size()>=2){
//                            relative_gallery.setVisibility(View.VISIBLE);
//                            img_count.setText(getCompanies.getContentPhotos().size()+"");
//                        }

                                    //tv_days.setText(getCompanies.getFullName());
                                    tv_views.setText(
                                        events.ViewersCount + " " +
                                                act.getString(R.string.Views)
                                    )
                                    if (languageSeassionManager.getLang()
                                            .equals("en", ignoreCase = true)
                                    ) {
                                        setContent(
                                            "",
                                            events.EventNameEn + "\n\n" + events.DetailsEn
                                        )
                                        tv_tree.setText(events.EventTypeNameEn)
                                        tv_tree.setVisibility(View.VISIBLE)
                                        tv_days.setText(events.EventDate + "\n" + events.EventAfterEn)
                                    } else {
                                        setContent(
                                            "",
                                            events.EventNameAr + "\n\n" + events.DetailsAr
                                        )
                                        tv_tree.setText(events.EventTypeNameAr)
                                        tv_tree.setVisibility(View.VISIBLE)
                                        tv_days.setText(events.EventDate + "\n" + events.EventAfterAr)
                                    }
                                    settings()
                                    if (events.Phone != null) if (events.Phone!!.length > 0) {
                                    } else {
                                        img_call.setAlpha(50)
                                    }
                                    if (events.Latitude != null && events.Longitude != null) if (events.Latitude!!.length > 0 && events.Longitude!!.length > 0) {
                                    } else {
                                        img_location.setAlpha(50)
                                    }
                                    if (events.Facebook != null) if (events.Facebook!!.length > 0) {
                                    } else {
                                        img_facebook.setAlpha(50)
                                    }
                                    if (events.Twitter != null) if (events.Twitter!!.length > 0) {
                                    } else {
                                        img_twitter.setAlpha(50)
                                    }
                                    if (events.Instagram != null) if (events.Instagram!!.length > 0) {
                                    } else {
                                        img_instagram.setAlpha(50)
                                    }
                                    if (events.YouTube != null) if (events.YouTube!!.length > 0) {
                                    } else {
                                        img_youtube.setAlpha(50)
                                    }
                                }
                            }
                        }
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                }
            })
    }

    internal inner class GetAndSaveBitmapForArticle : AsyncTask<String?, Void?, Uri?>() {
        protected override fun doInBackground(vararg params: String?): Uri? {
            /*val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/Monasabatena"
            val dir = File(file_path)*/
            val file_path = Environment.getExternalStorageDirectory().absolutePath
            val dir = File(file_path, "/Monasabatena")
            if (!dir.exists()) dir.mkdirs()
            file = File(dir, UUID.randomUUID().toString() + ".png")
            val fOut: FileOutputStream
            try {
                val bm: Bitmap = Picasso.with(act).load(params[0]).get()
                fOut = FileOutputStream(file)
                bm.compress(Bitmap.CompressFormat.PNG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            pShareUri = FileProvider.getUriForFile(
                act,
                act.applicationContext.packageName + ".fileprovider", file!!
            )
            shareContext = act
            imgUri = pShareUri
            return pShareUri
        }
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.STORAGE_PERMISSION_CODE -> shareContent()
                GlobalFunctions.CALL_PERMISSION_CODE -> {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:" + events.Phone)
                    act.startActivity(intent!!)
                }
                GlobalFunctions.LOCATION_PERMISSION_CODE -> {
                    val news = GetNews()
                    news.Latitude = events.Latitude
                    news.Longitude = events.Longitude
                    val gson = Gson()
                    val bundle = Bundle()
                    val contentsArrayList: ArrayList<GetNews?> = ArrayList<GetNews?>()
                    contentsArrayList.add(news)
                    bundle.putString("GetNews", gson.toJson(contentsArrayList))
                    ContentActivity.Companion.openShowMapFragment(bundle)
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun shareContent() {

//        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
//               getCompanies.getCompanyCategoryNameEn()+  " @ AlQenaat " + " \n " + getCompanies.getDetailsEN() + "\n\n" + getCompanies.getPhotos() + "\n\n" + act.getString(R.string.UrlLink)
//                : getCompanies.getCompanyCategoryNameAr() +  " @ AlQenaat" + " \n " + getCompanies.getDetailsAR() + "\n\n" + getCompanies.getPhotos() + "\n\n" + act.getString(R.string.UrlLink);
        val shareProductInfo1: String =
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true))
                events.EventNameEn + "" + "\n\n" + events.DetailsEn + "" + "\n\n" +
                        act.getString(R.string.UrlLink1) else events.EventNameAr + "" + "\n\n" +
                    events.DetailsAr + "" + "\n\n" + act.getString(R.string.UrlLink1)

        val shareProductInfo =
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) """
     ${events.EventNameEn}

     ${events.DetailsEn}

     ${act.getString(R.string.UrlLink1)}
     """.trimIndent() else """
     ${events.EventNameAr}

     ${events.DetailsAr}

     ${act.getString(R.string.UrlLink1)}
     """.trimIndent()


/*        String contentType="";
        if(getCompanies.getContentType().equalsIgnoreCase("1")){
            contentType = act.getString(R.string.NewsLabel);
        }
        else if(getCompanies.getContentType().equalsIgnoreCase("2")){
            if(getCompanies.getIsActivity().equalsIgnoreCase("1")){
                contentType = act.getString(R.string.SpecialOccasionLabel);
            }
            else{
                contentType = act.getString(R.string.OccasionLabel);
            }

        }
        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
               act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getCompanies.getContentLink() + "\n\n" + act.getString(R.string.UrlLink)
                : act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getCompanies.getContentLink() + "\n\n" + act.getString(R.string.UrlLink);
**/
        //val ob: GetAndSaveBitmapForArticle = GetAndSaveBitmapForArticle()
        val clipboard = act.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("AlQenaat", shareProductInfo)
        clipboard.setPrimaryClip(clip)
        //ob.execute(events.Photo)
        val dialog = Dialog(act)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.share_dialog)
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        val mimg_close: ImageView
        val mimg_facebook: ImageView
        val mimg_twitter: ImageView
        val mimg_instagram: ImageView
        val mimg_email: ImageView
        val mimg_sms: ImageView
        val mimg_whats: ImageView
        mimg_facebook = dialog.findViewById<View?>(R.id.img_facebook) as ImageView
        val title: TextView = dialog.findViewById<View?>(R.id.shartext) as TextView
        title.setTypeface(ContentActivity.Companion.tf)
        mimg_twitter = dialog.findViewById<View?>(R.id.img_twitter) as ImageView
        mimg_instagram = dialog.findViewById<View?>(R.id.img_instagram) as ImageView
        mimg_email = dialog.findViewById<View?>(R.id.img_email) as ImageView
        mimg_sms = dialog.findViewById<View?>(R.id.img_sms) as ImageView
        mimg_whats = dialog.findViewById<View?>(R.id.img_whats) as ImageView
        mimg_close = dialog.findViewById<View?>(R.id.img_searchClose) as ImageView
        mimg_close.setOnClickListener(View.OnClickListener {
            file?.delete()
            dialog.dismiss()
        })
        mimg_facebook.setOnClickListener(View.OnClickListener { /*Share(1,
                                "com.facebook.katana",
                                products.getDecription() + "\n" +
                                        products.getPhotos());*/
            dialog.dismiss()
            /* if (mSessionManager.isFacebookOpenBefore()) {
                 Share(1,
                         "facebook",
                         shareProductInfo)
             } else {*/
            AlertDialog.Builder(act)
                .setTitle(act.getString(R.string.AttentionLabel))
                .setMessage(act.getString(R.string.AttentionText))
                .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        shareData(1, "facebook", shareProductInfo)
                        mSessionManager.FacebookOpened()
                    }
                })
                .setIcon(R.drawable.icon_512)
                .show()
            //}
        })
        mimg_twitter.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(5, "twitter", shareProductInfo)
        })
        mimg_instagram.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            AlertDialog.Builder(act)
                .setIcon(R.drawable.icon_512)
                .setTitle(act.getString(R.string.AttentionLabel))
                .setMessage(act.getString(R.string.AttentionText))
                .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        shareData(6, "instagram", shareProductInfo)
                    }
                })
                .setIcon(R.drawable.icon_512)
                .show()
        })
        mimg_email.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(3, "gmail", shareProductInfo)
        })
        mimg_sms.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(4, "sms", shareProductInfo)
        })
        mimg_whats.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(2, "whatsapp", shareProductInfo)
        })
        dialog.show()
    }

    fun setContent(title: String?, content: String?) {
        val sp: Spannable = SpannableString(title)
        Linkify.addLinks(sp, Linkify.ALL)
        val sp1: Spannable = SpannableString(content)
        Linkify.addLinks(sp1, Linkify.ALL)
        var finalHTML = ""
        finalHTML = if (title!!.length > 0) {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center><center>" + Html.toHtml(
                sp
            )
                    + "</center></b></span style='color: #4c4c4c;'><br /><br /><center>" + sp1 + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        } else {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center>" + Html.toHtml(
                sp
            )
                    + "</center></span style='color: #4c4c4c;'><center>" + sp1 + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        }
        webView.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
        webView.setBackgroundColor(0)
        webView.setWebViewClient(WebViewClient())
        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true)
        webView.getSettings().setPluginState(WebSettings.PluginState.ON)
        webView.setWebChromeClient(WebChromeClient())
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, urlNewString: String?): Boolean {
                if (!loadingFinished) {
                    redirect = true
                }
                loadingFinished = false
                //webView.loadUrl(urlNewString);
                // Here the String url hold 'Clicked URL'
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlNewString)));
                if (urlNewString!!.startsWith("tel:")) {
                    val intent = Intent(
                        Intent.ACTION_DIAL,
                        Uri.parse(urlNewString)
                    )
                    startActivity(intent)
                } else if (urlNewString.startsWith("http:") || urlNewString.startsWith("https:")) {
                    view!!.loadUrl(urlNewString)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, facIcon: Bitmap?) {
                loadingFinished = false
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                mloading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (!redirect) {
                    loadingFinished = true
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
                if (loadingFinished && !redirect) {
                    //HIDE LOADING IT HAS FINISHED
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                } else {
                    redirect = false
                }
            }
        })
    }

    private fun settings() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()!!.settings()
            ?.enqueue(object : Callback<GetAppSettings?> {

                override fun onFailure(call: Call<GetAppSettings?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<GetAppSettings?>,
                    response: Response<GetAppSettings?>
                ) {
                    if (response.body() != null) {
                        val settings = response.body()
                        if (settings != null) {
                            if (settings != null) {
                                appSettings = settings
                                if (events != null && events.isInvited != null) {
                                    if (events.isInvited.equals("true", ignoreCase = true)) {
                                        setContent(
                                            "",
                                            events.EventNameEn + "\n\n" + events.DetailsEn + "\n\n" + act.getString(
                                                R.string.ContactDetailsLabel
                                            ) + ":\n\n" + act.getString(R.string.NameLabel) + ": " + appSettings.EventText + "\n\n" + act.getString(
                                                R.string.Phone
                                            ) + ": " + appSettings.EventPhone
                                        )
                                    }
                                }
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                }
            })
    }

    companion object {
        protected val TAG = MonasbatDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: MonasbatDetailsFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): MonasbatDetailsFragment {
            fragment = MonasbatDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}