package com.qenaat.app.adapters

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetEvents
import com.squareup.picasso.Picasso
import java.util.*

/**
 * Created by DELL on 04-Sep-17.
 */
class SlidingImageAdapter(context: Context, IMAGES: ArrayList<GetEvents>) : PagerAdapter() {

    private val IMAGES: ArrayList<GetEvents>
    private val inflater: LayoutInflater
    private val context: Context
    var languageSeassionManager: LanguageSessionManager

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout: View = inflater.inflate(R.layout.slidingimages_layout, view, false)!!
        val imageView: ImageView = imageLayout.findViewById(R.id.image) as ImageView

//        Glide.with(context).load(IMAGES.get(position)).into(imageView);

//        int imgW = ((BitmapDrawable) context.getResources().getDrawable(
//                R.drawable.no_img_details)).getBitmap().getWidth();
        val imgH = (context.getResources().getDrawable(
                R.drawable.no_img_details) as BitmapDrawable).bitmap.height

//        imageView.getLayoutParams().width = imgW;
        imageView.getLayoutParams().height = imgH
        if (IMAGES.get(position).Photo?.length!! > 0) {
            Picasso.with(context).load(IMAGES.get(position).Photo).placeholder(R.drawable.no_img_details)
                    .error(R.drawable.no_img_details)
                    .fit().into(imageView)
        }
        imageView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetEvents.Events", gson.toJson(IMAGES.get(position)))
                ContentActivity.Companion.openMonasbatDetailsFragment(b)
            }
        })
        view.addView(imageLayout, 0)
        return imageLayout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getCount(): Int {
        return IMAGES.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    init {
        this.context = context
        this.IMAGES = IMAGES
        inflater = LayoutInflater.from(context)
        languageSeassionManager = LanguageSessionManager(context)
    }
}