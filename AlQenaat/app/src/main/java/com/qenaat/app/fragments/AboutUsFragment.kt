package com.qenaat.app.fragments

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetAboutUs
import com.qenaat.app.model.GetTermsandconditions
import com.qenaat.app.networking.QenaatAPICall
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 25-May-17.
 */
class AboutUsFragment : Fragment() {
    var act: FragmentActivity? = null
    var mloading: ProgressBar? = null
    var XY: IntArray? = null
    var mSessionManager: SessionManager? = null
    var languageSessionManager: LanguageSessionManager? = null
    private var getAboutUsArrayList: ArrayList<GetAboutUs?>? = null
    private var getTermsAndConditiosArrayList: ArrayList<GetTermsandconditions?>? = null
    var webView: WebView? = null
    var mainLayout: RelativeLayout? = null
    var type: String? = ""
    var url: String? = ""
    var loadingFinished = true
    var redirect = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)
            mSessionManager = SessionManager(act!!)
            languageSessionManager = LanguageSessionManager(act)
            if (arguments != null) {
                type = arguments?.getString("type")
                url = arguments?.getString("url")
                Log.d("type-->", "" + type)
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return try {
            mainLayout = if (type.equals("video", ignoreCase = true)) {
                inflater.inflate(R.layout.about_us, null) as RelativeLayout
            } else {
                inflater.inflate(R.layout.about_us, null) as RelativeLayout
            }
            initViews(mainLayout)
            mainLayout
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun initViews(mainLayout: RelativeLayout?) {
        if (mainLayout != null) {
            webView = mainLayout.findViewById<View?>(R.id.webView) as WebView?
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar?
            ContentActivity.Companion.setTextFonts(mainLayout)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d("type--->", "" + type)
        ContentActivity.Companion.enableLogin(languageSessionManager)
        ContentActivity.Companion.img_topHall?.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd?.setVisibility(View.GONE)
        if (type.equals("video", ignoreCase = true)) {
        } else {
            ContentActivity.Companion.mtv_topTitle?.setVisibility(View.VISIBLE)
            // ContentActivity.mtv_topTitle.setText(act.getString(R.string.TermsTitle));
            getAboutUs()
        }
        webView?.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, urlNewString: String?): Boolean {
                if (!loadingFinished) {
                    redirect = true
                }
                loadingFinished = false
                view?.loadUrl(urlNewString)
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, facIcon: Bitmap?) {
                loadingFinished = false
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                mloading?.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (!redirect) {
                    loadingFinished = true
                    mloading?.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
                if (loadingFinished && !redirect) {
                    //HIDE LOADING IT HAS FINISHED
                    mloading?.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                } else {
                    redirect = false
                }
            }
        })
        if (type.equals("video", ignoreCase = true)) {
            // ContentActivity.Companion.topBarView?.setVisibility(View.GONE)
            ContentActivity.Companion.bottomBarView?.setVisibility(View.GONE)
            webView?.setWebViewClient(WebViewClient())
            webView?.getSettings()?.javaScriptEnabled = true
            webView?.getSettings()?.javaScriptCanOpenWindowsAutomatically = true
            webView?.getSettings()?.pluginState = WebSettings.PluginState.ON
            webView?.setWebChromeClient(WebChromeClient())
            webView?.getSettings()?.mediaPlaybackRequiresUserGesture = false
            val content = "<!DOCTYPE html>" +
                    "<html>" +
                    "<head>" +
                    "<title></title>" +
                    "</head>" +
                    "<body >" +
                    "" +
                    "<video autoplay controls style='width: 98%'>" +
                    "  <source src=\"" + url + "\" >" +
                    "  <p></p>" +
                    "</video>" +
                    "" +
                    "</body>" +
                    "</html>"
            Log.d("content", content)

            // webView.loadDataWithBaseURL(content);
            webView?.loadDataWithBaseURL("file:///android_asset/", content, "text/html", "UTF-8", null)
            webView?.setWebViewClient(WebViewClient())
            webView?.getSettings()?.javaScriptEnabled = true
            webView?.getSettings()?.javaScriptCanOpenWindowsAutomatically = true
            webView?.getSettings()?.pluginState = WebSettings.PluginState.ON
            webView?.setWebChromeClient(WebChromeClient())
            webView?.setBackgroundColor(Color.BLACK)

            //webView.setInitialScale(getScale());

//            WebSettings ws = webView.getSettings();
//            ws.setMediaPlaybackRequiresUserGesture(false);
//            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
    }

    fun getAboutUs() {
        mloading?.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        Log.d("type", "" + type)
        webView?.setWebViewClient(WebViewClient())
        webView?.getSettings()?.javaScriptEnabled = true
        webView?.getSettings()?.javaScriptCanOpenWindowsAutomatically = true
        webView?.getSettings()?.pluginState = WebSettings.PluginState.ON
        webView?.setWebChromeClient(WebChromeClient())
        if (type.equals("about_us", ignoreCase = true)) {
            Log.d("type", "about")

            // webView.loadUrl(act.getString(R.string.GetAboutUs)+ (languageSessionManager.getLang().equals("en") ? "en" : "ar" ));
            QenaatAPICall.getCallingAPIInterface()?.GetAboutUs()?.enqueue(object : Callback<GetAboutUs?> {
                override fun onFailure(call: Call<GetAboutUs?>, t: Throwable) {
                    t.printStackTrace()
                    mloading?.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(call: Call<GetAboutUs?>, response: Response<GetAboutUs?>) {
                    if (response.body() != null) {
                        val aboutUses = response.body()
                        if (mloading != null && aboutUses != null) {
                            getAboutUsArrayList = ArrayList()
                            getAboutUsArrayList?.add(aboutUses)
                            if (getAboutUsArrayList != null) {
                                webView?.setWebViewClient(WebViewClient())
                                webView?.getSettings()?.javaScriptEnabled = true
                                webView?.getSettings()?.javaScriptCanOpenWindowsAutomatically = true
                                webView?.getSettings()?.pluginState = WebSettings.PluginState.ON
                                webView?.setWebChromeClient(WebChromeClient())
                                var mainHTMLText: String? = ""
                                mainHTMLText = if (languageSessionManager?.getLang().equals("en", ignoreCase = true)) {
                                    ContentActivity.Companion.mtv_topTitle?.setText(act?.getString(R.string.AboutUSLabel))
                                    getAboutUsArrayList?.get(0)?.ContentsEn
                                }
                                else {
                                    ContentActivity.Companion.mtv_topTitle?.setText(act?.getString(R.string.AboutUSLabel))
                                    getAboutUsArrayList?.get(0)?.ContentsAr
                                }
                                mainHTMLText = mainHTMLText!!.replace("kashida".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("font-size:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("text-justify:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("font-size:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("line-height:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("font-family".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("f ont-family:".toRegex(), "font-family:")
                                mainHTMLText = mainHTMLText.replace("text-:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("150%".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("color".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("<strong>".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("padding".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("src=\\\"/uploads".toRegex(), "src=\\\"http://www.souq.com/uploads")
                                mainHTMLText = mainHTMLText.replace("app.souq.com".toRegex(), "www.souq.com")
                                mainHTMLText = mainHTMLText.replace("http://www.souq.comhttp://www.souq.com/".toRegex(), "http://www.souq.com/")
                                mainHTMLText = mainHTMLText.replace("<font".toRegex(), "<f ff")
                                mainHTMLText = mainHTMLText.replace("display:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("position:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("width:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("max-width:".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("<h1".toRegex(), "<h 1")
                                mainHTMLText = mainHTMLText.replace("<h2".toRegex(), "<h 2")
                                mainHTMLText = mainHTMLText.replace("<h3".toRegex(), "<h 3")
                                mainHTMLText = mainHTMLText.replace("<h4".toRegex(), "<h 4")
                                mainHTMLText = mainHTMLText.replace("<h5".toRegex(), "<h 5")
                                mainHTMLText = mainHTMLText.replace("<h6".toRegex(), "<h 6")
                                mainHTMLText = mainHTMLText.replace("id=\"".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("pastingspan1".toRegex(), "")
                                mainHTMLText = mainHTMLText.replace("c olor".toRegex(), "color")
                                mainHTMLText = mainHTMLText.replace("l ine-h eight:".toRegex(), "line-height:")
                                mainHTMLText = mainHTMLText.replace("ff ont-size".toRegex(), "font-size")
                                mainHTMLText = mainHTMLText.replace("bold".toRegex(), "")
                                Log.e("mainHTMLText", "" + mainHTMLText)
                                var finalHTML = ""
                                finalHTML = if (languageSessionManager?.getLang().equals("en", ignoreCase = true)) {
                                    ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/verdana.ttf');}" +
                                            " body {background-color: " +
                                            "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                                            "text-align: justify;line-height: 150%;}</style></head><body dir='LTR'><br />"
                                            + "</b></span style='color: #222222;'>" + mainHTMLText + "<br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
                                } else {
                                    ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                                            " body {background-color: " +
                                            "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                                            "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br />"
                                            + "</b></span style='color: #222222;'>" + mainHTMLText + "<br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
                                }
                                Log.e("finalHTML", "" + finalHTML)
                                webView?.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
                                webView?.setBackgroundColor(0)
                            } else {
                                Log.e("settingArrayList", "null")
                            }
                        }
                        mloading?.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                }
            })
        } else {
            Log.d("type", "tnc")


            //    webView.loadUrl(act.getString(R.string.GetTermsAndConditions)+ (languageSessionManager.getLang().equals("en") ? "en" : "ar" ));
            QenaatAPICall.getCallingAPIInterface()?.GetTermsAndConditions()?.enqueue(
                    object : Callback<GetTermsandconditions?> {

                        override fun onFailure(call: Call<GetTermsandconditions?>, t: Throwable) {
                            t.printStackTrace()
                            mloading?.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<GetTermsandconditions?>, response: Response<GetTermsandconditions?>) {
                            if (response.body() != null) {
                                val aboutUses = response.body()
                                if (mloading != null && aboutUses != null) {
                                    Log.d("aboutUses size", "size")
                                    if (aboutUses != null) {
                                        getTermsAndConditiosArrayList = ArrayList()
                                        getTermsAndConditiosArrayList?.add(aboutUses)
                                        if (getTermsAndConditiosArrayList != null) {
                                            webView?.setWebViewClient(WebViewClient())
                                            webView?.getSettings()?.javaScriptEnabled = true
                                            webView?.getSettings()?.javaScriptCanOpenWindowsAutomatically = true
                                            webView?.getSettings()?.pluginState = WebSettings.PluginState.ON
                                            webView?.setWebChromeClient(WebChromeClient())
                                            var mainHTMLText: String? = ""
                                            mainHTMLText = if (languageSessionManager?.getLang() == "en") {
                                                //ContentActivity.mtv_topTitle.setText(getTermsAndConditiosArrayList.get(0).getTitleEN());
                                                ContentActivity.Companion.mtv_topTitle?.setText(act?.getString(R.string.TermsConditionsLabel))
                                                getTermsAndConditiosArrayList?.get(0)?.ContentsEn
                                            } else {
                                                //ContentActivity.mtv_topTitle.setText(getTermsAndConditiosArrayList.get(0).getTitleAR());
                                                ContentActivity.Companion.mtv_topTitle?.setText(act?.getString(R.string.TermsConditionsLabel))
                                                getTermsAndConditiosArrayList?.get(0)?.ContentsAr
                                            }
                                            var finalHTML = ""
                                            finalHTML = if (languageSessionManager?.getLang() == "en") {
                                                ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/verdana.ttf');}" +
                                                        " body {background-color: " +
                                                        "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                                                        "text-align: justify;line-height: 150%;}</style></head><body dir='LTR'><br />"
                                                        + "</b></span style='color: #222222;'>" + mainHTMLText + "<br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
                                            } else {
                                                ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                                                        " body {background-color: " +
                                                        "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                                                        "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br />"
                                                        + "</b></span style='color: #222222;'>" + mainHTMLText + "<br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
                                            }
                                            Log.e("finalHTML", "" + finalHTML)
                                            webView?.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
                                            webView?.setBackgroundColor(0)
                                        } else {
                                            Log.e("settingArrayList", "null")
                                        }
                                    }
                                }
                            }
                            mloading?.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        }
    }

    private fun getScale(): Int {
        val PIC_WIDTH = webView?.getRight()!! - webView?.getLeft()!!
        val display = (act?.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
        val width = display.width
        var `val`: Double = (width / PIC_WIDTH).toDouble()
        `val` = `val` * 100.0
        return `val`.toInt()
    }

    companion object {
        protected val TAG = AboutUsFragment::class.java.simpleName
        var fragment: AboutUsFragment? = null
        fun newInstance(act: FragmentActivity?): AboutUsFragment? {
            fragment = AboutUsFragment()
            fragment!!.act = act
            return fragment
        }
    }
}