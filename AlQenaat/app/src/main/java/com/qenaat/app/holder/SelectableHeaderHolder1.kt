package com.qenaat.app.holder

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.fragments.HomeFragment
import com.qenaat.app.model.GetFamilyTree
import com.squareup.picasso.Picasso
import com.unnamed.b.atv.model.TreeNode
import com.unnamed.b.atv.model.TreeNode.BaseNodeViewHolder
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by DELL on 26-Nov-17.
 */
class SelectableHeaderHolder1(var act: FragmentActivity?, comingFrom: String?) : BaseNodeViewHolder<GetFamilyTree?>(act) {
    private var tv_person_name: TextView? = null
    private var tv_age: TextView? = null
    var linear_category: LinearLayout? = null
    var linear_content: LinearLayout? = null
    var ll_item: LinearLayout? = null
//    var img_category_colDate
    var img_category_color: CircleImageView? = null
    var img_user_profile: CircleImageView? = null
    var relative_parent: RelativeLayout? = null
    var img_arrow: ImageView? = null
    var img_add_tree: ImageView? = null
    var comingFrom: String? = ""
    var mSessionManager: SessionManager?
    var mLangSessionManager: LanguageSessionManager?

    override fun createNodeView(node: TreeNode?, value: GetFamilyTree?): View? {
        val inflater = LayoutInflater.from(act)
        val view = inflater.inflate(R.layout.layout_selectable_header1, null, false)
        linear_category = view.findViewById<View?>(R.id.linear_category) as LinearLayout?
        linear_content = view.findViewById<View?>(R.id.linear_content) as LinearLayout?
        ll_item = view.findViewById<View?>(R.id.ll_item) as LinearLayout?
        relative_parent = view.findViewById<View?>(R.id.relative_parent) as RelativeLayout?
        img_category_color = view.findViewById<View?>(R.id.img_category_color) as CircleImageView?
        img_user_profile = view.findViewById<View?>(R.id.img_user_profile) as CircleImageView?
        img_arrow = view.findViewById<View?>(R.id.img_arrow) as ImageView?
        img_add_tree = view.findViewById<View?>(R.id.img_add_tree) as ImageView?
        tv_age = view.findViewById<View?>(R.id.tv_age) as TextView?
        tv_person_name = view.findViewById<View?>(R.id.tv_person_name) as TextView?
        ContentActivity.Companion.setTextFonts(relative_parent!!)
        img_add_tree?.visibility = View.GONE

        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)

        if (mLangSessionManager?.getLang() == "en")
            ll_item?.layoutDirection = View.LAYOUT_DIRECTION_LTR
        else
            ll_item?.layoutDirection = View.LAYOUT_DIRECTION_RTL

        //if(comingFrom.equalsIgnoreCase("add")){
        if (value?.Gender.equals("1", ignoreCase = true)) {
            img_add_tree?.visibility = View.VISIBLE
            img_add_tree?.setOnClickListener(View.OnClickListener {
                ContentActivity.Companion.parentId = value?.Id!!
                ContentActivity.Companion.parentNameEn = value?.NameEn!!
                ContentActivity.Companion.parentNameAr = value?.NameAr!!
                if (comingFrom.equals("add", ignoreCase = true)) {
                    act?.getSupportFragmentManager()?.popBackStack()
                } else {
                    mSessionManager?.setImagePath("")

                    if (ContentActivity.mSessionManager?.isLoggedin()!!
                        && ContentActivity.mSessionManager?.getUserCode() !== "" && ContentActivity.mSessionManager?.getUserCode() != null
                    ) {
                        ContentActivity.openAddFamilyTreeFragment()

                    } else {
                        Snackbar.make(HomeFragment.mainLayout, HomeFragment.act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                        ContentActivity.openLoginFragment()
                    }

//                    ContentActivity.openAddFamilyTreeFragment()
                }
            })
        }

        //}
        tv_person_name?.setTextColor(Color.parseColor("#666666"))
        if (value?.IsVip.equals("true", ignoreCase = true)) {
            tv_person_name?.setTextColor(Color.parseColor("#87776f"))
        }
        tv_person_name?.text = value?.NameAr
        img_category_color?.setColorFilter(Color.parseColor("#f1f1f1"))
        img_category_color?.visibility = View.VISIBLE
        tv_age?.visibility = View.VISIBLE
        if (value?.IsBranch.equals("true", ignoreCase = true)) {

            //if(comingFrom.equalsIgnoreCase("add")){
            img_add_tree?.visibility = View.GONE

            //}
            img_category_color?.visibility = View.GONE
            tv_age?.visibility = View.GONE
        }
        if (value?.IsLive.equals("true", ignoreCase = true)) {
            tv_age?.text = (if (value?.BirthDate?.split("/".toRegex())?.toTypedArray()?.size!! > 1)
                value?.BirthDate?.split("/".toRegex())?.toTypedArray()!![2] + " - " else "") +
                    act?.getString(R.string.PresentLabel)
        } else {
            tv_age?.text = (if (value?.BirthDate?.split("/".toRegex())?.toTypedArray()?.size!! > 1)
                value?.BirthDate?.split("/".toRegex())?.toTypedArray()!![2] + " - " else "") +
                    if (value.DeathDate?.split("/".toRegex())?.toTypedArray()?.size!! > 1)
                        value?.DeathDate?.split("/".toRegex())?.toTypedArray()!![2]
                    else act?.getString(R.string.DeadLabel)
        }
        img_arrow?.setVisibility(View.VISIBLE)
        img_arrow?.setImageResource(R.drawable.arrow_left)
        Log.d("isExpanded", "" + node?.isExpanded())
        if (node?.isExpanded()!!) {
            img_arrow?.setImageResource(R.drawable.arrow_down)
        }
        if (node.getLevel() == 1) {
            img_arrow?.setVisibility(View.GONE)
        }
        if (value.HaveChild.equals("false", ignoreCase = true)) {
            img_arrow?.visibility = View.INVISIBLE
        }
        img_user_profile?.setOnClickListener(View.OnClickListener {
            if (value.IsVip.equals("true", ignoreCase = true)) {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetFamilyTree", gson.toJson(value))
                ContentActivity.openVIPPersonDetailsFragment(b)
            }
        })
        relative_parent?.setOnClickListener(View.OnClickListener {
            Log.d("Children", "size -> " + node.getChildren().size)
            Log.d("Children", "isExpanded -> " + node.isExpanded())
            Log.d("Children", "getId -> " + (value as GetFamilyTree?)?.Id)
            Log.d("Children", "getParentId -> " + (value as GetFamilyTree?)?.ParentId)

//                if(node.getChildren().size()>0){
            if (node.getChildren().size > 0) {
                Log.d("Children", "size -> 1")
                if (node.isExpanded()) {
                    Log.d("Children", "size -> 2")
                    treeView.collapseNode(node)
                    img_arrow?.setImageResource(R.drawable.arrow_left)
                } else {
                    Log.d("Children", "size -> 3")
                    treeView.expandNode(node)
                    img_arrow?.setImageResource(R.drawable.arrow_down)
                }
            } else {
                Log.d("Children", "size -> 4")
                if (value.HaveChild.equals("true", ignoreCase = true)) {
                    Log.d("Children", "size -> 5")
                    HomeFragment.Companion.getSubCategories(value as GetFamilyTree?, node, img_arrow)
                }
            }
        })
        if (value.Color?.length!! > 6) {
            img_category_color?.setColorFilter(Color.parseColor(value.Color))
        }
        img_user_profile?.getLayoutParams()?.width = (act?.getResources()?.getDrawable(
                R.drawable.tree_node) as BitmapDrawable).bitmap.width
        img_user_profile?.getLayoutParams()?.height = (act?.getResources()?.getDrawable(
                R.drawable.tree_node) as BitmapDrawable).bitmap.height
        img_user_profile?.setImageResource(R.drawable.tree_node)
        if (value.Photo?.length!! > 0) Picasso.with(act)
                .load(value.Photo)
                .error(R.drawable.tree_node)
                .placeholder(R.drawable.tree_node)
                .config(Bitmap.Config.RGB_565)
                .into(img_user_profile)


//        if(value.getPhotos().length()>0)
//            Glide.with(context)
//                    .load(value.getPhotos())
//                    .placeholder(R.drawable.tree_node)
//                    .error(R.drawable.tree_node)
//                    .into(img_user_profile);
        return view
    }

    override fun toggle(active: Boolean) {}
    override fun toggleSelectionMode(editModeEnabled: Boolean) {}

    init {
        this.comingFrom = comingFrom
        mSessionManager = SessionManager(act!!)
        mLangSessionManager = LanguageSessionManager(act!!)
    }
}