package com.qenaat.app.fragments

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.SplashActivity
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetCompanyCategories
import com.qenaat.app.model.RegisterModel
import com.qenaat.app.networking.QenaatAPICall
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by shahbazshaikh on 26/07/16.
 */
class MyAccountFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var tv_account: TextView
    lateinit var tv_services: TextView
    lateinit var tv_halls: TextView
    lateinit var tv_my_invitation: TextView
    lateinit var tv_my_occasion: TextView
    lateinit var tv_companies: TextView
    lateinit var tv_logout: TextView
    lateinit var tv_pass: TextView
    lateinit var tv_paymentview: TextView
    lateinit var img_account: ImageView
    lateinit var img_apass: ImageView
    lateinit var img_services: ImageView
    lateinit var img_my_occasion: ImageView
    lateinit var img_my_invitation: ImageView
    lateinit var img_halls: ImageView
    lateinit var img_companies: ImageView
    lateinit var img_paymentview: ImageView
    lateinit var img_logout: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.my_account, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            tv_my_invitation = mainLayout.findViewById<View?>(R.id.tv_my_invitation) as TextView
            tv_my_occasion = mainLayout.findViewById<View?>(R.id.tv_my_occasion) as TextView
            tv_halls = mainLayout.findViewById<View?>(R.id.tv_halls) as TextView
            tv_logout = mainLayout.findViewById<View?>(R.id.tv_logout) as TextView
            tv_account = mainLayout.findViewById<View?>(R.id.tv_account) as TextView
            tv_pass = mainLayout.findViewById<View?>(R.id.tv_pass) as TextView
            tv_services = mainLayout.findViewById<View?>(R.id.tv_services) as TextView
            tv_companies = mainLayout.findViewById<View?>(R.id.tv_companies) as TextView
            tv_paymentview = mainLayout.findViewById<View?>(R.id.tv_companies) as TextView
            img_account = mainLayout.findViewById<View?>(R.id.img_account) as ImageView
            img_logout = mainLayout.findViewById<View?>(R.id.img_logout) as ImageView
            img_halls = mainLayout.findViewById<View?>(R.id.img_halls) as ImageView
            img_companies = mainLayout.findViewById<View?>(R.id.img_companies) as ImageView
            img_my_invitation = mainLayout.findViewById<View?>(R.id.img_my_invitation) as ImageView
            img_my_occasion = mainLayout.findViewById<View?>(R.id.img_my_occasion) as ImageView
            img_services = mainLayout.findViewById<View?>(R.id.img_services) as ImageView
            img_apass = mainLayout.findViewById<View?>(R.id.img_apass) as ImageView
            img_paymentview = mainLayout.findViewById<View?>(R.id.img_paymentview) as ImageView

            img_logout.setOnClickListener(this)
            img_companies.setOnClickListener(this)
            img_halls.setOnClickListener(this)
            img_account.setOnClickListener(this)
            img_my_invitation.setOnClickListener(this)
            img_my_occasion.setOnClickListener(this)
            img_services.setOnClickListener(this)
            img_apass.setOnClickListener(this)
            img_paymentview.setOnClickListener(this)

            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_logout.setTypeface(ContentActivity.Companion.tf)
            tv_account.setTypeface(ContentActivity.Companion.tf)
            tv_halls.setTypeface(ContentActivity.Companion.tf)
            tv_companies.setTypeface(ContentActivity.Companion.tf)
            tv_pass.setTypeface(ContentActivity.Companion.tf)
            tv_services.setTypeface(ContentActivity.Companion.tf)
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.mtv_topTitle.setText(act.getText(R.string.MyAccountLabel))
    }

    override fun onClick(view: View?) {
        var bundle = Bundle()
        when (view?.getId()) {
            R.id.img_companies -> {
                val gson = Gson()
                val b = Bundle()
                val categories = GetCompanyCategories()
                categories.Id = "0"
                b.putString("GetCompanyCategories", gson.toJson(categories))
                b.putString("comingFrom", "my account")
                ContentActivity.Companion.openCompanyFragment(b)
            }
            R.id.img_my_invitation -> ContentActivity.Companion.openMyInvitationListFragment()
            R.id.img_paymentview -> ContentActivity.Companion.openPaymentlistFragment()
            R.id.img_my_occasion -> ContentActivity.Companion.openMyOccasionFragment()
            R.id.img_halls -> {

                //ContentActivity.openAddressFragment();
                bundle = Bundle()
                bundle.putString("type", "my account")
                ContentActivity.Companion.openHallFragment(bundle)
            }
            R.id.img_account -> {
                bundle.putString("type", "edit")
                ContentActivity.Companion.openRegisterFragment(bundle)
            }
            R.id.img_logout -> AlertDialog.Builder(act)
                    .setTitle(act.getString(R.string.Confirm))
                    .setMessage(act.getString(R.string.AreYouSureYouWantToLogoutLabel_))
                    .setPositiveButton(android.R.string.yes) { dialog, which -> logout(dialog) }
                    .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
                    .setIcon(R.drawable.icon_512)
                    .show()
            R.id.img_apass -> ContentActivity.Companion.openChangePasswordFragment()
            R.id.img_services -> ContentActivity.Companion.openServicesMyAccountFragment()
        }
    }

    fun logout(dialog: DialogInterface?) {

        //Toast.makeText(activity , "Work in progess..." , Toast.LENGTH_LONG).show()

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.logout(/*mSessionManager.getUserCode(),*/
                "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                languageSeassionManager.getRegId())?.enqueue(
                object : Callback<RegisterModel> {

                    override fun onFailure(call: Call<RegisterModel>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<RegisterModel>, response: Response<RegisterModel>) {
                        if (response?.body() != null) {
                            if (response.code() == ConstanstParameters.API_SUCCESS_CODE) {
                                val registerModel = response.body()
                                when (registerModel?.code) {
                                    "0" -> {
                                        mSessionManager.logout()
                                        mSessionManager.setUserCode("0")
                                        mSessionManager.setUserName("")
                                        mSessionManager.setUserPassword("")
                                        act.supportFragmentManager.popBackStack("HomeFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                        startActivity(Intent(act, SplashActivity::class.java)
                                                .putExtra("com.qenaat.fragType", 1))
                                        act.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                        act.finish()
                                    }
                                }
                            }
                        }
                        mloading.putVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        /*val body = response?.body
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(body?.`in`()))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (outResponse.toInt() > 0) {
                                mSessionManager.logout()
                                mSessionManager.setUserCode("0")
                                mSessionManager.setUserName("")
                                mSessionManager.setUserPassword("")

//                        act.getFragmentManager().popBackStack(null,
//                                FragmentManager.POP_BACK_STACK_INCLUSIVE);

//                        act.getSupportFragmentManager().popBackStack("HomeFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//                        Snackbar.make(mainLayout, act.getString(R.string.LoggedOut), Snackbar.LENGTH_LONG).show();
//
//                        Bundle bundle = new Bundle();
//                        bundle.putString("comingFrom", "home");
//                        ContentActivity.openNewsFragment(bundle);
                                act.supportFragmentManager.popBackStack("HomeFragment", FragmentManager.POP_BACK_STACK_INCLUSIVE)
                                startActivity(Intent(act, SplashActivity::class.java)
                                        .putExtra("com.qenaat.fragType", 1))
                                act.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                act.finish()
                            } else if (outResponse == "-1") {
                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                            } else if (outResponse == "-2") {
                                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        dialog?.dismiss()*/
                    }
                })
    }

    companion object {
        protected val TAG = MyAccountFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: MyAccountFragment
        fun newInstance(act: FragmentActivity): MyAccountFragment {
            fragment = MyAccountFragment()
            Companion.act = act
            return fragment
        }
    }
}