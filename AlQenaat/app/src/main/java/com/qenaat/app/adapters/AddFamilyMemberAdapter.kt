package com.qenaat.app.adapters

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.fragments.RequestHelpFormFragment
import com.qenaat.app.model.RequestForHelpPersons
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 14-Jan-18.
 */
class AddFamilyMemberAdapter(var act: FragmentActivity?, private val itemsData: ArrayList<RequestForHelpPersons?>?)
    : RecyclerView.Adapter<AddFamilyMemberAdapter.ViewHolder?>() {

    var languageSeassionManager: LanguageSessionManager?
    private var birthDatePickerDialog: DatePickerDialog? = null
    private val dateFormatter1: SimpleDateFormat? = SimpleDateFormat("dd/MM/yyyy", Locale.US)

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_family_person, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData?.get(position) != null) {
            viewHolder.relative_content.setVisibility(View.VISIBLE)
            viewHolder.relative_add_more_person.setVisibility(View.GONE)
            viewHolder.linear_male.setOnClickListener(View.OnClickListener {
                viewHolder.img_male.setImageResource(R.drawable.radio_on)
                viewHolder.img_female.setImageResource(R.drawable.radio_off)

                //update data here
                if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                    if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                        Log.e("afterTextChanged", "update")
                        val addFamilyMembers: RequestForHelpPersons = RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                        addFamilyMembers.Gender = "1"
                        RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                    }
                }
            })
            viewHolder.linear_female.setOnClickListener(View.OnClickListener {
                viewHolder.img_male.setImageResource(R.drawable.radio_off)
                viewHolder.img_female.setImageResource(R.drawable.radio_on)

                //update data here
                if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                    if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                        Log.e("afterTextChanged", "update")
                        val addFamilyMembers: RequestForHelpPersons =
                                RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                        addFamilyMembers.Gender = "2"
                        RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                    }
                }
            })
            viewHolder.tv_date_of_birth.setOnClickListener(View.OnClickListener { showCalendar(viewHolder.tv_date_of_birth, position) })
            if (itemsData.get(position)?.Name != null) {
                viewHolder.et_name.setText(itemsData.get(position)?.Name)
            }
            if (itemsData.get(position)?.Gender != null) {
                if (itemsData.get(position)?.Gender.equals("1", ignoreCase = true)) {
                    viewHolder.img_male.setImageResource(R.drawable.radio_on)
                    viewHolder.img_female.setImageResource(R.drawable.radio_off)
                } else {
                    viewHolder.img_male.setImageResource(R.drawable.radio_off)
                    viewHolder.img_female.setImageResource(R.drawable.radio_on)
                }
            }
            if (itemsData.get(position)?.DateOFBirth != null) {
                viewHolder.tv_date_of_birth.setText(itemsData.get(position)?.DateOFBirth)
            }
            if (itemsData.get(position)?.CivilIdNo != null) {
                viewHolder.et_civil_id.setText(itemsData.get(position)?.CivilIdNo)
            }
            if (itemsData.get(position)?.RelationShip != null) {
                viewHolder.et_relationship.setText(itemsData.get(position)?.RelationShip)
            }
            if (itemsData.get(position)?.Notes != null) {
                viewHolder.et_notes.setText(itemsData.get(position)?.Notes)
            }
            viewHolder.img_delete.setOnClickListener(View.OnClickListener { //update data here
                if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                    if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                        AlertDialog.Builder(act!!)
                                .setTitle(act?.getString(R.string.Confirm))
                                .setMessage(act?.getString(R.string.AreYouSureYouWantToDeleteMemberLabel))
                                .setPositiveButton(android.R.string.yes) { dialog, which ->
                                    Log.e("afterTextChanged", "update")
                                    dialog.dismiss()
                                    val addFamilyMembers: RequestForHelpPersons = RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                                    RequestHelpFormFragment.Companion.familyMembersArrayList?.remove(addFamilyMembers)
                                    notifyDataSetChanged()
                                }
                                .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
                                .setIcon(R.drawable.icon_512)
                                .show()
                    }
                }
            })
            viewHolder.et_name.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    Log.e("afterTextChanged", "afterTextChanged")
                    viewHolder.et_name.requestFocus()
                    viewHolder.et_name.setSelection(viewHolder.et_name.getText().length)
                    if (viewHolder.et_name.getText().toString().equals("", ignoreCase = true)) {
                    } else {

                        //update data here
                        if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                            if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                                Log.e("afterTextChanged", "update")
                                val addFamilyMembers: RequestForHelpPersons =
                                        RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                                addFamilyMembers.Name = viewHolder.et_name.getText().toString()
                                RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                            }
                        }
                    }
                }
            })
            viewHolder.et_civil_id.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    Log.e("afterTextChanged", "afterTextChanged")
                    viewHolder.et_civil_id.requestFocus()
                    viewHolder.et_civil_id.setSelection(viewHolder.et_civil_id.getText().length)
                    if (viewHolder.et_civil_id.getText().toString().equals("", ignoreCase = true)) {
                    } else {

                        //update data here
                        if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                            if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                                Log.e("afterTextChanged", "update")
                                val addFamilyMembers: RequestForHelpPersons =
                                        RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                                addFamilyMembers.CivilIdNo = viewHolder.et_civil_id.getText().toString()
                                RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                            }
                        }
                    }
                }
            })
            viewHolder.et_relationship.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    Log.e("afterTextChanged", "afterTextChanged")
                    viewHolder.et_relationship.requestFocus()
                    viewHolder.et_relationship.setSelection(viewHolder.et_relationship.getText().length)
                    if (viewHolder.et_relationship.getText().toString().equals("", ignoreCase = true)) {
                    } else {

                        //update data here
                        if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                            if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                                Log.e("afterTextChanged", "update")
                                val addFamilyMembers: RequestForHelpPersons =
                                        RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                                addFamilyMembers.RelationShip = viewHolder.et_relationship.getText().toString()
                                RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                            }
                        }
                    }
                }
            })
            viewHolder.et_notes.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
                override fun afterTextChanged(s: Editable?) {
                    Log.e("afterTextChanged", "afterTextChanged")
                    viewHolder.et_notes.requestFocus()
                    viewHolder.et_notes.setSelection(viewHolder.et_notes.getText().length)
                    if (viewHolder.et_notes.getText().toString().equals("", ignoreCase = true)) {
                    } else {

                        //update data here
                        if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                            if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                                Log.e("afterTextChanged", "update")
                                val addFamilyMembers: RequestForHelpPersons =
                                        RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                                addFamilyMembers.Notes = viewHolder.et_notes.getText().toString()
                                RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                            }
                        }
                    }
                }
            })
            if (itemsData.get(position)?.Id.equals("-1", ignoreCase = true)) {
                viewHolder.relative_content.setVisibility(View.GONE)
                viewHolder.relative_add_more_person.setVisibility(View.VISIBLE)
            }
            viewHolder.relative_add_more_person.setOnClickListener(View.OnClickListener { RequestHelpFormFragment.Companion.addMoreMember() })

//            viewHolder.tv_price.setVisibility(View.GONE);
//
//            if(languageSeassionManager.getLang().equalsIgnoreCase("en")){
//
//                viewHolder.tv_title.setText(itemsData.get(position).getServiceTypeEn());
//
//            }
//            else{
//
//                viewHolder.tv_title.setText(itemsData.get(position).getServiceTypeAr());
//
//            }
//
//            viewHolder.relative_parent.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Gson gson = new Gson();
//
//                    Bundle b = new Bundle();
//
//                    b.putString("GetServiceTypes", gson.toJson(itemsData.get(position)));
//
//                    ContentActivity.openServiceListFragment(b);
//
//                }
//            });
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var relative_add_more_person: RelativeLayout
        var relative_content: RelativeLayout
        var relative_parent: RelativeLayout
        var relative_gender_main: RelativeLayout
        var et_name: EditText
        var et_civil_id: EditText
        var et_relationship: EditText
        var et_notes: EditText
        var img_bg: ImageView? = null
        var img_female: ImageView
        var img_male: ImageView
        var img_add: ImageView
        var img_delete: ImageView
        var linear_gender: LinearLayout
        var linear_female: LinearLayout
        var linear_male: LinearLayout
        var tv_gender: TextView
        var tv_female: TextView
        var tv_male: TextView
        var tv_date_of_birth: TextView
        var tv_add_more: TextView

        init {
            img_delete = itemLayoutView
                    .findViewById<View?>(R.id.img_delete) as ImageView
            tv_female = itemLayoutView
                    .findViewById<View?>(R.id.tv_female) as TextView
            tv_male = itemLayoutView
                    .findViewById<View?>(R.id.tv_male) as TextView
            tv_date_of_birth = itemLayoutView
                    .findViewById<View?>(R.id.tv_date_of_birth) as TextView
            tv_add_more = itemLayoutView
                    .findViewById<View?>(R.id.tv_add_more) as TextView
            tv_gender = itemLayoutView
                    .findViewById<View?>(R.id.tv_gender) as TextView
            linear_female = itemLayoutView
                    .findViewById<View?>(R.id.linear_female) as LinearLayout
            linear_male = itemLayoutView
                    .findViewById<View?>(R.id.linear_male) as LinearLayout
            linear_gender = itemLayoutView
                    .findViewById<View?>(R.id.linear_gender) as LinearLayout
            img_male = itemLayoutView
                    .findViewById<View?>(R.id.img_male) as ImageView
            img_add = itemLayoutView
                    .findViewById<View?>(R.id.img_add) as ImageView
            img_female = itemLayoutView
                    .findViewById<View?>(R.id.img_female) as ImageView
            et_civil_id = itemLayoutView
                    .findViewById<View?>(R.id.et_civil_id) as EditText
            et_relationship = itemLayoutView
                    .findViewById<View?>(R.id.et_relationship) as EditText
            et_notes = itemLayoutView
                    .findViewById<View?>(R.id.et_notes) as EditText
            et_name = itemLayoutView
                    .findViewById<View?>(R.id.et_name) as EditText
            relative_gender_main = itemLayoutView
                    .findViewById<View?>(R.id.relative_gender_main) as RelativeLayout
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_add_more_person = itemLayoutView
                    .findViewById<View?>(R.id.relative_add_more_person) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_parent)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData?.size!!
    }

    fun showCalendar(tv_dob: TextView?, position: Int) {
        val newCalendar = Calendar.getInstance()
        birthDatePickerDialog = DatePickerDialog(act!!, OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            val newDate = Calendar.getInstance()
            newDate[year, monthOfYear] = dayOfMonth
            if (newDate.timeInMillis > Date().time) {
            } else {
                val birth_dateValue = dateFormatter1?.format(newDate.time)
                tv_dob?.setText(birth_dateValue)

                //update data here
                if (RequestHelpFormFragment.Companion.familyMembersArrayList?.size!! > position) {
                    if (RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position) != null) {
                        Log.e("afterTextChanged", "update")
                        val addFamilyMembers: RequestForHelpPersons = RequestHelpFormFragment.Companion.familyMembersArrayList?.get(position)!!
                        addFamilyMembers.DateOFBirth = tv_dob?.getText().toString()
                        RequestHelpFormFragment.Companion.familyMembersArrayList?.set(position, addFamilyMembers)
                    }
                }
            }
        }, newCalendar[Calendar.YEAR], newCalendar[Calendar.MONTH], newCalendar[Calendar.DAY_OF_MONTH])
        birthDatePickerDialog?.getDatePicker()?.maxDate = System.currentTimeMillis()
        birthDatePickerDialog?.show()
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}