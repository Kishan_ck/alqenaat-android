package com.qenaat.app.fragments

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.NewsAdapter
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetCommercialAds
import com.qenaat.app.model.GetEvents
import com.qenaat.app.model.GetNews
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.service_list.*
import okhttp3.internal.notify
import okhttp3.internal.notifyAll
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 31-Oct-17.
 */
class NewsFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var getNewsArrayList: ArrayList<GetNews> = ArrayList()
    var getContentsArrayList: ArrayList<GetEvents> = ArrayList()
    var comingFrom: String = ""
    lateinit var img_ad: ImageView
    var ForHomeSlider = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                comingFrom = arguments?.getString("comingFrom")!!
            }
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        img_ad = mainLayout.findViewById<View?>(R.id.img_ad) as ImageView
        my_recycler_view = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById<View?>(R.id.loading_progress) as ProgressBar
        progress_loading_more =
            mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
        val relativeParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        relativeParams.setMargins(0, 0, 0, 0)
        relativeParams.addRule(RelativeLayout.ABOVE, R.id.img_ad)
        relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP)
        my_recycler_view.setLayoutParams(relativeParams)
    }

    override fun onStart() {
        super.onStart()
        ForHomeSlider = true
        if (comingFrom.equals("news", ignoreCase = true)) {
            ForHomeSlider = false
        }
        ContentActivity.Companion.img_topAddAd.putVisibility(View.VISIBLE)
        if (comingFrom.equals("news", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.NewsLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)
        } else {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.app_name)
            ContentActivity.Companion.img_topAddAd.putVisibility(View.GONE)

            ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)
        }
        ContentActivity.Companion.img_topHall.putVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            if (mSessionManager?.isLoggedin()!!
                && mSessionManager?.getUserCode() !== "" && mSessionManager?.getUserCode() != null
            ) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                bundle.putString("isEdit", "0")
                bundle.putString("contentType", "1")
                ContentActivity.Companion.openAddNewsFragment(bundle)
            } else {
                mainLayout.showSnakeBar(act?.getString(R.string.LoggedIn)!!)
                ContentActivity.Companion.openLoginFragment()
            }
        })
        if (getNewsArrayList.size > 0) {
            mAdapter = NewsAdapter(act!!, getNewsArrayList, getContentsArrayList, comingFrom)
            my_recycler_view.setAdapter(mAdapter)
        } else {
            GetEvents()
        }
        if (comingFrom.equals("home", ignoreCase = true)) {
            GetRandomAd()
        }
    }

    fun GetNews() {
        page_index = 0
        endOfREsults = false
        loading_flag = true
        pastVisiblesItems = 0
        visibleItemCount = 0
        totalItemCount = 0

        Log.d("GetNews", "GetNews")
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)

        QenaatAPICall.getCallingAPIInterface()?.GetNews(
            "0",
            if (ForHomeSlider) "true" else "false", "" + page_index
        )?.enqueue(
            object : Callback<ArrayList<GetNews>?> {

                override fun onFailure(call: Call<ArrayList<GetNews>?>, t: Throwable) {
                    t.printStackTrace()
                    Log.d("GetNewss=", "failureeeee")
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetNews>?>,
                    response: Response<ArrayList<GetNews>?>
                ) {
                    if (response.body() != null) {
                        val getNews = response.body()
                        Log.d("GetNewss=", getNews.toString() + "")
                        if (mloading != null && getNews != null) {
                            Log.d("getContentses size", "" + getNews.size)
                            getNewsArrayList.clear()
                            getNewsArrayList.addAll(getNews)
                            if (getNews.size == 0)
                                tv_noDataFound?.putVisibility(View.VISIBLE)
                            mAdapter = NewsAdapter(
                                act!!,
                                getNewsArrayList,
                                getContentsArrayList,
                                comingFrom
                            )
                            my_recycler_view.setAdapter(mAdapter)
                            my_recycler_view.addOnScrollListener(object :
                                RecyclerView.OnScrollListener() {
                                override fun onScrolled(
                                    recyclerView: RecyclerView,
                                    dx: Int,
                                    dy: Int
                                ) {
                                    if (!endOfREsults) {
                                        visibleItemCount = mLayoutManager.getChildCount()
                                        totalItemCount = mLayoutManager.getItemCount()
                                        pastVisiblesItems =
                                            mLayoutManager.findFirstVisibleItemPosition()
                                        if (loading_flag) {
                                            if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                if (getNewsArrayList.size != 0) {
                                                    progress_loading_more.setVisibility(View.VISIBLE)
                                                    //GlobalFunctions.DisableLayout(mainLayout);
                                                }
                                                loading_flag = false
                                                page_index = page_index + 1
                                                GetMoreNews()
                                            }
                                        }
                                    }
                                }
                            })
                        } else {
                            Log.d("newsfragment", "no data for news")
                        }
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                }
            })
    }

    fun GetMoreNews() {
        QenaatAPICall.getCallingAPIInterface()?.GetNews(
            "0", if (ForHomeSlider) "true" else "false",
            "" + page_index
        )?.enqueue(
            object : Callback<ArrayList<GetNews>?> {

                override fun onFailure(call: Call<ArrayList<GetNews>?>, t: Throwable) {
                    t.printStackTrace()
                    progress_loading_more.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetNews>?>,
                    response: Response<ArrayList<GetNews>?>
                ) {
                    if (response.body() != null) {
                        val getNews = response.body()
                        if (progress_loading_more != null) {
                            progress_loading_more.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                            if (getNews != null) {
                                if (getNews.isEmpty()) {
                                    endOfREsults = true
                                    page_index = page_index - 1
                                }
                                loading_flag = true
                                getNewsArrayList.addAll(getNews)
                                mAdapter.notifyDataSetChanged()
                            }
                        }
                    }
                }
            })
    }

    fun GetEvents() {
        Log.d("GetEvents", "GetEvents-NewsFragment")
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetEvents(
            "0", "false",
            "true", "0", "0",
            mSessionManager?.getUserCode(), "-1"
        )?.enqueue(
            object : Callback<ArrayList<GetEvents>?> {

                override fun onFailure(call: Call<ArrayList<GetEvents>?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetEvents>?>,
                    response: Response<ArrayList<GetEvents>?>
                ) {
                    if (response.body() != null) {
                        val getContentses = response.body()
                        if (mloading != null) {
                            if (getContentses != null) {
                                Log.d("newsfragment", "data")
                                Log.d("GetContents activity", "" + getContentses.size)
                                getContentsArrayList.clear()
                                getContentsArrayList.addAll(getContentses)
                                Log.e("Jannat....", "" + getContentsArrayList.size)
                                for (data in getContentsArrayList) {
                                    Log.e("Jannat....11111", "" + data.EventNameEn);
                                    Log.e("Jannat....22222", "" + data.Phone);
                                }
                                GetNews()
                            } else {
                                tv_noDataFound?.putVisibility(View.VISIBLE)
                                Log.d("newsfragment", "no data")
                            }
                        }
                    }
                }
            })
    }

    fun GetRandomAd() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetBottomRandomAd("1")?.enqueue(
            object : Callback<ArrayList<GetCommercialAds?>?> {

                override fun onFailure(call: Call<ArrayList<GetCommercialAds?>?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetCommercialAds?>?>,
                    response: Response<ArrayList<GetCommercialAds?>?>
                ) {
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    if (response.body() != null) {
                        val bottomRandomAds = response.body()
                        if (bottomRandomAds != null) {
                            if (bottomRandomAds.size > 0) {
                                img_ad.setVisibility(View.VISIBLE)
                                img_ad.getLayoutParams().width = (act?.getResources()?.getDrawable(
                                    R.drawable.ad
                                ) as BitmapDrawable).bitmap.width
                                img_ad.getLayoutParams().height =
                                    (act?.getResources()!!.getDrawable(
                                        R.drawable.ad
                                    ) as BitmapDrawable).bitmap.height
                                if (bottomRandomAds[0]!!.Photo!!.length > 0) Picasso.with(act)
                                    .load(bottomRandomAds[0]!!.Photo)
                                    .error(R.drawable.ad)
                                    .placeholder(R.drawable.ad)
                                    .config(Bitmap.Config.RGB_565).fit()
                                    .into(img_ad)
                                img_ad.setOnClickListener(View.OnClickListener {
                                    if (bottomRandomAds[0]!!.Link!!.length > 0) {
                                        if (bottomRandomAds[0]!!.Link!!.contains("http")) {
                                            act?.startActivity(
                                                Intent(
                                                    Intent.ACTION_VIEW,
                                                    Uri.parse(bottomRandomAds[0]!!.Link)
                                                )
                                            )
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            })
    }

    companion object {
        protected val TAG = NewsFragment::class.java.simpleName
        var mSessionManager: SessionManager? = null
        var languageSeassionManager: LanguageSessionManager? = null
        var act: FragmentActivity? = null
        var fragment: NewsFragment? = null
        fun newInstance(act: FragmentActivity?): NewsFragment? {
            fragment = NewsFragment()
            Companion.act = act
            return fragment
        }
    }
}