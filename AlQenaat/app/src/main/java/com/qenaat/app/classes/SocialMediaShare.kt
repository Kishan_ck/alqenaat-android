package com.qenaat.app.classes

import android.content.ActivityNotFoundException
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl.showToast
import java.util.*

object SocialMediaShare {

    var shareContext: Context? = null
    var imgUri: Uri? = null
    var appName: String? = null
    var content: String? = null
    var resInfo: List<ResolveInfo>? = null

    //Facebook
    private fun shareFacebook() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "image/*"
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imgUri)
            /*shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, content)*/
            var isInstall = false
            for (info in resInfo!!) {
                if (info.activityInfo.packageName.toLowerCase(Locale.ENGLISH).contains(appName!!)
                        || info.activityInfo.name.toLowerCase(Locale.ENGLISH).contains(appName!!)) {
                    isInstall = true
                    shareIntent.setPackage(info.activityInfo.packageName)
                    shareContext?.startActivity(shareIntent)
                    break
                } else isInstall = false
            }
            if (!isInstall)
                showToast(shareContext, shareContext?.getString(R.string.facebook_not_install))
        } catch (e: Exception) {
            e.printStackTrace()
            showToast(shareContext, shareContext?.getString(R.string.facebook_not_install))
        }
    }

    //WhatsApp
    private fun shareWhatsApp() {
        try {
            val targetedShare = Intent(Intent.ACTION_SEND)
            targetedShare.putExtra(Intent.EXTRA_TEXT, content)
            targetedShare.type = "text/plain"
            targetedShare.putExtra(Intent.EXTRA_STREAM, imgUri)
            targetedShare.type = "image/*"
            targetedShare.setPackage("com.whatsapp")
            shareContext?.startActivity(targetedShare)
        } catch (e: Exception) {
            e.printStackTrace()
            showToast(shareContext, shareContext?.getString(R.string.whatsapp_not_install))
        }
    }

    //G-mail
    private fun shareGMail() {
        try {
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, shareContext?.getString(R.string.app_name))
            emailIntent.putExtra(Intent.EXTRA_TEXT, content)
            emailIntent.type = "image/*"
            emailIntent.putExtra(Intent.EXTRA_STREAM, imgUri)
            for (app in resInfo!!) {
                if (app.activityInfo.name.toLowerCase(Locale.ENGLISH).contains(appName!!)) {
                    val activity = app.activityInfo
                    val name = ComponentName(activity.applicationInfo.packageName, activity.name)
                    emailIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                    emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                    emailIntent.component = name
                    shareContext?.startActivity(emailIntent)
                    break
                }
            }
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            showToast(shareContext, shareContext?.getString(R.string.email_not_install))
        }
    }

    //SMS
    private fun shareSMS() {
        try {
            val smsIntent = Intent(Intent.ACTION_VIEW)
            smsIntent.data = Uri.parse("smsto:")
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra(Intent.EXTRA_SUBJECT, """${shareContext?.getString(R.string.app_name)} $content """.trimIndent())
            smsIntent.putExtra("sms_body", content)
            shareContext?.startActivity(smsIntent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            showToast(shareContext, shareContext?.getString(R.string.sms_not_install))
        }
    }

    //Twitter
    private fun shareTwitter() {
        try {
            val targetedShare = Intent(Intent.ACTION_SEND)
            targetedShare.type = "image/*"
            targetedShare.putExtra(Intent.EXTRA_STREAM, imgUri)
            targetedShare.type = "text/plain"
            targetedShare.putExtra(Intent.EXTRA_TEXT, content)
            targetedShare.setPackage("com.twitter.android")
            shareContext?.startActivity(targetedShare)
        } catch (e: Exception) {
            e.printStackTrace()
            showToast(shareContext, shareContext?.getString(R.string.twitter_not_install))
        }
    }

    //Instagram
    private fun shareInstagram() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "image/*"
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            shareIntent.putExtra(Intent.EXTRA_STREAM, imgUri)
            //shareIntent.type = "text/plain"
            //shareIntent.putExtra(Intent.EXTRA_TEXT, content)
            shareIntent.setPackage("com.instagram.android")
            shareContext?.startActivity(shareIntent)
        } catch (e: Exception) {
            e.printStackTrace()
            showToast(shareContext, shareContext?.getString(R.string.instagram_not_install))
        }
    }

    fun shareData(shareType: Int, app_Name: String?, contentText: String?) {
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        val resInfoList = shareContext?.packageManager?.queryIntentActivities(share, 0)

        appName = app_Name
        content = contentText
        resInfo = resInfoList

        if (resInfo?.isNotEmpty()!!) {
            when (shareType) {
                // Facebook
                1 -> shareFacebook()
                // WhatsApp
                2 -> shareWhatsApp()
                // G-mail
                3 -> shareGMail()
                //SMS
                4 -> shareSMS()
                // Twitter
                5 -> shareTwitter()
                // Instagram
                6 -> shareInstagram()
            }
        }
    }
}