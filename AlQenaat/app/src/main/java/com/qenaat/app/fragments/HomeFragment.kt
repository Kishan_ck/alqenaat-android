package com.qenaat.app.fragments

import android.graphics.Bitmap
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.ContentActivity.Companion.img_topProfile
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.holder.SelectableHeaderHolder1
import com.qenaat.app.model.GetFamilyTree
import com.qenaat.app.model.GetUser
import com.qenaat.app.model.IconTreeItem
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import com.unnamed.b.atv.model.TreeNode
import com.unnamed.b.atv.view.AndroidTreeView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by Musha on 9/1/2015.
 */
class HomeFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mSessionManager: SessionManager
    lateinit var languageSessionManager: LanguageSessionManager
    private var savedInstanceState: Bundle? = null
    lateinit var img_add_tree: ImageView
    lateinit var root: TreeNode
    var treeNodeArrayList: ArrayList<TreeNode> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = act
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSessionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.home, null) as RelativeLayout
            this.savedInstanceState = savedInstanceState!!
        } catch (e: Exception) {
//            Log.e(TAG + " " + " onCreate>>LineNumber: "
//                            + Thread.currentThread().getStackTrace()[2].getLineNumber(),
//                    e.getMessage());
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            containerView = mainLayout.findViewById(R.id.container) as ViewGroup
            img_add_tree = mainLayout.findViewById(R.id.img_add_tree) as ImageView
            img_add_tree.setOnClickListener(this)
            mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        }
    }

    override fun onStart() {
        super.onStart()
//        if (mSessionManager.isLoggedin()
//                && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
//        } else {
//            act?.getSupportFragmentManager()?.popBackStack()
//        }
        ContentActivity.Companion.mtv_topTitle.setText(R.string.FamilyTreeLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.tabType = 1
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSessionManager)


//        TreeNode root = TreeNode.root();
//
//        IconTreeItem iconTreeItem = new IconTreeItem();
//
//        iconTreeItem.setNameAR("يجب اولا الموافقة على الشروط و الاحكام يجب اولا الموافقة على الشروط و الاحكام");
//        iconTreeItem.setBirthYearFrom("1950");
//        iconTreeItem.setBirthYearTo("2001");
//
//        TreeNode s1 = new TreeNode(iconTreeItem).setViewHolder(new SelectableHeaderHolder1(act));
//
//        iconTreeItem = new IconTreeItem();
//
//        iconTreeItem.setNameAR("هل أنت متأكد انك تريد حذف هذا المنتج هل أنت متأكد انك تريد حذف هذا المنتج");
//        iconTreeItem.setBirthYearFrom("1976");
//        iconTreeItem.setBirthYearTo("1995");
//
//
//        TreeNode s2 = new TreeNode(iconTreeItem).setViewHolder(new SelectableHeaderHolder1(act));
//
//        fillFolder(s1);
//        fillFolder(s2);
//
//        root.addChildren(s1, s2);
//
//        tView = new AndroidTreeView(act, root);
//        tView.setDefaultAnimation(true);
//        tView.setUse2dScroll(true);
//        tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom);
//
//        Display mdisp = act.getWindowManager().getDefaultDisplay();
//        Point mdispSize = new Point();
//        mdisp.getSize(mdispSize);
//        int maxX = mdispSize.x;
//        int maxY = mdispSize.y;
//
//        containerView.addView(tView.getView(maxX, 0));
//
//        tView.expandAll();
        if (arguments != null) {
            comingFrom = arguments!!.getString("comingFrom")!!
        }
        img_add_tree.setVisibility(View.GONE)

        if (getCategoriesArrayList?.size!! > 0) {
            Log.d("onSaveInstanceState 0", "onSaveInstanceState")
            Log.d("onSaveInstanceState 1", "" + tView!!.getSaveState())
            displayTree(getCategoriesArrayList)
            var restorePath: String? = ""
            restorePath = tView!!.getSaveState()
            root = TreeNode.root()
            val treeNodeArrayList = ArrayList<TreeNode?>()

            // createTree(getCategoriesArrayList);
            for (i in getCategoriesArrayList!!.indices) {
                val getCategories = getCategoriesArrayList!!.get(i)
                val s1 = TreeNode(getCategories).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
                fillParent(s1, getCategories)
                treeNodeArrayList.add(s1)
            }
            root.addChildren(treeNodeArrayList)
            tView = AndroidTreeView(act, root)
            tView.setDefaultAnimation(true)
            tView.setUse2dScroll(true)
            tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom)
            val mdisp = act.getWindowManager().defaultDisplay
            val mdispSize = Point()
            mdisp.getSize(mdispSize)
            val maxX = mdispSize.x
            val maxY = mdispSize.y
            containerView.removeAllViews()
            containerView.addView(tView.getView(maxX, 0))

            //tView.expandAll();
            if (!TextUtils.isEmpty(restorePath)) {
                tView.restoreState(restorePath)
            }


//            if (savedInstanceState != null) {
//                String state = savedInstanceState.getString("tState");
//                if (!TextUtils.isEmpty(state)) {
//                    tView.restoreState(state);
//                }
//            }

            //tView.expandAll();
        } else {
            getCategories()
        }


        if (comingFrom.equals("add", ignoreCase = true)) {
            img_add_tree.setVisibility(View.GONE)
        }else {
            GetUser()
        }

    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.img_add_tree -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                mSessionManager.setImagePath("")
                ContentActivity.Companion.parentNameAr = ""
                ContentActivity.Companion.parentNameEn = ""
                ContentActivity.Companion.parentId = ""
                ContentActivity.Companion.openAddFamilyTreeFragment()
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
        }
    }

    private fun fillFolder(folder: TreeNode?) {
        var currentNode = folder
        for (i in 0..4) {

            //TreeNode root = TreeNode.root();
            val iconTreeItem = IconTreeItem()
            iconTreeItem.NameAr = "تم اضافة المناسبة للتذكير تم اضافة المناسبة للتذكير"
            iconTreeItem.BirthYearFrom = "1989"
            iconTreeItem.BirthYearTo = "Present"
            val file = TreeNode(iconTreeItem).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
            currentNode!!.addChild(file)
            currentNode = file
        }
    }

    //    @Override
    //    public void onSaveInstanceState(Bundle outState) {
    //        super.onSaveInstanceState(outState);
    //        outState.putString("tState", tView.getSaveState());
    //    }

    /*private fun uploadPhoto(fileName: String?, FileObjectType: String?) {
        QenaatAPICall.getCallingAPIInterface()?.uploadFile(fileName, FileObjectType, object : Callback<String?> {
            override fun success(s: String?, response: Response?) {
                Log.e("Upload response", "" + s)
            }

            override fun failure(error: RetrofitError?) {
                Log.e("Upload", "error")
                //goUploadNextVideo();
            }
        })
    }*/

    fun getCategories() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetFamilyTree("0")?.enqueue(
                object : Callback<ArrayList<GetFamilyTree?>?> {

                    override fun onFailure(call: Call<ArrayList<GetFamilyTree?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetFamilyTree?>?>, response: Response<ArrayList<GetFamilyTree?>?>) {
                        getCategoriesArrayList.clear()
                        if (response.body() != null) {
                            val categories = response.body()
                            if (mloading != null) {
                                if (categories != null) {
                                    getCategoriesArrayList.addAll(categories)
                                }
                                val treeNodeArrayList = ArrayList<TreeNode?>()
                                if (getCategoriesArrayList.size > 0) {
                                    root = TreeNode.root()
                                    for (i in getCategoriesArrayList.indices) {
                                        val getCategories = getCategoriesArrayList.get(i)
                                        val s1 = TreeNode(getCategories).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
                                        fillParent(s1, getCategories)
                                        treeNodeArrayList.add(s1)
                                    }
                                    root.addChildren(treeNodeArrayList)
                                    tView = AndroidTreeView(act, root)
                                    tView.setDefaultAnimation(true)
                                    tView.setUse2dScroll(true)
                                    tView.setDefaultContainerStyle(R.style.TreeNodeStyleCustom)
                                    val mdisp = act.getWindowManager().defaultDisplay
                                    val mdispSize = Point()
                                    mdisp.getSize(mdispSize)
                                    val maxX = mdispSize.x
                                    val maxY = mdispSize.y
                                    containerView.removeAllViews()
                                    containerView.addView(tView.getView(maxX, 0))

                                    //tView.expandAll();
                                    if (savedInstanceState != null) {
                                        val state = savedInstanceState?.getString("tState")
                                        if (!TextUtils.isEmpty(state)) {
                                            tView.restoreState(state)
                                        }
                                    }

                                    //tView.expandAll();
                                }
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }
                        }
                    }
                })
    }

    private fun createTree(actualFamilyTreeArrayList: ArrayList<GetFamilyTree?>?) {
        for (i in actualFamilyTreeArrayList?.indices!!) {
            val tree = actualFamilyTreeArrayList.get(i)
            Log.d("displayTree ", "searchParentId " + tree!!.Id)
            val currentNode = TreeNode(tree).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
            if (tree.Childs != null) {
                for (j in tree.Childs!!.indices) {

                    //TreeNode root = TreeNode.root();
                    val categories = tree.Childs!![j]
                    val file = TreeNode(categories).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
                    currentNode.addChild(file)
                    //currentNode = file;
                    createTree(categories!!.Childs)
                }
            }
            treeNodeArrayList.add(currentNode)
        }
    }

    private fun GetUser() {
        mloading.setVisibility(View.VISIBLE)

        Log.e("userid>>>>",""+mSessionManager.getUserCode())


        QenaatAPICall.getCallingAPIInterface()?.GetUser(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                mSessionManager.getUserCode())?.enqueue(
                object : Callback<ArrayList<GetUser?>?> {

                    override fun onFailure(call: Call<ArrayList<GetUser?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetUser?>?>, response: Response<ArrayList<GetUser?>?>) {
                        //ContentActivity.Companion.img_topProfile.setVisibility(View.VISIBLE)
                        ContentActivity.Companion.img_topProfile.setImageResource(R.drawable.user_who)
                        if (response.body() != null) {
                            val getUsers = response.body()
                            if (getUsers != null) {
                                if (getUsers.size > 0) {
                                    if (getUsers[0]!!.IsIdentified != null) {
                                        if (getUsers[0]!!.IsIdentified.equals("true", ignoreCase = true)) {

                                            //ContentActivity.img_topProfile.setVisibility(View.VISIBLE);
                                            ContentActivity.Companion.img_topProfile.getLayoutParams().width = (act.getResources().getDrawable(
                                                    R.drawable.user_who) as BitmapDrawable).bitmap.width
                                            ContentActivity.Companion.img_topProfile.getLayoutParams().height = (act.getResources().getDrawable(
                                                    R.drawable.user_who) as BitmapDrawable).bitmap.height
                                            if (getUsers[0]!!.Photo!!.isNotEmpty()) Picasso.with(act)
                                                    .load(getUsers[0]!!.Photo)
                                                    .error(R.drawable.user_who)
                                                    .placeholder(R.drawable.user_who)
                                                    .config(Bitmap.Config.RGB_565).fit()
                                                    .into(ContentActivity.Companion.img_topProfile)
                                        } else {
//                                            img_topProfile.setVisibility(View.VISIBLE)
                                            img_topProfile.setVisibility(View.GONE)
                                            ContentActivity.Companion.img_topProfile.setOnClickListener(View.OnClickListener {

                                                if (ContentActivity.mSessionManager?.isLoggedin()!!
                                                    && ContentActivity.mSessionManager?.getUserCode() !== "" && ContentActivity.mSessionManager?.getUserCode() != null
                                                ) {
                                                    ContentActivity.Companion.openIdentifyUserFragment()
                                                } else {
                                                    Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                                                    ContentActivity.openLoginFragment()
                                                }
                                            })
                                        }
                                    }
                                }
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                    }
                })
    }

    companion object {
        protected val TAG = HomeFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: HomeFragment
        lateinit var mainLayout: RelativeLayout
        lateinit var containerView: ViewGroup
        private lateinit var tView: AndroidTreeView
        lateinit var mloading: ProgressBar
        private val NAME: String = "Very long name for folder"
        var getCategoriesArrayList: ArrayList<GetFamilyTree?> = ArrayList()
        var comingFrom: String = ""
        var treeArrayList: ArrayList<GetFamilyTree?> = ArrayList()
        fun newInstance(act: FragmentActivity): HomeFragment? {
            fragment = HomeFragment()
            Companion.act = act
            return fragment
        }

        private fun fillParent(folder: TreeNode?, getCategories: GetFamilyTree?) {
            if (getCategories!!.Childs != null) {
                for (i in getCategories.Childs!!.indices) {

                    //TreeNode root = TreeNode.root();
                    val categories = getCategories.Childs!![i]
                    val file = TreeNode(categories).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
                    folder!!.addChildren(file)
                    //currentNode = file;
                    fillParent(file, categories)
                }
            }
        }

        fun getSubCategories(getCategories: GetFamilyTree?, parentNode: TreeNode?, img_arrow: ImageView?) {
            mloading?.setVisibility(View.VISIBLE)
            GlobalFunctions.DisableLayout(mainLayout)
            QenaatAPICall.getCallingAPIInterface()?.GetFamilyTree(
                    getCategories!!.Id)?.enqueue(
                    object : Callback<ArrayList<GetFamilyTree?>?> {

                        override fun onFailure(call: Call<ArrayList<GetFamilyTree?>?>, t: Throwable) {
                            t.printStackTrace()
                            mloading?.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ArrayList<GetFamilyTree?>?>, response: Response<ArrayList<GetFamilyTree?>?>) {
                            if (response.body() != null) {
                                val subCategories = response.body()
                                if (mloading != null) {
                                    if (subCategories != null) {

                                        //getCategoriesArrayList.addAll(categories);
                                        treeArrayList?.clear()
                                        addNodeInTree(getCategories, subCategories, getCategoriesArrayList)
                                        displayTree(getCategoriesArrayList)

                                        //getCategoriesArrayList.clear();

                                        //getCategoriesArrayList.addAll(treeArrayList);
                                        Log.d("getPath", "" + parentNode?.getPath())
                                        val isSearchFound = false

//                        for(int i=0; i<getCategoriesArrayList.size(); i++){
//
//                            GetFamilyTree getCategories1 = getCategoriesArrayList.get(i);
//
//                            Log.d("GetFamilyTree 0 ", ""+getCategoriesArrayList.get(i).getChilds().size());
//
//                            if(getCategories1.getRequestForHelpId().equalsIgnoreCase(getCategories.getRequestForHelpId())){
//
//                                isSearchFound=true;
//
//                                Log.d("GetFamilyTree 1 ", ""+getCategoriesArrayList.get(i).getChilds().size());
//
//                                getCategories1.setChilds(subCategories);
//
//                                getCategoriesArrayList.set(i, getCategories1);
//
//                                Log.d("GetFamilyTree 2 ", ""+getCategoriesArrayList.get(i).getChilds().size());
//
//                            }
//
//                            if(isSearchFound==false){
//
//                                for (int j=0; j<getCategories1.getChilds().size(); j++){
//
//                                    GetFamilyTree getCategories2 = getCategoriesArrayList.get(i);
//
//                                }
//
//                            }
//
//                        }
                                    }
                                    if (subCategories!!.size > 0) {
                                        val root = TreeNode.root()
                                        for (i in subCategories.indices) {
                                            val getCategories = subCategories.get(i)
                                            val s1 = TreeNode(getCategories).setViewHolder(SelectableHeaderHolder1(act, comingFrom))
                                            tView?.addNode(parentNode, s1)
                                        }
                                        tView?.expandNode(parentNode)

                                        // tView.expandAll();
                                    }else if(subCategories.size == 0){
                                        Snackbar.make(mainLayout, act.getString(R.string.inactive_member), Snackbar.LENGTH_LONG).show()
                                    }
                                    if (subCategories.size > 0) {
                                        img_arrow?.setImageResource(R.drawable.arrow_down)
                                    } else {
                                        img_arrow?.setImageResource(R.drawable.arrow_left)
                                    }
                                    mloading!!.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            }
                        }
                    })
        }

        private fun addNodeInTree(parentFamilyTree: GetFamilyTree?, childFamilyTree: ArrayList<GetFamilyTree?>?, actualFamilyTreeArrayList: ArrayList<GetFamilyTree?>?) {
            for (i in actualFamilyTreeArrayList?.indices!!) {
                val tree = actualFamilyTreeArrayList.get(i)
                Log.d("addNodeInTree ", "parentId " + parentFamilyTree!!.Id + " --> " + "searchParentId " + tree!!.Id)
                if (tree.Id.equals(parentFamilyTree.Id, ignoreCase = true)) {
                    Log.d("addNodeInTree found ", "parentId " + parentFamilyTree.Id + " --> " + "searchParentId " + tree.Id)
                    tree.Childs = childFamilyTree

//                return;
                } else {
                    Log.d("addNodeInTree not found", "parentId " + parentFamilyTree.Id + " --> " + "searchParentId " + tree.Id)
                    addNodeInTree(parentFamilyTree, childFamilyTree, tree.Childs)
                }
            }
        }

        private fun displayTree(actualFamilyTreeArrayList: ArrayList<GetFamilyTree?>?) {
            for (i in actualFamilyTreeArrayList?.indices!!) {
                val tree = actualFamilyTreeArrayList.get(i)
                Log.d("displayTree ", "searchParentId " + tree!!.Id)
                displayTree(tree.Childs)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        img_topProfile.putVisibility(View.GONE)
    }

    override fun onPause() {
        super.onPause()

        img_topProfile.putVisibility(View.GONE)
    }
}