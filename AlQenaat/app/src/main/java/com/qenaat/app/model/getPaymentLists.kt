package com.qenaat.app.model

data class GetPaymentLists(
	val list: ArrayList<ListItem>? = null
){
	data class ListItem(
		val PaymentMethodId: String? = null,
		val InsertDate: Any? = null,
		val DonationNumber: String? = null,
		val Projectid: Any? = null,
		val MonthlyDonationSubscriptionId: Any? = null,
		val DonnationSubCatId: Any? = null,
		val TranId: Any? = null,
		val CurrencyId: String? = null,
		val Result: Any? = null,
		val DonnationValue: String? = null,
		val Describtion: String? = null,
		val Auth: Any? = null,
		val SubCatName: Any? = null,
		val RefId: Any? = null,
		val SubCatNameAR: Any? = null,
		val PaidOn: Any? = null,
		val MonthlyDonationSubscriptionInvoiceId: Any? = null,
		val ZakatTypeId: String? = null,
		val CatNameAR: String? = null,
		val Amount: Any? = null,
		val PaymentId: Any? = null,
		val Title: String? = null,
		val DonnationCatId: String? = null,
		val CatName: String? = null,
		val DonationId: Any? = null,
		val UserId: String? = null,
		val PaymentMethod: Any? = null,
		val IsPaid: String? = null,
		val Id: Any? = null,
		val ChargeId: Any? = null,
		val TrackId: Any? = null
	)
}




