package com.qenaat.app.classes

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.qenaat.app.classes.SessionManager


/**
 * Created by webuser1 on 6/14/2015.
 */
class SessionManager @SuppressLint("CommitPrefEdits") constructor(var _context: Context) {
    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    var PRIVATE_MODE = 0
    fun LoginSeassion() {
        editor.putBoolean(SessionManager.Companion.IS_LOGGED, true)
        editor.commit()
    }

    fun isLoggedin(): Boolean {
        return pref.getBoolean(SessionManager.Companion.IS_LOGGED, false)
    }

    fun logout() {
        editor.putBoolean(SessionManager.Companion.IS_LOGGED, false)
        editor.commit()
    }

    fun setUserCode(code: String?) {
        editor.putString(SessionManager.Companion.UserCode, code)
        editor.commit()
    }

    fun getUserCode(): String? {
        return pref.getString(SessionManager.Companion.UserCode, "0")
    }

    fun setUserAddress(address: String?) {
        editor.putString(SessionManager.Companion.UserAddress, address)
        editor.commit()
    }

    fun getUserAddress(): String? {
        return pref.getString(SessionManager.Companion.UserAddress, "")
    }

    fun setUserName(name: String?) {
        editor.putString(SessionManager.Companion.UserName, name)
        editor.commit()
    }

    fun getUserName(): String? {
        return pref.getString(SessionManager.Companion.UserName, "")
    }

    fun getUserPassword(): String? {
        return pref.getString(SessionManager.Companion.UserPassword, "")
    }

    fun setUserPassword(password: String?) {
        editor.putString(SessionManager.Companion.UserPassword, password)
        editor.commit()
    }

    fun getUserMobile(): String? {
        return pref.getString(SessionManager.Companion.UserMobile, "")
    }

    fun setUserMobile(mob: String?) {
        editor.putString(SessionManager.Companion.UserMobile, mob)
        editor.commit()
    }

    fun setUserEmail(email: String?) {
        editor.putString(SessionManager.Companion.UserEmail, email)
        editor.commit()
    }

    fun getUserEmail(): String? {
        return pref.getString(SessionManager.Companion.UserEmail, "")
    }

    fun setUserCountryId(id: String?) {
        editor.putString(SessionManager.Companion.UserCountryId, id)
        editor.commit()
    }

    fun getUserCountryId(): String? {
        return pref.getString(SessionManager.Companion.UserCountryId, "")
    }

    fun setAuthToken(id: String?) {
        editor.putString(AuthToken, id)
        editor.commit()
    }

    fun getAuthToken(): String? {
        return pref.getString(AuthToken, "")
    }

    fun setIsCalendarPopupShownBefore(isCalendarPopupShownBefore: Boolean) {
        editor.putBoolean(SessionManager.Companion.IsCalendarPopupShownBefore, isCalendarPopupShownBefore)
        editor.commit()
    }

    fun getIsCalendarPopupShownBefore(): Boolean {
        return pref.getBoolean(SessionManager.Companion.IsCalendarPopupShownBefore, false)
    }

    fun WhatsAppOpened() {
        editor.putBoolean(SessionManager.Companion.IS_WhatsApp_OpenBefore, true)
        editor.commit()
    }

    fun FacebookOpened() {
        editor.putBoolean(SessionManager.Companion.IS_Facebook_OpenBefore, true)
        editor.commit()
    }

    fun InstagramOpened() {
        editor.putBoolean(SessionManager.Companion.IS_Instagram_OpenBefore, true)
        editor.commit()
    }

    fun isWhatsAppOpenBefore(): Boolean {
        return pref.getBoolean(SessionManager.Companion.IS_WhatsApp_OpenBefore, false)
    }

    fun isFacebookOpenBefore(): Boolean {
        return pref.getBoolean(SessionManager.Companion.IS_Facebook_OpenBefore, false)
    }

    fun isInstagramOpenBefore(): Boolean {
        return pref.getBoolean(SessionManager.Companion.IS_Instagram_OpenBefore, false)
    }

    fun setImagePath(imagePaths: String?) {
        editor.putString(SessionManager.Companion.imagePath, imagePaths)
        editor.commit()
    }

    fun getImagePath(): String? {
        return pref.getString(SessionManager.Companion.imagePath, "")
    }

    companion object {
        private val PREF_NAME: String? = "com.camouflage.pref"
        private val IS_LOGGED: String? = "isLogged"
        private val UserCode: String? = "UserCode"
        private val UserAddress: String? = "UserAddress"
        private val UserEmail: String? = "UserEmail"
        private val UserName: String? = "UserName"
        private val UserPassword: String? = "UserPassword"
        private val UserMobile: String? = "UserMobile"
        private val UserCountryId: String? = "UserCountryId"
        private val AuthToken: String? = "AuthToken"
        private val IsCalendarPopupShownBefore: String? = "IsCalendarPopupShownBefore"
        private val IS_WhatsApp_OpenBefore: String? = "IS_WhatsApp_OpenBefore"
        private val IS_Facebook_OpenBefore: String? = "IS_Facebook_OpenBefore"
        private val IS_Instagram_OpenBefore: String? = "IS_Instagram_OpenBefore"
        private val imagePath: String? = "imagePath"
    }

    init {
        pref = _context.getSharedPreferences(SessionManager.Companion.PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }
}