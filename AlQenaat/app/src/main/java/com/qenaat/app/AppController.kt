package com.qenaat.app

import android.content.Context
import android.content.res.Configuration
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import androidx.multidex.MultiDexApplication

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import com.qenaat.app.classes.LocaleHelper

class AppController : MultiDexApplication() {
//    private var mRequestQueue: RequestQueue? = null
//    private val mImageLoader: ImageLoader? = null
    protected override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, "ar"))
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        initImageLoader(applicationContext)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
    }

    fun getLocale(): String? {
        return getResources().getConfiguration().locale.toString()
    }

//    fun getRequestQueue(): RequestQueue? {
//        if (mRequestQueue == null) {
//            // getApplicationContext() is key, it keeps you from leaking the
//            // Activity or BroadcastReceiver if someone passes one in.
//            mRequestQueue = Volley.newRequestQueue(applicationContext)
//        }
//        return mRequestQueue
//    }

//    fun <T> addToRequestQueue(req: Request<T?>?) {
//        req?.setTag(TAG)
//        getRequestQueue()?.add(req)
//    }

//    fun <T> addToRequestQueue(req: Request<T?>?, tag: String?) {
//        req?.setTag(if (TextUtils.isEmpty(tag)) TAG else tag)
//        getRequestQueue()?.add(req)
//    }

//    fun cancelAllPendingRequest(tag: String?) {
//        if (mRequestQueue != null) mRequestQueue?.cancelAll(tag)
//    }

//    fun getImageLoader(): ImageLoader? {
//        return mImageLoader
//    }

    //function to get the device id
    fun getIMEI(): String? {
        return Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID)
    }

    companion object {
        private var mInstance: AppController? = null
        protected val TAG = AppController::class.java.simpleName

        @Synchronized
        fun getInstance(): AppController? {
            if (mInstance == null) {
                try {
                    mInstance = AppController::class.java.newInstance()
                } catch (e: InstantiationException) {
                    Log.e(TAG
                            + Thread.currentThread().stackTrace[2]
                            .lineNumber, e.message)
                    e.printStackTrace()
                } catch (e: IllegalAccessException) {
                    Log.e(TAG
                            + Thread.currentThread().stackTrace[2]
                            .lineNumber, e.message)
                    e.printStackTrace()
                }
            }
            return mInstance
        }

        fun initImageLoader(context: Context?) {
            // This configuration tuning is custom. You can tune every option, you
            // may tune some of them,
            // or you can create default configuration by
            // ImageLoaderConfiguration.createDefault(this);
            // method.
            val config: ImageLoaderConfiguration = ImageLoaderConfiguration.Builder(
                    context).threadPriority(Thread.NORM_PRIORITY - 3)
                    .denyCacheImageMultipleSizesInMemory()
                    .discCacheFileNameGenerator(Md5FileNameGenerator())
                    .tasksProcessingOrder(QueueProcessingType.FIFO) //                .writeDebugLogs() // Remove for release app
                    .build()
            // Initialize ImageLoader with configuration.
            com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config)
        }
    }
}