package com.qenaat.app.model

/**
 * Created by shahbazshaikh on 19/09/16.
 */
class Area {
    fun getCityAR(): String? {
        return CityAR
    }

    fun setCityAR(cityAR: String?) {
        CityAR = cityAR
    }

    fun getCityEN(): String? {
        return CityEN
    }

    fun setCityEN(cityEN: String?) {
        CityEN = cityEN
    }

    fun getCityId(): String? {
        return CityId
    }

    fun setCityId(cityId: String?) {
        CityId = cityId
    }

    fun getCountryAR(): String? {
        return CountryAR
    }

    fun setCountryAR(countryAR: String?) {
        CountryAR = countryAR
    }

    fun getCountryEN(): String? {
        return CountryEN
    }

    fun setCountryEN(countryEN: String?) {
        CountryEN = countryEN
    }

    fun getCountryId(): String? {
        return CountryId
    }

    fun setCountryId(countryId: String?) {
        CountryId = countryId
    }

    fun getFullTitleAR(): String? {
        return FullTitleAR
    }

    fun setFullTitleAR(fullTitleAR: String?) {
        FullTitleAR = fullTitleAR
    }

    fun getFullTitleEN(): String? {
        return FullTitleEN
    }

    fun setFullTitleEN(fullTitleEN: String?) {
        FullTitleEN = fullTitleEN
    }

    fun getId(): String? {
        return Id
    }

    fun setId(id: String?) {
        Id = id
    }

    fun getTitleAR(): String? {
        return TitleAR
    }

    fun setTitleAR(titleAR: String?) {
        TitleAR = titleAR
    }

    fun getTitleEN(): String? {
        return TitleEN
    }

    fun setTitleEN(titleEN: String?) {
        TitleEN = titleEN
    }

    private var CityAR: String? = null
    private var CityEN: String? = null
    private var CityId: String? = null
    private var CountryAR: String? = null
    private var CountryEN: String? = null
    private var CountryId: String? = null
    private var FullTitleAR: String? = null
    private var FullTitleEN: String? = null
    private var Id: String? = null
    private var TitleAR: String? = null
    private var TitleEN: String? = null
}