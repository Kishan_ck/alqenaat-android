package com.qenaat.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl.checkEmptyString
import com.qenaat.app.classes.FixControl.returnString
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.showToast
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.classes.SocialMediaShare
import com.qenaat.app.model.GetDewanias
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by DELL on 11-Nov-17.
 */
class DeewaniyaDetailsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var relative_top: RelativeLayout
    lateinit var img_event: ImageView
    lateinit var img_reminder: ImageView
    lateinit var img_fav: ImageView
    lateinit var img_share: ImageView
    lateinit var img_report: ImageView
    lateinit var img_facebook: ImageView
    lateinit var img_instagram: ImageView
    lateinit var img_twitter: ImageView
    lateinit var img_youtube: ImageView
    lateinit var img_location: ImageView
    lateinit var img_call: ImageView
    lateinit var img_calender: ImageView
    lateinit var linear_date: LinearLayout
    lateinit var linear_bottom: LinearLayout
    lateinit var linear_social: ConstraintLayout
    lateinit var tv_days: TextView
    lateinit var tv_views: TextView
    lateinit var tv_occasion_details: TextView
    lateinit var tv_deewaniya_title: TextView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var getDiwaniyas: GetDewanias
    lateinit var productImages: Array<String?>
    private val CALL_PHONE_PERMISSION_CODE = 23
    lateinit var pShareUri: Uri
    var file: File? = null
    lateinit var img_count: TextView
    lateinit var relative_gallery: RelativeLayout
    var permissionStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetDewanias")) {
                    val gson = Gson()
                    getDiwaniyas = gson.fromJson(arguments!!.getString("GetDewanias"),
                            GetDewanias::class.java)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.deewaniya_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            relative_gallery = mainLayout.findViewById(R.id.relative_gallery) as RelativeLayout
            img_count = mainLayout.findViewById(R.id.img_count) as TextView
            img_reminder = mainLayout.findViewById(R.id.img_reminder) as ImageView
            img_fav = mainLayout.findViewById(R.id.img_fav) as ImageView
            img_share = mainLayout.findViewById(R.id.img_share) as ImageView
            img_report = mainLayout.findViewById(R.id.img_report) as ImageView
            img_facebook = mainLayout.findViewById(R.id.img_facebook) as ImageView
            img_instagram = mainLayout.findViewById(R.id.img_instagram) as ImageView
            img_twitter = mainLayout.findViewById(R.id.img_twitter) as ImageView
            img_youtube = mainLayout.findViewById(R.id.img_youtube) as ImageView
            img_location = mainLayout.findViewById(R.id.img_location) as ImageView
            img_call = mainLayout.findViewById(R.id.img_call) as ImageView
            img_calender = mainLayout.findViewById(R.id.img_calender) as ImageView
            img_event = mainLayout.findViewById(R.id.img_event) as ImageView
            tv_views = mainLayout.findViewById(R.id.tv_views) as TextView
            tv_occasion_details = mainLayout.findViewById(R.id.tv_occasion_details) as TextView
            tv_days = mainLayout.findViewById(R.id.tv_days) as TextView
            tv_deewaniya_title = mainLayout.findViewById(R.id.tv_deewaniya_title) as TextView
            linear_date = mainLayout.findViewById(R.id.linear_date) as LinearLayout
            linear_bottom = mainLayout.findViewById(R.id.linear_bottom) as LinearLayout
            linear_social = mainLayout.findViewById(R.id.linear_social) as ConstraintLayout
            relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            img_calender.setOnClickListener(this)
            img_call.setOnClickListener(this)
            img_location.setOnClickListener(this)
            img_youtube.setOnClickListener(this)
            img_twitter.setOnClickListener(this)
            img_instagram.setOnClickListener(this)
            img_facebook.setOnClickListener(this)
            img_report.setOnClickListener(this)
            img_share.setOnClickListener(this)
            img_fav.setOnClickListener(this)
            img_reminder.setOnClickListener(this)
            img_event.setOnClickListener(this)
            tv_views.setTypeface(ContentActivity.Companion.tf)
            tv_occasion_details.setTypeface(ContentActivity.Companion.tf)
            tv_days.setTypeface(ContentActivity.Companion.tf)
            tv_deewaniya_title.setTypeface(ContentActivity.Companion.tf)
        }
    }

    override fun onStart() {
        super.onStart()
        //  ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.bottomBarView.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        SocialMediaShare.shareContext = act
        GetDiwaniyas()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.img_event -> {
                if (getDiwaniyas.Photos != null) {
                    if (getDiwaniyas.Photos?.size!! > 0) {
                        productImages = arrayOfNulls<String?>(getDiwaniyas.Photos!!.size)
                        var i = 0
                        while (i < getDiwaniyas.Photos!!.size) {
                            val photo = getDiwaniyas.Photos!![i]
                            productImages[i] = photo!!.Photo
                            i++
                        }
                        /*else{

                    productImages = new String[1];

                    productImages[0] = getDiwaniyas.getPhotos();

                }*/
                        val b = Bundle()
                        b.putInt("position", 0)
                        b.putStringArray("productImages", productImages)
                        ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                    }
                }
            }
            R.id.img_fav -> {
            }
            R.id.img_share -> {
                permissionStr = Manifest.permission.READ_EXTERNAL_STORAGE
                GlobalFunctions.requestStoragePermission(act)
            }
            R.id.img_report -> {
            }
            R.id.img_facebook -> if (returnString(getDiwaniyas.Facebook).isNotEmpty()) {
                if (getDiwaniyas.Facebook!!.contains("http") || getDiwaniyas.Facebook!!.contains("https"))
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getDiwaniyas.Facebook)))
                else
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getDiwaniyas.Facebook}")))
            }
            R.id.img_instagram -> if (returnString(getDiwaniyas.Instagram).isNotEmpty()) {
                if (getDiwaniyas.Instagram!!.contains("http") || getDiwaniyas.Instagram!!.contains("https"))
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getDiwaniyas.Instagram)))
                else
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getDiwaniyas.Instagram}")))
            }
            R.id.img_twitter -> if (returnString(getDiwaniyas.Twitter).isNotEmpty()!!) {
                if (getDiwaniyas.Twitter!!.contains("http") || getDiwaniyas.Twitter!!.contains("https"))
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getDiwaniyas.Twitter)))
                else
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getDiwaniyas.Twitter}")))
            }
            R.id.img_youtube -> if (returnString(getDiwaniyas.YouTube).isNotEmpty()) {
                if (getDiwaniyas.YouTube!!.contains("http") || getDiwaniyas.YouTube!!.contains("https"))
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getDiwaniyas.YouTube)))
                else
                    act!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("http://${getDiwaniyas.YouTube}")))
            }
            R.id.img_location -> {
                /* if (GlobalFunctions.isGPSAllowed(act)) {
                     if (getDiwaniyas.Latitude!!.isNotEmpty() && getDiwaniyas.Longitude!!.isNotEmpty()) {
                         val gson = Gson()
                         val bundle = Bundle()
                         val contentsArrayList = ArrayList<GetDewanias?>()
                         contentsArrayList.add(getDiwaniyas)
                         bundle.putString("GetDewanias", gson.toJson(contentsArrayList))
                         ContentActivity.Companion.openShowDeewaniyaMapFragment(bundle)
                     }
                     return
                 }*/
                if (getDiwaniyas.Latitude!!.isNotEmpty() && getDiwaniyas.Longitude!!.isNotEmpty()) {
                    permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                    GlobalFunctions.requestGPSPermission(act)
                } else
                    showToast(act, getString(R.string.no_location_found))
            }
            R.id.img_call -> {
                /*if (GlobalFunctions.isReadCallAllowed(act)) {
                    if (getDiwaniyas.Phone!!.length > 0) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.data = Uri.parse("tel:" + getDiwaniyas.Phone)
                        act?.startActivity(intent)
                    }
                    return
                }*/
                if (getDiwaniyas.Phone!!.isNotEmpty()) {
                    permissionStr = Manifest.permission.CALL_PHONE
                    GlobalFunctions.requestCallPermission(act)
                } else
                    showToast(act, getString(R.string.no_number_found))
            }
            R.id.img_calender -> {
            }
        }
    }

    private fun shareContent() {
        val shareProductInfo: String =
                if (languageSeassionManager.getLang().equals("en", ignoreCase = true))
                    getDiwaniyas.TitleEn + "" + "\n\n" + getDiwaniyas.DetailsEn + "" + "\n\n" + act?.getString(R.string.UrlLink1)
                else getDiwaniyas.TitleAr + "" + "\n\n" + getDiwaniyas.DetailsAr + "" + "\n\n" + act?.getString(R.string.UrlLink1)

        //val ob: GetAndSaveBitmapForArticle = GetAndSaveBitmapForArticle()
        val clipboard = act?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText("AlQenaat", shareProductInfo)
        clipboard.setPrimaryClip(clip)
        //ob.execute(events.Photo)
        val dialog = Dialog(act!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.share_dialog)
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        val mimg_close: ImageView
        val mimg_facebook: ImageView
        val mimg_twitter: ImageView
        val mimg_instagram: ImageView
        val mimg_email: ImageView
        val mimg_sms: ImageView
        val mimg_whats: ImageView
        mimg_facebook = dialog.findViewById<View?>(R.id.img_facebook) as ImageView
        val title: TextView = dialog.findViewById<View?>(R.id.shartext) as TextView
        title.setTypeface(ContentActivity.Companion.tf)
        mimg_twitter = dialog.findViewById<View?>(R.id.img_twitter) as ImageView
        mimg_instagram = dialog.findViewById<View?>(R.id.img_instagram) as ImageView
        mimg_email = dialog.findViewById<View?>(R.id.img_email) as ImageView
        mimg_sms = dialog.findViewById<View?>(R.id.img_sms) as ImageView
        mimg_whats = dialog.findViewById<View?>(R.id.img_whats) as ImageView
        mimg_close = dialog.findViewById<View?>(R.id.img_searchClose) as ImageView
        mimg_close.setOnClickListener(View.OnClickListener {
            file?.delete()
            dialog.dismiss()
        })
        mimg_facebook.setOnClickListener(View.OnClickListener { /*Share(1,
                                "com.facebook.katana",
                                products.getDecription() + "\n" +
                                        products.getPhotos());*/
            dialog.dismiss()
            /* if (mSessionManager.isFacebookOpenBefore()) {
                 Share(1,
                         "facebook",
                         shareProductInfo)
             } else {*/
            AlertDialog.Builder(act!!)
                    .setTitle(act?.getString(R.string.AttentionLabel))
                    .setMessage(act?.getString(R.string.AttentionText))
                    .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            SocialMediaShare.shareData(1, "facebook", shareProductInfo)
                            mSessionManager.FacebookOpened()
                        }
                    })
                    .setIcon(R.drawable.icon_512)
                    .show()
            //}
        })
        mimg_twitter.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            SocialMediaShare.shareData(5, "twitter", shareProductInfo)
        })
        mimg_instagram.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            AlertDialog.Builder(act!!)
                    .setIcon(R.drawable.icon_512)
                    .setTitle(act?.getString(R.string.AttentionLabel))
                    .setMessage(act?.getString(R.string.AttentionText))
                    .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            SocialMediaShare.shareData(6, "instagram", shareProductInfo)
                        }
                    })
                    .setIcon(R.drawable.icon_512)
                    .show()
        })
        mimg_email.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            SocialMediaShare.shareData(3, "gmail", shareProductInfo)
        })
        mimg_sms.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            SocialMediaShare.shareData(4, "sms", shareProductInfo)
        })
        mimg_whats.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            SocialMediaShare.shareData(2, "whatsapp", shareProductInfo)
        })
        dialog.show()
    }

    fun GetDiwaniyas() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mloading?.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetDewanias(getDiwaniyas.Id, "0", "0",
                "0")?.enqueue(
                object : Callback<ArrayList<GetDewanias?>?> {

                    override fun onFailure(call: Call<ArrayList<GetDewanias?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetDewanias?>?>, response: Response<ArrayList<GetDewanias?>?>) {
                        if (response.body() != null) {
                            val getDiwaniyases = response.body()
                            if (mloading != null) {
                                if (getDiwaniyases != null) {
                                    getDiwaniyas = getDiwaniyases[0]!!

//                        img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                                R.drawable.no_img_details)).getBitmap().getWidth();
                                    img_event.getLayoutParams().height = (act?.getResources()?.getDrawable(
                                            R.drawable.no_img_details) as BitmapDrawable).getBitmap().getHeight()
                                    if (getDiwaniyas.Photos != null) {
                                        if (getDiwaniyas.Photos!!.size > 0) {
                                            if (getDiwaniyas.Photos!![0]!!.Photo!!.isNotEmpty()) {
                                                Picasso.with(act).load(getDiwaniyas.Photos!![0]!!.Photo).placeholder(R.drawable.no_img_details)
                                                        .error(R.drawable.no_img_details).into(img_event)
                                                val ob = GetAndSaveBitmapForArticle()
                                                ob.execute(getDiwaniyas.Photos!![0]!!.Photo)
                                            }
                                        }
                                    }
                                    relative_gallery.setVisibility(View.INVISIBLE)
                                    if (getDiwaniyas.Photos != null) {
                                        if (getDiwaniyas.Photos!!.size >= 2) {
                                            relative_gallery.setVisibility(View.VISIBLE)
                                            img_count.setText(getDiwaniyas.Photos!!.size.toString() + "")
                                        }
                                    }
                                    if (returnString(getDiwaniyas.Latitude).isNotEmpty() && returnString(getDiwaniyas.Longitude).isNotEmpty()) {
                                    } else {
                                        img_location.setAlpha(50)
                                    }
                                    if (getDiwaniyas.WeekDays != null) {
                                        if (getDiwaniyas.WeekDays!!.size > 0) if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                                            tv_days.setText(getDiwaniyas.WeekDays!![0]!!.TitleEn + " " + act!!.getString(R.string.DeewanLabel))
                                        } else {
                                            tv_days.setText(getDiwaniyas.WeekDays!![0]!!.TitleAr + " " + act!!.getString(R.string.DeewanLabel))
                                        }
                                    }
                                    if (!getDiwaniyas.ViewersCount?.checkEmptyString()!!)
                                        tv_views.setText(getDiwaniyas.ViewersCount + " " + act!!.getString(R.string.Views))
                                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                                        tv_deewaniya_title.setText(getDiwaniyas.TitleEn)
                                        tv_occasion_details.setText(getDiwaniyas.DetailsEn)
                                    } else {
                                        tv_deewaniya_title.setText(getDiwaniyas.TitleAr)
                                        tv_occasion_details.setText(getDiwaniyas.DetailsAr)
                                    }
                                    if (returnString(getDiwaniyas.Facebook).isNotEmpty()) {
                                    } else {
                                        img_facebook.setAlpha(50)
                                    }
                                    if (returnString(getDiwaniyas.Twitter).isNotEmpty()) {
                                    } else {
                                        img_twitter.setAlpha(50)
                                    }
                                    if (returnString(getDiwaniyas.Instagram).length > 0) {
                                    } else {
                                        img_instagram.setAlpha(50)
                                    }
                                    if (returnString(getDiwaniyas.YouTube).length > 0) {
                                    } else {
                                        img_youtube.setAlpha(50)
                                    }
                                }
                            }
                            mloading?.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    // shareType : 1-fb , 5-twitter , 6-Instgram  , 2-wtsapp  , 3-gmail  , 4sms
    @SuppressLint("DefaultLocale")
    private fun Share(shareType: Int, appName: String?, content: String?) {
        val targetedShareIntents: MutableList<Intent?> = ArrayList()
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        val resInfo: MutableList<ResolveInfo?> = act?.getPackageManager()?.queryIntentActivities(share, 0)!!
        if (!resInfo.isEmpty()) {
            when (shareType) {
                1 -> {
                    Log.d("inside fbshare", "test0001")
                    // facebook
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info?.activityInfo?.packageName?.toLowerCase()?.contains(appName!!)!! || info.activityInfo.name.toLowerCase().contains(appName!!)) {
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        act?.startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Facebook is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                5 -> {
                    // twitter
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        if (info!!.activityInfo.packageName.toLowerCase().contains(appName!!) || info.activityInfo.name.toLowerCase().contains(appName)) {
                            Log.e("Twitter", "Twitter-Data: $content")
                            targetedShare.putExtra(Intent.EXTRA_TEXT, content)

//                            Uri uri = Uri.parse(products.getPhotos());
//                            targetedShare.putExtra(Intent.EXTRA_STREAM, String.valueOf(uri));
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "Twitter is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                6 ->                     //Instagram
                    if (1 == 1) {
                        try {
                            val shareIntent = Intent(Intent.ACTION_SEND)
                            shareIntent.type = "image/*"
                            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            shareIntent.putExtra(Intent.EXTRA_STREAM, pShareUri)
                            shareIntent.putExtra(Intent.EXTRA_TEXT, content)
                            shareIntent.setPackage("com.instagram.android")
                            targetedShareIntents.add(shareIntent)
                            val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                            startActivity(chooserIntent)
                        } catch (e: Exception) {
                            Toast.makeText(act, "Instagram is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                        }
                    }
                2 -> {
                    // whatsapp
                    for (info in resInfo) {
                        val targetedShare = Intent(
                                Intent.ACTION_SEND)
                        targetedShare.type = "text/plain"
                        // put here your mime type
                        if (info!!.activityInfo.packageName.toLowerCase().contains(
                                        appName!!)
                                || info.activityInfo.name.toLowerCase().contains(
                                        appName)) {
                            Log.d("whatsapp_share", content)
                            if (getDiwaniyas.Photos != null) {
                                targetedShare.putExtra(Intent.EXTRA_TEXT, """ $content
                                    ${if (getDiwaniyas.Photos!!.size > 0) getDiwaniyas.Photos!![0]!!.Photo else ""}
                                    """.trimIndent())
                            } else {
                                targetedShare.putExtra(Intent.EXTRA_TEXT, """$content""".trimIndent())
                            }
                            targetedShare.setPackage(info.activityInfo.packageName)
                            targetedShareIntents.add(targetedShare)
                            break
                        }
                    }
                    if (targetedShareIntents.size > 0) {
                        val chooserIntent = Intent.createChooser(targetedShareIntents.removeAt(0), "Select app to share")
                        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toTypedArray())
                        startActivity(chooserIntent)
                    } else {
                        Toast.makeText(act, "WhatsApp is not Installed, Please Install it", Toast.LENGTH_LONG).show()
                    }
                }
                3 -> {
                    // Gmail
                    var txt = ""
                    if (getDiwaniyas.Photos != null)
                        txt = "<img src=\"" + (if (getDiwaniyas.Photos!!.size > 0) getDiwaniyas.Photos!![0]?.Photo else "") + "\" width=\"600px\"  height=\"600px\"/>"
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/html"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, act!!.getString(R.string.app_name))
                    shareIntent.putExtra(Intent.EXTRA_TEXT, """$content
 ${Html.fromHtml(txt)}""")
                    for (app in resInfo) {
                        if (app!!.activityInfo.name.toLowerCase().contains(appName!!)) {
                            val activity: ActivityInfo = app.activityInfo
                            val name = ComponentName(activity.applicationInfo.packageName, activity.name)
                            shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                            shareIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                            shareIntent.component = name
                            act!!.startActivity(shareIntent)
                            break
                        }
                    }
                }
                4 -> {
                    //SMS
                    val smsIntent = Intent(Intent.ACTION_VIEW)
                    smsIntent.data = Uri.parse("smsto:")
                    smsIntent.type = "vnd.android-dir/mms-sms"
                    smsIntent.putExtra(Intent.EXTRA_SUBJECT, act!!.getString(R.string.app_name) + "\n" + content)
                    smsIntent.putExtra("sms_body", content)
                    try {
                        startActivity(smsIntent)
                        Log.i("Finished sending SMS...", "")
                    } catch (ex: ActivityNotFoundException) {
                    }
                }
                else -> {
                }
            }
        }
    }

    internal inner class GetAndSaveBitmapForArticle : AsyncTask<String?, Void?, Uri?>() {
        protected override fun doInBackground(vararg params: String?): Uri? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            file = File(dir, UUID.randomUUID().toString() + ".png")
            val fOut: FileOutputStream
            try {
                val bm: Bitmap = Picasso.with(act).load(params[0]).get()
                fOut = FileOutputStream(file)
                bm.compress(Bitmap.CompressFormat.PNG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            pShareUri = FileProvider.getUriForFile(act!!,
                    act?.applicationContext?.packageName + ".fileprovider", file!!)
            SocialMediaShare.shareContext = act
            SocialMediaShare.imgUri = pShareUri
            return pShareUri
        }
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.STORAGE_PERMISSION_CODE -> shareContent()
                GlobalFunctions.CALL_PERMISSION_CODE -> {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:" + getDiwaniyas.Phone)
                    act?.startActivity(intent!!)
                }
                GlobalFunctions.LOCATION_PERMISSION_CODE -> {
                    val gson = Gson()
                    val bundle = Bundle()
                    val contentsArrayList = ArrayList<GetDewanias?>()
                    contentsArrayList.add(getDiwaniyas)
                    bundle.putString("GetDewanias", gson.toJson(contentsArrayList))
                    ContentActivity.Companion.openShowDeewaniyaMapFragment(bundle)
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act!!, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act!!)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        protected val TAG = DeewaniyaDetailsFragment::class.java.simpleName
        var act: FragmentActivity? = null
        var fragment: DeewaniyaDetailsFragment? = null
        var mloading: ProgressBar? = null
        fun newInstance(act: FragmentActivity?): DeewaniyaDetailsFragment? {
            fragment = DeewaniyaDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}