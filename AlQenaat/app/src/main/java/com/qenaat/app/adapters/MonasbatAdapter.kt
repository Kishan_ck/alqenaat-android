package com.qenaat.app.adapters

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.MonasbatAdapter.ViewHolderStaticHeader
import com.qenaat.app.classes.FixControl.returnString
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetEvents
import com.squareup.picasso.Picasso
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter
import de.hdodenhof.circleimageview.CircleImageView
import java.security.SecureRandom
import java.util.*

/**
 * Created by DELL on 31-Oct-17.
 */
class MonasbatAdapter(var act: FragmentActivity, private val itemsData: ArrayList<GetEvents?>,
                      var header_position: ArrayList<Long?>?, private val catId: String) : RecyclerView.Adapter<RecyclerView.ViewHolder?>(), StickyRecyclerHeadersAdapter<ViewHolderStaticHeader?> {
    var languageSeassionManager: LanguageSessionManager
    var isHeaderLoaded = false

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ITEM) {
            val itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.news_list_row, null)

            // create ViewHolder
            return ViewHolder(itemLayoutView)
        } else if (viewType == TYPE_HEADER) {
            val itemLayoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.news_list_row, null)

            // create ViewHolder
            return ViewHolderHeader(itemLayoutView)
        }
        throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is ViewHolder) {
            val dataItem = getItem(position)
            if (dataItem != null) {
                (viewHolder as ViewHolder?)?.relative_content?.setVisibility(View.GONE)
                (viewHolder as ViewHolder?)?.img_ad?.setVisibility(View.GONE)
                (viewHolder as ViewHolder?)?.tv_ad?.setVisibility(View.GONE)
                (viewHolder as ViewHolder?)?.tv_content?.setVisibility(View.GONE)
                if (dataItem.Id.equals("-1", ignoreCase = true)) {
                    (viewHolder as ViewHolder?)?.img_ad?.setVisibility(View.VISIBLE)
                    (viewHolder as ViewHolder?)?.tv_ad?.setVisibility(View.VISIBLE)
                    (viewHolder as ViewHolder?)?.img_ad?.setImageResource(R.drawable.no_ing_list)
                    (viewHolder as ViewHolder?)?.img_ad?.getLayoutParams()?.width = (act.getResources().getDrawable(
                            R.drawable.ad) as BitmapDrawable).bitmap.width
                    (viewHolder as ViewHolder?)?.img_ad?.getLayoutParams()?.height = (act.getResources().getDrawable(
                            R.drawable.ad) as BitmapDrawable).bitmap.height
                    if (dataItem.Photo?.length!! > 0) Picasso.with(act)
                            .load(dataItem.Photo)
                            .error(R.drawable.ad)
                            .placeholder(R.drawable.ad)
                            .config(Bitmap.Config.RGB_565).fit()
                            .into((viewHolder as ViewHolder?)?.img_ad)
                    (viewHolder as ViewHolder?)?.img_ad?.setOnClickListener(View.OnClickListener {
                        if (dataItem.DetailsEn?.length!! > 0 && dataItem.DetailsEn?.contains("http")!!) {
                            act.startActivity(Intent(Intent.ACTION_VIEW,
                                    Uri.parse(dataItem.DetailsEn)))
                        }
                    })
                } else {
                    (viewHolder as ViewHolder?)?.tv_content?.setVisibility(View.VISIBLE)
                    (viewHolder as ViewHolder?)?.relative_content?.setVisibility(View.VISIBLE)
                    (viewHolder as ViewHolder?)?.relative_date?.setVisibility(View.VISIBLE)
                    (viewHolder as ViewHolder?)?.img_news?.setImageResource(R.drawable.day_bg)
                    (viewHolder as ViewHolder?)?.tv_day_number?.setText(
                            dataItem.EventDate?.split("/".toRegex())?.toTypedArray()!![0])
                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                        (viewHolder as ViewHolder?)?.tv_title?.setText(dataItem.EventNameEn)
                        (viewHolder as ViewHolder?)?.tv_day_name?.setText(dataItem.DayNameEn)
                        (viewHolder as ViewHolder?)?.tv_category_name?.setText(dataItem.EventTypeNameEn)
                    } else {
                        (viewHolder as ViewHolder?)?.tv_category_name?.setText(dataItem.EventTypeNameAr)
                        (viewHolder as ViewHolder?)?.tv_title?.setText(dataItem.EventNameAr)
                        (viewHolder as ViewHolder?)?.tv_day_name?.setText(dataItem.DayNameAr)
                    }
                    (viewHolder as ViewHolder?)?.tv_date?.setText(dataItem.EventDate)
                    (viewHolder as ViewHolder?)?.img_category_color?.circleBackgroundColor = Color.parseColor("#ffffff")
                    (viewHolder as ViewHolder?)?.tv_category_name?.setTextColor(Color.parseColor("#4c4c4c"))
                    if (returnString(dataItem.Color).length!! > 6) {
                        (viewHolder as ViewHolder?)?.img_category_color?.circleBackgroundColor = Color.parseColor(dataItem.Color)
                        (viewHolder as ViewHolder?)?.tv_category_name?.setTextColor(Color.parseColor(dataItem.Color))
                    }
                    (viewHolder as ViewHolder?)?.relative_content?.setOnClickListener(View.OnClickListener {
                        val gson = Gson()
                        val b = Bundle()
                        b.putString("GetEvents.Events", gson.toJson(itemsData.get(position)))
                        b.putString("catId", catId)
                        ContentActivity.Companion.openMonasbatDetailsFragment(b)
                    })
                }
            }
            (viewHolder as ViewHolder?)?.relative_parent?.setOnClickListener(View.OnClickListener {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetEvents.Events", gson.toJson(itemsData.get(position)))
                b.putString("catId", catId)
                ContentActivity.Companion.openMonasbatDetailsFragment(b)
            })
        } else if (viewHolder is ViewHolderHeader && !isHeaderLoaded) {
            (viewHolder as ViewHolderHeader?)?.relative_parent?.setVisibility(View.GONE)
        }
    }

    private fun getItem(position: Int): GetEvents? {
        return itemsData.get(position)
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var img_arrow: ImageView
        var img_news: ImageView
        var img_ad: ImageView
        var tv_category_name: TextView
        var tv_title: TextView
        var tv_date: TextView
        var tv_day_number: TextView
        var tv_day_name: TextView
        var tv_ad: TextView
        var tv_content: TextView
        var relative_content: RelativeLayout
        var relative_category: RelativeLayout
        var relative_parent: RelativeLayout
        var relative_date: RelativeLayout
        var img_category_color: CircleImageView

        init {
            tv_ad = itemLayoutView.findViewById(R.id.tv_ad) as TextView
            tv_content = itemLayoutView.findViewById(R.id.tv_content) as TextView
            tv_day_number = itemLayoutView.findViewById(R.id.tv_day_number) as TextView
            tv_day_name = itemLayoutView.findViewById(R.id.tv_day_name) as TextView
            tv_category_name = itemLayoutView.findViewById(R.id.tv_category_name) as TextView
            img_news = itemLayoutView.findViewById(R.id.img_news) as ImageView
            img_arrow = itemLayoutView.findViewById(R.id.img_arrow) as ImageView
            img_category_color = itemLayoutView.findViewById(R.id.img_category_color)
                    as CircleImageView
            img_bg = itemLayoutView.findViewById(R.id.img_bg) as ImageView
            tv_date = itemLayoutView.findViewById(R.id.tv_date) as TextView
            tv_title = itemLayoutView.findViewById(R.id.tv_title) as TextView
            img_ad = itemLayoutView.findViewById(R.id.img_ad) as ImageView
            relative_date = itemLayoutView
                    .findViewById(R.id.relative_date) as RelativeLayout
            relative_parent = itemLayoutView
                    .findViewById(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById(R.id.relative_content) as RelativeLayout
            relative_category = itemLayoutView
                    .findViewById(R.id.relative_category) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_parent)
            tv_day_number.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
        }
    }

    class ViewHolderHeader(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var relative_parent: RelativeLayout?

        init {
            relative_parent = itemLayoutView
                    .findViewById(R.id.relative_parent) as RelativeLayout?
        }
    }

    open class ViewHolderStaticHeader(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var tv_day: TextView
        var tv_month_year: TextView

        init {
            img_bg = itemLayoutView.findViewById(R.id.img_bg) as ImageView
            tv_day = itemLayoutView.findViewById(R.id.tv_day) as TextView
            tv_month_year = itemLayoutView.findViewById(R.id.tv_month_year) as TextView
            tv_day.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_month_year.setTypeface(ContentActivity.Companion.tf)
        }
    }

    override fun getHeaderId(position: Int): Long {
        return header_position?.get(position)!!
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup?): ViewHolderStaticHeader? {
        val view = LayoutInflater.from(parent?.getContext())
                .inflate(R.layout.static_header, parent, false)

        //MonasbatAdapter.ViewHolderStaticHeader viewHolder = new MonasbatAdapter.ViewHolderStaticHeader(view);
        return object : ViewHolderStaticHeader(view) {}
    }

    override fun onBindHeaderViewHolder(holder: ViewHolderStaticHeader?, position: Int) {
        Log.d("header_value", position.toString() + "")

        //if(header_position.get(position).equalsIgnoreCase("1")){
        if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
            holder?.tv_month_year?.setText(itemsData.get(position)?.EventDate?.split("/".toRegex())?.toTypedArray()!![2] + " " + itemsData.get(position)!!.MonthNameEn)
        } else {
            holder?.tv_month_year?.setText(itemsData.get(position)?.EventDate?.split("/".toRegex())?.toTypedArray()!![2] + " " + itemsData.get(position)!!.MonthNameAr)
        }
        holder?.tv_day?.setText(itemsData.get(position)?.EventDate?.split("/".toRegex())?.toTypedArray()!![1] + "")

        //holder.itemView.setBackgroundColor(getRandomColor());
        //}
    }

    private fun getRandomColor(): Int {
        val rgen = SecureRandom()
        //        return Color.HSVToColor(150, new float[]{
//                rgen.nextInt(359), 1, 1
//        });
        return Color.argb(150, 34, 26, 255)
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    override fun getItemViewType(position: Int): Int {
        return TYPE_ITEM
    }

    private fun isPositionHeader(position: Int): Boolean {
        return position == 0
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}