package com.qenaat.app.model

/**
 * Created by DELL on 08-Nov-17.
 */
class GetServiceTypes {
    var Id: String? = null
    var ServiceTypeEn: String? = null
    var ServiceTypeAr: String? = null
    var ServiceTypeNameEn: String? = null
    var ServiceTypeNameAr: String? = null
    var Color: String? = null
}