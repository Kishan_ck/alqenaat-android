package com.qenaat.app.classes

import android.widget.GridView
import android.widget.ListView

object ListViewInsideScrollViewUtitlity {
    fun setListViewHeightBasedOnChildren(listView: ListView?) {
        val listAdapter = listView?.getAdapter() ?: return
        var totalHeight = 0
        for (i in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(i, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }
        val params = listView.getLayoutParams()
        params.height = (totalHeight
                + listView.getDividerHeight() * (listAdapter.count - 1))
        listView.setLayoutParams(params)
    }

    fun setGridViewHeightBasedOnNumberOfColumns(
            gridView: GridView?, columnHeigh: Int, numberOfColumns: Int) {
        var totalHeight = numberOfColumns * columnHeigh
        totalHeight = totalHeight - (numberOfColumns - 1) * gridView?.getVerticalScrollbarWidth()!!
        val params = gridView.getLayoutParams()
        params.height = totalHeight
        gridView.setLayoutParams(params)
    }
}