package com.qenaat.app.fragments

import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.AlertAdapterFA
import com.qenaat.app.classes.*
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkLengthString
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.model.GetServiceTypes
import com.qenaat.app.networking.QenaatAPICall
import me.drakeet.materialdialog.MaterialDialog
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by DELL on 15-Nov-17.
 */
class AddServiceFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSessionManager: LanguageSessionManager
    lateinit var linear_category: LinearLayout
    lateinit var linear_time_from: LinearLayout
    lateinit var linear_to_time: LinearLayout
    lateinit var linear_details: LinearLayout
    lateinit var tv_category_label: TextView
    lateinit var tv_time_from: TextView
    lateinit var tv_to_time: TextView
    lateinit var tv_add_ad: TextView
    lateinit var et_title: EditText
    lateinit var et_price: EditText
    lateinit var et_details_label: EditText
    var getServiceTypesArrayList: ArrayList<GetServiceTypes?> = ArrayList()
    var serviceTypeId: String = "0"
    var serviceTypeName: String = ""
    lateinit var getServiceTypes: GetServiceTypes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSessionManager = LanguageSessionManager(act)
            if (arguments?.containsKey("GetServiceTypes")!!) {
                val gson = Gson()
                getServiceTypes = gson.fromJson(arguments?.getString("GetServiceTypes"),
                        GetServiceTypes::class.java)
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.add_service, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            linear_time_from = mainLayout.findViewById<View?>(R.id.linear_time_from) as LinearLayout
            linear_to_time = mainLayout.findViewById<View?>(R.id.linear_to_time) as LinearLayout
            linear_details = mainLayout.findViewById<View?>(R.id.linear_details) as LinearLayout
            linear_category = mainLayout.findViewById<View?>(R.id.linear_category) as LinearLayout
            tv_time_from = mainLayout.findViewById<View?>(R.id.tv_time_from) as TextView
            tv_to_time = mainLayout.findViewById<View?>(R.id.tv_to_time) as TextView
            tv_add_ad = mainLayout.findViewById<View?>(R.id.tv_add_ad) as TextView
            tv_category_label = mainLayout.findViewById<View?>(R.id.tv_category_label) as TextView
            et_details_label = mainLayout.findViewById<View?>(R.id.et_details_label) as EditText
            et_title = mainLayout.findViewById<View?>(R.id.et_title) as EditText
            et_price = mainLayout.findViewById<View?>(R.id.et_price) as EditText
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_add_ad.setOnClickListener(this)
            linear_category.setOnClickListener(this)
            tv_to_time.setOnClickListener(this)
            tv_time_from.setOnClickListener(this)
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.AddService)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSessionManager)
        GetServiceTypes()
        if (getServiceTypes != null) {
            serviceTypeId = getServiceTypes.Id!!
            if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                tv_category_label.setText(getServiceTypes.ServiceTypeNameEn)
                serviceTypeName = getServiceTypes.ServiceTypeNameEn!!
            } else {
                tv_category_label.setText(getServiceTypes.ServiceTypeNameAr)
                serviceTypeName = getServiceTypes.ServiceTypeNameAr!!
            }
        }
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.tv_to_time -> {
                val c = Calendar.getInstance()
                val hour = c[Calendar.HOUR_OF_DAY]
                val minute = c[Calendar.MINUTE]
                val timePickerDialog = TimePickerDialog(context, OnTimeSetListener { view, hourOfDay, minute ->
                    tv_to_time.text =
                            "${if (hourOfDay.toString().checkLengthString(1)) hourOfDay else "0$hourOfDay"}" +
                                    ":${if (minute.toString().checkLengthString(1)) minute else "0$minute"}"
                }, hour, minute, true)
                timePickerDialog.show()
            }
            R.id.tv_time_from -> {
                val c1 = Calendar.getInstance()
                val hour1 = c1[Calendar.HOUR_OF_DAY]
                val minute1 = c1[Calendar.MINUTE]
                val timePickerDialog1 = TimePickerDialog(context, OnTimeSetListener { view, hourOfDay, minute ->
                    tv_time_from.text =
                            "${if (hourOfDay.toString().checkLengthString(1)) hourOfDay else "0$hourOfDay"}" +
                                    ":${if (minute.toString().checkLengthString(1)) minute else "0$minute"}"
                }, hour1, minute1, true)
                timePickerDialog1.show()
            }
            R.id.linear_category -> if (getServiceTypesArrayList.size > 0) {
                val inflater1 = act.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(getString(R.string.SelectArea).replace("*", ""))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (serviceTypes in getServiceTypesArrayList) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        arrayList1.add(serviceTypes?.ServiceTypeNameEn)
                    } else {
                        arrayList1.add(serviceTypes?.ServiceTypeNameAr)
                    }
                }
                arrayList1.add(getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        serviceTypeId = getServiceTypesArrayList.get(i)?.Id!!
                        tv_category_label.setText(item)
                        serviceTypeName = item!!
                    }
                })
            }
            R.id.tv_add_ad -> if (/*et_details_label.getText().toString().length > 0
                    && et_title.getText().toString().length > 0
                    && serviceTypeName.length > 0
                    && tv_time_from.getText().length > 0
                    && tv_to_time.getText().length > 0
                    && et_price.getText().toString().length > 0*/
                    isValid()) {
                SendService()
            } /*else {
                Snackbar.make(mainLayout, act.getString(R.string.FillAllFields), Snackbar.LENGTH_LONG).show()
            }*/
        }
    }

    private fun isValid(): Boolean {
        when {
            et_title.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_title))
                et_title.requestFocus()
                return false
            }
            et_price.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_price))
                et_price.requestFocus()
                return false
            }
            tv_time_from.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.select_from_time))
                tv_time_from.requestFocus()
                return false
            }
            tv_to_time.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.select_to_time))
                tv_to_time.requestFocus()
                return false
            }
            tv_time_from.getTextValue() >= tv_to_time.getTextValue() -> {
                mainLayout.showSnakeBar(getString(R.string.err_time_before_after))
                return false
            }
            et_details_label.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_details))
                et_details_label.requestFocus()
                return false
            }
        }
        return true
    }

    fun GetServiceTypes() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetServiceTypes()?.enqueue(
                object : Callback<ArrayList<GetServiceTypes>?> {

                    override fun onFailure(call: Call<ArrayList<GetServiceTypes>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetServiceTypes>?>, response: Response<ArrayList<GetServiceTypes>?>) {
                        if (response.body() != null) {
                            val getServiceTypes = response.body()
                            if (mloading != null && getServiceTypes != null) {
                                Log.d("getContentses size", "" + getServiceTypes.size)
                                getServiceTypesArrayList.clear()
                                getServiceTypesArrayList.addAll(getServiceTypes)
                            }
                        }
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    private fun SendService() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.SendService(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                GlobalFunctions.EncodeParameter(serviceTypeId),
                GlobalFunctions.EncodeParameter(et_title.getText().toString()),
                GlobalFunctions.EncodeParameter(et_title.getText().toString()),
                GlobalFunctions.EncodeParameter(et_details_label.getText().toString()),
                GlobalFunctions.EncodeParameter(et_details_label.getText().toString()),
                mSessionManager.getUserCode(),
                GlobalFunctions.EncodeParameter(et_price.getText().toString()),
                GlobalFunctions.EncodeParameter(tv_time_from.getText().toString()),
                GlobalFunctions.EncodeParameter(tv_to_time.getText().toString()))?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.trim { it <= ' ' }.replace("\"", "")
                            val responseInteger = outResponse.toInt()
                            if (responseInteger > 0) {
                                Snackbar.make(mainLayout, act.getString(R.string.ContentAddedLabel), Snackbar.LENGTH_LONG).show()
                                fragmentManager?.popBackStack()
                            } else {
                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    companion object {
        protected val TAG = AddServiceFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: AddServiceFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): AddServiceFragment {
            fragment = AddServiceFragment()
            Companion.act = act
            return fragment
        }
    }
}