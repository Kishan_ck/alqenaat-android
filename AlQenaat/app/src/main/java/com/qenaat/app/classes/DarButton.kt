package com.qenaat.app.classes

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import com.qenaat.app.AppController

/**
 * Created by DELL on 11/08/2016.
 */
class DarButton : androidx.appcompat.widget.AppCompatButton {
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context!!, attrs, defStyle) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
        init()
    }

    constructor(context: Context?) : super(context!!) {
        init()
    }

    private fun init() {
        if (!isInEditMode()) {
            if (AppController.Companion.getInstance()?.getLocale() == "ar") {
                val tf = Typeface.createFromAsset(getContext().getAssets(), "arabic_font.ttf")
                setTypeface(tf)
            } else {
                val tf = Typeface.createFromAsset(getContext().getAssets(), "english_font.ttf")
                setTypeface(tf)
            }
        }
    }
}