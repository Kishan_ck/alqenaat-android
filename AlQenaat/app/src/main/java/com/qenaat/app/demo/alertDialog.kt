package com.qenaat.app.demo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.fragments.SubscriptionInvoiceFragment
import com.qenaat.app.model.GetMonthlyDonationSubscriptionInvoices
import com.qenaat.app.model.GetMonthlyDonations
import com.qenaat.app.networking.QenaatAPICall
import company.tap.gosellapi.SettingsManager
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.PhoneNumber
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.internal.interfaces.PaymentInterface
import company.tap.gosellapi.open.buttons.PayButtonView
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.CardType
import company.tap.gosellapi.open.enums.TransactionMode
import company.tap.gosellapi.open.models.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.math.BigDecimal
import java.time.Instant
import java.util.ArrayList
import java.util.HashMap

class alertDialog : AppCompatActivity(), PaymentInterface, SessionDelegate {

    private lateinit var sdkSession: SDKSession
    private var settingsManager: SettingsManager? = null
    private val SDK_REQUEST_CODE = 1001
    lateinit var mloading: ProgressBar
    lateinit var mSessionManager: SessionManager
    var idCurrent: String = "";
    lateinit var getMonthlyDonations: GetMonthlyDonations
    lateinit var mainLayout: RelativeLayout




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pay_dialog)

        mSessionManager = SessionManager(this)

        init()
        configureSDKSession()
    }

    private fun configureSDKSession() {

        // Instantiate SDK Session
        sdkSession = SDKSession(this) //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession.setTransactionCurrency(TapCurrency("KWD")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession.setAmount(BigDecimal(0)) //** Required **

        // Set Payment Items array list
        sdkSession.setPaymentItems(ArrayList<PaymentItem>()) // ** Optional ** you can pass empty array list


//       sdkSession.setPaymentType("CARD");   //** Merchant can pass paymentType

        // Set Taxes array list
        sdkSession.setTaxes(ArrayList<Tax>()) // ** Optional ** you can pass empty array list

        // Set Shipping array list
        sdkSession.setShipping(ArrayList<Shipping>()) // ** Optional ** you can pass empty array list

        // Post URL
        sdkSession.setPostURL("") // ** Optional **

        // Payment Description
        sdkSession.setPaymentDescription("") //** Optional **

        // Payment Extra Info
        sdkSession.setPaymentMetadata(HashMap<String, String>()) // ** Optional ** you can pass empty array hash map

        // Payment Reference
        sdkSession.setPaymentReference(null) // ** Optional ** you can pass null

        // Payment Statement Descriptor
        sdkSession.setPaymentStatementDescriptor("") // ** Optional **

        // Enable or Disable Saving Card
        sdkSession.isUserAllowedToSaveCard(true) //  ** Required ** you can pass boolean

        // Enable or Disable 3DSecure
        sdkSession.isRequires3DSecure(true)

        //Set Receipt Settings [SMS - Email ]
        sdkSession.setReceiptSettings(
            Receipt(
                false,
                false
            )
        ) // ** Optional ** you can pass Receipt object or null

        // Set Authorize Action
        sdkSession.setAuthorizeAction(null) // ** Optional ** you can pass AuthorizeAction object or null
        sdkSession.setDestination(null) // ** Optional ** you can pass Destinations object or null
        sdkSession.setMerchantID(null) // ** Optional ** you can pass merchant id or null
        sdkSession.setCardType(CardType.CREDIT) // ** Optional ** you can pass which cardType[CREDIT/DEBIT] you want.By default it loads all available cards for Merchant.

        // sdkSession.setDefaultCardHolderName("TEST TAP"); // ** Optional ** you can pass default CardHolderName of the user .So you don't need to type it.
        // sdkSession.isUserAllowedToEnableCardHolderName(false); // ** Optional ** you can enable/ disable  default CardHolderName .
    }

    private fun getCustomer(): Customer { // test customer id cus_Kh1b4220191939i1KP2506448
        val customer = if ((settingsManager != null)) settingsManager?.getCustomer() else null
        val phoneNumber =
            if (customer != null) customer.getPhone() else PhoneNumber("965", "69045932")
        return Customer.CustomerBuilder(null).email("abc@abc.com").firstName("firstname")
            .lastName("lastname").metadata("")
            .phone(PhoneNumber(phoneNumber?.getCountryCode(), phoneNumber?.getNumber()))
            .middleName("middlename").build()
    }

    private fun startSDKWithUI() {
        if (sdkSession != null) {
            val trx_mode =
                if (settingsManager != null) settingsManager?.getTransactionsMode("key_sdk_transaction_mode") else TransactionMode.PURCHASE
            // set transaction mode [TransactionMode.PURCHASE - TransactionMode.AUTHORIZE_CAPTURE - TransactionMode.SAVE_CARD - TransactionMode.TOKENIZE_CARD ]
            sdkSession.setTransactionMode(trx_mode) //** Required **
            // if you are not using tap button then start SDK using the following call
            //sdkSession.start(this);
        }
    }

    private fun initPayButton(payButtonView: PayButtonView) {
        Log.e("paymentclickkk", "4>>>>>")

        if (ThemeObject.getInstance().payButtonFont != null) payButtonView.setupFontTypeFace(
            ThemeObject.getInstance().payButtonFont
        )
        if (ThemeObject.getInstance().payButtonDisabledTitleColor != 0 && ThemeObject.getInstance().payButtonEnabledTitleColor != 0) payButtonView.setupTextColor(
            ThemeObject.getInstance().payButtonEnabledTitleColor,
            ThemeObject.getInstance().payButtonDisabledTitleColor
        )
        if (ThemeObject.getInstance().payButtonTextSize != 0) payButtonView.getPayButton()
            .setTextSize(ThemeObject.getInstance().payButtonTextSize.toFloat())
        //
        if (ThemeObject.getInstance().isPayButtSecurityIconVisible) payButtonView.getSecurityIconView()
            .setVisibility(if (ThemeObject.getInstance().isPayButtSecurityIconVisible) View.VISIBLE else View.INVISIBLE)
        if (ThemeObject.getInstance().payButtonResourceId != 0) payButtonView.setBackgroundSelector(
            ThemeObject.getInstance().payButtonResourceId
        )
        if (sdkSession != null) {
            val trx_mode: TransactionMode = sdkSession.getTransactionMode()
            if (trx_mode != null) {
                payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.pay))
            } else {
                startSDKWithUI()
            }
            sdkSession.setButtonView(payButtonView, this, SDK_REQUEST_CODE)
        }
    }

    private fun init() {
        Toast.makeText(applicationContext, "Clickkkeddd2", Toast.LENGTH_SHORT).show()

//        if (applicationContext.applicationInfo.targetSdkVersion <= 26)
//            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
//        if (window != null) {
//            window.setLayout(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT
//            );
//            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        }

//        mloading = findViewById(R.id.loading_progress) as ProgressBar
//        sdkSession.setAmount(BigDecimal(intent.getStringExtra("amount")))
        idCurrent = intent.getStringExtra("paymentID")

        var payButtonView = findViewById<PayButtonView>(R.id.pay_now_dialog)
        var message = findViewById<TextView>(R.id.text_message)
        message.text =
            resources.getString(R.string.pay_message) + " ${intent.getStringExtra("amount")}"

//        initPayButton(payButtonView)
//        payButtonView.setOnClickListener(View.OnClickListener {
//            val intent = Intent()
//            intent.putExtra("ispayment",true)
////                intent.putExtra("type","app")
//            setResult(Activity.RESULT_OK, intent)
//            finish()
//            Log.e("ispayment","ispaymentttttt2")
//
//        })

    }

    fun GetMonthlyDonationsInvoices() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
//        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)

        Log.e("ohkohk", "" + getMonthlyDonations.Id);

        QenaatAPICall.getCallingAPIInterface()?.GetMonthlyDonationsInvoices(
            "-1",
            getMonthlyDonations.Id
        )?.enqueue(
            object : Callback<ArrayList<GetMonthlyDonationSubscriptionInvoices>?> {

                override fun onFailure(
                    call: Call<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>,
                    t: Throwable
                ) {
                    t.printStackTrace()
//                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>,
                    response: Response<ArrayList<GetMonthlyDonationSubscriptionInvoices>?>
                ) {
                    Log.e("ohkohk", "" + call.request().url)
                    if (response.body() != null) {
                        val subscriptionInvoices = response.body()

                        startActivity(Intent(applicationContext, SubscriptionInvoiceFragment::class.java))

                    }
                }
            })
    }


    override fun onPayment() {
        Log.e("OKOKOK", "onPayment");
        sdkSession.isValid = true

    }

    override fun paymentSucceed(charge: Charge) {
//            mloading.setVisibility(View.VISIBLE)
            QenaatAPICall.getCallingAPIInterface()?.monthlySubscriptionInvoiceDonate(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                idCurrent, "3", getMonthlyDonations.Id, charge.amount.toString(),
                charge.id.toString(), mSessionManager.getUserCode()
            )?.enqueue(object : Callback<ResponseBody?> {
                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
//                    mloading.setVisibility(View.GONE);
                    Log.e("OKOKOK", "" + t.message)
//                    Snackbar.make(applicationContext, getString(R.string.someErr), Snackbar.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
//                    mloading.setVisibility(View.GONE)
                    var ok = response.body()?.string();
                    if (response.code() == 200) {
                        if (ok.equals("\"1\"")) {
//                            Snackbar.make(this, getString(R.string.payment_succeeded), Snackbar.LENGTH_LONG).show()
                            GetMonthlyDonationsInvoices()
                        } else {
//                            Snackbar.make(applicationContext, getString(R.string.money_deduct_someErr), Snackbar.LENGTH_LONG).show()
                            GetMonthlyDonationsInvoices()
                        }

                    } else {
//                        Snackbar.make(applicationContext, getString(R.string.money_deduct_someErr), Snackbar.LENGTH_LONG).show()
                    }

                }

            })


        }

    override fun paymentFailed(charge: Charge?) {
        Snackbar.make(mainLayout, SubscriptionInvoiceFragment.act.getString(R.string.someErr), Snackbar.LENGTH_LONG).show()

    }

    override fun sessionCancelled() {
        Log.e("OKOKOK", "sessionCancelled");
    }

    override fun savedCardsList(cardsList: CardsList) {
        Log.e("OKOKOK", "savedCardsList");

    }

    override fun sessionIsStarting() {
//        dialog.cancel()
    }

    override fun invalidCardDetails() {
        Snackbar.make(mainLayout, SubscriptionInvoiceFragment.act.getString(R.string.carddetail_invalid), Snackbar.LENGTH_LONG)
            .show()
    }

    override fun cardSavingFailed(charge: Charge) {
        Log.e("OKOKOK", "cardSavingFailed");
    }

    override fun backendUnknownError(message: String?) {
        Log.e("OKOKOK", "backendUnknownError  " + message);
    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {
        Log.e("OKOKOK", "userEnabledSaveCardOption  ");

    }

    override fun cardSaved(charge: Charge) {
        Log.e("OKOKOK", "cardSaved");

    }
    override fun authorizationFailed(authorize: Authorize?) {
        Log.e("OKOKOK", "authorizationFailed");
    }

    override fun cardTokenizedSuccessfully(token: Token) {
        Log.e("OKOKOK", "cardTokenizedSuccessfully");
    }

    override fun authorizationSucceed(authorize: Authorize) {
        Log.e("OKOKOK", "authorizationSucceed");
    }

    override fun invalidTransactionMode() {
        Log.e("OKOKOK", "invalidTransactionMode");
    }

    override fun sdkError(goSellError: GoSellError?) {

        Log.e("OKOKOK", "sdkError ::" + goSellError?.errorMessage);
    }

    override fun sessionFailedToStart() {
        Log.e("OKOKOK", "sdkError");
    }

    override fun sessionHasStarted() {
        Log.e("OKOKOK", "sessionHasStarted");

    }

    override fun invalidCustomerID() {
        Log.e("OKOKOK", "invalidCustomerID");
    }

}