package com.qenaat.app.model

import java.util.*

/**
 * Created by DELL on 14-May-17.
 */
class GetFamilyTree {
    var VIPDetailsAR: String? = null
    var UserId: String? = null
    var IsSelected: String? = null
    var ThirdNameAr: String? = null
    var ThirdNameEn: String? = null
    var Block: String? = null
    var VIPDetailsEN: String? = null
    var PositionNameAR: String? = null
    var PositionNameEN: String? = null
    var Id: String? = null
    var ParentNameEn: String? = null
    var CityNameEn: String? = null
    var CityNameAr: String? = null
    var CivilIdNo: String? = null
    var IsBranch: String? = null
    var CityId: String? = null
    var AreaId: String? = null
    var AreaNameEn: String? = null
    var AreaNameAr: String? = null
    var ParentNameAr: String? = null
    var ParentId: String? = null
    var AddressEN: String? = null
    var AddressAR: String? = null
    var Color: String? = null
    var IsLive: String? = null
    var IsVip: String? = null
    var BirthDate: String? = null
    var DeathDate: String? = null
    var Gender: String? = null
    var FamilyNameAr: String? = null
    var FamilyNameEn: String? = null
    var IsParent: String? = null
    var Phone: String? = null
    var Email: String? = null
    var FaceBook: String? = null
    var FamilyNameId: String? = null
    var Details: String? = null
    var Instgram: String? = null
    var Twitter: String? = null
    var HaveChild: String? = null
    var Contacts: String? = null
    var Childs: ArrayList<GetFamilyTree?>? = null
    var NameEn: String? = null
    var NameAr: String? = null
    var Photo: String? = null
    var SecondName: String? = null
    var ThirdName: String? = null
    var FourthName: String? = null
    var FifthName: String? = null
    var CountryCode: String? = null

}