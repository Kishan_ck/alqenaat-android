package com.qenaat.app.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.ServiceAdapter
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.classesimport.SpacesItemDecoration
import com.qenaat.app.model.GetServices
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_my_account.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 06-Dec-17.
 */
class ServicesMyAccountFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var my_recycler_view: RecyclerView
    lateinit var mloading: ProgressBar
    var page_index = 0
    var endOfREsults = false
    private val loading_flag = true
    lateinit private var mLayoutManager: LinearLayoutManager
    var getServicesArrayList: ArrayList<GetServices> = ArrayList()
    var getServicesArrayList1: ArrayList<GetServices> = ArrayList()
    var getServicesArrayList2: ArrayList<GetServices> = ArrayList()
    var getServicesArrayList3: ArrayList<GetServices> = ArrayList()
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    lateinit var progress_loading_more: ProgressBar
    var type: String? = ""
    lateinit var linear_latest_ads: LinearLayout
    lateinit var linear_search: LinearLayout
    lateinit var linear_location: LinearLayout
    lateinit var tv_latest_ads: TextView
    lateinit var tv_search: TextView
    lateinit var tv_location: TextView
    lateinit var img_bg: ImageView
    lateinit var img_line1: ImageView
    lateinit var img_line2: ImageView
    lateinit var relative_top: RelativeLayout
    lateinit var relative_latest_ads: RelativeLayout
    lateinit var relative_search: RelativeLayout
    lateinit var relative_location: RelativeLayout
    var tabNumber = 1
    var imgW = 0
    var imgH = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                type = arguments!!.getString("type")
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.service_my_account, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            relative_latest_ads = mainLayout.findViewById(R.id.relative_latest_ads) as RelativeLayout
            relative_search = mainLayout.findViewById(R.id.relative_search) as RelativeLayout
            relative_location = mainLayout.findViewById(R.id.relative_location) as RelativeLayout
            relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
            linear_search = mainLayout.findViewById(R.id.linear_search) as LinearLayout
            linear_location = mainLayout.findViewById(R.id.linear_location) as LinearLayout
            linear_latest_ads = mainLayout.findViewById(R.id.linear_latest_ads) as LinearLayout
            tv_search = mainLayout.findViewById(R.id.tv_search) as TextView
            tv_location = mainLayout.findViewById(R.id.tv_location) as TextView
            tv_latest_ads = mainLayout.findViewById(R.id.tv_latest_ads) as TextView
            img_line1 = mainLayout.findViewById(R.id.img_line1) as ImageView
            img_line2 = mainLayout.findViewById(R.id.img_line2) as ImageView
            img_bg = mainLayout.findViewById(R.id.img_bg) as ImageView
            my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
            mLayoutManager = LinearLayoutManager(activity)
            my_recycler_view.addItemDecoration(SpacesItemDecoration(10))
            my_recycler_view.setLayoutManager(mLayoutManager)
            my_recycler_view.setItemAnimator(DefaultItemAnimator())
            relative_search.setOnClickListener(this)
            relative_location.setOnClickListener(this)
            relative_top.setOnClickListener(this)
            relative_latest_ads.setOnClickListener(this)
            relative_top.setVisibility(View.GONE)
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.ServicesLabel))

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        if (getServicesArrayList1.size > 0 || getServicesArrayList2.size > 0 || getServicesArrayList3.size > 0) {
            setTabs(tabNumber)
        } else {
            if (tabNumber == 1) {
                GetServices()
            } else {
                GetServicesRequestORecived()
            }
        }
    }

    fun GetServices() {
        var type = "0"
        if (tabNumber == 1) {
            type = "0"
        }
        if (tabNumber == 2) {
            type = "1"
        }
        if (tabNumber == 3) {
            type = "2"
        }
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetServices(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager?.getAuthToken()}",
                mSessionManager.getUserCode(), "0", "0", "-1", type)?.enqueue(
                object : Callback<ArrayList<GetServices>?> {
                    override fun onFailure(call: Call<ArrayList<GetServices>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetServices>?>, response: Response<ArrayList<GetServices>?>) {
                        if (response.body() != null) {
                            val getServices = response.body()
                            if (mloading != null) {
                                Log.d("getServices size", "" + getServices?.size)
                                if (getServices?.size == 0)
                                    tv_noDataFound?.visibility = View.VISIBLE
                                else
                                    tv_noDataFound?.visibility = View.GONE
                                if (tabNumber == 1) {
                                    getServicesArrayList1.clear()
                                    getServicesArrayList1.addAll(getServices!!)
                                    Log.d("getServices size", "" + getServicesArrayList1.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)

                                    // setupCategoryTab();
                                    mAdapter = ServiceAdapter(act, getServicesArrayList1, tabNumber)
                                }
                                if (tabNumber == 2) {
                                    getServicesArrayList2.clear()
                                    getServicesArrayList2.addAll(getServices!!)
                                    Log.d("getServices size", "" + getServicesArrayList2.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)

                                    // setupCategoryTab();
                                    mAdapter = ServiceAdapter(act, getServicesArrayList2, tabNumber)
                                }
                                if (tabNumber == 3) {
                                    getServicesArrayList3.clear()
                                    getServicesArrayList3.addAll(getServices!!)
                                    Log.d("getServices size", "" + getServicesArrayList3.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)

                                    // setupCategoryTab();
                                    mAdapter = ServiceAdapter(act, getServicesArrayList3, tabNumber)
                                }
                                my_recycler_view.setAdapter(mAdapter)
                            }
                        }
                    }
                })
    }

    fun GetServicesRequestORecived() {
        var type = "0"
        if (tabNumber == 1) {
            type = "0"
        }
        if (tabNumber == 2) {
            type = "1"
        }
        if (tabNumber == 3) {
            type = "2"
        }
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetServicesRequestORecived(
                mSessionManager.getUserCode(), type, "-1")?.enqueue(
                object : Callback<ArrayList<GetServices>?> {

                    override fun onFailure(call: Call<ArrayList<GetServices>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetServices>?>, response: Response<ArrayList<GetServices>?>) {
                        if (response.body() != null) {
                            val getServices = response.body()
                            if (mloading != null) {
                                Log.d("getServices size", "" + getServices?.size)
                                if (tabNumber == 1) {
                                    getServicesArrayList1.clear()
                                    getServicesArrayList1.addAll(getServices!!)
                                    Log.d("getServices size", "" + getServicesArrayList1.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)

                                    // setupCategoryTab();
                                    mAdapter = ServiceAdapter(act, getServicesArrayList1, tabNumber)
                                }
                                if (tabNumber == 2) {
                                    getServicesArrayList2.clear()
                                    getServicesArrayList2.addAll(getServices!!)
                                    Log.d("getServices size", "" + getServicesArrayList2.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)

                                    // setupCategoryTab();
                                    mAdapter = ServiceAdapter(act, getServicesArrayList2, tabNumber)
                                }
                                if (tabNumber == 3) {
                                    getServicesArrayList3.clear()
                                    getServicesArrayList3.addAll(getServices!!)
                                    Log.d("getServices size", "" + getServicesArrayList3.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)

                                    // setupCategoryTab();
                                    mAdapter = ServiceAdapter(act, getServicesArrayList3, tabNumber)
                                }
                                my_recycler_view.setAdapter(mAdapter)
                            }
                        }
                    }
                })
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.relative_search -> setTabs(2)
            R.id.relative_location -> setTabs(3)
            R.id.relative_top -> {
            }
            R.id.relative_latest_ads -> setTabs(1)
        }
    }

    private fun setTabs(tab: Int) {
        when (tab) {
            1 -> {
                tabNumber = 1
                if (getServicesArrayList1.size > 0) {
                    mAdapter = ServiceAdapter(act, getServicesArrayList1, tabNumber)
                    my_recycler_view.setAdapter(mAdapter)
                } else {
                    GetServices()
                }
                my_recycler_view.setVisibility(View.VISIBLE)
                relative_latest_ads.setBackgroundColor(Color.parseColor("#87776f"))
                tv_latest_ads.setTextColor(Color.parseColor("#fff4e4"))
                relative_search.setBackgroundColor(Color.TRANSPARENT)
                tv_search.setTextColor(Color.parseColor("#87776f"))
                relative_location.setBackgroundColor(Color.TRANSPARENT)
                tv_location.setTextColor(Color.parseColor("#87776f"))
            }
            2 -> {
                tabNumber = 2
                if (getServicesArrayList2.size > 0) {
                    mAdapter = ServiceAdapter(act, getServicesArrayList2, tabNumber)
                    my_recycler_view.setAdapter(mAdapter)
                } else {
                    GetServicesRequestORecived()
                }
                my_recycler_view.setVisibility(View.VISIBLE)
                relative_latest_ads.setBackgroundColor(Color.TRANSPARENT)
                tv_latest_ads.setTextColor(Color.parseColor("#87776f"))
                relative_search.setBackgroundColor(Color.parseColor("#87776f"))
                tv_search.setTextColor(Color.parseColor("#fff4e4"))
                relative_location.setBackgroundColor(Color.TRANSPARENT)
                tv_location.setTextColor(Color.parseColor("#87776f"))
            }
            3 -> {
                tabNumber = 3
                if (getServicesArrayList3.size > 0) {
                    mAdapter = ServiceAdapter(act, getServicesArrayList3, tabNumber)
                    my_recycler_view.setAdapter(mAdapter)
                } else {
                    GetServicesRequestORecived()
                }
                my_recycler_view.setVisibility(View.VISIBLE)
                relative_latest_ads.setBackgroundColor(Color.TRANSPARENT)
                tv_latest_ads.setTextColor(Color.parseColor("#87776f"))
                relative_search.setBackgroundColor(Color.TRANSPARENT)
                tv_search.setTextColor(Color.parseColor("#87776f"))
                relative_location.setBackgroundColor(Color.parseColor("#87776f"))
                tv_location.setTextColor(Color.parseColor("#fff4e4"))
            }
        }
    }

    companion object {
        protected val TAG = ServicesMyAccountFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: ServicesMyAccountFragment
        lateinit var mAdapter: RecyclerView.Adapter<*>
        fun newInstance(act: FragmentActivity): ServicesMyAccountFragment {
            fragment = ServicesMyAccountFragment()
            Companion.act = act
            return fragment
        }
    }
}