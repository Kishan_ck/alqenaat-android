package com.qenaat.app.fragments

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetServices
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by DELL on 15-Nov-17.
 */
class ServiceDetailsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSessionManager: LanguageSessionManager
    lateinit var tv_category_name: TextView
    lateinit var tv_title: TextView
    lateinit var tv_time: TextView
    lateinit var tv_description: TextView
    lateinit var tv_price: TextView
    lateinit var tv_request: TextView
    lateinit var getServices: GetServices
    var tabNumber = 1
    lateinit var relative_contact: RelativeLayout
    lateinit var tv_line: TextView
    lateinit var tv_contact_details_label: TextView
    lateinit var tv_name: TextView
    lateinit var tv_phone: TextView
    lateinit var tv_email: TextView
    lateinit var tv_name_label: TextView
    lateinit var tv_phone_label: TextView
    lateinit var tv_email_label: TextView
    var permissionStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSessionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetServices")) {
                    val gson = Gson()
                    getServices = gson.fromJson(arguments!!.getString("GetServices"),
                            GetServices::class.java)
                    tabNumber = arguments!!.getInt("tabNumber", 1)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.services_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout?) {
        if (mainLayout != null) {
            tv_name_label = mainLayout.findViewById(R.id.tv_name_label) as TextView
            tv_name_label.setOnClickListener(this)
            tv_phone_label = mainLayout.findViewById(R.id.tv_phone_label) as TextView
            tv_phone_label.setOnClickListener(this)
            tv_email_label = mainLayout.findViewById(R.id.tv_email_label) as TextView
            tv_email_label.setOnClickListener(this)
            tv_line = mainLayout.findViewById(R.id.tv_line) as TextView
            tv_contact_details_label = mainLayout.findViewById(R.id.tv_contact_details_label) as TextView
            tv_name = mainLayout.findViewById(R.id.tv_name) as TextView
            tv_phone = mainLayout.findViewById(R.id.tv_phone) as TextView
            tv_phone.setOnClickListener(this)
            relative_contact = mainLayout.findViewById(R.id.relative_contact) as RelativeLayout
            tv_title = mainLayout.findViewById(R.id.tv_title) as TextView
            tv_email = mainLayout.findViewById(R.id.tv_email) as TextView
            tv_email.setOnClickListener(this)
            tv_time = mainLayout.findViewById(R.id.tv_time) as TextView
            tv_description = mainLayout.findViewById(R.id.tv_description) as TextView
            tv_price = mainLayout.findViewById(R.id.tv_price) as TextView
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            tv_request = mainLayout.findViewById(R.id.tv_request) as TextView
            tv_category_name = mainLayout.findViewById(R.id.tv_category_name) as TextView
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_request.setOnClickListener(this)
            tv_email_label.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_phone_label.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_name_label.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
        }
    }

    override fun onStart() {
        super.onStart()
        tv_request.setVisibility(View.GONE)
        relative_contact.setVisibility(View.VISIBLE)

//        if(tabNumber == 0){
//
//            tv_request.setVisibility(View.VISIBLE);
//
//            relative_contact.setVisibility(View.GONE);
//
//        }
//
//        if(tabNumber == 1){
//
//            tv_request.setVisibility(View.GONE);
//
//            relative_contact.setVisibility(View.GONE);
//
//        }
        ContentActivity.Companion.mtv_topTitle.setText(R.string.serviceDetailsLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.tabType = 1
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
//        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
//        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)



        ContentActivity.Companion.enableLogin(languageSessionManager)
        GetServiceById()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.tv_email_label, R.id.tv_email -> if (getServices.UserEmail!!.length > 0) {
                val email: String = getServices.UserEmail.toString()
                val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto",
                        email, null))
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, act.getString(R.string.app_name))
                emailIntent.putExtra(Intent.EXTRA_TEXT, "")
                startActivity(Intent.createChooser(emailIntent, "Send email..."))
            }
            R.id.tv_phone_label, R.id.tv_phone -> {
                if (getServices.UserPhone!!.isNotEmpty()) {
                    /* if (GlobalFunctions.isReadCallAllowed(act)) {
                        val intent = Intent(Intent.ACTION_CALL)
                        intent.setData(Uri.parse("tel:" + getServices.UserPhone))
                        act.startActivity(intent)
                         return
                     }*/
                    permissionStr = Manifest.permission.CALL_PHONE
                    GlobalFunctions.requestCallPermission(act)
                } else
                    FixControl.showToast(act, getString(R.string.no_number_found))
            }
            R.id.tv_request -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                RequestService()
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.CALL_PERMISSION_CODE -> {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.setData(Uri.parse("tel:" + getServices.UserPhone))
                    act.startActivity(intent)
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act)
        }
    }

    fun GetServiceById() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetServices("", "0",
                getServices.Id, "0", "0", "0")?.enqueue(
                object : Callback<ArrayList<GetServices>?> {

                    override fun onFailure(call: Call<ArrayList<GetServices>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetServices>?>, response: Response<ArrayList<GetServices>?>) {
                        if (response.body() != null) {
                            val getContentses = response.body()
                            if (mloading != null) {
                                if (getContentses != null) {
                                    Log.d("shippingCompanies size", "" + getContentses.size)
                                    if (getContentses.size > 0) {
                                        getServices = getContentses[0]!!
                                        tv_name.setVisibility(View.GONE)
                                        tv_contact_details_label.setVisibility(View.GONE)
                                        tv_line.setVisibility(View.GONE)
                                        tv_phone.setVisibility(View.GONE)
                                        tv_email.setVisibility(View.GONE)
                                        if (getServices.UserName!!.length > 0) {
                                            tv_line.setVisibility(View.VISIBLE)
                                            tv_name.setVisibility(View.VISIBLE)
                                            tv_name.setText(getServices.UserName)
                                            tv_contact_details_label.setVisibility(View.VISIBLE)
                                        }
                                        if (getServices.UserPhone!!.length > 0) {
                                            tv_line.setVisibility(View.VISIBLE)
                                            tv_phone.setVisibility(View.VISIBLE)
                                            tv_phone.setText(getServices.UserPhone)
                                            tv_contact_details_label.setVisibility(View.VISIBLE)
                                        }
                                        if (getServices.UserEmail!!.length > 0) {
                                            tv_line.setVisibility(View.VISIBLE)
                                            tv_email.setVisibility(View.VISIBLE)
                                            tv_email.setText(getServices.UserEmail)
                                            tv_contact_details_label.setVisibility(View.VISIBLE)
                                        }
                                        Log.d("getContentsArray size", "" + getContentses.size)
                                        tv_price.setText(getServices.ServicePrice + " " + act.getString(R.string.KD))
                                        if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                                            tv_category_name.setText(getServices.TypeNameEn)
                                            tv_description.setText(getServices.DescribtionEn)
                                            tv_title.setText(getServices.NameEn)
                                            tv_time.setText(getServices.FromTime + " - " + getServices.ToTime)
                                        } else {
                                            tv_category_name.setText(getServices.TypeNameAr)
                                            tv_description.setText(getServices.DescribtionAr)
                                            tv_title.setText(getServices.NameAr)
                                            tv_time.setText(getServices.FromTime + " - " + getServices.ToTime)
                                        }
                                    }
                                }
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    private fun RequestService() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.RequestService(
                getServices.Id, mSessionManager.getUserCode())?.enqueue(object : Callback<ResponseBody?> {

            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                t.printStackTrace()
                //mloading.setVisibility(View.INVISIBLE);
                //GlobalFunctions.EnableLayout(mainLayout);
            }

            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                val body = response?.body()
                var outResponse = ""
                try {
                    val reader = BufferedReader(InputStreamReader(
                            ByteArrayInputStream(body?.bytes())))
                    val out = StringBuilder()
                    val newLine = System.getProperty("line.separator")
                    var line: String?
                    while (reader.readLine().also { line = it } != null) {
                        out.append(line)
                        out.append(newLine)
                    }
                    outResponse = out.toString()
                    Log.d("outResponse", "" + outResponse)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
                if (outResponse != null) {
                    outResponse = outResponse.trim { it <= ' ' }.replace("\"", "")
                    val responseInteger = outResponse.toInt()
                    if (responseInteger > 0) {
                        Snackbar.make(mainLayout, act.getString(R.string.RequestSent), Snackbar.LENGTH_LONG).show()
                    } else {
                        Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                    }
                }
                mloading.setVisibility(View.INVISIBLE)
                GlobalFunctions.EnableLayout(mainLayout)
            }
        })
    }

    companion object {
        protected val TAG = ServiceDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: ServiceDetailsFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): ServiceDetailsFragment {
            fragment = ServiceDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}