package com.qenaat.app.model

import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by DELL on 31-Oct-17.
 */
class GetDewanias {
    var Id: String? = null
    var TitleEn: String? = null
    var TitleAr: String? = null
    var DetailsEn: String? = null
    var DetailsAr: String? = null
    var AreaTitleEn: String? = null
    var AreaTitleAr: String? = null
    var RequestFullName: String? = null
    var RequestPhone: String? = null
    var Longitude: String? = null
    var Latitude: String? = null
    var Phone: String? = null
    var YouTube: String? = null
    var Facebook: String? = null
    var Twitter: String? = null
    var Instagram: String? = null
    var Ordering: String? = null
    var IsHouse: String? = null
    var IsActive: String? = null
    var ViewersCount: String? = null
    var AreaId: String? = null
    var WeekId: String? = null
    var WeekDays: ArrayList<weekDays?>? = null
    var Photos: ArrayList<photos?>? = ArrayList()

    inner class weekDays {
        var TitleEn: String? = null
        var TitleAr: String? = null
    }

    inner class photos {
        var Id: String? = null
        var Photo: String? = null
    }
}