package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetAboutUs
import com.qenaat.app.networking.QenaatAPICall
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by DELL on 30-Oct-17.
 */
class DonateFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSessionManager: LanguageSessionManager
    lateinit var img_bg1: ImageView
    lateinit var img_quick_donate: ImageView
    lateinit var img_bg2: ImageView
    lateinit var img_zakaat: ImageView
    lateinit var img_bg3: ImageView
    lateinit var img_monthly_donate: ImageView
    lateinit var img_bg4: ImageView
    lateinit var img_donate_for_project: ImageView
    lateinit var img_bg5: ImageView
    lateinit var img_donate_for_cause: ImageView
    lateinit var img_bg6: ImageView
    lateinit var img_request_help: ImageView
    lateinit var relative_quick_donate: RelativeLayout
    lateinit var relative_zakaat: RelativeLayout
    lateinit var relative_monthly_donate: RelativeLayout
    lateinit var relative_donate_for_project: RelativeLayout
    lateinit var relative_donate_for_cause: RelativeLayout
    lateinit var relative_request_help: RelativeLayout
    lateinit var tv_quick_donate: TextView
    lateinit var tv_zakaat: TextView
    lateinit var tv_monthly_donate: TextView
    lateinit var tv_donate_for_project: TextView
    lateinit var tv_donate_for_cause: TextView
    lateinit var tv_request_help: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSessionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.donate, null) as RelativeLayout

        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            tv_zakaat = mainLayout.findViewById<View?>(R.id.tv_zakaat) as TextView
            tv_monthly_donate = mainLayout.findViewById<View?>(R.id.tv_monthly_donate) as TextView
            tv_donate_for_project = mainLayout.findViewById<View?>(R.id.tv_donate_for_project) as TextView
            tv_donate_for_cause = mainLayout.findViewById<View?>(R.id.tv_donate_for_cause) as TextView
            tv_request_help = mainLayout.findViewById<View?>(R.id.tv_request_help) as TextView
            tv_quick_donate = mainLayout.findViewById<View?>(R.id.tv_quick_donate) as TextView
            relative_zakaat = mainLayout.findViewById<View?>(R.id.relative_zakaat) as RelativeLayout
            relative_monthly_donate = mainLayout.findViewById<View?>(R.id.relative_monthly_donate)
                    as RelativeLayout
            relative_donate_for_project = mainLayout.findViewById<View?>(R.id.relative_donate_for_project)
                    as RelativeLayout
            relative_donate_for_cause = mainLayout.findViewById<View?>(R.id.relative_donate_for_cause)
                    as RelativeLayout
            relative_request_help = mainLayout.findViewById<View?>(R.id.relative_request_help)
                    as RelativeLayout
            relative_quick_donate = mainLayout.findViewById<View?>(R.id.relative_quick_donate)
                    as RelativeLayout
            relative_zakaat.setOnClickListener(this)
            relative_monthly_donate.setOnClickListener(this)
            relative_donate_for_project.setOnClickListener(this)
            relative_donate_for_cause.setOnClickListener(this)
            relative_request_help.setOnClickListener(this)
            relative_quick_donate.setOnClickListener(this)
            img_quick_donate = mainLayout.findViewById<View?>(R.id.img_quick_donate) as ImageView
            img_bg2 = mainLayout.findViewById<View?>(R.id.img_bg2) as ImageView
            img_zakaat = mainLayout.findViewById<View?>(R.id.img_zakaat) as ImageView
            img_bg3 = mainLayout.findViewById<View?>(R.id.img_bg3) as ImageView
            img_monthly_donate = mainLayout.findViewById<View?>(R.id.img_monthly_donate) as ImageView
            img_bg4 = mainLayout.findViewById<View?>(R.id.img_bg4) as ImageView
            img_donate_for_project = mainLayout.findViewById<View?>(R.id.img_donate_for_project) as ImageView
            img_donate_for_cause = mainLayout.findViewById<View?>(R.id.img_donate_for_cause) as ImageView
            img_bg5 = mainLayout.findViewById<View?>(R.id.img_bg5) as ImageView
            img_bg6 = mainLayout.findViewById<View?>(R.id.img_bg6) as ImageView
            img_request_help = mainLayout.findViewById<View?>(R.id.img_request_help) as ImageView
            img_bg1 = mainLayout.findViewById<View?>(R.id.img_bg1) as ImageView
            ContentActivity.Companion.setTextFonts(mainLayout)

//            GetDonnationText()

            ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)
        }
    }

    override fun onStart() {
        super.onStart()
//        ContentActivity.Companion.mtv_topTitle.setText(R.string.DonateLabel)
        ContentActivity.Companion.mtv_topTitle.setText(R.string.DonationeLabel)
        ContentActivity.Companion.tabType = 1
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSessionManager)
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.relative_monthly_donate -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                val bundle = Bundle()
                bundle.putBoolean("IsNew", false)
                bundle.putString("type", "MonthlyDonate")
                ContentActivity.Companion.openQuickDonateFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
            R.id.relative_donate_for_project -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                ContentActivity.Companion.openProjectForDonationFragment()
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
            R.id.relative_request_help -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                ContentActivity.Companion.openMyRequestHelpFragment()
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
            R.id.relative_quick_donate -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                val bundle = Bundle()
                bundle.putString("type", "QuickDonate")
                ContentActivity.Companion.openQuickDonateFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
            R.id.relative_zakaat -> if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                val bundle = Bundle()
                bundle.putString("type", "ZakaatDonate")
                ContentActivity.Companion.openQuickDonateFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
            R.id.relative_donate_for_cause -> if (mSessionManager.isLoggedin()
                && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                val bundle = Bundle()
                bundle.putString("type", "CauseDonate")
                ContentActivity.Companion.openQuickDonateFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
        }
    }

    fun GetDonnationText() {
        QenaatAPICall.getCallingAPIInterface()?.GetDonnationText()?.enqueue(object : Callback<GetAboutUs?> {
            override fun onFailure(call: Call<GetAboutUs?>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<GetAboutUs?>, response: Response<GetAboutUs?>) {
                if (response.body() != null) {
                    val bottomRandomAds = response.body()
                    if (bottomRandomAds != null) {
                        if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                            ContentActivity.Companion.setupMoreIfoDialog("", bottomRandomAds.ContentsEn)
                        } else {
                            ContentActivity.Companion.setupMoreIfoDialog("", bottomRandomAds.ContentsAr)
                        }
                    }
                }
            }
        })
    }

    companion object {
        protected val TAG = DonateFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: DonateFragment
        fun newInstance(act: FragmentActivity): DonateFragment {
            fragment = DonateFragment()
            Companion.act = act
            return fragment
        }
    }
}