package com.qenaat.app.model

import java.util.*

/**
 * Created by DELL on 13-Feb-18.
 */
class AppendPersonsToInvitationList {
    private var InvitationListId: String? = null
    fun getInvitationListId(): String? {
        return InvitationListId
    }

    fun setInvitationListId(invitationListId: String?) {
        InvitationListId = invitationListId
    }

    fun getPersonIds(): ArrayList<Int?>? {
        return PersonIds
    }

    fun setPersonIds(personIds: ArrayList<Int?>?) {
        PersonIds = personIds
    }

    private var PersonIds: ArrayList<Int?>? = null

    fun getUserIds(): ArrayList<Int?>? {
        return UserIds
    }

    fun setUserIds(userIds: ArrayList<Int?>?) {
        UserIds = userIds
    }

    private var UserIds: ArrayList<Int?>? = null
}