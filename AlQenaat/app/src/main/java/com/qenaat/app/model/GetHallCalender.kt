package com.qenaat.app.model

/**
 * Created by DELL on 08-Nov-17.
 */
class GetHallCalender {
    var BookingNo: String? = null
    var PersonId: String? = null
    var BookingStateEN: String? = null
    var BookingStateAR: String? = null
    var BookingStatus: String? = null
    var BookingColor: String? = null
    var DayNumber: String? = null
    var BookingDate: String? = null
    var BookCode: Int? = null
}