package com.qenaat.app.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetAttachmentTypes
import com.qenaat.app.model.RequestForHelp
import java.util.*

/**
 * Created by DELL on 17-Jan-18.
 */
class RequestFromDocumentAdapter(var act: FragmentActivity,
                                 private val itemsData: ArrayList<GetAttachmentTypes>,
                                 requestHelpForm: RequestForHelp) : RecyclerView.Adapter<RequestFromDocumentAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager
    var sessionManager: SessionManager
    var requestHelpForm: RequestForHelp? = null

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.tv_price.setVisibility(View.GONE)
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
//                viewHolder.tv_title.setText(itemsData.get(position).NameEN + " (" +
//                        itemsData.get(position).count + ")")

                viewHolder.tv_title.setText(itemsData.get(position).NameAR + " (" +
                        itemsData.get(position).count + ")")
                viewHolder.tv_title.textDirection = View.TEXT_DIRECTION_LTR
//                viewHolder.tv_title.setText("(" + itemsData.get(position).count + ")")
            } else {
                viewHolder.tv_title.setText(itemsData.get(position).NameAR + " (" +
                        itemsData.get(position).count + ")")
//                viewHolder.tv_title.setText("(" + itemsData.get(position).count + ")")
            }
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                sessionManager.setImagePath("")
                val gson = Gson()
                val b = Bundle()
                b.putString("GetAttachmentTypes", gson.toJson(itemsData.get(position)))
                b.putString("RequestForHelp", gson.toJson(requestHelpForm))
                ContentActivity.Companion.openUploadRequestFormDocumentFragment(b)
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var tv_title: TextView
        var tv_price: TextView

        init {
            tv_price = itemLayoutView.findViewById<View?>(R.id.tv_price) as TextView
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_parent)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        this.requestHelpForm = requestHelpForm
        languageSeassionManager = LanguageSessionManager(act)
        sessionManager = SessionManager(act)
    }
}