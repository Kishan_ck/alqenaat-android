package com.qenaat.app.model

/**
 * Created by DELL on 08-Nov-17.
 */
class GetHalls {
    var Id: String? = null
    var BookingDate: String? = null
    var Phone: String? = null
    var HallNameEN: String? = null
    var HallNameAR: String? = null
    var HallDescriptionEN: String? = null
    var HallDescriptionAR: String? = null
    var HallAddressEN: String? = null
    var HallAddressAR: String? = null
    var Longitude: String? = null
    var Latitude: String? = null
    var Photo: String? = null
    var IsActive: String? = null
    var contacts: ArrayList<Contacts?>? = null

    inner class Contacts {
        var ContactItemNameAr: String? = null
        var ContactValueAr: String? = null
        var ContactItemNameEn: String? = null
        var ContactValueEn: String? = null
        var IsActive: String? = null
    }
}