package com.qenaat.app.fragments

import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.fragments.AddPersonToInvitationListFragment.PersonListAdapter.ViewHolder
import com.qenaat.app.model.AppendPersonsToInvitationList
import com.qenaat.app.model.GetFamilyTree
import com.qenaat.app.model.GetServiceTypes
import com.qenaat.app.model.SearchForPersonToAddToList
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.person_list.*
import okhttp3.ResponseBody


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by DELL on 12-Feb-18.
 */
class AddPersonToInvitationListFragment : Fragment(), View.OnClickListener {
    var XY: IntArray? = null
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    lateinit var mainLayout: RelativeLayout
    var getServiceTypes: GetServiceTypes? = null
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var cityIdArray: Array<String?>? = null
    var areaIdArray: Array<String?>? = null
    var branchIdArray: Array<String?>? = null
    var familyNameIdArray: Array<String?>? = null
    var gender: String = "0"
    var personType: String = "0"
    var ageFrom: String = "0"
    var ageTo: String = "0"
    var keyword: String = ""
    var invitationId: String = "0"
    var comingFrom: String = ""
    var getFamilyTreeArrayList: ArrayList<GetFamilyTree?> = ArrayList<GetFamilyTree?>()
    lateinit var tv_add_person: TextView
    var tabNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.person_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        tv_add_person = mainLayout.findViewById(R.id.tv_add_person) as TextView
        tv_add_person.setOnClickListener(this)
        //my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
        ContentActivity.Companion.setTextFonts(mainLayout)
    }

    override fun onStart() {
        super.onStart()

        val arg = arguments
        if (arg != null) {
            if (arg.containsKey("tabNumber")) {
                tabNumber = arg.getInt("tabNumber", 0)
            }
            if (arg.containsKey("cityIdArray")) {
                cityIdArray = arg.getStringArray("cityIdArray")
            }
            if (arg.containsKey("areaIdArray")) {
                areaIdArray = arg.getStringArray("areaIdArray")
            }
            if (arg.containsKey("familyNameIdArray")) {
                familyNameIdArray = arg.getStringArray("familyNameIdArray")
            }
            if (arg.containsKey("branchIdArray")) {
                branchIdArray = arg.getStringArray("branchIdArray")
            }
            if (arg.containsKey("gender")) {
                gender = arg.getString("gender")!!
            }
            if (arg.containsKey("personType")) {
                personType = arg.getString("personType")!!
            }
            if (arg.containsKey("ageFrom")) {
                ageFrom = arg.getString("ageFrom")!!
            }
            if (arg.containsKey("ageTo")) {
                ageTo = arg.getString("ageTo")!!
            }
            if (arg.containsKey("keyword")) {
                keyword = arg.getString("keyword")!!
            }
            if (arg.containsKey("invitationId")) {
                invitationId = arg.getString("invitationId")!!
            }
            if (arg.containsKey("comingFrom")) {
                comingFrom = arg.getString("comingFrom")!!
            }
        }
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.SearchLabel))

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion. img_topAccount.setVisibility(View.GONE)
        ContentActivity.Companion.mtv_topTitle.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putStringArray("cityIdArray", cityIdArray)
            bundle.putStringArray("areaIdArray", areaIdArray)
            bundle.putStringArray("familyNameIdArray", familyNameIdArray)
            bundle.putStringArray("branchIdArray", branchIdArray)
            bundle.putString("gender", gender)
            bundle.putString("personType", personType)
            bundle.putString("ageFrom", ageFrom)
            bundle.putString("ageTo", ageTo)
            bundle.putString("keyword", keyword)
            bundle.putString("invitationId", invitationId)
            ContentActivity.Companion.openPersonFilterFragment(bundle)
        })
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topFilter.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topFilter.setOnClickListener(View.OnClickListener { //  getFragmentManager().popBackStack();
            val bundle = Bundle()
            bundle.putStringArray("cityIdArray", cityIdArray)
            bundle.putStringArray("areaIdArray", areaIdArray)
            bundle.putStringArray("familyNameIdArray", familyNameIdArray)
            bundle.putStringArray("branchIdArray", branchIdArray)
            bundle.putString("gender", gender)
            bundle.putString("personType", personType)
            bundle.putString("ageFrom", ageFrom)
            bundle.putString("ageTo", ageTo)
            bundle.putString("keyword", keyword)
            bundle.putString("invitationId", invitationId)
            ContentActivity.Companion.openPersonFilterFragment(bundle)
        })
        if (comingFrom.equals("home", ignoreCase = true)) {
            tv_add_person.setVisibility(View.GONE)
            tv_add_person.setText(act.getString(R.string.RemovePersonLabel))
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else {

            tv_add_person.setVisibility(View.VISIBLE)
            tv_add_person.setText(act.getString(R.string.AddPersonLabel))
        }
        if (getFamilyTreeArrayList.size > 0) {
            mAdapter = PersonListAdapter(act, getFamilyTreeArrayList, comingFrom)
            my_recycler_view.setAdapter(mAdapter)
        } else {
            if (comingFrom.equals("home", ignoreCase = true)) {
                GetInvitationListPersons()
//                ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            } else {

                searchForPersonToAddToList()

            }
        }
        if (tabNumber == 2) {
            tv_add_person.setVisibility(View.GONE)
            ContentActivity.Companion.img_topFilter?.setVisibility(View.GONE)
        }
    }

    fun searchForPersonToAddToList() {
        ContentActivity.Companion.img_topFilter?.setVisibility(View.GONE)

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()
                ?.SearchForPersonToAddToList("application/json", getFilterObject(),
                        "" + page_index)?.enqueue(
                        object : Callback<ArrayList<GetFamilyTree?>?> {

                            override fun onFailure(call: Call<ArrayList<GetFamilyTree?>?>, t: Throwable) {
                                t.printStackTrace()
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ArrayList<GetFamilyTree?>?>, response: Response<ArrayList<GetFamilyTree?>?>) {
                                if (response.body() != null) {
                                    val getFamilyTrees = response.body()
                                    if (getFamilyTrees!!.size == 0)
                                        tv_noDataFound?.visibility = View.VISIBLE
                                    else
                                        tv_noDataFound?.visibility = View.GONE
                                    if (mloading != null && getFamilyTrees != null) {
                                        Log.d("getContentses size", "" + getFamilyTrees.size)
                                        getFamilyTreeArrayList.clear()
                                        getFamilyTreeArrayList.addAll(getFamilyTrees)
                                        makeDefault()
                                        mAdapter = PersonListAdapter(act, getFamilyTreeArrayList, comingFrom)
                                        my_recycler_view.setAdapter(mAdapter)
                                        my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                                if (!endOfREsults) {
                                                    visibleItemCount = mLayoutManager.getChildCount()
                                                    totalItemCount = mLayoutManager.getItemCount()
                                                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                                                    if (loading_flag) {
                                                        if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                            if (getFamilyTreeArrayList.size != 0) {
                                                                progress_loading_more.setVisibility(View.VISIBLE)
                                                                //GlobalFunctions.DisableLayout(mainLayout);
                                                            }
                                                            loading_flag = false
                                                            page_index = page_index + 1
                                                            SearchForMorePersonToAddToList()
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                    }
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            }
                        })
    }

    fun GetInvitationListPersons() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetInvitationListPersons(
                invitationId, "" + page_index)?.enqueue(
                object : Callback<ArrayList<GetFamilyTree?>?> {

                    override fun onFailure(call: Call<ArrayList<GetFamilyTree?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetFamilyTree?>?>, response: Response<ArrayList<GetFamilyTree?>?>) {
                        if (response.body() != null) {
                            val getFamilyTrees = response.body()
                            if (mloading != null && getFamilyTrees != null) {
                                Log.d("getContentses size", "" + getFamilyTrees.size)
                                getFamilyTreeArrayList.clear()
                                getFamilyTreeArrayList.addAll(getFamilyTrees)
                                if (getFamilyTrees.size == 0)
                                    tv_noDataFound?.visibility = View.VISIBLE
                                else
                                    tv_noDataFound?.visibility = View.GONE
                                makeDefault()
                                mAdapter = PersonListAdapter(act, getFamilyTreeArrayList, comingFrom)
                                my_recycler_view.setAdapter(mAdapter)

//                    my_recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//
//
//                            if (!endOfREsults) {
//
//                                visibleItemCount = mLayoutManager.getChildCount();
//
//                                totalItemCount = mLayoutManager.getItemCount();
//
//                                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                                if (loading_flag) {
//
//
//                                    if ((visibleItemCount + pastVisiblesItems) + 2 >= totalItemCount) {
//
//
//                                        if (!(getFamilyTreeArrayList.size() == 0)) {
//
//                                            progress_loading_more.setVisibility(View.VISIBLE);
//                                            //GlobalFunctions.DisableLayout(mainLayout);
//
//                                        }
//
//                                        loading_flag = false;
//
//                                        page_index = page_index + 1;
//
//                                        SearchForMorePersonToAddToList();
//
//
//                                    }
//                                }
//
//                            }
//                        }
//                    });
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    fun SearchForMorePersonToAddToList() {
        QenaatAPICall.getCallingAPIInterface()?.SearchForPersonToAddToList(
                "application/json", getFilterObject(),
                "" + page_index)?.enqueue(
                object : Callback<ArrayList<GetFamilyTree?>?> {

                    override fun onFailure(call: Call<ArrayList<GetFamilyTree?>?>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetFamilyTree?>?>, response: Response<ArrayList<GetFamilyTree?>?>) {
                        if (response.body() != null) {
                            val getFamilyTrees = response.body()
                            if (progress_loading_more != null) {
                                progress_loading_more.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                if (getFamilyTrees != null) {
                                    if (getFamilyTrees.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    for (i in getFamilyTrees.indices) {
                                        val familyTree: GetFamilyTree? = getFamilyTrees[i]
                                        familyTree?.IsSelected = "1"
                                        getFamilyTrees[i] = familyTree
                                    }
                                    getFamilyTreeArrayList.addAll(getFamilyTrees)
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                })
    }

    private fun getFilterObject(): SearchForPersonToAddToList? {
        val addToList: SearchForPersonToAddToList = SearchForPersonToAddToList()
        addToList.AgeFrom = ageFrom
        addToList.AgeTo = ageTo
        addToList.AreaIds = areaIdArray
        addToList.InvitationListId = invitationId
        addToList.FamilyNameIds = familyNameIdArray
        addToList.BranchIds = branchIdArray
        addToList.CityIds = cityIdArray
        addToList.Gender = gender
        addToList.PersonType = personType
        addToList.SearchKeyword = keyword
        addToList.pageIndex = page_index.toString() + ""
        return addToList
    }

    private fun makeDefault() {
        for (i in getFamilyTreeArrayList.indices) {
            val familyTree: GetFamilyTree? = getFamilyTreeArrayList.get(i)
            familyTree?.IsSelected = "1"
            getFamilyTreeArrayList.set(i, familyTree)
        }
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.tv_add_person ->
                if (getPersonIds()?.size!! > 0 && getUserIds()?.size!! > 0) {
                    val appendPersonsToInvitationList = AppendPersonsToInvitationList()
                    appendPersonsToInvitationList.setInvitationListId(invitationId)
                    appendPersonsToInvitationList.setPersonIds(getPersonIds())
                    appendPersonsToInvitationList.setUserIds(getUserIds())
                    AppendPersonsToInvitationList(appendPersonsToInvitationList)
                } else {
                }
        }
    }

    private fun getPersonIds(): ArrayList<Int?>? {
        val list = ArrayList<Int?>()
        for (getFamilyTree in getFamilyTreeArrayList) {
            if (getFamilyTree?.IsSelected.equals("1", ignoreCase = true)) {
                list.add(getFamilyTree?.Id?.toInt())
            }
        }
        return list
    }

    private fun getUserIds(): ArrayList<Int?>? {
        val list = ArrayList<Int?>()
        for (getFamilyTree in getFamilyTreeArrayList) {
            if (getFamilyTree?.IsSelected.equals("1", ignoreCase = true)) {
                list.add(getFamilyTree?.UserId?.toInt())
            }
        }
        return list
    }

    fun AppendPersonsToInvitationList(appendPersonsToInvitationList: AppendPersonsToInvitationList?) {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.AppendPersonsToInvitationList(
                "application/json", appendPersonsToInvitationList)?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (outResponse.toInt() > 0) {
                            } else if (outResponse == "-1") {
                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                            } else if (outResponse == "-2") {
                                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
//Change by viral (24-04-2021) add person after referesh screen

                        val bundle = Bundle()
                        bundle.putStringArray("cityIdArray", cityIdArray)
                        bundle.putStringArray("areaIdArray", areaIdArray)
                        bundle.putStringArray("branchIdArray", branchIdArray)
                        bundle.putString("gender", "0")
                        bundle.putString("personType", "0")
                        bundle.putString("ageFrom", "0")
                        bundle.putString("ageTo", "0")
                        bundle.putString("keyword", "")
                        bundle.putString("comingFrom", "home")
                        bundle.putInt("tabNumber", tabNumber)
                        bundle.putString("invitationId", invitationId)
                        ContentActivity.Companion.openAddPersonToInvitationListFragment(bundle)


                    }
                })
    }

    fun RemovePersonsToInvitationList(appendPersonsToInvitationList: AppendPersonsToInvitationList?) {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.RemovePersonsToInvitationList(
                appendPersonsToInvitationList?.getInvitationListId(),
                if (appendPersonsToInvitationList?.getPersonIds()?.size!! > 0)
                    appendPersonsToInvitationList.getPersonIds()!!.get(0).toString() + "" else "0")?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (outResponse.toInt() > 0) {
                                if (comingFrom.equals("home", ignoreCase = true)) {
                                    GetInvitationListPersons()
                                }
                            } else if (outResponse == "-1") {
                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                            } else if (outResponse == "-2") {
                                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    inner class PersonListAdapter(a: FragmentActivity, itemsData: ArrayList<GetFamilyTree?>,
                                  comingFrom: String) : RecyclerView.Adapter<ViewHolder?>() {
        private val itemsData: ArrayList<GetFamilyTree?>
        var act: FragmentActivity
        var languageSeassionManager: LanguageSessionManager
        var comingFrom: String = ""

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            // create a new view
            val itemLayoutView: View = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.person_list_row, null)

            // create ViewHolder
            return ViewHolder(itemLayoutView)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

            // - get data from your itemsData at this position
            // - replace the contents of the view with that itemsData
            if (itemsData.get(position) != null) {
                if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                    viewHolder.tv_name.setText(itemsData.get(position)?.NameEn + " " + itemsData.get(position)?.SecondName
                            + " " + itemsData.get(position)?.ThirdName + " " + itemsData.get(position)?.FourthName
                            + " " + itemsData.get(position)?.FifthName)
                } else {
                    viewHolder.tv_name.setText(itemsData.get(position)?.NameAr + " " + itemsData.get(position)?.SecondName
                            + " " + itemsData.get(position)?.ThirdName + " " + itemsData.get(position)?.FourthName
                            + " " + itemsData.get(position)?.FifthName)
                }
                viewHolder.img_add.visibility = View.VISIBLE
                Log.d("home", "1")
                if (comingFrom.equals("home", ignoreCase = true)) {
                    Log.d("home", "2")
                    viewHolder.img_add.visibility = View.VISIBLE
                    viewHolder.img_add.setImageResource(R.drawable.remove_to_list)
                    viewHolder.img_add.setOnClickListener {
                        AlertDialog.Builder(act)
                                .setTitle(act.getString(R.string.Confirm))
                                .setMessage(act.getString(R.string.AreYouSureYouWantToDeletePersonLabel))
                                .setPositiveButton(android.R.string.yes, object : DialogInterface.OnClickListener {
                                    override fun onClick(dialog: DialogInterface, which: Int) {
                                        Log.e("afterTextChanged", "update")
                                        dialog.dismiss()
                                        val appendPersonsToInvitationList = AppendPersonsToInvitationList()
                                        appendPersonsToInvitationList.setInvitationListId(invitationId)
                                        val list = ArrayList<Int?>()
                                        list.add(itemsData.get(position)?.Id?.toInt())
                                        appendPersonsToInvitationList.setPersonIds(list)
                                        RemovePersonsToInvitationList(appendPersonsToInvitationList)
                                    }
                                })
                                .setNegativeButton(android.R.string.no, object : DialogInterface.OnClickListener {
                                    override fun onClick(dialog: DialogInterface, which: Int) {
                                        dialog.dismiss()
                                    }
                                })
                                .setIcon(R.drawable.icon_512)
                                .show()
                    }
                } else {
                    viewHolder.img_add.setImageResource(R.drawable.add_to_list)
                    if (itemsData.get(position)?.IsSelected.equals("1", ignoreCase = true)) {
                        viewHolder.img_add.setImageResource(R.drawable.added_to_list)
                    }
                }
                if (tabNumber == 2) {
                    viewHolder.img_add.visibility = View.GONE
                }
                viewHolder.relative_parent.setOnClickListener {
                    if (tabNumber == 1) {
                        if (comingFrom.equals("home", ignoreCase = true)) {
                        } else {

//                        AddPersonToInvitationListFragment.selectPerson(position);
                            val familyTree: GetFamilyTree? = getFamilyTreeArrayList.get(position)
                            if (familyTree?.IsSelected.equals("1", ignoreCase = true)) {
                                familyTree?.IsSelected = "0"
                            } else {
                                familyTree?.IsSelected = "1"
                            }
                            getFamilyTreeArrayList.set(position, familyTree)
                            var pos = 0
                            pos = mLayoutManager.findFirstVisibleItemPosition()
                            mAdapter = PersonListAdapter(act, getFamilyTreeArrayList, comingFrom)
                            my_recycler_view.setAdapter(mAdapter)
                            mAdapter.notifyDataSetChanged()
                            my_recycler_view.scrollToPosition(pos)
                        }
                    }
                }
                viewHolder.img_user_profile.layoutParams.width = (act.getResources().getDrawable(
                        R.drawable.tree_node) as BitmapDrawable).getBitmap().getWidth()
                viewHolder.img_user_profile.layoutParams.height = (act.getResources().getDrawable(
                        R.drawable.tree_node) as BitmapDrawable).getBitmap().getHeight()
                if (itemsData.get(position)?.Photo?.length!! > 0) Picasso.with(act)
                        .load(itemsData.get(position)?.Photo)
                        .error(R.drawable.tree_node)
                        .placeholder(R.drawable.tree_node)
                        .config(Bitmap.Config.RGB_565).fit()
                        .into(viewHolder.img_user_profile)

//            viewHolder.relative_parent.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Gson gson = new Gson();
//                    Bundle b = new Bundle();
//                    b.putString("GetCompanies", gson.toJson(itemsData.get(position)));
//                    ContentActivity.openCompanyDetailsFragment(b);
//
//                }
//            });
            }
        }

        // inner class to hold a reference to each item of RecyclerView
        inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
            var img_bg: ImageView
            var img_add: ImageView
            var img_user_profile: CircleImageView
            var relative_parent: RelativeLayout
            var tv_name: TextView

            init {
                tv_name = itemLayoutView.findViewById<View?>(R.id.tv_name) as TextView
                img_user_profile = itemLayoutView.findViewById<View?>(R.id.img_user_profile)
                        as CircleImageView
                img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
                img_add = itemLayoutView.findViewById<View?>(R.id.img_add) as ImageView
                relative_parent = itemLayoutView
                        .findViewById<View?>(R.id.relative_parent) as RelativeLayout
                ContentActivity.Companion.setTextFonts(relative_parent)
            }
        }

        // Return the size of your itemsData (invoked by the layout manager)
        override fun getItemCount(): Int {
            return itemsData.size
        }

        init {
            this.act = a
            this.itemsData = itemsData
            this.comingFrom = comingFrom
            languageSeassionManager = LanguageSessionManager(act)
        }
    }

    companion object {
        protected val TAG = AddPersonToInvitationListFragment::class.java.simpleName

        //lateinit var my_recycler_view_: RecyclerView
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        private lateinit var mLayoutManager: LinearLayoutManager
        lateinit var act: FragmentActivity
        lateinit var fragment: AddPersonToInvitationListFragment

        fun newInstance(act: FragmentActivity): AddPersonToInvitationListFragment {
            fragment = AddPersonToInvitationListFragment()
            Companion.act = act
            return fragment
        }
    }
}