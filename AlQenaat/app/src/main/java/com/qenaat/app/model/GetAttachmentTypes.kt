package com.qenaat.app.model


/**
 * Created by DELL on 17-Jan-18.
 */
class GetAttachmentTypes {
    var Id: String? = null
    var NameEN: String? = null
    var NameAR: String? = null
    var count: String? = null
    var photos: ArrayList<Photos?>? = null

    class Photos {
        var Id: String? = null
        var Photo: String? = null
    }
}