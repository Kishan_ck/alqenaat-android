package com.qenaat.app.adapters

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetProjects
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by DELL on 14-Nov-17.
 */
class ProjectDonationAdapter(var act: FragmentActivity, private val itemsData: ArrayList<GetProjects>) : RecyclerView.Adapter<ProjectDonationAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            viewHolder.img_category_color.setVisibility(View.GONE)
            viewHolder.tv_category_name.setTextColor(Color.parseColor("#87776f"))
            if (itemsData.get(position).HaveOpenBudget.equals("false", ignoreCase = true)) {
                viewHolder.tv_category_name.setText(act.getString(R.string.CostRemaining) + " " + itemsData.get(position).Amount + " " + act.getString(R.string.KD))
            } else {
                viewHolder.tv_category_name.setText(act.getString(R.string.OpenDonation))
            }
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                viewHolder.tv_category_name.setText(itemsData.get(position).ProjectNameEN)
            } else {
                viewHolder.tv_category_name.setText(itemsData.get(position).ProjectNameAR)
            }

            //viewHolder.tv_date.setText(itemsData.get(position).);
            viewHolder.tv_date.setVisibility(View.GONE)
            viewHolder.img_news.setImageResource(R.drawable.no_ing_list)
            if (itemsData.get(position).Photo?.length!! > 0) Picasso.with(act)
                    .load(itemsData.get(position).Photo)
                    .error(R.drawable.no_ing_list)
                    .placeholder(R.drawable.no_ing_list)
                    .config(Bitmap.Config.RGB_565).fit()
                    .into(viewHolder.img_news)
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetProjects", gson.toJson(itemsData.get(position)))
                ContentActivity.Companion.openProjectDonationDetailsFragment(b)
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_news: ImageView
        var img_bg: ImageView
        var img_category_color: CircleImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var relative_category: RelativeLayout
        var tv_category_name: TextView
        var tv_title: TextView
        var tv_date: TextView

        init {
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            tv_category_name = itemLayoutView.findViewById<View?>(R.id.tv_category_name) as TextView
            tv_date = itemLayoutView.findViewById<View?>(R.id.tv_date) as TextView
            img_news = itemLayoutView.findViewById<View?>(R.id.img_news) as ImageView
            img_category_color = itemLayoutView.findViewById<View?>(R.id.img_category_color)
                    as CircleImageView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_category = itemLayoutView
                    .findViewById<View?>(R.id.relative_category) as RelativeLayout
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_content)
            tv_category_name.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}