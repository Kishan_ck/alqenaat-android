package com.qenaat.app.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.DrawerItems
import kotlinx.android.synthetic.main.item_drawer_layout.view.*
import java.util.*

class DrawerAdapter : RecyclerView.Adapter<DrawerAdapter.ItemViewHolder> {
    private var onDrawerItemClickListener: OnDrawerItemClickListener?
    private var itemList: ArrayList<DrawerItems>? = null
    private var context: Context? = null
    lateinit var mLangSessionManager: LanguageSessionManager

    constructor(
            context: Context?,
            onDrawerItemClickListener: OnDrawerItemClickListener?
    ) {
        this.context = context
        this.onDrawerItemClickListener = onDrawerItemClickListener
        mLangSessionManager = LanguageSessionManager(this.context)
    }

    constructor(
            list: ArrayList<DrawerItems>?,
            onDrawerItemClickListener: OnDrawerItemClickListener?
    ) {
        itemList = list
        this.onDrawerItemClickListener = onDrawerItemClickListener
    }

    constructor(
            context: Context?,
            list: ArrayList<DrawerItems>?,
            onDrawerItemClickListener: OnDrawerItemClickListener?
    ) {
        this.context = context
        itemList = list
        this.onDrawerItemClickListener = onDrawerItemClickListener
    }

    fun doRefresh(dataSet: ArrayList<DrawerItems>?) {
        itemList = dataSet
        notifyDataSetChanged()
    }

    interface OnDrawerItemClickListener {
        fun onDrawerItemClick(position: Int, drawerItems: DrawerItems?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val v: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_drawer_layout, parent, false)
        return ItemViewHolder(v, this)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val dataSet = itemList!![position]
        try {
            holder.setDataToView(dataSet)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return itemList!!.size
    }

    inner class ItemViewHolder(
            view: View?,
            private val mAdapter: DrawerAdapter?
    ) : RecyclerView.ViewHolder(view!!), View.OnClickListener {
        override fun onClick(view: View) {
            mAdapter?.onItemHolderClick(this@ItemViewHolder)
        }

        private val iv_item = itemView.tv_home


        fun setDataToView(dataSet: DrawerItems) {
            iv_item.text = dataSet.displayText
            if (mLangSessionManager.getLang().equals("en", ignoreCase = true))
                iv_item.textDirection = View.TEXT_DIRECTION_LTR
            else
                iv_item.textDirection = View.TEXT_DIRECTION_RTL

        }

        init {
            itemView.setOnClickListener(this)
        }
    }

    private fun onItemHolderClick(holder: ItemViewHolder) {
        if (onDrawerItemClickListener != null) {
            val position = holder.adapterPosition
            onDrawerItemClickListener!!.onDrawerItemClick(position, itemList!![position])
        }
    }
}