package com.qenaat.app.model

/**
 * Created by admin on 6/21/2017.
 */
class GetTermsandconditions {
    var ContentsEn: String? = null
    var ContentsAr: String? = null
    var TitleAR: String? = null
    var TitleEN: String? = null
    var Id: String? = null

    override fun toString(): String {
        return "ClassPojo [ContentsEn = $ContentsEn, ContentsAr = $ContentsAr, NameAR = $TitleAR, NameEN = $TitleEN, requestForHelpId = $Id]"
    }
}