package com.qenaat.app.fragments

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.jdev.countryutil.Constants
import com.jdev.countryutil.CountryUtil
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkFixLength
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetHalls
import com.qenaat.app.model.GetTopBookingUser
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.add_news.LL_country_code
import kotlinx.android.synthetic.main.add_news.iv_country_flag
import kotlinx.android.synthetic.main.add_news.tv_country_code
import kotlinx.android.synthetic.main.book_hall.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 13-Nov-17.
 */
class BookHallFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var tv_hall_details: TextView
    lateinit var tv_book_hall: TextView
    lateinit var tv_last_booking_user: TextView
    lateinit var et_mobile: EditText
    lateinit var et_details: EditText
    lateinit var getHalls: GetHalls
    var date: String = ""
    var df1: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    private var countryCode: String = ""
    private var countryFlag: Int = 0
    private var countryName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        countryCode = Util.getUserCountryInfo(act, ConstanstParameters.COUNTRY_CODE)
        countryName = Util.getUserCountryInfo(act, ConstanstParameters.COUNTRY_NAME)
        countryFlag = Util.getUserCountryInfo(act, ConstanstParameters.COUNTRY_FLAG).toInt()

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments?.containsKey("GetHalls")!!) {
                    val gson = Gson()
                    getHalls = gson.fromJson(arguments?.getString("GetHalls"),
                            GetHalls::class.java)
                }
                date = arguments?.getString("date")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.book_hall, null) as RelativeLayout
            act.softWindow()
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            et_mobile = mainLayout.findViewById<View?>(R.id.et_mobile) as EditText
            et_details = mainLayout.findViewById<View?>(R.id.et_details) as EditText
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            tv_hall_details = mainLayout.findViewById<View?>(R.id.tv_hall_details) as TextView
            tv_hall_details.setTypeface(ContentActivity.Companion.tf)
            tv_last_booking_user = mainLayout.findViewById<View?>(R.id.tv_last_booking_user) as TextView
            tv_last_booking_user.setTypeface(ContentActivity.Companion.tf)
            tv_book_hall = mainLayout.findViewById<View?>(R.id.tv_book_hall) as TextView
            tv_book_hall.setTypeface(ContentActivity.Companion.tf)
            tv_book_hall.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)

            LL_country_code.setOnClickListener(this)

            tv_country_code.isSelected = true
            tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
            iv_country_flag.setImageResource(countryFlag)

        }
    }

    override fun onStart() {
        super.onStart()

        //getTopBookingUser();
        ContentActivity.Companion.bottomBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)


        ContentActivity.Companion.enableLogin(languageSeassionManager)
        if (getHalls != null) {
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                tv_hall_details.setText(Html.fromHtml("<b>" + getHalls.HallNameEN + "</b><br><br>" + act.getString(R.string.Date) + "<br>" + date))
            } else {
                tv_hall_details.setText(Html.fromHtml("<b>" + getHalls.HallNameAR + "</b><br><br>" + act.getString(R.string.Date) + "<br>" + date))
            }
        }
    }

    private fun isValid(): Boolean {
        when {
            et_name.checkEmpty() -> {
                et_name.requestFocus()
                mainLayout.showSnakeBar(getString(R.string.enter_name))
                return false
            }
            et_mobile.checkEmpty() -> {
                et_mobile.requestFocus()
                mainLayout.showSnakeBar(getString(R.string.enter_mobile_number))
                return false
            }
            !et_mobile.checkFixLength(8) -> {
                mainLayout.showSnakeBar(getString(R.string.mobile_character))
                et_mobile.requestFocus()
                return false
            }
            et_details.checkEmpty() -> {
                et_details.requestFocus()
                mainLayout.showSnakeBar(getString(R.string.enter_details))
                return false
            }
        }
        return true
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.LL_country_code -> {
                CountryUtil(act).setTitle("").build()
            }
            R.id.tv_book_hall -> if (isValid()) {
                mloading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
                QenaatAPICall.getCallingAPIInterface()?.MakeBooking(
                        "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                        getHalls.Id.toString(),
                        if (mSessionManager.getUserCode().equals("", ignoreCase = true)) "0" else mSessionManager.getUserCode(),
                        et_name.getText().toString(),
                        et_mobile.getText().toString(),
                        countryCode,
                        date,
                        et_details.getText().toString())?.enqueue(
                        object : Callback<ResponseBody?> {

                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                t.printStackTrace()
                                mloading.setVisibility(View.INVISIBLE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }

                            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                val body = response?.body()
                                var outResponse = ""
                                try {
                                    val reader = BufferedReader(InputStreamReader(
                                            ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }
                                if (outResponse != null) {
                                    outResponse = outResponse.replace("\"", "")
                                    outResponse = outResponse.replace("\n", "")
                                    Log.e("outResponse not null ", outResponse)
                                    when (outResponse) {
                                        "-1" -> {
                                            mainLayout.showSnakeBar(act.getString(R.string.OperationFailed))
                                        }
                                        "-2" -> {
                                            mainLayout.showSnakeBar(act.getString(R.string.DataMissing))
                                        }
                                        "2" -> {
                                            mainLayout.showSnakeBar(act.getString(R.string.already_booked))
                                        }
                                        else -> {
                                            mainLayout.showSnakeBar(act.getString(R.string.RequestSent))
                                            fragmentManager?.popBackStack()
                                        }
                                    }

                                    /*if (outResponse.contains("https")) {

                                            Bundle b = new Bundle();
                                            b.putString("id", outResponse.toString());

                                            Fragment frag = PayPaymentFragment.newInstance(act);
                                            frag.setArguments(b);
                                            act.getSupportFragmentManager()
                                                    .beginTransaction()
                                                    .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                                    .addToBackStack(frag.getClass().getName())
                                                    .replace(R.id.content_frame, frag).commit();

                                        } else {

                                            if (outResponse.equals("-1")) {
                                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show();
                                            }else if (outResponse.equals("-2")) {
                                                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show();
                                            }

                                        }*/
                                }
                                mloading.setVisibility(View.INVISIBLE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }
                        })
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ContentActivity.Companion.bottomBarView.setVisibility(View.GONE)
    }

    fun getTopBookingUser() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetTopBookingUser(
                getHalls.Id, date)?.enqueue(
                object : Callback<MutableList<GetTopBookingUser?>?> {

                    override fun onFailure(call: Call<MutableList<GetTopBookingUser?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.visibility = View.INVISIBLE
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<MutableList<GetTopBookingUser?>?>, response: Response<MutableList<GetTopBookingUser?>?>) {
                        if (response.body() != null) {
                            val bookingUsers = response.body()
                            if (bookingUsers != null) {
                                if (bookingUsers.size > 0) {
                                    var bookingConfirm = ""
                                    for (i in bookingUsers.indices) {
                                        val bookingUser = bookingUsers[i]
                                        if (bookingUser?.BookingStatus.equals("2", ignoreCase = true)) {
                                            bookingConfirm = act.getString(R.string.ConfirmUser) + ": " +
                                                    bookingUser?.RequestFullName
                                            bookingUsers.removeAt(i)
                                            break
                                        }
                                    }
                                    var max = 0
                                    if (bookingUsers.size == 1) {
                                        max = 1
                                    } else if (bookingUsers.size == 2) {
                                        max = 2
                                    } else if (bookingUsers.size >= 3) {
                                        max = 3
                                    }
                                    var topUsers = """
                            $bookingConfirm

                            ${act.getString(R.string.BookingUserMessage)}
                            """.trimIndent()
                                    for (i in 0 until max) {
                                        topUsers = """
                                $topUsers

                                ${i + 1}. ${bookingUsers[i]?.RequestFullName}
                                """.trimIndent()
                                    }
                                    tv_last_booking_user.setText(topUsers)
                                }
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.KEY_RESULT_CODE) {
            try {
                countryName = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                countryCode = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
                countryFlag = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                iv_country_flag.setImageResource(countryFlag)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        protected val TAG = BookHallFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: BookHallFragment
        lateinit var mloading: ProgressBar

        //SimpleDateFormat df1 = new SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.ENGLISH);
        fun newInstance(act: FragmentActivity): BookHallFragment {
            fragment = BookHallFragment()
            Companion.act = act
            return fragment
        }
    }
}