package com.qenaat.app.fragments

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.MonasbatAdapter
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetEvents
import com.qenaat.app.networking.QenaatAPICall
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 07-Nov-17.
 */
class MonasbatFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var my_recycler_view: RecyclerView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    var imgW = 0
    var imgH = 0
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var progress_loading_more: ProgressBar
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var getContentsArrayList: ArrayList<GetEvents?> = ArrayList()
    var month_number: ArrayList<String?> = ArrayList()
    var header_position: ArrayList<Long?> = ArrayList()
    var current_month: String = ""
    var type: String = "2"
    var date: String = ""
    var specificDate: String = "0"
    var special: String = "0"
    var catId: String = "0"
    var treeId: String = "0"
    var isActivity: String = "0"
    private var headersDecor: StickyRecyclerHeadersDecoration? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                catId = arguments!!.getString("catId")!!
                type = arguments!!.getString("type")!!
                date = arguments!!.getString("date")!!
                specificDate = arguments!!.getString("specificDate")!!
                special = arguments!!.getString("special")!!
                treeId = arguments!!.getString("treeId")!!
                if (arguments!!.containsKey("isActivity")) {
                    isActivity = arguments!!.getString("isActivity")!!
                }
            }
        } catch (e: Exception) {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.monasbat_fragment, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById<View?>(R.id.loading_progress) as ProgressBar
            my_recycler_view = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
            mLayoutManager = LinearLayoutManager(activity)
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
            my_recycler_view.setLayoutManager(mLayoutManager)
            progress_loading_more = mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
            imgW = (act?.getResources()?.getDrawable(
                    R.drawable.no_ing_list) as BitmapDrawable).bitmap.width
            imgH = (act?.getResources()?.getDrawable(
                    R.drawable.no_ing_list) as BitmapDrawable).bitmap.height
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.img_topHall.putVisibility(View.GONE)
        //if (ContentActivity.mSessionManager.isLoggedin())
        ContentActivity.Companion.img_topAddAd.putVisibility(View.VISIBLE)
        /* else
             ContentActivity.Companion.img_topAddAd.putVisibility(View.GONE)*/
//        ContentActivity.Companion.mtv_topTitle.setText(R.string.EventsLabel)
        ContentActivity.Companion.mtv_topTitle.setText(R.string.OccasionLabel)
        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)

        if (catId.equals("7", ignoreCase = true)) {
//            ContentActivity.Companion.mtv_topTitle.setText(R.string.OccasionDeadLabel)
            ContentActivity.Companion.mtv_topTitle.setText(R.string.CondolencesLabel)
        }
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                bundle.putString("isEdit", "0")
                bundle.putString("contentType", "2")
                ContentActivity.Companion.openAddNewsFragment(bundle)
            } else {
                mainLayout.showSnakeBar(act.getString(R.string.LoggedIn))
                ContentActivity.Companion.openLoginFragment()
            }
        })

//        if(getContentsArrayList.size()==0){
        GetEvents()

//        }
//        else{
//
//            if(headersDecor != null){
//
//                my_recycler_view.removeItemDecoration(headersDecor);
//
//            }
//
//            setupStickyHeaderData();
//
//            headersDecor = new StickyRecyclerHeadersDecoration((StickyRecyclerHeadersAdapter) mAdapter);
//
//            my_recycler_view.addItemDecoration(headersDecor);
//
//            mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
//                @Override
//                public void onChanged() {
//                    if(headersDecor != null){
//
//                        headersDecor.invalidateHeaders();
//
//                    }
//                }
//            });
//
////            mAdapter = new GetContentsAdapter(act, getMyRequestedHelps,  imgH, imgW,
////                    type, date, specificDate, special, treeId);
////
////            my_recycler_view.setAdapter(mAdapter);
//
//        }
        my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!endOfREsults) {
                    visibleItemCount = mLayoutManager.getChildCount()
                    totalItemCount = mLayoutManager.getItemCount()
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                    if (loading_flag) {
                        if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                            if (getContentsArrayList.size != 0) {
                                progress_loading_more.setVisibility(View.VISIBLE)
                                //GlobalFunctions.DisableLayout(mainLayout);
                            }
                            loading_flag = false
                            page_index = page_index + 1
                            GetMoreEvents()
                        }
                    }
                }
            }
        })
    }

    override fun onClick(v: View?) {}
    fun GetEvents() {
        page_index = 0
        endOfREsults = false
        loading_flag = true
        pastVisiblesItems = 0
        visibleItemCount = 0
        totalItemCount = 0
        Log.d("GetEvents", "GetEvents-MonasbatFragment")
        mloading?.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetEvents(
                "0",
                "false",
                "false",
                "" + page_index,
                "0",
                mSessionManager.getUserCode(),
                catId)?.enqueue(
                object : Callback<ArrayList<GetEvents>?> {
                    override fun onFailure(call: Call<ArrayList<GetEvents>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetEvents>?>, response: Response<ArrayList<GetEvents>?>) {
                        if (response.body() != null) {
                            val getContentses = response.body()
                            if (mloading != null) {
                                if (getContentses != null) {
                                    Log.d("GetContents activity", "" + getContentses.size)
                                    getContentsArrayList.clear()
                                    if (headersDecor != null) {
                                        my_recycler_view.removeItemDecoration(headersDecor!!)
                                    }
                                    getContentsArrayList.addAll(makeEventList(getContentses)!!)
                                    Log.d("getContentsArray size", "" + getContentsArrayList.size)
                                    mloading.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                    setupStickyHeaderData()

                                    // Add the sticky headers decoration
                                    headersDecor = StickyRecyclerHeadersDecoration(mAdapter as StickyRecyclerHeadersAdapter<*>?)
                                    my_recycler_view.addItemDecoration(headersDecor!!)
                                    mAdapter.registerAdapterDataObserver(object : AdapterDataObserver() {
                                        override fun onChanged() {
                                            headersDecor?.invalidateHeaders()
                                        }
                                    })
                                }
                            }
                        }
                    }
                })
    }

    fun GetMoreEvents() {
        Log.d("GetEvents", "GetEvents-MonasbatFragment-More")
        QenaatAPICall.getCallingAPIInterface()?.GetEvents(
                "0", "false", "false",
                "" + page_index, "0", mSessionManager.getUserCode(), catId)?.enqueue(
                object : Callback<ArrayList<GetEvents>?> {

                    override fun onFailure(call: Call<ArrayList<GetEvents>?>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetEvents>?>, response: Response<ArrayList<GetEvents>?>) {
                        if (response.body() != null) {
                            val getContentses = response.body()
                            if (progress_loading_more != null) {
                                if (getContentses != null) {
                                    if (getContentses.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    getContentsArrayList.addAll(makeEventList(getContentses)!!)
                                    loadMoreStickyHeaderData(getContentsArrayList)
                                    mAdapter.notifyDataSetChanged()
                                }
                                progress_loading_more.setVisibility(View.GONE)
                                //GlobalFunctions.EnableLayout(mainLayout);
                            }
                        }
                    }
                })
    }

    private fun setupStickyHeaderData() {
        header_position.clear()
        current_month = ""
        var `val`: Long = 1
        for (i in getContentsArrayList.indices) {
            if (getContentsArrayList.get(i)?.MonthTitle == null ||
                    getContentsArrayList.get(i)?.MonthTitle.equals("", ignoreCase = true)) {
                if (i == 0) {
                    `val` = 1
                    header_position.add(`val`)
                    //header_position.add("1");
                } else {
                    header_position.add(header_position.get(i - 1))
                }
            } else {
                month_number.add(getContentsArrayList.get(i)?.MonthTitle)
                if (i == 0) {
                    `val` = 1
                    header_position.add(`val`)
                    //header_position.add("1");
                    current_month = getContentsArrayList.get(i)?.MonthTitle!!
                } else {
                    if (getContentsArrayList.get(i)!!.MonthTitle.equals(current_month, ignoreCase = true)) {
                        //header_position.add("0");
                        header_position.add(`val`)
                        current_month = getContentsArrayList.get(i)?.MonthTitle!!
                    } else {
                        //header_position.add("1");
                        `val` = `val` + 1
                        header_position.add(`val`)
                        current_month = getContentsArrayList.get(i)?.MonthTitle!!
                    }
                }
            }
        }
        if (getContentsArrayList.size == 0)
            tv_noDataFound?.putVisibility(View.VISIBLE)
        mAdapter = MonasbatAdapter(act, getContentsArrayList, header_position, catId)
        my_recycler_view.setAdapter(mAdapter)
        for (i in header_position.indices) {
            Log.d("header_position", header_position.get(i).toString() + " --")
        }
    }

    private fun loadMoreStickyHeaderData(getContentses: MutableList<GetEvents?>?) {
        header_position.clear()
        current_month = ""
        var `val`: Long = 1
        for (i in getContentses?.indices!!) {
            if (getContentsArrayList.get(i)!!.MonthTitle == null ||
                    getContentsArrayList.get(i)!!.MonthTitle.equals("", ignoreCase = true)) {
                if (i == 0) {
                    `val` = 1
                    header_position.add(`val`)
                    //header_position.add("1");
                } else {
                    header_position.add(header_position.get(i - 1))
                }
            } else {
                month_number.add(getContentsArrayList.get(i)!!.MonthTitle)
                if (i == 0) {
                    `val` = 1
                    header_position.add(`val`)
                    //header_position.add("1");
                    current_month = getContentsArrayList.get(i)!!.MonthTitle!!
                } else {
                    if (getContentsArrayList.get(i)!!.MonthTitle.equals(current_month, ignoreCase = true)) {
                        //header_position.add("0");
                        header_position.add(`val`)
                        current_month = getContentsArrayList.get(i)!!.MonthTitle!!
                    } else {
                        //header_position.add("1");
                        `val` = `val` + 1
                        header_position.add(`val`)
                        current_month = getContentsArrayList.get(i)!!.MonthTitle!!
                    }
                }
            }
        }
        for (i in header_position.indices) {
            Log.d("header_position", header_position.get(i).toString() + " ==")
        }

        //my_recycler_view.setAdapter(mAdapter);
    }

    private fun makeEventList(getEventsArrayList: ArrayList<GetEvents>?): ArrayList<GetEvents?>? {
        val list = ArrayList<GetEvents?>()
        for (events in getEventsArrayList!!) {
            if (events!!.Id.equals("-1", ignoreCase = true)) {
                events.MonthTitle = ""
            } else {
                if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                    events.MonthTitle = events.MonthNameEn + " " + events.EventDate?.split("/".toRegex())?.toTypedArray()!![2]
                } else {
                    events.MonthTitle = events.MonthNameAr + " " + events.EventDate?.split("/".toRegex())?.toTypedArray()!![2]
                }
            }
            list.add(events)
        }
        return list
    }

    companion object {
        protected val TAG = MonasbatFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: MonasbatFragment
        lateinit var mloading: ProgressBar
        fun newInstance(act: FragmentActivity): MonasbatFragment {
            fragment = MonasbatFragment()
            Companion.act = act
            return fragment
        }
    }
}