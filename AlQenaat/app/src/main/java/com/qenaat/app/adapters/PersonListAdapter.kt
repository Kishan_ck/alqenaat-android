package com.qenaat.app.adapters

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetFamilyTree
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*

/**
 * Created by DELL on 13-Feb-18.
 */
class PersonListAdapter(var act: FragmentActivity, private val itemsData: ArrayList<GetFamilyTree>) : RecyclerView.Adapter<PersonListAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.person_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                viewHolder.tv_name.setText(itemsData.get(position).NameEn)
            } else {
                viewHolder.tv_name.setText(itemsData.get(position).NameAr)
            }
            viewHolder.img_add.setImageResource(R.drawable.add_to_list)
            if (itemsData.get(position).IsSelected.equals("1", ignoreCase = true)) {
                viewHolder.img_add.setImageResource(R.drawable.added_to_list)
            }
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                //  AddPersonToInvitationListFragment.selectPerson(position);
            })
            viewHolder.img_user_profile.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.tree_node) as BitmapDrawable).bitmap.width
            viewHolder.img_user_profile.getLayoutParams().height = (act.getResources().getDrawable(
                    R.drawable.tree_node) as BitmapDrawable).bitmap.height
            if (itemsData.get(position).Photo?.length!! > 0) Picasso.with(act)
                    .load(itemsData.get(position).Photo)
                    .error(R.drawable.tree_node)
                    .placeholder(R.drawable.tree_node)
                    .config(Bitmap.Config.RGB_565).fit()
                    .into(viewHolder.img_user_profile)

//            viewHolder.relative_parent.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    Gson gson = new Gson();
//                    Bundle b = new Bundle();
//                    b.putString("GetCompanies", gson.toJson(itemsData.get(position)));
//                    ContentActivity.openCompanyDetailsFragment(b);
//
//                }
//            });
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var img_add: ImageView
        var img_user_profile: CircleImageView
        var relative_parent: RelativeLayout
        var tv_name: TextView

        init {
            tv_name = itemLayoutView.findViewById<View?>(R.id.tv_name) as TextView
            img_user_profile = itemLayoutView.findViewById<View?>(R.id.img_user_profile) as CircleImageView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            img_add = itemLayoutView.findViewById<View?>(R.id.img_add) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_parent)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
    }
}