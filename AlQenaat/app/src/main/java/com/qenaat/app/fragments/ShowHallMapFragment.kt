package com.qenaat.app.fragments

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetHalls
import java.util.*

/**
 * Created by DELL on 11-Nov-17.
 */
class ShowHallMapFragment : Fragment(), View.OnClickListener {
    lateinit var act: FragmentActivity
    lateinit var mLangSessionManager: LanguageSessionManager
    lateinit var mainLayout: RelativeLayout
    lateinit private var map: GoogleMap
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var getHallsArrayList: ArrayList<GetHalls> = ArrayList()
    var markerArrayList: ArrayList<Marker?> = ArrayList()
    lateinit var tv_done: TextView
    lateinit var tv_hybrid_map: TextView
    lateinit var tv_normal_map: TextView
    lateinit var relative_map: RelativeLayout
    lateinit var relative_normal_map: RelativeLayout
    lateinit var relative_hybrid_map: RelativeLayout
    var isNormalMap = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            mLangSessionManager = LanguageSessionManager(act)
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(HallFragment.act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetHalls")) {
                    val gson = Gson()
                    getHallsArrayList = gson.fromJson(arguments!!.getString("GetHalls"),
                            object : TypeToken<ArrayList<GetHalls?>?>() {}.type)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.current_location, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        tv_done = mainLayout.findViewById<View?>(R.id.tv_done) as TextView
        tv_hybrid_map = mainLayout.findViewById<View?>(R.id.tv_hybrid_map) as TextView
        tv_normal_map = mainLayout.findViewById<View?>(R.id.tv_normal_map) as TextView
        relative_normal_map = mainLayout.findViewById<View?>(R.id.relative_normal_map) as RelativeLayout
        relative_hybrid_map = mainLayout.findViewById<View?>(R.id.relative_hybrid_map) as RelativeLayout
        relative_map = mainLayout.findViewById<View?>(R.id.relative_map) as RelativeLayout
        ContentActivity.Companion.setTextFonts(relative_map)
        relative_hybrid_map.setOnClickListener(this)
        relative_normal_map.setOnClickListener(this)
        Log.e("initViews", "initViews")
        try {
            if (map == null) {
                val supportMapFragment = fragment.getChildFragmentManager().findFragmentById(R.id.map) as SupportMapFragment?
                supportMapFragment?.getMapAsync(OnMapReadyCallback { googleMap -> map = googleMap })
            } else {
                Log.e("1 map not null", "map not null")
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " initViews: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(mLangSessionManager)
        Log.e("initViews", "initViews")
        try {
            val supportMapFragment = fragment.getChildFragmentManager().findFragmentById(R.id.map) as SupportMapFragment?
            supportMapFragment?.getMapAsync(OnMapReadyCallback { googleMap ->
                map = googleMap
                setupMap()
            })
        } catch (e: Exception) {
            Log.e(TAG + " " + " initViews: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        if (getHallsArrayList.size == 1) {
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
            tv_done.setText(act.getString(R.string.GoogleMapLabel))
        } else {
            tv_done.setVisibility(View.GONE)
        }
        tv_done.setOnClickListener(this)
     //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)


        /*map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Creating a marker
                MarkerOptions markerOptions = new MarkerOptions();

                // GetAppSettings the position for the marker
                markerOptions.position(latLng);

                Log.d("title clicked",markerOptions.getTitle());

                // GetAppSettings the title for the marker.
                // This will be displayed on taping the marker
                markerOptions.title(latLng.latitude + " : " + latLng.longitude);
                currentLatitude = latLng.latitude;
                currentLongitude = latLng.longitude;

                // Clears the previously touched position
                map.clear();

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position
                map.addMarker(markerOptions);
            }
        });*/
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.relative_normal_map -> {
                relative_hybrid_map.setBackgroundColor(Color.parseColor("#f8e8c7"))
                tv_hybrid_map.setTextColor(Color.LTGRAY)
                relative_normal_map.setBackgroundColor(Color.parseColor("#263eaa"))
                tv_normal_map.setTextColor(Color.parseColor("#f8e8c7"))
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL)
            }
            R.id.relative_hybrid_map -> {
                relative_normal_map.setBackgroundColor(Color.parseColor("#f8e8c7"))
                tv_normal_map.setTextColor(Color.LTGRAY)
                relative_hybrid_map.setBackgroundColor(Color.parseColor("#263eaa"))
                tv_hybrid_map.setTextColor(Color.parseColor("#f8e8c7"))
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID)
            }
            R.id.tv_done -> if (getHallsArrayList.size == 1) {

                /*String uri = String.format(Locale.ENGLISH, "geo:%f,%f", Double.parseDouble(getContentsArrayList.get(0).getLatitude()),
                            Double.parseDouble(getContentsArrayList.get(0).getLatitude()));*/
                val uriBegin = "geo:" + getHallsArrayList.get(0).Latitude!!.toDouble() + "," + getHallsArrayList.get(0).Longitude!!.toDouble()
                val query = getHallsArrayList.get(0).Latitude!!.toDouble().toString() + "," + getHallsArrayList.get(0).Longitude!!.toDouble() + "(" + getHallsArrayList.get(0).HallNameEN + ")"
                val encodedQuery = Uri.encode(query)
                val uriString = "$uriBegin?q=$encodedQuery&z=16"
                val url = ("http://maps.google.com/maps?daddr=" + getHallsArrayList.get(0).Latitude
                        + "," + getHallsArrayList.get(0).Longitude + "&mode=driving")
                val uri = Uri.parse(url)
                val intent = Intent(Intent.ACTION_VIEW, uri)
                intent.setPackage("com.google.android.apps.maps")
                act.startActivity(intent)
            }
        }
    }

    private fun moveMap() {
        val cameraPosition = CameraPosition.Builder().target(LatLng(currentLatitude,
                currentLongitude)).zoom(15f).build()
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun addMarker() {

        // Clears the previously touched position
        map.clear()
        markerArrayList.clear()
        for (halls in getHallsArrayList) {
            if (halls.Latitude!!.length > 0 && halls.Longitude!!.length > 0) {
                Log.d("getLatitude", halls.Latitude)
                currentLatitude = halls.Latitude!!.toDouble()
                currentLongitude = halls.Longitude!!.toDouble()

                // Creating a marker
                //MarkerOptions markerOptions = new MarkerOptions();

                // GetAppSettings the position for the marker
                //markerOptions.position(new LatLng(Double.parseDouble(contents.getLatitude()),
                //Double.parseDouble(contents.getLongitude())));

                //markerOptions.title(contents.getRequestForHelpId());
                val marker = map.addMarker(MarkerOptions().position(LatLng(halls.Latitude!!.toDouble(), halls.Longitude!!.toDouble())))
                if (languageSeassionManager?.getLang().equals("en", ignoreCase = true))
                    marker.setTitle(halls.HallNameEN)
                else
                    marker.setTitle(halls.HallNameAR)
                markerArrayList.add(marker)

                // Animating to the touched position
                map.animateCamera(CameraUpdateFactory.newLatLng(LatLng(halls.Latitude!!.toDouble(), halls.Longitude!!.toDouble())))
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID)

                // Placing a marker on the touched position
                //map.addMarker(markerOptions);
            }
        }

        // \n is for new line
        //Toast.makeText(act, "Your Location is - \nLat: " + currentLatitude + "\nLong: " +
        //currentLongitude, Toast.LENGTH_LONG).show();
        moveMap()
    }

    override fun onDestroy() {
        super.onDestroy()

        /*try{
            SupportMapFragment fragment1 = ((SupportMapFragment) fragment.getChildFragmentManager().
                    findFragmentById(R.id.map));
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.remove(fragment1);
            ft.commit();
        }catch(Exception e){
        }*/
    }

    private fun setupMap() {
        addMarker()
        /* map.setOnMarkerClickListener(OnMarkerClickListener { marker ->
             for (i in markerArrayList.indices) {
                 if (marker == markerArrayList.get(i)) {
                     Log.d("marker", "equals")
                     Log.d("marker latitude", marker.position.latitude.toString() + "")
                     Log.d("marker longitude", marker.position.longitude.toString() + "")
                     Log.d("marker", "equals")
                     Log.d("marker-", marker.title)
                     Log.d("marker--", getHallsArrayList.get(i).Id)
                     val gson = Gson()
                     val b = Bundle()
                     b.putString("GetHalls", gson.toJson(getHallsArrayList.get(i)))
                     ContentActivity.Companion.openHallDetailsFragment(b)
                 } else {
                     Log.d("marker", "not equals")
                 }
             }
             true
         })*/
    }

    companion object {
        protected val TAG = ShowHallMapFragment::class.java.simpleName
        lateinit var fragment: ShowHallMapFragment
        lateinit var mSessionManager: SessionManager
        var languageSeassionManager: LanguageSessionManager? = null
        fun newInstance(act: FragmentActivity): ShowHallMapFragment? {
            fragment = ShowHallMapFragment()
            fragment.act = act
            return fragment
        }
    }
}