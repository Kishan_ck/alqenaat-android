package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager.widget.ViewPager
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.ItemGalleryPagerAdapter
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager

/**
 * Created by DELL on 11/08/2016.
 */
class GalleryFragment : Fragment() {
    lateinit var XY: IntArray
    lateinit var mainLayout: LinearLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager

    //CarouselView carouselView;
    lateinit var productImages: Array<String>
    lateinit var options: DisplayImageOptions
    var position = 0
    var isLoadingFirstTime = false
    lateinit var mpager: ViewPager
    lateinit var mloading: ProgressBar
    lateinit var mProductGalleryPagerAdapter: ItemGalleryPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("position")) {
                    position = arguments!!.getInt("position")
                    productImages = arguments!!.getStringArray("productImages")!!
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.gallrery_fragment, null) as LinearLayout
            initViews(mainLayout)

        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: LinearLayout) {
        if (mainLayout != null) {
            mpager = mainLayout.findViewById<View?>(R.id.pager) as ViewPager
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
        }

    }

    override fun onStart() {
        super.onStart()
     //   ContentActivity.Companion.topBarView.setVisibility(View.GONE)
        ContentActivity.Companion.bottomBarView.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        mProductGalleryPagerAdapter = ItemGalleryPagerAdapter(
                act!!, productImages)
        mpager.setAdapter(mProductGalleryPagerAdapter)
        mpager.setCurrentItem(position)
        Log.e("position--", "" + position)
    }

    companion object {
        protected val TAG = GalleryFragment::class.java.simpleName
        var act: FragmentActivity? = null
        var fragment: GalleryFragment? = null
        fun newInstance(act: FragmentActivity?): GalleryFragment {
            fragment = GalleryFragment()
            Companion.act = act
            return fragment!!
        }
    }
}