package com.qenaat.app.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.ChatListAdapter
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.QenaatConstant
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetRequestHelpMessages
import com.qenaat.app.model.GetRequests
import com.qenaat.app.networking.QenaatAPICall
import me.drakeet.materialdialog.MaterialDialog
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

/**
 * Created by DELL on 13-May-17.
 */
class ChatListFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var linear_top: LinearLayout
    lateinit var my_recycler_view: RecyclerView
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mloading: ProgressBar
    lateinit var tv_pickup_no: TextView
    lateinit var relative_bottom: RelativeLayout
    lateinit var img_bg: ImageView
    lateinit var img_chat_img: ImageView
    lateinit var et_message: EditText
    lateinit var tv_send: TextView
    private val arrayListPhotoEdit: ArrayList<String?> = ArrayList()
    private val arrayListPhoto: ArrayList<String?> = ArrayList()
    var getRequestHelpMessagesArrayList: ArrayList<GetRequestHelpMessages?> = ArrayList()
    var isEdit = true
    var imageCount = 0
    private var outputFileUri: Uri? = null
    private var index = 0
    private var current_path: String? = ""
    var orderId: String? = "0"
    var pickupId: String? = "0"
    var type: String? = ""
    var isLoadedBefore = false
    var iAmHere = true
    var handler: Handler? = Handler()
    var runnable: Runnable? = null
    var img_label: ImageView? = null
    var myRequestedHelps: GetRequests? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments?.containsKey("GetRequests")!!) {
                    val gson = Gson()
                    myRequestedHelps = gson.fromJson(arguments?.getString("GetRequests"),
                            GetRequests::class.java)
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message + "" + "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.chat_list, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message + "" + "")
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            tv_send = mainLayout.findViewById<View?>(R.id.tv_send) as TextView
            et_message = mainLayout.findViewById<View?>(R.id.et_message) as EditText
            et_message.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.chat_input) as BitmapDrawable).bitmap.width
            img_chat_img = mainLayout.findViewById<View?>(R.id.img_chat_img) as ImageView
            img_bg = mainLayout.findViewById<View?>(R.id.img_bg) as ImageView
            relative_bottom = mainLayout.findViewById<View?>(R.id.relative_bottom) as RelativeLayout
            tv_pickup_no = mainLayout.findViewById<View?>(R.id.tv_pickup_no) as TextView
            tv_pickup_no.setVisibility(View.VISIBLE)
            img_label = mainLayout.findViewById<View?>(R.id.img_label) as ImageView
            linear_top = mainLayout.findViewById<View?>(R.id.linear_top) as LinearLayout
            my_recycler_view = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            mLayoutManager = LinearLayoutManager(activity)
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
            my_recycler_view.setLayoutManager(mLayoutManager)
            my_recycler_view.setItemAnimator(DefaultItemAnimator())
            ContentActivity.Companion.setTextFonts(mainLayout)

            //tv_pickups_label.setTypeface(ContentActivity.tf, Typeface.BOLD);
            tv_send.setOnClickListener(this)
            img_chat_img.setOnClickListener(this)
        }
    }

    override fun onStart() {
        super.onStart()
        iAmHere = true
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        if (getRequestHelpMessagesArrayList.size > 0) {
            mAdapter = ChatListAdapter(act, getRequestHelpMessagesArrayList)
            my_recycler_view.setAdapter(mAdapter)
            mAdapter.notifyDataSetChanged()
            my_recycler_view.scrollToPosition(getRequestHelpMessagesArrayList.size - 1)
        } else {
            GetPickupOrderMessagesByOrderId()
            tv_pickup_no.setText(act.getString(R.string.Order).toUpperCase() + " " +
                    orderId)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        iAmHere = false
        if (handler != null) {
            handler?.removeCallbacks(runnable)
        }
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.img_chat_img -> {

//                et_message.setText("");

                //First checking if the app is already having the permission
                if (GlobalFunctions.isReadStorageAllowed(act)) {
                    if (GlobalFunctions.isCameraAllowed(act)) {
                        openImageIntent()
                        return
                    }
                }

                //If the app has not the permission then asking for the permission
                if (GlobalFunctions.isCameraAllowed(act)) {
                    GlobalFunctions.requestStoragePermission(act)
                } else {
                    GlobalFunctions.requestCameraePermission(act)
                }
            }
            R.id.tv_send -> {
                val isError = false
                images = StringBuilder()
                if (et_message.getText().toString().length > 0) {

                    //isError=false;
                    sendMessage()
                }
                if (arrayListPhotoEdit.size > 0) {

                    /*index = 0;

                    current_path = arrayListPhotoEdit.get(index);

                    Log.d("arrayListPhotoEdit", ""+arrayListPhotoEdit.size());

                    UploadImagesAysn uploadImagesAysn = new UploadImagesAysn();

                    uploadImagesAysn.execute();*/
                }
                if (isError) {
                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    fun GetPickupOrderMessagesByOrderId() {

        //mloading.setVisibility(View.VISIBLE);

        //GlobalFunctions.DisableLayout(mainLayout);
        QenaatAPICall.getCallingAPIInterface()?.GetRequestHelpMessages(mSessionManager.getUserCode(),
                myRequestedHelps?.Id)?.enqueue(
                object : Callback<ArrayList<GetRequestHelpMessages?>?> {

                    override fun onFailure(call: Call<ArrayList<GetRequestHelpMessages?>?>, t: Throwable) {
                        t.printStackTrace()
                        //mloading.setVisibility(View.INVISIBLE);
                        //GlobalFunctions.EnableLayout(mainLayout);
                    }

                    override fun onResponse(call: Call<ArrayList<GetRequestHelpMessages?>?>, response: Response<ArrayList<GetRequestHelpMessages?>?>) {

                        //mloading.setVisibility(View.INVISIBLE);

                        //GlobalFunctions.EnableLayout(mainLayout);
                        if (response.body() != null) {
                            val getRequestHelpMessages = response.body()
                            if (getRequestHelpMessages != null) {
                                if (getRequestHelpMessages.size > 0) {
                                    getRequestHelpMessagesArrayList.clear()
                                    getRequestHelpMessagesArrayList.addAll(getRequestHelpMessages)

                                    //Collections.reverse(orderArrayList);
                                    mAdapter = ChatListAdapter(act, getRequestHelpMessagesArrayList)
                                    my_recycler_view.setAdapter(mAdapter)
                                    mAdapter.notifyDataSetChanged()
                                    my_recycler_view.scrollToPosition(getRequestHelpMessagesArrayList.size - 1)
                                }
                            } else {
                            }
                            GetMorePickupOrderMessagesByOrderId()
                        }
                    }
                })
    }

    fun GetMorePickupOrderMessagesByOrderId() {
        QenaatAPICall.getCallingAPIInterface()?.GetRequestHelpMessages(mSessionManager.getUserCode(),
                myRequestedHelps?.Id)?.enqueue(
                object : Callback<ArrayList<GetRequestHelpMessages?>?> {

                    override fun onFailure(call: Call<ArrayList<GetRequestHelpMessages?>?>, t: Throwable) {
                        t.printStackTrace()
                        //mloading.setVisibility(View.INVISIBLE);
                        //GlobalFunctions.EnableLayout(mainLayout);
                    }

                    override fun onResponse(call: Call<ArrayList<GetRequestHelpMessages?>?>, response: Response<ArrayList<GetRequestHelpMessages?>?>) {
                        if (response.body() != null) {
                            val getRequestHelpMessages = response.body()
                            if (getRequestHelpMessages != null) {
                                if (getRequestHelpMessages.size > 0) {
                                    var chatCount1 = 0
                                    var chatCount2 = 0
                                    chatCount1 = getRequestHelpMessagesArrayList.size
                                    chatCount2 = getRequestHelpMessages.size
                                    getRequestHelpMessagesArrayList.clear()
                                    getRequestHelpMessagesArrayList.addAll(getRequestHelpMessages)
                                    if (mAdapter == null) {
                                        mAdapter = ChatListAdapter(act, getRequestHelpMessagesArrayList)
                                        my_recycler_view.setAdapter(mAdapter)
                                        mAdapter.notifyDataSetChanged()
                                    } else {
                                        mAdapter.notifyDataSetChanged()
                                        if (chatCount2 != chatCount1 && getRequestHelpMessagesArrayList.size >= 5) {
                                            if (getRequestHelpMessagesArrayList.size - mLayoutManager.findLastVisibleItemPosition() < 5) {
                                                my_recycler_view.scrollToPosition(getRequestHelpMessagesArrayList.size - 1)
                                            }
                                        }
                                    }
                                }
                            } else {
                            }
                            runnable = Runnable { if (iAmHere) GetMorePickupOrderMessagesByOrderId() }
                            handler?.postDelayed(runnable, 3000)
                        }
                    }
                })
    }

    private fun sendMessage() {
        var msg = ""
        if (et_message.getText().toString().length > 0) {
            msg = et_message.getText().toString()
        }
        et_message.setText("")
        Log.d("sendMessage", "sendMessage")
        val finalMsg = msg
        QenaatAPICall.getCallingAPIInterface()?.SendRequestHelpMessages(
                myRequestedHelps?.Id,
                if (et_message.getText().toString().length > 0) et_message.getText().toString() else "",
                if (images.toString().length > 0) images.toString() else "",
                mSessionManager.getUserCode())?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        //mloading.setVisibility(View.INVISIBLE);
                        //GlobalFunctions.EnableLayout(mainLayout);
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {

                            //outResponse = outResponse.replace("\"", "");

                            //outResponse = outResponse.replace("\n", "");
                            Log.e("outResponse not null ", outResponse)
                            var type = ""
                            var message = ""
                            try {
                                val jsonObject = JSONObject(outResponse)
                                type = "" + jsonObject.getInt("MessageType")
                                message = jsonObject.getString("Message")
                                if (type.equals("1", ignoreCase = true)) {
                                    addMessageObject(finalMsg)

                                    //et_message.setText("");
                                } else {
                                    Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG).show()
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }

                        //mloading.setVisibility(View.INVISIBLE);
                        //GlobalFunctions.EnableLayout(mainLayout);
                    }
                })
    }

    // method to get the name of the image from the path
    fun getNameOfImage(path: String?): String? {
        val index = path?.lastIndexOf('/')
        return path?.substring(index!! + 1)
    }

    private fun getNamesOfImages() {
        images = StringBuilder()
        if (isEdit) {
            for (i in arrayListPhotoEdit.indices) {
                val file = File(arrayListPhotoEdit.get(i))
                images.append(getNameOfImage(arrayListPhotoEdit.get(i)))
                if (i != arrayListPhotoEdit.size - 1) {
                    images.append(",")
                }
                Log.e("images", images.toString())
            }
        } else {
            for (i in arrayListPhoto.indices) {
                val file = File(arrayListPhoto.get(i))
                images.append(getNameOfImage(arrayListPhoto.get(i)))
                if (i != arrayListPhoto.size - 1) {
                    if (i == 0) {
                    } else {
                        images.append(",")
                    }
                }
                Log.e("images", images.toString())
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("resultCode", "" + resultCode)
        Log.d("requestCode", "" + requestCode)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 121) {
                val isCamera: Boolean
                isCamera = if (data == null) {
                    true
                } else {
                    val action = data.action
                    if (action == null) {
                        false
                    } else {
                        action == MediaStore.ACTION_IMAGE_CAPTURE
                    }
                }
                val selectedImageUri: Uri?
                arrayListPhotoEdit.clear()
                arrayListPhoto.clear()
                selectedImageUri = if (isCamera) {
                    outputFileUri
                } else {
                    data?.data
                }
                if (isEdit) {
                    arrayListPhotoEdit.add(if (isCamera) selectedImageUri?.getPath() else getPathFromURI(selectedImageUri))

                    //getFamilyTree.setImagePath(isCamera ? selectedImageUri.getPath() : getPathFromURI(selectedImageUri));
                } else {
                    arrayListPhoto.add(if (isCamera) selectedImageUri?.getPath() else getPathFromURI(selectedImageUri))

                    //getFamilyTree.setImagePath(isCamera ? selectedImageUri.getPath() : getPathFromURI(selectedImageUri));
                }
                Log.d("aaa", "2")
                if (isEdit) {
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))

                    //setEditImages(arrayListPhotoEdit.size());
                    imageCount = arrayListPhotoEdit.size
                    index = 0
                    current_path = arrayListPhotoEdit.get(index)
                    Log.d("arrayListPhotoEdit", "" + arrayListPhotoEdit.size)
                    images = StringBuilder()
                    images.append(getNameOfImage(arrayListPhotoEdit.get(index)))
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                    //setImages(arrayListPhoto.size());
                    imageCount = arrayListPhoto.size
                }
                Log.d("aaa", "3")
            }
        }
    }

    internal inner class UploadImagesAysn : AsyncTask<String?, String?, String?>() {
        override fun onPreExecute() {
            super.onPreExecute()

            //mloading.setVisibility(View.VISIBLE);

            //GlobalFunctions.DisableLayout(mainLayout);
        }

        override fun onPostExecute(paramString: String?) {
            Log.d("paramString", "" + paramString)
            Log.d("image updates", "" + index + " image uploaded -> " + index + 1 + " image started")
            index = index + 1
            if (isEdit) {
                if (index < arrayListPhotoEdit.size) {
                    current_path = arrayListPhotoEdit.get(index)
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {

                    //mloading.setVisibility(View.INVISIBLE);
                    //GlobalFunctions.EnableLayout(mainLayout);
                    index = 0
                    Log.d("image", "uploaded successfully")
                    if (isEdit) {
                        sendMessage()
                    } else {

                        //register();
                    }
                }
            } else {
                if (index < arrayListPhoto.size) {
                    Log.d("image", "uploaded 1")
                    current_path = arrayListPhoto.get(index)
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    Log.d("image", "uploaded 2")

                    //mloading.setVisibility(View.INVISIBLE);
                    //GlobalFunctions.EnableLayout(mainLayout);
                    index = 0
                    Log.d("image", "uploaded successfully")
                    if (isEdit) {
                        sendMessage()
                    } else {

                        //register();
                    }
                }
            }
        }

        override fun doInBackground(vararg params: String?): String? {

            /*String PhotoName = GlobalFunctions.SendMultipartFile(
                    current_path, getString(R.string.UploadPhoto), ".jpg");*/
            var str: String? = "1"
            if (current_path?.contains("http://qenaat")!!
                    || current_path!!.contains(QenaatConstant.BASE_URL!!)
                    || current_path!!.contains("http://localhost:")
                    || current_path!!.contains("http://10.0.0.233:1234")) {
                Log.d("download_status", " inisde if")
            } else {
                Log.d("download_status", " inisde else")


                //if(!current_path.contains("http://easygooccasions.com/")){
                var file: File? = null
                var myBitmap: Bitmap? = null
                var filename: String? = ""
                var angle = 0
                val ei: ExifInterface
                try {
                    ei = ExifInterface(current_path)
                    val orientation = ei.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, 1)
                    when (orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> angle = 90
                        ExifInterface.ORIENTATION_ROTATE_180 -> angle = 180
                        ExifInterface.ORIENTATION_ROTATE_270 -> angle = 270
                    }
                } catch (e1: IOException) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace()
                }
                filename = getNameOfImage(current_path
                )
                Log.d("current_path", "" + current_path)
                file = File(current_path)
                if (file.exists()) {
                    Log.d("current_path", "exist")
                } else {
                    Log.d("current_path", "not exist")
                }
                //                myBitmap = decodeSampledBitmapFromPath(file.getAbsolutePath(),
//                        1000, 1000);
                myBitmap = decodeFile(file)

                //Bitmap resizedImage = Bitmap.createScaledBitmap(myBitmap,1080,desiredimageHeight, true);
                val matrix = Matrix()
                matrix.postRotate(angle.toFloat())
                myBitmap = Bitmap
                        .createBitmap(myBitmap!!, 0, 0, myBitmap?.getWidth()!!,
                                myBitmap.getHeight(), matrix, true)
                val os: OutputStream
                try {
                    os = FileOutputStream(current_path)
                    myBitmap.compress(Bitmap.CompressFormat.PNG, 80, os)
                    os.flush()
                    os.close()
                } catch (e: Exception) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
                var conn: HttpURLConnection? = null
                var dos: DataOutputStream? = null
                var inStream: DataInputStream? = null
                val lineEnd = "\r\n"
                val twoHyphens = "--"
                val boundary = "*****"
                var bytesRead: Int
                var bytesAvailable: Int
                var bufferSize: Int
                val buffer: ByteArray
                val maxBufferSize = 1 * 1024 * 1024
                try {
                    val fileInputStream = FileInputStream(file)
                    //once check url correct or not
                    //URL url = new URL(act.getString(R.string.UploadUserImage));
                    val url = URL(QenaatConstant.Photo_URL + "6")
                    conn = url.openConnection() as HttpURLConnection
                    conn.doInput = true
                    conn.doOutput = true
                    conn.requestMethod = "POST"
                    conn.useCaches = false
                    conn.setRequestProperty("Connection", "Keep-Alive")
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=$boundary")
                    dos = DataOutputStream(conn.outputStream)
                    dos.writeBytes(twoHyphens + boundary + lineEnd)
                    /*dos.writeBytes("Content-Disposition: form-data; "
                            + " supplierId=\"" + mSessionManager.getUserCode() + "\"" + "name=\"pic\";"
                            + " filename=\"" + filename + "\"" + lineEnd);*/dos.writeBytes("Content-Disposition: form-data; name=\"pic\";"
                            + " filename=\"" + filename + "\"" + lineEnd)
                    dos.writeBytes(lineEnd)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    buffer = ByteArray(bufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize)
                        bytesAvailable = fileInputStream.available()
                        bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    }
                    dos.writeBytes(lineEnd)
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)
                    fileInputStream.close()
                    dos.flush()
                    dos.close()
                } catch (ex: MalformedURLException) {
                    println("Error:$ex")
                } catch (ioe: IOException) {
                    println("Error:$ioe")
                }
                try {
                    inStream = DataInputStream(conn?.getInputStream())
                    while (inStream.readLine().also { str = it } != null) {
                        images = StringBuilder()
                        images.append(str)
                        println(str)
                    }
                    inStream.close()
                } catch (ioex: IOException) {
                    println("Error: $ioex")
                }
            }
            return str
        }
    }

    fun decodeSampledBitmapFromPath(path: String?, reqWidth: Int,
                                    reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(path, options)
    }

    fun calculateInSampleSize(options: BitmapFactory.Options?,
                              reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options?.outHeight
        val width = options?.outWidth
        var inSampleSize = 1
        if (height!! > reqHeight || width!! > reqWidth) {
            inSampleSize = if (width!! > height) {
                Math.round(height as Float / reqHeight as Float)
            } else {
                Math.round(width as Float / reqWidth as Float)
            }
        }
        return inSampleSize
    }

    private fun decodeFile(f: File?): Bitmap? {
        try {
            //Decode image size
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(FileInputStream(f), null, o)

            //The new size we want to scale to
            val REQUIRED_SIZE = 500

            //Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE) scale *= 2

            //Decode with inSampleSize
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(FileInputStream(f), null, o2)
        } catch (e: FileNotFoundException) {
        }
        return null
    }

    private fun openImageIntent() {

        // Determine Uri of camera image to save.
        val root = File(Environment.getExternalStorageDirectory().toString() + File.separator + "Wash" + File.separator)
        root.mkdirs()
        val fname = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
        val sdImageMainDirectory = File(root, fname)
        outputFileUri = Uri.fromFile(sdImageMainDirectory)
        val inflater1 = act.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v1 = inflater1.inflate(R.layout.gallery_camera, null)
        val linear_camera = v1.findViewById<View?>(R.id.linear_camera) as LinearLayout
        val img_camera = v1.findViewById<View?>(R.id.img_camera) as ImageView
        val tv_camera = v1.findViewById<View?>(R.id.tv_camera) as TextView
        val linear_gallery = v1.findViewById<View?>(R.id.linear_gallery) as LinearLayout
        val img_gallery = v1.findViewById<View?>(R.id.img_gallery) as ImageView
        val tv_gallery = v1.findViewById<View?>(R.id.tv_gallery) as TextView
        val alert1 = MaterialDialog(act).setContentView(v1)
        alert1.show()
        alert1.setCanceledOnTouchOutside(true)
        linear_gallery.setOnClickListener(View.OnClickListener {
            alert1.dismiss()
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            //intent.setType("image/*");
            //intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, 121)
        })
        linear_camera.setOnClickListener(View.OnClickListener {
            alert1.dismiss()
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
            cameraIntent.putExtra("outputX", 150)
            cameraIntent.putExtra("outputY", 150)
            cameraIntent.putExtra("aspectX", 1)
            cameraIntent.putExtra("aspectY", 1)
            startActivityForResult(cameraIntent, 121)
        })
    }

    fun getPathFromURI(contentUri: Uri?): String? {
        var res: String? = null
        val proj = arrayOf<String?>(MediaStore.Images.Media.DATA)
        val cursor = act.getContentResolver().query(contentUri!!, proj, null, null, null)
        if (cursor?.moveToFirst()!!) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    private fun addMessageObject(msg: String?) {
        if (getRequestHelpMessagesArrayList.size > 0) {
            val messages = GetRequestHelpMessages()
            messages.Photo = ""
            messages.MessageContent = ""
            if (images.toString().length > 0) {
                messages.Photo = QenaatConstant.BASE_URL + "/Images/ChatImages/" + images.toString()
            }

            //GetPickupOrderMessages.User_Web user_web = new GetPickupOrderMessages.User_Web();

            //user_web.setFirstName(mSessionManager.getFirstName());

            //user_web.setLastName(mSessionManager.getLastName());

            //messages.setUser_Web(user_web);
            if (msg?.length!! > 0) {
                messages.MessageContent = msg
            }
            messages.UserId = mSessionManager.getUserCode()
            getRequestHelpMessagesArrayList.add(messages)
            mAdapter = ChatListAdapter(act, getRequestHelpMessagesArrayList)
            my_recycler_view.setAdapter(mAdapter)
            mAdapter.notifyDataSetChanged()
            my_recycler_view.scrollToPosition(getRequestHelpMessagesArrayList.size - 1)

            //clear image variable and message variables
            images = StringBuilder()
            //et_message.setText("");
            arrayListPhoto.clear()
            arrayListPhotoEdit.clear()
            index = 0
            current_path = ""
            imageCount = 0
            mSessionManager.setImagePath("")
        }
    }

    companion object {
        protected val TAG = ChatListFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: ChatListFragment
        lateinit var mainLayout: RelativeLayout
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var mAdapter: RecyclerView.Adapter<*>
        private var images: StringBuilder = StringBuilder()

        fun newInstance(act: FragmentActivity): ChatListFragment {
            fragment = ChatListFragment()
            Companion.act = act
            return fragment
        }
    }
}