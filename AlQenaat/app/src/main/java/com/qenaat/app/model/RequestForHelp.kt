package com.qenaat.app.model

import java.util.*

/**
 * Created by DELL on 15-Jan-18.
 */
class RequestForHelp {
    var RequestName: String? = null
    var MobileNumber: String? = null
    var CountryCode: String? = null
    var HomeType: String? = null
    var RentAmount: String? = null
    var MaritalStatus: String? = null
    var MyIncomeType: String? = null
    var MyIncomeAmount: String? = null
    var HusbandIncomeType: String? = null
    var HusbandIncomeAmount: String? = null
    var WifeIncomeType: String? = null
    var WifeIncomeAmount: String? = null
    var Employer: String? = null
    var WorkType: String? = null
    var Position: String? = null
    var IsWorkingSons: String? = null
    var IsAffairs: String? = null
    var IsExpenses: String? = null
    var IsOther: String? = null
    var IsOrganisationDebt: String? = null
    var IsPersonalDebt: String? = null
    var IsResidence: String? = null
    var IsSchoolFees: String? = null
    var IsInstallment: String? = null
    var IsProvisions: String? = null
    var IsTreatment: String? = null
    var IsDelayedRent: String? = null
    var IsWeakIncome: String? = null
    var IncomeSourceIds: String? = null
    var Editable: String? = null
    var RequestDate: String? = null
    var RequestReasons: String? = null
    var UserId: String? = null
    var requestForHelpId: String? = null
    var Status: String? = null
    var requestForHelpPersonslist: ArrayList<RequestForHelpPersons?>? = null
}