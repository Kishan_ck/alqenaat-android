package com.qenaat.app.restfull

import android.text.TextUtils
import java.util.*

class RequestParameters {
    var map: HashMap<String?, String?>? = null
    var list: ArrayList<HashMap<String?, String?>?>?

    fun setParams(key: String?, value: Any?) {
        map = HashMap()
        map!![key] = if (TextUtils.isEmpty(value.toString())) "" else value.toString()
        list?.add(map)
    }

    private fun processRequestParameters(): String? {
        var parameters = ""
        if (list != null && list?.size!! > 0) {
            for (i in list?.indices!!) {
                parameters += if (i == 0) {
                    "?" + list?.get(i)
                } else {
                    "&" + list?.get(i)
                }
            }
        }
        return parameters.replace("{", "").replace("}", "")
    }

    fun getParams(): String? {
        return processRequestParameters()
    }

    init {
        list = ArrayList()
    }
}