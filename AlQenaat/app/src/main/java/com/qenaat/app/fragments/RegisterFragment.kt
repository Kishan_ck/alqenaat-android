package com.qenaat.app.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import com.jdev.countryutil.Constants
import com.jdev.countryutil.CountryUtil
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util.Companion.getCountryByCode
import com.qenaat.app.Utils.Util.Companion.getUserCountryInfo
import com.qenaat.app.classes.*
import com.qenaat.app.classes.ConstanstParameters.API_SUCCESS_CODE
import com.qenaat.app.classes.ConstanstParameters.COUNTRY_CODE
import com.qenaat.app.classes.ConstanstParameters.COUNTRY_FLAG
import com.qenaat.app.classes.ConstanstParameters.COUNTRY_NAME
import com.qenaat.app.classes.ConstanstParameters.KUWAIT_CODE
import com.qenaat.app.classes.FixControl.checkEmailPattern
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkLength
import com.qenaat.app.classes.FixControl.checkVisibility
import com.qenaat.app.classes.FixControl.encode
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.model.GetAppSettings
import com.qenaat.app.model.GetUser
import com.qenaat.app.model.RegisterModel
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.register.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by shahbazshaikh on 04/07/16.
 */
class RegisterFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var et_username: EditText
    lateinit var et_name: EditText
    lateinit var et_email: EditText
    lateinit var et_password: EditText
    lateinit var et_mobile: TextInputEditText
    lateinit var et_civil_id: EditText
    lateinit var tv_terms: TextView
    lateinit var mtv_register: TextView
    lateinit var tv_female: TextView
    lateinit var tv_male: TextView
    lateinit var img_female: ImageView
    lateinit var img_male: ImageView
    lateinit var linear_female: LinearLayout
    lateinit var linear_male: LinearLayout
    lateinit var LL_country_code: LinearLayout
    var isEdit = false
    lateinit var getUser: GetUser
    var isMale: String? = "0"
    var delay: Long = 1000 // 1 seconds after user stops typing
    var last_text_edit: Long = 0
    var handler: Handler? = Handler()
    private var appSettings: GetAppSettings? = null
    private var countryCode: String = ""
    private var countryFlag: Int = 0
    private var countryName: String = ""
    private var termsCondition = false
    private var isRegister = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        countryCode = getUserCountryInfo(act, COUNTRY_CODE)
        countryName = getUserCountryInfo(act, COUNTRY_NAME)
        countryFlag = getUserCountryInfo(act, COUNTRY_FLAG).toInt()

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)

            if (arguments != null) {
                isEdit = if (arguments?.getString("type").equals("register", ignoreCase = true)) {
                    false
                } else {
                    isRegister = true
                    true
                }
            }
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            mainLayout = inflater.inflate(R.layout.register, null) as RelativeLayout
            act.softWindow()
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            et_civil_id = mainLayout.findViewById<View?>(R.id.et_civil_id) as EditText
            et_username = mainLayout.findViewById<View?>(R.id.et_username) as EditText
            et_name = mainLayout.findViewById<View?>(R.id.et_name) as EditText
            et_password = mainLayout.findViewById<View?>(R.id.et_password) as EditText
            et_email = mainLayout.findViewById<View?>(R.id.et_email) as EditText
            tv_terms = mainLayout.findViewById<View?>(R.id.tv_terms) as TextView
            tv_male = mainLayout.findViewById<View?>(R.id.tv_male) as TextView
            tv_female = mainLayout.findViewById<View?>(R.id.tv_female) as TextView
            tv_terms.setOnClickListener(this)
            img_female = mainLayout.findViewById<View?>(R.id.img_female) as ImageView
            img_male = mainLayout.findViewById<View?>(R.id.img_male) as ImageView
            linear_female = mainLayout.findViewById<View?>(R.id.linear_female) as LinearLayout
            linear_male = mainLayout.findViewById<View?>(R.id.linear_male) as LinearLayout
            linear_female.setOnClickListener(this)
            linear_male.setOnClickListener(this)
            mtv_register = mainLayout.findViewById<View?>(R.id.tv_register) as TextView
            mtv_register.setOnClickListener(this)
            LL_country_code = mainLayout.findViewById<View?>(R.id.LL_country_code) as LinearLayout
            LL_country_code.setOnClickListener(this)
            tv_terms_condition.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)
            et_mobile = mainLayout.findViewById<View?>(R.id.et_mobile) as TextInputEditText
            //setMobilePlaceholderText(true, et_mobile, "$countryCode | " + getString(R.string.MobileLabel))
            //et_mobile.prefix = "$countryCode | "
            tv_country_code.isSelected = true
            tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
            iv_country_flag.setImageResource(countryFlag)
            setFamilyView()

            if (isEdit) {
                GetUser()
                mtv_register.setText(act.getString(R.string.SaveLabel))
            } else {
                mtv_register.setText(act.getString(R.string.RegisterLabel))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        //  ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        getSettings()
        if (isEdit) {
            tv_terms.setVisibility(View.GONE)
            LL_terms_condition.setVisibility(View.GONE)
            til_password.setVisibility(View.GONE)
            //Snackbar.make(mainLayout, act.getString(R.string.KeepPasswordEmpty), Snackbar.LENGTH_LONG).show()
            ContentActivity.Companion.mtv_topTitle.setText(R.string.EditProfileLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        } else {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.RegisterLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        }
        setData()
        //GetUser()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.LL_country_code -> CountryUtil(act).setTitle("").build()
            R.id.linear_male -> {
                isMale = "1"
                img_male.setImageResource(R.drawable.radio_on)
                img_female.setImageResource(R.drawable.radio_off)
            }
            R.id.linear_female -> {
                isMale = "2"
                img_male.setImageResource(R.drawable.radio_off)
                img_female.setImageResource(R.drawable.radio_on)
            }
            R.id.tv_terms_condition -> {
                val b = Bundle()
                b.putString("type", "terms")
                termsCondition = true
                ContentActivity.Companion.openAboutUsFragment(b)
            }
            R.id.tv_register -> {
                act.hideKeyboard()


                if (isEdit) {
                    if (isValid()) {

                        Log.e("countryCodee>>>>>>",""+countryCode)

                        mloading.setVisibility(View.VISIBLE)
                        GlobalFunctions.DisableLayout(mainLayout)
                        QenaatAPICall.getCallingAPIInterface()?.EditUser(
                            "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                            mSessionManager.getUserCode()?.encode()?.encode(),
                            et_email.getTextValue().encode().encode(),
                            et_username.getTextValue().encode().encode(),
                            /*GlobalFunctions.EncodeParameter(if (et_password.getText().toString().length > 0) et_password.getText().toString() else getUser.Password),*/
                            languageSeassionManager.getRegId()?.encode()?.encode(),
                            et_name.getTextValue().encode().encode(),
                            et_mobile.getTextValue().encode().encode(),
                            countryCode.encode().encode(),
                            et_civil_id.getTextValue().encode().encode(),
                            isMale?.encode()?.encode(),
                            if (ch_family.isChecked) "1".encode().encode() else "0".encode()
                                .encode(),
                            et_alternative_mobile.getTextValue().encode().encode()
                        )?.enqueue(
                            object : Callback<ResponseBody?> {

                                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                    t.printStackTrace()
                                    mloading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(
                                    call: Call<ResponseBody?>,
                                    response: Response<ResponseBody?>
                                ) {
                                    val body = response?.body()
                                    var outResponse = ""
                                    try {
                                        val reader = BufferedReader(
                                            InputStreamReader(
                                                ByteArrayInputStream(body?.bytes())
                                            )
                                        )
                                        val out = StringBuilder()
                                        val newLine = System.getProperty("line.separator")
                                        var line: String?
                                        while (reader.readLine().also { line = it } != null) {
                                            out.append(line)
                                            out.append(newLine)
                                        }
                                        outResponse = out.toString()
                                        Log.d("outResponse", "" + outResponse)
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                    if (outResponse != null) {
                                        outResponse = outResponse.replace("\"", "")
                                        outResponse = outResponse.replace("\n", "")
                                        Log.e("outResponse not null ", outResponse)
                                        if (outResponse.toInt() > 0) {
                                            isRegister = true
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.ProfileUpdated),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                            mSessionManager.LoginSeassion()
                                            mSessionManager.setUserCode(outResponse)
                                            mSessionManager.setUserName(
                                                et_username.getText().toString()
                                            )
                                            mSessionManager.setUserPassword(
                                                et_password.getText().toString()
                                            )
                                            act.getSupportFragmentManager().popBackStack()
                                        } else if (outResponse == "-1") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.OperationFailed),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-2") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.DataMissing),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-3") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.UserNotExistsLabel),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-4") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.emailExistLabel),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-5") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.AlreadyExistLabel),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-6") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.NotValidCivilId),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-7") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.civil_id_already_exists),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        } else if (outResponse == "-8") {
                                            Snackbar.make(
                                                mainLayout,
                                                act.getString(R.string.NotValidPhoneNumber),
                                                Snackbar.LENGTH_LONG
                                            ).show()
                                        }
                                    }
                                    mloading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                    }
                    /*} else {
                        Snackbar.make(mainLayout, act.getString(R.string.FillAllFields), Snackbar.LENGTH_LONG).show()
                    }*/
                } else {
                    if (isValid()) {
                        mloading.putVisibility(View.VISIBLE)
                        GlobalFunctions.DisableLayout(mainLayout)
                        var phonnumber = "" + et_mobile.text.toString()
                        val first = phonnumber[0]
                        if (first.toString() == "0") {
                            phonnumber = phonnumber.substring(1, phonnumber.length)
                        }

                        Log.e("countrycodeee",""+countryCode)

                        QenaatAPICall.getCallingAPIInterface()?.register(
                            et_name.getTextValue().encode().encode(),
                            et_email.getTextValue().encode().encode(),
                            phonnumber.encode().encode(),
                            if (ch_family.isChecked) "1".encode().encode() else "0".encode()
                                .encode(),
                            et_alternative_mobile.getTextValue().encode().encode(),
                            et_username.getTextValue().encode().encode(),
                            et_password.getTextValue().encode().encode(),
                            et_civil_id.getTextValue().encode().encode(),
                            isMale?.encode()?.encode(),
                            countryCode.encode().encode(),
                            languageSeassionManager.getRegId()?.encode()?.encode()
                        )?.enqueue(
                            object : Callback<RegisterModel> {

                                override fun onFailure(call: Call<RegisterModel>, t: Throwable) {
                                    t.printStackTrace()
                                    mloading.putVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(
                                    call: Call<RegisterModel>,
                                    response: Response<RegisterModel>
                                ) {
                                    if (response.body() != null) {
                                        if (response.code() == API_SUCCESS_CODE) {
                                            val registerModel = response.body()
                                            when (registerModel?.code) {
                                                "0" -> {
                                                    Alert.alert(context,
                                                        getString(R.string.app_name),
                                                        getString(R.string.register_successfully),
                                                        "",
                                                        getString(R.string.ok),
                                                        null,
                                                        Runnable {
                                                            if (appSettings != null) if (!appSettings?.ShowActivePage!!) {
                                                                isRegister = true
                                                                mSessionManager.LoginSeassion()
                                                                mSessionManager.setAuthToken(
                                                                    registerModel.token
                                                                )
                                                                mSessionManager.setUserCode(
                                                                    registerModel.Id.toString()
                                                                )
                                                                mSessionManager.setUserName(
                                                                    et_username.getTextValue()
                                                                )
                                                                mSessionManager.setUserPassword(
                                                                    et_password.getTextValue()
                                                                )


                                                                fragmentManager?.popBackStack()

//                                                                ContentActivity.Companion.openLoginFragment()

                                                                /*val bundle = Bundle()
                                                                bundle.putString("comingFrom", "home")
                                                                openNewsFragment(bundle)*/
                                                            }

                                                        })
                                                }
                                                "-1" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.OperationFailed)!!)
                                                }
                                                "-2" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.DataMissing)!!)
                                                }
                                                "-3" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.PhoneExists)!!)
                                                }
                                                "-4" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.EmailExists)!!)
                                                }
                                                "-5" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.UserNameExists)!!)
                                                }
                                                "-6" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.NotValidCivilId)!!)
                                                }
                                                "-7" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.civil_id_already_exists)!!)
                                                }
                                                "-8" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.NotValidPhoneNumber)!!)
                                                }
                                            }
                                        }
                                    }
                                    mloading.putVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                    }
                }
            }
        }
    }

    private fun isValid(): Boolean {
        when {
            et_name.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_name_civil))
                et_name.requestFocus()
                return false
            }
            et_username.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_username))
                et_username.requestFocus()
                return false
            }
            et_email.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_email))
                et_email.requestFocus()
                return false
            }
            !et_email.checkEmailPattern() -> {
                mainLayout.showSnakeBar(getString(R.string.EmailIsWrongLabel))
                et_email.requestFocus()
                return false
            }
            et_mobile.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_mobile_number))
                et_mobile.requestFocus()
                return false
            }
            et_civil_id.checkEmpty() -> {

                mainLayout.showSnakeBar(getString(R.string.enter_civil_id))
                et_civil_id.requestFocus()
                return false

            }
            isMale.equals("0", ignoreCase = true) -> {
                mainLayout.showSnakeBar(getString(R.string.select_gender))
                return false
            }
            !isEdit && et_password.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_password))
                et_password.requestFocus()
                return false
            }
            !isEdit && !et_password.checkLength(6) -> {
                mainLayout.showSnakeBar(getString(R.string.NotValidPassword))
                et_password.requestFocus()
                return false
            }
            !isEdit && !ch_terms.isChecked -> {
                mainLayout.showSnakeBar(getString(R.string.read_terms_condition))
                return false
            }
            else -> {
                when {
                    LL_family.checkVisibility(View.VISIBLE) -> {
                        when (ch_family.isChecked) {
                            true -> {
                                return when {
                                    et_alternative_mobile.checkEmpty() -> {
                                        mainLayout.showSnakeBar(getString(R.string.enter_mobile_number))
                                        et_alternative_mobile.requestFocus()
                                        false
                                    }
                                    else -> true
                                }
                            }
                            false -> {
                                mainLayout.showSnakeBar("Select Family")
                                return false
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    private fun setFamilyView() {
        if (countryCode.contains(KUWAIT_CODE))
            LL_family.putVisibility(View.GONE)
        else
            LL_family.putVisibility(View.GONE)
    }

    private fun GetUser() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetUser(
            "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
            mSessionManager.getUserCode()
        )?.enqueue(
            object : Callback<ArrayList<GetUser?>?> {

                override fun onFailure(call: Call<ArrayList<GetUser?>?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.INVISIBLE)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetUser?>?>,
                    response: Response<ArrayList<GetUser?>?>
                ) {
                    if (response.body() != null) {
                        val getUsers = response.body()
                        if (getUsers != null) {
                            if (getUsers.size > 0) {
                                getUser = getUsers[0]!!
                                et_email.setText(getUsers[0]?.Email)
                                et_username.setText(getUsers[0]?.UserName)
                                et_civil_id.setText(getUsers[0]?.CivilId)
                                img_male.setImageResource(R.drawable.radio_off)
                                img_female.setImageResource(R.drawable.radio_off)
                                if (getUsers[0]?.Gender.equals("1", ignoreCase = true)) {
                                    img_male.setImageResource(R.drawable.radio_on)
                                    isMale = "1"
                                }
                                if (getUsers[0]?.Gender.equals("2", ignoreCase = true)) {
                                    img_female.setImageResource(R.drawable.radio_on)
                                    isMale = "2"
                                }
                                et_mobile.setText(getUsers[0]?.Phone)
                                et_name.setText(getUsers[0]?.Name)
                                val country = getCountryByCode(act, getUsers[0]?.CountryCode!!)
                                countryCode = country.dialCode
                                countryName = country.code
                                countryFlag = country.flag
                                tv_country_code.text =
                                    String.format("(%s) %s", countryName, countryCode)
                                iv_country_flag.setImageResource(countryFlag)
                            }
                        }
                    }
                    mloading.setVisibility(View.INVISIBLE)
                }
            })
    }

    private val input_finish_checker: Runnable? = Runnable {
        if (System.currentTimeMillis() > last_text_edit + delay - 500) {
            // TODO: do what you need here
            // ............
            // ............
            //GetUserInfo();
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        if (!isRegister)
            ContentActivity.Companion.openLoginFragment()

        if (handler != null) {
            handler!!.removeCallbacks(input_finish_checker)
        }
    }

    private fun setData() {
        img_male.setImageResource(R.drawable.radio_off)
        img_female.setImageResource(R.drawable.radio_off)
        if (isMale.equals("1", ignoreCase = true)) {
            img_male.setImageResource(R.drawable.radio_on)
        }
        if (isMale.equals("2", ignoreCase = true)) {
            img_female.setImageResource(R.drawable.radio_on)
        }
        //
//        et_civil_id.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//                if (s.length() > 11) {
//
//                    if(GlobalFunctions.validateCivilID(et_civil_id.getText().toString())){
//
//                        last_text_edit = System.currentTimeMillis();
//
//                        handler.postDelayed(input_finish_checker, delay);
//
//                    }
//                    else{
//
//                        FixControl.hideKeybord(mainLayout, act);
//
//                        Snackbar.make(mainLayout, act.getString(R.string.NotValidCivilId), Snackbar.LENGTH_LONG).show();
//
//                    }
//
//                }
//
//            }
//
//        });
    }

    private fun getSettings() {
        QenaatAPICall.getCallingAPIInterface()?.settings()
            ?.enqueue(object : Callback<GetAppSettings?> {

                override fun onFailure(call: Call<GetAppSettings?>, t: Throwable) {
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<GetAppSettings?>,
                    response: Response<GetAppSettings?>
                ) {
                    if (response.body() != null) {
                        val settings = response.body()
                        if (settings != null) {
                            if (settings != null) {
                                appSettings = settings
                            }
                        }
                    }
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.KEY_RESULT_CODE) {
            try {
                countryName = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                countryCode = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
                countryFlag = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                iv_country_flag.setImageResource(countryFlag)
                setFamilyView()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        protected val TAG = RegisterFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: RegisterFragment
        fun newInstance(act: FragmentActivity): RegisterFragment {
            fragment = RegisterFragment()
            Companion.act = act
            return fragment
        }
    }
}