package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.ProjectDonationAdapter
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetProjects
import com.qenaat.app.networking.QenaatAPICall
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 14-Nov-17.
 */
class ProjectForDonationFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var getProjectsForDonations: ArrayList<GetProjects> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        initViews(mainLayout)
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.DonateForProjectLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)

//        if(getProjectsForDonations.size()>0){
//            mAdapter = new ProjectDonationAdapter(act, getProjectsForDonations);
//
//            my_recycler_view.setAdapter(mAdapter);
//        }
//        else{
        GetProjectsForDonation()
        // }
    }

    fun GetProjectsForDonation() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetProjectsForDonation()?.enqueue(
                object : Callback<ArrayList<GetProjects>?> {

                    override fun onFailure(call: Call<ArrayList<GetProjects>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetProjects>?>, response: Response<ArrayList<GetProjects>?>) {
                        if (response.body() != null) {
                            val getProjectsForDonations = response.body()
                            if (mloading != null && getProjectsForDonations != null) {
                                Log.d("getContentses size", "" + getProjectsForDonations.size)
                                this@ProjectForDonationFragment.getProjectsForDonations.clear()
                                this@ProjectForDonationFragment.getProjectsForDonations.addAll(getProjectsForDonations)
                                mAdapter = ProjectDonationAdapter(act, this@ProjectForDonationFragment.getProjectsForDonations)
                                my_recycler_view.setAdapter(mAdapter)

//                    my_recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//
//
//                            if (!endOfREsults) {
//
//                                visibleItemCount = mLayoutManager.getChildCount();
//
//                                totalItemCount = mLayoutManager.getItemCount();
//
//                                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                                if (loading_flag) {
//
//
//                                    if ((visibleItemCount + pastVisiblesItems) + 2 >= totalItemCount) {
//
//
//                                        if (!(ProjectForDonationFragment.this.getProjectsForDonations.size() == 0)) {
//
//                                            progress_loading_more.setVisibility(View.VISIBLE);
//                                            //GlobalFunctions.DisableLayout(mainLayout);
//
//                                        }
//
//                                        loading_flag = false;
//
//                                        page_index = page_index + 1;
//
//                                        GetMoreProjectsForDonation();
//
//
//                                    }
//                                }
//
//                            }
//                        }
//                    });
                            }
                        }
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    fun GetMoreProjectsForDonation() {
        QenaatAPICall.getCallingAPIInterface()?.GetProjectsForDonation()?.enqueue(
                object : Callback<ArrayList<GetProjects>?> {

                    override fun onFailure(call: Call<ArrayList<GetProjects>?>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetProjects>?>, response: Response<ArrayList<GetProjects>?>) {
                        if (response.body() != null) {
                            val projectsForDonations = response.body()
                            if (progress_loading_more != null) {
                                progress_loading_more.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                if (projectsForDonations != null) {
                                    if (projectsForDonations.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    getProjectsForDonations.addAll(projectsForDonations)
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                })
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        protected val TAG = ProjectForDonationFragment::class.java.simpleName
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var act: FragmentActivity
        lateinit var fragment: ProjectForDonationFragment
        fun newInstance(act: FragmentActivity): ProjectForDonationFragment {
            fragment = ProjectForDonationFragment()
            Companion.act = act
            return fragment
        }
    }
}