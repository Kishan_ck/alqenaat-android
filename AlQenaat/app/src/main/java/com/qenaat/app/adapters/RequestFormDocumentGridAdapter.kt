package com.qenaat.app.adapters

import android.content.Intent
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetRequestForHelpDocuments
import com.squareup.picasso.Picasso
import java.util.*

/**
 * Created by DELL on 17-Jan-18.
 */
class RequestFormDocumentGridAdapter(var act: FragmentActivity,
                                     private val itemsData: ArrayList<GetRequestForHelpDocuments>) : RecyclerView.Adapter<RequestFormDocumentGridAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager
    var sessionManager: SessionManager
    var imgH = 0
    var imgW = 0
    var productImages: Array<String?>? = null

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.random_list_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (itemsData.get(position) != null) {
            viewHolder.img_ad.setVisibility(View.GONE)
            viewHolder.linear_product_big.setVisibility(View.GONE)
            viewHolder.relative_left.setVisibility(View.GONE)
            viewHolder.linear_product_big.setVisibility(View.GONE)
            viewHolder.linear_product_big.setPadding(0, 0, 0, 5)
            Log.d("main_postition", position.toString() + "")
            val finalPosition = position * 3 + 0
            var finalId: String? = ""
            if (itemsData.get(finalPosition) != null) {
                finalId = itemsData.get(finalPosition).Id
            }
            Log.d("finalPosition--0001==", "$finalPosition-$finalId")
            if (finalId.equals("-1", ignoreCase = true)) {
                Log.d("single_line", "i m inside single line")
                viewHolder.img_ad.setVisibility(View.VISIBLE)
                viewHolder.relative_left_big.setVisibility(View.GONE)
                viewHolder.relative_right_big.setVisibility(View.GONE)
                if (itemsData.get(finalPosition) != null) {
                    imgW = (act.getResources().getDrawable(
                            R.drawable.ad) as BitmapDrawable).bitmap.width
                    imgH = (act.getResources().getDrawable(
                            R.drawable.ad) as BitmapDrawable).bitmap.height

                    //imgH = 300;
                    viewHolder.img_ad.getLayoutParams().width = imgW
                    viewHolder.img_ad.getLayoutParams().height = imgH

//                    Log.d("getDetails", itemsData.get(finalPosition).getDescription());
//
//                    if (itemsData.get(finalPosition).getDescription().length() > 0) {
//
//                        Picasso.with(act).load(itemsData.get(finalPosition).getDescription()).
//                                placeholder(R.drawable.ad)
//                                .error(R.drawable.ad).fit()
//                                .into(viewHolder.img_ad);
//
//                    }
                    viewHolder.img_ad.setOnClickListener(View.OnClickListener {
                        act.startActivity(Intent(Intent.ACTION_VIEW,
                                Uri.parse(itemsData.get(finalPosition).DocumentName)))
                    })
                }
            } else {
                viewHolder.linear_product_big.setVisibility(View.VISIBLE)
                viewHolder.relative_left_big.setVisibility(View.VISIBLE)
                viewHolder.relative_right_big.setVisibility(View.VISIBLE)
                if (itemsData.get(finalPosition) != null) {
                    viewHolder.relative_left_big.setTag(itemsData.get(finalPosition))
                    viewHolder.relative_left_big.setOnClickListener(View.OnClickListener {
                        val gson = Gson()
                        var b = Bundle()

//                            b.putString("GetProducts", gson.toJson((R)viewHolder.relative_left_big.getTag()));
//
//                            ContentActivity.openProductDetailsFragment(b);
                        if (itemsData.size > 0) {
                            //productImages = arrayOf(arrayOfNulls<String>(itemsData.size).toString())
                            productImages = arrayOfNulls<String?>(itemsData.size)
                            for (i in itemsData.indices) {
                                val photo = itemsData.get(i)
                                photo.DocumentName.let { productImages!![i] = it }
                                //productImages!![i] = photo.DocumentName
                            }
                        }
                        b = Bundle()
                        b.putInt("position", 0)
                        b.putStringArray("productImages", productImages)
                        ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                    })
                    viewHolder.tv_title_big.setVisibility(View.GONE)
                    viewHolder.tv_price_big.setVisibility(View.GONE)
                    imgW = (act.getResources().getDrawable(
                            R.drawable.prod_no_img3) as BitmapDrawable).bitmap.width
                    imgH = (act.getResources().getDrawable(
                            R.drawable.prod_no_img3) as BitmapDrawable).bitmap.height
                    viewHolder.img_product_big.getLayoutParams().width = imgW
                    viewHolder.img_product_big.getLayoutParams().height = imgH
                    if (itemsData.get(finalPosition).DocumentName?.length!! > 0) {
                        Picasso.with(act).load(itemsData.get(finalPosition).DocumentName).placeholder(R.drawable.prod_no_img3)
                                .error(R.drawable.prod_no_img3).fit()
                                .centerCrop().into(viewHolder.img_product_big)
                    }
                }

                //second item
                val finalPosition1 = position * 3 + 1
                if (itemsData.get(finalPosition1) != null) {
                    if (!itemsData.get(finalPosition1).Id.equals("-11", ignoreCase = true)) {
                        viewHolder.relative_center_big.setTag(itemsData.get(finalPosition1))
                        viewHolder.relative_center_big.setOnClickListener(View.OnClickListener {
                            val gson = Gson()
                            var b = Bundle()

//                                b.putString("GetProducts", gson.toJson((GetProducts)viewHolder.relative_center_big.getTag()));
//
//                                ContentActivity.openProductDetailsFragment(b);
                            if (itemsData.size > 0) {
                                productImages = arrayOfNulls<String?>(itemsData.size)
                                for (i in itemsData.indices) {
                                    val photo = itemsData.get(i)
                                    photo.DocumentName.let { productImages!![i] = it }
                                }
                            }
                            b = Bundle()
                            b.putInt("position", 0)
                            b.putStringArray("productImages", productImages)
                            ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                        })
                        viewHolder.tv_title_big1.setVisibility(View.GONE)
                        viewHolder.tv_price_big1.setVisibility(View.GONE)
                        imgW = (act.getResources().getDrawable(
                                R.drawable.prod_no_img3) as BitmapDrawable).bitmap.width
                        imgH = (act.getResources().getDrawable(
                                R.drawable.prod_no_img3) as BitmapDrawable).bitmap.height
                        viewHolder.img_product_big1.getLayoutParams().width = imgW
                        viewHolder.img_product_big1.getLayoutParams().height = imgH
                        if (itemsData.get(finalPosition1).DocumentName?.length!! > 0) {
                            Picasso.with(act).load(itemsData.get(finalPosition1).DocumentName).placeholder(R.drawable.prod_no_img3)
                                    .error(R.drawable.prod_no_img3).fit()
                                    .centerCrop().into(viewHolder.img_product_big1)
                        }
                    } else {
                        (viewHolder as ViewHolder?)?.relative_center_big?.setVisibility(View.INVISIBLE)
                    }
                }


                //third item
                val finalPosition2 = position * 3 + 2
                if (itemsData.get(finalPosition2) != null) {
                    if (!itemsData.get(finalPosition2).Id.equals("-11", ignoreCase = true)) {
                        viewHolder.relative_right_big.setTag(itemsData.get(finalPosition2))
                        viewHolder.relative_right_big.setOnClickListener(View.OnClickListener {
                            val gson = Gson()
                            var b = Bundle()

//                                b.putString("GetProducts", gson.toJson((GetProducts)viewHolder.relative_right_big.getTag()));
//
//                                ContentActivity.openProductDetailsFragment(b);
                            if (itemsData.size > 0) {
                                productImages = arrayOfNulls<String?>(itemsData.size)
                                for (i in itemsData.indices) {
                                    val photo = itemsData.get(i)
                                    photo.DocumentName.let { productImages!![i] = it }
                                }
                            }
                            b = Bundle()
                            b.putInt("position", 0)
                            b.putStringArray("productImages", productImages)
                            ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                        })
                        imgW = (act.getResources().getDrawable(
                                R.drawable.prod_no_img3) as BitmapDrawable).bitmap.width
                        imgH = (act.getResources().getDrawable(
                                R.drawable.prod_no_img3) as BitmapDrawable).bitmap.height
                        viewHolder.img_product_big2.getLayoutParams().width = imgW
                        viewHolder.img_product_big2.getLayoutParams().height = imgH
                        if (itemsData.get(finalPosition2).DocumentName?.length!! > 0) {
                            Picasso.with(act).load(itemsData.get(finalPosition2).DocumentName).placeholder(R.drawable.prod_no_img3)
                                    .error(R.drawable.prod_no_img3).fit()
                                    .centerCrop().into(viewHolder.img_product_big2)
                        }
                    } else {
                        (viewHolder as ViewHolder?)?.relative_right_big?.setVisibility(View.INVISIBLE)
                    }
                }
            }
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_product: ImageView
        var img_ad: ImageView
        var img_call: ImageView
        var img_product_big: ImageView
        var img_product_big1: ImageView
        var img_product_big2: ImageView
        var img_comment: ImageView

        //img_line1, img_video, img_line2, img_fav
        var tv_title: TextView
        var tv_price: TextView
        var tv_call: TextView
        var tv_category: TextView
        var tv_city: TextView
        var tv_price_big: TextView
        var tv_title_big: TextView
        var tv_price_big1: TextView
        var tv_title_big1: TextView
        var tv_comment: TextView

        //tv_date
        var relative_left: RelativeLayout
        var relative_left_big: RelativeLayout
        var relative_right_big: RelativeLayout
        var relative_comment: RelativeLayout
        var relative_center_big: RelativeLayout

        //linear_left, relative_bottom
        var linear_product_big: LinearLayout

        init {
            relative_center_big = itemLayoutView.findViewById<View?>(R.id.relative_center_big) as RelativeLayout
            relative_comment = itemLayoutView.findViewById<View?>(R.id.relative_comment) as RelativeLayout
            img_comment = itemLayoutView.findViewById<View?>(R.id.img_comment) as ImageView
            img_product_big2 = itemLayoutView.findViewById<View?>(R.id.img_product_big2) as ImageView
            img_product_big1 = itemLayoutView.findViewById<View?>(R.id.img_product_big1) as ImageView
            img_product_big = itemLayoutView.findViewById<View?>(R.id.img_product_big) as ImageView
            tv_comment = itemLayoutView.findViewById<View?>(R.id.tv_comment) as TextView
            tv_title_big = itemLayoutView.findViewById<View?>(R.id.tv_title_big) as TextView
            tv_price_big1 = itemLayoutView.findViewById<View?>(R.id.tv_price_big1) as TextView
            tv_title_big1 = itemLayoutView.findViewById<View?>(R.id.tv_title_big1) as TextView
            tv_price_big = itemLayoutView.findViewById<View?>(R.id.tv_price_big) as TextView
            relative_right_big = itemLayoutView.findViewById<View?>(R.id.relative_right_big) as RelativeLayout
            relative_left_big = itemLayoutView.findViewById<View?>(R.id.relative_left_big) as RelativeLayout
            linear_product_big = itemLayoutView.findViewById<View?>(R.id.linear_product_big) as LinearLayout

            //img_video = (ImageView) itemLayoutView.findViewById(R.id.img_video);
            img_call = itemLayoutView.findViewById<View?>(R.id.img_call) as ImageView

            //img_line1 = (ImageView) itemLayoutView.findViewById(R.id.img_line1);

            //img_line2 = (ImageView) itemLayoutView.findViewById(R.id.img_line2);

            //img_fav = (ImageView) itemLayoutView.findViewById(R.id.img_fav);
            tv_call = itemLayoutView.findViewById<View?>(R.id.tv_call) as TextView
            tv_category = itemLayoutView.findViewById<View?>(R.id.tv_category) as TextView

            //relative_bottom = (RelativeLayout) itemLayoutView.findViewById(R.id.relative_bottom);

            //linear_left = (RelativeLayout) itemLayoutView.findViewById(R.id.linear_left);

            //tv_date = (TextView) itemLayoutView.findViewById(R.id.tv_date);
            tv_price = itemLayoutView.findViewById<View?>(R.id.tv_price) as TextView
            img_product = itemLayoutView.findViewById<View?>(R.id.img_product) as ImageView
            tv_city = itemLayoutView.findViewById<View?>(R.id.tv_city) as TextView
            img_ad = itemLayoutView.findViewById<View?>(R.id.img_ad) as ImageView
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            relative_left = itemLayoutView
                    .findViewById<View?>(R.id.relative_left) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_left)
            tv_price.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        var returnRow = 0
        returnRow = if (itemsData.size % 3 == 0) {
            itemsData.size / 3 + 0
        } else {
            itemsData.size / 3 + 1
        }
        return returnRow
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
        sessionManager = SessionManager(act)
    }
}