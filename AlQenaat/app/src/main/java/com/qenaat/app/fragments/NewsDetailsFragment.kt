package com.qenaat.app.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.util.Linkify
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.showToast
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.GlobalFunctions.CALL_PERMISSION_CODE
import com.qenaat.app.classes.GlobalFunctions.LOCATION_PERMISSION_CODE
import com.qenaat.app.classes.GlobalFunctions.STORAGE_PERMISSION_CODE
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.classes.SocialMediaShare.imgUri
import com.qenaat.app.classes.SocialMediaShare.shareContext
import com.qenaat.app.classes.SocialMediaShare.shareData
import com.qenaat.app.model.GetNews
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.io.File
import java.io.FileOutputStream
import java.util.*

/**
 * Created by DELL on 09-Nov-17.
 */
class NewsDetailsFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var relative_top: RelativeLayout
    lateinit var img_event: ImageView
    lateinit var img_reminder: ImageView
    lateinit var img_fav: ImageView
    lateinit var img_share: ImageView
    lateinit var img_share1: ImageView
    lateinit var img_report: ImageView
    lateinit var img_facebook: ImageView
    lateinit var img_instagram: ImageView
    lateinit var img_twitter: ImageView
    lateinit var img_youtube: ImageView
    lateinit var img_location: ImageView
    lateinit var img_call: ImageView
    lateinit var img_calender: ImageView
    lateinit var linear_date: LinearLayout
    lateinit var linear_bottom: LinearLayout
    lateinit var linear_social: ConstraintLayout
    lateinit var tv_days: TextView
    lateinit var tv_views: TextView
    lateinit var tv_occasion_details: TextView
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var getNews: GetNews
    lateinit var productImages: Array<String?>
    private val CALL_PHONE_PERMISSION_CODE = 23
    lateinit var pShareUri: Uri
    var file: File? = null
    lateinit var relative_desc: RelativeLayout
    lateinit var tv_tree: TextView
    var loadingFinished = true
    var redirect = false
    lateinit var webView: WebView
    lateinit var img_count: TextView
    lateinit var img_expire: ImageView
    lateinit var relative_gallery: RelativeLayout
    var permissionStr = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("GetNews")) {
                    val gson = Gson()
                    getNews = gson.fromJson(
                        arguments!!.getString("GetNews"),
                        GetNews::class.java
                    )
                }
            }
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            mainLayout = inflater.inflate(R.layout.news_details, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            relative_gallery =
                mainLayout.findViewById<View?>(R.id.relative_gallery) as RelativeLayout
            img_expire = mainLayout.findViewById<View?>(R.id.img_expire) as ImageView
            img_count = mainLayout.findViewById<View?>(R.id.img_count) as TextView
            webView = mainLayout.findViewById<View?>(R.id.webView) as WebView
            tv_tree = mainLayout.findViewById<View?>(R.id.tv_tree) as TextView
            relative_desc = mainLayout.findViewById<View?>(R.id.relative_desc) as RelativeLayout
            img_reminder = mainLayout.findViewById<View?>(R.id.img_reminder) as ImageView
            img_fav = mainLayout.findViewById<View?>(R.id.img_fav) as ImageView
            img_share = mainLayout.findViewById<View?>(R.id.img_share) as ImageView
            img_share1 = mainLayout.findViewById<View?>(R.id.img_share1) as ImageView
            img_report = mainLayout.findViewById<View?>(R.id.img_report) as ImageView
            img_facebook = mainLayout.findViewById<View?>(R.id.img_facebook) as ImageView
            img_instagram = mainLayout.findViewById<View?>(R.id.img_instagram) as ImageView
            img_twitter = mainLayout.findViewById<View?>(R.id.img_twitter) as ImageView
            img_youtube = mainLayout.findViewById<View?>(R.id.img_youtube) as ImageView
            img_location = mainLayout.findViewById<View?>(R.id.img_location) as ImageView
            img_call = mainLayout.findViewById<View?>(R.id.img_call) as ImageView
            img_calender = mainLayout.findViewById<View?>(R.id.img_calender) as ImageView
            img_event = mainLayout.findViewById<View?>(R.id.img_event) as ImageView
            tv_views = mainLayout.findViewById<View?>(R.id.tv_views) as TextView
            tv_occasion_details =
                mainLayout.findViewById<View?>(R.id.tv_occasion_details) as TextView
            tv_days = mainLayout.findViewById<View?>(R.id.tv_days) as TextView
            linear_date = mainLayout.findViewById<View?>(R.id.linear_date) as LinearLayout
            linear_bottom = mainLayout.findViewById<View?>(R.id.linear_bottom) as LinearLayout
            linear_social = mainLayout.findViewById<View?>(R.id.linear_social) as ConstraintLayout
            relative_top = mainLayout.findViewById<View?>(R.id.relative_top) as RelativeLayout
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            img_calender.setOnClickListener(this)
            img_call.setOnClickListener(this)
            img_location.setOnClickListener(this)
            img_youtube.setOnClickListener(this)
            img_twitter.setOnClickListener(this)
            img_instagram.setOnClickListener(this)
            img_facebook.setOnClickListener(this)
            img_report.setOnClickListener(this)
            img_share.setOnClickListener(this)
            img_share1.setOnClickListener(this)
            img_fav.setOnClickListener(this)
            img_reminder.setOnClickListener(this)
            img_event.setOnClickListener(this)
            tv_views.setTypeface(ContentActivity.Companion.tf)
            tv_tree.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            tv_occasion_details.setTypeface(ContentActivity.Companion.tf)
            tv_days.setTypeface(ContentActivity.Companion.tf)
            img_reminder.setVisibility(View.GONE)
            img_calender.setVisibility(View.GONE)
            img_call.setImageResource(R.drawable.call_big)
            img_location.setImageResource(R.drawable.location_big)
            tv_days.setVisibility(View.GONE)
        }
        img_location.setVisibility(View.VISIBLE)
        img_call.setImageResource(R.drawable.call_big)
        img_location.setImageResource(R.drawable.location_big)
        img_call.setVisibility(View.VISIBLE)
        linear_bottom.setVisibility(View.VISIBLE)
        relative_desc.setBackgroundResource(R.drawable.details_bg_big)
        img_share1.setVisibility(View.VISIBLE)
        linear_date.setVisibility(View.GONE)
    }

    override fun onStart() {
        super.onStart()
        //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.mtv_topTitle.setText(
            act.getString(R.string.NewsDetails).replace("*", "")
        )
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        GetNewsById()
    }

    @SuppressLint("MissingPermission")
    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.img_reminder -> {
            }
            R.id.img_event -> {

                /*if(getProjectsForDonation.getContentPhotos().size()>0){

                    productImages = new String[getProjectsForDonation.getContentPhotos().size()];

                    for(int i = 0; i< getProjectsForDonation.getContentPhotos().size(); i++){

                        GetContents.ContentPhotos photo = getProjectsForDonation.getContentPhotos().get(i);

                        productImages[i] = photo.getPhotos();

                    }

                }
                else{*/
                if (getNews.Photo != null && getNews.Photo != "") {
                    productImages = arrayOfNulls<String?>(1)
                    productImages[0] = getNews.Photo

                    //}
                    val b = Bundle()
                    b.putInt("position", 0)
                    b.putStringArray("productImages", productImages)
                    ContentActivity.Companion.openGalleryFragmentFadeIn(b)
                }
            }
            R.id.img_fav -> {
            }
            R.id.img_share -> shareContent()
            R.id.img_share1 -> {
                /*if (GlobalFunctions.isReadStorageAllowed(act)) shareContent()
                else*/
                permissionStr = Manifest.permission.READ_EXTERNAL_STORAGE
                GlobalFunctions.requestStoragePermission(act)
            }
            R.id.img_report -> {
            }
            R.id.img_facebook -> if (getNews.Facebook?.isNotEmpty()!!) {
                if (getNews.Facebook!!.contains("http") || getNews.Facebook!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getNews.Facebook)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${getNews.Facebook}")
                        )
                    )
            }
            R.id.img_instagram -> if (getNews.Instagram!!.isNotEmpty()) {
                if (getNews.Instagram!!.contains("http") || getNews.Instagram!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getNews.Instagram)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${getNews.Instagram}")
                        )
                    )
            }
            R.id.img_twitter -> if (getNews.Twitter!!.length > 0) {
                if (getNews.Twitter!!.contains("http") || getNews.Twitter!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getNews.Twitter)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${getNews.Twitter}")
                        )
                    )
            }
            R.id.img_youtube -> if (getNews.YouTube!!.length > 0) {
                if (getNews.YouTube!!.contains("http") || getNews.YouTube!!.contains("https"))
                    act.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getNews.YouTube)))
                else
                    act.startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://${getNews.YouTube}")
                        )
                    )
            }
            R.id.img_location -> {
                if (getNews.Latitude!!.isNotEmpty() && getNews.Longitude!!.isNotEmpty()) {
                    /* if (GlobalFunctions.isGPSAllowed(act)) {
                         val gson = Gson()
                         val bundle = Bundle()
                         val contentsArrayList = ArrayList<GetNews?>()
                         contentsArrayList.add(getNews)
                         bundle.putString("GetNews", gson.toJson(contentsArrayList))
                         Log.d("img_location", "4")
                         ContentActivity.Companion.openShowMapFragment(bundle)
                         return
                     }*/
                    permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                    GlobalFunctions.requestGPSPermission(act)
                } else
                    showToast(act, getString(R.string.no_location_found))
            }
            R.id.img_call -> {
                if (getNews.Phone!!.isNotEmpty()) {
                    /* if (GlobalFunctions.isReadCallAllowed(act)) {
                         val intent = Intent(Intent.ACTION_CALL)
                         intent.data = Uri.parse("tel:" + getNews.Phone)
                         act.startActivity(intent)
                         return
                     }*/
                    permissionStr = Manifest.permission.CALL_PHONE
                    GlobalFunctions.requestCallPermission(act)
                } else
                    showToast(act, getString(R.string.no_number_found))
            }
            R.id.img_calender -> {
            }
        }
    }

    fun GetNewsById() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetNews(getNews.Id, "false", "0")?.enqueue(
            object : Callback<ArrayList<GetNews>?> {

                override fun onFailure(call: Call<ArrayList<GetNews>?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(
                    call: Call<ArrayList<GetNews>?>,
                    response: Response<ArrayList<GetNews>?>
                ) {
                    if (response.body() != null) {
                        val getContentses = response.body()
                        if (mloading != null) {
                            if (getContentses != null) {
                                Log.d("shippingCompanies size", "" + getContentses.size)
                                if (getContentses.size > 0) {
                                    getNews = getContentses[0]!!

                                    Log.d("getContentsArray size", "" + getContentses.size)
                                    img_expire.setVisibility(View.GONE)

//                            img_event.getLayoutParams().width = ((BitmapDrawable) act.getResources().getDrawable(
//                                    R.drawable.no_img_details)).getBitmap().getWidth();
                                    img_event.getLayoutParams().height =
                                        (act.getResources().getDrawable(
                                            R.drawable.no_img_details
                                        ) as BitmapDrawable).bitmap.height
                                    if (getNews.Photo!!.length > 0) {
                                        Picasso.with(act).load(getNews.Photo)
                                            .placeholder(R.drawable.no_img_details)
                                            .error(R.drawable.no_img_details).into(img_event)
                                        val ob = GetAndSaveBitmapForArticle()
                                        ob.execute(getNews.Photo)
                                    }
                                    relative_gallery.setVisibility(View.INVISIBLE)
                                    //                            if(getProjectsForDonation.getContentPhotos().size()>=2){
//                                relative_gallery.setVisibility(View.VISIBLE);
//                                img_count.setText(getProjectsForDonation.getContentPhotos().size()+"");
//                            }

                                    //tv_days.setText(getProjectsForDonation.getFullName());
                                    tv_views.setText(
                                        """${act.getString(R.string.PublishedOn)} ${getNews.AddedOn}
${getNews.ViewersCount} ${act.getString(R.string.Views)}"""
                                    )
                                    tv_views.setTextSize(10.toFloat())
                                    if (languageSeassionManager.getLang()
                                            .equals("en", ignoreCase = true)
                                    ) {
                                        setContent(
                                            "", """
     ${getNews.TitleEN}

     ${getNews.DetailsEN}
     """.trimIndent()
                                        )
                                    } else {
                                        setContent(
                                            "", """
     ${getNews.TitleAR}

     ${getNews.DetailsAR}
     """.trimIndent()
                                        )
                                    }
                                    if (getNews.Phone!!.length > 0) {
                                    } else {
                                        img_call.setAlpha(50)
                                    }
                                    if (getNews.Latitude!!.length > 0 && getNews.Longitude!!.length > 0) {
                                    } else {
                                        img_location.setAlpha(50)
                                    }
                                    if (getNews.Facebook!!.length > 0) {
                                    } else {
                                        img_facebook.setAlpha(50)
                                    }
                                    if (getNews.Twitter!!.length > 0) {
                                    } else {
                                        img_twitter.setAlpha(50)
                                    }
                                    if (getNews.Instagram!!.length > 0) {
                                    } else {
                                        img_instagram.setAlpha(50)
                                    }
                                    if (getNews.YouTube!!.length > 0) {
                                    } else {
                                        img_youtube.setAlpha(50)
                                    }
                                }
                            }
                        }
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                }
            })
    }

    internal inner class GetAndSaveBitmapForArticle : AsyncTask<String?, Void?, Uri?>() {
        override fun doInBackground(vararg params: String?): Uri? {
            //val file_path = Environment.getExternalStorageDirectory().absolutePath + "/Monasabatena"
            val file_path = Environment.getExternalStorageDirectory().absolutePath
            val dir = File(file_path, "/Monasabatena")
            if (!dir.exists()) dir.mkdirs()
            file = File(dir, UUID.randomUUID().toString() + ".png")
            val fOut: FileOutputStream
            try {
                val bm = Picasso.with(act).load(params[0]).get()
                fOut = FileOutputStream(file)
                bm.compress(Bitmap.CompressFormat.PNG, 75, fOut)
                fOut.flush()
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            pShareUri = FileProvider.getUriForFile(
                act,
                act.applicationContext.packageName + ".fileprovider", file!!
            )
            shareContext = act
            imgUri = pShareUri
            return pShareUri
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                STORAGE_PERMISSION_CODE -> shareContent()
                CALL_PERMISSION_CODE -> {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse("tel:" + getNews.Phone)
                    act.startActivity(intent!!)
                }
                LOCATION_PERMISSION_CODE -> {
                    val gson = Gson()
                    val bundle = Bundle()
                    val contentsArrayList = ArrayList<GetNews?>()
                    contentsArrayList.add(getNews)
                    bundle.putString("GetNews", gson.toJson(contentsArrayList))
                    Log.d("img_location", "4")
                    ContentActivity.Companion.openShowMapFragment(bundle)
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act, permissionStr))
                mainLayout.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun shareContent() {

//        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
//               getProjectsForDonation.getCompanyCategoryNameEn()+  " @ AlQenaat " + " \n " + getProjectsForDonation.getDetailsEN() + "\n\n" + getProjectsForDonation.getPhotos() + "\n\n" + act.getString(R.string.UrlLink)
//                : getProjectsForDonation.getCompanyCategoryNameAr() +  " @ AlQenaat" + " \n " + getProjectsForDonation.getDetailsAR() + "\n\n" + getProjectsForDonation.getPhotos() + "\n\n" + act.getString(R.string.UrlLink);
        val shareProductInfo =
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) """
     ${getNews.TitleEN}

     ${getNews.DetailsEN}

     ${act.getString(R.string.UrlLink1)}
     """.trimIndent() else """
     ${getNews.TitleAR}

     ${getNews.DetailsAR}

     ${act.getString(R.string.UrlLink1)}
     """.trimIndent()

/*        String contentType="";
        if(getProjectsForDonation.getContentType().equalsIgnoreCase("1")){
            contentType = act.getString(R.string.NewsLabel);
        }
        else if(getProjectsForDonation.getContentType().equalsIgnoreCase("2")){
            if(getProjectsForDonation.getIsActivity().equalsIgnoreCase("1")){
                contentType = act.getString(R.string.SpecialOccasionLabel);
            }
            else{
                contentType = act.getString(R.string.OccasionLabel);
            }

        }
        final String shareProductInfo = languageSeassionManager.getLang().equalsIgnoreCase("en") ?
               act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getProjectsForDonation.getContentLink() + "\n\n" + act.getString(R.string.UrlLink)
                : act.getString(R.string.CheckLabel)+  " " + contentType + " at LobQ8 app \n\n " + getProjectsForDonation.getContentLink() + "\n\n" + act.getString(R.string.UrlLink);
**/
        //val ob = GetAndSaveBitmapForArticle()
        val clipboard = act.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("AlQenaat", shareProductInfo)
        clipboard.setPrimaryClip(clip)
        //ob.execute(getNews.Photo)
        val dialog = Dialog(act)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.share_dialog)
        dialog.window?.attributes?.windowAnimations = R.style.DialogAnimation
        val mimg_close: ImageView?
        val mimg_facebook: ImageView?
        val mimg_twitter: ImageView?
        val mimg_instagram: ImageView?
        val mimg_email: ImageView?
        val mimg_sms: ImageView?
        val mimg_whats: ImageView?
        mimg_facebook = dialog.findViewById<View?>(R.id.img_facebook) as ImageView
        val title = dialog.findViewById<View?>(R.id.shartext) as TextView
        title.setTypeface(ContentActivity.Companion.tf)
        mimg_twitter = dialog.findViewById<View?>(R.id.img_twitter) as ImageView
        mimg_instagram = dialog.findViewById<View?>(R.id.img_instagram) as ImageView
        mimg_email = dialog.findViewById<View?>(R.id.img_email) as ImageView
        mimg_sms = dialog.findViewById<View?>(R.id.img_sms) as ImageView
        mimg_whats = dialog.findViewById<View?>(R.id.img_whats) as ImageView
        mimg_close = dialog.findViewById<View?>(R.id.img_searchClose) as ImageView
        mimg_close.setOnClickListener(View.OnClickListener {
            file?.delete()
            dialog.dismiss()
        })
        mimg_facebook.setOnClickListener(View.OnClickListener { /*Share(1,
                                "com.facebook_icon.katana",
                                products.getDecription() + "\n" +
                                        products.getPhotos());*/
            dialog.dismiss()
            /* if (mSessionManager.isFacebookOpenBefore()) {
                 shareData(1,
                         "facebook",
                         shareProductInfo)
             } else {*/
            AlertDialog.Builder(act)
                .setTitle(act.getString(R.string.AttentionLabel))
                .setMessage(act.getString(R.string.AttentionText))
                .setPositiveButton(android.R.string.yes) { dialog, which ->
                    shareData(1, "facebook", shareProductInfo)
                    mSessionManager.FacebookOpened()
                }
                .setIcon(R.drawable.icon_512)
                .show()
            //}
        })
        mimg_twitter.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(5, "twitter", shareProductInfo)
        })
        mimg_instagram.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            AlertDialog.Builder(act)
                .setIcon(R.drawable.icon_512)
                .setTitle(act.getString(R.string.AttentionLabel))
                .setMessage(act.getString(R.string.AttentionText))
                .setPositiveButton(android.R.string.yes) { dialog, which ->
                    shareData(6, "instagram", shareProductInfo)
                }
                .setIcon(R.drawable.icon_512)
                .show()
        })
        mimg_email.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(3, "gmail", shareProductInfo)
        })
        mimg_sms.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(4, "sms", shareProductInfo)
        })
        mimg_whats.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
            shareData(2, "whatsapp", shareProductInfo)
        })
        dialog.show()
    }

    fun setContent(title: String, content: String?) {
        val sp: Spannable = SpannableString(title)
        Linkify.addLinks(sp, Linkify.ALL)
        val sp1: Spannable = SpannableString(content)
        Linkify.addLinks(sp1, Linkify.ALL)
        var finalHTML = ""
        finalHTML = if (title.length > 0) {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center><center>" + Html.toHtml(
                sp
            )
                    + "</center></b></span style='color: #4c4c4c;'><br /><br /><center>" + sp1 + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        } else {
            ("<html><head><style type='text/css'>@font-face {font-family: 'GEDinarOne-Medium';src: url('fonts/GEDinarOne-Medium3.ttf');}" +
                    " body {background-color: " +
                    "transparent;border: 0px;margin: 0px;padding: 0px;font-family: 'GEDinarOne-Medium'; font-size: 15px;text-align: right;width: 100%;" +
                    "text-align: justify;line-height: 150%;}</style></head><body dir='RTL'><br /><span style='color: #4c4c4c;font-size: 16px;'><font color=\"#4c4c4c\"><center>" + Html.toHtml(
                sp
            )
                    + "</center></span style='color: #4c4c4c;'><center>" + sp1 + "</center></font><br /><br /><style type='text/css'> body {width:100%;} iframe { width: 100%;} img {width: 100%;} table {width: 100%;} td {word-wrap: break-word;}</style></body></html>")
        }
        webView.loadDataWithBaseURL("file:///android_asset/", finalHTML, "text/html", "UTF-8", null)
        webView.setBackgroundColor(0)
        webView.setWebViewClient(WebViewClient())
        webView.getSettings().javaScriptEnabled = true
        webView.getSettings().javaScriptCanOpenWindowsAutomatically = true
        webView.getSettings().pluginState = WebSettings.PluginState.ON
        webView.setWebChromeClient(WebChromeClient())
        webView.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, urlNewString: String?): Boolean {
                if (!loadingFinished) {
                    redirect = true
                }
                loadingFinished = false
                //webView.loadUrl(urlNewString);
                // Here the String url hold 'Clicked URL'
                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlNewString)));
                if (urlNewString?.startsWith("tel:")!!) {
                    val intent = Intent(
                        Intent.ACTION_DIAL,
                        Uri.parse(urlNewString)
                    )
                    startActivity(intent)
                } else if (urlNewString.startsWith("http:") || urlNewString.startsWith("https:")) {
                    view?.loadUrl(urlNewString)
                }
                return true
            }

            override fun onPageStarted(view: WebView?, url: String?, facIcon: Bitmap?) {
                loadingFinished = false
                //SHOW LOADING IF IT ISNT ALREADY VISIBLE
                mloading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                if (!redirect) {
                    loadingFinished = true
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
                if (loadingFinished && !redirect) {
                    //HIDE LOADING IT HAS FINISHED
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                } else {
                    redirect = false
                }
            }
        })
    }

    companion object {
        protected val TAG = NewsDetailsFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: NewsDetailsFragment
        lateinit var mloading: ProgressBar

        fun newInstance(act: FragmentActivity): NewsDetailsFragment {
            fragment = NewsDetailsFragment()
            Companion.act = act
            return fragment
        }
    }
}