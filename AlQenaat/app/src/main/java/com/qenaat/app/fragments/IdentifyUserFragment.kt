package com.qenaat.app.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image

import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.*
import com.qenaat.app.classes.FixControl.createPartFromFile
import com.qenaat.app.classes.FixControl.createPartFromString
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.model.UploadFile
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.reflect.Type
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

/**
 * Created by DELL on 03-Jan-18.
 */
class IdentifyUserFragment : Fragment(), View.OnClickListener {

    lateinit var act: FragmentActivity
    lateinit var mLangSessionManager: LanguageSessionManager
    lateinit var mainLayout: RelativeLayout
    lateinit var mloading: ProgressBar
    lateinit var relative_image1: RelativeLayout
    lateinit var relative_image2: RelativeLayout
    lateinit var relative_image3: RelativeLayout
    lateinit var relative_image4: RelativeLayout
    lateinit var relative_image5: RelativeLayout
    lateinit var img_select1: ImageView
    lateinit var img_select2: ImageView
    lateinit var img_select3: ImageView
    lateinit var img_select4: ImageView
    lateinit var img_select5: ImageView
    lateinit var img_delete1: ImageView
    lateinit var img_delete2: ImageView
    lateinit var img_delete3: ImageView
    lateinit var img_delete4: ImageView
    lateinit var img_delete5: ImageView
    lateinit var tv_add_images: TextView
    private lateinit var arrayListPhoto: ArrayList<String?>
    var imageCount = 0
    lateinit var tv_add_ad: TextView
    private var index = 0
    private var current_path: String = ""
    private var images: StringBuilder = StringBuilder()
    var isEdit = false

    //GetProducts items;
    private lateinit var arrayListPhotoEdit: ArrayList<String?>
    lateinit var horizontalScrollView: HorizontalScrollView
    lateinit var scroll_view: ScrollView
    lateinit var tv_tree_name: TextView
    var config: Config = Config()
    private var imagesCameraGallery: ArrayList<Image?> = ArrayList()

    //val multipartTypedOutput = MultipartTypedOutput()
    private var imagePartList: MutableList<MultipartBody.Part>? = mutableListOf()
    private var textPart: RequestBody? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        try {
            Log.d("onCreate", "Bundle")
            mLangSessionManager = LanguageSessionManager(act)
            mSessionManager = SessionManager(act)
            if (arguments != null) {
                if (arguments!!.getString("isEdit").equals("0", ignoreCase = true)) {
                    isEdit = false
                } else if (arguments!!.getString("isEdit").equals("1", ignoreCase = true)) {
                    isEdit = true
                    //                    Gson gson = new Gson();
//                    items = gson.fromJson(getArguments().getString("GetProducts"),
//                            GetProducts.class);
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return try {
            mainLayout = inflater.inflate(R.layout.identifiy_user, null) as RelativeLayout
            mainLayout
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        try {
            arrayListPhoto = ArrayList()
            arrayListPhotoEdit = ArrayList()
            tv_tree_name = mainLayout.findViewById(R.id.tv_tree_name) as TextView
            tv_tree_name.setOnClickListener(this)
            tv_add_ad = mainLayout.findViewById(R.id.tv_add_ad) as TextView
            tv_add_images = mainLayout.findViewById(R.id.tv_add_images) as TextView
            horizontalScrollView = mainLayout.findViewById(R.id.horizontalScrollView) as HorizontalScrollView
            scroll_view = mainLayout.findViewById(R.id.scroll_view) as ScrollView
            img_delete1 = mainLayout.findViewById(R.id.img_delete1) as ImageView
            img_delete2 = mainLayout.findViewById(R.id.img_delete2) as ImageView
            img_delete3 = mainLayout.findViewById(R.id.img_delete3) as ImageView
            img_delete4 = mainLayout.findViewById(R.id.img_delete4) as ImageView
            img_delete5 = mainLayout.findViewById(R.id.img_delete5) as ImageView
            img_select1 = mainLayout.findViewById(R.id.img_select1) as ImageView
            img_select2 = mainLayout.findViewById(R.id.img_select2) as ImageView
            img_select3 = mainLayout.findViewById(R.id.img_select3) as ImageView
            img_select4 = mainLayout.findViewById(R.id.img_select4) as ImageView
            img_select5 = mainLayout.findViewById(R.id.img_select5) as ImageView
            relative_image1 = mainLayout.findViewById(R.id.relative_image1) as RelativeLayout
            relative_image2 = mainLayout.findViewById(R.id.relative_image2) as RelativeLayout
            relative_image3 = mainLayout.findViewById(R.id.relative_image3) as RelativeLayout
            relative_image4 = mainLayout.findViewById(R.id.relative_image4) as RelativeLayout
            relative_image5 = mainLayout.findViewById(R.id.relative_image5) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            tv_add_ad.setOnClickListener(this)
            tv_add_images.setOnClickListener(this)
            img_delete1.setOnClickListener(this)
            img_select1.setOnClickListener(this)
            Log.d("img_select1", img_select1.toString() + "")
            Log.d("img_delete1", img_delete1.toString() + "")
            relative_image1.setOnClickListener(this)
            img_delete2.setOnClickListener(this)
            img_delete3.setOnClickListener(this)
            img_delete4.setOnClickListener(this)
            img_delete5.setOnClickListener(this)
            val imgW: Int = (act.getResources().getDrawable(
                    R.drawable.add_documents) as BitmapDrawable).getBitmap().getWidth()
            val imgH: Int = (act.getResources().getDrawable(
                    R.drawable.add_documents) as BitmapDrawable).getBitmap().getHeight()
            img_select1.getLayoutParams().height = imgH
            img_select1.getLayoutParams().width = imgW
            img_select2.getLayoutParams().height = imgH
            img_select2.getLayoutParams().width = imgW
            img_select3.getLayoutParams().height = imgH
            img_select3.getLayoutParams().width = imgW
            img_select4.getLayoutParams().height = imgH
            img_select4.getLayoutParams().width = imgW
            img_select5.getLayoutParams().height = imgH
            img_select5.getLayoutParams().width = imgW
            ContentActivity.Companion.setTextFonts(mainLayout)
            //setImagePickerConfig()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("onStart", "onStart")
        //    ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle.setVisibility(View.VISIBLE)
        ContentActivity.Companion.enableLogin(mLangSessionManager)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.UserVerification))
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        if (isEdit) {
        } else {
            if (ContentActivity.Companion.parentId.length > 0) {
                if (ContentActivity.Companion.parentNameEn.length > 0) {
                    tv_tree_name.setText(ContentActivity.Companion.parentNameEn)
                }
                if (ContentActivity.Companion.parentNameAr.length > 0) {
                    tv_tree_name.setText(ContentActivity.Companion.parentNameAr)
                }
            }
            val gson: Gson?
            if (mSessionManager.getImagePath() !== "" && mSessionManager.getImagePath() != null) {
                gson = Gson()
                val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.getType()
                arrayListPhoto = gson.fromJson(mSessionManager.getImagePath(), listType)
                setImages(arrayListPhoto.size)
            }
        }
    }

    override fun onClick(view: View?) {
        val b = Bundle()
        Log.d("onClick", "onClick==" + view?.getId())
        val gson = Gson()
        when (view?.getId()) {
            R.id.tv_add_ad -> if (arrayListPhoto.size > 0 || arrayListPhotoEdit.size > 0) {
                var isError = false
                val isPhoneError = false
                isError = if (tv_tree_name.getText().toString().length > 0) {
                    false
                } else {
                    true
                }
                if (isError) {
                    Toast.makeText(act, act.getString(R.string.FillAllFields), Toast.LENGTH_LONG).show()
                } else {
                    if (isPhoneError) {
                        Toast.makeText(act, act.getString(R.string.MobileError), Toast.LENGTH_LONG).show()
                    } else {
                        index = 0
                        mloading.setVisibility(View.VISIBLE)
                        GlobalFunctions.DisableLayout(mainLayout)
                        Log.d("et_title", "" + tv_tree_name.getText().toString())
                        Log.d("arrayListPhoto size", "" + arrayListPhoto.size)
                        Log.d("current_path", "" + current_path)
                        if (arrayListPhoto.size > 0) {
                            current_path = if (isEdit) {
                                arrayListPhotoEdit.get(index)!!
                            } else {
                                arrayListPhoto.get(index)!!
                            }
                            uploadPhotos()
                            /*val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                            uploadImagesAysn.execute()*/
                        } else {
                            IdentifyUser()
                        }
                    }
                }
            } else {
                Toast.makeText(act, act.getString(R.string.NoDocumentSelectedLabel), Toast.LENGTH_LONG).show()
            }
            R.id.img_select1 -> Log.d("test", "clicked img_select1")
            R.id.img_delete1 -> {
                Log.d("test", "clicked img_delete1")
                if (isEdit) {
                    arrayListPhotoEdit.remove(img_select1.getTag().toString())
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                    Log.e("arrayListPhoto after", "" + arrayListPhotoEdit.size)
                    setEditImages(arrayListPhotoEdit.size)
                } else {
                    arrayListPhoto.remove(img_select1.getTag().toString())
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                    Log.e("arrayListPhoto after", "" + arrayListPhoto.size)
                    setImages(arrayListPhoto.size)
                }
            }
            R.id.img_delete2 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select2.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select2.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.img_delete3 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select3.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select3.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.img_delete4 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select4.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select4.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.img_delete5 -> if (isEdit) {
                arrayListPhotoEdit.remove(img_select5.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit.size)
            } else {
                arrayListPhoto.remove(img_select5.getTag().toString())
                mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto.size)
            }
            R.id.tv_add_images -> {

                //First checking if the app is already having the permission
                if (GlobalFunctions.isReadStorageAllowed(act)) {
                    if (GlobalFunctions.isCameraAllowed(act)) {
                        saveData()
                        if (mLangSessionManager.getLang() == "ar") {
                            Toast.makeText(act, act.getString(R.string.SlideLeft), Toast.LENGTH_LONG).show()
                        }
                        if (isEdit) {
                            if (arrayListPhotoEdit.size < 5) {
                                ImagePicker.with(this)
                                        .setFolderMode(false)
                                        .setCameraOnly(false)
                                        .setFolderTitle(act.getString(R.string.GalleryLabel))
                                        .setMultipleMode(true)
                                        //.setSelectedImages(config.getSelectedImages())
                                        .setMaxSize(5 - arrayListPhotoEdit.size)
                                        .start()
                            }
                        } else {
                            if (arrayListPhoto.size < 5) {
                                ImagePicker.with(this)
                                        .setFolderMode(false)
                                        .setCameraOnly(false)
                                        .setFolderTitle(act.getString(R.string.GalleryLabel))
                                        .setMultipleMode(true)
                                        //.setSelectedImages(config.getSelectedImages())
                                        .setMaxSize(5 - arrayListPhoto.size)
                                        .start()
                            }
                        }
                        return
                    }
                }

                //If the app has not the permission then asking for the permission
                if (GlobalFunctions.isCameraAllowed(act)) {
                    GlobalFunctions.requestStoragePermission(act)
                } else {
                    GlobalFunctions.requestCameraePermission(act)
                }
            }
            R.id.linear_location -> {
                saveData()
                if (GlobalFunctions.isGPSAllowed(act)) {
                    ContentActivity.Companion.openCurrentLocationFragmentFadeIn()
                    return
                }
                GlobalFunctions.requestGPSPermission(act)
            }
            R.id.tv_date -> {
            }
            R.id.tv_tree_name -> {
                saveData()
                val bundle = Bundle()
                bundle.putString("comingFrom", "add")
                ContentActivity.Companion.openHomeFragment(bundle)
            }
        }
    }

    private fun uploadPhotos() {
        //multipartTypedOutput.addPart("FileObjectType", TypedString("userclaim"))
        textPart = "userclaim".createPartFromString()
        if (isEdit) {
            for (i in arrayListPhotoEdit.indices) {
                current_path = arrayListPhotoEdit[i]!!
                Log.e("$i current_path", current_path)
                SaveAndGetImageTask(i).execute(BitmapFactory.decodeFile(current_path))
            }
        } else {
            for (i in arrayListPhoto.indices) {
                current_path = arrayListPhoto[i]!!
                Log.e("$i current_path", current_path)
                SaveAndGetImageTask(i).execute(BitmapFactory.decodeFile(current_path))
            }
        }
    }

    private fun uploadPhotosAPI() {
        QenaatAPICall.getCallingAPIInterface()?.uploadImage(textPart, imagePartList)?.enqueue(
                object : Callback<ArrayList<UploadFile?>?> {

                    override fun onFailure(call: Call<ArrayList<UploadFile?>?>, t: Throwable) {
                        t.printStackTrace()
                        GlobalFunctions.EnableLayout(mainLayout)
                        mainLayout.showSnakeBar(t?.message!!)
                    }

                    override fun onResponse(call: Call<ArrayList<UploadFile?>?>, response: Response<ArrayList<UploadFile?>?>) {
                        GlobalFunctions.EnableLayout(mainLayout)
                        if (response.body() != null) {
                            val uploadFileList = response.body()

                            var isSuccess = false
                            for (i in uploadFileList?.indices!!) {
                                if (uploadFileList[i]?.Type == 1) {
                                    isSuccess = true
                                    Log.e("imageUrl", uploadFileList[i]?.FileUrl)
                                } else {
                                    isSuccess = false
                                    Log.e("imageUrlError", uploadFileList[i]?.error)
                                    mainLayout.showSnakeBar(uploadFileList[i]?.error!!)
                                    break
                                }
                            }
                            Log.e("imageUrl", images.toString())
                            Log.e("photo upload", isSuccess.toString())
                            mloading.setVisibility(View.GONE)
                            if (isSuccess) IdentifyUser()
                        }
                    }
                })
    }

    //Save image in hidden folder and get it...
    internal inner class SaveAndGetImageTask(private val num: Int) : AsyncTask<Bitmap?, Void?, File?>() {
        protected override fun doInBackground(vararg params: Bitmap?): File? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/.Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            val currentTime = "img_" + Calendar.getInstance().timeInMillis + ".png"
            images?.append(currentTime)
            if (images?.isNotEmpty()!!)
                images!!.append(",")
            val file = File(dir, currentTime)
            val fOut: FileOutputStream
            fOut = FileOutputStream(file)
            params[0]?.compress(Bitmap.CompressFormat.PNG, 60, fOut)
            fOut.flush()
            fOut.close()
            return file
        }

        override fun onPostExecute(file: File?) {
            super.onPostExecute(file)

            //multipartTypedOutput.addPart("File$num", TypedFile("image/*", file))
            imagePartList?.add(createPartFromFile("File$num", file!!)!!)
            //if (isEdit) {
            if (num == arrayListPhoto.size - 1)
                uploadPhotosAPI()
            //}
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("onActivityResult", "onActivityResult")
        if (requestCode == ContentActivity.Companion.FRAGMENT_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
            }
        } else if (requestCode == Config.RC_PICK_IMAGES && resultCode == Activity.RESULT_OK && data != null) {
            imagesCameraGallery = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES)
            if (imagesCameraGallery != null) {
                for (uri in imagesCameraGallery) {

                    //mMedia.add(uri);
                    Log.d("uri", "" + uri?.path)
                    if (isEdit) {
                        arrayListPhotoEdit.add(uri?.path)
                    } else {
                        arrayListPhoto.add(uri?.path)
                    }
                }
                imageCount = if (isEdit) {
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                    setEditImages(arrayListPhotoEdit.size)
                    arrayListPhotoEdit.size
                } else {
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                    setImages(arrayListPhoto.size)
                    arrayListPhoto.size
                }
            }

            //do something
        }
        /*else if (requestCode == 3 && data!=null) {

            Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

            if (parcelableUris == null) {
                return;
            }

            // Java doesn't allow array casting, this is a little hack
            Uri[] uris = new Uri[parcelableUris.length];
            Log.e("uris size ",""+uris.length);
            //mimg_deletePhoto1.setText(""+uris.length);
            System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

            if (uris != null) {
                for (Uri uri : uris) {
                    Log.e(TAG, " uri: " + uri.toString());
                    //mMedia.add(uri);
                    Log.d("uri", ""+uri.getPath());
                    if(isEdit){
                        arrayListPhotoEdit.add(uri.getPath());
                    }
                    else{
                        arrayListPhoto.add(uri.getPath());
                    }

                }
                if(isEdit){
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit));
                    setEditImages(arrayListPhotoEdit.size());
                    imageCount = arrayListPhotoEdit.size();
                }
                else{
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto));
                    setImages(arrayListPhoto.size());
                    imageCount = arrayListPhoto.size();
                }

            }
        }*/
    }

    private fun setImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
                relative_image1.setVisibility(View.GONE)
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            1 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select1)
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            2 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select2)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            3 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select2)
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhoto.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select3)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            4 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select2)
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhoto.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select3)
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhoto.get(3))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(3))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select4)
                relative_image5.setVisibility(View.GONE)
            }
            5 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhoto.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select1)
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhoto.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select2)
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhoto.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select3)
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhoto.get(3))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(3))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select4)
                relative_image5.setVisibility(View.VISIBLE)
                img_select5.setTag(arrayListPhoto.get(4))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(4))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_documents)
                        .placeholder(R.drawable.add_documents)
                        .into(img_select5)
            }
        }
    }

    /*private fun uploadPhoto(fileName: String?, FileObjectType: String?) {
        QenaatAPICall.getCallingAPIInterface()?.uploadFile(fileName, FileObjectType, object : Callback<String?> {
            override fun success(s: String?, response: Response?) {
                Log.e("Upload", "success")
            }

            override fun failure(error: RetrofitError?) {
                Log.e("Upload", "error")
                //goUploadNextVideo();
            }
        })
    }*/

    internal inner class UploadImagesAysn : AsyncTask<String?, String?, String?>() {
        protected override fun onPostExecute(paramString: String?) {
            Log.d("image updates", "" + index + " image uploaded -> " + index + 1 + " image started")
            index = index + 1
            if (isEdit) {
                if (index < arrayListPhotoEdit.size) {
                    current_path = arrayListPhotoEdit.get(index)!!
                    val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    IdentifyUser()
                }
            } else {
                if (index < arrayListPhoto.size) {
                    current_path = arrayListPhoto.get(index)!!
                    val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    IdentifyUser()
                }
            }
        }

        protected override fun doInBackground(vararg params: String?): String? {

            /*String PhotoName = GlobalFunctions.SendMultipartFile(
                    current_path, getString(R.string.UploadContentPhoto), ".jpg");*/
            if (!current_path.contains("http://otabi.")) {
                var file: File? = null
                val myBitmap: Bitmap? = null
                var filename = ""
                var angle = 0
                val ei: ExifInterface?
                try {
                    ei = ExifInterface(current_path)
                    val orientation: Int = ei.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, 1)
                    when (orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> angle = 90
                        ExifInterface.ORIENTATION_ROTATE_180 -> angle = 180
                        ExifInterface.ORIENTATION_ROTATE_270 -> angle = 270
                    }
                } catch (e1: IOException) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace()
                }
                if (isEdit) {
//                    filename = getNameOfImage(current_path
//                    );
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    Log.d("str1", "0-> " + images.toString())
                    val str1 = images.toString().replace(getNameOfImage(current_path)!!, filename)
                    Log.d("str1", "1-> " + getNameOfImage(current_path))
                    Log.d("str1", "2-> $filename")
                    images = StringBuilder()
                    images.append(str1)
                    Log.d("str1", "3-> " + images.toString())
                } else {
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    images.append(filename)
                    if (index != arrayListPhoto.size - 1) {
                        images.append(",")
                    }
                }
                file = File(current_path)
                /*myBitmap = decodeSampledBitmapFromPath(file.getAbsolutePath(),
                        1000, 1000);

                Matrix matrix = new Matrix();
                matrix.postRotate(angle);
                myBitmap = Bitmap
                        .createBitmap(myBitmap, 0, 0, myBitmap.getWidth(),
                                myBitmap.getHeight(), matrix, true);

                OutputStream os;
                try {
                    os = new FileOutputStream(current_path);
                    myBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
                var conn: HttpURLConnection? = null
                var dos: DataOutputStream? = null
                var inStream: DataInputStream? = null
                val lineEnd = "\r\n"
                val twoHyphens = "--"
                val boundary = "*****"
                var bytesRead: Int
                var bytesAvailable: Int
                var bufferSize: Int
                val buffer: ByteArray
                val maxBufferSize = 1 * 1024 * 1024
                try {
                    val fileInputStream = FileInputStream(Compressor(act).compressToFile(file))
                    //once check url correct or not
                    val url = URL(QenaatConstant.Photo_URL + "userclaim")
                    conn = url.openConnection() as HttpURLConnection
                    conn.doInput = true
                    conn.doOutput = true
                    conn.requestMethod = "POST"
                    conn.useCaches = false
                    conn.setRequestProperty("Connection", "Keep-Alive")
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=$boundary")
                    dos = DataOutputStream(conn.outputStream)
                    dos.writeBytes(twoHyphens + boundary + lineEnd)
                    dos.writeBytes("Content-Disposition: form-data; name=\"pic\";"
                            + " filename=\"" + filename + "\"" + lineEnd)
                    dos.writeBytes(lineEnd)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    buffer = ByteArray(bufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize)
                        bytesAvailable = fileInputStream.available()
                        bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    }
                    dos.writeBytes(lineEnd)
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)
                    fileInputStream.close()
                    dos.flush()
                    dos.close()
                } catch (ex: MalformedURLException) {
                    println("Error:$ex")
                } catch (ioe: IOException) {
                    println("Error:$ioe")
                }
                try {
                    inStream = DataInputStream(conn?.getInputStream())
                    var str: String?
                    while (inStream.readLine().also { str = it } != null) {
                        println(str)
                    }
                    inStream.close()
                } catch (ioex: IOException) {
                    println("Error: $ioex")
                }
            }
            return "1"
        }
    }

    private fun getNamesOfImages() {
        images = StringBuilder()
        if (isEdit) {
            for (i in arrayListPhotoEdit.indices) {
                val file = File(arrayListPhotoEdit.get(i))
                //Log.d("file name", ""+FilenameUtils.getBaseName(file.getName()));
                images.append(getNameOfImage(arrayListPhotoEdit.get(i)!!))
                //images.append(FilenameUtils.getBaseName(file.getName()));
                if (i != arrayListPhotoEdit.size - 1) {
                    images.append(",")
                }
                Log.e("images", images.toString())
            }
        } else {
            /*for (int i = 0; i < arrayListPhoto.size(); i++) {

                File file = new File(arrayListPhoto.get(i));
                //Log.d("file name", ""+FilenameUtils.getBaseName(file.getName()));

                images.append(getNameOfImage(arrayListPhoto.get(i)));
                //images.append(FilenameUtils.getBaseName(file.getName()));

                if (!(i == arrayListPhoto.size() - 1)) {
                    images.append(",");
                }

                Log.e("images", images.toString());
            }*/
        }
    }

    // method to get the name of the image from the path
    fun getNameOfImage(path: String): String? {
        val index = path.lastIndexOf('/')
        return path.substring(index + 1)
    }

    fun calculateInSampleSize(options: BitmapFactory.Options,
                              reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height: Int = options.outHeight
        val width: Int = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            inSampleSize = if (width > height) {
                Math.round(height as Float / reqHeight as Float)
            } else {
                Math.round(width as Float / reqWidth as Float)
            }
        }
        return inSampleSize
    }

    private fun IdentifyUser() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.IdentifyUser(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                ContentActivity.Companion.parentId,
                GlobalFunctions.EncodeParameter(images.toString()),
                mSessionManager.getUserCode())?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (Integer.valueOf(outResponse) > 0) {
                                    Snackbar.make(mainLayout, act.getString(R.string.HelpRequestSent), Snackbar.LENGTH_LONG).show()
                                    fragmentManager?.popBackStack()
                                } else if (outResponse == "-1") {
                                    Toast.makeText(act, act.getString(R.string.OperationFailed), Toast.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Toast.makeText(act, act.getString(R.string.DataMissing), Toast.LENGTH_LONG).show()
                                }
                            }
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                        }
                    }
                })
    }

    private fun setEditImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
                relative_image1.setVisibility(View.GONE)
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            1 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.GONE)
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            2 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(0)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.GONE)
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            3 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhotoEdit.get(2))
                if (arrayListPhotoEdit.get(2)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select3)
                }
                relative_image4.setVisibility(View.GONE)
                relative_image5.setVisibility(View.GONE)
            }
            4 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhotoEdit.get(2))
                if (arrayListPhotoEdit.get(2)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select3)
                }
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhotoEdit.get(3))
                if (arrayListPhotoEdit.get(3)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select4)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select4)
                }
                relative_image5.setVisibility(View.GONE)
            }
            5 -> {
                relative_image1.setVisibility(View.VISIBLE)
                img_select1.setTag(arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select1)
                }
                relative_image2.setVisibility(View.VISIBLE)
                img_select2.setTag(arrayListPhotoEdit.get(1))
                if (arrayListPhotoEdit.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select2)
                }
                relative_image3.setVisibility(View.VISIBLE)
                img_select3.setTag(arrayListPhotoEdit.get(2))
                if (arrayListPhotoEdit.get(2)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select3)
                }
                relative_image4.setVisibility(View.VISIBLE)
                img_select4.setTag(arrayListPhotoEdit.get(3))
                if (arrayListPhotoEdit.get(3)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select4)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select4)
                }
                relative_image5.setVisibility(View.VISIBLE)
                img_select5.setTag(arrayListPhotoEdit.get(4))
                if (arrayListPhotoEdit.get(4)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(4))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select5)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(4))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_documents)
                            .placeholder(R.drawable.add_documents)
                            .into(img_select5)
                }
            }
        }
    }

    private fun saveData() {
        if (tv_tree_name.getText().length > 0) {
            ContentActivity.Companion.parentNameEn = tv_tree_name.getText().toString()
            ContentActivity.Companion.parentNameAr = tv_tree_name.getText().toString()
        }
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {


        //Checking the request code of our request
        if (requestCode == PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                //Toast.makeText(act,"Permission granted now you can read the storage",Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(act,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                //Toast.makeText(act,"Permission granted now you can read the storage",Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                //Toast.makeText(act,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

//    private fun setImagePickerConfig() {
//        config = Config()
//        config.setCameraOnly(false)
//        config.setMultipleMode(true)
//        config.setFolderMode(false)
//        config.setShowCamera(true)
//        config.setMaxSize(Config.MAX_SIZE)
//        config.setDoneTitle(getString(R.string.imagepicker_action_done))
//        config.setFolderTitle(getString(R.string.imagepicker_title_folder))
//        config.setImageTitle(getString(R.string.imagepicker_title_image))
//        config.setSavePath(SavePath.DEFAULT)
//        config.setSelectedImages(ArrayList())
//    }

    companion object {
        protected val TAG = IdentifyUserFragment::class.java.simpleName
        lateinit var fragment: IdentifyUserFragment
        lateinit var mSessionManager: SessionManager
        var bitmaps: ArrayList<Bitmap?> = ArrayList<Bitmap?>()
        private const val PERMISSION_CODE = 23
        private const val INTENT_REQUEST_GET_IMAGES = 13
        fun newInstance(act: FragmentActivity): IdentifyUserFragment {
            fragment = IdentifyUserFragment()
            fragment.act = act
            return fragment
        }
    }
}