package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.DrawerAdapter
import com.qenaat.app.model.DrawerItems
import kotlinx.android.synthetic.main.fragment_drawer.*
import java.util.ArrayList

class DrawerFragment : Fragment(), DrawerAdapter.OnDrawerItemClickListener {

    private var mDrawerLayout: DrawerLayout? = null
    private var adapter: DrawerAdapter? = null
    private var containerView: View? = null
    private var drawerListener: FragmentDrawerListener? =
        null
    private var drawerItemClickListener: DrawerAdapter.OnDrawerItemClickListener? = null

    var list: ArrayList<DrawerItems>? = null


    fun DrawerFragment() {}

    fun setDrawerListener(listener: FragmentDrawerListener?) {
        drawerListener = listener
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_drawer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        drawerItemClickListener = this


    }


    private fun setDrawerAdapter() {
        if (adapter == null) {
            adapter = DrawerAdapter(activity, drawerItemClickListener)
        }
        if (drawerList.getAdapter() == null) {
            drawerList.setAdapter(adapter)
            drawerList.setLayoutManager(LinearLayoutManager(activity))
            drawerList.setNestedScrollingEnabled(false)
            drawerList.setFocusable(false)
        }
        adapter!!.doRefresh(list)
    }

    override fun onResume() {
        super.onResume()
    }

    fun setUp(
        fragmentId: Int,
        drawerLayout: DrawerLayout,
        img: ImageView,
        boolean: Boolean

    ) {
        if (activity == null) return

        list = DrawerItems.initSellerDrawerItems(this!!.activity!! , boolean)
        setDrawerAdapter()
        containerView = activity!!.findViewById(fragmentId)
        mDrawerLayout = drawerLayout

        img.setOnClickListener(
            View.OnClickListener {
                val drawer = mDrawerLayout as DrawerLayout
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START)
                } else {
                    drawer.openDrawer(GravityCompat.START)
                }
            }
        )

    }

    fun updateDrawerList(itemList: ArrayList<DrawerItems?>) {
        list = itemList as ArrayList<DrawerItems>
        setDrawerAdapter()
        //adapter.notifyDataSetChanged();
    }

    override fun onDrawerItemClick(position: Int, drawerItems: DrawerItems?) {
        mDrawerLayout!!.closeDrawer(containerView!!)
        drawerListener!!.onDrawerItemSelected(position, drawerItems)
    }


    interface FragmentDrawerListener {
        fun onDrawerItemSelected(position: Int, drawerItems: DrawerItems?)
    }

}