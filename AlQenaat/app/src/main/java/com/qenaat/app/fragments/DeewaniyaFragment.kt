package com.qenaat.app.fragments

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.AlertAdapterFA
import com.qenaat.app.adapters.DeewaniyaAdapter
import com.qenaat.app.classes.FixControl
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.classesimport.SpacesItemDecoration
import com.qenaat.app.model.GetAreas
import com.qenaat.app.model.GetDewanias
import com.qenaat.app.model.GetWeekDays
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import me.drakeet.materialdialog.MaterialDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * Created by DELL on 29-Oct-17.
 */
class DeewaniyaFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var my_recycler_view: RecyclerView
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    var getDiwaniyasArrayList: ArrayList<GetDewanias?> = ArrayList()
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    lateinit var progress_loading_more: ProgressBar
    lateinit var tv_all_days: TextView
    lateinit var tv_all_days1: TextView
    lateinit var tv_all_days0: TextView
    var dayId: String = "0"
    var dayName: String = ""
    var areaId: String = "0"
    var areaName: String = ""
    var getAreasArrayList: ArrayList<GetAreas?> = ArrayList()
    var getWeekDaysArrayList: ArrayList<GetWeekDays?> = ArrayList()
    lateinit var webView: WebView
    var type: String = ""
    lateinit var img_bg0: ImageView
    lateinit var img_bg1: ImageView
    lateinit var img_bg: ImageView
    lateinit var relative_top0: RelativeLayout
    lateinit var relative_top1: RelativeLayout
    lateinit var relative_top: RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                type = arguments!!.getString("type")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.deewaniya, null) as RelativeLayout
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            img_bg0 = mainLayout.findViewById<View?>(R.id.img_bg0) as ImageView
            img_bg1 = mainLayout.findViewById<View?>(R.id.img_bg1) as ImageView
            webView = mainLayout.findViewById<View?>(R.id.webView) as WebView
            img_bg = mainLayout.findViewById<View?>(R.id.img_bg) as ImageView
            relative_top = mainLayout.findViewById<View?>(R.id.relative_top) as RelativeLayout
            relative_top1 = mainLayout.findViewById<View?>(R.id.relative_top1) as RelativeLayout
            relative_top0 = mainLayout.findViewById<View?>(R.id.relative_top0) as RelativeLayout
            my_recycler_view = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            progress_loading_more = mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
            tv_all_days = mainLayout.findViewById<View?>(R.id.tv_title) as TextView
            tv_all_days.setTextSize(12f)
            tv_all_days.setTypeface(ContentActivity.Companion.tf)
            tv_all_days1 = mainLayout.findViewById<View?>(R.id.tv_title1) as TextView
            tv_all_days1.setTextSize(12f)
            tv_all_days1.setTypeface(ContentActivity.Companion.tf)
            tv_all_days0 = mainLayout.findViewById<View?>(R.id.tv_title0) as TextView
            tv_all_days0.setTextSize(12f)
            tv_all_days0.setTypeface(ContentActivity.Companion.tf)
            relative_top.setOnClickListener(this)
            relative_top1.setOnClickListener(this)
            relative_top0.setOnClickListener(this)
            mLayoutManager = LinearLayoutManager(activity)
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
            my_recycler_view.setLayoutManager(mLayoutManager)
            my_recycler_view.setItemAnimator(DefaultItemAnimator())
            my_recycler_view.addItemDecoration(SpacesItemDecoration(30))
            relative_top.setVisibility(View.VISIBLE)
            relative_top1.setVisibility(View.VISIBLE)

            //relative_top0.setVisibility(View.VISIBLE);
            tv_all_days.setHint(act?.getString(R.string.SelectDay1))
            tv_all_days1.setHint(act?.getString(R.string.SelectAreaLabel))

            //img_bg.setImageResource(R.drawable.list_small4);
            val wimgW = (act?.getResources()?.getDrawable(
                    R.drawable.input) as BitmapDrawable).bitmap.width
            val wimgH = (act?.getResources()!!.getDrawable(
                    R.drawable.input) as BitmapDrawable).bitmap.height
            webView.getLayoutParams().height = wimgH
            webView.getLayoutParams().width = wimgW

            /*param = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 250.0f);
            relative_top0.setLayoutParams(param);

            param = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 525.0f);
            relative_top1.setLayoutParams(param);

            param = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 250.0f);
            relative_top.setLayoutParams(param);*/
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.DeewaniyaLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            ContentActivity.Companion.clearVariables()
            val bundle = Bundle()
            bundle.putString("isEdit", "0")
            ContentActivity.Companion.openAddDeewaniyaFragment(bundle)
        })
        if (getDiwaniyasArrayList.size == 0 || getWeekDaysArrayList.size == 0 || getAreasArrayList.size == 0) {
            getWeekDays()
        } else {
            mAdapter = DeewaniyaAdapter(act!!, getDiwaniyasArrayList)
            my_recycler_view.setAdapter(mAdapter)
        }
        setFilters()
        my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                Log.d("addOnScrollListener", "addOnScrollListener")
                if (!endOfREsults) {
                    visibleItemCount = mLayoutManager.getChildCount()
                    totalItemCount = mLayoutManager.getItemCount()
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                    if (loading_flag) {
                        if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                            if (getDiwaniyasArrayList.size != 0) {
                                progress_loading_more.setVisibility(View.VISIBLE)
                                GlobalFunctions.DisableLayout(mainLayout)
                            }
                            loading_flag = false
                            page_index = page_index + 1
                            getMoreDeewaniya()
                        }
                    }
                }
            }
        })
    }


    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.relative_top0 -> {
            }
            R.id.relative_top1 -> if (getAreasArrayList.size > 0) {
                val inflater1 = act?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(getString(R.string.SelectArea).replace("*", ""))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (areas in getAreasArrayList) {
                    if (languageSeassionManager?.getLang() == "en") {
                        arrayList1.add(areas?.TitleEn)
                    } else {
                        arrayList1.add(areas?.TitleAr)
                    }
                }
                arrayList1.add(getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        areaId = getAreasArrayList.get(i)?.Id!!
                        tv_all_days1.setText(item)
                        areaName = item!!
                        endOfREsults = false
                        loading_flag = true
                        page_index = 0
                        getDeewaniya()
                    }
                })
            }
            R.id.relative_top -> if (getWeekDaysArrayList.size > 0) {
                val inflater1 = act?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(getString(R.string.SelectDay1))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (weekDays in getWeekDaysArrayList) {
                    if (languageSeassionManager.getLang() == "en") {
                        arrayList1.add(weekDays!!.TitleEn)
                    } else {
                        arrayList1.add(weekDays!!.TitleAr)
                    }
                }
                arrayList1.add(getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        dayId = getWeekDaysArrayList.get(i)!!.Id!!
                        tv_all_days.setText(item)
                        dayName = item!!
                        endOfREsults = false
                        loading_flag = true
                        page_index = 0
                        getDeewaniya()
                    }
                })
            }
        }
    }

    fun getDeewaniya() {
        mloading?.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetDewanias("0", areaId, dayId,
                page_index.toString() + "")?.enqueue(
                object : Callback<ArrayList<GetDewanias?>?> {
                    override fun onFailure(call: Call<ArrayList<GetDewanias?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetDewanias?>?>, response: Response<ArrayList<GetDewanias?>?>) {
                        if (response.body() != null) {
                            val getDiwaniyases = response.body()
                            if (mloading != null) {
                                if (getDiwaniyases != null) {
                                    Log.d("getDiwaniyases size", "" + getDiwaniyases.size)
                                    getDiwaniyasArrayList.clear()
                                    getDiwaniyasArrayList.addAll(getDiwaniyases)
                                    if (getDiwaniyases.size == 0)
                                        tv_noDataFound?.putVisibility(View.VISIBLE)
                                    else
                                        tv_noDataFound?.putVisibility(View.GONE)
                                    Log.d("getDiwaniyases-- size", "" + getDiwaniyasArrayList.size)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                    mAdapter = DeewaniyaAdapter(act!!, getDiwaniyasArrayList)
                                    my_recycler_view.setAdapter(mAdapter)
                                    mloading!!.setVisibility(View.GONE)
                                }
                            }
                        }
                    }
                })
    }

    fun getMoreDeewaniya() {
        QenaatAPICall.getCallingAPIInterface()?.GetDewanias("0", areaId, dayId,
                page_index.toString() + "", )?.enqueue(
                object : Callback<ArrayList<GetDewanias?>?> {

                    override fun onFailure(call: Call<ArrayList<GetDewanias?>?>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetDewanias?>?>, response: Response<ArrayList<GetDewanias?>?>) {
                        if (response.body() != null) {
                            val getDiwaniyases = response.body()
                            if (progress_loading_more != null) {
                                progress_loading_more.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                if (getDiwaniyases != null) {
                                    if (getDiwaniyases.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    getDiwaniyasArrayList.addAll(getDiwaniyases)
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                })
    }

    private fun getWeekDays() {
        mloading?.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetWeekDays()?.enqueue(
                object : Callback<ArrayList<GetWeekDays?>?> {

                    override fun onFailure(call: Call<ArrayList<GetWeekDays?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetWeekDays?>?>, response: Response<ArrayList<GetWeekDays?>?>) {
                        if (response.body() != null) {
                            val getWeekDayses = response.body()
                            if (getWeekDayses != null) {
                                if (getWeekDayses.size > 0) {
                                    getWeekDaysArrayList.clear()
                                    val weekDays = GetWeekDays()
                                    weekDays.Id = "0"
                                    weekDays.TitleAr = act!!.getString(R.string.AnyDay)
                                    weekDays.TitleEn = act!!.getString(R.string.AnyDay)
                                    getWeekDaysArrayList.add(0, weekDays)
                                    getWeekDaysArrayList.addAll(getWeekDayses)
                                    dayId = getWeekDaysArrayList.get(0)?.Id!!
                                    getAreas()
                                }
                            }
                        }
                        mloading?.setVisibility(View.INVISIBLE)
                    }
                })
    }

    private fun getAreas() {
        mloading?.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetAreas("0")?.enqueue(
                object : Callback<ArrayList<GetAreas>?> {

                    override fun onFailure(call: Call<ArrayList<GetAreas>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetAreas>?>, response: Response<ArrayList<GetAreas>?>) {
                        if (response.body() != null) {
                            val getAreases = response.body()
                            if (getAreases != null) {
                                getAreasArrayList.clear()
                                val areas = GetAreas()
                                areas.Id = "0"
                                areas.TitleAr = act!!.getString(R.string.AllAreas)
                                areas.TitleEn = act!!.getString(R.string.AllAreas)
                                getAreasArrayList.add(0, areas)
                                getAreasArrayList.addAll(getAreases)
                            }
                            getDeewaniya()
                            mloading?.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun setFilters() {
        if (dayName.length > 0) {
            tv_all_days.setText(dayName)
        }
        if (areaName.length > 0) {
            tv_all_days1.setText(areaName)
        }
    }

    companion object {
        protected val TAG = DeewaniyaFragment::class.java.simpleName
        var act: FragmentActivity? = null
        var fragment: DeewaniyaFragment? = null
        var mSessionManager: SessionManager? = null
        var mloading: ProgressBar? = null
        fun newInstance(act: FragmentActivity?): DeewaniyaFragment? {
            fragment = DeewaniyaFragment()
            Companion.act = act
            return fragment
        }
    }
}