package com.qenaat.app.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util.Companion.KEY_PAYMENT
import com.qenaat.app.adapters.AlertAdapterFA
import com.qenaat.app.adapters.MonthlyDonationAdapter
import com.qenaat.app.classes.*
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkEmptyString
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.model.CauseItem
import com.qenaat.app.model.CauseType
import com.qenaat.app.model.GetMonthlyDonations
import com.qenaat.app.model.ZakaatType
import com.qenaat.app.networking.QenaatAPICall
import company.tap.gosellapi.GoSellSDK
import company.tap.gosellapi.SettingsManager
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.PhoneNumber
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.internal.interfaces.PaymentInterface
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.AppearanceMode
import company.tap.gosellapi.open.enums.CardType
import company.tap.gosellapi.open.enums.TransactionMode
import company.tap.gosellapi.open.models.*
import kotlinx.android.synthetic.main.add_quick_donation.*
import me.drakeet.materialdialog.MaterialDialog
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 13-Nov-17.

 */
class QuickDonateFragment : Fragment(), View.OnClickListener, SessionDelegate, PaymentInterface {
    private var Charge_id = ""
    lateinit var XY: IntArray
    lateinit var languageSessionManager: LanguageSessionManager
    lateinit var et_amount: EditText
    lateinit var et_notes: EditText
    lateinit var tv_pay: TextView
    lateinit var tv_type_label: TextView
    lateinit var progress_loading_more: ProgressBar
    lateinit var my_recycler_view_: RecyclerView
    var type: String? = ""
    var zakaatTypeName: String? = ""
    var zakaatTypeId: String? = "0"
    var zakaatTypeArrayList: ArrayList<ZakaatType?>? = ArrayList()
    var causeTypeName: String? = ""
    var causeTypeId: String? = "0"
    var causeTypeArrayList: ArrayList<CauseItem?>? = ArrayList()
    private val mFormatter: SimpleDateFormat? = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    var page_index = 0
    var endOfREsults = false
    private lateinit var mLayoutManager: LinearLayoutManager
    var IsNew = false
    private lateinit var sdkSession: SDKSession
    private var settingsManager: SettingsManager? = null
    private val SDK_REQUEST_CODE = 1001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSessionManager = LanguageSessionManager(act)
            if (arguments != null) {
                type = arguments?.getString("type")
                if (arguments?.containsKey("IsNew")!!) {
                    IsNew = arguments?.getBoolean("IsNew")!!
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.add_quick_donation, null) as RelativeLayout
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        act.softWindow()
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            //tv_date = mainLayout.findViewById<View?>(R.id.tv_date) as MaterialTextView
            tv_date.setOnClickListener(this)
            progress_loading_more = mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            et_amount = mainLayout.findViewById<View?>(R.id.et_amount) as EditText
            et_notes = mainLayout.findViewById<View?>(R.id.et_notes) as EditText
            tv_pay = mainLayout.findViewById<View?>(R.id.tv_pay) as TextView
            tv_type_label = mainLayout.findViewById<View?>(R.id.tv_type_label) as TextView
            my_recycler_view_ = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
            mLayoutManager = LinearLayoutManager(activity)
            mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
            my_recycler_view.setLayoutManager(mLayoutManager)
            my_recycler_view.setItemAnimator(DefaultItemAnimator())
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_pay.setOnClickListener(this)
            tv_type_label.setOnClickListener(this)
            tv_subscribe.setOnClickListener(this)
            if (type.equals("ZakaatDonate", ignoreCase = true)) {
                tv_type_label.setVisibility(View.VISIBLE)
                scroll_view.setVisibility(View.VISIBLE)
                ti_notes.setVisibility(View.VISIBLE)
                my_recycler_view.setVisibility(View.GONE)
                ti_no_of_months.setVisibility(View.GONE)
            }
            if (type.equals("CauseDonate", ignoreCase = true)) {
                tv_type_label.setVisibility(View.VISIBLE)
                scroll_view.setVisibility(View.VISIBLE)
                my_recycler_view.setVisibility(View.GONE)
                ti_no_of_months.setVisibility(View.GONE)
                tv_type_label.hint = getString(R.string.SelectCause)
            }
            if (type.equals("QuickDonate", ignoreCase = true)) {
                tv_type_label.setVisibility(View.GONE)
                scroll_view.setVisibility(View.VISIBLE)
                my_recycler_view.setVisibility(View.GONE)
                ti_no_of_months.setVisibility(View.GONE)
                tv_date.setVisibility(View.GONE)
                ed_title.setVisibility(View.GONE)
            }
            if (type.equals("MonthlyDonate", ignoreCase = true)) {
                tv_type_label.setVisibility(View.GONE)
                scroll_view.setVisibility(View.GONE)
                my_recycler_view.setVisibility(View.GONE)
                if (!IsNew) {
                    ti_no_of_months.setVisibility(View.GONE)
                }
                tv_date.setVisibility(View.GONE)
                ed_title.setVisibility(View.GONE)
            }

            settingsManager = SettingsManager.getInstance()
            settingsManager?.setPref(act)
            et_amount.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (!et_amount.checkEmpty())
                        sdkSession.setAmount(BigDecimal(et_amount.getTextValue()))
                    else
                        sdkSession.setAmount(BigDecimal("0"))
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }

            })

            // start tap goSellSDK
            startSDK()
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.tabType = 1
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSessionManager)
        if (type.equals("QuickDonate", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.QuickDonateLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
        if (type.equals("MonthlyDonate", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.MonthlyDonationLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            tv_subscribe.visibility = View.VISIBLE
            payButtonView.visibility = View.GONE
            if (IsNew) {
                my_recycler_view.setVisibility(View.GONE)
                scroll_view.setVisibility(View.VISIBLE)
                ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
                ti_no_of_months.setVisibility(View.VISIBLE)
                tv_date.setVisibility(View.VISIBLE)
                ed_title.setVisibility(View.GONE)
                ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
            } else {
                GetMonthlyDonations()
            }
        }
        if (type.equals("ZakaatDonate", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.ZakaatDonationLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            ZakaatType()
        }
        if (type.equals("CauseDonate", ignoreCase = true)) {
            ContentActivity.Companion.mtv_topTitle.setText(R.string.DonateForCauseLabel)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

            CauseType()
        }
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            bundle.putBoolean("IsNew", true)
            bundle.putString("type", "MonthlyDonate")
            ContentActivity.Companion.openQuickDonateFragment(bundle)
        })
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.tv_date -> SlideDateTimePicker.Builder(act.getSupportFragmentManager())
                    .setListener(listener)
                    .setInitialDate(Date())
                    .setMinDate(Date())
                    .build()
                    .show()
            R.id.tv_pay -> if (et_amount.getText().toString().length > 0) {
                if (et_amount.getText().toString().toInt() > 0) {

                    sdkSession.setAmount(BigDecimal(et_amount.getTextValue()))
                    /*configureSDKSession()
                    configureSDKMode()
                    initPayButton()*/

                    if (type.equals("ZakaatDonate", ignoreCase = true)) {
                        if (zakaatTypeName?.length!! > 0) {
                            ZakaatDonation()
                        } else {
                            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                        }
                    } else if (type.equals("CauseDonate", ignoreCase = true)) {
                        if (causeTypeName?.length!! > 0) {
                            CauseDonation()
                        } else {
                            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                        }
                    }else if (type.equals("MonthlyDonate", ignoreCase = true)) {
                        if (et_no_of_months.getText()?.length!! > 0 &&
                                //ed_title.getText().length()>0 &&
                                tv_date.getText().length > 0) {
                            if (et_no_of_months.getText().toString().toInt() > 0) {
                                AddMonthlyDonation()
                            } else {
                                Snackbar.make(mainLayout, act.getString(R.string.MonthNotBeZero), Snackbar.LENGTH_LONG).show()
                            }
                        } else {
                            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                        }
                    } else {
                        AddQuickDonation()
                    }
                } else {
                    Snackbar.make(mainLayout, act.getString(R.string.enter_amount), Snackbar.LENGTH_LONG).show()
                }
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
            }

            R.id.tv_type_label -> if (zakaatTypeArrayList?.size!! > 0) {
                val inflater1 = act.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(getString(R.string.SelectType).replace("*", ""))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (zakaatType in zakaatTypeArrayList!!) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        arrayList1.add(zakaatType!!.NameEN)
                    } else {
                        arrayList1.add(zakaatType!!.NameAR)
                    }
                }
                arrayList1.add(getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        zakaatTypeId = zakaatTypeArrayList!!.get(i)?.Id
                        tv_type_label.setText(item)
                        zakaatTypeName = item
                    }
                })
            } else if (causeTypeArrayList?.size!! > 0) {
                val inflater1 = act.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(getString(R.string.SelectType).replace("*", ""))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (causeType in causeTypeArrayList!!) {
                    if (languageSessionManager.getLang().equals("en", ignoreCase = true)) {
                        arrayList1.add(causeType!!.TitleEN)
                    } else {
                        arrayList1.add(causeType!!.TitleAR)
                    }
                }
                arrayList1.add(getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        causeTypeId = causeTypeArrayList!!.get(i)?.Id
                        tv_type_label.setText(item)
                        causeTypeName = item

                    }
                })
            }
            R.id.tv_subscribe -> if (isValid()) AddMonthlyDonation()
        }
    }

    private fun ZakaatType() {

//        ZakaatType zakaatType = new ZakaatType();
//        zakaatType.setId("1");
//        zakaatType.setProjectNameAR(act.getString(R.string.Sadqa));
//        zakaatType.setProjectNameEN(act.getString(R.string.Sadqa));
//        zakaatTypeArrayList.add(zakaatType);
//
//        zakaatType = new ZakaatType();
//        zakaatType.setId("2");
//        zakaatType.setProjectNameAR(act.getString(R.string.SadqaeFitr));
//        zakaatType.setProjectNameEN(act.getString(R.string.SadqaeFitr));
//        zakaatTypeArrayList.add(zakaatType);
//
//        zakaatType = new ZakaatType();
//        zakaatType.setId("3");
//        zakaatType.setProjectNameAR(act.getString(R.string.SadqaeMoney));
//        zakaatType.setProjectNameEN(act.getString(R.string.SadqaeMoney));
//        zakaatTypeArrayList.add(zakaatType);
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.ZakaatType()?.enqueue(
                object : Callback<ArrayList<ZakaatType?>?> {

                    override fun onFailure(call: Call<ArrayList<ZakaatType?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<ZakaatType?>?>, response: Response<ArrayList<ZakaatType?>?>) {
                        if (response.body() != null) {
                            val zakaatTypes = response.body()

                            Log.e("zakaatresponse",""+response.body())

                            if (zakaatTypes != null) {
                                zakaatTypeArrayList?.clear()
                                zakaatTypeArrayList?.addAll(zakaatTypes)

                                Log.e("zakaat----",""+zakaatTypes)
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    private fun CauseType() {


        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)


        QenaatAPICall.getCallingAPIInterface()?.CauseType("${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}","application/json")?.enqueue(
            object : Callback<CauseType?> {
                override fun onFailure(call: Call<CauseType?>, t: Throwable) {
                    t.printStackTrace()
                    Log.e("llllaalalal11---",""+call.request().body)
                    Log.e("llllaalalal12---",""+call.request().url)
                    Log.e("llllaalalal12---",""+t.message)
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }

                override fun onResponse(call: Call<CauseType?>, response: Response<CauseType?>) {

                    Log.e("llllaalalal11",""+call.request().body)
                    Log.e("llllaalalal12",""+call.request().url)
                    Log.e("llllaalalal13",""+response.body())
                    Log.e("llllaalalal14",""+response.errorBody()?.string())

                    if (response.body() != null) {
                        val causeTypes = response.body()?.cause

                        Log.e("causeResponse",""+response.body())
                        Log.e("causeResponse",""+causeTypes)

                        if (causeTypes != null) {
//
                            causeTypeArrayList?.clear()
                            causeTypeArrayList?.addAll(causeTypes)
//
//                            Log.e("causetype",""+causeTypes)
                        }
                    }
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
            })
    }


    private fun AddQuickDonation() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.AddQuickDonation(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                et_amount.getText().toString(), "1",
                mSessionManager.getUserCode(), Charge_id)?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (outResponse.contains("https") || outResponse.contains("http")) {
                                val b = Bundle()
                                b.putString("id", outResponse)
                                val frag: Fragment = PayPaymentFragment.Companion.newInstance(act)!!
                                frag.arguments = b
                                act.getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                        .addToBackStack(frag.javaClass.name)
                                        .replace(R.id.content_frame, frag).commit()
                            } else {
                                if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                })
    }

    private fun ZakaatDonation() {

        Log.e("notessss",""+et_notes.text)

        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.ZakaatDonation(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                et_amount.getText().toString(), "2", zakaatTypeId, Charge_id,if (et_notes.checkEmpty()) "" else et_notes.getText().toString())?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (outResponse.contains("https") || outResponse.contains("http")) {
                                val b = Bundle()
                                b.putString("id", outResponse)
                                val frag: Fragment = PayPaymentFragment.Companion.newInstance(act)!!
                                frag.arguments = b
                                act.getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                        .addToBackStack(frag.javaClass.name)
                                        .replace(R.id.content_frame, frag).commit()
                            } else {
                                if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                })
    }

    private fun CauseDonation() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.CauseDonation(
            "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
            et_amount.getText().toString(), "6", causeTypeId, Charge_id)?.enqueue(
            object : Callback<ResponseBody?> {

                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)

                    Log.e("llllaalalal21---",""+call.request().body)
                    Log.e("llllaalalal22---",""+call.request().url)
                    Log.e("llllaalalal23---",""+t.message)
                }

                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    val body = response?.body()

                    Log.e("llllaalalal21",""+call.request().body)
                    Log.e("llllaalalal22",""+call.request().url)
                    Log.e("llllaalalal23",""+response.body())
                    Log.e("llllaalalal24",""+response.errorBody()?.string())

                    var outResponse = ""
                    try {
                        val reader = BufferedReader(InputStreamReader(
                            ByteArrayInputStream(body?.bytes())))
                        val out = StringBuilder()
                        val newLine = System.getProperty("line.separator")
                        var line: String?
                        while (reader.readLine().also { line = it } != null) {
                            out.append(line)
                            out.append(newLine)
                        }
                        outResponse = out.toString()
                        Log.d("outResponse", "" + outResponse)
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                    if (outResponse != null) {
                        outResponse = outResponse.replace("\"", "")
                        outResponse = outResponse.replace("\n", "")
                        Log.e("outResponse not null ", outResponse)
                        if (outResponse.contains("https") || outResponse.contains("http")) {
                            val b = Bundle()
                            b.putString("id", outResponse)
                            val frag: Fragment = PayPaymentFragment.Companion.newInstance(act)!!
                            frag.arguments = b
                            act.getSupportFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                .addToBackStack(frag.javaClass.name)
                                .replace(R.id.content_frame, frag).commit()
                        } else {
                            if (outResponse == "-1") {
                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                            } else if (outResponse == "-2") {
                                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                            }
                        }
                    }
                }
            })
    }

    private fun AddMonthlyDonation() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.AddMonthlyDonation(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                et_amount.getText().toString(),
                mSessionManager.getUserCode(), et_no_of_months.getText().toString(),
                tv_date.getText().toString())?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            fragmentManager?.popBackStack()
                            act.hideKeyboard()

                            /*if (outResponse.contains("https")) {

                                Bundle b = new Bundle();
                                b.putString("id", outResponse.toString());

                                Fragment frag = PayPaymentFragment.newInstance(act);
                                frag.setArguments(b);
                                act.getSupportFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                                        .addToBackStack(frag.getClass().getName())
                                        .replace(R.id.content_frame, frag).commit();

                            } else {

                                if (outResponse.equals("-1")) {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show();
                                }else if (outResponse.equals("-2")) {
                                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show();
                                }

                            }*/
                        }
                    }
                })
    }

    private val listener: SlideDateTimeListener? = object : SlideDateTimeListener() {
        override fun onDateTimeSet(date: Date?) {
            tv_date.setText(mFormatter?.format(date).toString())
            ContentActivity.Companion.date = mFormatter?.format(date).toString()
        }

        // Optional cancel listener
        override fun onDateTimeCancel() {}
    }

    companion object {
        protected val TAG = QuickDonateFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: QuickDonateFragment
        lateinit var mainLayout: RelativeLayout
        lateinit var mSessionManager: SessionManager
        lateinit var et_no_of_months: EditText
        lateinit var mloading: ProgressBar
        lateinit var scroll_view: ScrollView
        lateinit var my_recycler_view: RecyclerView
        private val getMonthlyDonationsArrayList: ArrayList<GetMonthlyDonations>? = ArrayList()
        private const val loading_flag = true
        var pastVisiblesItems = 0
        var visibleItemCount = 0
        var totalItemCount = 0
        private lateinit var mAdapter: RecyclerView.Adapter<*>
        lateinit var ed_title: EditText
        fun newInstance(act: FragmentActivity): QuickDonateFragment {
            fragment = QuickDonateFragment()
            Companion.act = act
            return fragment
        }

        fun CancelMonthlyDonation(position: Int) {
            mloading.setVisibility(View.VISIBLE)
            val monthlyDonations = getMonthlyDonationsArrayList?.get(position)
            QenaatAPICall.getCallingAPIInterface()?.CancelMonthlyDonation(monthlyDonations?.Id,
                    mSessionManager.getUserCode())?.enqueue(
                    object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.GONE)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            /* var items = GetMonthlyDonations()
                             try {
                                 items = monthlyDonations?.clone() as Unit
                             } catch (e: CloneNotSupportedException) {
                                 e.printStackTrace()
                             }*/
                            mloading.setVisibility(View.GONE)
                            val body = response?.body()
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                val outResponse = out.toString().trim { it <= ' ' }.replace("\"", "")
                                val responseInteger = outResponse.toInt()
                                if (responseInteger > 0) {
                                    Snackbar.make(mainLayout, act.getString(R.string.DonationCancelled), Snackbar.LENGTH_LONG).show()
                                } else {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                }
                                GetMonthlyDonations()
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                                mloading.setVisibility(View.GONE)
                            }
                        }
                    })
        }
    }

    private fun GetMonthlyDonations() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetMonthlyDonations(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager?.getAuthToken()}",
                "0",
                mSessionManager.getUserCode(), "-1")?.enqueue(
                object : Callback<ArrayList<GetMonthlyDonations>?> {

                    override fun onFailure(call: Call<ArrayList<GetMonthlyDonations>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetMonthlyDonations>?>, response: Response<ArrayList<GetMonthlyDonations>?>) {
                        if (response.body() != null) {
                            val getMonthlyDonations = response.body()
                            if (getMonthlyDonations != null) {
                                getMonthlyDonationsArrayList?.clear()
                                getMonthlyDonationsArrayList?.addAll(getMonthlyDonations)
                                if (getMonthlyDonationsArrayList?.size!! > 0) {
                                    my_recycler_view.setVisibility(View.VISIBLE)
                                    scroll_view.setVisibility(View.GONE)
                                    mAdapter = MonthlyDonationAdapter(act, getMonthlyDonationsArrayList)
                                    my_recycler_view.setAdapter(mAdapter)
                                    ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)
                                } else {
                                    my_recycler_view.setVisibility(View.GONE)
                                    scroll_view.setVisibility(View.VISIBLE)
                                    ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
                                    ti_no_of_months.setVisibility(View.VISIBLE)
                                    tv_date.setVisibility(View.VISIBLE)
                                    ed_title.setVisibility(View.GONE)
                                }
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun startSDK() {
        /**
         * Required step.
         * Configure SDK with your Secret API key and App Bundle name registered with tap company.
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            configureApp()
        }
        /**
         * Optional step
         * Here you can configure your app theme (Look and Feel).
         */
        configureSDKThemeObject()
        /**
         * Required step.
         * Configure SDK Session with all required data.
         */
        configureSDKSession()
        /**
         * Required step.
         * Choose between different SDK modes
         */
        configureSDKMode()
        /**
         * If you included Tap Pay Button then configure it first, if not then ignore this step.
         */
        initPayButton()
    }

    /**
     * Required step.
     * Configure SDK with your Secret API key and App Bundle name registered with tap company.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun configureApp() {
        //GoSellSDK.init(act, "sk_test_kovrMB0mupFJXfNZWx6Etg5y",
        //GoSellSDK.init(act, "sk_test_DMUdB4qz1XeoKfZn2SYwsP95",
        GoSellSDK.init(act, KEY_PAYMENT,
                "com.qenaat.app") // to be replaced by merchant
        GoSellSDK.setLocale(languageSessionManager.getLang()) //  language to be set by merchant
    }

    /**
     * Configure SDK Theme
     */
    private fun configureSDKThemeObject() {
        ThemeObject.getInstance()
                .setAppearanceMode(AppearanceMode.WINDOWED_MODE)
                .setSdkLanguage(languageSessionManager.getLang())
                .setHeaderTextColor(resources.getColor(R.color.black1))
                .setHeaderTextSize(17)
                .setHeaderBackgroundColor(resources.getColor(R.color.french_gray_new))
                .setCardInputTextColor(resources.getColor(R.color.black))
                .setCardInputInvalidTextColor(resources.getColor(R.color.red))
                .setCardInputPlaceholderTextColor(resources.getColor(R.color.gray))
                .setSaveCardSwitchOffThumbTint(resources.getColor(R.color.french_gray_new))
                .setSaveCardSwitchOnThumbTint(resources.getColor(R.color.color_button))
                .setSaveCardSwitchOffTrackTint(resources.getColor(R.color.french_gray))
                .setSaveCardSwitchOnTrackTint(resources.getColor(R.color.color_button))
                .setScanIconDrawable(resources.getDrawable(R.drawable.btn_card_scanner_normal))
                .setPayButtonResourceId(R.drawable.btn_pay_selector) //btn_pay_merchant_selector
                .setPayButtonDisabledTitleColor(resources.getColor(R.color.white))
                .setPayButtonEnabledTitleColor(resources.getColor(R.color.color_text_light))
                .setPayButtonTextSize(16)
                .setPayButtonLoaderVisible(true)
                .setPayButtonSecurityIconVisible(true) //.setPayButtonText("PAY BTN CAN BE VERY VERY VERY  LONGGGG LONGGGGG") // **Optional**
                // setup dialog textcolor and textsize
                .setDialogTextColor(resources.getColor(R.color.black1)).dialogTextSize = 17 // **Optional**
    }


    /**
     * Configure SDK Session
     */
    private fun configureSDKSession() {

        // Instantiate SDK Session
        sdkSession = SDKSession(this) //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession.setTransactionCurrency(TapCurrency("KWD")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession.setAmount(BigDecimal(0)) //** Required **

        // Set Payment Items array list
        sdkSession.setPaymentItems(ArrayList<PaymentItem>()) // ** Optional ** you can pass empty array list


//       sdkSession.setPaymentType("CARD");   //** Merchant can pass paymentType

        // Set Taxes array list
        sdkSession.setTaxes(ArrayList<Tax>()) // ** Optional ** you can pass empty array list

        // Set Shipping array list
        sdkSession.setShipping(ArrayList<Shipping>()) // ** Optional ** you can pass empty array list

        // Post URL
        sdkSession.setPostURL("") // ** Optional **

        // Payment Description
        sdkSession.setPaymentDescription("") //** Optional **

        // Payment Extra Info
        sdkSession.setPaymentMetadata(HashMap<String, String>()) // ** Optional ** you can pass empty array hash map

        // Payment Reference
        sdkSession.setPaymentReference(null) // ** Optional ** you can pass null

        // Payment Statement Descriptor
        sdkSession.setPaymentStatementDescriptor("") // ** Optional **

        // Enable or Disable Saving Card
        sdkSession.isUserAllowedToSaveCard(true) //  ** Required ** you can pass boolean

        // Enable or Disable 3DSecure
        sdkSession.isRequires3DSecure(true)

        //Set Receipt Settings [SMS - Email ]
        sdkSession.setReceiptSettings(Receipt(false, false)) // ** Optional ** you can pass Receipt object or null

        // Set Authorize Action
        sdkSession.setAuthorizeAction(null) // ** Optional ** you can pass AuthorizeAction object or null
        sdkSession.setDestination(null) // ** Optional ** you can pass Destinations object or null
        sdkSession.setMerchantID(null) // ** Optional ** you can pass merchant id or null
        sdkSession.setCardType(CardType.CREDIT) // ** Optional ** you can pass which cardType[CREDIT/DEBIT] you want.By default it loads all available cards for Merchant.

        // sdkSession.setDefaultCardHolderName("TEST TAP"); // ** Optional ** you can pass default CardHolderName of the user .So you don't need to type it.
        // sdkSession.isUserAllowedToEnableCardHolderName(false); // ** Optional ** you can enable/ disable  default CardHolderName .
    }


    /**
     * Configure SDK Theme
     */
    private fun configureSDKMode() {
        /**
         * You have to choose only one Mode of the following modes:
         * Note:-
         * - In case of using PayButton, then don't call sdkSession.start(this); because the SDK will start when user clicks the tap pay button.
         */
        //////////////////////////////////////////////////////    SDK with UI //////////////////////
        /**
         * 1- Start using  SDK features through SDK main activity (With Tap CARD FORM)
         */
        startSDKWithUI()
    }


    /**
     * Start using  SDK features through SDK main activity
     */
    private fun startSDKWithUI() {
        if (sdkSession != null) {
            val trx_mode = if (settingsManager != null) settingsManager?.getTransactionsMode("key_sdk_transaction_mode") else TransactionMode.PURCHASE
            // set transaction mode [TransactionMode.PURCHASE - TransactionMode.AUTHORIZE_CAPTURE - TransactionMode.SAVE_CARD - TransactionMode.TOKENIZE_CARD ]
            sdkSession.setTransactionMode(trx_mode) //** Required **
            // if you are not using tap button then start SDK using the following call
            //sdkSession.start(this);
        }
    }

    /**
     * Include pay button in merchant page
     */
    private fun initPayButton() {
        if (ThemeObject.getInstance().payButtonFont != null) payButtonView.setupFontTypeFace(ThemeObject.getInstance().payButtonFont)
        if (ThemeObject.getInstance().payButtonDisabledTitleColor != 0 && ThemeObject.getInstance().payButtonEnabledTitleColor != 0) payButtonView.setupTextColor(ThemeObject.getInstance().payButtonEnabledTitleColor,
                ThemeObject.getInstance().payButtonDisabledTitleColor)
        if (ThemeObject.getInstance().payButtonTextSize != 0) payButtonView.getPayButton().setTextSize(ThemeObject.getInstance().payButtonTextSize.toFloat())
        //
        if (ThemeObject.getInstance().isPayButtSecurityIconVisible) payButtonView.getSecurityIconView().setVisibility(if (ThemeObject.getInstance().isPayButtSecurityIconVisible) View.VISIBLE else View.INVISIBLE)
        if (ThemeObject.getInstance().payButtonResourceId != 0) payButtonView.setBackgroundSelector(ThemeObject.getInstance().payButtonResourceId)
        Log.e("rohannnnnLLLKLKK", "" + sdkSession.isValid)
        if (sdkSession != null) {
            val trx_mode: TransactionMode = sdkSession.getTransactionMode()
            if (trx_mode != null) {
                if (TransactionMode.SAVE_CARD == trx_mode || TransactionMode.SAVE_CARD_NO_UI == trx_mode) {
                    payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.save_card))
                } else if (TransactionMode.TOKENIZE_CARD == trx_mode || TransactionMode.TOKENIZE_CARD_NO_UI == trx_mode) {
                    payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.tokenize))
                } else {
                    payButtonView.getPayButton().setText(getString(company.tap.gosellapi.R.string.pay))
                }
            } else {
                configureSDKMode()
            }
            sdkSession.setButtonView(payButtonView, act, SDK_REQUEST_CODE)
        }
    }

    private fun getCustomer(): Customer { // test customer id cus_Kh1b4220191939i1KP2506448
        val customer = if ((settingsManager != null)) settingsManager?.getCustomer() else null
        val phoneNumber = if (customer != null) customer.getPhone() else PhoneNumber("965", "69045932")
        return Customer.CustomerBuilder(null).email("abc@abc.com").firstName("firstname")
                .lastName("lastname").metadata("").phone(PhoneNumber(phoneNumber?.getCountryCode(), phoneNumber?.getNumber()))
                .middleName("middlename").build()
    }

    override fun sessionCancelled() {
        Log.e("Tap_Payment", " Session Cancelled.......")
    }

    override fun savedCardsList(cardsList: CardsList) {

    }

    override fun sessionIsStarting() {
        Log.e("Tap_Payment", " Session Is Starting.....")
    }

    override fun invalidCardDetails() {
        Log.e("Tap_Payment", " Card details are invalid....")
    }

    override fun cardSavingFailed(charge: Charge) {

    }

    override fun backendUnknownError(message: String?) {
        Log.e("Tap_Payment", "Backend Un-Known error.... : $message")
    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {

    }

    override fun cardSaved(charge: Charge) {

    }

    override fun paymentSucceed(charge: Charge) {
        mainLayout.showSnakeBar(getString(R.string.payment_succeeded))
        Charge_id = charge.id

        Log.e("charge_id","charrggggeeeee iddddd"+Charge_id)

        //AddMonthlyDonation()
        when (type) {
            "QuickDonate" -> AddQuickDonation()
            "ZakaatDonate" -> ZakaatDonation()
            "CauseDonate" -> CauseDonation()
        }
        Log.e("Tap_Payment", "Payment Succeeded : charge id : " + charge.id)
        Log.e("Tap_Payment", "Payment Succeeded : charge status : " + charge.status)
        Log.e("Tap_Payment", "Payment Succeeded : description : " + charge.description)
        Log.e("Tap_Payment", "Payment Succeeded : message : " + charge.response.message)
        Log.e("Tap_Payment", "##############################################################################")
        if (charge.card != null) {
            Log.e("Tap_Payment", "Payment Succeeded : first six : " + charge.card!!.firstSix)
            Log.e("Tap_Payment", "Payment Succeeded : last four: " + charge.card!!.last4)
            Log.e("Tap_Payment", "Payment Succeeded : card object : " + charge.card!!.getObject())
            Log.e("Tap_Payment", "Payment Succeeded : brand : " + charge.card!!.brand)
            Log.e("Tap_Payment", """
    Payment Succeeded : expiry : ${charge.card!!.expiry!!.month}
    ${charge.card!!.expiry!!.year}
    """.trimIndent())


        }

        Log.e("Tap_Payment", "##############################################################################")
        if (charge.acquirer != null) {
            Log.e("Tap_Payment", "Payment Succeeded : acquirer id : " + charge.acquirer!!.id)
            Log.e("Tap_Payment", "Payment Succeeded : acquirer response code : " + charge.acquirer!!.response.code)
            Log.e("Tap_Payment", "Payment Succeeded : acquirer response message: " + charge.acquirer!!.response.message)
        }
        Log.e("Tap_Payment", "##############################################################################")
        if (charge.source != null) {
            Log.e("Tap_Payment", "Payment Succeeded : source id: " + charge.source.id)
            Log.e("Tap_Payment", "Payment Succeeded : source channel: " + charge.source.channel)
            Log.e("Tap_Payment", "Payment Succeeded : source object: " + charge.source.getObject())
            Log.e("Tap_Payment", "Payment Succeeded : source payment method: " + charge.source.paymentMethodStringValue)
            Log.e("Tap_Payment", "Payment Succeeded : source payment type: " + charge.source.paymentType)
            Log.e("Tap_Payment", "Payment Succeeded : source type: " + charge.source.type)
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (charge.expiry != null) {
            Log.e("Tap_Payment", "Payment Succeeded : expiry type :" + charge.expiry!!.type)
            Log.e("Tap_Payment", "Payment Succeeded : expiry period :" + charge.expiry!!.period)
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (charge.transaction != null) {
            Log.e("Tap_Payment", "Payment Succeeded : authorizationID :" + charge.transaction.authorizationID)
            Log.e("Tap_Payment", "Payment Succeeded : transactionUrl :" + charge.transaction.url)
        }

        et_amount.setText("")
        configureSDKSession()
    }

    override fun authorizationFailed(authorize: Authorize?) {
        Log.e("Tap_Payment", "Authorize Failed : " + authorize!!.status)
        Log.e("Tap_Payment", "Authorize Failed : " + authorize.description)
        Log.e("Tap_Payment", "Authorize Failed : " + authorize.response.message)
    }

    override fun cardTokenizedSuccessfully(token: Token) {
        Log.e("Tap_Payment", "Card Tokenized Succeeded : ")
        Log.e("Tap_Payment", "Token card : " + token.card.firstSix + " **** " + token.card.lastFour)
        Log.e("Tap_Payment", "Token card : " + token.card.fingerprint + " **** " + token.card.funding)
        Log.e("Tap_Payment", "Token card : " + token.card.id + " ****** " + token.card.name)
        Log.e("Tap_Payment", "Token card : " + token.card.address + " ****** " + token.card.getObject())
        Log.e("Tap_Payment", "Token card : " + token.card.expirationMonth + " ****** " + token.card.expirationYear)
    }

    override fun authorizationSucceed(authorize: Authorize) {
        Log.e("Tap_Payment", "Authorize Succeeded : $authorize")
        Log.e("Tap_Payment", "Authorize Succeeded : " + authorize.status)
        Log.e("Tap_Payment", "Authorize Succeeded : " + authorize.response.message)

        if (authorize.card != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : first six : " + authorize.card!!.firstSix)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : last four: " + authorize.card!!.last4)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : card object : " + authorize.card!!.getObject())
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (authorize.acquirer != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : acquirer id : " + authorize.acquirer!!.id)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : acquirer response code : " + authorize.acquirer!!.response.code)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : acquirer response message: " + authorize.acquirer!!.response.message)
        }
        Log.e("Tap_Payment", "##############################################################################")
        if (authorize.source != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source id: " + authorize.source.id)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source channel: " + authorize.source.channel)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source object: " + authorize.source.getObject())
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source payment method: " + authorize.source.paymentMethodStringValue)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source payment type: " + authorize.source.paymentType)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : source type: " + authorize.source.type)
        }

        Log.e("Tap_Payment", "##############################################################################")
        if (authorize.expiry != null) {
            Log.e("Tap_Payment", "Payment Authorized Succeeded : expiry type :" + authorize.expiry!!.type)
            Log.e("Tap_Payment", "Payment Authorized Succeeded : expiry period :" + authorize.expiry!!.period)
        }

        et_amount.setText("")
        configureSDKSession()
    }

    override fun invalidTransactionMode() {
        Log.e("Tap_Payment", " invalidTransactionMode  ......")
    }

    override fun sdkError(goSellError: GoSellError?) {
        if (goSellError != null) {
            Log.e("Tap_Payment", "SDK Process Error : " + goSellError.errorBody)
            Log.e("Tap_Payment", "SDK Process Error : " + goSellError.errorMessage)
            Log.e("Tap_Payment", "SDK Process Error : " + goSellError.errorCode)
        }
    }

    override fun sessionFailedToStart() {
        Log.e("Tap_Payment", "Session Failed to start.........")
    }

    override fun paymentFailed(charge: Charge?) {
        mainLayout.showSnakeBar(getString(R.string.payment_failed))
        Log.e("Tap_Payment", "Payment Failed : " + charge!!.status)
        Log.e("Tap_Payment", "Payment Failed : " + charge.description)
        Log.e("Tap_Payment", "Payment Failed : " + charge.response.code)
        Log.e("Tap_Payment", "Payment Failed : " + charge.response.message)
    }

    override fun sessionHasStarted() {
        Log.e("Tap_Payment", " Session Has Started .......")
    }

    override fun invalidCustomerID() {
        Log.e("Tap_Payment", "Invalid Customer ID .......")
    }

    override fun onPayment() {
        isValid()
    }

    private fun isValid(): Boolean {

        when {
            et_amount.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_amount))
                sdkSession.isValid = false
                et_amount.requestFocus()
                return false
            }
            et_amount.getTextValue().trim().toInt() <= 0  ->{
                mainLayout.showSnakeBar(getString(R.string.amount_grather_then_0))
                sdkSession.isValid = false
                et_amount.requestFocus()
                return false
            }


            else -> {
                when (type) {
                    "ZakaatDonate" -> {
                        when {
                            zakaatTypeName?.checkEmptyString()!! -> {
                                mainLayout.showSnakeBar(getString(R.string.select_type))
                                sdkSession.isValid = false
                                return false
                            }
                        }
                    }"CauseDonate" -> {
                        when {
                            causeTypeName?.checkEmptyString()!! -> {
                                mainLayout.showSnakeBar(getString(R.string.select_type))
                                sdkSession.isValid = false
                                return false
                            }
                        }
                    }
                    "MonthlyDonate" -> {
                        when {
                            tv_date.checkEmpty() -> {
                                mainLayout.showSnakeBar(getString(R.string.enter_date))
                                sdkSession.isValid = false
                                tv_date.requestFocus()
                                return false
                            }
                            et_no_of_months.checkEmpty() -> {
                                mainLayout.showSnakeBar(getString(R.string.enter_month))
                                sdkSession.isValid = false
                                et_no_of_months.requestFocus()
                                return false
                            }
                            et_no_of_months.getTextValue().trim() == "0" || et_no_of_months.getTextValue().take(1) == "0" -> {
                                mainLayout.showSnakeBar(getString(R.string.MonthNotBeZero))
                                sdkSession.isValid = false
                                et_no_of_months.requestFocus()
                                return false
                            }
                        }
                    }
                }
            }
        }
        sdkSession.isValid = true
        return true
    }
}