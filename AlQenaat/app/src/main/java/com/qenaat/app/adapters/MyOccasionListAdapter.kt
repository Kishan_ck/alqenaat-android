package com.qenaat.app.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.fragments.MyOccasionFragment
import com.qenaat.app.model.GetEvents
import java.util.*

/**
 * Created by DELL on 11-Feb-18.
 */
class MyOccasionListAdapter(var act: FragmentActivity, private val itemsData: ArrayList<GetEvents>,
                            var tabNumber: Int) : RecyclerView.Adapter<MyOccasionListAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager
    var imgH: Int = 0
    var imgW: Int = 0

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_occasion_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                viewHolder.tv_title?.setText(itemsData.get(position).EventNameEn)
            } else {
                viewHolder.tv_title?.setText(itemsData.get(position).EventNameAr)
            }
            viewHolder.tv_reminder?.setOnClickListener(View.OnClickListener {
                AlertDialog.Builder(act)
                        .setTitle(act.getString(R.string.Confirm))
                        .setMessage(act.getString(R.string.AreYouSureYouWantToRemindLabel))
                        .setPositiveButton(android.R.string.yes) { dialog, which ->
                            Log.e("afterTextChanged", "update")
                            dialog.dismiss()
                            MyOccasionFragment.Companion.RemindInvitationList(itemsData.get(position).Id,
                                    itemsData.get(position).InvitationListId)
                        }
                        .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
                        .setIcon(R.drawable.icon_512)
                        .show()
            })
            viewHolder.tv_resend?.setOnClickListener(View.OnClickListener {
                AlertDialog.Builder(act)
                        .setTitle(act.getString(R.string.Confirm))
                        .setMessage(act.getString(R.string.AreYouSureYouWantToResendLabel))
                        .setPositiveButton(android.R.string.yes) { dialog, which ->
                            Log.e("afterTextChanged", "update")
                            dialog.dismiss()
                            MyOccasionFragment.Companion.ResendInvitationList(itemsData.get(position).Id,
                                    itemsData.get(position).InvitationListId)
                        }
                        .setNegativeButton(android.R.string.no) { dialog, which -> dialog.dismiss() }
                        .setIcon(R.drawable.icon_512)
                        .show()
            })
            viewHolder.relative_parent?.setOnClickListener(View.OnClickListener {
                //                    Gson gson = new Gson();
//
//                    ContentActivity.clearVariables();
//
//                    Bundle bundle = new Bundle();
//
//                    bundle.putString("isEdit", "true");
//
//                    bundle.putString("GetInvitationLists", gson.toJson(itemsData.get(position)));
//
//                    ContentActivity.openCreateMyListFragment(bundle);
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView?
        var relative_parent: RelativeLayout?
        var tv_title: TextView?
        var tv_reminder: TextView?
        var tv_resend: TextView?

        init {
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView?
            tv_reminder = itemLayoutView.findViewById<View?>(R.id.tv_reminder) as TextView?
            tv_resend = itemLayoutView.findViewById<View?>(R.id.tv_resend) as TextView?
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView?
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout?
            ContentActivity.Companion.setTextFonts(relative_parent!!)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
        imgH = imgH
        imgW = imgW
    }
}