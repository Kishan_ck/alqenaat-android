package com.qenaat.app.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.model.GetDewanias
import java.util.*

/**
 * Created by DELL on 29-Oct-17.
 */
class DeewaniyaAdapter(var act: FragmentActivity, private val itemsData: ArrayList<GetDewanias?>) : RecyclerView.Adapter<DeewaniyaAdapter.ViewHolder?>() {
    var languageSeassionManager: LanguageSessionManager
    var imgH: Int = 0
    var imgW: Int = 0

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.deewaniya_row, null)

        // create ViewHolder
        return ViewHolder(itemLayoutView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // - get data from your itemsData at this position
        // - replace the contents of the view with that itemsData
        if (itemsData.get(position) != null) {
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                viewHolder.tv_title.setText(itemsData.get(position)?.TitleEn)
                viewHolder.tv_area.setText(itemsData.get(position)?.AreaTitleEn)
            } else {
                viewHolder.tv_title.setText(itemsData.get(position)?.TitleAr)
                viewHolder.tv_area.setText(itemsData.get(position)?.AreaTitleAr)
            }
            viewHolder.tv_day.setText("")
            if (itemsData.get(position)?.WeekDays != null) {
                if (itemsData.get(position)?.WeekDays!!.size > 0) {
                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                        viewHolder.tv_day.setText(itemsData.get(position)?.WeekDays!![0]?.TitleEn)
                    } else {
                        viewHolder.tv_day.setText(itemsData.get(position)?.WeekDays!![0]?.TitleAr)
                    }
                }
            }
            viewHolder.relative_parent.setOnClickListener(View.OnClickListener {
                val gson = Gson()
                val b = Bundle()
                b.putString("GetDewanias", gson.toJson(itemsData.get(position)))
                ContentActivity.Companion.openDeewaniyaDetailsFragment(b)
            })
        }
    }

    // inner class to hold a reference to each item of RecyclerView
    class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var img_bg: ImageView
        var relative_parent: RelativeLayout
        var relative_content: RelativeLayout
        var tv_area: TextView
        var tv_day: TextView
        var tv_title: TextView

        init {
            tv_title = itemLayoutView.findViewById<View?>(R.id.tv_title) as TextView
            tv_day = itemLayoutView.findViewById<View?>(R.id.tv_day) as TextView
            tv_area = itemLayoutView.findViewById<View?>(R.id.tv_area) as TextView
            img_bg = itemLayoutView.findViewById<View?>(R.id.img_bg) as ImageView
            relative_parent = itemLayoutView
                    .findViewById<View?>(R.id.relative_parent) as RelativeLayout
            relative_content = itemLayoutView
                    .findViewById<View?>(R.id.relative_content) as RelativeLayout
            ContentActivity.Companion.setTextFonts(relative_content)
        }
    }

    // Return the size of your itemsData (invoked by the layout manager)
    override fun getItemCount(): Int {
        return itemsData.size
    }

    init {
        languageSeassionManager = LanguageSessionManager(act)
        imgH = imgH
        imgW = imgW
    }
}