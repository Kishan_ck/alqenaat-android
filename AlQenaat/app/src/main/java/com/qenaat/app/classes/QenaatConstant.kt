package com.qenaat.app.classes

/**
 * Created by shahbazshaikh on 14/07/16.
 */
object QenaatConstant {
    /*val BASE_URL: String? = "http://webapi.alqenaat.com/api"
    val Photo_URL: String? = "http://admin.alqenaat.com/api/FileService/Upload?FileObjectType="*/ //    public static final String BASE_URL = "http://qenaatwebapi.hardtask.co/api";
    //    public static final String Photo_URL = "http://qenaatcpanel.hardtask.co/api/FileService/Upload?FileObjectType=";

    //.NET
    /*val BASE_URL: String? = "http://192.168.0.105/web_api/api/"
    val Photo_URL: String? = "http://192.168.0.105:8080/api/FileService/Upload?FileObjectType="*/
    //val Photo_URL: String? = "http://192.168.0.105/web_api/api/FileService/Upload?FileObjectType="
    //val Photo_URL: String? = "http://192.168.0.105/Alq_common/api/FileService/Upload?FileObjectType="

    //PHP
    //val BASE_URL: String? = "http://44.235.233.29/Alqenaat/api/"
    val BASE_URL: String? = "https://alqenaat.com/api/"
    //val Photo_URL: String? = "http://44.235.233.29/Alqenaat/api/FileService/Upload?FileObjectType="
    val Photo_URL: String? = "https://alqenaat.com/api/FileService/Upload?FileObjectType="
}

