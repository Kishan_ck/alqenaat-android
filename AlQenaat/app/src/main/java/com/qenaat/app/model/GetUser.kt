package com.qenaat.app.model

/**
 * Created by DELL on 19-Nov-17.
 */
class GetUser {
    var Id: String? = null
    var UserName: String? = null
    var CivilId: String? = null
    var Gender: String? = null
    var Name: String? = null
    var Photo: String? = null
    var IsApproved: String? = null
    var Email: String? = null
    var IsIdentified: String? = null
    var Phone: String? = null
    var Password: String? = null
    var CountryCode: String? = null
    var IsFamily: String? = null
    var AlternateNo: String? = null
}