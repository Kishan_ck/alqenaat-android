package com.qenaat.app.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jdev.countryutil.Constants
import com.jdev.countryutil.CountryUtil
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util.Companion.getCountryByCode
import com.qenaat.app.Utils.Util.Companion.getUserCountryInfo
import com.qenaat.app.adapters.AddFamilyMemberAdapter
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.checkFixLength
import com.qenaat.app.classes.FixControl.returnStr
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.classesimport.SpacesItemDecoration
import com.qenaat.app.interfaces.Interfaces
import com.qenaat.app.model.RequestForHelp
import com.qenaat.app.model.RequestForHelpPersons
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.add_news.*
import okhttp3.ResponseBody


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by DELL on 14-Jan-18.
 */
class RequestHelpFormFragment : Fragment(), View.OnClickListener {
    lateinit var act: FragmentActivity
    lateinit var mLangSessionManager: LanguageSessionManager
    lateinit var mainLayout: RelativeLayout
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var my_recycler_view: RecyclerView
    lateinit var et_requset_name: EditText
    lateinit var et_requset_mobile: EditText
    lateinit var et_rent_amount: EditText
    lateinit var et_my_income_amount: EditText
    lateinit var et_husband_income_amount: EditText
    lateinit var et_wife_income_amount: EditText
    lateinit var et_employer: EditText
    lateinit var et_work_type: EditText
    lateinit var et_position: EditText
    lateinit var tv_requset_date: TextView
    lateinit var tv_owner_label: TextView
    lateinit var tv_rent_label: TextView
    lateinit var tv_marital_status: TextView
    lateinit var tv_married_label: TextView
    lateinit var tv_divorce_label: TextView
    lateinit var tv_not_married_label: TextView
    lateinit var tv_not_widowed_label: TextView
    lateinit var tv_person_in_family: TextView
    lateinit var tv_general_info: TextView
    lateinit var tv_my_income: TextView
    lateinit var tv_my_daily_income_label: TextView
    lateinit var tv_my_monthly_income_label: TextView
    lateinit var tv_husband_income: TextView
    lateinit var tv_husband_daily_income_label: TextView
    lateinit var tv_husband_monthly_income_label: TextView
    lateinit var tv_husband_yearly_income_label: TextView
    lateinit var tv_wife_income: TextView
    lateinit var tv_wife_daily_income_label: TextView
    lateinit var tv_wife_monthly_income_label: TextView
    lateinit var tv_wife_yearly_income_label: TextView
    lateinit var tv_other_income: TextView
    lateinit var tv_working_sons_label: TextView
    lateinit var tv_affairs_label: TextView
    lateinit var tv_expenses_label: TextView
    lateinit var tv_other_label: TextView
    lateinit var tv_short_request: TextView
    lateinit var tv_organisation_debt_label: TextView
    lateinit var tv_personal_debt_label: TextView
    lateinit var tv_residence_fine_label: TextView
    lateinit var tv_school_fees_label: TextView
    lateinit var tv_installment_label: TextView
    lateinit var tv_provisions_label: TextView
    lateinit var tv_treatement_label: TextView
    lateinit var tv_delayed_rent_label: TextView
    lateinit var tv_weak_income_label: TextView
    lateinit var tv_next: TextView
    lateinit var relative_home_details: LinearLayout
    lateinit var relative_owner: LinearLayout
    lateinit var relative_rent: LinearLayout
    lateinit var relative_marital_status: LinearLayout
    lateinit var relative_married: LinearLayout
    lateinit var relative_divorce: LinearLayout
    lateinit var relative_not_married: LinearLayout
    lateinit var relative_widowed: LinearLayout
    lateinit var relative_person_in_family: LinearLayout
    lateinit var relative_general_info: LinearLayout
    lateinit var relative_my_income: LinearLayout
    lateinit var relative_my_daily_income: LinearLayout
    lateinit var relative_my_monthly_income: LinearLayout
    lateinit var relative_husband_income: LinearLayout
    lateinit var relative_husband_daily_income: LinearLayout
    lateinit var relative_husband_monthly_income: LinearLayout
    lateinit var relative_husband_yearly_income: LinearLayout
    lateinit var relative_wife_income: LinearLayout
    lateinit var relative_wife_daily_income: LinearLayout
    lateinit var relative_wife_monthly_income: LinearLayout
    lateinit var relative_wife_yearly_income: LinearLayout
    lateinit var relative_other_income: LinearLayout
    lateinit var relative_working_sons: LinearLayout
    lateinit var relative_affairs: LinearLayout
    lateinit var relative_expenses: LinearLayout
    lateinit var relative_other: LinearLayout
    lateinit var relative_short_request: LinearLayout
    lateinit var relative_organisation_debt: LinearLayout
    lateinit var relative_personal_debt: LinearLayout
    lateinit var relative_residence_fine: LinearLayout
    lateinit var relative_school_fees: LinearLayout
    lateinit var relative_installment: LinearLayout
    lateinit var relative_provisions: LinearLayout
    lateinit var relative_treatement: LinearLayout
    lateinit var relative_delayed_rent: LinearLayout
    lateinit var relative_weak_income: LinearLayout
    lateinit var img_check_owner: ImageView
    lateinit var img_check_rent: ImageView
    lateinit var img_check_married: ImageView
    lateinit var img_check_divorce: ImageView
    lateinit var img_check_not_married: ImageView
    lateinit var img_check_widowed: ImageView
    lateinit var img_check_my_daily_income: ImageView
    lateinit var img_check_my_monthly_income: ImageView
    lateinit var img_check_husband_daily_income: ImageView
    lateinit var img_check_husband_monthly_income: ImageView
    lateinit var img_check_husband_yearly_income: ImageView
    lateinit var img_check_wife_daily_income: ImageView
    lateinit var img_check_wife_monthly_income: ImageView
    lateinit var img_check_wife_yearly_income: ImageView
    lateinit var img_check_working_sons: ImageView
    lateinit var img_check_affairs: ImageView
    lateinit var img_check_expenses: ImageView
    lateinit var img_check_other: ImageView
    lateinit var img_check_organisation_debt: ImageView
    lateinit var img_check_personal_debt: ImageView
    lateinit var img_check_residence_fine: ImageView
    lateinit var img_check_school_fees: ImageView
    lateinit var img_check_installment: ImageView
    lateinit var img_check_provisions: ImageView
    lateinit var img_check_streatement: ImageView
    lateinit var img_check_delayed_rent: ImageView
    lateinit var img_check_weak_income: ImageView
    lateinit var loading: ProgressBar
    var homeDetails = 0
    var maritalStatus = 0
    var myIncome = 0
    var husbandIncome = 0
    var wifeIncome = 0
    var workingSons = false
    var affairs = false
    var expenses = false
    var other = false
    var organisationDebt = false
    var personalDebt = false
    var residence = false
    var schoolFees = false
    var provisions = false
    var installment = false
    var treatment = false
    var delayedRent = false
    var weakIncome = false
    var requestName: String = ""
    var requestMobile: String = ""
    var rentAmount: String = ""
    var myIncomeAmount: String = ""
    var husbandIncomeAmount: String = ""
    var wifeIncomeAmount: String = ""
    var employer: String = ""
    var workType: String = ""
    var position: String = ""
    var isEdit = false
    var requestHelpForm: RequestForHelp = RequestForHelp()
    private var countryCode: String = ""
    private var countryFlag: Int = 0
    private var countryName: String = ""
    private var incomeList: MutableList<IncomeModel> = mutableListOf()
    private var shortRequestList: MutableList<IncomeModel> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        countryCode = getUserCountryInfo(act, ConstanstParameters.COUNTRY_CODE)
        countryName = getUserCountryInfo(act, ConstanstParameters.COUNTRY_NAME)
        countryFlag = getUserCountryInfo(act, ConstanstParameters.COUNTRY_FLAG).toInt()

        try {
            mLangSessionManager = LanguageSessionManager(act)
            mSessionManager = SessionManager(act)
            if (arguments != null) {
                if (arguments!!.containsKey("isEdit")) {
                    if (arguments!!.getString("isEdit").equals("1", ignoreCase = true)) {
                        isEdit = true
                        val gson = Gson()
                        if (arguments!!.containsKey("RequestForHelp")) {
                            requestHelpForm = gson.fromJson(arguments!!.getString("RequestForHelp"),
                                    RequestForHelp::class.java)
                            fillDataInVariables()
                        }
                    }
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.add_request_form, null) as RelativeLayout
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadIncomeList()
        loadShortRequestList()
        initViews(mainLayout)
    }

    private fun loadIncomeList() {
        incomeList.add(IncomeModel(1, getString(R.string.WorkingSonsLabel)))
        incomeList.add(IncomeModel(2, getString(R.string.AffairsLabel)))
        incomeList.add(IncomeModel(3, getString(R.string.ExpensesLabel)))
        incomeList.add(IncomeModel(4, getString(R.string.OtherLabel)))
    }

    private fun loadShortRequestList() {
        shortRequestList.add(IncomeModel(1, getString(R.string.OrganisationDebtLabel)))
        shortRequestList.add(IncomeModel(2, getString(R.string.PersonalDebtLabel)))
        shortRequestList.add(IncomeModel(3, getString(R.string.ResidenceFineLabel)))
        shortRequestList.add(IncomeModel(4, getString(R.string.SchoolFeesLabel)))
        shortRequestList.add(IncomeModel(5, getString(R.string.InstallmentLabel)))
        shortRequestList.add(IncomeModel(6, getString(R.string.ProvisionsLabel)))
        shortRequestList.add(IncomeModel(7, getString(R.string.TreatmentLabel)))
        shortRequestList.add(IncomeModel(8, getString(R.string.DelayedRentLabel)))
        shortRequestList.add(IncomeModel(9, getString(R.string.WeakIncomeLabel)))
    }

    fun initViews(mainLayout: RelativeLayout) {
        et_requset_name = mainLayout.findViewById(R.id.et_requset_name) as EditText
        et_requset_mobile = mainLayout.findViewById(R.id.et_requset_mobile) as EditText
        et_rent_amount = mainLayout.findViewById(R.id.et_rent_amount) as EditText
        et_my_income_amount = mainLayout.findViewById(R.id.et_my_income_amount) as EditText
        et_husband_income_amount = mainLayout.findViewById(R.id.et_husband_income_amount) as EditText
        et_wife_income_amount = mainLayout.findViewById(R.id.et_wife_income_amount) as EditText
        et_employer = mainLayout.findViewById(R.id.et_employer) as EditText
        et_work_type = mainLayout.findViewById(R.id.et_work_type) as EditText
        et_position = mainLayout.findViewById(R.id.et_position) as EditText
        tv_owner_label = mainLayout.findViewById(R.id.tv_owner_label) as TextView
        tv_requset_date = mainLayout.findViewById(R.id.tv_requset_date) as TextView
        tv_rent_label = mainLayout.findViewById(R.id.tv_rent_label) as TextView
        tv_marital_status = mainLayout.findViewById(R.id.tv_marital_status) as TextView
        tv_married_label = mainLayout.findViewById(R.id.tv_married_label) as TextView
        tv_divorce_label = mainLayout.findViewById(R.id.tv_divorce_label) as TextView
        tv_not_married_label = mainLayout.findViewById(R.id.tv_not_married_label) as TextView
        tv_not_widowed_label = mainLayout.findViewById(R.id.tv_not_widowed_label) as TextView
        tv_person_in_family = mainLayout.findViewById(R.id.tv_person_in_family) as TextView
        tv_general_info = mainLayout.findViewById(R.id.tv_general_info) as TextView
        tv_my_income = mainLayout.findViewById(R.id.tv_my_income) as TextView
        tv_my_daily_income_label = mainLayout.findViewById(R.id.tv_my_daily_income_label) as TextView
        tv_my_monthly_income_label = mainLayout.findViewById(R.id.tv_my_monthly_income_label) as TextView
        tv_husband_income = mainLayout.findViewById(R.id.tv_husband_income) as TextView
        tv_husband_daily_income_label = mainLayout.findViewById(R.id.tv_husband_daily_income_label) as TextView
        tv_husband_monthly_income_label = mainLayout.findViewById(R.id.tv_husband_monthly_income_label) as TextView
        tv_husband_yearly_income_label = mainLayout.findViewById(R.id.tv_husband_yearly_income_label) as TextView
        tv_wife_income = mainLayout.findViewById(R.id.tv_wife_income) as TextView
        tv_wife_daily_income_label = mainLayout.findViewById(R.id.tv_wife_daily_income_label) as TextView
        tv_wife_monthly_income_label = mainLayout.findViewById(R.id.tv_wife_monthly_income_label) as TextView
        tv_wife_yearly_income_label = mainLayout.findViewById(R.id.tv_wife_yearly_income_label) as TextView
        tv_other_income = mainLayout.findViewById(R.id.tv_other_income) as TextView
        tv_working_sons_label = mainLayout.findViewById(R.id.tv_working_sons_label) as TextView
        tv_affairs_label = mainLayout.findViewById(R.id.tv_affairs_label) as TextView
        tv_expenses_label = mainLayout.findViewById(R.id.tv_expenses_label) as TextView
        tv_other_label = mainLayout.findViewById(R.id.tv_other_label) as TextView
        tv_short_request = mainLayout.findViewById(R.id.tv_short_request) as TextView
        tv_organisation_debt_label = mainLayout.findViewById(R.id.tv_organisation_debt_label) as TextView
        tv_personal_debt_label = mainLayout.findViewById(R.id.tv_personal_debt_label) as TextView
        tv_residence_fine_label = mainLayout.findViewById(R.id.tv_residence_fine_label) as TextView
        tv_school_fees_label = mainLayout.findViewById(R.id.tv_school_fees_label) as TextView
        tv_installment_label = mainLayout.findViewById(R.id.tv_installment_label) as TextView
        tv_provisions_label = mainLayout.findViewById(R.id.tv_provisions_label) as TextView
        tv_treatement_label = mainLayout.findViewById(R.id.tv_treatement_label) as TextView
        tv_delayed_rent_label = mainLayout.findViewById(R.id.tv_delayed_rent_label) as TextView
        tv_weak_income_label = mainLayout.findViewById(R.id.tv_weak_income_label) as TextView
        tv_next = mainLayout.findViewById(R.id.tv_next) as TextView
        tv_next.setOnClickListener(this)
        relative_home_details = mainLayout.findViewById(R.id.relative_home_details) as LinearLayout
        relative_owner = mainLayout.findViewById(R.id.relative_owner) as LinearLayout
        relative_owner.setOnClickListener(this)
        relative_rent = mainLayout.findViewById(R.id.relative_rent) as LinearLayout
        relative_rent.setOnClickListener(this)
        relative_marital_status = mainLayout.findViewById(R.id.relative_marital_status) as LinearLayout
        relative_married = mainLayout.findViewById(R.id.relative_married) as LinearLayout
        relative_married.setOnClickListener(this)
        relative_divorce = mainLayout.findViewById(R.id.relative_divorce) as LinearLayout
        relative_divorce.setOnClickListener(this)
        relative_not_married = mainLayout.findViewById(R.id.relative_not_married) as LinearLayout
        relative_not_married.setOnClickListener(this)
        relative_widowed = mainLayout.findViewById(R.id.relative_widowed) as LinearLayout
        relative_widowed.setOnClickListener(this)
        relative_person_in_family = mainLayout.findViewById(R.id.relative_person_in_family) as LinearLayout
        relative_general_info = mainLayout.findViewById(R.id.relative_general_info) as LinearLayout
        relative_my_income = mainLayout.findViewById(R.id.relative_my_income) as LinearLayout
        relative_my_daily_income = mainLayout.findViewById(R.id.relative_my_daily_income) as LinearLayout
        relative_my_daily_income.setOnClickListener(this)
        relative_my_monthly_income = mainLayout.findViewById(R.id.relative_my_monthly_income) as LinearLayout
        relative_my_monthly_income.setOnClickListener(this)
        relative_husband_income = mainLayout.findViewById(R.id.relative_husband_income) as LinearLayout
        relative_husband_daily_income = mainLayout.findViewById(R.id.relative_husband_daily_income) as LinearLayout
        relative_husband_daily_income.setOnClickListener(this)
        relative_husband_monthly_income = mainLayout.findViewById(R.id.relative_husband_monthly_income) as LinearLayout
        relative_husband_monthly_income.setOnClickListener(this)
        relative_husband_yearly_income = mainLayout.findViewById(R.id.relative_husband_yearly_income) as LinearLayout
        relative_husband_yearly_income.setOnClickListener(this)
        relative_wife_income = mainLayout.findViewById(R.id.relative_wife_income) as LinearLayout
        relative_wife_daily_income = mainLayout.findViewById(R.id.relative_wife_daily_income) as LinearLayout
        relative_wife_daily_income.setOnClickListener(this)
        relative_wife_monthly_income = mainLayout.findViewById(R.id.relative_wife_monthly_income) as LinearLayout
        relative_wife_monthly_income.setOnClickListener(this)
        relative_wife_yearly_income = mainLayout.findViewById(R.id.relative_wife_yearly_income) as LinearLayout
        relative_wife_yearly_income.setOnClickListener(this)
        relative_other_income = mainLayout.findViewById(R.id.relative_other_income) as LinearLayout
        relative_working_sons = mainLayout.findViewById(R.id.relative_working_sons) as LinearLayout
        relative_working_sons.setOnClickListener(this)
        relative_affairs = mainLayout.findViewById(R.id.relative_affairs) as LinearLayout
        relative_affairs.setOnClickListener(this)
        relative_expenses = mainLayout.findViewById(R.id.relative_expenses) as LinearLayout
        relative_expenses.setOnClickListener(this)
        relative_other = mainLayout.findViewById(R.id.relative_other) as LinearLayout
        relative_other.setOnClickListener(this)
        relative_short_request = mainLayout.findViewById(R.id.relative_short_request) as LinearLayout
        relative_organisation_debt = mainLayout.findViewById(R.id.relative_organisation_debt) as LinearLayout
        relative_organisation_debt.setOnClickListener(this)
        relative_personal_debt = mainLayout.findViewById(R.id.relative_personal_debt) as LinearLayout
        relative_personal_debt.setOnClickListener(this)
        relative_residence_fine = mainLayout.findViewById(R.id.relative_residence_fine) as LinearLayout
        relative_residence_fine.setOnClickListener(this)
        relative_school_fees = mainLayout.findViewById(R.id.relative_school_fees) as LinearLayout
        relative_school_fees.setOnClickListener(this)
        relative_installment = mainLayout.findViewById(R.id.relative_installment) as LinearLayout
        relative_installment.setOnClickListener(this)
        relative_provisions = mainLayout.findViewById(R.id.relative_provisions) as LinearLayout
        relative_provisions.setOnClickListener(this)
        relative_treatement = mainLayout.findViewById(R.id.relative_treatement) as LinearLayout
        relative_treatement.setOnClickListener(this)
        relative_delayed_rent = mainLayout.findViewById(R.id.relative_delayed_rent) as LinearLayout
        relative_delayed_rent.setOnClickListener(this)
        relative_weak_income = mainLayout.findViewById(R.id.relative_weak_income) as LinearLayout
        relative_weak_income.setOnClickListener(this)
        img_check_owner = mainLayout.findViewById(R.id.img_check_owner) as ImageView
        img_check_rent = mainLayout.findViewById(R.id.img_check_rent) as ImageView
        img_check_married = mainLayout.findViewById(R.id.img_check_married) as ImageView
        img_check_divorce = mainLayout.findViewById(R.id.img_check_divorce) as ImageView
        img_check_not_married = mainLayout.findViewById(R.id.img_check_not_married) as ImageView
        img_check_widowed = mainLayout.findViewById(R.id.img_check_widowed) as ImageView
        img_check_my_daily_income = mainLayout.findViewById(R.id.img_check_my_daily_income) as ImageView
        img_check_my_monthly_income = mainLayout.findViewById(R.id.img_check_my_monthly_income) as ImageView
        img_check_husband_daily_income = mainLayout.findViewById(R.id.img_check_husband_daily_income) as ImageView
        img_check_husband_monthly_income = mainLayout.findViewById(R.id.img_check_husband_monthly_income) as ImageView
        img_check_husband_yearly_income = mainLayout.findViewById(R.id.img_check_husband_yearly_income) as ImageView
        img_check_wife_daily_income = mainLayout.findViewById(R.id.img_check_wife_daily_income) as ImageView
        img_check_wife_monthly_income = mainLayout.findViewById(R.id.img_check_wife_monthly_income) as ImageView
        img_check_wife_yearly_income = mainLayout.findViewById(R.id.img_check_wife_yearly_income) as ImageView
        img_check_working_sons = mainLayout.findViewById(R.id.img_check_working_sons) as ImageView
        img_check_affairs = mainLayout.findViewById(R.id.img_check_affairs) as ImageView
        img_check_expenses = mainLayout.findViewById(R.id.img_check_expenses) as ImageView
        img_check_other = mainLayout.findViewById(R.id.img_check_other) as ImageView
        img_check_organisation_debt = mainLayout.findViewById(R.id.img_check_organisation_debt) as ImageView
        img_check_personal_debt = mainLayout.findViewById(R.id.img_check_personal_debt) as ImageView
        img_check_residence_fine = mainLayout.findViewById(R.id.img_check_residence_fine) as ImageView
        img_check_school_fees = mainLayout.findViewById(R.id.img_check_school_fees) as ImageView
        img_check_installment = mainLayout.findViewById(R.id.img_check_installment) as ImageView
        img_check_provisions = mainLayout.findViewById(R.id.img_check_provisions) as ImageView
        img_check_streatement = mainLayout.findViewById(R.id.img_check_streatement) as ImageView
        img_check_delayed_rent = mainLayout.findViewById(R.id.img_check_delayed_rent) as ImageView
        img_check_weak_income = mainLayout.findViewById(R.id.img_check_weak_income) as ImageView
        loading = mainLayout.findViewById(R.id.loading) as ProgressBar
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
        my_recycler_view.addItemDecoration(SpacesItemDecoration(30))
        my_recycler_view.setNestedScrollingEnabled(false)
        ContentActivity.Companion.setTextFonts(mainLayout)
        if (isEdit) {
        } else {
            var addFamilyMembers: RequestForHelpPersons? = RequestForHelpPersons()
            addFamilyMembers?.Id = "0"
            familyMembersArrayList?.add(addFamilyMembers)
            addFamilyMembers = RequestForHelpPersons()
            addFamilyMembers.Id = "-1"
            familyMembersArrayList?.add(addFamilyMembers)
        }
        LL_country_code.setOnClickListener(this)

        tv_country_code.isSelected = true
        tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
        iv_country_flag.setImageResource(countryFlag)

        if (isEdit)
            setData()
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.enableLogin(mLangSessionManager)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.RequestHelpLabel))
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        mAdapter = AddFamilyMemberAdapter(act, familyMembersArrayList)
        my_recycler_view.setAdapter(mAdapter)
        //setData()
        et_requset_name.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_requset_name.requestFocus()
                et_requset_name.setSelection(et_requset_name.getText().length)
                if (et_requset_name.getText().toString().equals("", ignoreCase = true)) {
                    requestName = ""
                } else {

                    //update data here
                    requestName = et_requset_name.getText().toString()
                }
            }
        })
        et_requset_mobile.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_requset_mobile.requestFocus()
                et_requset_mobile.setSelection(et_requset_mobile.getText().length)
                if (et_requset_mobile.getText().toString().equals("", ignoreCase = true)) {
                    requestMobile = ""
                } else {

                    //update data here
                    requestMobile = et_requset_mobile.getText().toString()
                }
            }
        })
        et_rent_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_rent_amount.requestFocus()
                et_rent_amount.setSelection(et_rent_amount.getText().length)
                if (et_rent_amount.getText().toString().equals("", ignoreCase = true)) {
                    rentAmount = ""
                } else {

                    //update data here
                    rentAmount = et_rent_amount.getText().toString()
                }
            }
        })
        et_my_income_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_my_income_amount.requestFocus()
                et_my_income_amount.setSelection(et_my_income_amount.getText().length)
                if (et_my_income_amount.getText().toString().equals("", ignoreCase = true)) {
                } else {

                    //update data here
                    myIncomeAmount = et_my_income_amount.getText().toString()
                }
            }
        })
        et_husband_income_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_husband_income_amount.requestFocus()
                et_husband_income_amount.setSelection(et_husband_income_amount.getText().length)
                if (et_husband_income_amount.getText().toString().equals("", ignoreCase = true)) {
                } else {

                    //update data here
                    husbandIncomeAmount = et_husband_income_amount.getText().toString()
                }
            }
        })
        et_wife_income_amount.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_wife_income_amount.requestFocus()
                et_wife_income_amount.setSelection(et_wife_income_amount.getText().length)
                if (et_wife_income_amount.getText().toString().equals("", ignoreCase = true)) {
                } else {

                    //update data here
                    wifeIncomeAmount = et_wife_income_amount.getText().toString()
                }
            }
        })
        et_employer.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_employer.requestFocus()
                et_employer.setSelection(et_employer.getText().length)
                if (et_employer.getText().toString().equals("", ignoreCase = true)) {
                    employer = ""
                } else {

                    //update data here
                    employer = et_employer.getText().toString()
                }
            }
        })
        et_work_type.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_work_type.requestFocus()
                et_work_type.setSelection(et_work_type.getText().length)
                if (et_work_type.getText().toString().equals("", ignoreCase = true)) {
                    workType = ""
                } else {

                    //update data here
                    workType = et_work_type.getText().toString()
                }
            }
        })
        et_position.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                Log.e("afterTextChanged", "afterTextChanged")
                et_position.requestFocus()
                et_position.setSelection(et_position.getText().length)
                if (et_position.getText().toString().equals("", ignoreCase = true)) {
                    position = ""
                } else {

                    //update data here
                    position = et_position.getText().toString()
                }
            }
        })
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.LL_country_code -> {
                CountryUtil(act).setTitle("").build()
            }
            R.id.tv_next -> {
                if (isValid())
                    RequestHelpForm()
            }
            R.id.relative_owner -> setOwner()
            R.id.relative_rent -> setRent()
            R.id.relative_married -> setMarried()
            R.id.relative_divorce -> setDivorce()
            R.id.relative_not_married -> setNotMarried()
            R.id.relative_widowed -> setWidowed()
            R.id.relative_my_daily_income -> setMyDailyIncome()
            R.id.relative_my_monthly_income -> setMyMonthlyIncome()
            R.id.relative_husband_daily_income -> setHusbandDailyIncome()
            R.id.relative_husband_monthly_income -> setHusbandMonthlyIncome()
            R.id.relative_husband_yearly_income -> setHusbandYearlyIncome()
            R.id.relative_wife_daily_income -> setWifeDailyIncome()
            R.id.relative_wife_monthly_income -> setWifeMonthlyIncome()
            R.id.relative_wife_yearly_income -> setWifeYearlyIncome()
            R.id.relative_working_sons -> {
                workingSons = !workingSons
                setWorkingSons()
            }
            R.id.relative_affairs -> {
                affairs = !affairs
                setAffairs()
            }
            R.id.relative_expenses -> {
                expenses = !expenses
                setExpenses()
            }
            R.id.relative_other -> {
                other = !other
                setOther()
            }
            R.id.relative_organisation_debt -> {
                organisationDebt = !organisationDebt
                setOrganisationDebt()
            }
            R.id.relative_personal_debt -> {
                personalDebt = !personalDebt
                setPersonalDebt()
            }
            R.id.relative_residence_fine -> {
                residence = !residence
                setResidenceDebt()
            }
            R.id.relative_school_fees -> {
                schoolFees = !schoolFees
                setSchoolFees()
            }
            R.id.relative_installment -> {
                installment = !installment
                setInstallment()
            }
            R.id.relative_provisions -> {
                provisions = !provisions
                setProvisions()
            }
            R.id.relative_treatement -> {
                treatment = !treatment
                setTreatment()
            }
            R.id.relative_delayed_rent -> {
                delayedRent = !delayedRent
                setDelayedRent()
            }
            R.id.relative_weak_income -> {
                weakIncome = !weakIncome
                setWeakIncome()
            }
        }
    }

    private fun setOwner() {
        img_check_owner.setImageResource(R.drawable.checked)
        img_check_rent.setImageResource(R.drawable.check)
        et_rent_amount.setAlpha(0.4f)
        et_rent_amount.setEnabled(false)
        et_rent_amount.setText("")
        homeDetails = 1
        requestHelpForm.HomeType = homeDetails.toString() + ""
    }

    private fun setRent() {
        img_check_owner.setImageResource(R.drawable.check)
        img_check_rent.setImageResource(R.drawable.checked)
        et_rent_amount.setAlpha(1.0f)
        et_rent_amount.setEnabled(true)
        //et_rent_amount.setText(rentAmount)
        homeDetails = 2
        requestHelpForm.HomeType = homeDetails.toString() + ""
    }

    private fun setMarried() {
        img_check_married.setImageResource(R.drawable.checked)
        img_check_divorce.setImageResource(R.drawable.check)
        img_check_not_married.setImageResource(R.drawable.check)
        img_check_widowed.setImageResource(R.drawable.check)
        maritalStatus = 1
        requestHelpForm.MaritalStatus = maritalStatus.toString() + ""
    }

    private fun setDivorce() {
        img_check_married.setImageResource(R.drawable.check)
        img_check_divorce.setImageResource(R.drawable.checked)
        img_check_not_married.setImageResource(R.drawable.check)
        img_check_widowed.setImageResource(R.drawable.check)
        maritalStatus = 2
        requestHelpForm.MaritalStatus = maritalStatus.toString() + ""
    }

    private fun setNotMarried() {
        img_check_married.setImageResource(R.drawable.check)
        img_check_divorce.setImageResource(R.drawable.check)
        img_check_not_married.setImageResource(R.drawable.checked)
        img_check_widowed.setImageResource(R.drawable.check)
        maritalStatus = 3
        requestHelpForm.MaritalStatus = maritalStatus.toString() + ""
    }

    private fun setWidowed() {
        img_check_married.setImageResource(R.drawable.check)
        img_check_divorce.setImageResource(R.drawable.check)
        img_check_not_married.setImageResource(R.drawable.check)
        img_check_widowed.setImageResource(R.drawable.checked)
        maritalStatus = 4
        requestHelpForm.MaritalStatus = maritalStatus.toString() + ""
    }

    private fun setMyDailyIncome() {
        img_check_my_daily_income.setImageResource(R.drawable.checked)
        img_check_my_monthly_income.setImageResource(R.drawable.check)
        myIncome = 1
        requestHelpForm.MyIncomeType = myIncome.toString() + ""
        if (myIncomeAmount.isNotEmpty())
            et_my_income_amount.setText(myIncomeAmount)
    }

    private fun setMyMonthlyIncome() {
        img_check_my_daily_income.setImageResource(R.drawable.check)
        img_check_my_monthly_income.setImageResource(R.drawable.checked)
        myIncome = 2
        requestHelpForm.MyIncomeType = myIncome.toString() + ""
        if (myIncomeAmount.isNotEmpty())
            et_my_income_amount.setText(myIncomeAmount)
    }

    private fun setHusbandDailyIncome() {
        img_check_husband_daily_income.setImageResource(R.drawable.checked)
        img_check_husband_monthly_income.setImageResource(R.drawable.check)
        img_check_husband_yearly_income.setImageResource(R.drawable.check)
        husbandIncome = 1
        requestHelpForm.HusbandIncomeType = husbandIncome.toString() + ""
        if (husbandIncomeAmount.isNotEmpty())
            et_husband_income_amount.setText(husbandIncomeAmount)
    }

    private fun setHusbandMonthlyIncome() {
        img_check_husband_daily_income.setImageResource(R.drawable.check)
        img_check_husband_monthly_income.setImageResource(R.drawable.checked)
        img_check_husband_yearly_income.setImageResource(R.drawable.check)
        husbandIncome = 2
        requestHelpForm.HusbandIncomeType = husbandIncome.toString() + ""
        if (husbandIncomeAmount.isNotEmpty())
            et_husband_income_amount.setText(husbandIncomeAmount)
    }

    private fun setHusbandYearlyIncome() {
        img_check_husband_daily_income.setImageResource(R.drawable.check)
        img_check_husband_monthly_income.setImageResource(R.drawable.check)
        img_check_husband_yearly_income.setImageResource(R.drawable.checked)
        husbandIncome = 3
        requestHelpForm.HusbandIncomeType = husbandIncome.toString() + ""
        if (husbandIncomeAmount.isNotEmpty())
            et_husband_income_amount.setText(husbandIncomeAmount)
    }

    private fun setWifeDailyIncome() {
        img_check_wife_daily_income.setImageResource(R.drawable.checked)
        img_check_wife_monthly_income.setImageResource(R.drawable.check)
        img_check_wife_yearly_income.setImageResource(R.drawable.check)
        wifeIncome = 1
        requestHelpForm.WifeIncomeType = wifeIncome.toString() + ""
        if (wifeIncomeAmount.isNotEmpty())
            et_wife_income_amount.setText(wifeIncomeAmount)
    }

    private fun setWifeMonthlyIncome() {
        img_check_wife_daily_income.setImageResource(R.drawable.check)
        img_check_wife_monthly_income.setImageResource(R.drawable.checked)
        img_check_wife_yearly_income.setImageResource(R.drawable.check)
        wifeIncome = 2
        requestHelpForm.WifeIncomeType = wifeIncome.toString() + ""
        if (wifeIncomeAmount.isNotEmpty())
            et_wife_income_amount.setText(wifeIncomeAmount)
    }

    private fun setWifeYearlyIncome() {
        img_check_wife_daily_income.setImageResource(R.drawable.check)
        img_check_wife_monthly_income.setImageResource(R.drawable.check)
        img_check_wife_yearly_income.setImageResource(R.drawable.checked)
        wifeIncome = 3
        requestHelpForm.WifeIncomeType = wifeIncome.toString() + ""
        if (wifeIncomeAmount.isNotEmpty())
            et_wife_income_amount.setText(wifeIncomeAmount)
    }

    private fun setWorkingSons() {
        if (workingSons) {
            img_check_working_sons.setImageResource(R.drawable.checked)
        } else {
            img_check_working_sons.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsWorkingSons = workingSons.toString() + ""
    }

    private fun setAffairs() {
        if (affairs) {
            img_check_affairs.setImageResource(R.drawable.checked)
        } else {
            img_check_affairs.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsAffairs = affairs.toString() + ""
    }

    private fun setExpenses() {
        if (expenses) {
            img_check_expenses.setImageResource(R.drawable.checked)
        } else {
            img_check_expenses.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsExpenses = expenses.toString() + ""
    }

    private fun setOrganisationDebt() {
        if (organisationDebt) {
            img_check_organisation_debt.setImageResource(R.drawable.checked)
        } else {
            img_check_organisation_debt.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsOrganisationDebt = organisationDebt.toString() + ""
    }

    private fun setPersonalDebt() {
        if (personalDebt) {
            img_check_personal_debt.setImageResource(R.drawable.checked)
        } else {
            img_check_personal_debt.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsPersonalDebt = personalDebt.toString() + ""
    }

    private fun setResidenceDebt() {
        if (residence) {
            img_check_residence_fine.setImageResource(R.drawable.checked)
        } else {
            img_check_residence_fine.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsResidence = residence.toString() + ""
    }

    private fun setSchoolFees() {
        if (schoolFees) {
            img_check_school_fees.setImageResource(R.drawable.checked)
        } else {
            img_check_school_fees.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsSchoolFees = schoolFees.toString() + ""
    }

    private fun setInstallment() {
        if (installment) {
            img_check_installment.setImageResource(R.drawable.checked)
        } else {
            img_check_installment.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsInstallment = installment.toString() + ""
    }

    private fun setProvisions() {
        if (provisions) {
            img_check_provisions.setImageResource(R.drawable.checked)
        } else {
            img_check_provisions.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsProvisions = provisions.toString() + ""
    }

    private fun setTreatment() {
        if (treatment) {
            img_check_streatement.setImageResource(R.drawable.checked)
        } else {
            img_check_streatement.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsTreatment = treatment.toString() + ""
    }

    private fun setDelayedRent() {
        if (delayedRent) {
            img_check_delayed_rent.setImageResource(R.drawable.checked)
        } else {
            img_check_delayed_rent.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsDelayedRent = delayedRent.toString() + ""
    }

    private fun setWeakIncome() {
        if (weakIncome) {
            img_check_weak_income.setImageResource(R.drawable.checked)
        } else {
            img_check_weak_income.setImageResource(R.drawable.check)
        }
        requestHelpForm.IsWeakIncome = weakIncome.toString() + ""
    }

    private fun setData() {
        if (homeDetails == 1) {
            setOwner()
        }
        if (homeDetails == 2) {
            setRent()
        }
        if (maritalStatus == 1) {
            setMarried()
        }
        if (maritalStatus == 2) {
            setDivorce()
        }
        if (maritalStatus == 3) {
            setNotMarried()
        }
        if (maritalStatus == 4) {
            setWidowed()
        }
        if (myIncome == 1) {
            setMyDailyIncome()
        }
        if (myIncome == 2) {
            setMyMonthlyIncome()
        }
        if (husbandIncome == 1) {
            setHusbandDailyIncome()
        }
        if (husbandIncome == 2) {
            setHusbandMonthlyIncome()
        }
        if (husbandIncome == 3) {
            setHusbandYearlyIncome()
        }
        if (wifeIncome == 1) {
            setWifeDailyIncome()
        }
        if (wifeIncome == 2) {
            setWifeMonthlyIncome()
        }
        if (wifeIncome == 3) {
            setWifeYearlyIncome()
        }
        setWorkingSons()
        setAffairs()
        setExpenses()
        setOther()
        setOrganisationDebt()
        setPersonalDebt()
        setResidenceDebt()
        setSchoolFees()
        setInstallment()
        setProvisions()
        setTreatment()
        setDelayedRent()
        setWeakIncome()
        if (requestName.length > 0) {
            et_requset_name.setText(requestName)
        }
        if (requestMobile.length > 0) {
            et_requset_mobile.setText(requestMobile)
        }
        val country = getCountryByCode(act, requestHelpForm.CountryCode!!)
        countryCode = country.dialCode
        countryName = country.code
        countryFlag = country.flag
        tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
        iv_country_flag.setImageResource(countryFlag)
        if (homeDetails == 2) {
            if (rentAmount.length > 0) {
                et_rent_amount.setText(rentAmount)
            }
        }
        if (employer.length > 0) {
            et_employer.setText(employer)
        }
        if (workType.length > 0) {
            et_work_type.setText(workType)
        }
        if (position.length > 0) {
            et_position.setText(position)
        }
    }

    private fun setOther() {
        if (other) {
            img_check_other.setImageResource(R.drawable.checked)
        } else {
            img_check_other.setImageResource(R.drawable.check)
        }
    }

    private fun fillDataInVariables() {
        if (requestHelpForm != null) {
            try {
                homeDetails = requestHelpForm.HomeType?.toInt()!!
                maritalStatus = requestHelpForm.MaritalStatus!!.toInt()
                myIncome = requestHelpForm.MyIncomeType!!.toInt()
                husbandIncome = requestHelpForm.HusbandIncomeType!!.toInt()
                wifeIncome = requestHelpForm.WifeIncomeType!!.toInt()
            } catch (e: Exception) {
            }
            if (requestHelpForm.IncomeSourceIds!!.contains("1")) {
                workingSons = true
            }
            if (requestHelpForm.IncomeSourceIds!!.contains("2")) {
                affairs = true
            }
            if (requestHelpForm.IncomeSourceIds!!.contains("3")) {
                expenses = true
            }
            if (requestHelpForm.IncomeSourceIds!!.contains("4")) {
                other = true
            }
            if (requestHelpForm.RequestReasons!!.contains("1")) {
                organisationDebt = true
            }
            if (requestHelpForm.RequestReasons!!.contains("2")) {
                personalDebt = true
            }
            if (requestHelpForm.RequestReasons!!.contains("3")) {
                residence = true
            }
            if (requestHelpForm.RequestReasons!!.contains("4")) {
                schoolFees = true
            }
            if (requestHelpForm.RequestReasons!!.contains("5")) {
                installment = true
            }
            if (requestHelpForm.RequestReasons!!.contains("6")) {
                provisions = true
            }
            if (requestHelpForm.RequestReasons!!.contains("7")) {
                treatment = true
            }
            if (requestHelpForm.RequestReasons!!.contains("8")) {
                delayedRent = true
            }
            if (requestHelpForm.RequestReasons!!.contains("9")) {
                weakIncome = true
            }
            familyMembersArrayList?.clear()
            if (requestHelpForm.requestForHelpPersonslist != null) {
                familyMembersArrayList?.addAll(requestHelpForm.requestForHelpPersonslist!!)
            }
            val addFamilyMembers = RequestForHelpPersons()
            addFamilyMembers.Id = "-1"
            familyMembersArrayList?.add(addFamilyMembers)
            requestName = requestHelpForm.RequestName!!
            requestMobile = requestHelpForm.MobileNumber!!
            rentAmount = requestHelpForm.RentAmount!!
            myIncomeAmount = requestHelpForm.MyIncomeAmount!!
            husbandIncomeAmount = requestHelpForm.HusbandIncomeAmount!!
            wifeIncomeAmount = requestHelpForm.WifeIncomeAmount!!
            employer = requestHelpForm.Employer!!
            workType = requestHelpForm.WorkType!!
            position = requestHelpForm.Position!!
        }
    }

    private fun createHelpObject(): JsonObject {
        val jsonObject = JsonObject()
        if (isEdit)
            jsonObject.addProperty("requestForHelpId", requestHelpForm.requestForHelpId)

        jsonObject.addProperty("UserId", mSessionManager.getUserCode())
        jsonObject.addProperty("RequestName", requestName)
        jsonObject.addProperty("MobileNumber", requestMobile)
        jsonObject.addProperty("CountryCode", countryCode)
        jsonObject.addProperty("HomeType", homeDetails.toString())
        jsonObject.addProperty("RentAmount", rentAmount)
        jsonObject.addProperty("MaritalStatus", maritalStatus.toString())
        jsonObject.addProperty("MyIncomeType", myIncome.toString())
        jsonObject.addProperty("MyIncomeAmount", if (myIncomeAmount.length > 0) myIncomeAmount else "0")
        jsonObject.addProperty("HusbandIncomeType", husbandIncome.toString())
        jsonObject.addProperty("HusbandIncomeAmount", if (husbandIncomeAmount.length > 0) husbandIncomeAmount else "0")
        jsonObject.addProperty("WifeIncomeType", wifeIncome.toString())
        jsonObject.addProperty("WifeIncomeAmount", if (wifeIncomeAmount.length > 0) wifeIncomeAmount else "0")
        jsonObject.addProperty("Employer", employer)
        jsonObject.addProperty("WorkType", workType)
        jsonObject.addProperty("Position", position)
        var incomeSourceIds = ""
        if (workingSons) {
            incomeSourceIds = incomeSourceIds + "1"
        }
        if (affairs) {
            incomeSourceIds = if (incomeSourceIds.length > 0) {
                "$incomeSourceIds,2"
            } else {
                incomeSourceIds + "2"
            }
        }
        if (expenses) {
            incomeSourceIds = if (incomeSourceIds.length > 0) {
                "$incomeSourceIds,3"
            } else {
                incomeSourceIds + "3"
            }
        }
        if (other) {
            incomeSourceIds = if (incomeSourceIds.length > 0) {
                "$incomeSourceIds,4"
            } else {
                incomeSourceIds + "4"
            }
        }
        jsonObject.addProperty("IncomeSourceIds", incomeSourceIds)

        val incomeArray = JsonArray()
        for (i in incomeList.indices) {
            if (incomeSourceIds.contains(incomeList[i].id.toString())) {
                val incomeObject = JsonObject()
                incomeObject.addProperty("IncomeSourceEN", incomeList[i].item)
                incomeObject.addProperty("IncomeSourceAR", incomeList[i].item)
                incomeArray.add(incomeObject)
            }
        }
        jsonObject.add("incomeSourcesList", incomeArray)

        var requestForReason = ""
        if (organisationDebt) {
            requestForReason = requestForReason + "1"
        }
        if (personalDebt) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,2"
            } else {
                requestForReason + "2"
            }
        }
        if (residence) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,3"
            } else {
                requestForReason + "3"
            }
        }
        if (schoolFees) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,4"
            } else {
                requestForReason + "4"
            }
        }
        if (installment) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,5"
            } else {
                requestForReason + "5"
            }
        }
        if (provisions) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,6"
            } else {
                requestForReason + "6"
            }
        }
        if (treatment) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,7"
            } else {
                requestForReason + "7"
            }
        }
        if (delayedRent) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,8"
            } else {
                requestForReason + "8"
            }
        }
        if (weakIncome) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,9"
            } else {
                requestForReason + "9"
            }
        }
        jsonObject.addProperty("RequestReasons", requestForReason)

        val shortRequestArray = JsonArray()
        for (i in shortRequestList.indices) {
            if (requestForReason.contains(shortRequestList[i].id.toString())) {
                val shortRequestObject = JsonObject()
                shortRequestObject.addProperty("ReasonEN", shortRequestList[i].item)
                shortRequestObject.addProperty("ReasonAR", shortRequestList[i].item)
                shortRequestArray.add(shortRequestObject)
            }
        }
        jsonObject.add("requestReasonsList", shortRequestArray)

        val list: ArrayList<RequestForHelpPersons?> = ArrayList<RequestForHelpPersons?>()
        val familyArray = JsonArray()
        for (requestForHelpPersons in familyMembersArrayList!!) {
            if (requestForHelpPersons!!.Id.equals("-1", ignoreCase = true)) {
            } else {
                if (requestForHelpPersons.Gender == null && requestForHelpPersons.Notes == null
                        && requestForHelpPersons.CivilIdNo == null
                        && requestForHelpPersons.DateOFBirth == null
                        && requestForHelpPersons.Name == null
                        && requestForHelpPersons.RelationShip == null) {
                } else {
                    //list.add(requestForHelpPersons)
                    val familyObject = JsonObject()
                    /*familyObject.addProperty("Id", requestForHelpPersons.Id)
                    familyObject.addProperty("RequestForHelpId", requestForHelpPersons.RequestForHelpId)*/
                    familyObject.addProperty("Name", requestForHelpPersons.Name.returnStr())
                    familyObject.addProperty("Gender", requestForHelpPersons.Gender.returnStr())
                    familyObject.addProperty("DateOFBirth", requestForHelpPersons.DateOFBirth.returnStr())
                    familyObject.addProperty("CivilIdNo", requestForHelpPersons.CivilIdNo.returnStr())
                    familyObject.addProperty("RelationShip", requestForHelpPersons.RelationShip.returnStr())
                    familyObject.addProperty("Notes", requestForHelpPersons.Notes.returnStr())
                    familyArray.add(familyObject)
                }
            }
        }

        jsonObject.add("requestForHelpPersonslist", familyArray)

        Log.e("Rohann:::", "" + jsonObject.toString());


        return jsonObject
    }

    private fun createObject() {
        if (isEdit) {
        } else {
            requestHelpForm.requestForHelpId = "0"
        }
        requestHelpForm.UserId = mSessionManager.getUserCode()
        requestHelpForm.HomeType = homeDetails.toString() + ""
        requestHelpForm.MaritalStatus = maritalStatus.toString() + ""
        requestHelpForm.MyIncomeType = myIncome.toString() + ""
        requestHelpForm.HusbandIncomeType = husbandIncome.toString() + ""
        requestHelpForm.WifeIncomeType = wifeIncome.toString() + ""
        requestHelpForm.MyIncomeAmount = if (myIncomeAmount.length > 0) myIncomeAmount else "0"
        requestHelpForm.HusbandIncomeAmount = if (husbandIncomeAmount.length > 0) husbandIncomeAmount else "0"
        requestHelpForm.WifeIncomeAmount = if (wifeIncomeAmount.length > 0) wifeIncomeAmount else "0"
        requestHelpForm.IsWorkingSons = workingSons.toString() + ""
        requestHelpForm.IsAffairs = affairs.toString() + ""
        requestHelpForm.IsExpenses = expenses.toString() + ""
        requestHelpForm.IsOther = other.toString() + ""
        requestHelpForm.IsOrganisationDebt = organisationDebt.toString() + ""
        requestHelpForm.IsPersonalDebt = personalDebt.toString() + ""
        requestHelpForm.IsResidence = residence.toString() + ""
        requestHelpForm.IsSchoolFees = schoolFees.toString() + ""
        requestHelpForm.IsInstallment = installment.toString() + ""
        requestHelpForm.IsProvisions = provisions.toString() + ""
        requestHelpForm.IsTreatment = treatment.toString() + ""
        requestHelpForm.IsDelayedRent = delayedRent.toString() + ""
        requestHelpForm.IsWeakIncome = weakIncome.toString() + ""
        val list: ArrayList<RequestForHelpPersons?> = ArrayList<RequestForHelpPersons?>()
        for (requestForHelpPersons in familyMembersArrayList!!) {
            if (requestForHelpPersons!!.Id.equals("-1", ignoreCase = true)) {
            } else {
                if (requestForHelpPersons.Gender == null && requestForHelpPersons.Notes == null && requestForHelpPersons.CivilIdNo == null && requestForHelpPersons.DateOFBirth == null && requestForHelpPersons.Name == null && requestForHelpPersons.RelationShip == null) {
                } else {
                    list.add(requestForHelpPersons)
                }
            }
        }
        requestHelpForm.requestForHelpPersonslist = list
        requestHelpForm.RequestName = requestName
        requestHelpForm.MobileNumber = requestMobile
        requestHelpForm.RentAmount = rentAmount
        requestHelpForm.Employer = employer
        requestHelpForm.WorkType = workType
        requestHelpForm.Position = position
        var incomeSourceIds = ""
        if (workingSons) {
            incomeSourceIds = incomeSourceIds + "1"
        }
        if (affairs) {
            incomeSourceIds = if (incomeSourceIds.length > 0) {
                "$incomeSourceIds,2"
            } else {
                incomeSourceIds + "2"
            }
        }
        if (expenses) {
            incomeSourceIds = if (incomeSourceIds.length > 0) {
                "$incomeSourceIds,3"
            } else {
                incomeSourceIds + "3"
            }
        }
        if (other) {
            incomeSourceIds = if (incomeSourceIds.length > 0) {
                "$incomeSourceIds,4"
            } else {
                incomeSourceIds + "4"
            }
        }
        Log.d("incomeSourceIds", "" + incomeSourceIds)
        requestHelpForm.IncomeSourceIds = incomeSourceIds
        var requestForReason = ""
        if (organisationDebt) {
            requestForReason = requestForReason + "1"
        }
        if (personalDebt) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,2"
            } else {
                requestForReason + "2"
            }
        }
        if (residence) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,3"
            } else {
                requestForReason + "3"
            }
        }
        if (schoolFees) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,4"
            } else {
                requestForReason + "4"
            }
        }
        if (installment) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,5"
            } else {
                requestForReason + "5"
            }
        }
        if (provisions) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,6"
            } else {
                requestForReason + "6"
            }
        }
        if (treatment) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,7"
            } else {
                requestForReason + "7"
            }
        }
        if (delayedRent) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,8"
            } else {
                requestForReason + "8"
            }
        }
        if (weakIncome) {
            requestForReason = if (requestForReason.length > 0) {
                "$requestForReason,9"
            } else {
                requestForReason + "9"
            }
        }
        Log.d("requestForReason", "" + requestForReason)
        requestHelpForm.RequestReasons = requestForReason
    }

    private fun isValid(): Boolean {
        when {
            requestName.isEmpty() -> {
                et_requset_name.requestFocus()
                mainLayout.showSnakeBar(getString(R.string.enter_name))
                return false
            }
            requestMobile.isEmpty() -> {
                et_requset_mobile.requestFocus()
                mainLayout.showSnakeBar(getString(R.string.enter_mobile_number))
                return false
            }
            !et_requset_mobile.checkFixLength(8) -> {
                mainLayout.showSnakeBar(getString(R.string.mobile_character))
                et_requset_mobile.requestFocus()
                return false
            }
            homeDetails <= 0 -> {
                mainLayout.showSnakeBar(getString(R.string.select_home_details))
                return false
            }
            homeDetails == 2 && rentAmount.isEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_rent_amount))
                et_rent_amount.requestFocus()
                return false
            }
            maritalStatus <= 0 -> {
                mainLayout.showSnakeBar(getString(R.string.select_marital_status))
                return false
            }
            !organisationDebt && !personalDebt && !residence && !schoolFees && !installment && !provisions &&
                    !treatment && !delayedRent && !weakIncome -> {
                mainLayout.showSnakeBar(getString(R.string.select_short_request))
                return false
            }
        }
        return true
    }

    private fun RequestHelpForm1() {
        if (requestName.length > 0
                && requestMobile.length > 0
                && homeDetails > 0
                && maritalStatus > 0 &&  // myIncome>0 &&
                // myIncomeAmount.length()>0 &&
                //husbandIncome>0 &&
                // husbandIncomeAmount.length()>0 &&
                //wifeIncome>0 &&
                //wifeIncomeAmount.length()>0 &&
                // (workingSons || affairs || expenses || other) &&
                (organisationDebt || personalDebt || residence || schoolFees || installment || provisions ||
                        treatment || delayedRent || weakIncome) //   familyMembersArrayList.size()>0 &&
        //employer.length()>0 &&
        //workType.length()>0 &&
        /*position.length()>0*/) {
            var isError = false
            if (homeDetails == 2) {
                if (rentAmount.length > 0) {
                } else {
                    isError = true
                }
            }
            if (familyMembersArrayList?.size!! > 0) {
                for (familyMembers in familyMembersArrayList!!) {
                    if (familyMembers?.Id.equals("-1", ignoreCase = true)) {
                    } else {

                        /*if(familyMembers.getGender() == null){

                            isError=true;

                        }

                        if(familyMembers.getNotes() == null){

                            isError=true;

                        }

                        if(familyMembers.getCivilIdNo() == null){

                            isError=true;

                        }

                        if(familyMembers.getDateOFBirth() == null){

                            isError=true;

                        }

                        if(familyMembers.getName() == null){

                            isError=true;

                        }

                        if(familyMembers.getRelationShip() == null){

                            isError=true;

                        }*/
                    }
                }
            }
            if (isError) {
                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
            } else {
                createObject()
                loading.setVisibility(View.VISIBLE)
                GlobalFunctions.DisableLayout(mainLayout)
                if (isEdit) {
                    QenaatAPICall.getCallingAPIInterface()?.EditRequestForHelp(
                            "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                            "application/json",
                            createHelpObject())?.enqueue(
                            object : Callback<ResponseBody?> {

                                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                    t.printStackTrace()
                                    loading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                    val body = response?.body()
                                    var outResponse = ""
                                    try {
                                        val reader = BufferedReader(InputStreamReader(
                                                ByteArrayInputStream(body?.bytes())))
                                        val out = StringBuilder()
                                        val newLine = System.getProperty("line.separator")
                                        var line: String?
                                        while (reader.readLine().also { line = it } != null) {
                                            out.append(line)
                                            out.append(newLine)
                                        }
                                        outResponse = out.toString()
                                        Log.d("outResponse", "" + outResponse)
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                    if (outResponse != null) {
                                        outResponse = outResponse.replace("\"", "")
                                        outResponse = outResponse.replace("\n", "")
                                        Log.e("outResponse not null ", outResponse)
                                        if (outResponse.toInt() > 0) {
                                            fragmentManager?.popBackStack()
                                            if (isEdit) {
                                            } else {
                                            }
                                            val helpForm = RequestForHelp()
                                            helpForm.requestForHelpId = outResponse
                                            val gson = Gson()
                                            val b = Bundle()
                                            b.putString("RequestForHelp", gson.toJson(helpForm))
                                            ContentActivity.Companion.openRequestFormDocumentListFragment(b)
                                        } else if (outResponse == "-1") {
                                            Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-2") {
                                            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                        }
                                    }
                                    loading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                } else {
                    QenaatAPICall.getCallingAPIInterface()?.RequestHelpForm(
                            "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                            "application/json",
                            createHelpObject())?.enqueue(
                            object : Callback<ResponseBody?> {

                                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                    t.printStackTrace()
                                    loading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                    val body = response?.body()
                                    var outResponse = ""
                                    try {
                                        val reader = BufferedReader(InputStreamReader(
                                                ByteArrayInputStream(body?.bytes())))
                                        val out = StringBuilder()
                                        val newLine = System.getProperty("line.separator")
                                        var line: String?
                                        while (reader.readLine().also { line = it } != null) {
                                            out.append(line)
                                            out.append(newLine)
                                        }
                                        outResponse = out.toString()
                                        Log.d("outResponse", "" + outResponse)
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                    if (outResponse != null) {
                                        outResponse = outResponse.replace("\"", "")
                                        outResponse = outResponse.replace("\n", "")
                                        Log.e("outResponse not null ", outResponse)
                                        if (outResponse.toInt() > 0) {
                                            if (isEdit) {
                                            } else {
                                            }
                                            val helpForm = RequestForHelp()
                                            helpForm.requestForHelpId = outResponse
                                            val gson = Gson()
                                            val b = Bundle()
                                            b.putString("RequestForHelp", gson.toJson(helpForm))

                                            //activity?.onBackPressed()
                                            //fragmentManager?.popBackStack()
                                            fragmentManager?.popBackStack()
                                            Handler().post {
                                                ContentActivity.openRequestFormDocumentListFragment(b)
                                            }

                                        } else if (outResponse == "-1") {
                                            Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-2") {
                                            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                        }
                                    }
                                    loading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                }

                /*QenaatAPICall.getCallingAPIInterface().AddRequestForHelp(requestName, requestMobile, "05/02/2018", homeDetails+"", homeDetails == 2 ? rentAmount+"" : "0", maritalStatus+"", myIncome+"", myIncomeAmount+"", husbandIncome+"", husbandIncomeAmount, wifeIncome+"", wifeIncomeAmount, employer, workType, position, "", "", "application/json", familyMembersArrayList, mSessionManager.getUserCode(),  new Callback<Response>() {
                    @Override
                    public void success(retrofit.client.Response s, retrofit.client.Response response) {

                        TypedInput body = response.getBody();
                        String outResponse="";

                        try {

                            BufferedReader reader = new BufferedReader(new InputStreamReader(body.in()));

                            StringBuilder out = new StringBuilder();

                            String newLine = System.getProperty("line.separator");

                            String line;

                            while ((line = reader.readLine()) != null) {
                                out.append(line);
                                out.append(newLine);
                            }

                            outResponse = out.toString();
                            Log.d("outResponse", ""+outResponse);



                        } catch (Exception ex) {

                            ex.printStackTrace();


                        }

                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"","");
                            outResponse = outResponse.replace("\n", "");
                            Log.e("outResponse not null ", outResponse);

                            if (Integer.parseInt(outResponse.toString()) > 0) {

                                getFragmentManager().popBackStack();

                                if(isEdit){

                                }
                                else {

                                }

                                RequestForHelp helpForm = new RequestForHelp();

                                helpForm.setRequestForHelpId(outResponse.toString());

                                Gson gson = new Gson();
                                Bundle b = new Bundle();
                                b.putString("RequestForHelp", gson.toJson(helpForm));


                                ContentActivity.openRequestFormDocumentListFragment(b);



                            } else if (outResponse.equals("-1")) {
                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show();
                            } else if (outResponse.equals("-2")) {
                                Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show();
                            }
                        }
                        loading.setVisibility(View.INVISIBLE);
                        GlobalFunctions.EnableLayout(mainLayout);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        loading.setVisibility(View.INVISIBLE);
                        GlobalFunctions.EnableLayout(mainLayout);
                    }
                });*/
            }
        } else {
            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
        }
    }

    private fun RequestHelpForm() {
        if (isEdit) {
            QenaatAPICall.getCallingAPIInterface()?.EditRequestForHelp(
                    "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                    "application/json",
                    createHelpObject())?.enqueue(
                    object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            loading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            val body = response?.body()
                            var outResponse = ""
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                outResponse = out.toString()
                                Log.d("outResponse", "" + outResponse)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (outResponse.toInt() > 0) {
                                    fragmentManager?.popBackStack()
                                    requestHelpListener.refreshRequestHelp()
                                    /*val helpForm = RequestForHelp()
                                    helpForm.requestForHelpId = outResponse
                                    val gson = Gson()
                                    val b = Bundle()
                                    b.putString("RequestForHelp", gson.toJson(helpForm))
                                    ContentActivity.Companion.openRequestFormDocumentListFragment(b)*/
                                } else if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                }
                            }
                            loading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        } else {
            QenaatAPICall.getCallingAPIInterface()?.RequestHelpForm(
                    "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                    "application/json",
                    createHelpObject())?.enqueue(
                    object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            loading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            val body = response?.body()
                            var outResponse = ""
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                outResponse = out.toString()
                                Log.d("outResponse", "" + outResponse)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (outResponse.toInt() > 0) {
                                    if (isEdit) {
                                    } else {
                                    }
                                    val helpForm = RequestForHelp()
                                    helpForm.requestForHelpId = outResponse
                                    val gson = Gson()
                                    val b = Bundle()
                                    b.putString("RequestForHelp", gson.toJson(helpForm))

                                    //activity?.onBackPressed()
                                    //fragmentManager?.popBackStack()
                                    fragmentManager?.popBackStack()
                                    Handler().post {
                                        ContentActivity.openRequestFormDocumentListFragment(b)
                                    }

                                } else if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                }
                            }
                            loading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.KEY_RESULT_CODE) {
            try {
                countryName = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                countryCode = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
                countryFlag = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                iv_country_flag.setImageResource(countryFlag)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    companion object {
        protected val TAG = RequestHelpFormFragment::class.java.simpleName
        lateinit var fragment: RequestHelpFormFragment
        lateinit var mSessionManager: SessionManager
        lateinit var mAdapter: RecyclerView.Adapter<*>
        var familyMembersArrayList: ArrayList<RequestForHelpPersons?>? = ArrayList<RequestForHelpPersons?>()
        lateinit var requestHelpListener: Interfaces.RequestHelpListener

        fun newInstance(act: FragmentActivity): RequestHelpFormFragment {
            fragment = RequestHelpFormFragment()
            fragment.act = act
            return fragment
        }

        fun newInstance(act: FragmentActivity, requestHelpListener: Interfaces.RequestHelpListener): RequestHelpFormFragment {
            fragment = RequestHelpFormFragment()
            fragment.act = act
            this.requestHelpListener = requestHelpListener
            return fragment
        }

        fun addMoreMember() {
            val addFamilyMembers = RequestForHelpPersons()
            addFamilyMembers.Id = "0"
            familyMembersArrayList?.add(familyMembersArrayList?.size!! - 1, addFamilyMembers)
            mAdapter.notifyDataSetChanged()
        }
    }

    data class IncomeModel(val id: Int, val item: String)
}