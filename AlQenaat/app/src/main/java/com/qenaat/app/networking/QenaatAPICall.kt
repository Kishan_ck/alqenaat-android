package com.qenaat.app.networking

import com.google.gson.GsonBuilder
import com.qenaat.app.classes.QenaatConstant
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object QenaatAPICall {

    private var retrofit: Retrofit? = null
    var dpi: QenaatAPIInterface? = null

    var interceptor = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

    private val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .readTimeout(100, TimeUnit.SECONDS)
            .connectTimeout(100, TimeUnit.SECONDS)
            .cache(null)
            .build()

    private fun buildGsonConverter(): GsonConverterFactory? {
        val gsonBuilder = GsonBuilder()
        val myGson = gsonBuilder.excludeFieldsWithoutExposeAnnotation().create()
        return GsonConverterFactory.create(myGson)
    }

    var gson = GsonBuilder()
            .setLenient()
            .create()

    fun getCallingAPIInterface(): QenaatAPIInterface? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(QenaatConstant.BASE_URL!!)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(okHttpClient)
                    .build()
        }
        if (dpi == null) {
            dpi = retrofit?.create(QenaatAPIInterface::class.java)
        }
        return dpi
    }
}




