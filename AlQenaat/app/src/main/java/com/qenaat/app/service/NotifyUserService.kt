package com.qenaat.app.service

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.qenaat.app.ContentActivity
import com.qenaat.app.R

/**
 * Created by DELL on 11/08/2016.
 */
class NotifyUserService : IntentService("") {
    var NOTIFICATION_ID = 1
    private var mNotificationManager: NotificationManager? = null
    override fun onHandleIntent(arg0: Intent?) {
        sendNotification(arg0?.extras?.getString("message"))
    }

    private fun sendNotification(msg: String?) {
        mNotificationManager = this
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val contentIntent = PendingIntent.getActivity(this, 0,
                Intent(this, ContentActivity::class.java).putExtra("com.qenaat.fragType", 1), PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmSound = Uri.parse("android.resource://$packageName/raw/android_notification")
        Log.i("UriUri", "android.resource://$packageName/raw/android_notification")
        val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH)

            // Configure the notification channel.
            notificationChannel.setDescription(msg)
            notificationChannel.enableLights(true)
            notificationChannel.setLightColor(Color.RED)
            notificationChannel.setVibrationPattern(longArrayOf(0, 1000, 500, 1000))
            notificationChannel.enableVibration(true)
            mNotificationManager?.createNotificationChannel(notificationChannel)
        }
        val mBuilder = NotificationCompat.Builder(
                this, NOTIFICATION_CHANNEL_ID).setSmallIcon(R.drawable.icon_512)
                .setContentTitle(getString(R.string.notify_user_message))
                .setStyle(NotificationCompat.BigTextStyle().bigText(msg)).setAutoCancel(true).setSound(alarmSound)
                .setContentText(msg)
        mBuilder.setContentIntent(contentIntent)
        if (NOTIFICATION_ID == 50) {
            NOTIFICATION_ID = 0
        } else {
            NOTIFICATION_ID++
        }
        mNotificationManager?.notify(NOTIFICATION_ID, mBuilder.build())
    }
}