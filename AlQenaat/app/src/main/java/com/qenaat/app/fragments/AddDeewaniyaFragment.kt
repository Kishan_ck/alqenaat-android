package com.qenaat.app.fragments


import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jdev.countryutil.Constants
import com.jdev.countryutil.CountryUtil
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.Utils.Util
import com.qenaat.app.adapters.AlertAdapterFA
import com.qenaat.app.classes.*
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkEmptyString
import com.qenaat.app.classes.FixControl.checkFixLength
import com.qenaat.app.classes.FixControl.createPartFromFile
import com.qenaat.app.classes.FixControl.createPartFromString
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.model.GetAreas
import com.qenaat.app.model.GetWeekDays
import com.qenaat.app.model.UploadFile
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.add_news.*
import me.drakeet.materialdialog.MaterialDialog
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 12-Nov-17.
 */
class AddDeewaniyaFragment : Fragment(), View.OnClickListener {
    var act: FragmentActivity? = null
    var mLangSessionManager: LanguageSessionManager? = null
    var mainLayout: RelativeLayout? = null
    var tv_days_label: TextView? = null
    var tv_category_label: TextView? = null
    var tv_type_label: TextView? = null
    var tv_area_label: TextView? = null
    var tv_location_label: TextView? = null
    var tv_date: TextView? = null
    var et_full_name: EditText? = null
    var et_title: EditText? = null
    var et_mobile: EditText? = null
    var et_request_mobile: EditText? = null
    var et_details_label: EditText? = null
    var et_youTube: EditText? = null
    var et_facebook: EditText? = null
    var et_instagram: EditText? = null
    var et_twitter: EditText? = null
    var mloading: ProgressBar? = null
    var relative_image1: RelativeLayout? = null
    var relative_image2: RelativeLayout? = null
    var relative_image3: RelativeLayout? = null
    var relative_image4: RelativeLayout? = null
    var relative_image5: RelativeLayout? = null
    var img_select1: ImageView? = null
    var img_select2: ImageView? = null
    var img_select3: ImageView? = null
    var img_select4: ImageView? = null
    var img_select5: ImageView? = null
    var img_delete1: ImageView? = null
    var img_delete2: ImageView? = null
    var img_delete3: ImageView? = null
    var img_delete4: ImageView? = null
    var img_delete5: ImageView? = null
    var tv_add_images: TextView? = null
    private var arrayListPhoto: ArrayList<String?>? = null
    var imageCount = 0
    var tv_add_ad: TextView? = null
    private var index = 0
    private var current_path: String? = ""
    private var images: StringBuilder? = StringBuilder()
    var isEdit = false

    //GetProducts items;
    private var arrayListPhotoEdit: ArrayList<String?>? = null
    var horizontalScrollView: HorizontalScrollView? = null
    var countryId: String? = ""
    var cityId: String? = ""
    var dayId: String? = ""
    var catId: String? = ""
    var typeId: String? = ""
    var areaId: String? = ""
    var isAgree = false
    private val fromDatePickerDialog: DatePickerDialog? = null
    private var dateFormatter: SimpleDateFormat? = null
    private val mFormatter: SimpleDateFormat? = SimpleDateFormat("dd/MM/yyyy hh:mm aa", Locale.ENGLISH)
    var getAreasArrayList: ArrayList<GetAreas?>? = ArrayList()
    var getWeekDaysArrayList: ArrayList<GetWeekDays?>? = ArrayList()
    var scroll_view: ScrollView? = null
    var config = Config()
    private var imagesCameraGallery: ArrayList<Image?>? = ArrayList()
    private var countryCode: String = ""
    private var countryFlag: Int = 0
    private var countryName: String = ""
    private var countryCodeRequest: String = ""
    private var countryFlagRequest: Int = 0
    private var countryNameRequest: String = ""
    private var isRequestPhone = false

    //val multipartTypedOutput = MultipartTypedOutput()
    var permissionStr = ""
    private var imagePartList: MutableList<MultipartBody.Part>? = mutableListOf()
    private var textPart: RequestBody? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }

        countryCode = Util.getUserCountryInfo(act!!, ConstanstParameters.COUNTRY_CODE)
        countryName = Util.getUserCountryInfo(act!!, ConstanstParameters.COUNTRY_NAME)
        countryFlag = Util.getUserCountryInfo(act!!, ConstanstParameters.COUNTRY_FLAG).toInt()

        countryCodeRequest = Util.getUserCountryInfo(act!!, ConstanstParameters.COUNTRY_CODE)
        countryNameRequest = Util.getUserCountryInfo(act!!, ConstanstParameters.COUNTRY_NAME)
        countryFlagRequest = Util.getUserCountryInfo(act!!, ConstanstParameters.COUNTRY_FLAG).toInt()

        try {
            Log.d("onCreate", "Bundle")
            mLangSessionManager = LanguageSessionManager(act)
            mSessionManager = SessionManager(act!!)
            if (arguments != null) {
                if (arguments?.getString("isEdit").equals("0", ignoreCase = true)) {
                    isEdit = false
                } else if (arguments?.getString("isEdit").equals("1", ignoreCase = true)) {
                    isEdit = true
                    //                    Gson gson = new Gson();
//                    items = gson.fromJson(getArguments().getString("GetProducts"),
//                            GetProducts.class);
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return try {
            mainLayout = inflater.inflate(R.layout.add_news, null) as RelativeLayout
            act?.softWindow()
            mainLayout
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout!!)
    }

    fun initViews(mainLayout: RelativeLayout) {
        try {
            arrayListPhoto = ArrayList()
            arrayListPhotoEdit = ArrayList()
            scroll_view = mainLayout.findViewById<View?>(R.id.scroll_view) as ScrollView?
            tv_add_ad = mainLayout.findViewById<View?>(R.id.tv_add_ad) as TextView?
            tv_add_images = mainLayout.findViewById<View?>(R.id.tv_add_images) as TextView?
            horizontalScrollView = mainLayout.findViewById<View?>(R.id.horizontalScrollView) as HorizontalScrollView?
            img_delete1 = mainLayout.findViewById<View?>(R.id.img_delete1) as ImageView?
            img_delete2 = mainLayout.findViewById<View?>(R.id.img_delete2) as ImageView?
            img_delete3 = mainLayout.findViewById<View?>(R.id.img_delete3) as ImageView?
            img_delete4 = mainLayout.findViewById<View?>(R.id.img_delete4) as ImageView?
            img_delete5 = mainLayout.findViewById<View?>(R.id.img_delete5) as ImageView?
            img_select1 = mainLayout.findViewById<View?>(R.id.img_select1) as ImageView?
            img_select2 = mainLayout.findViewById<View?>(R.id.img_select2) as ImageView?
            img_select3 = mainLayout.findViewById<View?>(R.id.img_select3) as ImageView?
            img_select4 = mainLayout.findViewById<View?>(R.id.img_select4) as ImageView?
            img_select5 = mainLayout.findViewById<View?>(R.id.img_select5) as ImageView?
            relative_image1 = mainLayout.findViewById<View?>(R.id.relative_image1) as RelativeLayout?
            relative_image2 = mainLayout.findViewById<View?>(R.id.relative_image2) as RelativeLayout?
            relative_image3 = mainLayout.findViewById<View?>(R.id.relative_image3) as RelativeLayout?
            relative_image4 = mainLayout.findViewById<View?>(R.id.relative_image4) as RelativeLayout?
            relative_image5 = mainLayout.findViewById<View?>(R.id.relative_image5) as RelativeLayout?
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar?
            tv_days_label = mainLayout.findViewById<View?>(R.id.tv_tree_label) as TextView?
            tv_category_label = mainLayout.findViewById<View?>(R.id.tv_category_label) as TextView?
            tv_type_label = mainLayout.findViewById<View?>(R.id.tv_type_label) as TextView?
            tv_area_label = mainLayout.findViewById<View?>(R.id.tv_area_label) as TextView?
            et_mobile = mainLayout.findViewById<View?>(R.id.et_mobile) as EditText?
            et_request_mobile = mainLayout.findViewById<View?>(R.id.et_request_mobile) as EditText?
            et_details_label = mainLayout.findViewById<View?>(R.id.et_details_label) as EditText?
            et_youTube = mainLayout.findViewById<View?>(R.id.et_youTube) as EditText?
            et_facebook = mainLayout.findViewById<View?>(R.id.et_facebook) as EditText?
            et_instagram = mainLayout.findViewById<View?>(R.id.et_instagram) as EditText?
            et_twitter = mainLayout.findViewById<View?>(R.id.et_twitter) as EditText?
            et_full_name = mainLayout.findViewById<View?>(R.id.et_full_name) as EditText?
            et_title = mainLayout.findViewById<View?>(R.id.et_title) as EditText?
            tv_location_label = mainLayout.findViewById<View?>(R.id.tv_location_label) as TextView?
            tv_date = mainLayout.findViewById<View?>(R.id.tv_date) as TextView?
            tv_location_label?.setOnClickListener(this)
            tv_date?.setOnClickListener(this)
            tv_add_ad?.setOnClickListener(this)
            tv_add_images?.setOnClickListener(this)
            tv_days_label?.setOnClickListener(this)
            tv_category_label?.setOnClickListener(this)
            tv_type_label?.setOnClickListener(this)
            tv_area_label?.setOnClickListener(this)
            img_delete1?.setOnClickListener(this)
            img_select1?.setOnClickListener(this)
            relative_image1?.setOnClickListener(this)
            img_delete2?.setOnClickListener(this)
            img_delete3?.setOnClickListener(this)
            img_delete4?.setOnClickListener(this)
            img_delete5?.setOnClickListener(this)
            LL_country_code.setOnClickListener(this)
            LL_country_code_request.setOnClickListener(this)
            et_mobile?.setTypeface(ContentActivity.Companion.tf)
            et_request_mobile?.setTypeface(ContentActivity.Companion.tf)
            et_details_label?.setTypeface(ContentActivity.Companion.tf)
            et_youTube?.setTypeface(ContentActivity.Companion.tf)
            et_facebook?.setTypeface(ContentActivity.Companion.tf)
            et_instagram?.setTypeface(ContentActivity.Companion.tf)
            et_twitter?.setTypeface(ContentActivity.Companion.tf)
            et_full_name?.setTypeface(ContentActivity.Companion.tf)
            et_title?.setTypeface(ContentActivity.Companion.tf)
            tv_location_label?.setTypeface(ContentActivity.Companion.tf)
            tv_date?.setTypeface(ContentActivity.Companion.tf)
            tv_days_label?.setTypeface(ContentActivity.Companion.tf)
            tv_category_label?.setTypeface(ContentActivity.Companion.tf)
            tv_type_label?.setTypeface(ContentActivity.Companion.tf)
            tv_area_label?.setTypeface(ContentActivity.Companion.tf)
            var imgW = (act?.getResources()?.getDrawable(
                    R.drawable.add_photos) as BitmapDrawable).bitmap.width
            val imgH = (act?.getResources()?.getDrawable(
                    R.drawable.add_photos) as BitmapDrawable).bitmap.height
            img_select1?.getLayoutParams()?.height = imgH
            img_select1?.getLayoutParams()?.width = imgW
            img_select2?.getLayoutParams()?.height = imgH
            img_select2?.getLayoutParams()?.width = imgW
            img_select3?.getLayoutParams()?.height = imgH
            img_select3?.getLayoutParams()?.width = imgW
            img_select4?.getLayoutParams()?.height = imgH
            img_select4?.getLayoutParams()?.width = imgW
            img_select5?.getLayoutParams()?.height = imgH
            img_select5?.getLayoutParams()?.width = imgW
            imgW = (act?.getResources()?.getDrawable(
                    R.drawable.desc) as BitmapDrawable).bitmap.width
            et_details_label?.getLayoutParams()?.width = imgW
            dateFormatter = SimpleDateFormat("dd-MM-yyyy", Locale.US)
            et_title?.setHint(act?.getString(R.string.TitleLabel))
            tv_category_label?.setVisibility(View.GONE)
            tv_type_label?.setVisibility(View.VISIBLE)
            tv_area_label?.setVisibility(View.VISIBLE)
            tv_days_label?.setVisibility(View.VISIBLE)
            tv_date?.setVisibility(View.GONE)
            tv_days_label?.setHint(act?.getString(R.string.SelectDay1))
            tv_add_ad?.setText(act?.getString(R.string.AddDeewaniya))
            et_details_label?.setHint(act?.getString(R.string.DeewaniyaDetails))
            tv_type_label?.setText(act?.getString(R.string.DeewaniyaLabel))
            typeId = "0"
            ContentActivity.Companion.typeId = "0"
            ContentActivity.Companion.typeName = tv_type_label?.getText().toString()
            ContentActivity.Companion.setTextFonts(mainLayout)
            ////setImagePickerConfig()

            if (Calendar.getInstance()[Calendar.HOUR_OF_DAY] <= 16
                    || Calendar.getInstance()[Calendar.HOUR_OF_DAY] >= 17
                    && Calendar.getInstance()[Calendar.HOUR_OF_DAY] <= 17
                    && Calendar.getInstance()[Calendar.MINUTE] <= 30
            ) {

                Log.e("111111", "INNNNNNNNNNNNNNNN");
                val date = Date() // your date
                val cal = Calendar.getInstance()
                cal.time = date
                val year = cal[Calendar.YEAR]
                val month = cal[Calendar.MONTH]
                val day = cal[Calendar.DAY_OF_MONTH]

                // tv_from_time.setText(new Date().getDay() + "/" + new Date().getMonth() + "/" + new Date().getYear() + " 05:30 PM");
                /*tv_date.setText(cal[Calendar.DAY_OF_MONTH].toString() + "/"
                        + cal[Calendar.MONTH]
                        + "/" + cal[Calendar.YEAR] + " 05:30 PM")*/
                tv_date?.setText(cal[Calendar.DAY_OF_MONTH].toString() + "/" + (
                        if (cal[Calendar.MONTH].toString().length > 0) "0" + cal[Calendar.MONTH]
                        else cal[Calendar.MONTH].toString())
                        + "/" + cal[Calendar.YEAR] + " 05:30 PM")
            }

            tv_country_code.isSelected = true
            tv_country_code_request.isSelected = true
            tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
            iv_country_flag.setImageResource(countryFlag)
            tv_country_code_request.text = String.format("(%s) %s", countryNameRequest, countryCodeRequest)
            iv_country_flag_request.setImageResource(countryFlagRequest)
        } catch (e: Exception) {
            Log.e(TAG + " initViews: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onStart() {
        super.onStart()
        Log.e("onStart", "onStart")
        ContentActivity.Companion.img_topAddAd?.setVisibility(View.GONE)
        // ContentActivity.Companion.topBarView?.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle?.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle?.setText(act?.getString(R.string.AddDeewaniya))

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(mLangSessionManager)
        et_details_label?.setMovementMethod(ScrollingMovementMethod())
        scroll_view?.setOnTouchListener(OnTouchListener { v, event ->
            et_details_label?.getParent()?.requestDisallowInterceptTouchEvent(false)
            false
        })
        et_details_label?.setOnTouchListener(OnTouchListener { v, event ->
            et_details_label?.getParent()?.requestDisallowInterceptTouchEvent(true)
            false
        })

        //select by default deewaniya
        selectDeewaniya()
        if (isEdit) {
        } else {
            if (ContentActivity.Companion.catId?.length!! > 0) {
                tv_category_label?.setText(ContentActivity.Companion.catName)
                catId = ContentActivity.Companion.catId
            } else {
                getCategories()
            }
            if (ContentActivity.Companion.typeId?.length!! > 0) {
                tv_type_label?.setText(ContentActivity.Companion.typeName)
                typeId = ContentActivity.Companion.typeId
            }
            if (ContentActivity.Companion.areaId?.length!! > 0) {
                tv_area_label?.setText(ContentActivity.Companion.areaName)
                areaId = ContentActivity.Companion.areaId
            } else {
                GetAreas()
            }
            if (ContentActivity.Companion.dayId?.length!! > 0) {
                tv_days_label?.setText(ContentActivity.Companion.dayName)
                dayId = ContentActivity.Companion.dayId
            } else {
                GetWeekDays()
            }
            if (ContentActivity.Companion.fullName?.length!! > 0) {
                et_full_name?.setText(ContentActivity.Companion.fullName)
            }
            if (ContentActivity.Companion.title?.length!! > 0) {
                et_title?.setText(ContentActivity.Companion.title)
            }
            if (ContentActivity.Companion.mobile?.length!! > 0) {
                et_mobile?.setText(ContentActivity.Companion.mobile)
            }
            if (ContentActivity.Companion.request_mobile?.length!! > 0) {
                et_request_mobile?.setText(ContentActivity.Companion.request_mobile)
            }
            if (ContentActivity.Companion.location?.length!! > 0) {
                tv_location_label?.setText(ContentActivity.Companion.location)
            }
            if (ContentActivity.Companion.details?.length!! > 0) {
                et_details_label?.setText(ContentActivity.Companion.details)
            }
            if (ContentActivity.Companion.date?.length!! > 0) {
                tv_date?.setText(ContentActivity.Companion.date)
            }
            if (ContentActivity.Companion.youTube?.length!! > 0) {
                et_youTube?.setText(ContentActivity.Companion.youTube)
            }
            if (ContentActivity.Companion.facebook?.length!! > 0) {
                et_facebook?.setText(ContentActivity.Companion.facebook)
            }
            if (ContentActivity.Companion.instagram?.length!! > 0) {
                et_instagram?.setText(ContentActivity.Companion.instagram)
            }
            if (ContentActivity.Companion.twitter?.length!! > 0) {
                et_twitter?.setText(ContentActivity.Companion.twitter)
            }
            val gson: Gson
            if (mSessionManager?.getImagePath() !== "" && mSessionManager?.getImagePath() != null) {
                gson = Gson()
                val listType = object : TypeToken<ArrayList<String?>?>() {}.type
                arrayListPhoto = gson.fromJson(mSessionManager?.getImagePath(), listType)
                setImages(arrayListPhoto?.size!!)
            }
        }
    }

    override fun onClick(view: View?) {
        val b = Bundle()
        Log.d("onClick", "onClick==" + view?.getId())
        val gson = Gson()
        when (view?.getId()) {
            R.id.LL_country_code -> {
                isRequestPhone = false
                CountryUtil(act).setTitle("").build()
            }
            R.id.LL_country_code_request -> {
                isRequestPhone = true
                CountryUtil(act).setTitle("").build()
            }
            /*R.id.tv_add_ad -> {
                //if(arrayListPhoto.size()>0 || arrayListPhotoEdit.size()>0){
                var isError = false
                var isPhoneError = false
                isError = if (areaId?.length!! > 0 && typeId?.length!! > 0 && et_title?.getText().toString().length > 0 && et_request_mobile?.getText().toString().length > 0 && et_details_label?.getText().toString().length > 0) {
                    if (typeId.equals("0", ignoreCase = true)) {
                        if (dayId?.length!! > 0) {
                            false
                        } else {
                            true
                        }
                    } else {
                        false
                    }
                } else {
                    true
                }
                if (et_mobile?.getText().toString().length > 0) {
                    if (FixControl.CheckMobileNumberIs_8(et_mobile)!!) {
                    } else {
                        isPhoneError = true
                    }
                }
                if (isError) {
                    Toast.makeText(act, act?.getString(R.string.FillAllFields), Toast.LENGTH_LONG).show()
                } else {
                    if (isPhoneError) {
                        Toast.makeText(act, act?.getString(R.string.MobileError), Toast.LENGTH_LONG).show()
                    } else {
                        index = 0
                        mloading?.setVisibility(View.VISIBLE)
                        GlobalFunctions.DisableLayout(mainLayout)
                        if (arrayListPhoto?.size!! > 0) {
                            current_path = if (isEdit) {
                                arrayListPhotoEdit?.get(index)
                            } else {
                                arrayListPhoto?.get(index)
                            }
                            uploadPhotos()
                            *//*val uploadImagesAysn = UploadImagesAysn()
                            uploadImagesAysn.execute()*//*
                        } else {
                            SendDewania()
                        }
                    }
                }
            }*/
            R.id.tv_add_ad -> {
                act?.hideKeyboard()
                if (isValid()) {
                    index = 0
                    mloading?.setVisibility(View.VISIBLE)
                    GlobalFunctions.DisableLayout(mainLayout)
                    if (arrayListPhoto?.size!! > 0) {
                        current_path = if (isEdit) {
                            arrayListPhotoEdit?.get(index)
                        } else {
                            arrayListPhoto?.get(index)
                        }
                        uploadPhotos()
                        /*val uploadImagesAysn = UploadImagesAysn()
                        uploadImagesAysn.execute()*/
                    } else SendDewania()
                }
            }
            R.id.img_select1 -> Log.d("test", "clicked img_select1")
            R.id.img_delete1 -> {
                Log.d("test", "clicked img_delete1")
                if (isEdit) {
                    arrayListPhotoEdit?.remove(img_select1?.getTag().toString())
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                    Log.e("arrayListPhoto after", "" + arrayListPhotoEdit?.size)
                    setEditImages(arrayListPhotoEdit?.size!!)
                } else {
                    arrayListPhoto?.remove(img_select1?.getTag().toString())
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                    Log.e("arrayListPhoto after", "" + arrayListPhoto?.size)
                    setImages(arrayListPhoto?.size!!)
                }
            }
            R.id.img_delete2 -> if (isEdit) {
                arrayListPhotoEdit?.remove(img_select2?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit?.size!!)
            } else {
                arrayListPhoto?.remove(img_select2?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto?.size!!)
            }
            R.id.img_delete3 -> if (isEdit) {
                arrayListPhotoEdit?.remove(img_select3?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit?.size!!)
            } else {
                arrayListPhoto?.remove(img_select3?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto?.size!!)
            }
            R.id.img_delete4 -> if (isEdit) {
                arrayListPhotoEdit?.remove(img_select4?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit?.size!!)
            } else {
                arrayListPhoto?.remove(img_select4?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto?.size!!)
            }
            R.id.img_delete5 -> if (isEdit) {
                arrayListPhotoEdit?.remove(img_select5?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                setEditImages(arrayListPhotoEdit?.size!!)
            } else {
                arrayListPhoto?.remove(img_select5?.getTag().toString())
                mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                setImages(arrayListPhoto?.size!!)
            }
            R.id.tv_add_images -> {
                //First checking if the app is already having the permission
                /*if (GlobalFunctions.isReadStorageAllowed(act)) {
                    if (GlobalFunctions.isCameraAllowed(act)) {
                        val imageCount = 1
                        saveData()
                        if (mLangSessionManager?.getLang() == "ar") {
                            Toast.makeText(act, act?.getString(R.string.SlideLeft), Toast.LENGTH_LONG).show()
                        }
                        if (isEdit) {
                            if (arrayListPhotoEdit?.size!! < imageCount) {
                                ImagePicker.with(this)
                                        .setFolderMode(false)
                                        .setCameraOnly(false)
                                        .setFolderTitle(act?.getString(R.string.GalleryLabel))
                                        .setMultipleMode(true)
                                        .setSelectedImages(config?.getSelectedImages())
                                        .setMaxSize(imageCount - arrayListPhotoEdit?.size!!)
                                        .start()
                            }
                        } else {
                            if (arrayListPhoto?.size!! < imageCount) {
                                ImagePicker.with(this)
                                        .setFolderMode(false)
                                        .setCameraOnly(false)
                                        .setFolderTitle(act?.getString(R.string.GalleryLabel))
                                        .setMultipleMode(true)
                                        .setSelectedImages(config?.getSelectedImages())
                                        .setMaxSize(imageCount - arrayListPhoto?.size!!)
                                        .start()
                            }
                        }
                        return
                    }
                }*/

                //If the app has not the permission then asking for the permission
                /*  if (GlobalFunctions.isCameraAllowed(act)) {
                      permissionStr = Manifest.permission.READ_EXTERNAL_STORAGE
                      GlobalFunctions.requestStoragePermission(act)
                  } else {
                      permissionStr = Manifest.permission.CAMERA
                      GlobalFunctions.requestCameraePermission(act)
                  }*/
                permissionStr = Manifest.permission.CAMERA
                GlobalFunctions.requestMultiplePermission(act,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE))
            }
            R.id.tv_location_label -> {
                saveData()
                /* if (GlobalFunctions.isGPSAllowed(act)) {
                     ContentActivity.openCurrentLocationFragmentFadeIn()
                     return
                 }*/
                permissionStr = Manifest.permission.ACCESS_FINE_LOCATION
                GlobalFunctions.requestGPSPermission(act)
            }
            R.id.tv_date -> SlideDateTimePicker.Builder(act?.getSupportFragmentManager())
                    .setListener(listener)
                    .setInitialDate(Date())
                    .build()
                    .show()
            R.id.tv_tree_label -> if (getWeekDaysArrayList?.size!! > 0 && typeId.equals("0", ignoreCase = true)) {
                val inflater1 = act
                        ?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView?
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView?
                tv_title1?.setText(act?.getString(R.string.SelectDay))
                tv_title1?.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (weekDays in getWeekDaysArrayList!!) {
                    if (mLangSessionManager?.getLang() == "en") {
                        arrayList1.add(weekDays?.TitleEn)
                    } else {
                        arrayList1.add(weekDays?.TitleAr)
                    }
                }
                arrayList1.add(act?.getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1?.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1?.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(act?.getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        dayId = getWeekDaysArrayList?.get(i)?.Id
                        ContentActivity.Companion.dayId = dayId!!
                        tv_days_label?.setText(item)
                        ContentActivity.Companion.dayName = item!!
                    }
                })
            }
            R.id.tv_area_label -> if (getAreasArrayList?.size!! > 0) {
                val inflater1 = act
                        ?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1 = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView?
                val tv_title1 = v1.findViewById<View?>(R.id.tv_title) as TextView?
                tv_title1?.setText(act?.getString(R.string.SelectAreaLabel))
                tv_title1?.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (areas in getAreasArrayList!!) {
                    if (mLangSessionManager?.getLang() == "en") {
                        arrayList1.add(areas?.TitleEn)
                    } else {
                        arrayList1.add(areas?.TitleAr)
                    }
                }
                arrayList1.add(act?.getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1?.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1 = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1?.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert1.dismiss()
                    val item = arrayList1[i]
                    if (item.equals(act?.getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else {
                        areaId = getAreasArrayList?.get(i)?.Id
                        ContentActivity.Companion.areaId = areaId!!
                        tv_area_label?.setText(item)
                        ContentActivity.Companion.areaName = item!!
                    }
                })
            }
            R.id.tv_type_label -> {
                val inflater11 = act
                        ?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v11 = inflater11.inflate(R.layout.country_dialog, null)
                val listView11 = v11.findViewById<View?>(R.id.lv) as ListView?
                val tv_title11 = v11.findViewById<View?>(R.id.tv_title) as TextView?
                tv_title11?.setText(act?.getString(R.string.SelectType1))
                tv_title11?.setTypeface(ContentActivity.Companion.tf)
                val arrayList11 = ArrayList<String?>()
                arrayList11.add(act?.getString(R.string.DeewaniyaLabel))
                arrayList11.add(act?.getString(R.string.HouseLabel))
                arrayList11.add(act?.getString(R.string.CancelLabel))
                val alertAdapter11 = AlertAdapterFA(act, arrayList11)
                listView11?.setAdapter(alertAdapter11)
                FixControl.setListViewHeightBasedOnChildren(listView11)
                val alert11 = MaterialDialog(act).setContentView(v11)
                alert11.show()
                alert11.setCanceledOnTouchOutside(true)
                listView11?.setOnItemClickListener(OnItemClickListener { adapterView, view, i, l ->
                    alert11.dismiss()
                    val item = arrayList11[i]
                    if (item.equals(act?.getString(R.string.CancelLabel), ignoreCase = true)) {
                    } else if (item.equals(act?.getString(R.string.DeewaniyaLabel), ignoreCase = true)) {
                        tv_type_label?.setText(item)
                        ContentActivity.Companion.typeName = item!!
                        typeId = "0"
                        ContentActivity.Companion.typeId = "0"
                        //tv_days_label.setVisibility(View.VISIBLE);
                        tv_days_label?.setAlpha(1.0 as Float)
                        et_details_label?.setHint(act?.getString(R.string.DeewaniyaDetails))
                        tv_add_ad?.setText(act?.getString(R.string.AddDeewaniya))
                    } else if (item.equals(act?.getString(R.string.HouseLabel), ignoreCase = true)) {
                        tv_type_label?.setText(item)
                        ContentActivity.Companion.typeName = item!!
                        typeId = "1"
                        ContentActivity.Companion.typeId = "1"
                        tv_days_label?.setAlpha(0.5 as Float)
                        et_details_label?.setHint(act?.getString(R.string.HouseDetailsLabel))
                        tv_add_ad?.setText(act?.getString(R.string.AddHouseLabel))
                        /*dayId="";
                                    tv_days_label.setText("");
                                    ContentActivity.dayId="";
                                    ContentActivity.dayName="";*/
                    }
                })
            }
            R.id.tv_category_label -> {
            }
        }
    }

    private fun isValid(): Boolean {
        when {
            typeId == "0" && dayId?.checkEmptyString()!! -> {
                mainLayout?.showSnakeBar(getString(R.string.SelectDay1))
                return false
            }
            areaId?.checkEmptyString()!! -> {
                mainLayout?.showSnakeBar(getString(R.string.select_area))
                return false
            }
            et_title?.checkEmpty()!! -> {
                mainLayout?.showSnakeBar(getString(R.string.enter_title))
                et_title?.requestFocus()
                return false
            }
            et_details_label?.checkEmpty()!! -> {
                mainLayout?.showSnakeBar(getString(R.string.enter_details))
                et_details_label?.requestFocus()
                return false
            }
            tv_date?.checkEmpty()!! -> {
                mainLayout?.showSnakeBar(getString(R.string.enter_date))
                tv_date?.requestFocus()
                return false
            }
            et_mobile?.checkEmpty()!! -> {
                mainLayout?.showSnakeBar(getString(R.string.enter_mobile_number))
                et_mobile?.requestFocus()
                return false
            }
            !et_mobile?.checkFixLength(8)!! -> {
                mainLayout?.showSnakeBar(getString(R.string.mobile_character))
                et_mobile?.requestFocus()
                return false
            }
            !et_request_mobile?.checkEmpty()!! -> {
                when {
                    !et_request_mobile?.checkFixLength(8)!! -> {
                        mainLayout?.showSnakeBar(getString(R.string.mobile_character))
                        et_request_mobile?.requestFocus()
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun uploadPhotos() {
        //multipartTypedOutput.addPart("FileObjectType", TypedString("dewania"))
        textPart = "dewania".createPartFromString()
        SaveAndGetImageTask().execute(BitmapFactory.decodeFile(current_path))
    }

    private fun uploadPhotosAPI() {
        QenaatAPICall.getCallingAPIInterface()?.uploadImage(textPart, imagePartList)?.enqueue(
                object : Callback<ArrayList<UploadFile?>?> {

                    override fun onFailure(call: Call<ArrayList<UploadFile?>?>, t: Throwable) {
                        t.printStackTrace()
                        GlobalFunctions.EnableLayout(mainLayout)
                        mloading?.setVisibility(View.GONE)
                        mainLayout?.showSnakeBar(t?.message!!)
                    }

                    override fun onResponse(call: Call<ArrayList<UploadFile?>?>, response: Response<ArrayList<UploadFile?>?>) {
                        GlobalFunctions.EnableLayout(mainLayout)
                        if (response.body() != null) {
                            val uploadFileList = response.body()

                            var isSuccess = false
                            for (i in uploadFileList?.indices!!) {
                                if (uploadFileList[i]?.Type == 1) {
                                    isSuccess = true
                                    Log.e("imageUrl", uploadFileList[i]?.FileUrl)
                                } else {
                                    isSuccess = false
                                    Log.e("imageUrlError", uploadFileList[i]?.error)
                                    mainLayout?.showSnakeBar(uploadFileList[i]?.error!!)
                                }
                            }
                            Log.e("imageUrl", images.toString())
                            Log.e("photo upload", isSuccess.toString())
                            if (isSuccess) SendDewania()
                            mloading?.setVisibility(View.GONE)
                        }
                    }
                })
    }

    //Save image in hidden folder and get it...
    internal inner class SaveAndGetImageTask() : AsyncTask<Bitmap?, Void?, File?>() {
        protected override fun doInBackground(vararg params: Bitmap?): File? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/.Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            val currentTime = "img_" + Calendar.getInstance().timeInMillis + ".png"
            images?.append(currentTime)
            /*if (images?.isNotEmpty()!!)
                images!!.append(",")*/
            val file = File(dir, currentTime)
            val fOut: FileOutputStream
            fOut = FileOutputStream(file)
            params[0]?.compress(Bitmap.CompressFormat.PNG, 60, fOut)
            fOut.flush()
            fOut.close()
            return file
        }

        override fun onPostExecute(file: File?) {
            super.onPostExecute(file)

            //multipartTypedOutput.addPart("File", TypedFile("image/*", file))
            imagePartList?.add(createPartFromFile("File", file!!)!!)
            uploadPhotosAPI()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.e("onActivityResult", "onActivityResult")
        if (requestCode == ContentActivity.Companion.FRAGMENT_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
            }
        } else if (requestCode == Config.RC_PICK_IMAGES && resultCode == Activity.RESULT_OK && data != null) {
            imagesCameraGallery = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES)
            if (imagesCameraGallery != null) {
                for (uri in imagesCameraGallery!!) {
                    Log.e(TAG, " uri: " + uri?.path)
                    //mMedia.add(uri);
                    Log.d("uri", "" + uri?.path)
                    if (isEdit) {
                        arrayListPhotoEdit?.add(uri?.path)
                    } else {
                        arrayListPhoto?.add(uri?.path)
                    }
                }
                imageCount = if (isEdit) {
                    val gson = Gson()
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                    setEditImages(arrayListPhotoEdit?.size!!)
                    arrayListPhotoEdit?.size!!
                } else {
                    val gson = Gson()
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                    setImages(arrayListPhoto?.size!!)
                    arrayListPhoto?.size!!
                }
            }

            //do something
        }
        /*else if (requestCode == 3 && data!=null) {

            Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

            if (parcelableUris == null) {
                return;
            }

            // Java doesn't allow array casting, this is a little hack
            Uri[] uris = new Uri[parcelableUris.length];
            Log.e("uris size ",""+uris.length);
            //mimg_deletePhoto1.setText(""+uris.length);
            System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

            if (uris != null) {
                for (Uri uri : uris) {
                    Log.e(TAG, " uri: " + uri.toString());
                    //mMedia.add(uri);
                    Log.d("uri", ""+uri.getPath());
                    if(isEdit){
                        arrayListPhotoEdit.add(uri.getPath());
                    }
                    else{
                        arrayListPhoto.add(uri.getPath());
                    }

                }
                if(isEdit){
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit));
                    setEditImages(arrayListPhotoEdit.size());
                    imageCount = arrayListPhotoEdit.size();
                }
                else{
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto));
                    setImages(arrayListPhoto.size());
                    imageCount = arrayListPhoto.size();
                }

            }
        }*/
        else if (requestCode == Constants.KEY_RESULT_CODE) {
            try {
                if (isRequestPhone) {
                    countryNameRequest = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                    countryCodeRequest = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                    tv_country_code_request.text = String.format("(%s) %s", countryNameRequest, countryCodeRequest)
                    countryFlagRequest = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                    iv_country_flag_request.setImageResource(countryFlagRequest)
                } else {
                    countryName = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                    countryCode = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                    tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
                    countryFlag = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                    iv_country_flag.setImageResource(countryFlag)
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun setImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
                relative_image1?.setVisibility(View.GONE)
                relative_image2?.setVisibility(View.GONE)
                relative_image3?.setVisibility(View.GONE)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            1 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhoto?.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2?.setVisibility(View.GONE)
                relative_image3?.setVisibility(View.GONE)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            2 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhoto?.get(0))
                img_select2?.setTag(arrayListPhoto?.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2?.setVisibility(View.VISIBLE)
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3?.setVisibility(View.GONE)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            3 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhoto?.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2?.setVisibility(View.VISIBLE)
                img_select2?.setTag(arrayListPhoto?.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3?.setVisibility(View.VISIBLE)
                img_select3?.setTag(arrayListPhoto?.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select3)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            4 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhoto?.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2?.setVisibility(View.VISIBLE)
                img_select2?.setTag(arrayListPhoto?.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3?.setVisibility(View.VISIBLE)
                img_select3?.setTag(arrayListPhoto?.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select3)
                relative_image4?.setVisibility(View.VISIBLE)
                img_select4?.setTag(arrayListPhoto?.get(3))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(3))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select4)
                relative_image5?.setVisibility(View.GONE)
            }
            5 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhoto?.get(0))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select1)
                relative_image2?.setVisibility(View.VISIBLE)
                img_select2?.setTag(arrayListPhoto?.get(1))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(1))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select2)
                relative_image3?.setVisibility(View.VISIBLE)
                img_select3?.setTag(arrayListPhoto?.get(2))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(2))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select3)
                relative_image4?.setVisibility(View.VISIBLE)
                img_select4?.setTag(arrayListPhoto?.get(3))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(3))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select4)
                relative_image5?.setVisibility(View.VISIBLE)
                img_select5?.setTag(arrayListPhoto?.get(4))
                Picasso.with(act)
                        .load("file://" + arrayListPhoto?.get(4))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photos)
                        .placeholder(R.drawable.add_photos)
                        .into(img_select5)
            }
        }
    }

    internal inner class UploadImagesAysn : AsyncTask<String?, String?, String?>() {
        override fun onPostExecute(paramString: String?) {
            Log.d("image updates", "" + index + " image uploaded -> " + index + 1 + " image started")
            index = index + 1
            if (isEdit) {
                if (index < arrayListPhotoEdit?.size!!) {
                    current_path = arrayListPhotoEdit?.get(index)
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading?.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    SendDewania()
                }
            } else {
                if (index < arrayListPhoto?.size!!) {
                    current_path = arrayListPhoto?.get(index)
                    val uploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading?.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    SendDewania()
                }
            }
        }

        override fun doInBackground(vararg params: String?): String? {

            /*String PhotoName = GlobalFunctions.SendMultipartFile(
                    current_path, getString(R.string.UploadContentPhoto), ".jpg");*/
            if (!current_path?.contains("http://otabi.")!!) {
                var file: File? = null
                val myBitmap: Bitmap? = null
                var filename = ""
                var angle = 0
                val ei: ExifInterface
                try {
                    ei = ExifInterface(current_path)
                    val orientation = ei.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, 1)
                    when (orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> angle = 90
                        ExifInterface.ORIENTATION_ROTATE_180 -> angle = 180
                        ExifInterface.ORIENTATION_ROTATE_270 -> angle = 270
                    }
                } catch (e1: IOException) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace()
                }
                if (isEdit) {
//                    filename = getNameOfImage(current_path
//                    );
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    Log.d("str1", "0-> " + images.toString())
                    val str1 = images.toString().replace(getNameOfImage(current_path).toString(), filename)
                    Log.d("str1", "1-> " + getNameOfImage(current_path))
                    Log.d("str1", "2-> $filename")
                    images = StringBuilder()
                    images?.append(str1)
                    Log.d("str1", "3-> " + images.toString())
                } else {
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    images?.append(filename)
                    if (index != arrayListPhoto?.size!! - 1) {
                        images?.append(",")
                    }
                }
                file = File(current_path)
                /*myBitmap = decodeSampledBitmapFromPath(file.getAbsolutePath(),
                        1000, 1000);

                Matrix matrix = new Matrix();
                matrix.postRotate(angle);
                myBitmap = Bitmap
                        .createBitmap(myBitmap, 0, 0, myBitmap.getWidth(),
                                myBitmap.getHeight(), matrix, true);

                OutputStream os;
                try {
                    os = new FileOutputStream(current_path);
                    myBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
                var conn: HttpURLConnection? = null
                var dos: DataOutputStream? = null
                var inStream: DataInputStream? = null
                val lineEnd = "\r\n"
                val twoHyphens = "--"
                val boundary = "*****"
                var bytesRead: Int
                var bytesAvailable: Int
                var bufferSize: Int
                val buffer: ByteArray
                val maxBufferSize = 1 * 1024 * 1024
                try {
                    val fileInputStream = FileInputStream(Compressor(act).compressToFile(file))
                    //once check url correct or not
                    val url = URL(QenaatConstant.Photo_URL + "dewania")
                    conn = url.openConnection() as HttpURLConnection
                    conn.doInput = true
                    conn.doOutput = true
                    conn.requestMethod = "POST"
                    conn.useCaches = false
                    conn.setRequestProperty("Connection", "Keep-Alive")
                    conn.setRequestProperty("Content-Type",
                            "multipart/form-data;boundary=$boundary")
                    dos = DataOutputStream(conn.outputStream)
                    dos.writeBytes(twoHyphens + boundary + lineEnd)
                    dos.writeBytes("Content-Disposition: form-data; name=\"pic\";"
                            + " filename=\"" + filename + "\"" + lineEnd)
                    dos.writeBytes(lineEnd)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    buffer = ByteArray(bufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize)
                        bytesAvailable = fileInputStream.available()
                        bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    }
                    dos.writeBytes(lineEnd)
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)
                    fileInputStream.close()
                    dos.flush()
                    dos.close()
                } catch (ex: MalformedURLException) {
                    println("Error:$ex")
                } catch (ioe: IOException) {
                    println("Error:$ioe")
                }
                try {
                    inStream = DataInputStream(conn?.getInputStream())
                    var str: String?
                    while (inStream.readLine().also { str = it } != null) {
                        println(str)
                    }
                    inStream.close()
                } catch (ioex: IOException) {
                    println("Error: $ioex")
                }
            }
            return "1"
        }
    }

    private fun getNamesOfImages() {
        images = StringBuilder()
        if (isEdit) {
            for (i in arrayListPhotoEdit?.indices!!) {
                val file = File(arrayListPhotoEdit?.get(i))
                //Log.d("file name", ""+FilenameUtils.getBaseName(file.getName()));
                images?.append(getNameOfImage(arrayListPhotoEdit?.get(i)))
                //images.append(FilenameUtils.getBaseName(file.getName()));
                if (i != arrayListPhotoEdit?.size!! - 1) {
                    images?.append(",")
                }
                Log.e("images", images.toString())
            }
        } else {
            /*for (int i = 0; i < arrayListPhoto.size(); i++) {

                File file = new File(arrayListPhoto.get(i));
                //Log.d("file name", ""+FilenameUtils.getBaseName(file.getName()));

                images.append(getNameOfImage(arrayListPhoto.get(i)));
                //images.append(FilenameUtils.getBaseName(file.getName()));

                if (!(i == arrayListPhoto.size() - 1)) {
                    images.append(",");
                }

                Log.e("images", images.toString());
            }*/
        }
    }

    // method to get the name of the image from the path
    fun getNameOfImage(path: String?): String? {
        val index = path?.lastIndexOf('/')
        return path?.substring(index!! + 1)
    }

    fun decodeSampledBitmapFromPath(path: String?, reqWidth: Int,
                                    reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(path, options)
    }

    fun calculateInSampleSize(options: BitmapFactory.Options,
                              reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            inSampleSize = if (width > height) {
                Math.round(height as Float / reqHeight as Float)
            } else {
                Math.round(width as Float / reqWidth as Float)
            }
        }
        return inSampleSize
    }


    private fun SendDewania() {
        mloading?.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.SendDewania(
                GlobalFunctions.EncodeParameter(et_title?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_title?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_details_label?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_details_label?.getText().toString()),
                if (typeId.equals("0", ignoreCase = true)) dayId else "1",
                areaId,
                "0",
                GlobalFunctions.EncodeParameter(et_full_name?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_request_mobile?.getText().toString()),
                countryCodeRequest,
                ContentActivity.Companion.longitude, ContentActivity.Companion.latitude,
                GlobalFunctions.EncodeParameter(et_mobile?.getText().toString()),
                countryCode,
                GlobalFunctions.EncodeParameter(et_youTube?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_facebook?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_twitter?.getText().toString()),
                GlobalFunctions.EncodeParameter(et_instagram?.getText().toString()),
                GlobalFunctions.EncodeParameter(images.toString()), "false")?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        mloading?.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (Integer.valueOf(outResponse) > 0) {
                                Snackbar.make(mainLayout!!, act?.getString(R.string.ContentAddedLabel).toString(), Snackbar.LENGTH_LONG).show()
                                fragmentManager?.popBackStack()
                            } else if (outResponse == "-1") {
                                Toast.makeText(act, act?.getString(R.string.OperationFailed), Toast.LENGTH_LONG).show()
                            } else if (outResponse == "-2") {
                                Toast.makeText(act, act?.getString(R.string.DataMissing), Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                })
    }

    private fun setEditImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
                relative_image1?.setVisibility(View.GONE)
                relative_image2?.setVisibility(View.GONE)
                relative_image3?.setVisibility(View.GONE)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            1 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhotoEdit?.get(0))
                if (arrayListPhotoEdit?.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2?.setVisibility(View.GONE)
                relative_image3?.setVisibility(View.GONE)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            2 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhotoEdit?.get(0))
                img_select2?.setTag(arrayListPhotoEdit?.get(1))
                if (arrayListPhotoEdit?.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2?.setVisibility(View.VISIBLE)
                if (arrayListPhotoEdit?.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3?.setVisibility(View.GONE)
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            3 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhotoEdit?.get(0))
                if (arrayListPhotoEdit?.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2?.setVisibility(View.VISIBLE)
                img_select2?.setTag(arrayListPhotoEdit?.get(1))
                if (arrayListPhotoEdit?.get(1)!!.contains("http://otabi.")) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3?.setVisibility(View.VISIBLE)
                img_select3?.setTag(arrayListPhotoEdit?.get(2))
                if (arrayListPhotoEdit?.get(2)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                }
                relative_image4?.setVisibility(View.GONE)
                relative_image5?.setVisibility(View.GONE)
            }
            4 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhotoEdit?.get(0))
                if (arrayListPhotoEdit?.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2?.setVisibility(View.VISIBLE)
                img_select2?.setTag(arrayListPhotoEdit?.get(1))
                if (arrayListPhotoEdit?.get(1)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3?.setVisibility(View.VISIBLE)
                img_select3?.setTag(arrayListPhotoEdit?.get(2))
                if (arrayListPhotoEdit?.get(2)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                }
                relative_image4?.setVisibility(View.VISIBLE)
                img_select4?.setTag(arrayListPhotoEdit?.get(3))
                if (arrayListPhotoEdit?.get(3)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                }
                relative_image5?.setVisibility(View.GONE)
            }
            5 -> {
                relative_image1?.setVisibility(View.VISIBLE)
                img_select1?.setTag(arrayListPhotoEdit?.get(0))
                if (arrayListPhotoEdit?.get(0)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select1)
                }
                relative_image2?.setVisibility(View.VISIBLE)
                img_select2?.setTag(arrayListPhotoEdit?.get(1))
                if (arrayListPhotoEdit?.get(1)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(1))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select2)
                }
                relative_image3?.setVisibility(View.VISIBLE)
                img_select3?.setTag(arrayListPhotoEdit?.get(2))
                if (arrayListPhotoEdit?.get(2)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(2))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select3)
                }
                relative_image4?.setVisibility(View.VISIBLE)
                img_select4?.setTag(arrayListPhotoEdit?.get(3))
                if (arrayListPhotoEdit?.get(3)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(3))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select4)
                }
                relative_image5?.setVisibility(View.VISIBLE)
                img_select5?.setTag(arrayListPhotoEdit?.get(4))
                if (arrayListPhotoEdit?.get(4)?.contains("http://otabi.")!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit?.get(4))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select5)
                } else {
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit?.get(4))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photos)
                            .placeholder(R.drawable.add_photos)
                            .into(img_select5)
                }
            }
        }
    }

    private fun saveData() {
        if (tv_category_label?.getText()?.length!! > 0) {
            ContentActivity.Companion.catName = tv_category_label?.getText().toString()
            ContentActivity.Companion.catId = catId!!
        }
        if (tv_type_label?.getText()?.length!! > 0) {
            ContentActivity.Companion.typeName = tv_type_label?.getText().toString()
            ContentActivity.Companion.typeId = typeId!!
            if (typeId.equals("0", ignoreCase = true)) {

                //tv_days_label.setVisibility(View.VISIBLE);
                tv_days_label?.setAlpha(1.0.toFloat())
                et_details_label?.setHint(act?.getString(R.string.DeewaniyaDetails))
                tv_add_ad?.setText(act?.getString(R.string.AddDeewaniya))
            } else {

                //tv_days_label.setVisibility(View.GONE);
                tv_days_label?.setAlpha(0.5F)
                et_details_label?.setHint(act?.getString(R.string.HouseDetailsLabel))
                tv_add_ad?.setText(act?.getString(R.string.AddHouseLabel))

                /*dayId="";
                  tv_days_label.setText("");
                  ContentActivity.dayId="";
                  ContentActivity.dayName="";*/
            }
        }
        if (tv_area_label?.getText()?.length!! > 0) {
            ContentActivity.Companion.areaName = tv_area_label?.getText().toString()
            ContentActivity.Companion.areaId = areaId!!
        }
        if (tv_days_label?.getText()?.length!! > 0) {
            ContentActivity.Companion.dayName = tv_days_label?.getText().toString()
            ContentActivity.Companion.dayId = dayId!!
        }
        if (et_title?.getText()?.length!! > 0) {
            ContentActivity.Companion.title = et_title?.getText().toString()
        }
        if (et_full_name?.getText()?.length!! > 0) {
            ContentActivity.Companion.fullName = et_full_name?.getText().toString()
        }
        if (et_mobile?.getText()?.length!! > 0) {
            ContentActivity.Companion.mobile = et_mobile?.getText().toString()
        }
        if (et_request_mobile?.getText()?.length!! > 0) {
            ContentActivity.Companion.request_mobile = et_request_mobile?.getText().toString()
        }
        if (tv_location_label?.getText()?.length!! > 0) {
            ContentActivity.Companion.location = tv_location_label?.getText().toString()
        }
        if (et_details_label?.getText()?.length!! > 0) {
            ContentActivity.Companion.details = et_details_label?.getText().toString()
        }
        if (tv_date?.getText()?.length!! > 0) {
            ContentActivity.Companion.date = tv_date?.getText().toString()
        }
        if (et_youTube?.getText()?.length!! > 0) {
            ContentActivity.Companion.youTube = et_youTube?.getText().toString()
        }
        if (et_facebook?.getText()?.length!! > 0) {
            ContentActivity.Companion.facebook = et_facebook?.getText().toString()
        }
        if (et_instagram?.getText()?.length!! > 0) {
            ContentActivity.Companion.instagram = et_instagram?.getText().toString()
        }
        if (et_twitter?.getText()?.length!! > 0) {
            ContentActivity.Companion.twitter = et_twitter?.getText().toString()
        }
    }

    private val listener: SlideDateTimeListener? = object : SlideDateTimeListener() {
        override fun onDateTimeSet(date: Date?) {
            tv_date?.setText(mFormatter?.format(date).toString())
            ContentActivity.Companion.date = mFormatter?.format(date).toString()
        }

        // Optional cancel listener
        override fun onDateTimeCancel() {}
    }

    private fun getCategories() {

        //mloading.setVisibility(View.VISIBLE);

        /*QenaatAPICall.getCallingAPIInterface().GetCategoriesByType("0", "1", new Callback<List<GetFamilyTree>>() {
            @Override
            public void success(final List<GetFamilyTree> GetFamilyTree, retrofit.client.Response response) {

                if(GetFamilyTree != null){

                    getCategoriesArrayList.clear();

                    getCategoriesArrayList.addAll(GetFamilyTree);

                }

                mloading.setVisibility(View.INVISIBLE);

            }

            @Override
            public void failure(RetrofitError error) {

                mloading.setVisibility(View.INVISIBLE);

            }

        });*/
    }

    private fun GetAreas() {
        mloading?.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetAreas("0")?.enqueue(
                object : Callback<ArrayList<GetAreas>?> {

                    override fun onFailure(call: Call<ArrayList<GetAreas>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetAreas>?>, response: Response<ArrayList<GetAreas>?>) {
                        if (response.body() != null) {
                            val getAreases = response.body()
                            if (getAreases != null) {
                                getAreasArrayList?.clear()
                                getAreasArrayList?.addAll(getAreases)
                            }
                            mloading?.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.MULTIPLE_PERMISSION_CODE -> {
                    val imageCount = 1
                    saveData()
                    if (mLangSessionManager?.getLang() == "ar") {
                        Toast.makeText(act, act?.getString(R.string.SlideLeft), Toast.LENGTH_LONG).show()
                    }
                    if (isEdit) {
                        if (arrayListPhotoEdit?.size!! < imageCount) {
                            ImagePicker.with(this)
                                    .setFolderMode(false)
                                    .setCameraOnly(false)
                                    .setFolderTitle(act?.resources?.getString(R.string.GalleryLabel)!!)
                                    .setMultipleMode(true)
                                    //.setSelectedImages(config?.getSelectedImages())
                                    .setMaxSize(imageCount - arrayListPhotoEdit?.size!!)
                                    .start()
                        }
                    } else {
                        if (arrayListPhoto?.size!! < imageCount) {
                            ImagePicker.with(this)
                                    .setFolderMode(false)
                                    .setCameraOnly(false)
                                    .setFolderTitle(act?.resources?.getString(R.string.GalleryLabel)!!)
                                    .setMultipleMode(true)
                                    //     .setSelectedImages(config?.getSelectedImages())
                                    .setMaxSize(imageCount - arrayListPhoto?.size!!)
                                    .start()
                        }
                    }
                }
                GlobalFunctions.LOCATION_PERMISSION_CODE -> {
                    ContentActivity.openCurrentLocationFragmentFadeIn()
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act!!, permissionStr))
                mainLayout?.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act!!)
        }
    }

    private fun GetWeekDays() {
        mloading?.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetWeekDays()?.enqueue(
                object : Callback<ArrayList<GetWeekDays?>?> {

                    override fun onFailure(call: Call<ArrayList<GetWeekDays?>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading?.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetWeekDays?>?>, response: Response<ArrayList<GetWeekDays?>?>) {
                        if (response.body() != null) {
                            val getWeekDayses = response.body()
                            if (getWeekDayses != null) {
                                getWeekDaysArrayList?.clear()
                                getWeekDaysArrayList?.addAll(getWeekDayses)
                            }
                        }
                        mloading?.setVisibility(View.INVISIBLE)
                    }
                })
    }

    private fun selectDeewaniya() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        tv_type_label?.setVisibility(View.GONE)
        tv_type_label?.setText(act?.getString(R.string.DeewaniyaLabel))
        ContentActivity.Companion.typeName = act?.getString(R.string.DeewaniyaLabel)!!

        typeId = "0"
        ContentActivity.Companion.typeId = "0"
        //tv_days_label.setVisibility(View.VISIBLE);
        tv_days_label?.setAlpha(1.0.toFloat())
        et_details_label?.setHint(act?.getString(R.string.DeewaniyaDetails))
        tv_add_ad?.setText(act?.getString(R.string.AddDeewaniya))
    }

//    private fun setImagePickerConfig() {
//        config = Config()
//        config.setCameraOnly(false)
//        config.setMultipleMode(true)
//        config.setFolderMode(false)
//        config.setShowCamera(true)
//        config.setMaxSize(Config.MAX_SIZE)
//        config.setDoneTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_action_done))
//        config.setFolderTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_title_folder))
//        config.setImageTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_title_image))
//        config.setSavePath(SavePath.DEFAULT)
//        config.setSelectedImages(ArrayList())
//    }

    companion object {
        protected val TAG = AddDeewaniyaFragment::class.java.simpleName
        var fragment: AddDeewaniyaFragment? = null
        var mSessionManager: SessionManager? = null
        var bitmaps: ArrayList<Bitmap?>? = ArrayList()
        private const val PERMISSION_CODE = 23
        private const val INTENT_REQUEST_GET_IMAGES = 13
        fun newInstance(act: FragmentActivity?): AddDeewaniyaFragment {
            fragment = AddDeewaniyaFragment()
            fragment?.act = act
            return fragment!!
        }
    }
}