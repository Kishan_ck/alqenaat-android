package com.qenaat.app.model

/**
 * Created by DELL on 04/10/2016.
 */
class GetAboutUs {
    var ContentsEn: String? = null
    var ContentsAr: String? = null
    var Id: String? = null

    override fun toString(): String {
        return "ClassPojo [ContentsEn = $ContentsEn, ContentsAr = $ContentsAr, requestForHelpId = $Id]"
    }
}