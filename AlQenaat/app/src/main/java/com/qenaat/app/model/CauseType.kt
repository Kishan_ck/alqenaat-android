package com.qenaat.app.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CauseType(
	val cause: List<CauseItem?>? = null,
	val category: Category? = null
) : Parcelable

@Parcelize
data class Category(
	val TilteAR: String? = null,
	val Id: String? = null,
	val TitleEN: String? = null
) : Parcelable

@Parcelize
data class CauseItem(
	val ParentId: String? = null,
	val TitleAR: String? = null,
	val Id: String? = null,
	val TitleEN: String? = null
) : Parcelable
