package com.qenaat.app.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetPersonTypes
import java.util.*

/**
 * Created by DELL on 13-Feb-18.
 */
class PersonTypesAdapter(a: FragmentActivity, list: ArrayList<GetPersonTypes>) : BaseAdapter() {
    private var inflater: LayoutInflater? = null
    var act: FragmentActivity
    var data: ArrayList<GetPersonTypes>
    var languageSeassionManager: LanguageSessionManager
    var sessionManager: SessionManager

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any? {
        return data.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        return try {
            val viewHolder: ViewHolder?
            if (convertView == null) {
                convertView = inflater?.inflate(R.layout.multiple_select_list, null)
                viewHolder = ViewHolder()
                viewHolder.text1 = convertView?.findViewById<View?>(R.id.tv_title) as TextView?
                viewHolder.tv_order_id = convertView?.findViewById<View?>(R.id.tv_order_id) as TextView?
                viewHolder.img_check = convertView?.findViewById<View?>(R.id.img_check) as ImageView?
                convertView?.tag = viewHolder
            } else {
                viewHolder = convertView.tag as ViewHolder
            }
            if (getItem(position) != null) {
                if (data.get(position).TitleEn.equals(act.getString(R.string.DoneLabel), ignoreCase = true) ||
                        data.get(position).TitleAr.equals(act.getString(R.string.DoneLabel), ignoreCase = true)) {
                    viewHolder.text1?.setTextColor(Color.RED)
                    viewHolder.text1?.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
                    viewHolder.text1?.textSize = 16f
                    viewHolder.text1?.gravity = Gravity.CENTER
                    viewHolder.text1?.setText(data.get(position).TitleEn)
                } else {
                    viewHolder.text1?.gravity = Gravity.CENTER_VERTICAL
                    viewHolder.text1?.textSize = 12f
                    viewHolder.text1?.setTextColor(Color.parseColor("#777777"))
                    viewHolder.text1?.typeface = ContentActivity.Companion.tf
                }
                viewHolder.img_check?.setImageResource(R.drawable.check_02)
                if (data.get(position).IsSelected.equals("1", ignoreCase = true)) {
                    viewHolder.img_check?.setImageResource(R.drawable.checked_02)
                    if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                        viewHolder.text1?.setText(data.get(position).TitleEn + "")
                    } else {
                        viewHolder.text1?.setText(data.get(position).TitleAr + "")
                    }
                    viewHolder.text1?.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
                    //viewHolder.text1.setTextColor(Color.parseColor("#0e9762"));
                }
                viewHolder.img_check?.visibility = View.VISIBLE
                viewHolder.tv_order_id?.typeface = ContentActivity.Companion.tf
                viewHolder.text1?.setTextColor(Color.parseColor("#242b3a"))
                viewHolder.tv_order_id?.visibility = View.GONE
                if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                    viewHolder.text1?.setText(data.get(position).TitleEn)
                } else {
                    viewHolder.text1?.setText(data.get(position).TitleAr)
                }
            }
            convertView
        } catch (e: Exception) {
            Log.e(TAG + " " + " getView: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
            null
        }
    }

    internal inner class ViewHolder {
        var text1: TextView? = null
        var tv_order_id: TextView? = null
        var img_check: ImageView? = null
    }

    companion object {
        protected val TAG = PersonTypesAdapter::class.java.simpleName
        var typeFace: String? = null
        var tf: Typeface? = null
    }

    init {
        act = a
        data = list
        languageSeassionManager = LanguageSessionManager(act)
        sessionManager = SessionManager(act)
        inflater = act
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }
}