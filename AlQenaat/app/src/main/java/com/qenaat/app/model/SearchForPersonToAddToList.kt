package com.qenaat.app.model

/**
 * Created by DELL on 13-Feb-18.
 */
class SearchForPersonToAddToList {
    var CityIds: Array<String?>? = null
    var AreaIds: Array<String?>? = null
    var BranchIds: Array<String?>? = null
    var FamilyNameIds: Array<String?>? = null
    var Gender: String? = null
    var AgeFrom: String? = null
    var AgeTo: String? = null
    var PersonType: String? = null
    var InvitationListId: String? = null
    var pageIndex: String? = null
    var SearchKeyword: String? = null
}