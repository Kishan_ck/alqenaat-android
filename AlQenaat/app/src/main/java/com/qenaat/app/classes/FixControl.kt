package com.qenaat.app.classes

import android.app.Activity
import android.content.Context
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.google.android.material.snackbar.Snackbar
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.regex.Pattern


/**
 * Created by shahbazshaikh on 16/04/16.
 */
object FixControl {

    fun CheckCivilId_12(view: View?): Boolean? {
        return if (view is EditText) {
            if ((view as EditText?)?.getText() != null
                    && (view as EditText?)?.getText().toString().length == 12) {
                true
            } else false
        } else false
    }

    fun CheckMobileNumberIs_8(view: View?): Boolean? {
        return if (view is EditText) {
            if ((view as EditText?)?.getText() != null
                    && (view as EditText?)?.getText().toString().length == 8) {
                true
            } else false
        } else false
    }

    fun isValidEmaillId(email: String?): Boolean {
        return if (email == null) {
            false
        } else {
            Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$").matcher(email).matches()
        }
    }

    fun isValidPassword(pw: String?): Boolean {
        return if (pw == null) {
            false
        } else {
            Pattern.compile("^.*(?=.{6,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$!&]).*$").matcher(pw).matches()
        }
    }

    fun setListViewHeightBasedOnChildren(listView: ListView?) {
        val listAdapter = listView?.getAdapter() ?: return
        var totalHeight = 0
        for (i in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(i, null, listView)
            //listItem.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            listItem.layoutParams = AbsListView.LayoutParams(0, 0)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }
        val params = listView.getLayoutParams()
        params.height = (totalHeight
                + listView?.getDividerHeight() * (listAdapter.count - 1))
        listView?.setLayoutParams(params)
    }

    fun getListViewHeightBasedOnChildren(listView: ListView?): Int {
        val listAdapter = listView?.getAdapter() ?: return 0
        var totalHeight = 0
        for (i in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(i, null, listView)
            //listItem.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            listItem.layoutParams = AbsListView.LayoutParams(0, 0)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }

        //params.height = totalHeight
        //      + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        //listView.setLayoutParams(params);
        return (totalHeight
                + listView.getDividerHeight() * (listAdapter.count - 1))
    }

    fun round(value: Double, places: Int): Double? {
        require(places >= 0)
        var bd: BigDecimal? = BigDecimal(value)
        bd = bd?.setScale(places, RoundingMode.HALF_UP)
        return bd?.toDouble()
    }

    fun round2(price: String?): String? {
        //price = "5250,00";
        //price = Double.parseDouble(price)+"";
        var price = price
        price = price?.replace(".", ",")
        Log.e("price", "" + price)
        val pArray: Array<String?> = price?.split(",".toRegex())?.toTypedArray()!!
        Log.e("pArray", "" + pArray.size)
        Log.e("pArray 0", "" + pArray[0])
        Log.e("pArray [1]", "" + pArray[1])
        if (pArray[1]?.length == 1) {
            pArray[1] = pArray[1].toString() + "00"
            Log.e("pArray 1", "" + pArray[1])
        } else if (pArray[1]?.length == 2) {
            pArray[1] = pArray[1].toString() + "0"
            Log.e("pArray 2", "" + pArray[1])
        } else if (pArray[1]?.length == 3) {
            pArray[1] = pArray[1]
            Log.e("pArray 3", "" + pArray[1])
        } else {
            pArray[1] = pArray[1]?.get(0).toString() + "" + pArray[1]?.get(1) + "" + pArray[1]?.get(2) + ""
            Log.e("pArray 3>", "" + pArray[1])
        }
        Log.e("pArray 1 final", "" + pArray[1])
        val d = (pArray[0].toString() + "." + pArray[1]).toDouble()
        Log.e("pArray d", "" + pArray[0] + "." + pArray[1])
        return pArray[0].toString() + "." + pArray[1] + ""
    }

    /*fun getDrawable(context: Context?, resource: Int): Drawable? {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            context?.getResources()?.getDrawable(resource, context.getTheme())
        } else {
            context?.getResources()?.getDrawable(resource, null)
        }
    }*/

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    fun milliSecondsToTimer(milliseconds: Long): String? {
        var finalTimerString = ""
        var secondsString = ""

        // Convert total duration into time
        val hours = (milliseconds / (1000 * 60 * 60)) as Int
        val minutes = (milliseconds % (1000 * 60 * 60)) as Int / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000) as Int
        // Add hours if there
        if (hours > 0) {
            finalTimerString = "$hours:"
        }

        // Prepending 0 to seconds if it is one digit
        secondsString = if (seconds < 10) {
            "0$seconds"
        } else {
            "" + seconds
        }
        finalTimerString = "$finalTimerString$minutes:$secondsString"

        // return timer string
        return finalTimerString
    }

    fun returnString(text: String?): String {
        return text ?: ""
    }

    fun showToast(context: Context?, text: String?) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun Activity.softWindow() {
        this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    fun Activity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    fun String.checkEmptyString(): Boolean {
        return this.isEmpty()
    }

    fun String.checkLengthString(length: Int): Boolean {
        return toString().trim().length > length
    }

    fun String?.returnStr(): String {
        return this ?: ""
    }

    fun View.getTextValue(): String {
        if (this is EditText)
            return text.toString()
        if (this is TextView)
            return text.toString()
        return ""
    }

    fun View.checkEmpty(): Boolean {
        if (this is EditText)
            return text.toString().trim().isEmpty()
        if (this is TextView)
            return text.toString().trim().isEmpty()
        return false
    }

    fun Activity.showToast(text: String, duration: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, text, duration).show()
    }

    fun View.showSnakeBar(text: String, duration: Int = Snackbar.LENGTH_SHORT) {
        Snackbar.make(this, text, duration).show()
    }

    fun View.checkLength(length: Int): Boolean {
        if (this is EditText)
            return text.toString().trim().length >= length
        if (this is TextView)
            return text.toString().trim().length >= length
        return false
    }

    fun View.checkFixLength(length: Int): Boolean {
        if (this is EditText)
            return text.toString().trim().length == length
        if (this is TextView)
            return text.toString().trim().length == length
        return false
    }

    fun View.checkEmailPattern(): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        if (this is EditText)
            return text.toString().trim().matches(emailPattern.toRegex())
        if (this is TextView)
            return text.toString().trim().matches(emailPattern.toRegex())
        return false
    }

    fun View.putVisibility(value: Int) {
        visibility = value
    }

    fun View.checkVisibility(value: Int): Boolean {
        return visibility == value
    }

    fun String.decode(): String {
        return Base64.decode(this, Base64.DEFAULT).toString(charset("UTF-8"))
    }

    fun String.encode(): String {
        return Base64.encodeToString(this.toByteArray(charset("UTF-8")), Base64.DEFAULT)
    }

    fun String.createPartFromString(): RequestBody? {
        return RequestBody.create("text/plain".toMediaTypeOrNull(), this)
    }

    fun createPartFromFile(partName: String, file: File): MultipartBody.Part? {
        val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }

}