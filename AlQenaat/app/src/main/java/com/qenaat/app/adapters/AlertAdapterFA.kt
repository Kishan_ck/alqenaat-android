package com.qenaat.app.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import java.util.*

/**
 * Created by DELL on 06-Apr-17.
 */
class AlertAdapterFA(a: FragmentActivity?, list: ArrayList<String?>?) : BaseAdapter() {
    private var inflater: LayoutInflater? = null
    var act: FragmentActivity?
    var data: ArrayList<String?>?
    var languageSeassionManager: LanguageSessionManager?
    var sessionManager: SessionManager?
    override fun getCount(): Int {
        return data?.size!!
    }

    override fun getItem(position: Int): Any? {
        return data?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        return try {
            val viewHolder: ViewHolder?
            if (convertView == null) {
                convertView = inflater?.inflate(android.R.layout.simple_list_item_1, null)
                viewHolder = ViewHolder()
                viewHolder.text1 = convertView?.findViewById(android.R.id.text1) as TextView
                viewHolder.text1?.setTextColor(Color.parseColor("#45bf55"))
                viewHolder.text1?.gravity = Gravity.CENTER
                convertView?.tag = viewHolder
            } else {
                viewHolder = convertView.tag as ViewHolder
            }
            if (getItem(position) != null) {
                viewHolder.text1?.text = data?.get(position)
                if (data?.get(position).equals(act?.getString(R.string.CancelLabel), ignoreCase = true)) {
                    viewHolder.text1?.setTextColor(Color.RED)
                    viewHolder.text1?.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
                } else {
                    viewHolder.text1?.setTextColor(Color.parseColor("#009999"))
                    viewHolder.text1?.typeface = ContentActivity.Companion.tf
                }
            }
            convertView
        } catch (e: Exception) {
            Log.e(TAG + " " + " getView: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
            null
        }
    }

    internal inner class ViewHolder {
        var text1: TextView? = null
    }

    companion object {
        protected val TAG = AlertAdapterFA::class.java.simpleName
        var typeFace: String? = null
        var tf: Typeface? = null
    }

    init {
        act = a
        data = list
        languageSeassionManager = LanguageSessionManager(act)
        sessionManager = SessionManager(act!!)
        inflater = act?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        /*if(languageSeassionManager.getLang().equals("en")){
            typeFace = "fonts/verdana.ttf";
            tf = Typeface.createFromAsset(act.getAssets(), typeFace);
        }else{
            // Font Path
            typeFace = "fonts/GEDinarOne-Medium3.ttf";
            // Font typeFace
            tf = Typeface.createFromAsset(act.getAssets(), typeFace);
        }*/
    }
}