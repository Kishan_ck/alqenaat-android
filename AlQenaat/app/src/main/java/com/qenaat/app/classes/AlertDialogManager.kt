package com.qenaat.app.classes

import android.app.AlertDialog
import android.content.Context

object AlertDialogManager {
    /**
     * Function to display simple Alert Dialog
     * @param context - application context
     * @param title - alert dialog title
     * @param message - alert message
     * @param status - success/failure (used to set icon)
     * - pass null if you don't want icon
     */
    fun showAlertDialog(context: Context?, title: String?, message: String?,
                        status: Boolean?) {
        val alertDialog = AlertDialog.Builder(context).create()

        // Setting Dialog Title
        alertDialog.setTitle(title)

        // Setting Dialog Message
        alertDialog.setMessage(message)
        if (status != null) // Setting alert dialog icon
        //			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        // Setting OK Button
            alertDialog.setButton("OK") { dialog, which -> }

        // Showing Alert Message
        alertDialog.show()
    }
}