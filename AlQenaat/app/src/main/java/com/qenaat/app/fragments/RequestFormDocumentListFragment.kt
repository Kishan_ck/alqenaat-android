package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.RequestFromDocumentAdapter
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetAboutUs
import com.qenaat.app.model.GetAttachmentTypes
import com.qenaat.app.model.RequestForHelp
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import kotlinx.android.synthetic.main.top_bar.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 17-Jan-18.
 */
class RequestFormDocumentListFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var loading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var requestFormDocumentArrayList: ArrayList<GetAttachmentTypes> = ArrayList()
    lateinit var requestHelpForm: RequestForHelp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments?.containsKey("RequestForHelp")!!) {
                val gson = Gson()
                requestHelpForm = gson.fromJson(arguments?.getString("RequestForHelp"),
                        RequestForHelp::class.java)
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        loading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.documents)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        /* if (requestFormDocumentArrayList.size > 0) {
             mAdapter = RequestFromDocumentAdapter(act, requestFormDocumentArrayList, requestHelpForm)
             my_recycler_view.setAdapter(mAdapter)
         } else {*/
        GetRequestFromDocuments()
        //}

        // GetTermsAndConditions();
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun GetRequestFromDocuments() {
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        loading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetRequestFromDocuments(
                "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                requestHelpForm.requestForHelpId,
                "0",
                mSessionManager.getUserCode())?.enqueue(
                object : Callback<ArrayList<GetAttachmentTypes>?> {

                    override fun onFailure(call: Call<ArrayList<GetAttachmentTypes>?>, t: Throwable) {
                        t.printStackTrace()
                        loading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetAttachmentTypes>?>, response: Response<ArrayList<GetAttachmentTypes>?>) {
                        if (response.body() != null) {
                            val getServiceTypes = response.body()
                            if (loading != null && getServiceTypes != null) {
                                Log.d("getContentses size", "" + getServiceTypes.size)
                                requestFormDocumentArrayList.clear()
                                requestFormDocumentArrayList.addAll(getServiceTypes)
                                if (getServiceTypes.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = RequestFromDocumentAdapter(act, requestFormDocumentArrayList, requestHelpForm)
                                my_recycler_view.setAdapter(mAdapter)
                            }
                        }
                        loading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    private fun GetTermsAndConditions() {
        QenaatAPICall.getCallingAPIInterface()?.GetAboutUs()?.enqueue(object : Callback<GetAboutUs?> {

            override fun onFailure(call: Call<GetAboutUs?>, t: Throwable) {
                t.printStackTrace()
                loading.setVisibility(View.GONE)
                GlobalFunctions.EnableLayout(mainLayout)
            }

            override fun onResponse(call: Call<GetAboutUs?>, response: Response<GetAboutUs?>) {
                if (response.body() != null) {
                    val aboutUses = response.body()
                    if (loading != null && aboutUses != null) {

                        //Log.d("aboutUses size", ""+aboutUses.size());

                        //if(aboutUses.size()>0){
                        if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                            ContentActivity.Companion.setupMoreIfoDialog(aboutUses.ContentsEn,
                                    aboutUses.ContentsEn)
                        } else {
                            ContentActivity.Companion.setupMoreIfoDialog(aboutUses.ContentsAr,
                                    aboutUses.ContentsAr)
                        }
                    }
                    //}
                    loading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)
                }
            }
        })
    }

    companion object {
        protected val TAG = RequestFormDocumentListFragment::class.java.simpleName
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var act: FragmentActivity
        lateinit var fragment: RequestFormDocumentListFragment

        fun newInstance(act: FragmentActivity): RequestFormDocumentListFragment {
            fragment = RequestFormDocumentListFragment()
            Companion.act = act
            return fragment
        }
    }
}