package com.qenaat.app.model

/**
 * Created by DELL on 31-Oct-17.
 */
class GetServices {
    var Id: String? = null
    var IsApproved: String? = null
    var RequestId: String? = null
    var RequestDate: String? = null
    var UserId: String? = null
    var UserName: String? = null
    var UserEmail: String? = null
    var UserPhone: String? = null
    var NameAr: String? = null
    var FromTime: String? = null
    var ToTime: String? = null
    var NameEn: String? = null
    var TypeNameAr: String? = null
    var TypeNameEn: String? = null
    var DescribtionAr: String? = null
    var DescribtionEn: String? = null
    var ServicePrice: String? = null
    var ServiceTypeId: String? = null
}