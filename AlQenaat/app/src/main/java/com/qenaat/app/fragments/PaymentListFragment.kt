package com.qenaat.app.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.PaymentAdapter
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetPaymentLists
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList


class PaymentListFragment : Fragment() {

    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var getPaymentsArrayList: ArrayList<GetPaymentLists.ListItem> = ArrayList<GetPaymentLists.ListItem>()
//    var type: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
//                type = arguments?.getString("type")!!
            }
        } catch (e: Exception) {
            Log.e(
                TAG + " " + " onCreate: "
                        + Thread.currentThread().stackTrace[2].lineNumber,
                e.message
            )
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout

//        return inflater.inflate(R.layout.service_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
        ContentActivity.Companion.setTextFonts(mainLayout)


    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.payments)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)

//        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
//            if (mSessionManager!!.isLoggedin()
//                && mSessionManager!!.getUserCode() !== "" && mSessionManager!!.getUserCode() != null) {
//                ContentActivity.Companion.clearVariables()
//                val bundle = Bundle()
//                bundle.putString("isEdit", "false")
//                ContentActivity.Companion.openCreateMyListFragment(bundle)
//            } else {
//                mainLayout.showSnakeBar(MonasbatFragment.act.getString(R.string.LoggedIn))
//                ContentActivity.Companion.openLoginFragment()
//            }
//        })

        if (getPaymentsArrayList.size > 0) {
            mAdapter = PaymentAdapter(act!!, getPaymentsArrayList)
            my_recycler_view.setAdapter(mAdapter)
        } else {
            getPaymentList()
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
    }

    fun getPaymentList() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)

        QenaatAPICall.getCallingAPIInterface()?.GetPaymentLists(
            "${ConstanstParameters.AUTH_TEXT} ${mSessionManager?.getAuthToken()}",
            "application/json",
            mSessionManager?.getUserCode(),
            "-1"
        )?.enqueue(
            object : Callback<GetPaymentLists> {

                override fun onFailure(call: Call<GetPaymentLists>, t: Throwable) {
                    t.printStackTrace()
                    mloading.setVisibility(View.GONE)
                    GlobalFunctions.EnableLayout(mainLayout)

                    Log.e("paayyymeennnntttt11---", "" + call.request().body)
                    Log.e("paayyymeennnntttt12---", "" + call.request().url)
                    Log.e("paayyymeennnntttt13---", "" + t.message)


                }

                override fun onResponse(
                    call: Call<GetPaymentLists>,
                    response: Response<GetPaymentLists>
                ) {
                    if (response.body() != null) {
                        val getPayment = response.body()

                        Log.e("paayyymeennnntttt11", "" + call.request().body)
                        Log.e("paayyymeennnntttt12", "" + call.request().url)
                        Log.e("paayyymeennnntttt13", "" + response.body())
                        Log.e("paayyymeennnntttt15", "" + response.code())
                        Log.e("paayyymeennnntttt14", "" + response.errorBody()?.string())

//                        getPayment?.list?.forEach {
//                            Log.e("rohannnnnnnnnn111" , ""+it?.PaymentMethodId);
//                            Log.e("rohannnnnnnnnn222" , ""+it?.DonationNumber);
//                        }


                        if (mloading != null && getPayment != null) {
//                            Log.d("getContentses size", "" + getHalls.size)


                            getPaymentsArrayList?.clear()
                            getPayment.list?.forEach {
                                getPaymentsArrayList?.add(it)
                            }

                            getPaymentsArrayList.forEach {
                                Log.e("dataaa",""+it.CatName)
                            }
                            if(getPaymentsArrayList.size == 0) tv_noDataFound?.putVisibility(View.VISIBLE)
                            mAdapter = PaymentAdapter(act!!, getPaymentsArrayList)
                            my_recycler_view.setAdapter(mAdapter)
                        }
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                }
            })
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {

        protected val TAG = PaymentListFragment::class.java.simpleName
        var mSessionManager: SessionManager? = null
        var languageSeassionManager: LanguageSessionManager? = null
        var act: FragmentActivity? = null
        var fragment: PaymentListFragment? = null
        fun newInstance(act: FragmentActivity?): PaymentListFragment? {
            fragment = PaymentListFragment()
            Companion.act = act
            return fragment
        }
    }


}


