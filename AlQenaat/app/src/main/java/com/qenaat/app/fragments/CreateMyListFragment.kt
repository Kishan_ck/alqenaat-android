package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetInvitationLists
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader

/**
 * Created by DELL on 11-Feb-18.
 */
class CreateMyListFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var et_name: EditText
    lateinit var tv_create: TextView
    var isEdit = false
    lateinit var getInvitationLists: GetInvitationLists
    lateinit var relative_type: RelativeLayout
    lateinit var checkbox: CheckBox
    lateinit var tv_type: TextView
    var isPublic = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                if (arguments!!.getString("isEdit").equals("true", ignoreCase = true)) {
                    isEdit = true
                    if (arguments!!.containsKey("GetInvitationLists")) {
                        val gson = Gson()
                        getInvitationLists = gson.fromJson(arguments!!.getString("GetInvitationLists"),
                                GetInvitationLists::class.java)
                    }
                }
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.create_my_list, null) as RelativeLayout
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            tv_type = mainLayout.findViewById(R.id.tv_type) as TextView
            checkbox = mainLayout.findViewById(R.id.checkbox) as CheckBox
            relative_type = mainLayout.findViewById(R.id.relative_type) as RelativeLayout
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            et_name = mainLayout.findViewById(R.id.et_name) as EditText
            tv_create = mainLayout.findViewById(R.id.tv_create) as TextView
            tv_create.setOnClickListener(this)
            relative_type.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)
        }
    }

    override fun onStart() {
        super.onStart()
        //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle.setText(R.string.MyListLabel)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
//        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

//        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
//        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        setData()
        checkbox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) {
                isPublic = if (b) {
                    true
                } else {
                    false
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.relative_type -> {
            }
            R.id.tv_create ->                 //Snackbar.make(mainLayout, "Login please wait", Snackbar.LENGTH_LONG).show();
                if (!et_name.checkEmpty()) {
                    mloading.setVisibility(View.VISIBLE)
                    GlobalFunctions.DisableLayout(mainLayout)
                    if (isEdit) {
                        QenaatAPICall.getCallingAPIInterface()?.EditInvitationList(
                                "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                                getInvitationLists.Id,
                                GlobalFunctions.EncodeParameter(et_name.getText().toString()),
                                mSessionManager.getUserCode(),
                                if (isPublic) "true" else "false")?.enqueue(
                                object : Callback<ResponseBody?> {

                                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                        t.printStackTrace()
                                        mloading.setVisibility(View.INVISIBLE)
                                        GlobalFunctions.EnableLayout(mainLayout)
                                    }

                                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                        val body = response?.body()
                                        var outResponse = ""
                                        try {
                                            val reader = BufferedReader(InputStreamReader(
                                                    ByteArrayInputStream(body?.bytes())))
                                            val out = StringBuilder()
                                            val newLine = System.getProperty("line.separator")
                                            var line: String?
                                            while (reader.readLine().also { line = it } != null) {
                                                out.append(line)
                                                out.append(newLine)
                                            }
                                            outResponse = out.toString()
                                            Log.d("outResponse", "" + outResponse)
                                        } catch (ex: Exception) {
                                            ex.printStackTrace()
                                        }
                                        if (outResponse != null) {
                                            outResponse = outResponse.replace("\"", "")
                                            outResponse = outResponse.replace("\n", "")
                                            Log.e("outResponse not null ", outResponse)
                                            if (outResponse.toInt() > 0) {
                                                fragmentManager?.popBackStack()
                                            } else if (outResponse == "-1") {
                                                Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                            }
                                        }
                                        mloading.setVisibility(View.INVISIBLE)
                                        GlobalFunctions.EnableLayout(mainLayout)
                                    }
                                })
                    } else {
                        QenaatAPICall.getCallingAPIInterface()?.AddInvitationList(
                                "$AUTH_TEXT ${mSessionManager.getAuthToken()}",
                                GlobalFunctions.EncodeParameter(et_name.getText().toString()),
                                mSessionManager.getUserCode(),
                                if (isPublic) "true" else "false")?.enqueue(
                                object : Callback<ResponseBody?> {

                                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                        t.printStackTrace()
                                        mloading.setVisibility(View.INVISIBLE)
                                        GlobalFunctions.EnableLayout(mainLayout)
                                    }

                                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                        val body = response?.body()
                                        var outResponse = ""
                                        try {
                                            val reader = BufferedReader(InputStreamReader(
                                                    ByteArrayInputStream(body?.bytes())))
                                            val out = StringBuilder()
                                            val newLine = System.getProperty("line.separator")
                                            var line: String?
                                            while (reader.readLine().also { line = it } != null) {
                                                out.append(line)
                                                out.append(newLine)
                                            }
                                            outResponse = out.toString()
                                            Log.d("outResponse", "" + outResponse)
                                        } catch (ex: Exception) {
                                            ex.printStackTrace()
                                        }
                                        if (outResponse != null) {
                                            outResponse = outResponse.replace("\"", "")
                                            outResponse = outResponse.replace("\n", "")
                                            Log.e("outResponse not null ", outResponse)
                                            if (outResponse.toInt() > 0) {
                                                fragmentManager?.popBackStack()
                                            } else if (outResponse == "-1") {
                                                mainLayout.showSnakeBar(getString(R.string.OperationFailed))
                                            } else if (outResponse == "-2") {
                                                mainLayout.showSnakeBar(getString(R.string.DataMissing))
                                            } else if (outResponse == "-3") {
                                                mainLayout.showSnakeBar(getString(R.string.group_name_exists))
                                            }
                                        }
                                        mloading.setVisibility(View.INVISIBLE)
                                        GlobalFunctions.EnableLayout(mainLayout)
                                    }
                                })
                    }
                } else {
                    Snackbar.make(mainLayout, act.getString(R.string.enter_group_name), Snackbar.LENGTH_LONG).show()
                }
        }
    }

    private fun setData() {

        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)


        if (isEdit) {
            tv_create.setText(act.getString(R.string.SaveLabel))
            if (getInvitationLists != null) {
                et_name.setText(getInvitationLists.Name)
                if (getInvitationLists.IsPublic.equals("true", ignoreCase = true)) {
                    isPublic = true
                    checkbox.setChecked(true)
                } else {
                    isPublic = false
                    checkbox.setChecked(false)
                }
            }
        }
    }

    companion object {
        protected val TAG = CreateMyListFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: CreateMyListFragment
        fun newInstance(act: FragmentActivity): CreateMyListFragment {
            fragment = CreateMyListFragment()
            Companion.act = act
            return fragment
        }
    }
}