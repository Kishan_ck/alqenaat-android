package com.qenaat.app.model

/**
 * Created by DELL on 15-Jan-18.
 */
class RequestForHelpPersons {
    var Name: String? = null
    var Gender: String? = null
    var DateOFBirth: String? = null
    var CivilIdNo: String? = null
    var RelationShip: String? = null
    var RequestForHelpId: String? = null
    var Id: String? = null
    var Notes: String? = null
}