package com.qenaat.app.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader

/**
 * Created by shahbazshaikh on 18/07/16.
 */
class ForgotPasswordFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var et_username: EditText
    lateinit var tv_forgot_password: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.forgot_password, null) as RelativeLayout
            act?.softWindow()
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout?) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            et_username = mainLayout.findViewById(R.id.et_username) as EditText
            tv_forgot_password = mainLayout.findViewById(R.id.tv_forgot_password) as TextView
            tv_forgot_password.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_forgot_password.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            et_username.setTypeface(ContentActivity.Companion.tf)
        }
    }

    override fun onStart() {
        super.onStart()
        //    ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.mtv_topTitle.setText(R.string.ForgetLabel_)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.tv_forgot_password -> {
                act?.hideKeyboard()
                if (!et_username.checkEmpty()) {
                    mloading.setVisibility(View.VISIBLE)
                    GlobalFunctions.DisableLayout(mainLayout)
                    QenaatAPICall.getCallingAPIInterface()?.forgetPassword(
                            et_username.getText().toString())?.enqueue(
                            object : Callback<ResponseBody?> {

                                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                    t.printStackTrace()
                                    mloading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                    val body = response?.body()
                                    var outResponse = ""
                                    try {
                                        val reader = BufferedReader(InputStreamReader(
                                                ByteArrayInputStream(body?.bytes())))
                                        val out = StringBuilder()
                                        val newLine = System.getProperty("line.separator")
                                        var line: String?
                                        while (reader.readLine().also { line = it } != null) {
                                            out.append(line)
                                            out.append(newLine)
                                        }
                                        outResponse = out.toString()
                                        Log.d("outResponse", "" + outResponse)
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                    if (outResponse != null) {
                                        outResponse = outResponse.replace("\"", "")
                                        outResponse = outResponse.replace("\n", "")
                                        Log.e("outResponse not null ", outResponse)
                                        if (outResponse.toInt() > 0) {
                                            fragmentManager?.popBackStack()
                                            Snackbar.make(mainLayout, act?.getString(R.string.send_success)!!, Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-1") {
                                            Snackbar.make(mainLayout, act?.getString(R.string.OperationFailed)!!, Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-2") {
                                            Snackbar.make(mainLayout, act?.getString(R.string.DataMissing)!!, Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-3") {
                                            Snackbar.make(mainLayout, act?.getString(R.string.UserNotExistsLabel)!!, Snackbar.LENGTH_LONG).show()
                                        }
                                    }
                                    mloading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                } else {
                    mainLayout.showSnakeBar(getString(R.string.enter_email_or_username))
                    et_username.requestFocus()
                }
            }
        }
    }

    companion object {
        protected val TAG = ForgotPasswordFragment::class.java.simpleName
        var act: FragmentActivity? = null
        var fragment: ForgotPasswordFragment? = null
        fun newInstance(act: FragmentActivity?): ForgotPasswordFragment {
            fragment = ForgotPasswordFragment()
            Companion.act = act
            return fragment!!
        }
    }
}