package com.qenaat.app.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.checkLength
import com.qenaat.app.classes.FixControl.encode
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader

/**
 * Created by shahbazshaikh on 31/07/16.
 */
class ChangePasswordFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var et_old_password: EditText
    lateinit var et_new_password: EditText
    lateinit var et_confirm_password: EditText
    lateinit var tv_change_password: TextView
    var type: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                type = arguments?.getString("type")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.change_password, null) as RelativeLayout
            act.softWindow()
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            et_old_password = mainLayout.findViewById(R.id.et_old_password) as EditText
            et_new_password = mainLayout.findViewById(R.id.et_new_password) as EditText
            et_confirm_password = mainLayout.findViewById(R.id.et_confirm_password) as EditText
            tv_change_password = mainLayout.findViewById(R.id.tv_change_password) as TextView
            tv_change_password.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)
            tv_change_password.setTypeface(ContentActivity.Companion.tf, Typeface.BOLD)
            et_old_password.setTypeface(ContentActivity.Companion.tf)
            et_new_password.setTypeface(ContentActivity.Companion.tf)
            et_confirm_password.setTypeface(ContentActivity.Companion.tf)
        }
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.mtv_topTitle.setText(act.getString(R.string.ChangePassword))
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }


    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.tv_change_password ->
                if (isValid()/*et_old_password.getText().length > 0
                        && et_new_password.getText().length > 0
                        && et_confirm_password.getText().length > 0*/) {
                    //if (et_new_password.getText().toString().length >= 6) {
                    //if (et_new_password.getText().toString() == et_confirm_password.getText().toString()) {
                    mloading.setVisibility(View.VISIBLE)
                    GlobalFunctions.DisableLayout(mainLayout)
                    QenaatAPICall.getCallingAPIInterface()?.changePassword(
                            "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                            mSessionManager.getUserCode(),
                            et_old_password.getTextValue().encode().encode(),
                            et_new_password.getTextValue().encode().encode())?.enqueue(
                            object : Callback<ResponseBody?> {

                                override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                    t.printStackTrace()
                                    mloading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                    val body = response?.body()
                                    var outResponse = ""
                                    try {
                                        val reader = BufferedReader(InputStreamReader(
                                                ByteArrayInputStream(body?.bytes())))
                                        val out = StringBuilder()
                                        val newLine = System.getProperty("line.separator")
                                        var line: String?
                                        while (reader.readLine().also { line = it } != null) {
                                            out.append(line)
                                            out.append(newLine)
                                        }
                                        outResponse = out.toString()
                                        Log.d("outResponse", "" + outResponse)
                                    } catch (ex: Exception) {
                                        ex.printStackTrace()
                                    }
                                    if (outResponse != null) {
                                        outResponse = outResponse.replace("\"", "")
                                        outResponse = outResponse.replace("\n", "")
                                        Log.e("outResponse not null ", outResponse)
                                        if (outResponse.toInt() > 0) {
                                            Snackbar.make(mainLayout, act.getString(R.string.PassworduUpdated), Snackbar.LENGTH_LONG).show()
                                            fragmentManager?.popBackStack()
                                        } else if (outResponse == "-1") {
                                            Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-2") {
                                            Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-3") {
                                            Snackbar.make(mainLayout, act.getString(R.string.UserNotExistsLabel), Snackbar.LENGTH_LONG).show()
                                        } else if (outResponse == "-4") {
                                            Snackbar.make(mainLayout, act.getString(R.string.WrongPasswordLabel), Snackbar.LENGTH_LONG).show()
                                        }
                                    }
                                    mloading.setVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                    /* }
                     else {
                         Snackbar.make(mainLayout, act.getString(R.string.PasswordError), Snackbar.LENGTH_LONG).show()
                     }*/
                    /*} else {
                        Toast.makeText(act, getString(R.string.NotValidPassword), Toast.LENGTH_LONG).show()
                    }*/
                } /*else {
                    Toast.makeText(act, act.getString(R.string.FillAllFields), Toast.LENGTH_LONG).show()
                }*/
        }
    }

    private fun isValid(): Boolean {
        when {
            et_old_password.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_old_password))
                et_old_password.requestFocus()
                return false
            }
            et_new_password.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_new_password))
                et_new_password.requestFocus()
                return false
            }
            !et_new_password.checkLength(6) -> {
                mainLayout.showSnakeBar(getString(R.string.NotValidPassword))
                et_new_password.requestFocus()
                return false
            }
            et_confirm_password.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_confirm_password))
                et_confirm_password.requestFocus()
                return false
            }
            et_new_password.getTextValue() != et_confirm_password.getTextValue() -> {
                mainLayout.showSnakeBar(getString(R.string.PasswordError))
                return false
            }
        }
        return true
    }

    companion object {
        protected val TAG = ChangePasswordFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: ChangePasswordFragment
        fun newInstance(act: FragmentActivity): ChangePasswordFragment {
            fragment = ChangePasswordFragment()
            Companion.act = act
            return fragment
        }
    }
}