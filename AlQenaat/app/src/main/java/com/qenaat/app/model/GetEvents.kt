package com.qenaat.app.model

/**
 * Created by DELL on 31-Oct-17.
 */
class GetEvents {
    var Id: String? = null
    var isInvited: String? = null
    var loginUserId: String? = null
    var InvitationListId: String? = null
    var MonthTitle: String? = null
    var EventTypeNameAr: String? = null
    var EventTypeNameEn: String? = null
    var EventNameAr: String? = null
    var EventNameEn: String? = null
    var EventDate: String? = null
    var Color: String? = null
    var RequestFullName: String? = null
    var RequestPhone: String? = null
    var Photo: String? = null
    var Facebook: String? = null
    var Instagram: String? = null
    var Twitter: String? = null
    var YouTube: String? = null
    var Latitude: String? = null
    var Longitude: String? = null
    var Phone: String? = null
    var ViewersCount: String? = null
    var AddedOn: String? = null
    var DetailsEn: String? = null
    var DetailsAr: String? = null
    var MonthNameEn: String? = null
    var MonthNameAr: String? = null
    var DayNameEn: String? = null
    var DayNameAr: String? = null
    var EventAfterEn: String? = null
    var EventAfterAr: String? = null
}