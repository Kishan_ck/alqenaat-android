package com.qenaat.app

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Window
import android.view.WindowManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.firebase.iid.FirebaseInstanceId
import com.qenaat.app.classes.*
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

class SplashActivity : Activity() {
    private val splashDuration = 2000
    var SENDER_ID: String? = "611914818036"
    var mLangSessionManager: LanguageSessionManager? = null
    var sessionManager: SessionManager? = null
    var deviceId: String? = null
    private var isBackButtonPressed = false
    var context: Context? = null
    var regid: String? = null
    var msgId: AtomicInteger? = AtomicInteger()

    // Internet detector
    var cd: ConnectionDetector? = null

    var android_id: String? = null
    var shared_pref: SharedPreferences? = null
    protected override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }

    protected override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        // Removes notification bar

//        Log.e("okokokok",""+System.currentTimeMillis())
//        val cal = Calendar.getInstance()
//        cal.add(Calendar.MONTH, 3)
////        cal.timeInMillis
//        cal.time
//        Log.e("okokokok",""+cal)

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mLangSessionManager = LanguageSessionManager(this)
        sessionManager = SessionManager(this)
        shared_pref = getSharedPreferences("gcm", 0)
        // pending
        cd = ConnectionDetector(getApplicationContext())
        updateViews(mLangSessionManager?.getLang())
        if (checkPlayServices()) {
            regid = FirebaseInstanceId.getInstance().getToken()
            Log.d("Refreshed", "Refreshed token: $regid")
            if (regid != null) {
                registerInBackground()
            } else {
                val handler = Handler()
                handler.postDelayed({
                    if (!isBackButtonPressed) {
                        startActivity(Intent(this@SplashActivity, ContentActivity::class.java)
                                .putExtra("com.qenaat.fragType", 1))
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        finish()
                    }
                }, 2000)
            }
        } else {
            val handler = Handler()
            handler.postDelayed({
                if (!isBackButtonPressed) {
                    startActivity(Intent(this@SplashActivity, ContentActivity::class.java)
                            .putExtra("com.qenaat.fragType", 1))
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                    finish()
                }
            }, 2000)
        }
    }

    override fun onBackPressed() {
        isBackButtonPressed = true
        super.onBackPressed()
    }

    /**
     * Registers the application with GCM servers asynchronously.
     *
     *
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private fun registerInBackground() {
        object : AsyncTask<Void?, Void?, String?>() {
            protected override fun doInBackground(vararg params: Void?): String? {
                var msg = ""
                //try {
                /*if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);*/regid = FirebaseInstanceId.getInstance().getToken()
                msg = "Device registered, registration ID=$regid"

                //} catch (IOException ex) {
                //msg = "Error :" + ex.getMessage();
                // If there is an error, don't just keep trying to register.
                // Require the user to click a button again, or perform
                // exponential back-off.
                //}
                return msg
            }

            protected override fun onPostExecute(msg: String?) {
                Log.e("registerationid Splash ", regid)
                QenaatAPICall.getCallingAPIInterface()?.insertToken(
                        if (sessionManager?.isLoggedin()!!) "${ConstanstParameters.AUTH_TEXT} ${sessionManager?.getAuthToken()}" else "",
                        regid, "2",
                        AppController.Companion.getInstance()?.getIMEI(),
                        if (sessionManager?.getUserCode()?.length!! > 0) sessionManager?.getUserCode() else "0")?.enqueue(
                        object : Callback<ResponseBody?> {
                            override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                                t.printStackTrace()
                                val handler = Handler()
                                handler.postDelayed({
                                    if (!isBackButtonPressed) {
                                        startActivity(Intent(this@SplashActivity, ContentActivity::class.java)
                                                .putExtra("com.qenaat.fragType", 1))
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                        finish()
                                    }
                                }, 2000)
                            }

                            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                                val body = response?.body()
                                try {
                                    val reader = BufferedReader(InputStreamReader(
                                            ByteArrayInputStream(body?.bytes())))
                                    val out = StringBuilder()
                                    val newLine = System.getProperty("line.separator")
                                    var line: String?
                                    while (reader.readLine().also { line = it } != null) {
                                        out.append(line)
                                        out.append(newLine)
                                    }
                                    val outResponse = out.toString()
                                    Log.d("outResponse", "" + outResponse)
                                    mLangSessionManager?.setRegId(regid)
                                } catch (ex: Exception) {
                                    ex.printStackTrace()
                                }

                                //if(sessionManager.getIsPopupShownBefore()){
                                val handler = Handler()
                                handler.postDelayed({
                                    startActivity(Intent(this@SplashActivity, ContentActivity::class.java)
                                            .putExtra("com.otaban.fragType", 3))
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                                    finish()
                                }, 2000)

//                        }
//                        else{
//
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//
//                                @Override
//                                public void run() {
//                                    startActivity(new Intent(SplashActivity.this, ContentActivity.class)
//                                            .putExtra("com.otaban.fragType", 2));
//                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                                    finish();
//                                }
//                            }, 2000);
//
//                        }
                            }
                        })
            }
        }.execute(null, null, null)
    }

    /**
     * Gets the current registration ID for application on GCM service, if there
     * is one.
     *
     *
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private fun getRegistrationId(context: Context?): String? {
        val prefs: SharedPreferences? = getGcmPreferences(context)
        val registrationId: String = prefs?.getString(PROPERTY_REG_ID, "")!!
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.")
            return ""
        }

        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        val registeredVersion: Int = prefs.getInt(PROPERTY_APP_VERSION, Int.MIN_VALUE)
        val currentVersion = getAppVersion(context)
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.")
            return ""
        }
        return registrationId
    }

    /**
     * @return Application's `SharedPreferences`.
     */
    private fun getGcmPreferences(context: Context?): SharedPreferences? {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(SplashActivity::class.java.simpleName, Context.MODE_PRIVATE)
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If it
     * doesn't, display a dialog that allows users to download the APK from the
     * Google Play Store or enable it in the device's system settings.
     */
    private fun checkPlayServices(): Boolean {
        val resultCode: Int = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getApplicationContext())
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            } else {
            }
            return false
        }
        return true
    }

    private fun updateViews(languageCode: String?) {
        LocaleHelper.setLocale(this, languageCode)
    }

    companion object {
        val EXTRA_MESSAGE: String? = "message"
        val PROPERTY_REG_ID: String? = "registration_id"
        private val PROPERTY_APP_VERSION: String? = "appVersion"

        /**
         * Tag used on log messages.
         */
        val TAG: String? = "GCM Demo"

        /**
         * @return Application's version code from the `PackageManager`.
         */
        private fun getAppVersion(context: Context?): Int {
            return try {
                val packageInfo: PackageInfo? = context?.getPackageManager()
                        ?.getPackageInfo(context.getPackageName(), 0)
                packageInfo?.versionCode!!
            } catch (e: PackageManager.NameNotFoundException) {
                // should never happen
                throw RuntimeException("Could not get package name: $e")
            }
        }
    }
}