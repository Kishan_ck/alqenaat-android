package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.HallAdapter
import com.qenaat.app.classes.ConstanstParameters.AUTH_TEXT
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetHalls
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 09-Nov-17.
 */
class HallFragment : Fragment() {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var mloading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    var page_index = 0
    var endOfREsults = false
    private var loading_flag = true
    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0
    var getHallsArrayList: ArrayList<GetHalls> = ArrayList<GetHalls>()
    var type: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
                type = arguments?.getString("type")!!
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        mloading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.HallsLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
//        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

//        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
//        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)

        if (type.equals("my account", ignoreCase = true)) {
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)

        } else {

            ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
            ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)
        }

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        if (getHallsArrayList.size > 0) {
            mAdapter = HallAdapter(act!!, getHallsArrayList, type)
            my_recycler_view.setAdapter(mAdapter)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else {
            if (type.equals("my account", ignoreCase = true)) {
                GetBookedHalls()

                ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)
                ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
                ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)

            } else {
                GetHalls()
            }
        }
    }

    fun GetHalls() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetHalls("0", "" + page_index)?.enqueue(
                object : Callback<ArrayList<GetHalls>> {

                    override fun onFailure(call: Call<ArrayList<GetHalls>>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetHalls>>, response: Response<ArrayList<GetHalls>>) {
                        if (response.body() != null) {
                            val getHalls = response.body()
                            if (mloading != null && getHalls != null) {
                                Log.d("getContentses size", "" + getHalls.size)
                                getHallsArrayList.clear()
                                getHallsArrayList.addAll(getHalls)
                                if (getHalls.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = HallAdapter(act!!, getHallsArrayList, type)
                                my_recycler_view.setAdapter(mAdapter)
                                my_recycler_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                        if (!endOfREsults) {
                                            visibleItemCount = mLayoutManager.getChildCount()
                                            totalItemCount = mLayoutManager.getItemCount()
                                            pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                                            if (loading_flag) {
                                                if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                    if (getHallsArrayList.size != 0) {
                                                        progress_loading_more.setVisibility(View.VISIBLE)
                                                        //GlobalFunctions.DisableLayout(mainLayout);
                                                    }
                                                    loading_flag = false
                                                    page_index = page_index + 1
                                                    GetMoreHalls()
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    fun GetMoreHalls() {
        QenaatAPICall.getCallingAPIInterface()?.GetHalls("0", "" + page_index)?.enqueue(
                object : Callback<ArrayList<GetHalls>> {

                    override fun onFailure(call: Call<ArrayList<GetHalls>>, t: Throwable) {
                        t.printStackTrace()
                        progress_loading_more.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetHalls>>, response: Response<ArrayList<GetHalls>>) {
                        if (response.body() != null) {
                            val getHalls = response.body()
                            if (progress_loading_more != null) {
                                progress_loading_more.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                                if (getHalls != null) {
                                    if (getHalls.isEmpty()) {
                                        endOfREsults = true
                                        page_index = page_index - 1
                                    }
                                    loading_flag = true
                                    getHallsArrayList.addAll(getHalls)
                                    mAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                })
    }

    fun GetBookedHalls() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetBookedHalls(
                "$AUTH_TEXT ${mSessionManager?.getAuthToken()}",
                "0",
                mSessionManager?.getUserCode(),
                "-1")?.enqueue(
                object : Callback<ArrayList<GetHalls>> {

                    override fun onFailure(call: Call<ArrayList<GetHalls>>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetHalls>>, response: Response<ArrayList<GetHalls>>) {
                        if (response.body() != null) {
                            val getHalls = response.body()
                            if (mloading != null && getHalls != null) {
                                Log.d("getContentses size", "" + getHalls.size)
                                getHallsArrayList.clear()
                                getHallsArrayList.addAll(getHalls.reversed())
                                if (getHalls.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = HallAdapter(act!!, getHallsArrayList, type)
                                my_recycler_view.setAdapter(mAdapter)
                            }
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        protected val TAG = HallFragment::class.java.simpleName
        var mSessionManager: SessionManager? = null
        var languageSeassionManager: LanguageSessionManager? = null
        var act: FragmentActivity? = null
        var fragment: HallFragment? = null
        fun newInstance(act: FragmentActivity?): HallFragment? {
            fragment = HallFragment()
            Companion.act = act
            return fragment
        }
    }
}