package com.qenaat.app.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.classes.ConstanstParameters.API_SUCCESS_CODE
import com.qenaat.app.classes.FixControl.checkEmpty
import com.qenaat.app.classes.FixControl.encode
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetAppSettings
import com.qenaat.app.model.RegisterModel
import com.qenaat.app.networking.QenaatAPICall

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by DELL on 13-Nov-17.
 */
class LoginFragment : Fragment(), View.OnClickListener {
    lateinit var XY: IntArray
    lateinit var mainLayout: RelativeLayout
    lateinit var mSessionManager: SessionManager
    lateinit var languageSeassionManager: LanguageSessionManager
    lateinit var mloading: ProgressBar
    lateinit var et_username: EditText
    lateinit var et_password: EditText
    lateinit var mtv_login: TextView
    lateinit var mtv_register: TextView
    lateinit var tv_or: TextView
    lateinit var tv_forgotpassword: TextView
    lateinit var tv_activate: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (act == null) {
            act = activity
        }
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act!!)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.login, null) as RelativeLayout
            act?.softWindow()
            initViews(mainLayout)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    fun initViews(mainLayout: RelativeLayout) {
        if (mainLayout != null) {
            mloading = mainLayout.findViewById<View?>(R.id.loading) as ProgressBar
            et_username = mainLayout.findViewById<View?>(R.id.et_username) as EditText
            et_password = mainLayout.findViewById<View?>(R.id.et_password) as EditText
            tv_activate = mainLayout.findViewById<View?>(R.id.tv_activate) as TextView
            tv_or = mainLayout.findViewById<View?>(R.id.tv_or) as TextView
            tv_forgotpassword = mainLayout.findViewById<View?>(R.id.tv_forgotpassword) as TextView
            tv_forgotpassword.setOnClickListener(this)
            tv_activate.setOnClickListener(this)
            mtv_login = mainLayout.findViewById<View?>(R.id.tv_login) as TextView
            mtv_login.setOnClickListener(this)
            mtv_register = mainLayout.findViewById<View?>(R.id.tv_register) as TextView
            mtv_register.setOnClickListener(this)
            ContentActivity.Companion.setTextFonts(mainLayout)
            if (languageSeassionManager.getLang().equals("en", ignoreCase = true)) {
                et_password.setGravity(Gravity.START or Gravity.CENTER_VERTICAL)
                et_username.setGravity(Gravity.START or Gravity.CENTER_VERTICAL)
                et_username.textDirection = View.TEXT_DIRECTION_LTR
                et_password.textDirection = View.TEXT_DIRECTION_LTR

            } else {
                et_password.setGravity(Gravity.RIGHT or Gravity.CENTER_VERTICAL)
                et_username.setGravity(Gravity.RIGHT or Gravity.CENTER_VERTICAL)
                et_username.textDirection = View.TEXT_DIRECTION_RTL
                et_password.textDirection = View.TEXT_DIRECTION_RTL

            }
        }
    }

    override fun onStart() {
        super.onStart()
//        if (mSessionManager.isLoggedin()
//                && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
//            fragmentManager?.popBackStack()
//        }
        //   ContentActivity.Companion.topBarView.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        getSettings()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.LoginLabel)
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)

    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.tv_activate -> ContentActivity.Companion.openActivateAccountFragment()
            R.id.tv_forgotpassword -> ContentActivity.Companion.openForgotPasswordFragment()
            R.id.tv_login -> {
                act?.hideKeyboard()
                if (isValid()) {
                    mloading.putVisibility(View.VISIBLE)
                    GlobalFunctions.DisableLayout(mainLayout)
                    QenaatAPICall.getCallingAPIInterface()?.login(
                            et_username.getTextValue().encode().encode(),
                            et_password.getTextValue().encode().encode(),
                            languageSeassionManager.getRegId()?.encode()?.encode())?.enqueue(
                            object : Callback<RegisterModel> {

                                override fun onFailure(call: Call<RegisterModel>, t: Throwable) {
                                    t.printStackTrace()
                                    mloading.putVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }

                                override fun onResponse(call: Call<RegisterModel>, response: Response<RegisterModel>) {
                                    if (response.body() != null) {
                                        if (response.code() == API_SUCCESS_CODE) {
                                            val registerModel = response.body()
                                            when (registerModel?.code) {
                                                "0" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.successfully_login)!!)
                                                    mSessionManager.LoginSeassion()
                                                    mSessionManager.setAuthToken(registerModel.token)
                                                    mSessionManager.setUserCode(registerModel.Id.toString())
                                                    mSessionManager.setUserName(et_username.getTextValue())
                                                    mSessionManager.setUserPassword(et_password.getTextValue())

                                                    Log.e("userid>>>>",""+registerModel.Id.toString())
//                                                    fragmentManager?.popBackStack()
//                                                    val bundle = Bundle()
//                                                    bundle.putString("comingFrom", "home")
//                                                    ContentActivity.openNewsFragment(bundle)

                                                    var intent = Intent(activity as Context, ContentActivity::class.java)
                                                    intent.putExtra("isfromLogin",true)
                                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                    startActivity(intent)
                                                    fragmentManager?.popBackStack()


                                                }
                                                "-1" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.OperationFailed)!!)
                                                }
                                                "-2" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.DataMissing)!!)
                                                }
                                                "-3" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.UserNotExistsLabel)!!)
                                                }
                                                "-4" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.WrongPasswordLabel)!!)
                                                }
                                                "-5" -> {
                                                    mainLayout.showSnakeBar(act?.getString(R.string.NotActivatedLabel)!!)
                                                    //ContentActivity.openActivateAccountFragment();
                                                }
                                            }
                                        }
                                    }
                                    mloading.putVisibility(View.INVISIBLE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                }
                            })
                }
            }
            R.id.tv_register -> {
                fragmentManager?.popBackStack()
                val bundle = Bundle()
                bundle.putString("type", "register")
                ContentActivity.Companion.openRegisterFragment(bundle)
            }
        }
    }

    private fun isValid(): Boolean {
        return when {
            et_username.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_username))
                et_username.requestFocus()
                false
            }
            et_password.checkEmpty() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_password))
                et_password.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun getSettings() {

        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)


        QenaatAPICall.getCallingAPIInterface()?.settings()?.enqueue(object : Callback<GetAppSettings?> {
            override fun onFailure(call: Call<GetAppSettings?>, t: Throwable) {
                t.printStackTrace()
            }

            override fun onResponse(call: Call<GetAppSettings?>, response: Response<GetAppSettings?>) {
                if (response.body() != null) {
                    val settings = response.body()
                    if (settings != null) {
                        if (settings != null) {
                            tv_activate.setVisibility(View.GONE)
                            if (settings.ShowActivePage) {
                                tv_activate.setVisibility(View.VISIBLE)
                            }
                        }
                    }
                }
            }
        })
    }

    companion object {
        protected val TAG = LoginFragment::class.java.simpleName
        var act: FragmentActivity? = null
        var fragment: LoginFragment? = null
        fun newInstance(act: FragmentActivity?): LoginFragment {
            fragment = LoginFragment()
            Companion.act = act
            return fragment!!
        }
    }
}