package com.qenaat.app.fragments

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.media.ExifInterface
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jdev.countryutil.Constants
import com.jdev.countryutil.CountryUtil
import com.nguyenhoanglam.imagepicker.model.Config
import com.nguyenhoanglam.imagepicker.model.Image
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker
import com.qenaat.app.ContentActivity
import com.qenaat.app.ContentActivity.Companion.familyName
import com.qenaat.app.ContentActivity.Companion.parentId
import com.qenaat.app.R
import com.qenaat.app.Utils.Util
import com.qenaat.app.adapters.AlertAdapterFA
import com.qenaat.app.classes.*
import com.qenaat.app.classes.FixControl.checkEmptyString
import com.qenaat.app.classes.FixControl.createPartFromFile
import com.qenaat.app.classes.FixControl.createPartFromString
import com.qenaat.app.classes.FixControl.getTextValue
import com.qenaat.app.classes.FixControl.hideKeyboard
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.FixControl.softWindow
import com.qenaat.app.model.*
import com.qenaat.app.networking.QenaatAPICall
import com.squareup.picasso.Picasso
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.add_family_tree.*
import me.drakeet.materialdialog.MaterialDialog
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody


import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.reflect.Type
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by DELL on 07-Dec-17.
 */
class AddFamilyTreeFragment : Fragment(), View.OnClickListener {

    var XY: IntArray? = null
    lateinit var mainLayout: RelativeLayout
    var languageSeassionManager: LanguageSessionManager? = null
    lateinit var relative_user_image: RelativeLayout
    lateinit var relative_gender_main: RelativeLayout
    lateinit var img_user: ImageView
    lateinit var img_edit_icon: ImageView
    lateinit var img_bg: ImageView
    lateinit var img_male: ImageView
    lateinit var img_female: ImageView
    lateinit var linear_register: LinearLayout
    lateinit var linear_name: LinearLayout
    lateinit var linear_gender: LinearLayout
    lateinit var linear_male: LinearLayout
    lateinit var linear_female: LinearLayout
    private var outputFileUri: Uri? = null
    private var index = 0
    private var birthDatePickerDialog: DatePickerDialog? = null
    private var current_path: String? = ""
    private var arrayListPhotoEdit: ArrayList<String?> = ArrayList()
    private var arrayListPhotoEditTemp: ArrayList<String?> = ArrayList()
    private val arrayListPhoto: ArrayList<String?> = ArrayList()
    private val dateFormatter1: SimpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.US)
    var isEdit = true
    var imageCount = 1
    var getFamilyTree: GetFamilyTree? = null
    private val STORAGE_PERMISSION_CODE = 23
    private val CAMERA_PERMISSION_CODE = 24
    lateinit var tv_tree_name: TextView
    lateinit var tv_city: TextView
    lateinit var tv_area: TextView
    lateinit var tv_alive: TextView
    lateinit var tv_dod: TextView
    lateinit var et_name: EditText
    lateinit var et_name2: EditText
    lateinit var et_name3: EditText
    lateinit var et_name4: EditText
    lateinit var et_name5: EditText
    lateinit var et_position: EditText
    lateinit var ed_address: EditText
    lateinit var ed_details: EditText
    lateinit var et_block: EditText
    lateinit var relative_alive: RelativeLayout
    lateinit var check_box: CheckBox
    lateinit var scroll_view: ScrollView
    var cityAreaArrayList: ArrayList<GetCities?> = ArrayList()
    var getFamilyNamesArrayList: ArrayList<GetFamilyNames?> = ArrayList()
    var areaArrayList: ArrayList<GetAreas?> = ArrayList()
    var cityId: String = ""
    var areaId: String = ""
    var familyNameId: String = ""
    lateinit var tv_family_name: TextView
    var permissionStr = ""

    //var config: Config = Config()
    //val multipartTypedOutput = MultipartTypedOutput()
    private var countryCode: String = ""
    private var countryFlag: Int = 0
    private var countryName: String = ""
    private var imagePartList: MutableList<MultipartBody.Part>? = mutableListOf()
    private var textPart: RequestBody? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        countryCode = Util.getUserCountryInfo(act, ConstanstParameters.COUNTRY_CODE)
        countryName = Util.getUserCountryInfo(act, ConstanstParameters.COUNTRY_NAME)
        countryFlag = Util.getUserCountryInfo(act, ConstanstParameters.COUNTRY_FLAG).toInt()

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try {
            mainLayout = inflater.inflate(R.layout.add_family_tree, null) as RelativeLayout
            act.softWindow()
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout?) {
        if (mainLayout != null) {
            scroll_view = mainLayout.findViewById(R.id.scroll_view) as ScrollView
            tv_alive = mainLayout.findViewById(R.id.tv_alive) as TextView
            tv_dod = mainLayout.findViewById(R.id.tv_dod) as TextView
            tv_dod.setOnClickListener(this)
            tv_tree_name = mainLayout.findViewById(R.id.tv_tree_name) as TextView
            tv_tree_name.setOnClickListener(this)
            tv_city = mainLayout.findViewById(R.id.tv_governorate) as TextView
            tv_city.setOnClickListener(this)
            tv_family_name = mainLayout.findViewById(R.id.tv_family_name) as TextView
            tv_family_name.setOnClickListener(this)
            tv_area = mainLayout.findViewById(R.id.tv_area) as TextView
            tv_area.setOnClickListener(this)
            ed_address = mainLayout.findViewById(R.id.ed_address) as EditText
            /* ed_address.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.text_area) as BitmapDrawable).getBitmap().getWidth()
             ed_address.getLayoutParams().height = (act.getResources().getDrawable(
                     R.drawable.text_area) as BitmapDrawable).getBitmap().getHeight()*/
            ed_details = mainLayout.findViewById(R.id.ed_details) as EditText
            /* ed_details.getLayoutParams().width = (act.getResources().getDrawable(
                     R.drawable.text_area) as BitmapDrawable).getBitmap().getWidth()
             ed_details.getLayoutParams().height = (act.getResources().getDrawable(
                     R.drawable.text_area) as BitmapDrawable).getBitmap().getHeight()*/
            et_block = mainLayout.findViewById(R.id.et_block) as EditText
            /*et_block.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()*/
            relative_alive = mainLayout.findViewById(R.id.relative_alive) as RelativeLayout
            relative_alive.setOnClickListener(this)
            check_box = mainLayout.findViewById(R.id.check_box) as CheckBox
            relative_gender_main = mainLayout.findViewById(R.id.relative_gender_main) as RelativeLayout
            relative_user_image = mainLayout.findViewById(R.id.relative_user_image) as RelativeLayout
            img_edit_icon = mainLayout.findViewById(R.id.img_edit_icon) as ImageView
            img_male = mainLayout.findViewById(R.id.img_male) as ImageView
            img_female = mainLayout.findViewById(R.id.img_female) as ImageView
            img_bg = mainLayout.findViewById(R.id.img_bg) as ImageView
            img_user = mainLayout.findViewById(R.id.img_user) as ImageView
            linear_name = mainLayout.findViewById(R.id.linear_name) as LinearLayout
            linear_gender = mainLayout.findViewById(R.id.linear_gender) as LinearLayout
            linear_male = mainLayout.findViewById(R.id.linear_male) as LinearLayout
            linear_female = mainLayout.findViewById(R.id.linear_female) as LinearLayout
            linear_register = mainLayout.findViewById(R.id.linear_register) as LinearLayout
            et_email.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_name = mainLayout.findViewById(R.id.et_name) as EditText
            et_name.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_name2 = mainLayout.findViewById(R.id.et_name2) as EditText
            et_name2.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_name3 = mainLayout.findViewById(R.id.et_name3) as EditText
            et_name3.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_name4 = mainLayout.findViewById(R.id.et_name4) as EditText
            et_name4.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_name5 = mainLayout.findViewById(R.id.et_name5) as EditText
            et_name5.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_position = mainLayout.findViewById(R.id.et_position) as EditText
            et_position.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_mobile.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_password.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_civil_id.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_instagram.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_snapchat.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_twitter.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            et_facebook.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.input) as BitmapDrawable).getBitmap().getWidth()
            mloading = mainLayout.findViewById(R.id.loading) as ProgressBar
            ContentActivity.Companion.setTextFonts(mainLayout)
            relative_user_image.setOnClickListener(this)
            tv_dob.setOnClickListener(this)
            linear_female.setOnClickListener(this)
            linear_male.setOnClickListener(this)
            tv_nationality.setOnClickListener(this)
            tv_save.setOnClickListener(this)
            LL_country_code.setOnClickListener(this)
            profile_image.getLayoutParams().width = (act.getResources().getDrawable(
                    R.drawable.add_photo) as BitmapDrawable).getBitmap().getWidth()
            profile_image.getLayoutParams().height = (act.getResources().getDrawable(
                    R.drawable.add_photo) as BitmapDrawable).getBitmap().getHeight()
            et_email.setHint(act.getString(R.string.EmailLabel))
            ed_address.setHint(act.getString(R.string.AddressLabel))
            ed_details.setHint(act.getString(R.string.DetailsLabel))
            et_block.setHint(act.getString(R.string.Block))
            et_name.setHint(act.getString(R.string.NameLabel) + "*")
            et_name2.setHint(act.getString(R.string.SecondNameLabel))
            et_name3.setHint(act.getString(R.string.ThirdNameLabel))
            et_name4.setHint(act.getString(R.string.FourthNameLabel))
            et_name5.setHint(act.getString(R.string.FifthNameLabel))
            et_position.setHint(act.getString(R.string.PositionLabel))
            et_civil_id.setHint(act.getString(R.string.CivilId))
            //et_name.setHint(act.getString(R.string.UserNameLabel)+"*");
            et_mobile.setHint(act.getString(R.string.MobileLabel))
            et_password.setHint(act.getString(R.string.PasswordLabel))
            ////setImagePickerConfig()

            tv_country_code.isSelected = true
            tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
            iv_country_flag.setImageResource(countryFlag)
        }
    }

    override fun onStart() {
        super.onStart()
        ed_address.setMovementMethod(ScrollingMovementMethod())
        ed_details.setMovementMethod(ScrollingMovementMethod())
        check_box.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(compoundButton: CompoundButton?, b: Boolean) {
                if (b) {
                    tv_dod.setVisibility(View.GONE)
                    tv_dod.setText("")
                    death_dateValue = ""
                    getFamilyTree = GetFamilyTree()
                    getFamilyTree?.DeathDate = ""
                } else {
                    tv_dod.setVisibility(View.VISIBLE)
                }
            }
        })
        scroll_view.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                ed_address.getParent().requestDisallowInterceptTouchEvent(false)
                return false
            }
        })
        ed_address.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                ed_address.getParent().requestDisallowInterceptTouchEvent(true)
                return false
            }
        })
        scroll_view.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                ed_details.getParent().requestDisallowInterceptTouchEvent(false)
                return false
            }
        })
        ed_details.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                ed_details.getParent().requestDisallowInterceptTouchEvent(true)
                return false
            }
        })
        ContentActivity.Companion.enableLogin(languageSeassionManager)
        // ContentActivity.Companion.topBarView?.setVisibility(View.VISIBLE)
        ContentActivity.Companion.mtv_topTitle?.setText(act.getString(R.string.AddFamilyTreeLabel))
        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        if (getFamilyTree != null) {
            showData()
        } else {
            if (ContentActivity.Companion.parentId?.length!! > 0) {
                getFamilyTree = GetFamilyTree()
                if (ContentActivity.cityId.length > 0) {
                    getFamilyTree?.CityId = ContentActivity.Companion.cityId
                    cityId = ContentActivity.Companion.cityId
                }
                if (ContentActivity.Companion.familyNameId.length > 0) {
                    getFamilyTree?.FamilyNameId = ContentActivity.Companion.familyNameId
                    familyNameId = ContentActivity.Companion.familyNameId
                }
                if (ContentActivity.Companion.cityName.length > 0) {
                    getFamilyTree?.CityNameEn = ContentActivity.Companion.cityName
                }
                if (ContentActivity.Companion.cityName.length > 0) {
                    getFamilyTree?.CityNameAr = ContentActivity.Companion.cityName
                }
                if (ContentActivity.Companion.familyName.length > 0) {
                    getFamilyTree?.FamilyNameEn = ContentActivity.Companion.familyName
                }
                if (ContentActivity.Companion.familyName.length > 0) {
                    getFamilyTree?.FamilyNameAr = ContentActivity.Companion.familyName
                }
                if (ContentActivity.Companion.areaId.length > 0) {
                    getFamilyTree?.AreaId = ContentActivity.Companion.areaId
                    areaId = ContentActivity.Companion.areaId
                }
                if (ContentActivity.Companion.areaName.length > 0) {
                    getFamilyTree?.AreaNameEn = ContentActivity.Companion.areaName
                }
                if (ContentActivity.Companion.areaName.length > 0) {
                    getFamilyTree?.AreaNameAr = ContentActivity.Companion.areaName
                }
                if (ContentActivity.Companion.parentId.length > 0) {
                    getFamilyTree?.ParentId = ContentActivity.Companion.parentId
                }
                if (ContentActivity.Companion.parentNameEn.length > 0) {
                    getFamilyTree?.ParentNameEn = ContentActivity.Companion.parentNameEn
                }
                if (ContentActivity.Companion.parentNameAr.length > 0) {
                    getFamilyTree?.ParentNameAr = ContentActivity.Companion.parentNameAr
                }
                if (languageSeassionManager?.getLang().equals("en", ignoreCase = true)) {
                    tv_tree_name.setText(getFamilyTree?.ParentNameEn)
                    tv_area.setText(getFamilyTree?.AreaNameEn)
                    tv_city.setText(getFamilyTree?.CityNameEn)
                    tv_family_name.setText(getFamilyTree?.FamilyNameEn)
                } else {
                    tv_tree_name.setText(getFamilyTree?.ParentNameAr)
                    tv_area.setText(getFamilyTree?.AreaNameAr)
                    tv_city.setText(getFamilyTree?.CityNameAr)
                    tv_family_name.setText(getFamilyTree?.FamilyNameAr)
                }
            }
        }
        if (areaArrayList.size > 0) {
        } else {
            if (cityId.isNotEmpty()) {
                GetAreas()
            }
        }
        if (cityAreaArrayList.size > 0) {
        } else {
            GetCity()
        }
        if (getFamilyNamesArrayList.size > 0) {
        } else {
            GetFamilyNames()
        }
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.LL_country_code -> CountryUtil(act).setTitle("").build()
            R.id.tv_dod -> showCalendar1()
            R.id.tv_tree_name -> {
                saveDataTemporary()
                val bundle = Bundle()
                bundle.putString("comingFrom", "add")
                ContentActivity.Companion.openHomeFragment(bundle)
            }
            R.id.tv_area -> {
                if (cityId.isNotEmpty()) {
                    if (areaArrayList.size > 0) {
                        val inflater1: LayoutInflater = act
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                        val v1: View = inflater1.inflate(R.layout.country_dialog, null)
                        val listView1 = v1.findViewById<View?>(R.id.lv) as ListView?
                        val tv_title1: TextView = v1.findViewById<View?>(R.id.tv_title) as TextView
                        tv_title1.setText(act.getString(R.string.SelectArea))
                        tv_title1.setTypeface(ContentActivity.Companion.tf)
                        val arrayList1 = ArrayList<String?>()
                        for (getAreas in areaArrayList) {
                            if (languageSeassionManager?.getLang() == "en") {
                                arrayList1.add(getAreas?.TitleEn)
                            } else {
                                arrayList1.add(getAreas?.TitleAr)
                            }
                        }
                        arrayList1.add(act.getString(R.string.CancelLabel))
                        val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                        listView1?.setAdapter(alertAdapter1)
                        FixControl.setListViewHeightBasedOnChildren(listView1)
                        val alert1: MaterialDialog = MaterialDialog(act).setContentView(v1)
                        alert1.show()
                        alert1.setCanceledOnTouchOutside(true)
                        listView1?.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                            override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                                alert1.dismiss()
                                val item = arrayList1[i]
                                if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                                } else {
                                    areaId = areaArrayList.get(i)?.Id!!
                                    ContentActivity.Companion.areaId = areaId
                                    tv_area.setText(item)
                                    ContentActivity.Companion.areaName = item!!
                                }
                            }
                        })
                    }
                } else
                    mainLayout.showSnakeBar(getString(R.string.select_governorate_first))
            }
            R.id.tv_family_name -> if (getFamilyNamesArrayList.size > 0) {
                val inflater1: LayoutInflater = act
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1: View = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView?
                val tv_title1: TextView = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(act.getString(R.string.SelectFamilyName))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (getFamilyNames in getFamilyNamesArrayList) {
                    if (languageSeassionManager?.getLang() == "en") {
                        arrayList1.add(getFamilyNames?.NameEN)
                    } else {
                        arrayList1.add(getFamilyNames?.NameAR)
                    }
                }
                arrayList1.add(act.getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1?.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1: MaterialDialog = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1?.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                    override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                        alert1.dismiss()
                        val item = arrayList1[i]
                        if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                        } else {
                            familyNameId = getFamilyNamesArrayList.get(i)?.Id!!
                            ContentActivity.Companion.familyNameId = familyNameId
                            tv_family_name.setText(item)
                            ContentActivity.Companion.familyName = item!!
                        }
                    }
                })
            }
            R.id.tv_governorate -> if (cityAreaArrayList.size > 0) {
                val inflater1: LayoutInflater = act
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val v1: View = inflater1.inflate(R.layout.country_dialog, null)
                val listView1 = v1.findViewById<View?>(R.id.lv) as ListView?
                val tv_title1: TextView = v1.findViewById<View?>(R.id.tv_title) as TextView
                tv_title1.setText(act.getString(R.string.SelectGovernorate))
                tv_title1.setTypeface(ContentActivity.Companion.tf)
                val arrayList1 = ArrayList<String?>()
                for (cityArea in cityAreaArrayList) {
                    if (languageSeassionManager?.getLang() == "en") {
                        arrayList1.add(cityArea?.NameEN)
                    } else {
                        arrayList1.add(cityArea?.NameAR)
                    }
                }
                arrayList1.add(act.getString(R.string.CancelLabel))
                val alertAdapter1 = AlertAdapterFA(act, arrayList1)
                listView1?.setAdapter(alertAdapter1)
                FixControl.setListViewHeightBasedOnChildren(listView1)
                val alert1: MaterialDialog = MaterialDialog(act).setContentView(v1)
                alert1.show()
                alert1.setCanceledOnTouchOutside(true)
                listView1?.setOnItemClickListener(object : AdapterView.OnItemClickListener {
                    override fun onItemClick(adapterView: AdapterView<*>?, view: View?, i: Int, l: Long) {
                        alert1.dismiss()
                        val item = arrayList1[i]
                        if (item.equals(act.getString(R.string.CancelLabel), ignoreCase = true)) {
                        } else {
                            cityId = cityAreaArrayList.get(i)?.Id!!
                            ContentActivity.Companion.cityId = cityId
                            tv_city.setText(item)
                            ContentActivity.Companion.cityName = item!!
                            areaId = ""
                            tv_area.setText("")
                            ContentActivity.Companion.areaName = ""
                            ContentActivity.Companion.areaId = ""
                            GetAreas()
                        }
                    }
                })
            }
            R.id.relative_alive -> {
            }
            R.id.linear_male -> {
                isMale = "1"
                img_male.setImageResource(R.drawable.radio_on)
                img_female.setImageResource(R.drawable.radio_off)
            }
            R.id.linear_female -> {
                isMale = "2"
                img_male.setImageResource(R.drawable.radio_off)
                img_female.setImageResource(R.drawable.radio_on)
            }
            R.id.tv_dob -> showCalendar()
            /*R.id.relative_user_image -> {

                //First checking if the app is already having the permission
                if (isReadStorageAllowed()) {
                    if (isCameraAllowed()) {
                        saveDataTemporary()
                        openImageIntent()
                        return
                    }
                }

                //If the app has not the permission then asking for the permission
                if (isCameraAllowed()) {
                    requestStoragePermission()
                } else {
                    requestCameraePermission()
                }
            }*/
            R.id.relative_user_image -> {
                permissionStr = Manifest.permission.CAMERA
                GlobalFunctions.requestMultiplePermission(act,
                        arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE))
            }
            /* R.id.tv_save -> {
                 if (
                 //et_mobile.getText().toString().length()>0 &&
                 //et_email.getText().toString().length()>0 &&
                         et_name.getText().toString().length > 0 &&
                         familyName.isNotEmpty() &&
                         //et_position.getText().toString().length()>0 &&
                         tv_tree_name.getText().toString().length > 0
                 // tv_area.getText().toString().length()>0 &&
                 //tv_city.getText().toString().length()>0 &&
                 *//*birth_dateValue.toString().length()>0*//*) {
                    var isError = false
                    if (isMale.equals("0", ignoreCase = true)) {
                        isError = true
                    }
                    if (getFamilyTree?.IsLive.equals("false", ignoreCase = true)) {

                        *//*if(death_dateValue.length()>0){

                        }
                        else{
                            isError=true;
                        }*//*
                    }
                    if (isError) {
                        Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                    } else {
                        mloading.setVisibility(View.VISIBLE)
                        GlobalFunctions.DisableLayout(mainLayout)
                        saveData()
                        if (arrayListPhotoEdit.size > 0) {
                            index = 0
                            current_path = arrayListPhotoEdit.get(index)
                            Log.d("arrayListPhotoEdit", "" + arrayListPhotoEdit.size)
                            val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                            uploadImagesAysn.execute()
                        } else {
                            AddToFamilyTree()
                        }
                    }
                } else {
                    Snackbar.make(mainLayout, act.getString(R.string.DataMissing), Snackbar.LENGTH_LONG).show()
                }
            }*/
            R.id.tv_save -> {
                act.hideKeyboard()
                if (isValid()) {
                    mloading.visibility = View.VISIBLE
                    GlobalFunctions.DisableLayout(mainLayout)
                    saveData()
                    if (arrayListPhotoEdit.size > 0) {
                        index = 0
                        current_path = arrayListPhotoEdit.get(index)
                        uploadPhotos()
                        /*val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                        uploadImagesAysn.execute()*/
                    } else {
                        AddToFamilyTree()
                    }
                }
            }
        }
    }

    private fun isValid(): Boolean {
        when {
            tv_tree_name.getTextValue().checkEmptyString() -> {
                mainLayout.showSnakeBar(getString(R.string.select_family))
                tv_tree_name.requestFocus()
                return false
            }
            familyName.checkEmptyString() -> {
                mainLayout.showSnakeBar(getString(R.string.select_family_name))
                return false
            }
            et_name.getTextValue().checkEmptyString() -> {
                mainLayout.showSnakeBar(getString(R.string.enter_name))
                et_name.requestFocus()
                return false
            }
            isMale == "0" -> {
                mainLayout.showSnakeBar(getString(R.string.select_gender))
                return false
            }
        }
        return true
    }

    private fun uploadPhotos() {
        //multipartTypedOutput.addPart("FileObjectType", TypedString("person"))
        textPart = "person".createPartFromString()
        SaveAndGetImageTask().execute(BitmapFactory.decodeFile(current_path))
    }

    private fun uploadPhotosAPI() {
        QenaatAPICall.getCallingAPIInterface()?.uploadImage(textPart, imagePartList)?.enqueue(
                object : Callback<ArrayList<UploadFile?>?> {
                    override fun onFailure(call: Call<ArrayList<UploadFile?>?>, t: Throwable) {
                        t.printStackTrace()
                        GlobalFunctions.EnableLayout(mainLayout)
                        mloading?.setVisibility(View.GONE)
                        mainLayout?.showSnakeBar(t?.message!!)
                    }

                    override fun onResponse(call: Call<ArrayList<UploadFile?>?>, response: Response<ArrayList<UploadFile?>?>) {
                        GlobalFunctions.EnableLayout(mainLayout)
                        if (response.body() != null) {
                            val uploadFileList = response.body()

                            var isSuccess = false
                            for (i in uploadFileList?.indices!!) {
                                if (uploadFileList[i]?.Type == 1) {
                                    isSuccess = true
                                    Log.e("imageUrl", uploadFileList[i]?.FileUrl)
                                } else {
                                    isSuccess = false
                                    Log.e("imageUrlError", uploadFileList[i]?.error)
                                    mainLayout?.showSnakeBar(uploadFileList[i]?.error!!)
                                }
                            }
                            Log.e("imageUrl", images.toString())
                            Log.e("photo upload", isSuccess.toString())
                            if (isSuccess) AddToFamilyTree()
                            mloading?.setVisibility(View.GONE)
                        }
                    }
                })
    }

    //Save image in hidden folder and get it...
    internal inner class SaveAndGetImageTask() : AsyncTask<Bitmap?, Void?, File?>() {
        protected override fun doInBackground(vararg params: Bitmap?): File? {
            val file_path = Environment.getExternalStorageDirectory().absolutePath +
                    "/.Monasabatena"
            val dir = File(file_path)
            if (!dir.exists()) dir.mkdirs()
            val currentTime = "img_" + Calendar.getInstance().timeInMillis + ".png"
            images = StringBuilder()
            images?.append(currentTime)
            /*  if (images?.isNotEmpty()!!)
                  images!!.append(",")*/
            val file = File(dir, currentTime)
            val fOut: FileOutputStream
            fOut = FileOutputStream(file)
            params[0]?.compress(Bitmap.CompressFormat.PNG, 60, fOut)
            fOut.flush()
            fOut.close()
            return file
        }

        override fun onPostExecute(file: File?) {
            super.onPostExecute(file)

            //multipartTypedOutput.addPart("File", TypedFile("image/*", file))
            imagePartList?.add(createPartFromFile("File", file!!)!!)
            uploadPhotosAPI()
        }
    }

    private fun getNamesOfImages() {
        images = StringBuilder()
        if (isEdit) {
            for (i in arrayListPhotoEdit.indices) {
                val file = File(arrayListPhotoEdit.get(i))
                images?.append(getNameOfImage(arrayListPhotoEdit.get(i)!!))
                if (i != arrayListPhotoEdit.size - 1) {
                    images?.append(",")
                }
                Log.e("images", images.toString())
            }
        } else {
            for (i in arrayListPhoto.indices) {
                val file = File(arrayListPhoto.get(i))
                images?.append(getNameOfImage(arrayListPhoto.get(i)!!))
                if (i != arrayListPhoto.size - 1) {
                    if (i == 0) {
                    } else {
                        images?.append(",")
                    }
                }
                Log.e("images", images.toString())
            }
        }
    }

    private fun setEditImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
            }
            1 -> {
                img_user.setVisibility(View.GONE)
                profile_image.setVisibility(View.VISIBLE)

                //img_edit_icon.setVisibility(View.VISIBLE);
                profile_image.setTag(arrayListPhotoEdit.get(0))
                Log.d("arrayListPhotoEdit", "" + arrayListPhotoEdit.size)
                Log.d("arrayListPhotoEdit", "" + arrayListPhotoEdit.get(0))
                if (arrayListPhotoEdit.get(0)?.contains(QenaatConstant.Photo_URL!!)!!) {
                    Picasso.with(act)
                            .load(arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photo)
                            .placeholder(R.drawable.add_photo)
                            .into(profile_image)
                } else {
                    Log.d("file", "" + arrayListPhotoEdit.get(0))
                    Picasso.with(act)
                            .load("file://" + arrayListPhotoEdit.get(0))
                            .fit()
                            .centerCrop()
                            .error(R.drawable.add_photo)
                            .placeholder(R.drawable.add_photo)
                            .into(profile_image)
                }
            }
        }
    }

    private fun setImages(numberOfImages: Int) {
        getNamesOfImages()
        when (numberOfImages) {
            0 -> {
            }
            1 -> {
                profile_image.setTag(arrayListPhoto.get(0))
                img_user.setVisibility(View.GONE)
                profile_image.setVisibility(View.VISIBLE)
                img_edit_icon.setVisibility(View.VISIBLE)
                Picasso.with(act)
                        .load("file://" + arrayListPhoto.get(0))
                        .fit()
                        .centerCrop()
                        .error(R.drawable.add_photo)
                        .placeholder(R.drawable.add_photo)
                        .into(profile_image)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            ImageCropFunction(outputFileUri)
        } else if (requestCode == 2) {
            if (data != null) {
                ImageCropFunction(data.getData())
            }
        } else if (requestCode == 1) {
            Log.d("aaa", "1")
            if (data != null) {
                val bundle: Bundle = data.getExtras()!!
                val bitmap: Bitmap = bundle.getParcelable("data")!!
                Log.d("aaa", "2")

                //imageView.setImageBitmap(bitmap);
                val selectedImageUri = getImageUri(act, bitmap)
                arrayListPhotoEdit.clear()
                arrayListPhoto.clear()
                Log.d("aaa", "1 - > " + getRealPathFromURI(selectedImageUri))
                if (isEdit) {
                    arrayListPhotoEdit.add(getRealPathFromURI(selectedImageUri))
                    getFamilyTree?.Photo = getRealPathFromURI(selectedImageUri)
                } else {
                    arrayListPhoto.add(getRealPathFromURI(selectedImageUri))
                    getFamilyTree?.Photo = getRealPathFromURI(selectedImageUri)
                }
                Log.d("aaa", "2 - > " + selectedImageUri?.getPath())
                imageCount = if (isEdit) {
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                    setEditImages(arrayListPhotoEdit.size)
                    arrayListPhotoEdit.size
                } else {
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto))
                    setImages(arrayListPhoto.size)
                    arrayListPhoto.size
                }
            }
        } else if (requestCode == Config.RC_PICK_IMAGES && resultCode == Activity.RESULT_OK && data != null) {
            val imagesCameraGallery: ArrayList<Image?>? = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES)
            if (imagesCameraGallery != null) {
                for (uri in imagesCameraGallery) {
                    if (isEdit) {
                        arrayListPhotoEdit.clear()
                        arrayListPhotoEdit.add(uri?.path)
                        //arrayListPhotoEditTemp.add(uri?.path)
                    } else {
                        arrayListPhoto.clear()
                        arrayListPhoto.add(uri?.path)
                    }
                }
                imageCount = if (isEdit) {
                    val gson = Gson()
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhotoEdit))
                    setEditImages(arrayListPhotoEdit.size)
                    arrayListPhotoEdit.size
                } else {
                    val gson = Gson()
                    mSessionManager?.setImagePath(gson.toJson(arrayListPhoto))
                    setImages(arrayListPhoto.size)
                    arrayListPhoto.size
                }
            }
            //do something
        } else if (requestCode == Constants.KEY_RESULT_CODE) {
            try {
                countryName = data?.getStringExtra(Constants.KEY_COUNTRY_NAME_CODE)!!
                countryCode = data?.getStringExtra(Constants.KEY_COUNTRY_ISD_CODE)!!
                tv_country_code.text = String.format("(%s) %s", countryName, countryCode)
                countryFlag = data.getIntExtra(Constants.KEY_COUNTRY_FLAG, 0)
                iv_country_flag.setImageResource(countryFlag)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("resultCode", ""+resultCode);
        Log.d("requestCode", ""+requestCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 121) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Uri selectedImageUri;

                arrayListPhotoEdit.clear();

                arrayListPhoto.clear();

                if (isCamera) {
                    selectedImageUri = outputFileUri;
                } else {
                    selectedImageUri = data == null ? null : data.getData();
                }


                if(isEdit){

                    arrayListPhotoEdit.add(isCamera ? selectedImageUri.getPath() : getPathFromURI(selectedImageUri));

                    getFamilyTree.setPhoto(isCamera ? selectedImageUri.getPath() : getPathFromURI(selectedImageUri));

                }
                else{

                    arrayListPhoto.add(isCamera ? selectedImageUri.getPath() : getPathFromURI(selectedImageUri));

                    getFamilyTree.setPhoto(isCamera ? selectedImageUri.getPath() : getPathFromURI(selectedImageUri));

                }
                Log.d("aaa", "2");
                if(isEdit){
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit));
                    setEditImages(arrayListPhotoEdit.size());
                    imageCount = arrayListPhotoEdit.size();
                }
                else{
                    Gson gson = new Gson();
                    mSessionManager.setImagePath(gson.toJson(arrayListPhoto));
                    setImages(arrayListPhoto.size());
                    imageCount = arrayListPhoto.size();
                }
                Log.d("aaa", "3");
            }
        }
    }*/
    internal inner class UploadImagesAysn : AsyncTask<String?, String?, String?>() {
        protected override fun onPostExecute(paramString: String?) {
            Log.d("paramString", "" + paramString)
            Log.d("image updates", "" + index + " image uploaded -> " + index + 1 + " image started")
            index = index + 1
            if (isEdit) {
                if (index < arrayListPhotoEdit.size) {
                    current_path = arrayListPhotoEdit.get(index)
                    val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    index = 0

                    //images = new StringBuilder();

                    //images.append(paramString);
                    Log.d("image", "uploaded successfully")
                    if (isEdit) {
                        AddToFamilyTree()
                    } else {

                        //register();
                    }
                }
            } else {
                if (index < arrayListPhoto.size) {
                    Log.d("image", "uploaded 1")
                    current_path = arrayListPhoto.get(index)
                    val uploadImagesAysn: UploadImagesAysn = UploadImagesAysn()
                    uploadImagesAysn.execute()
                } else {
                    Log.d("image", "uploaded 2")
                    mloading.setVisibility(View.INVISIBLE)
                    GlobalFunctions.EnableLayout(mainLayout)
                    index = 0
                    Log.d("image", "uploaded successfully")
                    if (isEdit) {
                        AddToFamilyTree()
                    } else {

                        //register();
                    }
                }
            }
        }

        protected override fun doInBackground(vararg params: String?): String? {

            /*String PhotoName = GlobalFunctions.SendMultipartFile(
                    current_path, getString(R.string.UploadContentPhoto), ".jpg");*/
            if (!current_path?.contains("http://otabi.")!!) {
                var file: File? = null
                val myBitmap: Bitmap? = null
                var filename = ""
                var angle = 0
                val ei: ExifInterface?
                try {
                    ei = ExifInterface(current_path)
                    val orientation: Int = ei.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, 1)
                    when (orientation) {
                        ExifInterface.ORIENTATION_ROTATE_90 -> angle = 90
                        ExifInterface.ORIENTATION_ROTATE_180 -> angle = 180
                        ExifInterface.ORIENTATION_ROTATE_270 -> angle = 270
                    }
                } catch (e1: IOException) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace()
                }
                if (isEdit) {
//                    filename = getNameOfImage(current_path
//                    );
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    Log.d("str1", "0-> " + images.toString())
                    val str1 = images.toString().replace(getNameOfImage(current_path!!)!!, filename)
                    Log.d("str1", "1-> " + getNameOfImage(current_path!!))
                    Log.d("str1", "2-> $filename")
                    images = StringBuilder()
                    images?.append(str1)
                    Log.d("str1", "3-> " + images.toString())
                } else {
                    filename = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
                    images?.append(filename)
                    if (index != arrayListPhoto.size - 1) {
                        images?.append(",")
                    }
                }
                file = File(current_path)
                /*myBitmap = decodeSampledBitmapFromPath(file.getAbsolutePath(),
                        1000, 1000);

                Matrix matrix = new Matrix();
                matrix.postRotate(angle);
                myBitmap = Bitmap
                        .createBitmap(myBitmap, 0, 0, myBitmap.getWidth(),
                                myBitmap.getHeight(), matrix, true);

                OutputStream os;
                try {
                    os = new FileOutputStream(current_path);
                    myBitmap.compress(Bitmap.CompressFormat.PNG, 80, os);
                    os.flush();
                    os.close();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
                var conn: HttpURLConnection? = null
                var dos: DataOutputStream? = null
                var inStream: DataInputStream? = null
                val lineEnd = "\r\n"
                val twoHyphens = "--"
                val boundary = "*****"
                var bytesRead: Int
                var bytesAvailable: Int
                var bufferSize: Int
                val buffer: ByteArray
                val maxBufferSize = 1 * 1024 * 1024
                try {
                    val fileInputStream = FileInputStream(Compressor(act).compressToFile(file))
                    val url = URL(QenaatConstant.Photo_URL + "person")
                    conn = url.openConnection() as HttpURLConnection
                    conn.doInput = true
                    conn.doOutput = true
                    conn.requestMethod = "POST"
                    conn.useCaches = false
                    conn.setRequestProperty("Connection", "Keep-Alive")
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=$boundary")
                    dos = DataOutputStream(conn.outputStream)
                    dos.writeBytes(twoHyphens + boundary + lineEnd)
                    dos.writeBytes("Content-Disposition: form-data; name=\"pic\";" + " filename=\"" + filename + "\"" + lineEnd)
                    dos.writeBytes(lineEnd)
                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)
                    buffer = ByteArray(bufferSize)
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    while (bytesRead > 0) {
                        dos.write(buffer, 0, bufferSize)
                        bytesAvailable = fileInputStream.available()
                        bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    }
                    dos.writeBytes(lineEnd)
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)
                    fileInputStream.close()
                    dos.flush()
                    dos.close()
                } catch (ex: MalformedURLException) {
                    println("Error:$ex")
                } catch (ioe: IOException) {
                    println("Error:$ioe")
                }
                try {
                    inStream = DataInputStream(conn?.getInputStream())
                    var str: String?
                    while (inStream.readLine().also { str = it } != null) {
                        println(str)
                    }
                    inStream.close()
                } catch (ioex: IOException) {
                    println("Error: $ioex")
                }
            }
            return "1"
        }
    }

    fun decodeSampledBitmapFromPath(path: String?, reqWidth: Int,
                                    reqHeight: Int): Bitmap? {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options: BitmapFactory.Options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeFile(path, options)
    }

    fun calculateInSampleSize(options: BitmapFactory.Options?,
                              reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height: Int = options?.outHeight!!
        val width: Int = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            inSampleSize = if (width > height) {
                Math.round(height as Float / reqHeight as Float)
            } else {
                Math.round(width as Float / reqWidth as Float)
            }
        }
        return inSampleSize
    }

    private fun openImageIntent() {

        // Determine Uri of camera image to save.
        val root = File(Environment.getExternalStorageDirectory().toString() + File.separator + "Qenaat" + File.separator)
        root.mkdirs()
        val fname = "img_" + Calendar.getInstance().timeInMillis + ".jpg"
        val sdImageMainDirectory = File(root, fname)
        //outputFileUri = Uri.fromFile(sdImageMainDirectory)
        outputFileUri = FileProvider.getUriForFile(act!!,
                act?.applicationContext?.packageName + ".fileprovider", sdImageMainDirectory!!)
        val inflater1: LayoutInflater = act.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v1: View = inflater1.inflate(R.layout.gallery_camera, null)
        val linear_camera: LinearLayout? = v1.findViewById<View?>(R.id.linear_camera) as LinearLayout?
        val img_camera = v1.findViewById<View?>(R.id.img_camera) as ImageView?
        val tv_camera: TextView? = v1.findViewById<View?>(R.id.tv_camera) as TextView?
        val linear_gallery: LinearLayout? = v1.findViewById<View?>(R.id.linear_gallery) as LinearLayout?
        val img_gallery = v1.findViewById<View?>(R.id.img_gallery) as ImageView?
        val tv_gallery: TextView? = v1.findViewById<View?>(R.id.tv_gallery) as TextView?
        val alert1: MaterialDialog = MaterialDialog(act).setContentView(v1)
        alert1.show()
        alert1.setCanceledOnTouchOutside(true)
        linear_gallery?.setOnClickListener(View.OnClickListener {
            alert1.dismiss()
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            //intent.setType("image/*");
            //intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(intent, 2)
        })
        linear_camera?.setOnClickListener(View.OnClickListener {
            alert1.dismiss()
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
            cameraIntent.putExtra("return-data", true)
            startActivityForResult(cameraIntent, 0)
        })
    }

    fun getPathFromURI(contentUri: Uri?): String? {
        var res: String? = null
        val proj = arrayOf<String?>(MediaStore.Images.Media.DATA)
        val cursor: Cursor = act.getContentResolver().query(contentUri!!, proj, null, null, null)!!
        if (cursor.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    fun saveData() {
        getFamilyTree = GetFamilyTree()
        getFamilyTree?.Id = mSessionManager.getUserCode()
        getFamilyTree?.Photo = if (images.toString().length > 0) getNameOfImage(images.toString()) else null
        getFamilyTree?.CityId = if (ContentActivity.Companion.cityId.length > 0) ContentActivity.Companion.cityId else null
        getFamilyTree?.CityNameEn = if (ContentActivity.Companion.cityName.length > 0) ContentActivity.Companion.cityName else null
        getFamilyTree?.CityNameAr = if (ContentActivity.Companion.cityName.length > 0) ContentActivity.Companion.cityName else null
        getFamilyTree?.FamilyNameId = if (ContentActivity.Companion.familyNameId.length > 0) ContentActivity.Companion.familyNameId else null
        getFamilyTree?.FamilyNameEn = if (ContentActivity.Companion.familyName.length > 0) ContentActivity.Companion.familyName else null
        getFamilyTree?.FamilyNameAr = if (ContentActivity.Companion.familyName.length > 0) ContentActivity.Companion.familyName else null
        getFamilyTree?.AreaId = if (ContentActivity.Companion.areaId.length > 0) ContentActivity.Companion.areaId else null
        getFamilyTree?.AreaNameEn = if (ContentActivity.Companion.areaName.length > 0) ContentActivity.Companion.areaName else null
        getFamilyTree?.AreaNameAr = if (ContentActivity.Companion.areaName.length > 0) ContentActivity.Companion.areaName else null
        getFamilyTree?.ParentId = if (ContentActivity.Companion.parentId.length > 0) ContentActivity.Companion.parentId else null
        getFamilyTree?.ParentNameEn = if (ContentActivity.Companion.parentNameEn.length > 0) ContentActivity.Companion.parentNameEn else null
        getFamilyTree?.ParentNameAr = if (ContentActivity.Companion.parentNameAr.length > 0) ContentActivity.Companion.parentNameAr else null
        getFamilyTree?.ParentId = if (ContentActivity.Companion.parentId.length > 0) ContentActivity.Companion.parentId else null
        getFamilyTree?.CivilIdNo = if (et_civil_id.getText().toString().length > 0) et_civil_id.getText().toString() else null
        getFamilyTree?.PositionNameEN = if (et_position.getText().toString().length > 0) et_position.getText().toString() else null
        getFamilyTree?.PositionNameAR = if (et_position.getText().toString().length > 0) et_position.getText().toString() else null
        getFamilyTree?.NameEn = if (et_name.getText().toString().length > 0) et_name.getText().toString() else null
        getFamilyTree?.NameAr = if (et_name.getText().toString().length > 0) et_name.getText().toString() else null
        getFamilyTree?.SecondName = if (et_name2.getText().toString().length > 0) et_name2.getText().toString() else null
        getFamilyTree?.SecondName = if (et_name2.getText().toString().length > 0) et_name2.getText().toString() else null
        getFamilyTree?.ThirdNameEn = if (et_name3.getText().toString().length > 0) et_name3.getText().toString() else null
        getFamilyTree?.ThirdNameAr = if (et_name3.getText().toString().length > 0) et_name3.getText().toString() else null
        getFamilyTree?.FourthName = if (et_name4.getText().toString().length > 0) et_name4.getText().toString() else null
        getFamilyTree?.FourthName = if (et_name4.getText().toString().length > 0) et_name4.getText().toString() else null
        getFamilyTree?.FifthName = if (et_name5.getText().toString().length > 0) et_name5.getText().toString() else null
        getFamilyTree?.FifthName = if (et_name5.getText().toString().length > 0) et_name5.getText().toString() else null
        getFamilyTree?.Block = if (et_block.getText().toString().length > 0) et_block.getText().toString() else null
        getFamilyTree?.Details = if (ed_details.getText().toString().length > 0) ed_details.getText().toString() else null
        getFamilyTree?.AddressEN = if (ed_address.getText().toString().length > 0) ed_address.getText().toString() else null
        getFamilyTree?.AddressAR = if (ed_address.getText().toString().length > 0) ed_address.getText().toString() else null
        getFamilyTree?.Email = if (et_email.getText().toString().length > 0) et_email.getText().toString() else null
        getFamilyTree?.BirthDate = if (birth_dateValue?.length!! > 0) birth_dateValue else null
        getFamilyTree?.DeathDate = if (death_dateValue?.length!! > 0) death_dateValue else null
        getFamilyTree?.Gender = isMale
        getFamilyTree?.Phone = et_mobile.getText().toString()
        getFamilyTree?.Instgram = if (et_instagram.getText().toString().length > 0) et_instagram.getText().toString() else null
        getFamilyTree?.Twitter = if (et_twitter.getText().toString().length > 0) et_twitter.getText().toString() else null
        getFamilyTree?.FaceBook = if (et_facebook.getText().toString().length > 0) et_facebook.getText().toString() else null
        if (check_box.isChecked()) {
            getFamilyTree?.IsLive = "true"
            getFamilyTree?.DeathDate = ""
            death_dateValue = ""
        } else {
            getFamilyTree?.IsLive = "false"
        }
    }

    override fun onPause() {
        super.onPause()
        saveDataTemporary()
    }

    fun saveDataTemporary() {
        getFamilyTree = GetFamilyTree()
        getFamilyTree?.run {
            Id = mSessionManager.getUserCode()
            Photo = getNameOfImage(images.toString())
            CityId = ContentActivity.Companion.cityId
            CityNameEn = ContentActivity.Companion.cityName
            CityNameAr = ContentActivity.Companion.cityName
            FamilyNameId = ContentActivity.Companion.familyNameId
            FamilyNameEn = ContentActivity.Companion.familyName
            FamilyNameAr = ContentActivity.Companion.familyName
            AreaId = ContentActivity.Companion.areaId
            AreaNameEn = ContentActivity.Companion.areaName
            AreaNameAr = ContentActivity.Companion.areaName
            ParentId = ContentActivity.Companion.parentId
            ParentNameEn = ContentActivity.Companion.parentNameEn
            ParentNameAr = ContentActivity.Companion.parentNameAr
            CivilIdNo = et_civil_id.getText().toString()
            PositionNameEN = et_position.getText().toString()
            PositionNameAR = et_position.getText().toString()
            NameAr = et_name.getText().toString()
            NameEn = et_name.getText().toString()
            SecondName = et_name2.getText().toString()
            SecondName = et_name2.getText().toString()
            ThirdNameEn = et_name3.getText().toString()
            ThirdNameAr = et_name3.getText().toString()
            FourthName = et_name4.getText().toString()
            FourthName = et_name4.getText().toString()
            FifthName = et_name5.getText().toString()
            FifthName = et_name5.getText().toString()
            Block = et_block.getText().toString()
            Details = ed_details.getText().toString()
            AddressEN = ed_address.getText().toString()
            AddressAR = ed_address.getText().toString()
            Email = et_email.getText().toString()
            BirthDate = birth_dateValue
            DeathDate = death_dateValue
            Gender = isMale
            Phone = et_mobile.getText().toString()
            Instgram = et_instagram.getText().toString()
            Twitter = et_twitter.getText().toString()
            FaceBook = et_facebook.getText().toString()
        }
        if (check_box.isChecked()) {
            getFamilyTree?.IsLive = "true"
            getFamilyTree?.DeathDate = ""
            death_dateValue = ""
        } else {
            getFamilyTree?.IsLive = "false"
        }
    }

    private fun showData() {
        if (getFamilyTree != null) {
            getFamilyTree?.CityId = ContentActivity.Companion.cityId
            getFamilyTree?.CityNameEn = ContentActivity.Companion.cityName
            getFamilyTree?.CityNameAr = ContentActivity.Companion.cityName
            getFamilyTree?.FamilyNameId = ContentActivity.Companion.familyNameId
            getFamilyTree?.FamilyNameEn = ContentActivity.Companion.familyName
            getFamilyTree?.FamilyNameAr = ContentActivity.Companion.familyName
            getFamilyTree?.AreaId = ContentActivity.Companion.areaId
            getFamilyTree?.AreaNameEn = ContentActivity.Companion.areaName
            getFamilyTree?.AreaNameAr = ContentActivity.Companion.areaName
            getFamilyTree?.ParentId = ContentActivity.Companion.parentId
            getFamilyTree?.ParentNameAr = ContentActivity.Companion.parentNameAr
            getFamilyTree?.ParentNameEn = ContentActivity.Companion.parentNameEn
            if (getFamilyTree?.Photo != null) if (getFamilyTree?.Photo!!.length > 0) {
                arrayListPhotoEdit.clear()
                if (mSessionManager.getImagePath() !== "" && mSessionManager.getImagePath() != null) {
                    val gson = Gson()
                    val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.getType()
                    arrayListPhotoEdit = gson.fromJson(mSessionManager.getImagePath(), listType)
                } else {
                    arrayListPhotoEdit.add(getFamilyTree?.Photo)
                    val gson = Gson()
                    mSessionManager.setImagePath(gson.toJson(arrayListPhotoEdit))
                    setEditImages(arrayListPhotoEdit.size)
                }
                images = StringBuilder()
                images?.append(getFamilyTree?.Photo)
                setEditImages(arrayListPhotoEdit.size)
            }
            et_block.setText(getFamilyTree?.Block)
            ed_details.setText(getFamilyTree?.Details)
            et_civil_id.setText(getFamilyTree?.CivilIdNo)
            if (languageSeassionManager?.getLang().equals("en", ignoreCase = true)) {
                et_name.setText(getFamilyTree?.NameEn)
                et_name2.setText(getFamilyTree?.SecondName)
                et_name3.setText(getFamilyTree?.ThirdNameEn)
                et_name4.setText(getFamilyTree?.FourthName)
                et_name5.setText(getFamilyTree?.FifthName)
                et_position.setText(getFamilyTree?.PositionNameEN)
                tv_tree_name.setText(getFamilyTree?.ParentNameEn)
                tv_city.setText(getFamilyTree?.CityNameEn)
                tv_family_name.setText(getFamilyTree?.FamilyNameEn)
                tv_area.setText(getFamilyTree?.AreaNameEn)
                ed_address.setText(getFamilyTree?.AddressEN)
            } else {
                et_name.setText(getFamilyTree?.NameAr)
                et_name2.setText(getFamilyTree?.SecondName)
                et_name3.setText(getFamilyTree?.ThirdNameAr)
                et_name4.setText(getFamilyTree?.FourthName)
                et_name5.setText(getFamilyTree?.FifthName)
                et_position.setText(getFamilyTree?.PositionNameAR)
                tv_tree_name.setText(getFamilyTree?.ParentNameAr)
                tv_city.setText(getFamilyTree?.CityNameAr)
                tv_family_name.setText(getFamilyTree?.FamilyNameAr)
                tv_area.setText(getFamilyTree?.AreaNameAr)
                ed_address.setText(getFamilyTree?.AddressAR)
            }
            et_email.setText(getFamilyTree?.Email)
            et_mobile.setText(getFamilyTree?.Phone)
            birth_dateValue = if (getFamilyTree?.BirthDate.equals("01/01/0001", ignoreCase = true) ||
                    getFamilyTree?.BirthDate.equals("", ignoreCase = true)) {
                tv_dob.setText("")
                ""
            } else {
                tv_dob.setText(getFamilyTree?.BirthDate)
                getFamilyTree?.BirthDate
            }
            death_dateValue = if (getFamilyTree?.DeathDate.equals("01/01/0001", ignoreCase = true) ||
                    getFamilyTree?.DeathDate.equals("", ignoreCase = true)) {
                tv_dod.setText("")
                ""
            } else {
                tv_dod.setText(getFamilyTree?.DeathDate)
                getFamilyTree?.DeathDate
            }
            if (getFamilyTree?.Gender.equals("1", ignoreCase = true)) {
                isMale = "1"
            } else if (getFamilyTree?.Gender.equals("2", ignoreCase = true)) {
                isMale = "2"
            } else if (getFamilyTree?.Gender.equals("0", ignoreCase = true)) {
                isMale = "0"
            }
            if (isMale.equals("1", ignoreCase = true)) {
                img_male.setImageResource(R.drawable.radio_on)
                img_female.setImageResource(R.drawable.radio_off)
            } else if (isMale.equals("2", ignoreCase = true)) {
                img_male.setImageResource(R.drawable.radio_off)
                img_female.setImageResource(R.drawable.radio_on)
            } else if (isMale.equals("0", ignoreCase = true)) {
                img_male.setImageResource(R.drawable.radio_off)
                img_female.setImageResource(R.drawable.radio_off)
            }
            et_instagram.setText(getFamilyTree?.Instgram)
            et_twitter.setText(getFamilyTree?.Twitter)
            et_facebook.setText(getFamilyTree?.FaceBook)
            if (getFamilyTree?.IsLive.equals("true", ignoreCase = true)) {
                check_box.setChecked(true)
                death_dateValue = ""
                tv_dod.setText("")
                tv_dod.setVisibility(View.GONE)
            } else {
                check_box.setChecked(false)
                death_dateValue = getFamilyTree?.DeathDate
                tv_dod.setText(getFamilyTree?.DeathDate)
                tv_dod.setVisibility(View.VISIBLE)
            }
        }
    }

    fun showCalendar() {
        val newCalendar = Calendar.getInstance()
        birthDatePickerDialog = DatePickerDialog(act, object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                val newDate = Calendar.getInstance()
                newDate[year, monthOfYear] = dayOfMonth
                if (newDate.timeInMillis > Date().time) {
                } else {
                    birth_dateValue = dateFormatter1.format(newDate.time)
                    tv_dob.setText(birth_dateValue)
                }
            }
        }, newCalendar[Calendar.YEAR], newCalendar[Calendar.MONTH], newCalendar[Calendar.DAY_OF_MONTH])
        birthDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
        birthDatePickerDialog?.show()
    }

    fun showCalendar1() {
        val newCalendar = Calendar.getInstance()
        birthDatePickerDialog = DatePickerDialog(act, object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                val newDate = Calendar.getInstance()
                newDate[year, monthOfYear] = dayOfMonth
                if (newDate.timeInMillis > Date().time) {
                } else {
                    death_dateValue = dateFormatter1.format(newDate.time)
                    tv_dod.setText(death_dateValue)
                }
            }
        }, newCalendar[Calendar.YEAR], newCalendar[Calendar.MONTH], newCalendar[Calendar.DAY_OF_MONTH])
        birthDatePickerDialog?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
        birthDatePickerDialog?.show()
    }

    private fun AddToFamilyTree() {
        mloading.setVisibility(View.VISIBLE)
        GlobalFunctions.DisableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.AddToFamilyTree(
                "${ConstanstParameters.AUTH_TEXT} ${mSessionManager.getAuthToken()}",
                parentId,
                if (areaId.length > 0) areaId else "0",
                et_block.getText().toString(),
                ed_details.getText().toString(),
                ed_details.getText().toString(),
                et_name.getText().toString(),
                et_name.getText().toString(),
                et_name2.getText().toString(),
                et_name3.getText().toString(),
                et_name4.getText().toString(),
                et_name5.getText().toString(),
                et_position.getText().toString(),
                et_position.getText().toString(),
                birth_dateValue,
                death_dateValue,
                isMale,
                ed_address.getText().toString(),
                ed_address.getText().toString(),
                if (check_box.isChecked()) "true" else "false",
                et_mobile.getText().toString(),
                countryCode,
                et_email.getText().toString(),
                et_facebook.getText().toString(),
                et_instagram.getText().toString(),
                et_twitter.getText().toString(),
                images.toString(),
                mSessionManager.getUserCode(),
                et_civil_id.getText().toString(),
                if (familyNameId.length > 0) familyNameId else "0")?.enqueue(
                object : Callback<ResponseBody?> {

                    override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                        val body = response?.body()
                        var outResponse = ""
                        try {
                            val reader = BufferedReader(InputStreamReader(
                                    ByteArrayInputStream(body?.bytes())))
                            val out = StringBuilder()
                            val newLine = System.getProperty("line.separator")
                            var line: String?
                            while (reader.readLine().also { line = it } != null) {
                                out.append(line)
                                out.append(newLine)
                            }
                            outResponse = out.toString()
                            Log.d("outResponse", "" + outResponse)
                        } catch (ex: Exception) {
                            ex.printStackTrace()
                        }
                        if (outResponse != null) {
                            outResponse = outResponse.replace("\"", "")
                            outResponse = outResponse.replace("\n", "")
                            Log.e("outResponse not null ", outResponse)
                            if (Integer.valueOf(outResponse) > 0) {
                                Snackbar.make(mainLayout, act.getString(R.string.ContentAddedLabel), Snackbar.LENGTH_LONG).show()
                                fragmentManager?.popBackStack()
                            } else if (outResponse == "-1") {
                                Toast.makeText(act, act.getString(R.string.OperationFailed), Toast.LENGTH_LONG).show()
                            } else if (outResponse == "-2") {
                                Toast.makeText(act, act.getString(R.string.DataMissing), Toast.LENGTH_LONG).show()
                            }
                        }
                        mloading.setVisibility(View.INVISIBLE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }
                })
    }

    //We are calling this method to check the permission status
    private fun isReadStorageAllowed(): Boolean {
        //Getting the permission status
        val result = ContextCompat.checkSelfPermission(act, Manifest.permission.READ_EXTERNAL_STORAGE)

        //If permission is granted returning true
        return if (result == PackageManager.PERMISSION_GRANTED) true else false

        //If permission is not granted returning false
    }

    //Requesting permission
    private fun requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(act, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission

            //checkMyPermission();
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(act, arrayOf<String?>(Manifest.permission.READ_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
    }

    //We are calling this method to check the permission status
    private fun isCameraAllowed(): Boolean {
        //Getting the permission status
        val result = ContextCompat.checkSelfPermission(act, Manifest.permission.CAMERA)

        //If permission is granted returning true
        return if (result == PackageManager.PERMISSION_GRANTED) true else false

        //If permission is not granted returning false
    }

    //Requesting permission
    private fun requestCameraePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(act, Manifest.permission.CAMERA)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            //checkMyPermission();
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(act, arrayOf<String?>(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
    }

//    private fun setImagePickerConfig() {
//        config = Config()
//        config.setCameraOnly(false)
//        config.setMultipleMode(true)
//        config.setFolderMode(false)
//        config.setShowCamera(true)
//        config.setMaxSize(Config.MAX_SIZE)
//        config.setDoneTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_action_done))
//        config.setFolderTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_title_folder))
//        config.setImageTitle(getString(com.nguyenhoanglam.imagepicker.R.string.imagepicker_title_image))
//        config.setSavePath(SavePath.DEFAULT)
//        config.setSelectedImages(ArrayList())
//    }

    //This method will be called when the user will tap on allow or deny
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                GlobalFunctions.MULTIPLE_PERMISSION_CODE -> {
                    arrayListPhotoEdit.clear()
                    if (isEdit) {
                        //if (arrayListPhotoEdit.size < imageCount) {
                        ImagePicker.with(this)
                                .setFolderMode(false)
                                .setCameraOnly(false)
                                .setFolderTitle(act.getString(R.string.GalleryLabel))
                                .setMultipleMode(true)
                                // //.setSelectedImages(config.getSelectedImages())
                                .setMaxSize(imageCount - arrayListPhotoEdit.size)
                                .start()
                        //}
                    } else {
                        //if (arrayListPhoto.size < imageCount) {
                        ImagePicker.with(this)
                                .setFolderMode(false)
                                .setCameraOnly(false)
                                .setFolderTitle(act.getString(R.string.GalleryLabel))
                                .setMultipleMode(true)
                                //   //.setSelectedImages(config.getSelectedImages())
                                .setMaxSize(imageCount - arrayListPhoto.size)
                                .start()
                        //}
                    }
                }
                GlobalFunctions.LOCATION_PERMISSION_CODE -> {
                    ContentActivity.openCurrentLocationFragmentFadeIn()
                }
            }
        } else if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            //When click "Deny"
            if (ActivityCompat.shouldShowRequestPermissionRationale(act!!, permissionStr))
                mainLayout?.showSnakeBar(getString(R.string.accept_permission))
            //When click "Deny & Don't ask again"
            else
                GlobalFunctions.redirectAppPermission(act!!)
        }
    }

    private fun decodeFile(f: File?): Bitmap? {
        try {
            //Decode image size
            val o: BitmapFactory.Options = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            BitmapFactory.decodeStream(FileInputStream(f), null, o)

            //The new size we want to scale to
            val REQUIRED_SIZE = 500

            //Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE) scale *= 2

            //Decode with inSampleSize
            val o2: BitmapFactory.Options = BitmapFactory.Options()
            o2.inSampleSize = scale
            return BitmapFactory.decodeStream(FileInputStream(f), null, o2)
        } catch (e: FileNotFoundException) {
        }
        return null
    }

    private fun GetFamilyNames() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetFamilyNames()?.enqueue(
                object : Callback<ArrayList<GetFamilyNames>?> {

                    override fun onFailure(call: Call<ArrayList<GetFamilyNames>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetFamilyNames>?>, response: Response<ArrayList<GetFamilyNames>?>) {
                        if (response.body() != null) {
                            val getFamilyNames = response.body()
                            if (getFamilyNames != null) {
                                getFamilyNamesArrayList.clear()
                                getFamilyNamesArrayList.addAll(getFamilyNames)
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun GetCity() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetCity()?.enqueue(
                object : Callback<ArrayList<GetCities>?> {

                    override fun onFailure(call: Call<ArrayList<GetCities>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetCities>?>, response: Response<ArrayList<GetCities>?>) {
                        if (response.body() != null) {
                            val cityAreas = response.body()
                            if (cityAreas != null) {
                                cityAreaArrayList.clear()
                                cityAreaArrayList.addAll(cityAreas)
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    private fun GetAreas() {
        mloading.setVisibility(View.VISIBLE)
        QenaatAPICall.getCallingAPIInterface()?.GetAreas(cityId)?.enqueue(
                object : Callback<ArrayList<GetAreas>?> {
                    override fun onFailure(call: Call<ArrayList<GetAreas>?>, t: Throwable) {
                        t.printStackTrace()
                        mloading.setVisibility(View.INVISIBLE)
                    }

                    override fun onResponse(call: Call<ArrayList<GetAreas>?>, response: Response<ArrayList<GetAreas>?>) {
                        if (response.body() != null) {
                            val cityAreas = response.body()
                            if (cityAreas != null) {
                                areaArrayList.clear()
                                areaArrayList.addAll(cityAreas)
                            }
                            mloading.setVisibility(View.INVISIBLE)
                        }
                    }
                })
    }

    fun ImageCropFunction(uri: Uri?) {

        // Image Crop Code
        try {
            val CropIntent = Intent("com.android.camera.action.CROP")
            CropIntent.setDataAndType(uri, "image/*")
            CropIntent.putExtra("crop", "true")
            CropIntent.putExtra("outputX", 180)
            CropIntent.putExtra("outputY", 180)
            CropIntent.putExtra("aspectX", 3)
            CropIntent.putExtra("aspectY", 4)
            CropIntent.putExtra("scaleUpIfNeeded", true)
            CropIntent.putExtra("return-data", true)
            startActivityForResult(CropIntent, 1)
        } catch (e: ActivityNotFoundException) {
        }
    }

    fun getImageUri(inContext: Context?, inImage: Bitmap?): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path: String = MediaStore.Images.Media.insertImage(inContext?.getContentResolver(), inImage, "Title", null)
        return Uri.parse(path)
    }

    fun getRealPathFromURI(uri: Uri?): String? {
        val cursor: Cursor = act.getContentResolver().query(uri!!, null, null, null, null)!!
        cursor.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }

    companion object {
        protected val TAG = AddFamilyTreeFragment::class.java.simpleName
        lateinit var act: FragmentActivity
        lateinit var fragment: AddFamilyTreeFragment
        lateinit var mSessionManager: SessionManager
        lateinit var mloading: ProgressBar
        private var images: StringBuilder? = StringBuilder()
        var birth_dateValue: String? = ""
        var death_dateValue: String? = ""
        var isMale: String? = "0"
        const val RESULT_OK = -1
        fun newInstance(act: FragmentActivity): AddFamilyTreeFragment? {
            fragment = AddFamilyTreeFragment()
            Companion.act = act
            return fragment
        }

        // method to get the name of the image from the path
        fun getNameOfImage(path: String): String? {
            val index = path.lastIndexOf('/')
            return path.substring(index + 1)
        }
    }
}