package com.qenaat.app.model

/**
 * Created by DELL on 02-Dec-17.
 */
class IconTreeItem {
    var Id: String? = null
    var Color: String? = null
    var NameEn: String? = null
    var NameAr: String? = null
    var BirthYearFrom: String? = null
    var BirthYearTo: String? = null
    var Photo: String? = null
    var Address: String? = null
}