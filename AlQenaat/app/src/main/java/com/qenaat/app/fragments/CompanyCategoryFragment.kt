package com.qenaat.app.fragments

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.CompanyCategoryAdapter
import com.qenaat.app.classes.FixControl.putVisibility
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetCompanyCategories
import com.qenaat.app.networking.QenaatAPICall
import kotlinx.android.synthetic.main.service_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import java.util.*

/**
 * Created by DELL on 16-Nov-17.
 */
class CompanyCategoryFragment : Fragment(), View.OnClickListener {
    lateinit var my_recycler_view: RecyclerView
    lateinit var XY: IntArray
    lateinit var loading: ProgressBar
    lateinit var progress_loading_more: ProgressBar
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: LinearLayoutManager
    lateinit var mainLayout: RelativeLayout
    lateinit var linear_qenaat_family: LinearLayout
    lateinit var linear_others: LinearLayout
    lateinit var tv_qenaat_family: TextView
    lateinit var tv_others: TextView
    lateinit var img_bg: ImageView
    lateinit var img_line1: ImageView
    lateinit var img_line2: ImageView
    lateinit var relative_top: RelativeLayout
    lateinit var relative_qenaat_family: RelativeLayout
    lateinit var relative_others: RelativeLayout
    var getCompanyCategoriesArrayList: ArrayList<GetCompanyCategories?> = ArrayList<GetCompanyCategories?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!
        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.company_category, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        relative_qenaat_family = mainLayout.findViewById(R.id.relative_qenaat_family) as RelativeLayout
        relative_others = mainLayout.findViewById(R.id.relative_others) as RelativeLayout
        relative_top = mainLayout.findViewById(R.id.relative_top) as RelativeLayout
        linear_others = mainLayout.findViewById(R.id.linear_others) as LinearLayout
        linear_qenaat_family = mainLayout.findViewById(R.id.linear_qenaat_family) as LinearLayout
        tv_others = mainLayout.findViewById(R.id.tv_others) as TextView
        tv_qenaat_family = mainLayout.findViewById(R.id.tv_qenaat_family) as TextView
        img_line1 = mainLayout.findViewById(R.id.img_line1) as ImageView
        img_line2 = mainLayout.findViewById(R.id.img_line2) as ImageView
        img_bg = mainLayout.findViewById(R.id.img_bg) as ImageView
        relative_others.setOnClickListener(this)
        relative_qenaat_family.setOnClickListener(this)
        my_recycler_view = mainLayout.findViewById(R.id.my_recycler_view) as RecyclerView
        loading = mainLayout.findViewById(R.id.loading_progress) as ProgressBar
        progress_loading_more = mainLayout.findViewById(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view.setLayoutManager(mLayoutManager)
        my_recycler_view.setItemAnimator(DefaultItemAnimator())
        ContentActivity.Companion.setTextFonts(mainLayout)
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.CompaniesLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.VISIBLE)


        ContentActivity.Companion.enableLogin(languageSeassionManager)
        if (getCompanyCategoriesArrayList.size > 0) {
            mAdapter = CompanyCategoryAdapter(act, getCompanyCategoriesArrayList, tabNumber)
            my_recycler_view.setAdapter(mAdapter)
            setTabs(tabNumber)
        } else {
            GetCompanyCategories()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun GetCompanyCategories() {
        loading.setVisibility(View.VISIBLE)
        GlobalFunctions.EnableLayout(mainLayout)
        QenaatAPICall.getCallingAPIInterface()?.GetCompanyCategories()?.enqueue(
                object : Callback<ArrayList<GetCompanyCategories?>?> {

                    override fun onFailure(call: Call<ArrayList<GetCompanyCategories?>?>, t: Throwable) {
                        t.printStackTrace()
                        loading.setVisibility(View.GONE)
                        GlobalFunctions.EnableLayout(mainLayout)
                    }

                    override fun onResponse(call: Call<ArrayList<GetCompanyCategories?>?>, response: Response<ArrayList<GetCompanyCategories?>?>) {
                        if (response.body() != null) {
                            val getCompanyCategories = response.body()
                            if (loading != null && getCompanyCategories != null) {
                                Log.d("getContentses size", "" + getCompanyCategories.size)
                                getCompanyCategoriesArrayList.clear()
                                getCompanyCategoriesArrayList.addAll(getCompanyCategories)
                                if (getCompanyCategories.size == 0)
                                    tv_noDataFound?.putVisibility(View.VISIBLE)
                                mAdapter = CompanyCategoryAdapter(act, getCompanyCategoriesArrayList, tabNumber)
                                my_recycler_view.setAdapter(mAdapter)
                            }
                            loading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    }
                })
    }

    override fun onClick(view: View?) {
        when (view?.getId()) {
            R.id.relative_others -> setTabs(2)
            R.id.relative_qenaat_family -> setTabs(1)
        }
    }

    private fun setTabs(tab: Int) {
        when (tab) {
            1 -> {
                tabNumber = 1
                relative_qenaat_family.setBackgroundColor(Color.parseColor("#87776f"))
                tv_qenaat_family.setTextColor(Color.parseColor("#fff4e4"))
                relative_others.setBackgroundColor(Color.TRANSPARENT)
                tv_others.setTextColor(Color.parseColor("#87776f"))
                GetCompanyCategories()
            }
            2 -> {
                tabNumber = 2
                relative_qenaat_family.setBackgroundColor(Color.TRANSPARENT)
                tv_qenaat_family.setTextColor(Color.parseColor("#87776f"))
                relative_others.setBackgroundColor(Color.parseColor("#87776f"))
                tv_others.setTextColor(Color.parseColor("#fff4e4"))
                GetCompanyCategories()
            }
        }
    }

    companion object {
        protected val TAG = CompanyCategoryFragment::class.java.simpleName
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var act: FragmentActivity
        lateinit var fragment: CompanyCategoryFragment
        var tabNumber = 1
        fun newInstance(act: FragmentActivity): CompanyCategoryFragment {
            fragment = CompanyCategoryFragment()
            Companion.act = act
            return fragment
        }
    }
}