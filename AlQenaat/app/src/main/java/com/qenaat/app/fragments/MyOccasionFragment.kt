package com.qenaat.app.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.qenaat.app.ContentActivity
import com.qenaat.app.R
import com.qenaat.app.adapters.MyOccasionListAdapter
import com.qenaat.app.classes.ConstanstParameters
import com.qenaat.app.classes.FixControl.showSnakeBar
import com.qenaat.app.classes.GlobalFunctions
import com.qenaat.app.classes.LanguageSessionManager
import com.qenaat.app.classes.SessionManager
import com.qenaat.app.model.GetEvents
import com.qenaat.app.model.GetServiceTypes
import com.qenaat.app.networking.QenaatAPICall
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.InputStreamReader
import java.util.*

/**
 * Created by DELL on 12-Feb-18.
 */
class MyOccasionFragment : Fragment() {
    lateinit var XY: IntArray
    lateinit var getServiceTypes: GetServiceTypes

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act = activity!!

        try {
            XY = GlobalFunctions.getScreenWidthAndHeight(act)!!
            mSessionManager = SessionManager(act)
            languageSeassionManager = LanguageSessionManager(act)
            if (arguments != null) {
            }
        } catch (e: Exception) {
            Log.e(TAG + " " + " onCreate: "
                    + Thread.currentThread().stackTrace[2].lineNumber,
                    e.message)
        }
    }

    // Inflate the view for the fragment based on layout XML
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mainLayout = inflater.inflate(R.layout.service_list, null) as RelativeLayout
        return mainLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews(mainLayout)
    }

    fun initViews(mainLayout: RelativeLayout) {
        my_recycler_view_ = mainLayout.findViewById<View?>(R.id.my_recycler_view) as RecyclerView
        tv_noDataFound = mainLayout.findViewById<View?>(R.id.tv_noDataFound) as TextView
        mloading = mainLayout.findViewById<View?>(R.id.loading_progress) as ProgressBar
        progress_loading_more_ = mainLayout.findViewById<View?>(R.id.progress_loading_more) as ProgressBar
        mLayoutManager = LinearLayoutManager(activity)
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL)
        my_recycler_view_.setLayoutManager(mLayoutManager)
        my_recycler_view_.setItemAnimator(DefaultItemAnimator())
    }

    override fun onStart() {
        super.onStart()
        ContentActivity.Companion.mtv_topTitle.setText(R.string.MyOccasionLabel)
        ContentActivity.Companion.img_topHall.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAddAd.setVisibility(View.VISIBLE)

        ContentActivity.Companion.img_topback_.setVisibility(View.VISIBLE)
        ContentActivity.Companion.img_topmenu_.setVisibility(View.GONE)
        ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        ContentActivity.Companion.enableLogin(languageSeassionManager)
        ContentActivity.Companion.img_topAddAd.setOnClickListener(View.OnClickListener {
            if (mSessionManager.isLoggedin()
                    && mSessionManager.getUserCode() !== "" && mSessionManager.getUserCode() != null) {
                ContentActivity.Companion.clearVariables()
                val bundle = Bundle()
                bundle.putString("isEdit", "0")
                bundle.putString("contentType", "2")
                ContentActivity.Companion.openAddNewsFragment(bundle)
            } else {
                Snackbar.make(mainLayout, act.getString(R.string.LoggedIn), Snackbar.LENGTH_LONG).show()
                ContentActivity.Companion.openLoginFragment()
            }
        })
        if (getEventsArrayList.size > 0) {
            mAdapter = MyOccasionListAdapter(act, getEventsArrayList, 0)
            my_recycler_view_.setAdapter(mAdapter)
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        } else {
            GetEvents()
            ContentActivity.Companion.img_topAccount.setVisibility(View.GONE)

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    companion object {
        protected val TAG = MyOccasionFragment::class.java.simpleName
        lateinit var my_recycler_view_: RecyclerView
        lateinit var tv_noDataFound: TextView
        lateinit var mSessionManager: SessionManager
        lateinit var languageSeassionManager: LanguageSessionManager
        lateinit var mloading: ProgressBar
        lateinit var progress_loading_more_: ProgressBar
        private lateinit var mAdapter: RecyclerView.Adapter<*>
        private lateinit var mLayoutManager: LinearLayoutManager
        lateinit var mainLayout: RelativeLayout
        lateinit var act: FragmentActivity
        lateinit var fragment: MyOccasionFragment
        var page_index = 0
        var endOfREsults = false
        private var loading_flag = true
        var pastVisiblesItems = 0
        var visibleItemCount = 0
        var totalItemCount = 0
        var getEventsArrayList: ArrayList<GetEvents> = ArrayList()
        fun newInstance(act: FragmentActivity): MyOccasionFragment {
            fragment = MyOccasionFragment()
            Companion.act = act
            return fragment
        }

        fun GetEvents() {
            Log.d("GetEvents", "GetEvents-MyOccasionFragment")
            mloading.setVisibility(View.VISIBLE)
            GlobalFunctions.EnableLayout(mainLayout)
            QenaatAPICall.getCallingAPIInterface()?.GetEvents("0", "false",
                    "false", "" + page_index, mSessionManager.getUserCode(),
                    mSessionManager.getUserCode(), "0")?.enqueue(
                    object : Callback<ArrayList<GetEvents>?> {

                        override fun onFailure(call: Call<ArrayList<GetEvents>?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ArrayList<GetEvents>?>, response: Response<ArrayList<GetEvents>?>) {
                            if (response.body() != null) {
                                val getServices = response.body()
                                if (mloading != null && getServices != null) {
                                    Log.d("getContentses size", "" + getServices.size)
                                    getEventsArrayList.clear()
                                    getEventsArrayList.addAll(getServices)
                                    if (getServices.size == 0)
                                        tv_noDataFound?.visibility = View.VISIBLE
                                    mAdapter = MyOccasionListAdapter(act, getEventsArrayList, 0)
                                    my_recycler_view_.adapter = mAdapter
                                    my_recycler_view_.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                                            if (!endOfREsults) {
                                                visibleItemCount = mLayoutManager.getChildCount()
                                                totalItemCount = mLayoutManager.getItemCount()
                                                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition()
                                                if (loading_flag) {
                                                    if (visibleItemCount + pastVisiblesItems + 2 >= totalItemCount) {
                                                        if (getEventsArrayList.size != 0) {
                                                            progress_loading_more_.setVisibility(View.VISIBLE)
                                                            //GlobalFunctions.DisableLayout(mainLayout);
                                                        }
                                                        loading_flag = false
                                                        page_index = page_index + 1
                                                        GetMoreEvents()
                                                    }
                                                }
                                            }
                                        }
                                    })
                                }
                                mloading.setVisibility(View.GONE)
                                GlobalFunctions.EnableLayout(mainLayout)
                            }
                        }
                    })
        }

        private fun GetMoreEvents() {
            Log.d("GetEvents", "GetEvents-MyOccasionFragment-More")
            QenaatAPICall.getCallingAPIInterface()?.GetEvents("0", "false",
                    "false", "" + page_index, mSessionManager.getUserCode(),
                    mSessionManager.getUserCode(), "0")?.enqueue(
                    object : Callback<ArrayList<GetEvents>?> {
                        override fun onFailure(call: Call<ArrayList<GetEvents>?>, t: Throwable) {
                            t.printStackTrace()
                            progress_loading_more_.setVisibility(View.GONE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ArrayList<GetEvents>?>, response: Response<ArrayList<GetEvents>?>) {
                            if (response.body() != null) {
                                val getServices = response.body()
                                if (progress_loading_more_ != null) {
                                    progress_loading_more_.setVisibility(View.GONE)
                                    GlobalFunctions.EnableLayout(mainLayout)
                                    if (getServices != null) {
                                        if (getServices.isEmpty()) {
                                            endOfREsults = true
                                            page_index = page_index - 1
                                        }
                                        loading_flag = true
                                        getEventsArrayList.addAll(getServices)
                                        mAdapter.notifyDataSetChanged()
                                    }
                                }
                            }
                        }
                    })
        }

        fun ResendInvitationList(id: String?, invitationId: String?) {
            mloading.setVisibility(View.VISIBLE)
            GlobalFunctions.DisableLayout(mainLayout)
            QenaatAPICall.getCallingAPIInterface()?.ResendInvitationList(
                    "${ConstanstParameters.AUTH_TEXT} ${mSessionManager?.getAuthToken()}",
                    id, invitationId)?.enqueue(
                    object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            val body = response?.body()
                            var outResponse = ""
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                outResponse = out.toString()
                                Log.d("outResponse", "" + outResponse)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (outResponse.toInt() > 0) {
                                    mainLayout.showSnakeBar(act.getString(R.string.resend_event))
                                    GetEvents()
                                } else if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-3") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                }
                            }
                            mloading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        }

        fun RemindInvitationList(id: String?, invitationId: String?) {
            mloading.setVisibility(View.VISIBLE)
            GlobalFunctions.DisableLayout(mainLayout)
            QenaatAPICall.getCallingAPIInterface()?.RemindInvitationList(
                    "${ConstanstParameters.AUTH_TEXT} ${mSessionManager?.getAuthToken()}",
                    id, invitationId)?.enqueue(
                    object : Callback<ResponseBody?> {

                        override fun onFailure(call: Call<ResponseBody?>, t: Throwable) {
                            t.printStackTrace()
                            mloading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }

                        override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                            val body = response?.body()
                            var outResponse = ""
                            try {
                                val reader = BufferedReader(InputStreamReader(
                                        ByteArrayInputStream(body?.bytes())))
                                val out = StringBuilder()
                                val newLine = System.getProperty("line.separator")
                                var line: String?
                                while (reader.readLine().also { line = it } != null) {
                                    out.append(line)
                                    out.append(newLine)
                                }
                                outResponse = out.toString()
                                Log.d("outResponse", "" + outResponse)
                            } catch (ex: Exception) {
                                ex.printStackTrace()
                            }
                            if (outResponse != null) {
                                outResponse = outResponse.replace("\"", "")
                                outResponse = outResponse.replace("\n", "")
                                Log.e("outResponse not null ", outResponse)
                                if (outResponse.toInt() > 0) {
                                    mainLayout.showSnakeBar(act.getString(R.string.send_event_reminder))
                                    GetEvents()
                                } else if (outResponse == "-1") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-2") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                } else if (outResponse == "-3") {
                                    Snackbar.make(mainLayout, act.getString(R.string.OperationFailed), Snackbar.LENGTH_LONG).show()
                                }
                            }
                            mloading.setVisibility(View.INVISIBLE)
                            GlobalFunctions.EnableLayout(mainLayout)
                        }
                    })
        }
    }
}