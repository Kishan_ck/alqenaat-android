package com.qenaat.app.model

/**
 * Created by DELL on 11-Feb-18.
 */
class GetInvitationLists {
    var Id: String? = null
    var Name: String? = null
    var UserId: String? = null
    var PersonCount: String? = null
    var IsPublic: String? = null
    var Type: String? = null
    var AdminUserId: String? = null
}