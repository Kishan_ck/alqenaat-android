package com.jdev.countryutil;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;

import java.util.ArrayList;

public class CountriesActivity extends AppCompatActivity {
    public static CountriesAdapter mCountriesAdapter;
    ListView mLvCountries;
    ArrayList<Country> mCountriesList = new ArrayList<Country>();
    Toolbar mToolbar;
    String mTitle = "";
    private ImageView img_back;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);
        getInputData();
        setUpLayout();
        setUpToolbar();
        setDataInViewObjects();
    }

    private void getInputData() {
        mTitle = getIntent().getStringExtra(Constants.KEY_TITLE);
    }

    private void setDataInViewObjects() {
        try {
            for (Country c : Country.getAllCountries()) {
                mCountriesList.add(c);
            }
            //
            mCountriesAdapter = new CountriesAdapter(CountriesActivity.this,
                    R.layout.list_item_country, mCountriesList);
            mLvCountries.setFastScrollEnabled(true);
            mLvCountries.setAdapter(mCountriesAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setUpLayout() {
        mLvCountries = (ListView) findViewById(R.id.lv_countries);
        mLvCountries.setFastScrollEnabled(true);
        //
        mLvCountries.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Country country = mCountriesList.get(position);
                //
                Intent intent = new Intent();
                String countryName = country.getName();
                String countryCode = country.getCode();
                String countryDialCode = country.getDialCode();
                int countryFlag = country.getFlag();
                //
                intent.putExtra(Constants.KEY_COUNTRY_NAME, countryName);
                intent.putExtra(Constants.KEY_COUNTRY_NAME_CODE, countryCode);
                intent.putExtra(Constants.KEY_COUNTRY_ISD_CODE, countryDialCode);
                intent.putExtra(Constants.KEY_COUNTRY_FLAG, countryFlag);
                setResult(Constants.KEY_RESULT_CODE, intent);
                //
                finish();

            }
        });

    }

    public void setUpToolbar() {
        try {
            mToolbar = (Toolbar) findViewById(R.id.toolbar);
            img_back=(ImageView)findViewById(R.id.img_back);
            setSupportActionBar(mToolbar);

            if (getSupportActionBar() != null) {
                if (mTitle == null || mTitle.isEmpty())
                    getSupportActionBar().setTitle(R.string.all_countries);
                else
                    getSupportActionBar().setTitle(mTitle);
            }

            img_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_right_to_left_return,
                R.anim.anim_left_to_right_return);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_countries, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        EditText txtSearch = ((EditText)searchView.findViewById(androidx.appcompat.R.id.search_src_text));
        txtSearch.setHint(getResources().getString(R.string.enter_country));
        txtSearch.setHintTextColor(Color.WHITE);
        txtSearch.setTextColor(Color.WHITE);

        SearchManager searchManager = (SearchManager)getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setQueryHint(getResources().getString(R.string.enter_country));

   SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String query) {
                // this is your adapter that will be filtered
                if (TextUtils.isEmpty(query)) {
                    CountriesActivity.mCountriesAdapter.filter("");
                } else {
                    CountriesActivity.mCountriesAdapter.filter(query);
                }
                return false;
            }

            public boolean onQueryTextSubmit(String query) {
                // Here u can get the value "query" which is entered in the
                return false;

            }
        };
        searchView.setOnQueryTextListener(queryTextListener);



        return true;
    }

}